<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>
    <?$APPLICATION->IncludeComponent(
	"bitrix:sale.basket.basket",
	"",
	Array(
		"ACTION_VARIABLE" => "action",
		"ADDITIONAL_PICT_PROP_110" => "-",
		"ADDITIONAL_PICT_PROP_114" => "MORE_PHOTO",
		"AJAX_MODE_CUSTOM" => "Y",
		"AUTO_CALCULATION" => "Y",
		"BASKET_IMAGES_SCALING" => "adaptive",
		"COLUMNS_LIST" => array(0=>"NAME",1=>"DISCOUNT",2=>"PROPS",3=>"DELETE",4=>"DELAY",5=>"TYPE",6=>"PRICE",7=>"QUANTITY",8=>"SUM",9=>"PROPS"),
		"COLUMNS_LIST_EXT" => array("PREVIEW_PICTURE","DISCOUNT","DELETE","DELAY","SUM","PROPS"),
		"COLUMNS_LIST_MOBILE" => array("PREVIEW_PICTURE","DISCOUNT","DELETE","DELAY","SUM","PROPS"),
		"COMPATIBLE_MODE" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CORRECT_RATIO" => "Y",
		"COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
		"DEFERRED_REFRESH" => "N",
		"DISCOUNT_PERCENT_POSITION" => "bottom-right",
		"DISPLAY_MODE" => "extended",
		"EMPTY_BASKET_HINT_PATH" => "/",
		"GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_CONVERT_CURRENCY" => "N",
		"GIFTS_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_HIDE_NOT_AVAILABLE" => "N",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_MESS_BTN_DETAIL" => "Подробнее",
		"GIFTS_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_PLACE" => "BOTTOM",
		"GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
		"GIFTS_PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"GIFTS_TEXT_LABEL_GIFT" => "Подарок",
		"HIDE_COUPON" => "N",
		"LABEL_PROP" => array(),
		"OFFERS_PROPS" => array(),
		"PATH_TO_BASKET" => "basket/",
		"PATH_TO_ORDER" => SITE_DIR."order/",
		"PICTURE_HEIGHT" => "100",
		"PICTURE_WIDTH" => "100",
		"PRICE_DISPLAY_MODE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_BLOCKS_ORDER" => "props,sku,columns",
		"QUANTITY_FLOAT" => "N",
		"SET_TITLE" => "N",
		"SHOW_DISCOUNT_PERCENT" => "Y",
		"SHOW_FAST_ORDER_BUTTON" => "Y",
		"SHOW_FILTER" => "N",
		"SHOW_FULL_ORDER_BUTTON" => "Y",
		"SHOW_MEASURE" => "Y",
		"SHOW_RESTORE" => "Y",
		"TEMPLATE_THEME" => "blue",
		"TOTAL_BLOCK_DISPLAY" => array("top"),
		"USE_DYNAMIC_SCROLL" => "Y",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_GIFTS" => "Y",
		"USE_PREPAYMENT" => "N",
		"USE_PRICE_ANIMATION" => "Y"
	)
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"basket",
	Array(
		"AREA_FILE_RECURSIVE" => "Y",
		"AREA_FILE_SHOW" => "file",
		"AREA_FILE_SUFFIX" => "",
		"BIG_DATA_RCM_TYPE" => "bestsell",
		"COMPONENT_TEMPLATE" => "basket",
		"EDIT_TEMPLATE" => "standard.php",
		"PATH" => SITE_DIR."include/comp_basket_bigdata.php",
		"PRICE_CODE" => array(0=>"BASE",1=>"OPT",),
		"SALE_STIKER" => "SALE_TEXT",
		"STIKERS_PROP" => "HIT",
		"STORES" => array(0=>"1",1=>"2",2=>"",)
	)
);?>
<div class="maxwidth-theme podborki-ajax-sliders">
    <div id="79d36f6d75fc3ade710cec1a6f9335a5" class="rees46-recommend-custom"
     data-rees46-component="bitrix:catalog.top"
     data-rees46-template="products_slider"></div>
</div>   
<script>
    function alertBasketFromAdmin() {
        var generalDiv = document.getElementsByClassName('page-top-main')[0];
        generalDiv.style = 'display:flex; flex-wrap:wrap;';
        var elem = document.createElement('DIV');
        elem.className = 'notify-from-admin';
        if (screen.width > 800){
            elem.style = 'width:60%;';
        }else {
            elem.style = 'width:100%;';
        }
        $.ajax({
            url: "/ajax/admin/notifications/notificationsForBasket.php",
            context: document.body,
            success: function(data){
                elem.innerHTML = data;
            },
            error: function() {
                console.log('Ошибка загрузки уведомлений.');
            }
        });
        var nameSection = document.getElementsByClassName('pagetitle-after')[0];
        nameSection.parentNode.insertBefore(elem, nameSection.nextSibling);
    }
    alertBasketFromAdmin();
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>