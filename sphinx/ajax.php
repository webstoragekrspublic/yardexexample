<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$wordformsFilePath = __DIR__ . '/wordformsyardex.txt';
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$answer = [
    'error' => '1',
    'text' => 'Неизвестная ошибка! попробуйте снова или сохраните данные отдельно и обратитесь к администратору.',
];
if (\Helpers\SafetyValidation::isAdminOrManger(CUser::GetID())) {
    $newText = $request->getPost('wordformsText');
    $res = file_put_contents($wordformsFilePath,$newText);
    if ($res !== false) {
        $answer = [
            'error' => '0',
            'text' => 'Файл сохранен!',
        ];
    } else {
        $answer = [
            'error' => '1',
            'text' => 'Ошибка записи в файл! попробуйте снова или сохраните данные отдельно и обратитесь к администратору.',
        ];
    }
} else {
    $answer = [
        'error' => '1',
        'text' => 'нет доступа'
    ];
}
echo json_encode($answer);


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>