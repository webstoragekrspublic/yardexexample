<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$wordformsFilePath = __DIR__ . '/wordformsyardex.txt';
?>

<? if (\Helpers\SafetyValidation::isAdminOrManger(CUser::GetID())): ?>
    <?
    $fileInfo = file_get_contents($wordformsFilePath);
    ?>

    <div>Введите словоформы(синонимы):</div>
    <textarea id="wordform" style="height: 800px;resize: both;" spellcheck="false"><?= htmlspecialchars($fileInfo); ?></textarea>
    <input id="savebutton" type="submit" value="Сохранить"/>
    <div>
        <span id="wordform_status_error" style="color:red;"></span>
        <span id="wordform_status_success" style="color:green;"></span>
        <span id="wordform_status_normal" style="font-weight: bold"></span>
    </div>

    <script>
        (function () {
            function successMessage(message) {
                let elem = document.getElementById('wordform_status_success');
                setMessage(elem, message);
            }

            function normalMessage(message) {
                let elem = document.getElementById('wordform_status_normal');
                setMessage(elem, message);
            }

            function errorMessage(message) {
                alert(message);
                let elem = document.getElementById('wordform_status_error');
                setMessage(elem, message);
            }

            function setMessage(elem, message) {
                clearMessages();
                if (elem) {
                    elem.innerText = message;
                }
            }

            function clearMessages() {
                let clearDomIds = ['wordform_status_success', 'wordform_status_normal', 'wordform_status_error'];
                for (let k = 0; k < clearDomIds.length; k++) {
                    let elem = document.getElementById(clearDomIds[k]);
                    if (elem) {
                        elem.innerText = '';
                    }
                }
            }

            let saveButton = document.getElementById('savebutton');
            let textArea = document.getElementById('wordform');
            if (saveButton && textArea) {

                saveButton.onclick = function () {
                    let textValue = textArea.value;
                    let method = 'POST';
                    let url = '/sphinx/ajax.php';
                    const xhr = new XMLHttpRequest();

                    xhr.open(method, url);

                    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    xhr.onload = function () {
                        if (xhr.response) {
                            let answer = JSON.parse(xhr.response);
                            if (answer.error === "0") {
                                successMessage(answer.text);
                            } else {
                                errorMessage(answer.text);
                            }
                        } else {
                            console.log(xhr);
                            let message = 'Отсутсвует ответ от сервера... Попробуйте снова или обратитесь к администратору!';
                            errorMessage(message);
                        }
                        saveButton.removeAttribute("disabled");
                        textArea.removeAttribute("disabled");
                    };

                    xhr.onabort = xhr.onerror = xhr.ontimeout = function () {
                        let message = 'Ошибка обращения к серверу, возможно проблемы с интернетом... Попробуйте снова или обратитесь к администратору!';
                        errorMessage(message);
                        saveButton.removeAttribute("disabled");
                        textArea.removeAttribute("disabled");
                    };

                    //beforeSend
                    let message = 'Идет сохранения файла...';
                    normalMessage(message);
                    saveButton.setAttribute("disabled", "disabled");
                    textArea.setAttribute("disabled", "disabled");

                    xhr.send('wordformsText=' + encodeURIComponent(textValue));
                };


                textArea.oninput = function () {
                    normalMessage('Не забудьте сохранить изменения!');
                }
            }
        })();

    </script>


<? else: ?>
    <? LocalRedirect('/'); ?>
<? endif; ?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>