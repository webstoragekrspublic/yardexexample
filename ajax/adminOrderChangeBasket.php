<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$orderId = (int)$_GET['order_id'];

if (\Helpers\SafetyValidation::isAdminOrManger($USER->GetID())) {
    if ($orderId) {
        $APPLICATION->IncludeComponent("bitrix:sale.personal.order.detail.mail",
            "email_products_table_info_for_admin",
            array(
                "ID" => $orderId,
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "3600",
                "CACHE_GROUPS" => "N",
                "SET_TITLE" => "N",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SHOW_RESERVE_STATUS" => "Y",
            )
        );
    } else {
        echo 'Ошибка номера заказа.';
    }
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>
