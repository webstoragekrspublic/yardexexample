<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;

use Bitrix\Main,
    Bitrix\Sale\Basket,
    Bitrix\Sale;

CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");


$context = \Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();
$arBackParametrs = CNext::GetBackParametrsValues(SITE_ID);

$arResult = array('status' => false);


// используем текущую корзину
$basket = Bitrix\Sale\Basket::loadItemsForFUser(Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());


// проверяем параметр ORDER_ID и Валидность корзины
if ($request->get('order_id') > 0) {

    $ORDER_ID = intval($request->get('order_id'));

    $order = \Bitrix\Sale\Order::load($ORDER_ID);

    if ($order) {
        // методы оплаты
        $paymentCollection = $order->getPaymentCollection();
        foreach ($paymentCollection as $payment) {
            $paySysID = $payment->getPaymentSystemId(); // ID метода оплаты
            $paySysName = $payment->getPaymentSystemName(); // Название метода оплаты
        }

        // службы доставки
        $shipmentCollection = $order->getShipmentCollection();
        foreach ($shipmentCollection as $shipment) {
            if ($shipment->isSystem()) continue;
            $shipID = $shipment->getField('DELIVERY_ID'); // ID службы доставки
            $shipName = $shipment->getField('DELIVERY_NAME'); // Название службы доставки
        }


        // объект нового заказа
        $orderNew = \Bitrix\Sale\Order::create(SITE_ID, $USER->GetID());

        // код валюты
        $orderNew->setField('CURRENCY', $order->getCurrency());

        // задаём тип плательщика
        $orderNew->setPersonTypeId($order->getPersonTypeId());



        // привязываем корзину к заказу
        $orderNew->setBasket($basket);

        // задаём службу доставки
        $shipmentCollectionNew = $orderNew->getShipmentCollection();
        $shipmentNew = $shipmentCollectionNew->createItem();
        $shipmentNew->setFields(
            array(
                'DELIVERY_ID' => $shipID,
                'DELIVERY_NAME' => $shipName,
                'CURRENCY' => $order->getCurrency()
            )
        );

        // пересчёт стоимости доставки
        $shipmentCollectionNew->calculateDelivery();

        // задаём метод оплаты
        $paymentCollectionNew = $orderNew->getPaymentCollection();
        $paymentNew = $paymentCollectionNew->createItem();
        $paymentNew->setFields(
            array(
                'PAY_SYSTEM_ID' => $paySysID,
                'PAY_SYSTEM_NAME' => $paySysName,
                'SUM' => $orderNew->getPrice()
            )
        );

        // задаём свойства заказа
        $propertyCollection = $order->getPropertyCollection();
        $propertyCollectionNew = $orderNew->getPropertyCollection();

        foreach ($propertyCollection as $property) {
            // получаем свойство в коллекции нового заказа
            $somePropValue = $propertyCollectionNew->getItemByOrderPropertyId($property->getPropertyId());
            // задаём значение свойства
            $somePropValue->setValue($property->getField('VALUE'));
        }

        $orderNewDozakazPropValue = $propertyCollectionNew->getItemByOrderPropertyId($orderDozakazPropId = 40);
        $orderNewDozakazPropValue->setValue('Да '.$order->getId());


        // сохраняем новый заказ
        $orderNew->doFinalAction(true);
        $rs = $orderNew->save();

        // проверяем результат, присваивает статус ответа
        if ($rs->isSuccess()) {
            $arResult['status'] = true;
            $arResult['orderID'] = $orderNew->getId();
            if ($paySysID == 25 || $paySysID == 35) {
                $arResult['orderPaymentUrl'] = 'https://'.SITE_SERVER_NAME.'/order/payment/?ORDER_ID='.$orderNew->getId().'&PAYMENT_ID='.$orderNew->getId().'/1';
            }
        }
    }
}
echo json_encode($arResult, JSON_UNESCAPED_UNICODE);


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
