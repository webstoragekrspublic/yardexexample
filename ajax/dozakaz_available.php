<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$context = \Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();
$arBackParametrs = CNext::GetBackParametrsValues(SITE_ID);

$arResult = [
    'CAN_DOZAKAZ' => false,
    'NOT_EMPTY_CART' => false,
];

global $USER;
$orderData = \Bitrix\Sale\Order::getList([
    'select' => ['ID'],
    'filter' => ['=USER_ID' => $USER->GetID()],
    'order' => ['ID' => 'DESC'],
    'limit' => 1
]);

if ($order = $orderData->fetch()) {
    $lastOrderId = $order['ID'];
}

if ($lastOrderId) {
    $order = \Bitrix\Sale\Order::load($lastOrderId);
    $propertyCollection = $order->getPropertyCollection();
    $deliveryDateProp = $propertyCollection->getItemByOrderPropertyId($deliveryDatePropId = 20);
    $deliveryTimeProp = $propertyCollection->getItemByOrderPropertyId($deliveryTimePropId = 21);

    $basket = Bitrix\Sale\Basket::loadItemsForFUser(Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

    if (count($basket->getQuantityList())) {
        $arResult['NOT_EMPTY_CART'] = true;
    }
    if (validateLastOrderForDozakaz($deliveryDateProp, $deliveryTimeProp)) {
        $arResult['CAN_DOZAKAZ'] = true;
        $arResult['LAST_ORDER_ID'] = $lastOrderId;
        $arResult['DALIVERY_DATE'] = $deliveryDateProp->getValue();
        $arResult['DALIVERY_TIME'] = $deliveryTimeProp->getValue();
        $arResult['MIN_SUM'] =  $arBackParametrs['MIN_ORDER_PRICE'];
        $arResult['MORE_MIN_SUM'] =  $basket->getPrice() > $arBackParametrs['MIN_ORDER_PRICE'];

        $basketItems = $basket->getBasketItems();
        foreach ($basket as $basketItem) {
            $resultBasketItem = [];
            $resultBasketItem['id'] = $basketItem->getProductId();
            $resultBasketItem['name'] = $basketItem->getField('NAME');
            $resultBasketItem['url'] = $basketItem->getField('DETAIL_PAGE_URL');
            $resultBasketItem['count'] = $basketItem->getQuantity();
            $resultBasketItem['price'] = $basketItem->getPrice();
            $resultBasketItem['measure'] = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure(array($basketItem->getProductId()))[$basketItem->getProductId()]['MEASURE']['SYMBOL_RUS'];
            $resultBasketItem['finalPrice'] = $basketItem->getFinalPrice();
            $resultBasketItem['image'] = getProductImage($basketItem->getProductId());

            $arResult['CART']['items'][] = $resultBasketItem;
            $arResult['CART']['SUM'] = $basket->getPrice();
        }

        $paymentCollection = $order->getPaymentCollection();
        foreach ($paymentCollection as $payment) {
            $arResult['ORDER_PROPS']['PAYMENT']['ID'] = $payment->getPaymentSystemId();
            $arResult['ORDER_PROPS']['PAYMENT']['NAME'] = $payment->getPaymentSystemName();
        }

        $shipmentCollection = $order->getShipmentCollection();
        foreach ($shipmentCollection as $shipment) {
            if ($shipment->isSystem()) continue;
            $arResult['ORDER_PROPS']['DELIVERY']['ID'] = $shipment->getField('DELIVERY_ID');
            $arResult['ORDER_PROPS']['DELIVERY']['NAME'] = $shipment->getField('DELIVERY_NAME');
        }

        $arResult['ORDER_PROPS']['TIMESLOT']['DATE'] = $deliveryDateProp->getValue();
        $arResult['ORDER_PROPS']['TIMESLOT']['TIME'] = $deliveryTimeProp->getValue();

        $deliveryAddressProp = $propertyCollection->getItemByOrderPropertyId($deliveryAddressPropId = 7);
        $arResult['ORDER_PROPS']['ADDRESS'] = $deliveryAddressProp->getValue();
    }
}
// pr($arResult);
echo json_encode($arResult, JSON_UNESCAPED_UNICODE);

function validateLastOrderForDozakaz($deliveryDateProp, $deliveryTimeProp)
{
    $availableTimeSlots = \Custom\DeliveryTime\DeliveryTime::getInstance()->getDaysInfoForCurrentCart(6);

    if ($deliveryTimeProp->getValue() != 'любое время') {
        foreach ($availableTimeSlots as $dayTimeSlot) {
            if (!$dayTimeSlot['disabled'] && $dayTimeSlot['fullDate'] === $deliveryDateProp->getValue()) {
                foreach ($dayTimeSlot['timeSlots'] as $timeSlots) {
                    if (!$timeSlots['disabled'] && $timeSlots['time'] === $deliveryTimeProp->getValue()) {
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

function getProductImage($productId)
{
    $product = CIBlockElement::GetByID($productId);
    $arProduct = $product->GetNextElement();
    $arProductProps = $arProduct->GetFields();

    return \Helpers\CustomTools::getResizedPictSrc($arProductProps['~PREVIEW_PICTURE'], ['width' => 180, 'height' => 90]);
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
