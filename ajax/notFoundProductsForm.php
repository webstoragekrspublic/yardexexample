<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use ExternalApi\Dadata\DadataApi;

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$productsName = trim($request->get('product_name'));
$phone = trim($request->get('phone'));

$emailEventName = 'CLIENT_ACTIVE_NOTIFICATION';
$messageId = 141;

$answer = [
    'error' => 1
];

if ($productsName && $phone) {
    $iBlockInfo = CIBlock::GetList(
        [],
        [
            'TYPE' => 'clients',
            'ACTIVE' => 'Y',
            "CODE" => 'CLIENTS_NOT_FOUND_PRODUCTS',
            "CHECK_PERMISSIONS" => "N"
        ]
    )->Fetch();
    if ($iBlockInfo) {
        $iBlockId = $iBlockInfo['ID'];
        $el = new CIBlockElement();
        $arLoadProductArray = array(
            "IBLOCK_ID" => $iBlockId,
            "NAME" => $productsName,
            "ACTIVE" => "Y",
            "PREVIEW_TEXT" => $phone,
        );
        if ($newElementId = $el->Add($arLoadProductArray)) {
            $body = "<p>не найдено: " . htmlspecialchars($productsName) . "</p>";
            $body .= "<p>Телефон: " . htmlspecialchars($phone) . "</p>";
            $fields = [
                'THEME' => 'Форма "не найден товар"',
                'BODY_TEXT' => $body
            ];

            \Bitrix\Main\Mail\Event::SendImmediate(array(
                "EVENT_NAME" => $emailEventName,
                "LID" => SITE_ID,
                "C_FIELDS" => $fields,
                "MESSAGE_ID" => $messageId
            ));
            $answer = [
                'error' => 0
            ];
        }
    }
}
echo json_encode($answer);
