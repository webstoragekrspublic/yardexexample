<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$request =  \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$mob = $request->get('mobile_version');
?>

<? $APPLICATION->IncludeComponent(
    "custom:empty_component",
    "search_title_items",
    ["MOBILE_VERSION" => "$mob"],
    false
);
?>

<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>