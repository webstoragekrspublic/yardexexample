<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$products = \Bitrix\Sale\Internals\DiscountTable::getList(
    ['filter'=>['XML_ID'=>'PROGRESS_BAR%', 'ACTIVE'=>'Y'],
        'select'=>['CONDITIONS_LIST', 'ACTIONS_LIST']]
);
$iblockID = 114;
$arrId = [];
$arrIdDiscount = [];
$imgIds = [];
$outID = [];
$printDiv=[];


die();// временное отключение прогрес бара
if ($products->getSelectedRowsCount() != 0):
    $arBackParametrs = CNext::GetBackParametrsValues(SITE_ID);
    $minPrice = $arBackParametrs['MIN_ORDER_PRICE'];
    ?>
    <div class="progress-in-down-screen" id="progressBar">
        <div class="text-popup-products-progress-bar" data-fancybox="" href="#moveGift">
            <a class="text-popup-products-progress-bar-name">Подарки</a>
        </div>
        <div class="progress with-text">
            <div class="progress-bar fill">
            </div>
            <div class="one-item-progress" value="<?=$minPrice?>" style="left:31.45%;"><div class="progress-value-text"><?=$minPrice?> </div><div class="progress-rub-text">руб.</div></div><!-- бесплатная доставка -->
            <?
            while($item = $products->fetch()) {
                foreach ($item['CONDITIONS_LIST']['CHILDREN'] as $children) {
                    if ($children['CLASS_ID'] == 'CondBsktAmtGroup') {
                        $val = $children['DATA']['Value'];
                        $str = '<div class="one-item-progress" value="'.$val.'"><div class="progress-value-text">'.$val.'</div><div class="progress-rub-text">руб.</div></div>';
                        $printDiv += [$val=>$str];
                    }
                }
                if (count($item['ACTIONS_LIST']['CHILDREN']) == 1){
                    foreach ($item['ACTIONS_LIST']['CHILDREN'] as $childrenAct) {
                        if ($childrenAct['CLASS_ID'] == 'ActSaleBsktGrp' && $childrenAct['DATA']['Type'] == 'Discount') {
                            $arrIdDiscount += [$val => ['VALUE' => $childrenAct['DATA']['Value'], 'NAME' => $childrenAct['DATA']['Unit'], 'TYPE' => $childrenAct['DATA']['Type']]];
                        }
                    }
                }
                else {
                    foreach ($item['ACTIONS_LIST']['CHILDREN'] as $childrenAct) {
                        if ($childrenAct['CLASS_ID'] == 'ActSaleBsktGrp') {
                            foreach ($childrenAct['CHILDREN'] as $isProdact) {
                                if ($isProdact['CLASS_ID'] == 'CondBsktFldProduct') {
                                    $arrId += [$val => $isProdact['DATA']['value']];
                                }
                            }
                        }
                    }
                }
            }
            if ($arrId || $arrIdDiscount){
                $products = CIBlockElement::getList(
                    [],
                    ['IBLOCK_ID' => $iblockID, 'ID'=>$arrId],
                    false,
                    false,
                    ['select'=>'PREVIEW_PICTURE','ID', 'CATALOG_AVAILABLE']
                );
                while ($item = $products->fetch()){
                    if ($item['CATALOG_AVAILABLE'] == 'Y') {
                        $imgIds += [$item['ID'] => $item['PREVIEW_PICTURE']];
                    }
                    else{
                        foreach($arrId as $key => $val){
                            if ($val == $item['ID']){
                                unset($arrId[$key]);
                            }
                        }
                    }
                }
                if ($arrIdDiscount){
                    $arrId = array_replace($arrId, $arrIdDiscount);
                }
                ksort($arrId);
                foreach ($arrId as $sum=>$id){
                    $outID += [$sum=>$imgIds[$id]];
                    ?>
                        <?=$printDiv[$sum];?>
                <?}
            }
            ?>
        </div>
    </div>

    <div style="display: none;" id="moveGift" class="col-sm-12">
        <h2 class="not_found_products_h">Получите подарки за покупки</h2>
        <p class="not_found_products_description">Вы можете выбрать свой подарок при оформлении заказа</p>
        <form id="temp">
            <div class="not_found_products_block">
                <div class="not_found_sale_1000">
                    <img class="not_found_products_gift_block_image_1000" src="/images/popups/sale1000.png"/>
                    <span class="not_found_products_gift_block_text_1000">Доставка бесплатно от <?=$minPrice;?>р</span>
                </div>
                <?
                foreach ($outID as $sum=>$imgId):
                    if ($imgId != null):
                        $imgSrc = \Helpers\CustomTools::getResizedPictSrc($imgId, ['width' => 150, 'height' => 150], BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
                    ?>
                    <div class="not_found_products_gift_block">
                        <div class="not_found_products_gift_block_summ">наберите <?=$sum?> руб. и получите:</div>
                        <img class="not_found_products_gift_block_image" src="<?=$imgSrc?>"/>
                    </div>
                    <?
                    else:
                        if ($arrId[$sum]['TYPE'] == 'Discount'):
                            if ($arrId[$sum]['NAME'] == 'Perc'){
                                $discounStr = (int)$arrId[$sum]['VALUE'] . '% на общую сумму заказа' ;
                            }
                            else{
                                $discounStr = (int)$arrId[$sum]['VALUE'] . 'руб. на общую сумму заказа';
                            }?>
                            <div class="not_found_products_gift_block discount">
                                <div class="not_found_products_gift_block_summ">наберите <?=$sum?> руб. и получите:</div>
                                <span class="not_found_products_gift_block_image"><?=$discounStr?></span>
                            </div>
                        <?
                        endif;
                    endif;
                endforeach;?>
            </div>
        </form>
    </div>
<?endif;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>