<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

global $USER;

$context = \Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();
$itemId = $request->get('item_id');

$response = ['status' => 'error'];
if (\Bitrix\Main\Loader::includeModule('custom.oftentimesproducts') && $USER->IsAuthorized()) {

    $otpHelper = \Custom\OftentimesProducts\OftentimesProductsHelper::getInstance();
    $curUserAllOTPs = $otpHelper->loadCurUserAllOftentimeProducts();

    if (isset($curUserAllOTPs[$itemId])) {
        $firstDate = $curUserAllOTPs[$itemId]['TIME_FIRST_ADDED']->getTimestamp();
        $lastDate = $curUserAllOTPs[$itemId]['TIME_LAST_ADDED']->getTimestamp();

        $seconds = abs($firstDate - $lastDate);
        $days = floor($seconds / 86400);

        $period = ceil(floor($seconds / 86400) / $curUserAllOTPs[$itemId]['ORDER_COUNT']);
        $period = ($period < 1 ? 1 : $period);

        $response['order_count'] = $curUserAllOTPs[$itemId]['ORDER_COUNT'] . ' ' . getInclinationByNumber($curUserAllOTPs[$itemId]['ORDER_COUNT'], ['раз', 'раза', 'раз']);
        $response['order_period'] = $period . ' ' . getInclinationByNumber($period, ['день', 'дня', 'дней']);
        $response['days'] = $days;
        $response['status'] = 'ok';
    }
}
echo json_encode($response);
