<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//$products = CSaleDiscount::getList(
//    ['PRIORITY'=>'ASC','SORT'=>'ASC', 'ID'=>2042],
//    [],
//    false,
//    false,
//    ['ACTIVE_FROM','ACTIVE_TO','ACTIVE','NAME','SORT','PRIORITY','LAST_DISCOUNT','LAST_LEVEL_DISCOUNT','LID','ID','USER_GROUPS','CODE','UNPACK','APPLICATION','USE_COUPONS']
//);
$products = \Bitrix\Sale\Internals\DiscountTable::getList(
    ['filter'=>['XML_ID'=>'PROGRESS_BAR%', 'ACTIVE'=>'Y'],
        'select'=>['CONDITIONS_LIST']]
);
?>
<div class="notifications-ajax-container-basket">
    <?php
    while($item = $products->fetch()){
        foreach ($item['CONDITIONS_LIST']['CHILDREN'] as $children){
            if ($children['CLASS_ID'] == 'CondBsktAmtGroup'){
                $vals[] = $children['DATA']['Value'];
            }
        }
        ?>
        <div class="notifications-ajax-text-basket">
            <a href="<?=$product['TEXT_HREF']['VALUE'] ? $product['TEXT_HREF']['VALUE'] : '#'?>" target="<?if($product['TEXT_HREF']['VALUE'] != null){echo '_blank';}?>" style="<?if($product['TEXT_HREF']['VALUE'] == null){echo 'text-decoration:none;cursor:default;color:black;';}?>"><?=$product['TEXT_NOTIFY']['VALUE']?></a>
        </div>
        <?
    }
    ?>
</div>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>
