<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
\Bitrix\Main\Loader::includeModule('sale');
if($_GET['active'] && $_GET['name']){

}
$SiteID = 's1';
$active = 'Y';
$name = 'Hellow';
$sumGift = 2500.00;
$productID = 39376;
$XML_ID = 'PROGRESS_BAR_' . $sumGift;
global $APPLICATION;

$products = \Bitrix\Sale\Internals\DiscountTable::getList(
    ['filter'=>['XML_ID'=>'PROGRESS_BAR%', 'ACTIVE'=>'Y'],
        'select'=>['*']]
);

while($item = $products->Fetch()) {
    $item['ACTIVE'] == 'Y' ? $active = 'yes' : $active = 'no';
}
$conditionList = [
    'CLASS_ID '=>'CondGroup',
    'DATA'=>[
        'All'=>'AND',
        'True'=>'True'
    ],
    'CHILDREN'=>[
        [
            'CLASS_ID'=>'CondSaleOrderSumm',
            'DATA'=>['logic'=>'EqGr','value'=>$sumGift]
        ]
    ]
];
$actionList = [
    'CLASS_ID'=>'CondGroup',
    'DATA'=>['All'=>'AND'],
    'CHILDREN'=>
        [
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Closeout','Value'=>1.0,'Unit'=>'CurEach','Max'=>1,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>1.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>50.0,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>2.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>33.33,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>3.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>25.0,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>4.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>20.0,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>5.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>16.665,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>6.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>14.0,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>7.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>12.5,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>8.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>11.11,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>9.0]
                    ]
                ]
            ],
            [
                'CLASS_ID'=>'ActSaleBsktGrp',
                'DATA'=>['Type'=>'Discount','Value'=>10,'Unit'=>'Perc','Max'=>0,'All'=>'AND','True'=>'True'],
                'CHILDREN'=>[
                    [
                        'CLASS_ID'=>'CondBsktFldProduct',
                        'DATA'=>['logic'=>'Equal','value'=>$productID]
                    ],
                    [
                        'CLASS_ID'=>'CondBsktFldQuantity',
                        'DATA'=>['logic'=>'Equal','value'=>10.0]
                    ]
                ]
            ]
        ]
];
$rulesBasket = [
    'XML_ID'=>$XML_ID,
    'LID'=>$SiteID,
    'NAME'=>$name,
    'CURRENCY'=>'RUB',
    'DISCOUNT_VALUE'=>'0.00',
    'DISCOUNT_TYPE'=>'P',
    'ACTIVE '=>$active,
    'SORT'=>'100',
    'PRIORITY'=>'1',
    'LAST_DISCOUNT'=>'Y',
    'LAST_LEVEL_DISCOUNT'=>'N',
    'VERSION' => 3,
    'EXECUTE_MODULE'=>'sale',
    'CONDITIONS_LIST'=>$conditionList,
    'ACTIONS_LIST'=>$actionList,
    'USER_GROUPS'=>[3]

];

$ID = \Bitrix\Sale\Internals\DiscountTable::Add($rulesBasket);


$test=1;
?>
