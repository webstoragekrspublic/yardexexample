<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$inputNotificationsId = $_GET['notify_id'];

for ($i = 0; $i < count($inputNotificationsId); $i++){
    $inputNotificationsId[$i] = (int)$inputNotificationsId[$i];
}

$products = CIBlockElement::getList(
    [],
    ['IBLOCK_CODE' => 'NOTIFY_BASKET_FORM_ADMIN', 'ID'=>$inputNotificationsId],
    false,
    false,
    []
);
$success = false;
if ($inputNotificationsId) {
    while ($prod = $products->Fetch()) {
        $success = CIBlockElement::Delete($prod['ID']);
    }
}

return $success;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>