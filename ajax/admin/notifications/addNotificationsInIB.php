<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if($_GET['title'] && $_GET['text'] && $_GET['active'] && $_GET['mode'])
{
    $title = strip_tags($_GET['title']);
    $text = strip_tags($_GET['text']);
    $active = strip_tags($_GET['active']);
    $mode = strip_tags($_GET['mode']);
    $href = strip_tags($_GET['href']);

    if ($active == 'yes'){$active="Y";}else{$active="N";}

    $el = new CIBlockElement;
    $iblock_id = 133;//138 - тестовый сервер

    $PROP = array();
    $PROP['TEXT_NOTIFY'] = $text;
    $PROP['TEXT_HREF'] = $href;

    //Основные поля элемента
    $fields = array(
        "DATE_CREATE" => date("d.m.Y H:i:s"),
        "CREATED_BY" => $GLOBALS['USER']->GetID(),
        "IBLOCK_ID" => $iblock_id,
        "PROPERTY_VALUES" => $PROP,
        "NAME" => $title,
        "ACTIVE" => $active,
    );
    if ($mode == 'add') {
        if ($ID = $el->Add($fields)) {
            echo '<div>Уведомление добавлено.</div>';
        } else {
            echo '<div>Произошел сбой, попробуйте еще раз.</div>';
        }
    }
    elseif ($mode == 'update' && $_GET['id']){
        $idNotify = (int)$_GET['id'];
        if ($ID = $el->Update($idNotify, $fields)) {
            echo '<div>Уведомление изменено.</div>';
        } else {
            echo '<div>Произошел сбой, попробуйте еще раз.</div>';
        }
    }
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>