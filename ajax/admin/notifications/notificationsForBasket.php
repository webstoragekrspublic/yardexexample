<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$products = CIBlockElement::getList(
    [],
    ['IBLOCK_CODE' => 'NOTIFY_BASKET_FORM_ADMIN', 'ACTIVE'=>'Y'],
    false,
    false,
    []
);
?>
<div class="notifications-ajax-container-basket">
    <?php
    while($item = $products->getNextElement()){
        $id = $item->getFields()['ID'];
        $product = $item->getProperties();
        ?>
        <div class="notifications-ajax-text-basket">
            <a href="<?=$product['TEXT_HREF']['VALUE'] ? $product['TEXT_HREF']['VALUE'] : '#'?>" target="<?if($product['TEXT_HREF']['VALUE'] != null){echo '_blank';}?>" style="<?if($product['TEXT_HREF']['VALUE'] == null){echo 'text-decoration:none;cursor:default;color:black;';}?>"><?=$product['TEXT_NOTIFY']['VALUE']?></a>
        </div>
        <?
    }
    ?>
</div>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>
