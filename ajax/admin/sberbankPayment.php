<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \ExternalApi\SberbankPayment\Sberbank;

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$action = $request->get('action');
$orderId = $request->get('orderId');
$orderPaymentId = $request->get('orderPaymentId');

$resInfo = [
    'error' => 1,
    'errorText' => 'ошибка',
];
switch ($action) {
    case 'status':
        $resInfo = Sberbank::getOrderPaymentInfo($orderId);
        break;
    case 'confirmOrder':
        $resInfo = Sberbank::confirmOrderPayment($orderId, $orderPaymentId);

        if (\Bitrix\Main\Loader::includeModule('custom.orderpaycheck')) {
            \Custom\OrderPaycheck\OrderPaycheckPull::addOrderToPullIfNotExist($orderId);
        }

        break;
}

die(json_encode($resInfo, JSON_UNESCAPED_UNICODE));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
