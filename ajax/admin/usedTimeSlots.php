<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$usedTimeSlots = [];;
if (\Helpers\SafetyValidation::isAdminOrManger(\CUser::GetID())) {
    $usedTimeSlots = \Custom\DeliveryTime\DeliveryTime::getInstance()->getUsedSlots();
}

die(json_encode($usedTimeSlots, JSON_UNESCAPED_UNICODE));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");