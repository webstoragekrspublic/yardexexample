<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use ExternalApi\Dadata\DadataApi;

$query = $_REQUEST['q'];


$suggestions = \ExternalApi\Dadata\Dadata::getStandartedAddress($query);

// pr($suggestions);
echo json_encode($suggestions, JSON_UNESCAPED_UNICODE);
