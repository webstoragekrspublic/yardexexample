<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
if (\Bitrix\Main\Loader::includeModule('custom.productcomments')) {
    $idProduct = $request->get('idProduct');
    $page = $request->get('page');
    $APPLICATION->IncludeComponent(
        "custom:empty_component",
        "comments_for_product",
        [
            'ID_PRODUCT' => $idProduct,
            'PAGE' => $page,
        ],
        false
    );
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");