<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$answer = [
    'error' => 1,
    'errorText' => 'произошла ошибка, попробуйте позже'
];
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$emailEventName = 'CLIENT_ACTIVE_NOTIFICATION';
$messageId = 141;

if (\Bitrix\Main\Loader::includeModule('custom.productcomments')) {
    $idProduct = $request->get('idProduct');
    $rating = $request->get('rating');
    $comment = $request->get('comment');
    $res = \Custom\ProductComments\ProductComments::addCommentByCurrentUser($idProduct, $comment, $rating);
    if ($res) {

        try {
            $body = "<p>Пользователь: " . htmlspecialchars($USER->GetFirstName().' '.$USER->GetLastName()) . "</p>";
            $body .= "<p>Товар: " . htmlspecialchars($idProduct.' '.CIBlockElement::GetByID($idProduct)->Fetch()['NAME']) . "</p>";
            $body .= "<p>Рейтинг: " . htmlspecialchars($rating) . "</p>";
            $body .= "<p>Комментарий: " . htmlspecialchars($comment) . "</p>";
            $fields = [
                'THEME' => 'Оставлен новый комментарий',
                'BODY_TEXT' => $body
            ];

            \Bitrix\Main\Mail\Event::SendImmediate(array(
                "EVENT_NAME" => $emailEventName,
                "LID" => SITE_ID,
                "C_FIELDS" => $fields,
                "MESSAGE_ID" => $messageId
            ));
        } catch (\Exception $exception){

        }

        $answer = [
            'error' => 0,
            'errorText' => ''
        ];
    } else {
        $answer = [
            'error' => 1,
            'errorText' => implode("<br>", \Custom\ProductComments\ProductComments::getLastErrors())
        ];
    }
}

echo json_encode($answer);
