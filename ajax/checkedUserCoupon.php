<?php
define('NO_KEEP_STATISTIC','Y');
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK',true);
define('PUBLIC_AJAX_MODE',true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;

global $USER;

$arResult = [
    'isSuccessCoupon' => true
];

$context = Application::getInstance()->getContext();
$coupon = $context->getRequest()->get('coupon');

$userId = $USER->GetID();

$dbUserResult = CUser::GetByID($userId)->Fetch();

if (!empty($dbUserResult['UF_USED_COUPON'])) {
    $arUserCoupons = [];
    foreach ($dbUserResult['UF_USED_COUPON'] as $userCoupon) {
        $arUserCoupons[] = mb_strtolower($userCoupon);
    }

    if (in_array(mb_strtolower($coupon), $arUserCoupons)) {
        $arResult['isSuccessCoupon'] = false;
    }
}

echo json_encode($arResult, JSON_UNESCAPED_UNICODE);