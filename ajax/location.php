<?
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

use ExternalApi\Dadata\DadataApi;

$query = $_REQUEST['q'];
$useAllowedLocation = !isset($_REQUEST['showAll']);

$suggestions = \ExternalApi\Dadata\Dadata::getMainSuggestAddresses($query, $useAllowedLocation);

echo json_encode($suggestions, JSON_UNESCAPED_UNICODE);
