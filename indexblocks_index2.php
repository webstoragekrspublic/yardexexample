<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? global $isShowSale, $isShowCatalogSections, $isShowCatalogElements, $isShowMiddleAdvBottomBanner, $isShowBlog, $arBackParametrs; ?>


    <div class="maxwidth-theme nopadding_top">
        <h1 class="text-center">Доставка продуктов питания на дом</h1>

        <? $APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"custom_main_slider_top", 
	array(
		"ACTIVE_DATE_FORMAT" => "j F Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"FILTER_NAME" => "productSlidersMain",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "124",
		"IBLOCK_TYPE" => "-",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "99",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PRODUCT_COUNT" => "16",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "LINK_GOODS_FILTER",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ID",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "custom_main_slider_top",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y"
	),
	false
); ?>

    </div>
    <div class="maxwidth-theme nopadding_top">
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "custom_main_slider_after_top",
            array(
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array("DETAIL_PICTURE", ""),
                "FILTER_NAME" => "productSlidersMain",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "125",
                "IBLOCK_TYPE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "99",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_TITLE" => "",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PRODUCT_COUNT" => "16",
                "PROPERTY_CODE" => array("LINK_GOODS_FILTER", ""),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "DESC",
                "STRICT_SECTION_CHECK" => "N"
            ),
            false
        ); ?>
    </div>

   
    <div class="maxwidth-theme">       
        <? $APPLICATION->IncludeComponent(
            "custom:empty_component",
            "oftentimes_products_main",
            array(
                "CACHE_TIME" => "360000",
                "CACHE_TYPE" => "A",
                "RETURN_HTML" => ""
            )
        ); ?>
    </div>
    <div class="maxwidth-theme podborki-ajax-sliders">
        <?
        global $productSlidersMain;
        $productSlidersMain = [
            'PROPERTY_SHOW_IN_SLIDER_VALUE' => 'Y'
        ];
        //        "ADDITIONAL_BLOCKS" => "[[\"1042\",\"1\"],[\"1059\",\"321\"],[\"1058\",\"321\"],[\"986\",\"3213\"]]",
        ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "product_sliders_main",
            array(
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "ADDITIONAL_BLOCKS" => "",
                "ADDITIONAL_IBLOCK_ID" => "114",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array("", ""),
                "FILTER_NAME" => "productSlidersMain",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "120",
                "IBLOCK_TYPE" => "aspro_next_content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "99",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_TITLE" => "",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PRODUCT_COUNT" => "16",
                "PROPERTY_CODE" => array("LINK_GOODS_FILTER", ""),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "DESC",
                "STRICT_SECTION_CHECK" => "N"
            ),
            false,
            array(
                'HIDE_ICONS' => 'Y'
            )
        ); ?>
    </div>

    <div class="grey_background">
        <div class="maxwidth-theme grey_background">
            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "main_page_categories_list",
                array(
                    "VIEW_MODE" => "",
                    "SHOW_PARENT_NAME" => "N",
                    "IBLOCK_TYPE" => "catalog1c77",
                    "IBLOCK_ID" => "114",
                    "SECTION_ID" => '',
                    "SECTION_CODE" => "",
                    "SECTION_URL" => "",
                    "COUNT_ELEMENTS" => "N",
                    "TOP_DEPTH" => "4",
                    "SECTION_FIELDS" => "",
                    "SECTION_USER_FIELDS" => "",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_NOTES" => "",
                    "CACHE_GROUPS" => "N"
                )
            ); ?>
        </div>
    </div>

    <div class="maxwidth-theme main_page_promo_text">
        <div class="main_page_promo_text_left">
            <div class="main_page_promo_text_left_text"><span class="orange_color">Ярбокс –</span><span> доставка продуктов на дом в Красноярске</span>
            </div>
            <div class="main_page_promo_text_right">
                
                <picture>
                    <source type="image/webp" srcset="<?=Helpers\CustomTools::getWebpImgSrc('/images/mainpage/promo_text_bg.jpg');?>">
                    <source type="image/jpeg" srcset="/images/mainpage/promo_text_bg.jpg">
                    <img class="hidden-xs" srcset="/images/mainpage/promo_text_bg.jpg" src="/images/mainpage/promo_text_bg.jpg"/>
                </picture>
                <picture>
                    <source type="image/webp" srcset="<?=Helpers\CustomTools::getWebpImgSrc('/images/mainpage/promo_text_bg_mobile.jpg');?>">
                    <source type="image/jpeg" srcset="/images/mainpage/promo_text_bg_mobile.jpg">
                    <img class="main_page_promo_text_right_mobile_img visible-xs" srcset="/images/mainpage/promo_text_bg_mobile.jpg" src="/images/mainpage/promo_text_bg_mobile.jpg"/>
                </picture>
            </div>
        </div>
        <div class="main_page_promo_text_bottom">
            <p class="main_page_promo_text_bottom_p1">
                Новый интернет-магазин продуктов в Красноярске с собственным складом и курьерской службой. В нашем
                онлайн-каталоге 5000 продуктов питания. Каждое наименование проходит тщательный контроль качества, а
                каждый
                заказ бережно собирается на специализированном складе.
            </p>
            <p class="main_page_promo_text_bottom_p2">
                На нашем складе соблюдаются нормы товарного соседства и контроль сроков годности, а для сохранности
                продуктов во время транспортировки используются только современные автомобили с необходимым
                температурным
                режимом. Благодаря пунктуальной доставке и 2-х часовым временным интервалам получения Ярбокс гарантирует
                высокий уровень сервиса.
            </p>
            <p class="main_page_promo_text_bottom_p3">
                Купить продукты питания с доставкой на дом можно в онлайн-магазине или с помощью Контакт-центра по
                телефону:
            </p>
        </div>

        <? $phone = $arBackParametrs['HEADER_PHONES_array_PHONE_VALUE_0']; ?>
        <div class="main_page_promo_text_bottom_phone">
            <a class="orange_color" rel="nofollow" href="tel:+<?= preg_replace("/[^\d]+/", "", $phone); ?>">
                <?= $phone; ?>
            </a>
        </div>
    </div>

<?//убрал бренды с главной страницы!!!
//global $brands_main_page_filter;
//$brands_main_page_filter['!PREVIEW_PICTURE'] = false;
//?>
<?// $APPLICATION->IncludeComponent(
//    "bitrix:news.list",
//    "brands_main_page",
//    array(
//        "ACTIVE_DATE_FORMAT" => "d.m.Y",
//        "ADD_SECTIONS_CHAIN" => "N",
//        "AJAX_MODE" => "N",
//        "AJAX_OPTION_ADDITIONAL" => "",
//        "AJAX_OPTION_HISTORY" => "N",
//        "AJAX_OPTION_JUMP" => "N",
//        "AJAX_OPTION_STYLE" => "Y",
//        "ALL_URL" => "torgovye-marki/",
//        "CACHE_FILTER" => "Y",
//        "CACHE_GROUPS" => "N",
//        "CACHE_TIME" => "36000000",
//        "CACHE_TYPE" => "A",
//        "CHECK_DATES" => "Y",
//        "COMPOSITE_FRAME_MODE" => "A",
//        "COMPOSITE_FRAME_TYPE" => "AUTO",
//        "DETAIL_URL" => "",
//        "DISPLAY_BOTTOM_PAGER" => "N",
//        "DISPLAY_DATE" => "Y",
//        "DISPLAY_NAME" => "Y",
//        "DISPLAY_PICTURE" => "N",
//        "DISPLAY_PREVIEW_TEXT" => "N",
//        "DISPLAY_TOP_PAGER" => "N",
//        "FIELD_CODE" => array(
//            0 => "PREVIEW_PICTURE",
//            1 => "",
//        ),
//        "FILTER_NAME" => "brands_main_page_filter",
//        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
//        "IBLOCK_ID" => "118",
//        "IBLOCK_TYPE" => "catalog1c77",
//        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
//        "INCLUDE_SUBSECTIONS" => "Y",
//        "MESSAGE_404" => "",
//        "NEWS_COUNT" => "20",
//        "PAGER_BASE_LINK_ENABLE" => "N",
//        "PAGER_DESC_NUMBERING" => "N",
//        "PAGER_DESC_NUMBERING_CACHE_TIME" => "3600",
//        "PAGER_SHOW_ALL" => "N",
//        "PAGER_SHOW_ALWAYS" => "N",
//        "PAGER_TEMPLATE" => "",
//        "PAGER_TITLE" => "",
//        "PARENT_SECTION" => "",
//        "PARENT_SECTION_CODE" => "",
//        "PREVIEW_TRUNCATE_LEN" => "",
//        "PROPERTY_CODE" => array(
//            0 => "",
//            1 => "",
//        ),
//        "SET_BROWSER_TITLE" => "Y",
//        "SET_LAST_MODIFIED" => "N",
//        "SET_META_DESCRIPTION" => "Y",
//        "SET_META_KEYWORDS" => "Y",
//        "SET_STATUS_404" => "N",
//        "SET_TITLE" => "N",
//        "SHOW_404" => "N",
//        "SORT_BY1" => "SORT",
//        "SORT_BY2" => "SORT",
//        "SORT_ORDER1" => "DESC",
//        "SORT_ORDER2" => "ASC",
//        "STRICT_SECTION_CHECK" => "N",
//        "TITLE_BLOCK" => "Торговые марки",
//        "TITLE_BLOCK_ALL" => "Все торговые марки",
//        "COMPONENT_TEMPLATE" => "brands_main_page"
//    ),
//    false
//); ?>
<script type="text/javascript">

</script>