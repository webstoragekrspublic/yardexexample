<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Телефон Контакт-центра для приема заказов и поддержки клиентов: +7 (391) 290-24-00.  Почта для предложений от поставщиков уникальных и вкусных продуктов – hello@yarbox.ru. Адрес офиса и склада: г. Красноярск, Ленинский район, ул. Рязанская, 65Г");
$APPLICATION->SetPageProperty("keywords", "контакты, адрес, телефон, email, электронная почта");
$APPLICATION->SetPageProperty("title", "Контакты интернет-магазин доставки еды Ярбокс");
$APPLICATION->SetTitle("Контакты");?>

<?CNext::ShowPageType('page_contacts');?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>