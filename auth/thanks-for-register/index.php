<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Спасибо за регистрацию");
?>
<div class="register_finish_block-success">
    <div class='register_finish_block-border'>
        <div class="register_finish_block-success__img_block">
            СПАСИБО ЗА РЕГИСТРАЦИЮ!
        </div>
        <div style="width: 100%; text-align: center;">
            <? $APPLICATION->IncludeComponent(
                "custom:empty_component",
                "messengers_links",
                array(
                    "CACHE_TIME" => "360000",
                    "CACHE_TYPE" => "A"
                )
            ); ?>
        </div>
    </div>
    <div style="width: 100%; text-align: center; margin-top: 20px;">
        Что-то не нашли на нашем сайте?
    </div>
    <div style="width: 100%; text-align: center;">
        <a data-fancybox="" href="#not_found_products">Напишите в Ярдекс</a>
    </div>

</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>