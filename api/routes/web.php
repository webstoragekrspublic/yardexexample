<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api/v1/supplier', 'middleware' => ['contentTypeJson', 'login:API_ACCESS_GROUP_ID','checkSupplierCode']], function () use ($router) {

    $router->post('search/orders', ['uses' => 'OrderController@searchSupplierOrders']);

    $router->get('orders/{id}/', ['uses' => 'OrderController@showSupplierOrder']);

    $router->put('orders/{id}', ['uses' => 'OrderController@updateSupplierOrder']);

    $router->post('yml', ['uses' => 'YMLController@uploadFile']);
});

$router->group(['prefix' => 'api/v1/internal', 'middleware' => ['contentTypeJson', 'login:API_INTERNAL_ACCESS_GROUP_ID']], function () use ($router) {
    $router->post('search/products', ['uses' => 'ProductController@searchProducts']);

    $router->put('products', ['uses' => 'ProductController@updateProducts']);

    $router->post('search/suppliersOrders', ['uses' => 'OrderController@searchSupplierOrdersFor1c']);
});

$router->group(['prefix' => 'api/v1/site', 'middleware' => ['contentTypeJson']], function () use ($router) {
    $router->get('productsPrice', ['uses' => 'ProductController@productsPrice']);

    $router->get('catalog/section/{sectionId}/items', ['uses' => 'CatalogController@sectionProducts']);

    $router->get('timeslots/products', ['uses' => 'TimeSlotsController@products']);

    $router->get('timeslots/extremeSlots', ['uses' => 'TimeSlotsController@extremeSlots']);
});
