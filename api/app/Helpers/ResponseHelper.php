<?php

namespace App\Helpers;

class ResponseHelper
{
    public static function json($data, $status = 200)
    {
        return response()->json($data, $status, ['Content-Type' => 'application/json', 'Charset' => 'utf-8'], JSON_UNESCAPED_UNICODE);
    }

    public static function error($message, $code, $status = 400)
    {
        $data = [
            'error' => [
                'code' => (string)$code,
                'message' => (string)$message
            ]
        ];
        return self::json($data, $status);
    }

    public static function ok(array $result = [], array $params = [], $status = 200)
    {
        if (!$result){
            $result['success'] = true;
        }
        $answer = [];
        if ($params) {
            $answer['params'] = $params;
        }

        $answer['result'] = $result;
        return self::json($answer, $status);
    }
}
