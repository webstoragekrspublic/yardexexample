<?php

namespace App\Helpers;

use Helpers\GlobalStorage;

class UserHelper
{
    public static function getUserSupplierCode($userId)
    {
        $supplierCode = null;
        if ($userId) {
            $cacheKey = 'UserHelper-getUserSupplierCode-' . $userId;
            $supplierCode = GlobalStorage::get($cacheKey);
            if (is_null($supplierCode)) {
                $rsUser = \CUser::GetByID($userId);
                if ($userRow = $rsUser->Fetch()) {
                    $supplierCode = $userRow['UF_SUPPLIER_CODE'];
                    GlobalStorage::set($cacheKey, $supplierCode);
                }
            }
        }
        return $supplierCode;
    }
}
