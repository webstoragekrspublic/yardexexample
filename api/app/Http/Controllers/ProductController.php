<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use Helpers\Constants;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class ProductController extends Controller
{
    public function productsPrice(Request $request): \Illuminate\Http\JsonResponse
    {
        global $USER;
        $limitProducts = 50;
        $requestIDs = explode(',', $request->get('product_ids', ''));
        if (!$requestIDs) {
            return ResponseHelper::error('отсутсвует параметр product_ids', 505);
        }
        $productIDs = [];
        foreach ($requestIDs as $id) {
            $id = (int)$id;
            if ($id > 0) {
                $productIDs[] = $id;
            }
        }
        if (count($productIDs) > $limitProducts || count($productIDs) <= 0) {
            return ResponseHelper::error('количество товаров должно быть в диапазоне 0 - ' . $limitProducts, 505);
        }

        $res = [];
        try {
            $quantity = 1;
            $renewal = 'N';
            $userGroups = $USER->GetUserGroupArray();
            foreach ($productIDs as $productID) {
                $arPrice = \CCatalogProduct::GetOptimalPrice(
                    $productID,
                    $quantity,
                    $userGroups,
                    $renewal
                );
                if ($arPrice['RESULT_PRICE']) {
                    $row = $arPrice['RESULT_PRICE'];
                    unset($row['ID']);
                    $row['PRODUCT_ID'] = $productID;
                    $res[] = $row;
                }
            }
        } catch (\Exception $e) {
            return ResponseHelper::error($e->getMessage(), 505);
        }

        return ResponseHelper::ok(
            ['prices' => $res]
        );
    }

    public function searchProducts(Request $request): \Illuminate\Http\JsonResponse
    {
        $allowFilterFields = [
            'ID' => 1,
            'ACTIVE' => 1,
            'XML_ID' => 1,
            'IBLOCK_SECTION_ID' => 1
        ];
        $selectFields = [
            'ID' => 1,
            'NAME' => 1,
            'ACTIVE' => 1,
            'XML_ID' => 1,
            'IBLOCK_SECTION_ID' => 1,
            'DETAIL_TEXT' => 1,
            'DETAIL_TEXT_TYPE' => 1,
        ];
        $body = json_decode($request->getContent(), 1);
        if (json_last_error()) {
            return ResponseHelper::error('ошибка декодирования json ' . $request->getContent(), 1001);
        }
        $page = (int)$body['page'];
        $pageLimit = (int)$body['page_limit'];
        if ($page < 1) {
            $page = 1;
        }
        if ($pageLimit < 1 || $pageLimit > 1000) {
            $pageLimit = 100;
        }

        $filter = [];
        $requestFilter = $body['filter'];
        if ($requestFilter && is_array($requestFilter)) {
            foreach ($requestFilter as $key => $value) {
                if (isset($allowFilterFields[$key]) && $value) {
                    $filter[$key] = $value;
                }
            }
        }
        if (!$filter) {
            return ResponseHelper::error('нет доступных фильтров в "filter" ' . $request->getContent(), 1010);
        }

        $requestSelect = $body['select'];
        $select = [];
        if ($requestSelect && is_array($requestSelect)) {
            foreach ($requestSelect as $value) {
                if (isset($selectFields[$value]) && $value) {
                    $select[] = $value;
                }
            }
        } else {
            $select = array_keys($selectFields);
        }

        $filter['IBLOCK_ID'] = Constants::PRODUCTS_IBLOCK_ID;

        $products = array();

        $dbProducts = \CIBlockElement::GetList(
            ['ID' => 'ASC'],
            $filter,
            false,
            [
                'nPageSize' => $pageLimit,
                'iNumPage' => $page
            ],
            $select
        );

        if ((($page - 1) * $pageLimit) < $dbProducts->NavRecordCount) { //провека на превышение количества
            while ($row = $dbProducts->Fetch()) {
                $products[] = $row;
            }
        }
        return ResponseHelper::ok(
            ['products' => $products, 'total' => $dbProducts->NavRecordCount],
            ['page' => $page, 'page_limit' => $pageLimit]
        );
    }

    public function updateProducts(Request $request): \Illuminate\Http\JsonResponse
    {
        set_time_limit(300);
        $body = json_decode($request->getContent(), 1);
        if (json_last_error()) {
            return ResponseHelper::error('ошибка декодирования json ' . $request->getContent(), 1001);
        }

        $validator = \Illuminate\Support\Facades\Validator::make($body, [
            'items' => 'array|required|min:1'
        ]);
        if ($validator->fails()) {
            return ResponseHelper::error('ошибка валидации полей: ' . json_encode($validator->errors()->all()), 400);
        }
        $fields = $validator->validated();

        $updateProducts = [];
        foreach ($fields['items'] as $item) {
            $validator = \Illuminate\Support\Facades\Validator::make($item, [
                'product_id' => 'int|required|min:1',
                'props' => 'array',
                'fields' => 'array',
                'prices' => 'array',
            ]);
            if ($validator->fails()) {
                return ResponseHelper::error('ошибка валидации полей: ' . json_encode($validator->errors()->all()), 400);
            }
            $prFields = $validator->validated();

            $prFields['product_id'] = (int)$prFields['product_id'];
            if ($prFields['props']) {
                $validator = \Illuminate\Support\Facades\Validator::make($prFields['props'], [
                    'ACTIVE' => 'in:Y,N|string'
                ]);
                if ($validator->fails()) {
                    return ResponseHelper::error('ошибка валидации полей: ' . json_encode($validator->errors()->all()), 400);
                }
                $prFields['props'] = $validator->validated();
            }
            if ($prFields['fields']) {
                $validator = \Illuminate\Support\Facades\Validator::make($prFields['fields'], [
                    'QUANTITY' => 'numeric|min:0'
                ]);
                if ($validator->fails()) {
                    return ResponseHelper::error('ошибка валидации полей: ' . json_encode($validator->errors()->all()), 400);
                }
                $prFields['fields'] = $validator->validated();
            }
            if ($prFields['prices']) {
                $validator = \Illuminate\Support\Facades\Validator::make($prFields['prices'], [
                    'PRICE' => 'numeric|min:0',
                    'PURCHASING_PRICE' => 'numeric|min:0',
                ]);
                if ($validator->fails()) {
                    return ResponseHelper::error('ошибка валидации полей: ' . json_encode($validator->errors()->all()), 400);
                }

                $prFields['prices'] = $validator->validated();
            }
            $updateProducts[] = $prFields;
        }
        if (count($updateProducts) > 1000) {
            return ResponseHelper::error('максиамльное количество items = 1000', 400);
        }

        foreach ($updateProducts as $updateProduct) {
            $id = $updateProduct['product_id'];
            if ($updateProduct['fields']) {
                $res = \Bitrix\Catalog\Model\Product::update($id, $updateProduct['fields']);
                if (!$res) {
                    return ResponseHelper::error('ошибка обновления fields! ' . $id . print_r($updateProduct['fields'], 1), 500);
                }
            }

            if ($updateProduct['props']) {
                $el = new \CIBlockElement;
                $res = $el->update($id, $updateProduct['props']);
                if (!$res) {
                    return ResponseHelper::error('ошибка обновления props! ' . $id . print_r($updateProduct['props'], 1), 500);
                }
            }

            if ($updateProduct['prices']) {
                if (isset($updateProduct['prices']['PURCHASING_PRICE'])) {
                    $price = trim($updateProduct['prices']['PURCHASING_PRICE']);
                    $arFieldsPurchasing = array(
                        'ID' => $id,
                        'PURCHASING_PRICE' => $price > 0 ? $price : '',
                        'PURCHASING_CURRENCY' => "RUB"
                    );
                    $CCatalogProduct = new \CCatalogProduct();
                    $res = $CCatalogProduct->add($arFieldsPurchasing);
                    if (!$res) {
                        return ResponseHelper::error('ошибка обновления PURCHASING_PRICE! ' . $id . print_r($updateProduct['prices'], 1), 500);
                    }
                }
                if (isset($updateProduct['prices']['PRICE'])) {
                    $price = trim($updateProduct['prices']['PRICE']);
                    $res = \CPrice::SetBasePrice($id, (float)$price, 'RUB');
                    if (!$res) {
                        return ResponseHelper::error('ошибка обновления PRICE! ' . $id . print_r($updateProduct['prices'], 1), 500);
                    }
                }
            }
        }


        return ResponseHelper::ok();
    }

}
