<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use Custom\DeliveryTime\DeliveryDateAvailable;
use Custom\DeliveryTime\DeliveryTime;
use Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition;
use Handlers\ExternalSuppliers\ExternalSuppliersOrderTable;
use Helpers\Constants;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class TimeSlotsController extends Controller
{
    public function products(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'product_ids' => 'string|required|min:1',
        ]);

        if ($validator->fails()) {
            return ResponseHelper::error('ошибка валидации полей: ' . json_encode($validator->errors()->all()), 400);
        }
        $queryParams = $validator->validated();
        $productQueryArr = explode(',', $queryParams['product_ids']);
        $productsId = [];
        foreach ($productQueryArr as $id) {
            $id = (int)$id;
            if ($id > 0) {
                $productsId[] = $id;
            }
        }
        if (!$productsId) {
            return ResponseHelper::error('не переданы товары в запрос', 400);
        }
        if (count($productsId) > 50) {
            return ResponseHelper::error('превышен лимит товаров ', 400);
        }

        $shortlyWeekDay = DeliveryTime::getInstance()->getShortlyWeekDay();

//        pr($shortlyWeekDay);

        $shortlyProductsDay = DeliveryTime::getInstance()->getShortlyDayInfoForProductIds($productsId);

        $resProdInfo = [];
        foreach ($shortlyProductsDay as $productID => $day) {
            $fastDelivery = '0';
            if ($day == $shortlyWeekDay) {
                $fastDelivery = '1';
            }
            $resProdInfo[] = [
                'PRODUCT_ID' => $productID,
                'CLOSEST_TIME_SLOT' => DeliveryTime::getInstance()->getFirstNotDisabledTimeSlot($day['timeSlots']),
                'CLOSEST_DELIVERY_DAY' => $day,
                'FAST_DELIVERY' => $fastDelivery
            ];
        }

        $requestResult = [];
        $requestResult['closestDay'] = [
            'dayInfo' => $shortlyWeekDay,
            'timeSlot' => DeliveryTime::getInstance()->getFirstNotDisabledTimeSlot($shortlyWeekDay['timeSlots'])
        ];
        $requestResult['closestProductsDay'] = $resProdInfo;

        return ResponseHelper::ok(
            $requestResult
        );
    }

    public function extremeSlots(Request $request): \Illuminate\Http\JsonResponse
    {
        $nearestDeliveryDay = \Custom\DeliveryTime\DeliveryTime::getInstance()->getShortlyWeekDay();
        $nearestDeliveryTimeSlot = \Custom\DeliveryTime\DeliveryTime::getInstance()->getFirstNotDisabledTimeSlot($nearestDeliveryDay['timeSlots']);

        $nearestAllProductsDeliveryDay = \Custom\DeliveryTime\DeliveryTime::getInstance()->getShortlyWeekDayWithAllConditions();
        $nearestAllProductsDeliveryTimeSlot = \Custom\DeliveryTime\DeliveryTime::getInstance()->getFirstNotDisabledTimeSlot($nearestAllProductsDeliveryDay['timeSlots']);
        $requestResult = [
            'nearestDelivery' => [
                'dayInfo' => $nearestDeliveryDay,
                'timeSlot' => $nearestDeliveryTimeSlot
            ],
            'nearestDeliveryForLongestSuppliers' => [
                'dayInfo' => $nearestAllProductsDeliveryDay,
                'timeSlot' => $nearestAllProductsDeliveryTimeSlot
            ],
        ];
        return ResponseHelper::ok(
            $requestResult
        );
    }
}
