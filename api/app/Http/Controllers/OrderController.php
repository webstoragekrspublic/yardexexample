<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Helpers\UserHelper;
use Handlers\ExternalSuppliers\ExternalSupplierBasketProductDTO;
use Handlers\ExternalSuppliers\ExternalSuppliers;
use Handlers\ExternalSuppliers\ExternalSuppliersOrderTable;
use Helpers\Constants;
use Helpers\CustomTools;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function searchSupplierOrders(Request $request)
    {
        global $USER;
        $body = json_decode($request->getContent(), 1);
        if (json_last_error()) {
            return ResponseHelper::error('ошибка декодирования json ' . request()->getContent(), 1001);
        }
        $page = (int)$body['page'];
        $pageLimit = (int)$body['page_limit'];
        $statusNames = $body['status'];
        if ($statusNames && !is_array($statusNames)) {
            return ResponseHelper::error('поле status должно быть массивом ', 4021);
        }

        if ($page < 1) {
            $page = 1;
        }
        if ($pageLimit < 1 || $pageLimit > 100) {
            $pageLimit = 20;
        }

        $orders = [];
        $arFilter = [];
        $total = 0;


        $filter = [];
        if ($statusNames) {
            $filter['STATUS_ID'] = [];
            foreach ($statusNames as $name) {
                $statusId = ExternalSuppliersOrderTable::getStatusCodeByName($name);
                if (!$statusId) {
                    return ResponseHelper::error('неверный status = ' . $name, 4021);
                }
                $filter['STATUS_ID'][] = $statusId;
            }
        }

        $supplierOrders = ExternalSuppliers::getOrdersForSupplier(UserHelper::getUserSupplierCode($USER->GetID()), $filter);
        if ($supplierOrders) {
            $supplierOrders = CustomTools::indexArrayByKey($supplierOrders, 'ID_ORDER');
            $arFilter['ID'] = array_keys($supplierOrders);

            $dbOrders = \CSaleOrder::GetList(
                ["ID" => "DESC"],
                $arFilter,
                false,
                [
                    'nPageSize' => $pageLimit,
                    'iNumPage' => $page
                ],
                ['ID', 'DATE_INSERT', 'DATE_UPDATE', 'STATUS_ID']
            );
            if ((($page - 1) * $pageLimit) < $dbOrders->NavRecordCount) { //проверка на превышение количества
                while ($row = $dbOrders->Fetch()) {
                    $statusCode = $supplierOrders[$row['ID']]['STATUS_ID'];
                    $orders[] = [
                        'id' => $row['ID'],
                        'date_create' => $row['DATE_INSERT'],
                        'status' => ExternalSuppliersOrderTable::getStatusNameByCode($statusCode),
                    ];
                }
            }
            $total = $dbOrders->NavRecordCount;
        }

        return ResponseHelper::ok(
            ['orders' => $orders, 'total' => $total],
            ['page' => $page, 'page_limit' => $pageLimit]
        );
    }

    public function searchSupplierOrdersFor1c(Request $request)
    {
        global $USER;

        $filtersRules = [
            'status' => [
                'validate' => 'array',
                'filterName' => 'STATUS_ID'
            ],
            'supplier_code' => [
                'validate' => 'array',
                'filterName' => 'SUPPLIER_CODE'
            ],
            'id' => [
                'validate' => 'array',
                'filterName' => 'ID'
            ],
            '>id' => [
                'validate' => 'int',
                'filterName' => '>ID'
            ],
            '>=id' => [
                'validate' => 'int',
                'filterName' => '>=ID'
            ],
            '<id' => [
                'validate' => 'int',
                'filterName' => '<ID'
            ],
            '<=id' => [
                'validate' => 'int',
                'filterName' => '<ID'
            ],
            'id_order' => [
                'validate' => 'array',
                'filterName' => 'ID_ORDER'
            ],
            '>id_order' => [
                'validate' => 'int',
                'filterName' => '>ID_ORDER'
            ],
            '>=id_order' => [
                'validate' => 'int',
                'filterName' => '>=ID_ORDER'
            ],
            '<id_order' => [
                'validate' => 'int',
                'filterName' => '<ID_ORDER'
            ],
            '<=id_order' => [
                'validate' => 'int',
                'filterName' => '<=ID_ORDER'
            ],
        ];

        $body = json_decode($request->getContent(), 1);
        if (json_last_error()) {
            return ResponseHelper::error('ошибка декодирования json ' . request()->getContent(), 1001);
        }
        $page = (int)$body['page'];
        $pageLimit = (int)$body['page_limit'];

        if ($page < 1) {
            $page = 1;
        }
        if ($pageLimit < 1 || $pageLimit > 1000) {
            $pageLimit = 100;
        }

        $orders = [];
        $filter = [];


        $bodyFilter = $body['filter'];
        if ($bodyFilter && is_array($bodyFilter)) {
            $errorValidateFilters = $this->errorFilterValues($filtersRules, $bodyFilter);
            if ($errorValidateFilters) {
                return ResponseHelper::error($errorValidateFilters, 4021);
            }
            $filter = $this->collectFilter($filtersRules, $bodyFilter);

            //особые правила создания фильтра для STATUS_ID
            $statusNames = $bodyFilter['status'];
            if ($statusNames) {
                $filter['STATUS_ID'] = [];
                foreach ($statusNames as $name) {
                    $statusId = ExternalSuppliersOrderTable::getStatusCodeByName($name);
                    if (!$statusId) {
                        return ResponseHelper::error('неверный status = ' . $name, 4021);
                    }
                    $filter['STATUS_ID'][] = $statusId;
                }
            }

        }


        $params = [
            'select' => ['*', 'ORDER.DATE_INSERT'],
            'order' => ['ID' => 'DESC'],
            'filter' => $filter,
            'limit' => $pageLimit,
            'offset' => ($page - 1) * $pageLimit
        ];
        $qRes = ExternalSuppliers::getSupplierOrderByParametersWithCount($params);
        $supplierOrders = $qRes['result'];
//        pr($supplierOrders);
//        exit;
        $total = $qRes['total'];
        if ($supplierOrders) {

            $productsInfo = [];
            foreach ($supplierOrders as $supplierOrder) {
                /* @var $supProductDTO ExternalSupplierBasketProductDTO */
                foreach ($supplierOrder['PRODUCTS_INFO'] as $supProductDTO) {
                    $prId = $supProductDTO->getIdProduct();
                    $productsInfo[$prId] = [];
                }
            }
            if ($productsInfo) {
                $productsInfoQ = \CIBlockElement::GetList(
                    [],
                    ['IBLOCK_ID' => Constants::PRODUCTS_IBLOCK_ID, 'ID' => array_keys($productsInfo)],
                    false,
                    false,
                    ['ID', 'PROPERTY_SUPPLIER_ID', 'PROPERTY_SUPPLIER_GOODS_ID', 'XML_ID']
                );
                while ($row = $productsInfoQ->fetch()) {
                    $prId = $row['ID'];
                    $productsInfo[$prId]['supplierArticle'] = $row['PROPERTY_SUPPLIER_GOODS_ID_VALUE'];
                    $productsInfo[$prId]['xmlId'] = $row['XML_ID'];
                }
            }

            foreach ($supplierOrders as $supplierOrder) {
                $statusCode = $supplierOrder['STATUS_ID'];
                $products = [];

                /* @var $supProductDTO ExternalSupplierBasketProductDTO */
                foreach ($supplierOrder['PRODUCTS_INFO'] as $supProductDTO) {
                    $prId = $supProductDTO->getIdProduct();
                    $prRow = $supProductDTO->toExternal();
                    $prRow += $productsInfo[$prId];
                    $products[] = $prRow;
                }
                $orders[] = [
                    'id' => $supplierOrder['ID'],
                    'date_create' => $supplierOrder['HANDLERS_EXTERNALSUPPLIERS_EXTERNAL_SUPPLIERS_ORDER_ORDER_DATE_INSERT']->toString(),
                    'id_order' => $supplierOrder['ID_ORDER'],
                    'supplier_code' => $supplierOrder['SUPPLIER_CODE'],
                    'status' => ExternalSuppliersOrderTable::getStatusNameByCode($statusCode),
                    'products' => $products
                ];
            }

        }

        return ResponseHelper::ok(
            ['orders' => $orders, 'total' => $total],
            ['page' => $page, 'page_limit' => $pageLimit]
        );
    }

    public function showSupplierOrder($id)
    {
        global $USER;
        $id = (int)$id;

        $orderSupplier = ExternalSuppliers::getSupplierOrder($id, UserHelper::getUserSupplierCode($USER->GetID()));
        if (!$orderSupplier) {
            return ResponseHelper::error('не найден заказ с id = ' . $id, 4001);
        }
        $statusCode = $orderSupplier['STATUS_ID'];

        $order = \Bitrix\Sale\Order::load($id);

        if (!$order) {
            return ResponseHelper::error('заказ был удален и больше не действителен ' . $id, 555);
        }

        $answerProducts = [];
        if ($orderSupplier['PRODUCTS_INFO']) {
            /* @var $supProductDTO ExternalSupplierBasketProductDTO */
            foreach ($orderSupplier['PRODUCTS_INFO'] as $supProductDTO) {
                $prId = $supProductDTO->getIdProduct();
                $answerProducts[$prId] = $supProductDTO->toExternal(['quantity']);
            }
        }
        if ($answerProducts) {
            $productsInfoQ = \CIBlockElement::GetList(
                [],
                ['IBLOCK_ID' => Constants::PRODUCTS_IBLOCK_ID, 'ID' => array_keys($answerProducts)],
                false,
                false,
                ['ID', 'PROPERTY_SUPPLIER_ID', 'PROPERTY_SUPPLIER_GOODS_ID']
            );
            while ($row = $productsInfoQ->fetch()) {
                $prId = $row['ID'];
                $answerProducts[$prId]['supplierArticle'] = $row['PROPERTY_SUPPLIER_GOODS_ID_VALUE'];
            }
        }

        $resAnswerProducts = [];
        foreach ($answerProducts as $answerProduct) {
            $resAnswerProducts[] = $answerProduct;
        }

        $answer = ['order' => [
            'id' => $order->getField('ID'),
            'date_create' => $order->getField('DATE_INSERT')->toString(),
            'status' => ExternalSuppliersOrderTable::getStatusNameByCode($statusCode),
            'products' => $resAnswerProducts
        ]];

        return ResponseHelper::ok(
            $answer
        );
    }

    public function updateSupplierOrder($id)
    {
        global $USER;
        $id = (int)$id;
        $supplierCode = UserHelper::getUserSupplierCode($USER->GetID());

        $orderSupplier = ExternalSuppliers::getSupplierOrder($id, $supplierCode);
        if (!$orderSupplier) {
            return ResponseHelper::error('не найден заказ с id = ' . $id, 4001);
        }

        $order = \Bitrix\Sale\Order::load((int)$id);
        if (!$order) {
            return ResponseHelper::error('не найден заказ с id = ' . $id, 555);
        }
        $body = json_decode(request()->getContent(), 1);
        if (json_last_error()) {
            return ResponseHelper::error('ошибка декодирования json ' . request()->getContent(), 1001);
        }
        if (!$this->isValidForUpdate($orderSupplier)) {
            return ResponseHelper::error('Нельзя изменять заказ в статусе ' . ExternalSuppliersOrderTable::getStatusNameByCode($orderSupplier['STATUS_ID']), 4100);
        }


        $bodyProducts = $body['products'];
        $bodyNewStatusName = $body['status'];
        if (!$bodyProducts) {
            $bodyProducts = [];
        }

        $newStatusId = false;
        if ($bodyNewStatusName) {
            $newStatusId = ExternalSuppliersOrderTable::getStatusCodeByName($bodyNewStatusName);
            if (!$newStatusId) {
                return ResponseHelper::error('неверный status = ' . $bodyNewStatusName, 4021);
            }
        }
        writeAppendLog([
            'ContinentPutMetodQuantity_'.$id.'_01' => [
                'order_id' => $id,
                'status_before_query_chack' => 'current',
                'status_current' => ExternalSuppliersOrderTable::getStatusNameByCode($newStatusId),
            ]
        ]);

        if (!$bodyProducts && !$bodyNewStatusName) {
            return ResponseHelper::error('должны присутствовать в запросе параметры "products" или "status"  ' . request()->getContent(), 1002);
        }

        if ($bodyProducts && !is_array($bodyProducts)) {
            return ResponseHelper::error('параметр "products" должен быть массивом: ' . request()->getContent(), 1002);
        }

        if ($bodyProducts) {
            foreach ($bodyProducts as $bodyProduct) {
                if (!isset($bodyProduct['supplierArticle'], $bodyProduct['quantity'])) {
                    return ResponseHelper::error('в "products" обязательно должны быть поля "supplierArticle" и "quantity" ' . request()->getContent(), 1002);
                }
            }
        }

        $supplierProducts = [];

        /* @var $supProductDTO ExternalSupplierBasketProductDTO */
        foreach ($orderSupplier['PRODUCTS_INFO'] as $supProductDTO) {
            $prId = $supProductDTO->getIdProduct();
            $supplierProducts[$prId] = $supProductDTO;
        }

        if ($bodyProducts) {
            $setGetQuantity = [];
            $this->enrichBodyProductsWithID($bodyProducts, $supplierCode);

            foreach ($bodyProducts as $key => $bodyProduct) {
                $prId = $bodyProduct['id'];
                if (!isset($supplierProducts[$prId])) {
                    return ResponseHelper::error('в заказе отстуствует товар с supplierArticle = ' . $bodyProduct['supplierArticle'], 1010);
                }

                if ($prId < 1 || $bodyProduct['quantity'] < 0 || $bodyProduct['quantity'] > $supplierProducts[$prId]->getQuantity()) {
                    return ResponseHelper::error('параметры "products" должны быть id > 0 quantity >= 0 а quantity не превышать текущее количество в заказе (' . $supplierProducts[$prId]->getQuantity() . ')  ' . request()->getContent(), 1002);
                }
                $supplierProducts[$prId]->setQuantity($bodyProduct['quantity']);
                $setGetQuantity[$prId]['set'] = $bodyProduct['quantity'];
                $setGetQuantity[$prId]['get'] = $supplierProducts[$prId]->getQuantity();
            }
        }

        writeAppendLog([
            'ContinentPutMetodQuantity_'.$id.'_01_001' => [
                'order_id' => $id,
                'bodyProducts' => $bodyProducts,
                'setGetQuantity' => $setGetQuantity,
            ]
        ]);

        if ($bodyProducts) {
            $productsQuantitySum = 0; // общее количество единиц товара в заказе
            $productsInfo = []; // информация о товарах из которые обновляються континентом
            $productRequestCount = count($bodyProducts); // кол-во товаров которые переданы в запросе на обновление
            $supplierProductsCount = count($supplierProducts); // всего кол-во товаров в заказе
            $idsRequestProduct = []; // id товаров переданных в запросе на обновление
            // цикл находит количество единиц товаров, которые нужно обновить
            foreach ($supplierProducts as $supplierProductItem) {
                $prId = $supplierProductItem->getIdProduct(); // id товаров из заказа (базы данных)
                // цикл обхода товаров из запроса обновления, на суммирование их кол-ва
                foreach ($bodyProducts as $bodyProduct) {
                    $flagSearch = false;
                    // условие: если id товара из запроса, совпадает с id товара Заказа из базы данных
                    // то: сохраняеться его id
                    // ставиться флаг для того чтобы суммировать quantity этого товара из запроса к другим из запроса
                    if ($bodyProduct['id'] == $prId) {
                        $idsRequestProduct[] = $bodyProduct['id']; // добавляеться id товара из запроса
                        $flagSearch = true; // для того чтобы суммировать quantity текущего товара с остальными
                        $quantityProdFloat = (float)$bodyProduct['quantity']; // выборка quntity из запроса
                    }
                }
                // условие если единиц товара больше нуля, то складываем с остальными
                if ($quantityProdFloat > 0 && $flagSearch) {
                    $productsQuantitySum += $quantityProdFloat; // сложение единиц товара
                }
                if ($flagSearch) {
                    // информация о товаре из запроса
                    $productsInfo['bodyProducts'][$prId]['IdProduct'] = $supplierProductItem->getIdProduct();
                    $productsInfo['bodyProducts'][$prId]['quantity'] = $supplierProductItem->getQuantity();
                    $productsInfo['bodyProducts'][$prId]['Price'] = $supplierProductItem->getPrice();
                }
            }
            // условие на проверку: если кол-во товаров в запросе меньше дейстивительных из Заказа в Базе Данных
            if ($productRequestCount < $supplierProductsCount) {
                // цикл обработки товаров из Базы Данных Заказа, которых нет в запросе
                foreach ($supplierProducts as $supplierProductItem) {
                    // условие если товара нет в запросе, в массиве обработанных запросов
                    if (!in_array($supplierProductItem->getIdProduct(),$idsRequestProduct)) {
                        $quantityProdFloatBd = (float)$supplierProductItem->getQuantity();
                        if ($quantityProdFloatBd > 0) {
                            $productsQuantitySum += $quantityProdFloatBd;
                        }
                        $productsInfo['supplierProducts'][$prId]['IdProduct'] = $supplierProductItem->getIdProduct();
                        $productsInfo['supplierProducts'][$prId]['quantity'] = $supplierProductItem->getQuantity();
                        $productsInfo['supplierProducts'][$prId]['Price'] = $supplierProductItem->getPrice();
                    }
                }
            }
            if ($productsQuantitySum <= 0) {
                $newStatusId = ExternalSuppliersOrderTable::getStatusCodeByName('crossed_out');
                writeAppendLog([
                    'ContinentPutMetodQuantity_'.$id.'_02_1' => [
                        'order_id' => $id,
                        'status_ready' => 'crossed_out',
                        'status_current' => ExternalSuppliersOrderTable::getStatusNameByCode($newStatusId),
                        'productsQuantitySum' => $productsQuantitySum,
                        'supplierProductsInfo' => $productsInfo,
                    ]
                ]);
            } else {
                writeAppendLog([
                    'ContinentPutMetodQuantity_'.$id.'_02_2' => [
                        'order_id' => $id,
                        'status_ready' => 'other_status',
                        'status_current' => ExternalSuppliersOrderTable::getStatusNameByCode($newStatusId),
                        'productsQuantitySum' => $productsQuantitySum,
                        'supplierProductsInfo' => $productsInfo,
                    ]
                ]);
            }
        }

        try {
            ExternalSuppliers::UpdateOrderProducts($orderSupplier['ID'], $supplierProducts);
        } catch (\Exception $e) {
            return ResponseHelper::error('ошибка ' . $e->getMessage(), 1100, 500);
        }
        if ($newStatusId != false) {
            ExternalSuppliers::updateStatusId($id, $supplierCode, $newStatusId);
        }

        return ResponseHelper::ok();
    }

    public function isValidForUpdate($orderSupplier): bool
    {
        $statusDontChange = ExternalSuppliersOrderTable::STATUS_COMPLETED;
        if ($orderSupplier['STATUS_ID'] == $statusDontChange) {
            return false;
        }
        return true;
    }

    protected function errorFilterValues(array $filterRules, array $values): ?string
    {
        foreach ($values as $fieldName => $val) {
            if (!isset($filterRules[$fieldName])) {
                return "поле $fieldName не доступно для фильтрации";
            }

            $validateRule = $filterRules[$fieldName]['validate'];
            if ($validateRule) {
                switch ($validateRule) {
                    case 'array':
                        if (!is_array($val) || count($val) == 0) {
                            return "поле $fieldName должно быть не пустым массивом";
                        }
                        break;
                    case 'int':
                        if (!is_int($val)) {
                            return "поле $fieldName должно быть целым числом";
                        }
                        break;
                    default:
                        return "поле $fieldName ($validateRule) не может быть обработано, обратитесь к администратору.";
                }
            }
        }
        return null;
    }

    protected function collectFilter(array $filterRules, array $values): array
    {
        $res = [];
        if ($filterRules && $values) {
            foreach ($values as $fieldName => $val) {
                if (isset($filterRules[$fieldName]['filterName'])) {
                    $res[$filterRules[$fieldName]['filterName']] = $val;
                }
            }
        }
        return $res;
    }


    protected function enrichBodyProductsWithID(&$bodyProducts, $supplierCode)
    {
        $supplierArticles = [];
        foreach ($bodyProducts as $pr) {
            if (!empty(trim($pr['supplierArticle']))) {
                $supplierArticles[] = $pr['supplierArticle'];
            }
        }
        if ($supplierArticles) {
            $productsInfoQ = \CIBlockElement::GetList(
                [],
                [
                    'IBLOCK_ID' => Constants::PRODUCTS_IBLOCK_ID,
                    'PROPERTY_SUPPLIER_GOODS_ID' => $supplierArticles,
                    'PROPERTY_SUPPLIER_ID' => $supplierCode
                ],
                false,
                false,
                ['ID', 'PROPERTY_SUPPLIER_ID', 'PROPERTY_SUPPLIER_GOODS_ID', 'XML_ID']
            );
            $resInfo = [];
            while ($row = $productsInfoQ->fetch()) {
                $resInfo[$row['PROPERTY_SUPPLIER_GOODS_ID_VALUE']] = $row;
            }
            foreach ($bodyProducts as $key => $value) {
                if (isset($resInfo[$value['supplierArticle']])) {
                    $bodyProducts[$key]['id'] = $resInfo[$value['supplierArticle']]['ID'];
                }
            }
        }
    }
}
