<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Helpers\UserHelper;
use Illuminate\Http\Request;

class YMLController extends Controller
{
    protected $uploadDir;

    public function __construct()
    {
        $this->uploadDir = $_SERVER["DOCUMENT_ROOT"] . '/api/upload/supplierYml';
    }

    public function uploadFile()
    {
        $body = json_decode(request()->getContent(), 1);
        if (json_last_error()) {
            return ResponseHelper::error('ошибка декодирования json ' . request()->getContent(), 1001);
        }
        if (empty($body['base64_content'])) {
            return ResponseHelper::error('параметр "base64_content" пустой: ' . request()->getContent(), 1002);
        }

        $ymlString = base64_decode($body['base64_content']);
        if ($ymlString === false) {
            return ResponseHelper::error('ошибка декодирования "base64_content" ' . request()->getContent(), 1003);
        }
        try {
            new \SimpleXMLElement($ymlString);
        } catch (\Exception $exception) {
            return ResponseHelper::error('ошибка парсинга xml документа: ' . $exception->getMessage(), 1010);
        }

        $fileName = UserHelper::getUserSupplierCode(\CUser::GetID()) . '.xml';
        $ymlFilePath = $this->uploadDir . '/' . $fileName;
        $ok = file_put_contents($ymlFilePath, $ymlString);
        if ($ok === false) {
            return ResponseHelper::error('ошибка создания файла', 1020, 500);
        }

        return ResponseHelper::ok();
    }

}
