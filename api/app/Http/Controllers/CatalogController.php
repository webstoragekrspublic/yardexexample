<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use Handlers\ExternalSuppliers\ExternalSuppliersOrderTable;
use Helpers\Constants;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class CatalogController extends Controller
{
    public function sectionProducts($sectionId, Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'page_num' => 'numeric|min:1',
            'page_size' => 'numeric|min:1|max:40',
            'sort_field' => 'in:NAME,PROPERTY_TOTAL_PRICE_ALL,PROPERTY_DISCOUNT_AMOUNT_PERCENT',
            'sort_order' => 'in:DESC,ASC',
            'select_props' => 'string',
            'filter' => 'json'
        ]);

        if ($validator->fails()) {
            return ResponseHelper::error('ошибка валидации полей: ' . json_encode($validator->errors()->all()), 400);
        }
        $queryParams = $validator->validated();

        $page = (int)($queryParams['page_num'] ?? 1);
        $pageSize = (int)($queryParams['page_size'] ?? 6);
        $sortField = ($queryParams['sort_field'] ?? 'NAME');
        $sortOrder = ($queryParams['sort_order'] ?? 'ASC');
        $selectProps = ($queryParams['select_props'] ?? '');
        $queryFilter = [];
        $queryProps = $this->parseQuerySelectProps($selectProps);
        if ($queryParams['filter']) {
            $queryFilter = $this->parseQueryFilters(json_decode($queryParams['filter'], 1));
        }

        $arSelect = ["ID", "ACTIVE", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID"];
        $arSelect = array_merge($arSelect, $queryProps);
        $arFilter = ["IBLOCK_ID" => Constants::PRODUCTS_IBLOCK_ID, "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE" => "Y", 'SECTION_ID' => $sectionId];
        $arFilter = array_merge($arFilter, $queryFilter);

        $q = \CIBlockElement::GetList([
            $sortField => $sortOrder
        ],
            $arFilter,
            false,
            ["nPageSize" => $pageSize, "iNumPage" => $page],
            $arSelect
        );

        $arItems = [];
        if ((($page - 1) * $pageSize) < $q->NavRecordCount) { //проверка на превышение количества
            while ($ob = $q->GetNextElement()) {
                $arFields = $ob->GetFields();


                $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
                $arItem = [
                    'ID' => $arFields['ID'],
                    'ACTIVE' => $arFields['ACTIVE'],
                    'NAME' => $arFields['NAME'],
                    'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
                    'PREVIEW_PICTURE_SRC' => \CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
                    'PRICE' => \CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
                    'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
                    'RATIO' => $measure['RATIO'],
                ];

                $productProps = [];
                foreach ($arFields as $fieldKey => $propValue) {
                    if (
                        substr($fieldKey, 0, 9) === "PROPERTY_" &&
                        substr($fieldKey, -6) === "_VALUE"
                    ) {
                        $propCode = substr($fieldKey, 9, strlen($fieldKey) - 15);
                        $rawPropKey = '~' . $fieldKey;
                        $productProps[$propCode]['VALUE'] = $propValue;
                        $productProps[$propCode]['~VALUE'] = $arFields[$rawPropKey];
                    }
                }
                $arItem['PROPS'] = $productProps;
                $arItems[] = $arItem;
            }
        }
        $requestResult = [];
        $requestResult['CUR_PAGE'] = $q->NavPageNomer;
        $requestResult['PAGE_COUNT'] = $q->NavPageCount;
        $requestResult['SORT_FIELDS'] = \Helpers\Events::getCatalogAllowSortFields();
        $requestResult['ITEMS'] = $arItems;

        return ResponseHelper::ok(
            $requestResult
        );
    }

    protected function parseQueryFilters(array $filterArr): array
    {
        $validFilterOP = ["=", "!=", ">", "<", ">=", "<="];
        $resFilters = [];
        if (!$filterArr) {
            return $resFilters;
        }

        foreach ($filterArr as $filterBy => $vArr) {
            if (preg_match('/^PROPERTY_*/', $filterBy) &&
                in_array($vArr['OP'], $validFilterOP) &&
                isset($vArr['V'])) {
                $resFilters[$vArr['OP'] . $filterBy] = $vArr['V'];
            }
        }

        return $resFilters;
    }

    protected function parseQuerySelectProps(string $querySelectProps): array
    {
        $validPropsCodes = [
            'FAST_DELIVERY' => true,
            'INGREDIENTS' => true,
            'PROTEINS' => true,
            'FAT' => true,
            'CARBOHYDRATES' => true,
            'DATE_EXPIRATION' => true,
            'TOTAL_PRICE_ALL' => true,
            'DISCOUNT_AMOUNT_PERCENT' => true,
            'ENERGY' => true,
            'SHELF_LIFE' => true,
            'WEIGHT_UNIT' => true,
            'WEIGHT_NET' => true,
            'COMMENTS_COUNT' => true,
            'COMMENTS_AVERAGE_SCORE' => true,
            'NEW' => true,
            'BESTSELLER' => true,

        ];
        $resSelectProps = [];

        if (!$querySelectProps) {
            return $resSelectProps;
        }

        if ($querySelectProps !== '*') {
            $props = explode(',', $querySelectProps);
        } else {
            $props = array_keys($validPropsCodes);
        }
        foreach ($props as $propCode) {
            if (isset($validPropsCodes[$propCode])) {
                $resSelectProps[] = 'PROPERTY_' . $propCode;
            }
        }

        return $resSelectProps;
    }
}
