<?php

namespace App\Http\Middleware;

use App\Helpers\ResponseHelper;
use Closure;

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $checkGroup)
    {
        global $USER;
        if ($checkGroup) {
            $groupAccessId = (int)env($checkGroup);
            if (!$groupAccessId) {
                return ResponseHelper::error('нет доступа к переменной окружения groupAccessId', 401, 500);
            }
        }
        $clientLogin = $request->headers->get('Client-Id');
        $clientPass = $request->headers->get('Api-Key');
        $result = $USER->Login($clientLogin, $clientPass, "N", 'Y');

        if ($result !== true) {
            return ResponseHelper::error('Ошибка авторизации', 401, 401);
        } else {
            $result = \CSite::InGroup([$groupAccessId]);
            if ($result !== true) {
                return ResponseHelper::error('Нет доступа', 403, 403);
            }
        }
        return $next($request);
    }
}
