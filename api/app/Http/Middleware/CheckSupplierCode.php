<?php

namespace App\Http\Middleware;

use App\Helpers\ResponseHelper;
use App\Helpers\UserHelper;
use Closure;

class CheckSupplierCode
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        global $USER;
        $supplierCode = UserHelper::getUserSupplierCode($USER->GetID());

        if (!$supplierCode) {
            return ResponseHelper::error('Не задан код поставщика. Обратитесь, пожалуйста, к администратору.', 405, 403);
        }
        return $next($request);
    }
}
