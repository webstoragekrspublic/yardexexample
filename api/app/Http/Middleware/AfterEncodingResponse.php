<?php

namespace App\Http\Middleware;

use App\Helpers\ResponseHelper;
use Closure;

class AfterEncodingResponse
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //заготовка для кодирования ответа в windows-1251 для 1с7, если потребуется.
//        $response = $next($request);
//        $encodeTo = trim($request->get('encoding'));
//        if ($encodeTo) {
//            $data = $response->getOriginalContent();
//            $encodedData = mb_convert_encoding($data, $encodeTo, "UTF-8");
//            if ($encodedData !== false) {
//                $response->setContent(mb_convert_encoding('тест', $encodeTo, "UTF-8"));
//            } else {
//                return ResponseHelper::error('ошибка преобразования в кодировку = ' . $encodeTo, 5100);
//            }
//        }
//
//        return $response;
        return $next($request);
    }
}
