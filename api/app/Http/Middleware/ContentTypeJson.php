<?php

namespace App\Http\Middleware;

use App\Helpers\ResponseHelper;
use Closure;

class ContentTypeJson
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->method() !== 'GET') {
            $contentType = $request->headers->get('Content-Type');
            if ($contentType !== 'application/json') {
                return ResponseHelper::error('Ошибка Content-Type (должен быть application/json)', 100, 400);
            }
        }
        return $next($request);
    }
}
