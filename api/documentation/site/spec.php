<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $USER;
if (!\Helpers\SafetyValidation::isAdminOrManger(\CUser::GetID())){
    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
    echo '<h3>Необходимо авторизоваться</h3>';
    die();
}
echo file_get_contents(__DIR__.'/'.'spec_site448c1aab6ff48b686730db119170b3f5.yaml');
