<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$calledUsersFilePath = __DIR__ . '/calledUsers.txt';
$minOrdersForUsers = 5;
$validStatuses = ["F", 'IS', 'P', 'R'];
$minUserOrderDateTS = time() - 5 * 24 * 3600; //последний заказ не ранее 5 дней
$maxUserOrderDateTS = time() - 30 * 3 * 24 * 3600; //последний заказ не позднее 3 месяцев

$favStyle = 'color:#e65c00;';
$notActiveStyle = 'opacity: 0.3;';
$manyOrdersStyle = 'font-weight: 700;';


$fileData = file_get_contents($calledUsersFilePath);
$calledUsersInfo = [];
if ($fileData !== false) {
    $calledUsersInfo = json_decode($fileData, true);
    if (!$calledUsersInfo) {
        $calledUsersInfo = [];
    }
} else {
    echo '<b>Ошибка чтения файла обзвоненых клиентов! Обновите страницу!</b>';
}
?>
    <style>
        .call_user_success::after {
            content: "";
            background-image: url("/admin_subscription/success.png");
            display: inline-block;
            width: 32px;
            height: 32px;
        }

        .call_user_failure {
            content: "";
            background-image: url("/admin_subscription/failure.png");
            display: inline-block;
            width: 32px;
            height: 32px;
        }

        .choosen_a {
            font-weight: bold;
        }

        .not_choosen_a {
            font-weight: normal;
        }
    </style>

<? if (\Helpers\SafetyValidation::isAdminOrManger(CUser::GetID())): ?>
    <?
    $allFavs = Custom\FavoritesOnCookies\FavoritesTable::getList()->fetchAll();

    $usersForCalling = [];

    $usersFavs = [];
    foreach ($allFavs as $fav) {
        $usersFavs[$fav['ID_USER']][$fav['ID_ITEM']] = $fav['ID_ITEM'];
    }
    unset($allFavs);
    $select = ['ID', 'DATE_INSERT', 'USER_ID', 'STATUS_ID', 'PRICE', 'USER_LOGIN'];
    $allOrdersQ = CSaleOrder::GetList(
        ['ID' => 'ASC'],
        [
//                "STATUS_ID" => $validStatuses
        ],
        false,
        false,
        $select
    );
    while ($order = $allOrdersQ->Fetch()) {
        $usersForCalling[$order['USER_ID']]['ORDERS_ID'][] = $order['ID'];
        $usersForCalling[$order['USER_ID']]['ID_USER'] = $order['USER_ID'];
        $usersForCalling[$order['USER_ID']]['USER_LOGIN'] = $order['USER_LOGIN'];
        $usersForCalling[$order['USER_ID']]['ORDERS_TTL_PRICE'] += (float)$order['PRICE'];
        $usersForCalling[$order['USER_ID']]['LAST_ORDER_DATE_INSERT'] = $order['DATE_INSERT'];//22.10.2019 07:41:22
        $usersForCalling[$order['USER_ID']]['LAST_ORDER_DATE_INSERT_TS'] = 0;
        $d = DateTime::createFromFormat('d.m.Y H:i:s', $order['DATE_INSERT']);
        if ($d !== false) {
            $usersForCalling[$order['USER_ID']]['LAST_ORDER_DATE_INSERT_TS'] = $d->getTimestamp();
        }
    }


    $usersInfo = [];
    $by = 'id';
    $order = 'asc';
    $dbRes = \CUser::GetList($by, $order,
        array("ID" => implode(' | ', array_keys($usersForCalling))),
        array("FIELDS" => array("ID", "LOGIN", "NAME", "LAST_NAME", "LAST_LOGIN", "DATE_REGISTER", "PERSONAL_PHONE", "LAST_ACTIVITY_DATE"))
    );
    while ($arUser = $dbRes->Fetch()) {
        $usersInfo[$arUser['ID']] = $arUser;
    }

    ?>

    <p>Пользователи с товарами в избранном или с количеством заказов >= <?= $minOrdersForUsers; ?>, и датой последней покупки с <?= date("F j, Y H:m:s",$maxUserOrderDateTS); ?> по <?= date("F j, Y H:m:s",$minUserOrderDateTS); ?>:</p>
    <p>Цвета у товаров:</p>
    <div style="<?= $favStyle; ?>">- Товар добавлен в избранное:</div>
    <div style="<?= $notActiveStyle; ?>">- Товар не активен на данный момент(или вообще удален с сайта):</div>
    <div style="<?= $manyOrdersStyle; ?>">- Товар присутсвует в 2х и более заказах:</div>

    <p>Периодичность покупок товара расчитывает как (ПоследняяДатаПокупкиТовара -
        ПерваяДатаПокупкиТовара)/КоличествоЗаказовСЭтимТоваром</p>


    <? $k = 1; ?>
    <? foreach ($usersForCalling as $userForCalling): ?>

        <? if (
            ( //проверка на дату последнего заказа
                $userForCalling['LAST_ORDER_DATE_INSERT_TS'] > $maxUserOrderDateTS and $userForCalling['LAST_ORDER_DATE_INSERT_TS'] < $minUserOrderDateTS
            )
            and
            ( // проверка на минимум заказов у пользователя или наличие у пользователя товаров в избранном
                count($userForCalling['ORDERS_ID']) > $minOrdersForUsers or
                (count($userForCalling['ORDERS_ID']) > 0 && isset($usersFavs[$userForCalling['ID_USER']]))
            )
        ): ?>
            <? $userInfo = $usersInfo[$userForCalling['ID_USER']];
            $telPhone = preg_replace('/[^\d\+]/', '', $userInfo['PERSONAL_PHONE']);
            $telPhone = preg_replace('/^\+7/', '8', $telPhone);
            $lastCallInfo = 'обзвон еще не проводился';

            if (isset($calledUsersInfo[$userInfo['ID']])) {
                $calledUserInfo = $calledUsersInfo[$userInfo['ID']];
                $lastCallInfo = 'Время обзвона:' . date("Y-m-d H:i:s", $calledUserInfo['time_ts']) . '<br>';
                $lastCallInfo .= 'Результат: <span data-role="span_success_indication" class="' . ($calledUserInfo['success'] == 1 ? 'call_user_success' : 'call_user_failure') . '"></span><br>';
                $lastCallInfo .= 'Информация по звонку:<br>' . $calledUserInfo['message'];
            }
            ?>
            <br>
            <div data-role='info_block'>
                <div data-role='user_info_block'>
                    <b>#<?= $k++; ?></b> - id = <?= $userForCalling['ID_USER']; ?>
                    <a href="/bitrix/admin/sale_buyers_profile.php?USER_ID=<?= $userInfo['ID']; ?>&lang=ru"
                       target="_blank"><?= $userInfo['LAST_NAME'] . ' ' . $userInfo['NAME']; ?></a>
                    телефон: <b><a rel="nofollow"
                                   href="tel:<?= $telPhone; ?>"><?= $userInfo['PERSONAL_PHONE']; ?></a></b>,

                    <? $callClass = '';
                    if (isset($calledUsersInfo[$userInfo['ID']])) {
                        if ($calledUsersInfo[$userInfo['ID']]['success'] == 1) {
                            $callClass = "call_user_success";
                        } else {
                            $callClass = "call_user_failure";
                        }
                    } ?>

                    <span data-role="span_success_indication" class="<?= $callClass; ?>"></span>
                </div>
                <br>
                <div data-role='orders_info_block_parent' style="margin-left: 50px;">
                    <a data-role='show_user_orders_info' data-user_id='<?= $userInfo['ID']; ?>'>подробнее о заказах</a>
                    <div data-role='orders_info_block' style="display:none">

                        <a data-role='close_user_orders_info'>скрыть</a>


                        <br>
                        дата регистрации: <?= $userInfo['DATE_REGISTER']; ?>,
                        последняя активность на сайте: <?= $userInfo['LAST_ACTIVITY_DATE']; ?>,
                        <br>
                        всего
                        заказов: <b><?= count($userForCalling['ORDERS_ID']); ?></b> на
                        сумму: <?= round($userForCalling['ORDERS_TTL_PRICE'], 2); ?> руб.
                        Последний заказ от: <b><?= $userForCalling['LAST_ORDER_DATE_INSERT']; ?></b>


                        <br>
                        <div>
                            добавить информацию по звонку:<br>
                            <form method="POST" data-role="call_info_form">
                                <textarea style="width:400px;height: 150px;"
                                          data-role="call_info_form_textarea"
                                          placeholder="*введите сюда результат обзвона"></textarea>
                                <br>
                                <b>Не забудьте сохранить текст нажав на кнопку ниже!</b>
                                <br>
                                обзвон: <input type="button" data-role="call_info_form_success" value="Успешен">
                                <input data-role="call_info_form_failure" type="button" value="Не успешен">
                                <input data-role="call_info_form_user_id" type="hidden" value="<?= $userInfo['ID']; ?>">
                                <br>
                                <span>
                                    рельтат последнего обзвона: <br>
                                    <div data-role="last_call_info">
                                        <?= $lastCallInfo; ?>
                                    </div>
                                </span>
                            </form>
                        </div>
                        <br>
                        <div data-role='orders_info' style="margin-left: 50px;">

                        </div>
                        <br>

                        <br>
                        <a data-role='close_user_orders_info'>скрыть заказы</a>

                    </div>
                </div>

            </div>
        <? endif; ?>

    <? endforeach; ?>

    <script>
        (function () {
            $("[data-role='show_user_orders_info']").click(function (e) {
                e.preventDefault();
                var userId = $(this).data('user_id');
                var outputContainer = $(this).parent().find("[data-role='orders_info']");
                outputContainer.html('<img src="/bitrix/js/rest/images/loader.gif"/>');
                $(this).parent().find("[data-role='orders_info_block']").fadeIn();

                $.ajax({
                    url: "/admin_subscription/ajax.php",
                    data: {'action': 'ordersDetail', 'userId': userId},
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (data.error == '1') {
                            modalError(data.text);
                            outputContainer.html(data.text);
                        } else {
                            var output = '<h3>Избранное пользователя и повторяющиеся товары:<h3>';

                            if (data.products) {
                                var normalProductsHtml = '';
                                var notBuyedFavsHtml = '';

                                output += '<div style="font-size: 14px;">';
                                output += ' <input type="checkbox" id="show_no_orders_products_' + userId + '"> <label for="show_no_orders_products_' + userId + '">Избранные товары без покупок</label> ';
                                output += ' <input type="checkbox" id="show_not_active_products_' + userId + '"> <label for="show_not_active_products_' + userId + '">не активные товары</label> ';
                                output += '<br>';
                                output += 'Сортировать по <a style="font-size: 14px;" class="choosen_a" data-role="sort_by_orders">Количеству заказов</a>|<a class="not_choosen_a" style="font-size: 14px;" data-role="sort_by_period">Периодичности</a>';
                                output += '</div>';
                                output += normalProductsHtml;
                                output += notBuyedFavsHtml;


                                output += '<div data-role="popular_products_block">';
                                Object.values(data.products).sort(function (a, b) {
                                    return parseInt(b.ORDERS_CNT) - parseInt(a.ORDERS_CNT)
                                }).forEach(function (pr, index) {
                                    var prInfo = data.products[pr['PRODUCT_ID']];
                                    if (prInfo['FAV'] == 1 || prInfo['ORDERS_CNT'] > 1) {
                                        output += getProductRowHtml(prInfo, 'all');
                                    }
                                });
                                output += '</div>';

                            }

                            output += '<h3>Заказы пользователя:<h3>';
                            output += '<p><a href="javascript:void(0);" data-role="show_orders_block">Показать</a>/<a href="javascript:void(0);" data-role="hide_orders_block">Скрыть</a> Заказы пользователя</p><br>';
                            output += '<div data-role="orders_block" style="display:none">';
                            if (data.orders) {
                                Object.keys(data.orders).sort(function (a, b) {
                                    return parseInt(b) - parseInt(a)
                                }).forEach(function (orderId) {
                                    var orderInfo = data.orders[orderId];
                                    output += '<br>';
                                    output += getOrderRowHtml(orderInfo);
                                });
                            }
                            output += '</div>';
                            outputContainer.html(output);
                        }
                    },
                    error: function () {
                        modalError('Ошибка загрузки заказа');
                        outputContainer.html('Ошибка загрузки заказа! попробуйте нажать кнопку "подробнее" еще раз.');
                    }
                });
            });


            $("[data-role='close_user_orders_info']").click(function (e) {
                e.preventDefault();
                var parent = $(this).closest("[data-role='orders_info_block_parent']");
                parent.find("[data-role='orders_info_block']").fadeOut(0);
                parent.find("[data-role='orders_info']").html('');
            });

            function getProductRowHtml(prInfo, type) {
                var favStyle = '<?=$favStyle; ?>';
                var notActiveStyle = '<?=$notActiveStyle; ?>';
                var manyOrdersStyle = '<?=$manyOrdersStyle; ?>';

                var prName = 'Товар удален из базы';
                if (prInfo['NAME'] != '') {
                    prName = prInfo['NAME'];
                }

                var style = '';
                var ttlBuyStyle = '';
                if (prInfo['FAV'] == 1) {
                    style += favStyle;
                }
                if (prInfo['ACTIVE'] == 'N') {
                    style += notActiveStyle;
                    ttlBuyStyle += notActiveStyle;
                }
                if (prInfo['ORDERS_CNT'] > 1) {
                    style += manyOrdersStyle;
                    ttlBuyStyle += manyOrdersStyle;
                }
                var ttlQty = 0;
                if (prInfo['TTL_QUANTITY']) {
                    ttlQty = prInfo['TTL_QUANTITY'];
                }

                var display = '';
                var divAttrs = 'data-product_orders_cnt="' + prInfo['ORDERS_CNT'] + '"';

                var periodDays = 9999;
                if (prInfo['ORDERS_CNT'] > 1) {
                    periodDays = parseInt(prInfo['DAYS_BETWEEN_PURCHASES']);
                }
                divAttrs += 'data-product_period = "' + periodDays + '"';

                if (prInfo['ORDERS_CNT'] <= 0) {
                    display = 'display: none;';
                    divAttrs += 'data-product_no_order = "1"';
                }
                if (prInfo['ACTIVE'] == 'N') {
                    divAttrs += 'data-product_active = "0"';
                    display = 'display: none;';
                } else {
                    divAttrs += 'data-product_active = "1"';
                }

                if (type == 'all') {
                    var html = `<div data-role="popular_products" style="font-size: 16px;${display}" ${divAttrs} >`;
                    html += `${prInfo['PRODUCT_ID']} - <span style="${style}">${prName}</span>, <span style="${ttlBuyStyle}">Всего покупок:${prInfo['ORDERS_CNT']}, всего шт:${ttlQty}</span>`;
                    if (prInfo['ORDERS_CNT'] > 1) {
                        html += `, Периодичность: ${periodDays} д.`;
                    }
                    html += '</div>';

                } else if (type == 'order') {
                    var html = '<div style="font-size: 16px;">';
                    html += `${prInfo['PRODUCT_ID']} - <span style="${style}">${prName}</span> - <span style="${style}">${prInfo['CURRENT_QUANTITY']} шт. по ${prInfo['CURRENT_PRICE']} руб.</span>,
                                <span style="${ttlBuyStyle}">Всего покупок:${prInfo['ORDERS_CNT']}, всего шт:${ttlQty}</span>`;
                    if (prInfo['ORDERS_CNT'] > 1) {
                        html += `, Периодичность: ${prInfo['DAYS_BETWEEN_PURCHASES']} д.`;
                    }
                    html += '</div>';

                }
                return html;
            }

            function getOrderRowHtml(orderInfo) {
                var html = '<div>';
                html += `<p>Заказ <a href="/bitrix/admin/sale_order_view.php?amp%3Bfilter=Y&%3Bset_filter=Y&lang=ru&ID=${orderInfo['ORDER_ID']}" target="_blank">№${orderInfo['ORDER_ID']}</a>
                        От ${orderInfo['DATE_INSERT']} на сумму ${orderInfo['PRICE']}</p>`;
                html += '<div style="margin-left: 20px">';
                for (var prId in orderInfo.PRODUCTS) {
                    var prInfo = orderInfo.PRODUCTS[prId];
                    html += getProductRowHtml(prInfo, 'order');
                }
                html += '</div>';
                html += '</div>';
                return html;
            }

            $("[data-role='call_info_form_success']").click(function (e) {
                e.preventDefault();
                $form = $(this).closest('[data-role="call_info_form"]');
                sendCallInfo($form, 1);
            });
            $("[data-role='call_info_form_failure']").click(function (e) {
                e.preventDefault();
                $form = $(this).closest('[data-role="call_info_form"]');
                sendCallInfo($form, 0);
            });

            function sendCallInfo($form, success) {
                var textAreaVal = $form.find("[data-role='call_info_form_textarea']").val().trim();
                var userId = $form.find("[data-role='call_info_form_user_id']").val().trim();
                var $parentBlock = $form.closest("[data-role='info_block']");
                if (!textAreaVal || textAreaVal == '') {
                    modalError('заполните поле с результатом обзвона!');
                } else {
                    $.ajax({
                        url: "/admin_subscription/ajax.php",
                        data: {'action': 'addCallInfo', 'text': textAreaVal, 'success': success, 'userId': userId},
                        type: 'POST',
                        dataType: 'json',
                        beforeSend: function (xhr) {
                            $form.find('input,textarea').attr('disabled', 'disabled');
                        },
                        success: function (data) {
                            if (data.error == '1') {
                                modalError(data.text);
                            } else {
                                var successClass = 'call_user_failure';
                                if (data.callData.success == 1) {
                                    successClass = 'call_user_success';
                                }
                                var date = new Date(data.callData['time_ts'] * 1000);//format('Y-m-d H:i:s');
                                var lastCallInfo = `Время обзвона:${date.getFullYear()}-${date.getMonth()}-${date.getDay()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}<br>`;
                                lastCallInfo += 'Результат: <span data-role="span_success_indication"></span><br>';
                                lastCallInfo += 'Информация по звонку:<br>' + data.callData['message'];
                                $parentBlock.find('[data-role="last_call_info"]').html(lastCallInfo);

                                $parentBlock.find('[data-role="span_success_indication"]').attr('class', successClass);

                            }
                            $form.find('input,textarea').removeAttr('disabled');
                        },
                        error: function () {
                            $form.find('input,textarea').removeAttr('disabled');
                            modalError('Ошибка!');
                            outputContainer.html('Ошибка! попробуйте еще раз.');
                        }
                    });
                }
            }


            $(document).on("change", "[id^='show_no_orders_products_']", function (e) {
                matchVisibilityProducts($(this).closest('[data-role="orders_info_block_parent"]'));
            });

            $(document).on("change", "[id^='show_not_active_products_']", function (e) {
                matchVisibilityProducts($(this).closest('[data-role="orders_info_block_parent"]'));
            });

            function matchVisibilityProducts($parentBlock) {
                var showNotActive = $parentBlock.find('[id^="show_not_active_products_"]:checked').length;
                var showNoOrders = $parentBlock.find('[id^="show_no_orders_products_"]:checked').length;
                $parentBlock.find(`[data-role="popular_products"]`).fadeIn(0);
                if (!showNotActive) {
                    $parentBlock.find(`[data-role="popular_products"][data-product_active="0"]`).fadeOut(0);
                }
                if (!showNoOrders) {
                    $parentBlock.find(`[data-product_no_order="1"]`).fadeOut(0);
                }
            }

            $(document).on("click", "[data-role='sort_by_orders']", function (e) {
                var $parent = $(this).closest('[data-role="orders_info_block_parent"]');
                $(this).removeClass('not_choosen_a');
                $(this).addClass('choosen_a');
                $("[data-role='sort_by_period']").removeClass('choosen_a');
                $("[data-role='sort_by_period']").addClass('not_choosen_a');

                sortProductsBy($parent, 'orders');
            });

            $(document).on("click", "[data-role='sort_by_period']", function (e) {
                var $parent = $(this).closest('[data-role="orders_info_block_parent"]');
                $(this).removeClass('not_choosen_a');
                $(this).addClass('choosen_a');
                $("[data-role='sort_by_orders']").removeClass('choosen_a');
                $("[data-role='sort_by_orders']").addClass('not_choosen_a');
                sortProductsBy($parent, 'period');
            });

            function sortProductsBy($block, by) {
                $rows = $block.find('[data-role="popular_products"]');
                if ($rows.length) {
                    var productsInfo = [];
                    for (var k = 0; k < $rows.length; k++) {
                        $productNode = $rows[k];
                        productsInfo.push({
                            'node': $productNode,
                            'orders': $productNode.getAttribute('data-product_orders_cnt'),
                            'period': $productNode.getAttribute('data-product_period'),
                        });
                    }
                    if (by == 'orders') {
                        productsInfo = productsInfo.sort(function (a, b) {
                            return parseInt(b.orders) - parseInt(a.orders)
                        })
                    } else if (by == 'period') {
                        productsInfo = productsInfo.sort(function (a, b) {
                            return parseInt(a.period) - parseInt(b.period)
                        })
                    }
                    $productsBlock = $block.find('[data-role="popular_products_block"]');
                    $productsBlock.html('');
                    for (var k = 0; k < productsInfo.length; k++) {
                        pr = productsInfo[k];
                        $productsBlock.append(pr.node);
                    }
                }
            }

            $(document).on("click", "[data-role='show_orders_block']", function (e) {
                $(this).closest('[data-role="orders_info_block_parent"]').find('[data-role="orders_block"]').fadeIn();
            });
            $(document).on("click", "[data-role='hide_orders_block']", function (e) {
                $(this).closest('[data-role="orders_info_block_parent"]').find('[data-role="orders_block"]').fadeOut();
            });

        })();

    </script>


<? else: ?>
    <? LocalRedirect('/'); ?>
<? endif; ?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>