<?

use Custom\FavoritesOnCookies\FavoritesTable;
use \Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$calledUsersFilePath = __DIR__ . '/calledUsers.txt';
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$answer = [
    'error' => '1',
    'text' => 'Неизвестная ошибка!',
];
$validStatuses = ["F", 'IS', 'P', 'R'];

if (!Loader::includeModule('custom.favoritesoncookies')) {
    $answer = [
        'error' => '1',
        'text' => 'Нет модуля ихбранного',
    ];
    return false;
} else if (\Helpers\SafetyValidation::isAdminOrManger(CUser::GetID())) {
    $action = $request->get('action');
    switch ($action) {
        case 'ordersDetail':
            $ordersInfo = [];
            $productsInfo = [];

            $userId = $request->get('userId');
            $select = ['ID'];
            $allOrdersQ = CSaleOrder::GetList(
                ['ID' => 'ASC'],
                ["STATUS_ID" => $validStatuses, 'USER_ID' => $userId],
                false,
                false,
                $select
            );
            $favProductsIds = FavoritesTable::getItemIdsForUserId($userId);
            if ($favProductsIds) {
                foreach ($favProductsIds as $favProductId) {
                    $productsInfo[$favProductId] = [
                        'PRODUCT_ID' => $favProductId,
                        'ACTIVE' => 'N',
                        'NAME' => '',
                        'PRODUCT_PICTURE_ID' => false,
                        'PRODUCT_PICTURE_SRC' => false,
                        'FAV' => 1,
                        'ORDERS_CNT' => 0,
                    ];
                }
            }

            while ($row = $allOrdersQ->Fetch()) {
                $order = Bitrix\Sale\Order::load($row['ID']);
                $basket = $order->getBasket();
                $currentOrdersInfo = [
                    'ORDER_ID' => $order->getId(),
                    'DATE_INSERT' => $order->getDateInsert()->toString(),
                    'PRICE' => $order->getPrice(),
                    'PRODUCTS_RAW' => []
                ];

                foreach ($basket as $basketItem) {
                    $prId = $basketItem->getProductId();
                    $currentOrdersInfo['PRODUCTS_RAW'][$prId] = [
                        'ID' => $prId,
                        'PRICE' => $basketItem->getPrice(),
                        'QUANTITY' => $basketItem->getQuantity()
                    ];
                    $productsInfo[$prId]['PRODUCT_ID'] = $prId;
                    $productsInfo[$prId]['ACTIVE'] = 'N';
                    $productsInfo[$prId]['NAME'] = '';
                    $productsInfo[$prId]['PRODUCT_PICTURE_ID'] = false;
                    $productsInfo[$prId]['PRODUCT_PICTURE_SRC'] = false;
                    $productsInfo[$prId]['LAST_DATE_BUY'] = $order->getDateInsert()->toString();
                    $productsInfo[$prId]['LAST_DATE_BUY_TS'] = $order->getDateInsert()->getTimestamp();

                    if (!isset($productsInfo[$prId]['FIRST_DATE_BUY'])) {
                        $productsInfo[$prId]['FIRST_DATE_BUY'] = $order->getDateInsert()->toString();
                        $productsInfo[$prId]['FIRST_DATE_BUY_TS'] = $order->getDateInsert()->getTimestamp();
                    }
                    if (!isset($productsInfo[$prId]['FAV'])) {
                        $productsInfo[$prId]['FAV'] = 0;
                    }
                    if (!isset($productsInfo[$prId]['ORDERS_CNT'])) {
                        $productsInfo[$prId]['ORDERS_CNT'] = 1;
                    } else {
                        $productsInfo[$prId]['ORDERS_CNT']++;
                    }
                    if (!isset($productsInfo[$prId]['TTL_QUANTITY'])) {
                        $productsInfo[$prId]['TTL_QUANTITY'] = $basketItem->getQuantity();
                    } else {
                        $productsInfo[$prId]['TTL_QUANTITY'] += $basketItem->getQuantity();
                    }
                }
                $ordersInfo[] = $currentOrdersInfo;
            }

            foreach ($productsInfo as $key => $pr) {
                if ($pr['ORDERS_CNT'] > 1) {
                    $productsInfo[$key]['DAYS_BETWEEN_PURCHASES'] = ((float)($pr['LAST_DATE_BUY_TS'] - $pr['FIRST_DATE_BUY_TS'])) / (24 * 3600) / ($pr['ORDERS_CNT'] - 1);
                    $productsInfo[$key]['DAYS_BETWEEN_PURCHASES'] = ceil($productsInfo[$key]['DAYS_BETWEEN_PURCHASES']);
                }
            }


            $productsQ = \CIBlockElement::GetList(
                array(),
                array('IBLOCK_ID' => 114, 'ID' => array_keys($productsInfo)),
                false,
                false,
                array('ID', 'NAME', 'DETAIL_PICTURE', 'ACTIVE', 'DETAIL_PAGE_URL')
            );
            while ($row = $productsQ->GetNext()) {
                $prId = $row['ID'];
                $productsInfo[$prId]['NAME'] = $row['NAME'];
                $productsInfo[$prId]['ACTIVE'] = $row['ACTIVE'];
                $productsInfo[$prId]['DETAIL_PICTURE_ID'] = $row['~DETAIL_PICTURE'];
                $productsInfo[$prId]['DETAIL_PAGE_URL'] = $row['DETAIL_PAGE_URL'];
            }

            foreach ($ordersInfo as $ordersInfoKey => $orderInfo) {
                $ordersInfo[$ordersInfoKey]['PRODUCTS'] = [];
                foreach ($orderInfo['PRODUCTS_RAW'] as $productRaw) {
                    $prId = $productRaw['ID'];
                    $ordersInfo[$ordersInfoKey]['PRODUCTS'][$prId] = $productsInfo[$prId];
                    $ordersInfo[$ordersInfoKey]['PRODUCTS'][$prId]['CURRENT_QUANTITY'] = $productRaw['QUANTITY'];
                    $ordersInfo[$ordersInfoKey]['PRODUCTS'][$prId]['CURRENT_PRICE'] = $productRaw['PRICE'];
                }
            }

            $answer = [
                'error' => '0',
                'orders' => $ordersInfo,
                'products' => $productsInfo
            ];

            break;
        case 'addCallInfo':
            $text = trim($request->get('text'));
            $success = $request->get('success');
            $userId = $request->get('userId');
            if ($text) {
                $text = htmlspecialchars($text);
                $text = nl2br($text);
                if ($userId) {
                    $fileData = file_get_contents($calledUsersFilePath);
                    if ($fileData !== false) {
                        $calledUsersInfo = json_decode($fileData, true);
                        if (!$calledUsersInfo) {
                            $calledUsersInfo = [];
                        }
                        $calledUsersInfo[$userId] = [
                            'time_ts' => time(),
                            'success' => (int)$success,
                            'message' => $text
                        ];
                        if (file_put_contents($calledUsersFilePath, json_encode($calledUsersInfo)) !== false) {
                            $answer = [
                                'error' => '0',
                                'callData' => $calledUsersInfo[$userId]
                            ];
                        } else {
                            $answer = [
                                'error' => '1',
                                'text' => 'ошибка записи в файл! попробуйте позже или обратитесь к администратору!'
                            ];
                        }

                    } else {
                        $answer = [
                            'error' => '1',
                            'text' => 'ошибка чтения файла! попробуйте позже или обратитесь к администратору!'
                        ];
                    }
                } else {
                    $answer = [
                        'error' => '1',
                        'text' => 'не пришел userId обратитесь к администратору'
                    ];
                }

            } else {
                $answer = [
                    'error' => '1',
                    'text' => 'должен быть заполнен текст!'
                ];
            }
            break;
        default:
            break;
    }

} else {
    $answer = [
        'error' => '1',
        'text' => 'нет доступа попробуйте перезайти в свою учетку'
    ];
}
echo json_encode($answer);


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>