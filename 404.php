<?

include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");
$arDirProperties = Array(
    "viewed_show" => "Y",
    "HIDE_LEFT_BLOCK" => "Y",
    "MENU_SHOW_SECTIONS" => "Y"
);
$APPLICATION->SetPageProperty("HIDE_LEFT_BLOCK","Y");
?>
<div class="maxwidth-theme">
	<div class="content">

        <div class="block_404">
            <img class="block_404_img" src="/images/404.png"/>
            <div class="block_404_title">Страница не найдена</div>
            <div class="block_404_description">Неправильно набран адрес или такой страницы не существует</div><br/>
            <a class="btn btn-default btn-lg block_404_main_btn" href="<?=SITE_DIR?>"><span>Перейти на главную</span></a>
            <a class="block_404_back_block" onclick="history.back()">
                <img src="/images/404_back_btn.png" class="block_404_back_btn_img"/>
                <div class="block_404_back_btn">
                    вернуться назад
                </div>
            </a>
        </div>

	</div>
</div>
<?php
$GLOBALS['404_page_slider'] = true;
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>