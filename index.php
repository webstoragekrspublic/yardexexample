<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интернет-магазин продуктов питания и деликатесов Ярбокс: бесплатная доставка еды на дом и в офис в Красноярске. Минимальный заказ 1000 руб. Оформляйте заказ на сайте. Или звоните 8 (391) 290-24-00.");
$APPLICATION->SetPageProperty("keywords", "доставка еды, доставка продуктов, доставка продуктов Красноярск, доставка продуктов на дом, заказать продукты с доставкой, интернет магазин продуктов питания");
$APPLICATION->SetPageProperty("title", "Доставка продуктов на дом и в офис в Красноярске - Ярбокс");
$APPLICATION->SetPageProperty("viewed_show", "Y");
$APPLICATION->SetTitle("Доставка продуктов на дом и в офис в Красноярске-Ярбокс");
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>