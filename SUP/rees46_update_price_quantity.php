<?
#require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "IBLOCK_SECTION_ID");
//// Фильтровать только по изменненым за последний час товарам
//$hourSeconds = 60 * 60;
//
//$dateTime = new \DateTime();
//$dateTime->setTimestamp(time());
//$beforeTimeStamp = $dateTime->getTimestamp() - $hourSeconds;
//$beforeDateTime = new \DateTime();
//$beforeDateTime->setTimestamp($beforeTimeStamp);
////$beforeDateTime = \DateTime::createFromFormat('!d.m.Y',"{$beforeDateTime->format('d')}.{$beforeDateTime->format('m')}.{$beforeDateTime->format('Y')}");
//$DATE_CREATE_FROM = \Bitrix\Main\Type\DateTime::createFromTimestamp($beforeDateTime->getTimestamp());
//
//$currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";
//
//$currentUrl .= $_SERVER["HTTP_HOST"];
//
//$arFilter = Array("IBLOCK_ID"=>114, ">DATE_MODIFY_FROM" => $DATE_CREATE_FROM->toString());
//$resEl = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
//$items = [];
//while($arEl = $resEl->GetNextElement()){
//    $arFields = $arEl->GetFields();
//    
//    $currency = "RUB";
//    
//    $imgSrc = CFile::GetPath($arFields["DETAIL_PICTURE"]);
//    
//    $arCatalogRes = CCatalogProduct::GetByID($arFields["ID"]);
//    $available = false;
//    if($arCatalogRes["QUANTITY"] > 0){
//        $available = true;
//    }
//    
//    $price = CPrice::GetBasePrice($arFields["ID"]);
//    $price = $price["PRICE"];
//    
//    $arFields["NAME"] = str_replace('&quot;', '"', $arFields["NAME"]);
//    
//    $items[] = [
//        'id' => $arFields["ID"],
//        'name' => $arFields["NAME"],
//        'price' => $price,
//        'currency' => $currency,
//        'url' => $currentUrl.$arFields["DETAIL_PAGE_URL"],
//        'picture' => $imgSrc,
//        'available' => $available,
//        'categories' => [$arFields["IBLOCK_SECTION_ID"]],
//        'stock_quantity' => $arCatalogRes["QUANTITY"],
//    ];
//}
//
////pr($items);
////exit;
//
//$request = array(
//    'shop_id'	=> '60eec8bb4851421cbbcdac54907c00',
//    'shop_secret' => '1a7394f1a45a26b68d4f01751a2fee3b',
//    'items' => $items,
//);
//$put = json_encode($request);
//
////pr($put);
////exit;
//
//$ch = curl_init('https://api.rees46.com/import/products');
//curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_ENCODING, '');
//curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
//curl_setopt($ch, CURLOPT_TIMEOUT, 0);
//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $put);
//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
//$data = curl_exec($ch);
//
//curl_close($ch);
//
//pr($data);
//exit;

$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.rees46.com/import/products',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'PUT',
  CURLOPT_POSTFIELDS =>'{
    "shop_id": "60eec8bb4851421cbbcdac54907c00",
    "shop_secret": "1a7394f1a45a26b68d4f01751a2fee3b",
    "items": [
        {
            "id": "49393",
            "name": "Сметана Простоквашино 10% стакан 0,300гр",
            "price": 90.2,
            "currency": "RUB",
            "url": "https://yarbox.ru/catalog/moloko_syr_jajco/smetana/49393-smetana_prostokvashino_10_stakan_0300gr/",
            "picture": "https://yarbox.ru/upload/iblock/35f/cjam8jn7vu0cv4qt09qmdncgqxh22h8g.jpg",
            "available": true,
            "categories": ["1311"],
            "stock_quantity": "4"
        }
    ]
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;

?>
