<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Страница с данными компании (ИНН, ОГРН, ОКПО, ОКВЭД) и банковскими реквизитами (БИК, к/с, р/с), а также адресом регистрации и фактического ведения деятельности.");
$APPLICATION->SetPageProperty("keywords", "реквизиты, учредительные документы, карточка предприятия");
$APPLICATION->SetPageProperty("title", "Реквизиты интернет-магазин доставки еды Ярбокс");
$APPLICATION->SetTitle("Реквизиты");?><h2 class="no-top-space"><b>Реквизиты компании</b></h2>
<p>
</p>
<p>
	 «Ярдекс» является зарегистрированным товарным знаком. Все права принадлежат индивидуальному предпринимателю Ярошенко Сергею Анатольевичу.<br>
</p>
<table border="1" cellpadding="1" cellspacing="1">
<tbody>
<tr>
	<td>
 <b>&nbsp;</b><b>Полное наименование</b><b> </b><b> </b>
	</td>
	<td>
		 &nbsp;Индивидуальный предприниматель Ярошенко Сергей Анатольевич<br>
	</td>
</tr>
<tr>
	<td>
 <b> </b><b>
		&nbsp;Юридический адрес </b><b> </b>
	</td>
	<td>
 <b> </b>&nbsp;660059, г. Красноярск, пр. им. газ. Красноярский рабочий, д 102А, кв 18&nbsp; &nbsp; &nbsp; &nbsp;
	</td>
</tr>
<tr>
	<td>
 <b>&nbsp;Фактический адрес </b><b> </b>
	</td>
	<td>
		 &nbsp;660031, г. Красноярск, ул. Рязанская, 65Г, оф. А-375 <br>
	</td>
</tr>
<tr>
	<td>
 <b> </b><b>
		&nbsp;ОГРНИП </b><b> </b>
	</td>
	<td>
		 &nbsp;304246123900011 <b><br>
 </b><b> </b>
	</td>
</tr>
<tr>
	<td colspan="1">
		 &nbsp;<b>ИНН</b>
	</td>
	<td colspan="1">
		 &nbsp;246300064991<br>
	</td>
</tr>
<tr>
	<td>
 <b> </b><b>
		&nbsp;ОКПО </b><b> </b>
	</td>
	<td>
		 &nbsp;0080335101<b><br>
 </b><b> </b>
	</td>
</tr>
<tr>
	<td>
 <b> </b><b>
		&nbsp;ОКВЭД </b><b> </b>
	</td>
	<td>
 <b> </b><b>
		&nbsp;</b>51.36, 51.18, 51.38<b><br>
 </b><b> </b>
	</td>
</tr>
<tr>
	<td>
 <b> </b><b>
		&nbsp;Свидетельство о регистрации&nbsp;&nbsp;</b><b> </b>
	</td>
	<td>
 <b> </b><b>
		&nbsp;</b>№7631, серии 24 №004241750 выдано администрацией Кировского района&nbsp;<br>
		 &nbsp;города Красноярска 10.01.2000 г.<b><br>
 </b><b> </b>
	</td>
</tr>
<tr>
	<td>
 <b> </b><b>
		&nbsp;Расчётный счет </b><b> </b>
	</td>
	<td>
 <b> </b><b>
		&nbsp;</b>40802810531280116335<b><br>
 </b><b> </b>
	</td>
</tr>
<tr>
	<td>
 <b> </b><b>
		&nbsp;Корреспондентский счет </b><b> </b>
	</td>
	<td>
 <b> </b><b>
		&nbsp;</b>30101810800000000627<b><br>
 </b><b> </b>
	</td>
</tr>
<tr>
	<td>
 <b> </b><b>
		&nbsp;БИК </b><b> </b>
	</td>
	<td>
 <b> </b><b>
		&nbsp;</b>040407627<b><br>
 </b><b> </b>
	</td>
</tr>
<tr>
	<td colspan="1">
 <b> </b><b>
		&nbsp;Руководитель </b><b> </b>
	</td>
	<td colspan="1">
 <b> </b><b>
		&nbsp;</b>Ярошенко Сергей Анатольевич<b><br>
 </b><b> </b>
	</td>
</tr>
<tr>
	<td colspan="1">
 <b> </b><b>
		&nbsp;Главный бухгалтер</b><b> </b><b> </b>
	</td>
	<td colspan="1">
 <b> </b><b>
		&nbsp;</b>Бабурина Ирина Ильинична<b><br>
 </b>
	</td>
</tr>
</tbody>
</table>
<p>
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>