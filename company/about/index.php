<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Интернет-магазин продуктов питания и деликатесов Ярбокс: бесплатная доставка еды на дом и в офис в Красноярске. Минимальный заказ 1000 руб. Оформляйте заказ на сайте. Или звоните 8 (391) 290-24-00.");
$APPLICATION->SetPageProperty("keywords", "компания, информация о компании, карточка предприятия, презентация компании");
$APPLICATION->SetPageProperty("title", "Доставка продуктов на дом и в офис в Красноярске - Ярбокс");
$APPLICATION->SetTitle("Доставка продуктов питания на дом");
global $arBackParametrs;
$phone = $arBackParametrs['HEADER_PHONES_array_PHONE_VALUE_0'];
$telPhone = preg_replace("/[^\d]+/", "", $phone);
?><div class="company_block">
    <div class="left_block">
        <? CNext::ShowPageType('left_block'); ?>
    </div>
    <div class="right_block">
        <h2 class="information_page_header"> <span class="orange_color">Ярбокс –</span> доставка <br>
            продуктов на дом в Красноярске </h2>
        <div class="information_page_image_container">
            <img src="/company/about/aboutcompany.jpg">
        </div>
        <p>
        </p>
        <p>
            Ярбокс.рф – это интернет-магазин продуктов питания с собственными складами и курьерской доставкой в день заказа.
        </p>
        <p>
        </p>
        <p>
            Ярбокс экономит&nbsp;ваше время&nbsp;и энергию, которые вы каждый день тратите в очередях, пробках или неся тяжелые покупки домой. Освободите время для любимых занятий или близких людей!
        </p>
    </div>
    </div>
    <div class="company_block grey_background company_block_with_phone">
        <div class="left_block">
            <img src="/company/about/mobile.png">
        </div>
        <div class="right_block">
            <p>
                <span class="orange_color">В нашем онлайн-каталоге </span>вы найдете более 5 000 товаров: от мясной продукции до чая и кофе.
            </p>
            <p>
                Заказать продукты на сайте вы можете с компьютера или смартфона в любом месте, где есть интернет.
            </p>
            <p>
                Совершать покупки можно из дома, на прогулке с маленьким ребёнком, в пробке по дороге на работу или в обеденный перерыв. <span class="orange_color">Поиск нужного товара</span> не займет у вас много времени. Вводить данные при каждом заказе не придется – наш сайт запоминает информацию, введенную при первом заказе и автоматически предлагает использовать ее в следующий раз.
            </p>
            <p>
                При заказе на сайте проще контролировать расходы, так как вы всегда видите сумму покупок в <span class="orange_color">онлайн-корзине.</span>
            </p>
        </div>
    </div>
    <p>
    </p>
    <br>
    <br>
    <div class="company_block">
        <div class="left_block">
        </div>
        <div class="right_block">
            <img src="/images/icons/service_products.png">
            <p>
            </p>
            <h2 class="information_page_header orange_color">Наш сервис пригодится,<br>
                когда вы:</h2>
            <p>
            </p>
            <p>
            </p>
            <ul>
                <li>хотите порадовать себя и свою семью любимыми продуктами;</li>
                <p>
                </p>
                <li>делаете недельный запас продуктов или готовитесь к семейному торжеству;</li>
                <p>
                </p>
                <li>проявляете заботу о близких, даря им оплаченный на сайте заказ.</li>
            </ul>
        </div>
    </div>
    <div class="company_block_features">
        <div class="row">
            <div class="col-xs-12 col-sm-4 company_block_features_item">
                <div class="company_block_features_item_image">
                    <img src="/images/icons/shop_delivery.png">
                </div>
                <div class="company_block_features_item_title">
                    Удобные<br>
                    двухчасовые интервалы<br>
                    доставки
                </div>
                <div class="company_block_features_item_text">
                    Чтобы вам не пришлось ожидать курьера весь день
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 company_block_features_item">
                <div class="company_block_features_item_image">
                    <img src="/images/icons/action_big.png">
                </div>
                <div class="company_block_features_item_title">
                    Регулярные<br>
                    акции<br>
                    и спец предложения
                </div>
                <div class="company_block_features_item_text">
                    Чтобы вы покупали продукты выгодно
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 company_block_features_item">
                <div class="company_block_features_item_image">
                    <img src="/images/icons/shop_delivery.png">
                </div>
                <div class="company_block_features_item_title">
                    Тщательный<br>
                    контроль<br>
                    качества
                </div>
                <div class="company_block_features_item_text">
                    Каждый заказ бережно собирается на специализированном пригородном складе Ярдекс площадью 26 000 м².
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="company_block company_block_with_call">
        <div class="left_block">
            <img src="/company/about/call.png">
        </div>
        <div class="right_block">
            <h2 class="information_page_header">Собственная<br>
                курьерская служба</h2>
            <p>
            </p>
            <p>
                Наша компания не работает с посредниками. Для вас это означает, что заказы доставляются на современном транспорте с необходимым температурным режимом и не потеряются с неизвестным курьером.
            </p>
            <br>
            <h2 class="information_page_header orange_color"> <a rel="nofollow" class="no_wrap_text" href="tel:+<?= $telPhone; ?>"><?= $phone; ?></a> </h2>
            <p>
                Операторы Контакт-центра ответят на вопросы и помогут решить любую ситуацию по номеру:
            </p>
            <p>
                Звонки принимаются в будни с 08:00 до 20:00.
            </p>
            <br>
        </div>
    </div>
    <div class="company_block grey_background company_block_with_logo">
        <div class="left_block">
        </div>
        <div class="right_block">
            <h2 class="information_page_header orange_color">Также в группу компаний входят:</h2>
            <p>
                <a href="https://www.siblogistic.ru/"> <img src="/images/company/logo/слк.png"> </a> <a href="https://megafors.ru/"> <img src="/images/company/logo/форс.png"> </a> <a href="https://sibkon.ru/"> <img src="/images/company/logo/сибкон.png"> </a> <a href="#"> <img src="/images/company/logo/угостинцы.png"> </a> <a href="http://24rls.ru/"> <img src="/images/company/logo/рлс.png"> </a>
            </p>
        </div>
    </div>
    <br>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>