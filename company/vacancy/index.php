<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Ярдекс – современный и удобный интернет магазин продуктов в Красноярске, где можно купить обычную еду и деликатесы. ️Бесплатная доставка по Красноярску. Звоните: 8 (391) 290-24-00.");
$APPLICATION->SetPageProperty("keywords", "компания, информация о вакансиях");
$APPLICATION->SetPageProperty("title", "Вакансии | Интернет магазин Ярдекс");
$APPLICATION->SetTitle("Вакансии");

$APPLICATION->AddHeadScript('https://www.gstatic.com/firebasejs/8.9.0/firebase-app.js');
$APPLICATION->AddHeadScript('https://www.gstatic.com/firebasejs/8.9.0/firebase-database.js');
$APPLICATION->AddHeadScript('/company/vacancy/vakancy.helper.js');

?>

<div class="vacancy-container">
    <h2>Список вакансий</h2>
    <div class="vacancy-list"></div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>