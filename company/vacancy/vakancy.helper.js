var firebaseConfig = {
    apiKey: 'AIzaSyALKRpGR2bJjJcDJyDXZr-hybeochoLVDc',
    authDomain: 'yaroshenko-vakansii.firebaseapp.com',
    databaseURL: 'https://yaroshenko-vakansii-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'yaroshenko-vakansii',
};

firebase.initializeApp(firebaseConfig);
const db = firebase.database();

const vakanciesListRef = db.ref('vakancies');
vakanciesListRef.on(
    'value',
    function (data) {
        var itemsBlock = document.querySelector('.vacancy-container .vacancy-list');

        var newHtml = '';

        data.forEach(function (vakancy) {
            var key = vakancy.key;
            var childData = vakancy.val();
            newHtml += `
            <div class="col-md-12 vacancy-card" id="${key}">
                <div class="vacancy-card__header">
                    <div class="title">${childData.name}</div>
                    <div class="pay"><b>Зарплата:</b> ${childData.cost}</div>    
                </div>
                <div class="vacancy-card__body">           
                    <div><b>Описание:</b>${childData.duties}</div>
                </div>
            </div>`;
        });

        itemsBlock.innerHTML = newHtml;
    },
    function (error) {
        console.error(error);
    }
);
