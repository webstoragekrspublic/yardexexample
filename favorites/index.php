<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");

use \Bitrix\Main\Loader;

?>

<? if (Loader::includeModule('custom.favoritesoncookies')): ?>
    <?
    $currentUserFavorites = \Custom\FavoritesOnCookies\CurrentUserFavorites::getInstance();

    $catalogIBlockID = \Helpers\Constants::PRODUCTS_IBLOCK_ID;
    $pageAllItems = false;

    if ($currentUserFavorites->isHaveItems()) {

        $pageAllItemsFilter = array(
            "IBLOCK_ID" => $catalogIBlockID,
            '=ID' => $currentUserFavorites->getItemIds()//false//$currentUserFavorites->getItemIds()
        );


        $pageAllItems = CNextCache::CIBLockElement_GetList(
            array(
                'CACHE' =>
                    array(
                        "MULTI" => "Y",
                        "TAG" => CNextCache::GetIBlockCacheTag($catalogIBlockID)
                    )
            ),
            $pageAllItemsFilter,
            false,
            false,
            array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID")
        );

        $arAllSections = $arSectionsID = $pageAllItemsId = array();
        $currentUserFavorites->setActualItemIds(array_column($pageAllItems, 'ID'));

        // пересчет товаров для быстрой доставки
        if (\Helpers\CustomTools::isFastDeliveryEnabled()){
            $pageAllItemsFilter['PROPERTY_'.\Helpers\Constants::PROP_FAST_DELIVERY] = \Helpers\Constants::PROP_FAST_DELIVERY_VALUE_EXPRESS;
            $pageAllItems = CNextCache::CIBLockElement_GetList(
                array(
                    'CACHE' =>
                        array(
                            "MULTI" => "Y",
                            "TAG" => CNextCache::GetIBlockCacheTag($catalogIBlockID)
                        )
                ),
                $pageAllItemsFilter,
                false,
                false,
                array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID")
            );
        }

    }
    ?>


    <? $isAjax = "N"; ?>
    <? if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest" && isset($_GET["ajax_get"]) && $_GET["ajax_get"] == "Y" || (isset($_GET["ajax_basket"]) && $_GET["ajax_basket"] == "Y")) {
        $isAjax = "Y";
    } ?>
    <? if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest" && isset($_GET["ajax_get_filter"]) && $_GET["ajax_get_filter"] == "Y") {
        $isJsonAjax = "Y";
        $ajaxJsonAnswer = [];
    } ?>
    <? global $arTheme, $arRegion; ?>
    <?

    $arParams["FILTER_NAME"] = "arrFilter";
    $arParams["LIST_OFFERS_FIELD_CODE"][] = "DETAIL_PAGE_URL";
    $arParams["AJAX_FILTER_CATALOG"] = "N";

    if ($pageAllItems) {
        foreach ($pageAllItems as $arItem) {
            $pageAllItemsId[$arItem["ID"]] = $arItem["ID"];
            if ($arItem["IBLOCK_SECTION_ID"]) {
                if (is_array($arItem["IBLOCK_SECTION_ID"])) {
                    foreach ($arItem["IBLOCK_SECTION_ID"] as $id) {
                        $arAllSections[$id]["COUNT"]++;
                        $arAllSections[$id]["ITEMS"][$arItem["ID"]] = $arItem["ID"];
                    }
                } else {
                    $arAllSections[$arItem["IBLOCK_SECTION_ID"]]["COUNT"]++;
                    $arAllSections[$arItem["IBLOCK_SECTION_ID"]]["ITEMS"][$arItem["ID"]] = $arItem["ID"];
                }
            }
        }

        if ($arAllSections) {
            $arSectionsID = array_keys($arAllSections);
        }
        ?>
        <? $setionIDRequest = (isset($_GET["section_id"]) && $_GET["section_id"] ? $_GET["section_id"] : 0); ?>


        <? $APPLICATION->IncludeComponent(
            "custom:empty_component",
            "catalog_sections_by_items",
            [
                'IBLOCK_ID' => $catalogIBlockID,
                'ITEMS_ID' => $pageAllItemsId,
                'PAGE_BASE_URL' => $APPLICATION->GetCurPage(),
                'USE_SECTION_CODE' => 'N',
                'SELECTED_SECTION_ID' => (int)$setionIDRequest,
            ],
            false
        );
        ?>

        <?
        $GLOBALS[$arParams["FILTER_NAME"]]['ID'] = array_column($pageAllItems, 'ID');
        $GLOBALS[$arParams["FILTER_NAME"]]['SECTION_GLOBAL_ACTIVE'] = 'Y';

        if ($setionIDRequest) {
            $GLOBALS[$arParams["FILTER_NAME"]][] = array("SECTION_ID" => $setionIDRequest);
        }
        ?>

        <? if ($isJsonAjax == 'Y') : ?>
            <? ob_start(); ?>
        <? endif; ?>

        <? $APPLICATION->IncludeComponent(
            "aspro:catalog.smart.filter",
            'main_compact_ajax_default',
            array(
                "IBLOCK_TYPE" => 'catalog1c77',
                "IBLOCK_ID" => $catalogIBlockID,
                "FILTER_NAME" => $arParams["FILTER_NAME"],
                "PRICE_CODE" => array(
                    0 => "Цены единицы",
                ),
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => '100000',
                "CACHE_NOTES" => "",
                "CACHE_GROUPS" => 'N',
                "SECTION_IDS" => ($setionIDRequest ? array($setionIDRequest) : $arSectionsID),
                "ELEMENT_IDS" => $pageAllItemsId,
                "SAVE_IN_SESSION" => "N",
                "XML_EXPORT" => "Y",
                "SECTION_TITLE" => "NAME",
                "HIDDEN_PROP" => array("BRAND"),
                "SECTION_DESCRIPTION" => "DESCRIPTION",
                "SHOW_HINTS" => $arParams["SHOW_HINTS"],
                'CONVERT_CURRENCY' => 'N',
                "INSTANT_RELOAD" => "Y",
                "SEF_MODE" => 'N',
                "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
                "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
                "HIDE_NOT_AVAILABLE" => 'N',
            ),
            $component
        );
        ?>

        <? if ($isJsonAjax == 'Y'): ?>
            <? $ajaxJsonAnswer['filtersContainer'] = ob_get_clean();; ?>
        <? endif; ?>

    <? } ?>
    <? if ($isAjax == "Y"): ?>
        <? $APPLICATION->RestartBuffer(); ?>
    <? endif; ?>
    <? if ($pageAllItems): ?>

        <? if ($isJsonAjax == "Y") : ?>
            <? ob_start(); ?>
        <? endif; ?>
        <? $APPLICATION->IncludeComponent(
            "custom:empty_component",
            "catalog_sort_block",
            [
                'SORT_PRICES' => $arParams['SORT_PRICES'],
            ],
            false
        ); ?>
        <?
        $templateData = \Helpers\GlobalStorage::get('catalog_sort_block_templateData');
        $sort = $templateData['sort'] ?? 'NAME';
        $sort_order = $templateData['sort_order'] ?? 'asc';
        ?>

        <? if ($isJsonAjax == "Y") : ?>
            <? $ajaxJsonAnswer['sortButtonsContainer'] = ob_get_clean(); ?>
        <? endif; ?>

        <div class="clearfix catalog compact inner_wrapper json_ajax" id="search_product_container">

            <? if ($isJsonAjax == 'Y') : ?>
                <? ob_start(); ?>
            <? endif; ?>

            <? if ($isAjax == "N") {
                $frame = new \Bitrix\Main\Page\FrameHelper("viewtype-brand-block");
                $frame->begin(); ?>
            <? } ?>

            <? if (isset($GLOBALS[$arParams["FILTER_NAME"]]["FACET_OPTIONS"]))
                unset($GLOBALS[$arParams["FILTER_NAME"]]["FACET_OPTIONS"]); ?>

            <? $arTransferParams = array(
                "SHOW_ABSENT" => $arParams["SHOW_ABSENT"],
                "HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
                "PRICE_CODE" => $arParams["PRICE_CODE"],
                "OFFER_TREE_PROPS" => $arParams["OFFER_TREE_PROPS"],
                "CACHE_TIME" => $arParams["CACHE_TIME"],
                "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                "LIST_OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
                "LIST_OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                "SHOW_DISCOUNT_TIME" => $arParams["SHOW_DISCOUNT_TIME"],
                "SHOW_COUNTER_LIST" => $arParams["SHOW_COUNTER_LIST"],
                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                "SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                "SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
                "SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"],
                "SHOW_DISCOUNT_PERCENT_NUMBER" => $arParams["SHOW_DISCOUNT_PERCENT_NUMBER"],
                "USE_REGION" => ($arRegion ? "Y" : "N"),
                "STORES" => $arParams["STORES"],
                "DEFAULT_COUNT" => $arParams["DEFAULT_COUNT"],
                //"BASKET_URL" => $arTheme["BASKET_PAGE_URL"]["VALUE"],
                "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                "PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
                "ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
                "SHOW_DISCOUNT_TIME_EACH_SKU" => $arParams["SHOW_DISCOUNT_TIME_EACH_SKU"],
                "SHOW_ARTICLE_SKU" => $arParams["SHOW_ARTICLE_SKU"],
                "OFFER_ADD_PICT_PROP" => $arParams["OFFER_ADD_PICT_PROP"],
                "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                "OFFER_SHOW_PREVIEW_PICTURE_PROPS" => $arParams["OFFER_SHOW_PREVIEW_PICTURE_PROPS"],
            ); ?>

            <div class="ajax_load <?= $display; ?> js_wrapper_items"
                 data-params='<?= str_replace('\'', '"', CUtil::PhpToJSObject($arTransferParams, false)) ?>'>
                <? if ($isAjax == "Y" && $isJsonAjax != "Y"): ?>
                    <? $APPLICATION->RestartBuffer(); ?>
                <? endif; ?>

                <? $GLOBALS[$arParams["FILTER_NAME"]]["ACTIVE"] = ''; ?>


                <? $APPLICATION->IncludeComponent(
                    "custom:catalog.section",
                    "catalog_block",
                    array(
                        "USE_REGION" => "N",
                        "STORES" => array(
                            0 => "",
                            1 => "",
                        ),
                        "SHOW_UNABLE_SKU_PROPS" => "N",
                        "ALT_TITLE_GET" => "NORMAL",
                        "IBLOCK_TYPE" => "catalog1c77",
                        "IBLOCK_ID" => $catalogIBlockID,
                        "SHOW_COUNTER_LIST" => "Y",
                        "SECTION_ID" => "",
                        "SECTION_CODE" => "",
                        "AJAX_REQUEST" => (($isAjax == "Y" && $isJsonAjax != "Y") ? "Y" : "N"),
                        "ELEMENT_SORT_FIELD" => $sort,
                        "ELEMENT_SORT_ORDER" => $sort_order,
                        "SHOW_DISCOUNT_TIME_EACH_SKU" => "N",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "FILTER_NAME" => $arParams["FILTER_NAME"],
                        "INCLUDE_SUBSECTIONS" => "Y",
                        "SHOW_ALL_WO_SECTION" => "Y",
                        "PAGE_ELEMENT_COUNT" => "40",
                        "DISPLAY_TYPE" => $display,
                        "TYPE_SKU" => $arTheme["TYPE_SKU"]["VALUE"],
                        "PROPERTY_CODE" => array(
                            0 => "MORE_PHOTO",
                            1 => "UNIT_BASE_ID",
                            2 => "UNIT_BASE_NAME",
                            3 => "POPUP_VIDEO",
                            4 => "CLASS_ABC",
                            5 => "UNIT_BLOCK_ID",
                            6 => "UNIT_BLOCK_K",
                            7 => "UNIT_BLOCK_NAME",
                            8 => "PODBORKI",
                            9 => "TIP_KASHI",
                            10 => "TORGOVAJAMARKA",
                            11 => "UNIT_ONE_ID",
                            12 => "UNIT_ONE_K",
                            13 => "UNIT_ONE_NAME",
                            14 => "vote_count",
                            15 => "rating",
                            16 => "vote_sum",
                            17 => "",
                        ),
                        "SHOW_ARTICLE_SKU" => "N",
                        "SHOW_MEASURE_WITH_RATIO" => "N",
                        "OFFERS_FIELD_CODE" => array(
                            0 => "ID",
                            1 => "NAME",
                            2 => "",
                        ),
                        "OFFERS_PROPERTY_CODE" => array(
                            0 => "SIZES",
                            1 => "COLOR_REF",
                            2 => "ARTICLE",
                            3 => "SIZES2",
                            4 => "",
                        ),
                        "OFFERS_SORT_FIELD" => $sort,
                        "OFFERS_SORT_ORDER" => $sort_order,
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "OFFER_TREE_PROPS" => array(
                            0 => "SIZES",
                            1 => "COLOR_REF",
                        ),
                        "OFFERS_LIMIT" => "10",
                        "SECTION_URL" => "",
                        "DETAIL_URL" => "",
                        "BASKET_URL" => $arTheme["BASKET_PAGE_URL"]["VALUE"],
                        "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                        "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                        "SET_LAST_MODIFIED" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "N",
                        "AJAX_OPTION_HISTORY" => "N",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "10000",
                        "CACHE_GROUPS" => "N",
                        "CACHE_FILTER" => "N",
                        "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                        "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                        "BROWSER_TITLE" => "-",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "HIDE_NOT_AVAILABLE" => "L",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "L",
                        "SET_TITLE" => "N",
                        "SET_STATUS_404" => "N",
                        "SHOW_404" => "N",
                        "MESSAGE_404" => "",
                        "FILE_404" => "",
                        "PRICE_CODE" => array(
                            0 => "Цены единицы",
                        ),
                        "USE_PRICE_COUNT" => "Y",
                        "PRICE_VAT_INCLUDE" => "Y",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "OFFERS_CART_PROPERTIES" => "",
                        "DISPLAY_TOP_PAGER" => "N",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "PAGER_TITLE" => "",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "main",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
                        "PAGER_SHOW_ALL" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "ADD_CHAIN_ITEM" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "N",
                        "SHOW_DISCOUNT_PERCENT_NUMBER" => "N",
                        "SHOW_DISCOUNT_TIME" => "Y",
                        "SHOW_OLD_PRICE" => "N",
                        "CONVERT_CURRENCY" => "N",
                        "DEFAULT_COUNT" => "1",
                        "OFFER_HIDE_NAME_PROPS" => "N",
                        "USE_MAIN_ELEMENT_SECTION" => "N",
                        "ADD_PROPERTIES_TO_BASKET" => "N",
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRODUCT_PROPERTIES" => "",
                        "SALE_STIKER" => "-",
                        "STIKERS_PROP" => "-",
                        "SHOW_RATING" => "N",
                        "DISPLAY_COMPARE" => "N",
                        "ADD_PICT_PROP" => "-",
                        "OFFER_SHOW_PREVIEW_PICTURE_PROPS" => "",
                        "COMPONENT_TEMPLATE" => "catalog_block",
                        "SECTION_USER_FIELDS" => array(
                            0 => "",
                            1 => "",
                        ),
                        "BACKGROUND_IMAGE" => "-",
                        "SEF_MODE" => "N",
                        "SET_BROWSER_TITLE" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "COMPATIBLE_MODE" => "Y",
                        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                        "LINE_ELEMENT_COUNT" => "4",
                        "SHOW_PRICE_COUNT" => "1"
                    ),
                    false
                ); ?>
                <? if ($isAjax == "Y" && $isJsonAjax != "Y"): ?>
                    <? die(); ?>
                <? endif; ?>
            </div>
            <? if ($isAjax != "Y"): ?>
                <? $frame->end(); ?>
            <? endif; ?>

            <? if ($isJsonAjax == 'Y') : ?>
                <? $ajaxJsonAnswer['productContainer'] = ob_get_clean(); ?>
            <? endif; ?>

        </div>
    <? else: ?>
        <? if ($isJsonAjax == 'Y') : ?>
            <? ob_start(); ?>
        <? endif; ?>

        <p>
            Добавьте товар в избранное
        </p>

        <? if ($isJsonAjax == 'Y') : ?>
            <? $ajaxJsonAnswer['productContainer'] = ob_get_clean(); ?>
        <? endif; ?>
    <? endif; ?>

    <? if ($isAjax == "Y" && $isJsonAjax != 'Y'): ?>
        <? die(); ?>
    <? endif; ?>

    <? if ($isJsonAjax == 'Y') : ?>
        <?
        ob_clean();
        die(json_encode($ajaxJsonAnswer));
        ?>
    <? endif; ?>

<? endif; ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>