<?
// обновляем свойство "Хит продаж"
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $DB;
//начало присвоения 0 свойству хит
set_time_limit(1000);
$testProduct = \CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => 114, '!PROPERTY_BESTSELLER'=>0),
    false,
    false,
    array('ID')
);
while ($catalogItem = $testProduct->fetch()){
    CIBlockElement::SetPropertyValuesEx($catalogItem['ID'], 114, ['BESTSELLER' => 0]);
}
//конец присвоения

//поиск самых продаваемых товаров
$lastmonth = mktime(0, 0, 0, date("m")-6, date("d"),   date("Y"));
$dateMonthBefore = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), $lastmonth);

$parameters = [
    'select' => ['ID', 'USER_ID', 'DATE_INSERT', 'DATE_PAYED'],
    'filter' => ['>=DATE_PAYED' => $dateMonthBefore],
    'order' => ['USER_ID' => 'ASC', 'DATE_INSERT' => 'ASC']
];
$orders = \Bitrix\Sale\Order::getList($parameters);
$countProducts = [];
while ($item = $orders->fetch())
{
    $basket = \Bitrix\Sale\Order::load($item['ID'])->getBasket();
    $basketItems = $basket->getBasketItems();;
    foreach ($basketItems as $product){
        $countProducts[$product->getField('PRODUCT_ID')] += $product->getQuantity();
    }
}
arsort($countProducts);

$count = 0;
foreach ($countProducts as $hitProductKey => $hitProductValue){
    if ($count < 41){
        CIBlockElement::SetPropertyValuesEx($hitProductKey, 114, ['BESTSELLER' => 1]);
        pr($hitProductKey);
    }
    else if ($count > 40 && $count < 101){ //стикеры которые должны отображаться с хитом, но не в подборке
        CIBlockElement::SetPropertyValuesEx($hitProductKey, 114, ['BESTSELLER' => 2]);
        pr($hitProductKey);
    }
    else{break;}
    $count++;
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>