<?
// обновляем свойство "SORT" для приоритетной выдачи в поиске
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
global $DB;
$defaultSort = 500;
$el = new CIBlockElement;

set_time_limit(1000);
$resetProductQ = \CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => 114, '!SORT' => $defaultSort),
    false,
    false,
    array('ID', 'SORT')
);
while ($resetProduct = $resetProductQ->fetch()) {
    $arLoadProductArray = array(
        "SORT" => $defaultSort,
    );
    $res = $el->Update($resetProduct['ID'], $arLoadProductArray);
}

//поиск самых продаваемых товаров
$lastmonth = mktime(0, 0, 0, date("m") - 6, date("d"), date("Y"));
$dateMonthBefore = date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), $lastmonth);

$parameters = [
    'select' => ['ID', 'USER_ID', 'DATE_INSERT', 'DATE_PAYED'],
    'filter' => ['>=DATE_PAYED' => $dateMonthBefore],
    'order' => ['USER_ID' => 'ASC', 'DATE_INSERT' => 'ASC']
];

$orders = \Bitrix\Sale\Order::getList($parameters);
$countProducts = [];
while ($item = $orders->fetch()) {
    $basket = \Bitrix\Sale\Order::load($item['ID'])->getBasket();
    $basketItems = $basket->getBasketItems();;
    foreach ($basketItems as $product) {
        $countProducts[$product->getField('PRODUCT_ID')] += $product->getQuantity();
    }
}

foreach ($countProducts as $productId => $countBuy) {
    $newSort = $defaultSort - 100 - $countBuy;
    if ($newSort < 1) {
        $newSort = 1;
    }

    $arLoadProductArray = array(
        "SORT" => $newSort,
    );

    $res = $el->Update($productId, $arLoadProductArray);
    var_dump([$productId,$arLoadProductArray]);
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>