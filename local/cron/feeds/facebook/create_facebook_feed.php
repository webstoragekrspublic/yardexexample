<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$productsIBlockId = 114;
$allTorgovajaMarka;
setIndexedTorgovajaMarka();

$csvFilePath = __DIR__ . '/facebook_feed.csv';
$csvDelimiter = ',';
$fp = fopen($csvFilePath, 'w');

$columns = [
    'ID' => 'id',
    'NAME' => 'title',
    'DETAIL_TEXT' => 'description',
    'PROPERTY_TORGOVAJAMARKA' => 'brand',
    'availability' => 'availability',
    'condition' => 'condition',
    'DETAIL_PAGE_URL' => 'link',
    'DETAIL_PICTURE' => 'image_link',
    'price' => 'price'
];
fputcsv($fp, $columns, $csvDelimiter);

$filter = [
    "ACTIVE" => 'Y',
    "IBLOCK_ID" => $productsIBlockId,
];

$productsQ = CIBlockElement::getList(
    ['PROPERTY_TORGOVAJAMARKA' => 'ASC', 'ID' => 'ASC'],
    $filter,
    false,
    false,
    array_keys($columns)
);

$columns['availability'] = 'in stock';
$columns['condition'] = 'new';

$productInfoOut = $columns;
while ($productInfo = $productsQ->GetNext()) {
    $productInfoOut['price'] = CCatalogProduct::GetOptimalPrice($productInfo['ID'])['RESULT_PRICE']['DISCOUNT_PRICE'] . ' RUB';
    foreach ($productInfo as $fieldName => $value) {
        if (substr($fieldName, -9) == '_VALUE_ID') {
            unset($productInfo[$fieldName]);
        } else if ($fieldName == 'PROPERTY_TORGOVAJAMARKA_VALUE') {
            $productInfo[$fieldName] = getTorgovajaMarkaName($value);
            $productInfoOut['PROPERTY_TORGOVAJAMARKA'] = $productInfo[$fieldName];
        }
        if (array_key_exists($fieldName, $columns)) {
            if ($fieldName == 'DETAIL_PAGE_URL') {
                $productInfoOut[$fieldName] =  'https://' . SITE_SERVER_NAME . $productInfo[$fieldName];
            } elseif ($fieldName == 'DETAIL_PICTURE') {
                if ($productInfo[$fieldName] == null) {
                    $productInfoOut[$fieldName] = 'https://' . SITE_SERVER_NAME . '/local/templates/aspro_next_custom/images/no_photo_medium.png';
                } else {
                    $productInfoOut[$fieldName] = 'https://' . SITE_SERVER_NAME . CFile::GetPath($productInfo[$fieldName]);
                }
            } elseif ($fieldName == 'DETAIL_TEXT' && $productInfo[$fieldName] == null) {
                $productInfoOut[$fieldName] = 'без описания';
            } else {
                $productInfoOut[$fieldName] = $productInfo[$fieldName];
            }
        }
    }
    fputcsv($fp, $productInfoOut, $csvDelimiter);
}

fclose($fp);



function setIndexedTorgovajaMarka()
{
    global $allTorgovajaMarka;
    $torgovajaMarkaIBlockId = 118;
    $columns = ['ID', 'NAME'];
    $filter = [
        "IBLOCK_ID" => $torgovajaMarkaIBlockId
    ];


    $tmQ = CIBlockElement::getList(
        ['DATE_CREATE' => 'ASC', 'ID' => 'ASC'],
        $filter,
        false,
        false,
        $columns
    );

    while ($tm = $tmQ->fetch()) {
        $allTorgovajaMarka[$tm['ID']] = $tm;
    }
}

function getTorgovajaMarkaName($tmId)
{
    global $allTorgovajaMarka;
    return $allTorgovajaMarka[$tmId]['NAME'] ?? 'Не заполнено';
}

function convertFileToWin1251($filePath)
{
    $data = file_get_contents($filePath);
    $win1251data = mb_convert_encoding($data, "Windows-1251", "UTF-8");
    file_put_contents($filePath, $win1251data);
}
