<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$productsIBlockId = 114;

$csvFilePath = __DIR__ . '/bad_feed.csv';
$csvDelimiter = ',';
$fp = fopen($csvFilePath, 'w');

$columns = [
    'ID' => 'id',
    'NAME' => 'название',
    'DETAIL_TEXT' => 'описание',
    'DETAIL_PICTURE' => 'изображение',
    'DETAIL_PAGE_URL' => 'ссылка',
];
fputcsv($fp, $columns, $csvDelimiter);

$filter = [
    "ACTIVE" => 'Y',
    "IBLOCK_ID" => $productsIBlockId,
];

$productsQ = CIBlockElement::getList(
    ['ID' => 'ASC'],
    $filter,
    false,
    false,
    array_keys($columns)
);

$productInfoOut = $columns;
while ($productInfo = $productsQ->GetNext()) {
    if ($productInfo['DETAIL_PICTURE'] != null || $productInfo['DETAIL_TEXT'] != null) {
        continue;
    }

    foreach ($productInfo as $fieldName => $value) {
        if (substr($fieldName, -9) == '_VALUE_ID') {
            unset($productInfo[$fieldName]);
        }
        if (array_key_exists($fieldName, $columns)) {
            if ($fieldName == 'DETAIL_PAGE_URL') {
                $productInfoOut[$fieldName] =  'https://' . SITE_SERVER_NAME . $productInfo[$fieldName];
            } elseif ($fieldName == 'DETAIL_PICTURE') {
                if ($productInfo[$fieldName] == null) {
                    $productInfoOut[$fieldName] = 'нет изображения';
                } else {
                    $productInfoOut[$fieldName] = 'https://' . SITE_SERVER_NAME . CFile::GetPath($productInfo[$fieldName]);
                }
            } elseif ($fieldName == 'DETAIL_TEXT' && $productInfo[$fieldName] == null) {
                $productInfoOut[$fieldName] = 'нет описания';
            } else {
                $productInfoOut[$fieldName] = $productInfo[$fieldName];
            }
        }
    }
    fputcsv($fp, $productInfoOut, $csvDelimiter);
}

fclose($fp);
convertFileToWin1251($csvFilePath);

function convertFileToWin1251($filePath)
{
    $data = file_get_contents($filePath);
    $win1251data = mb_convert_encoding($data, "Windows-1251", "UTF-8");
    file_put_contents($filePath, $win1251data);
}
