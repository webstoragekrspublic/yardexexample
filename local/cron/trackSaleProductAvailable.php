<?

use Handlers\ActualSalesTrack\SaleTracker;
use Handlers\ActualSalesTrack\SaleSwitcher;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

SaleSwitcher::switchSaleActiveByProductsAvailable();

///////////////////////////////////////////
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
