<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$blackThursday = 43207;//баннер "черный четверг"
$freeSaleToday = 43901;//баннер бесплатная "доставка сегодня"
$blackFriday = 2804;//правило корзины "Черная пятница"
$iblockId = 124;
$dateFormat = 'd.m.Y H:i:s';
$date = new \Bitrix\Main\Type\DateTime;
$dbItems = \Bitrix\Iblock\ElementTable::getList(
    [
        'select' => ['ID', 'ACTIVE_FROM', 'ACTIVE_TO', 'NAME'],
        'order' => ['SORT' => 'ASC'],
        'filter' => ['IBLOCK_ID' => $iblockId, 'ID' => [$blackThursday,$freeSaleToday]]
    ]
);
while ($banner = $dbItems->fetch()){
    if ($banner["ID"] == $blackThursday){
        $update = add7DaysAndBannerUpdate($blackThursday, $date, $banner);
        if ($update){echo $banner['NAME'] . ' (обновлен)\n';}
    }
    elseif ($banner["ID"] == $freeSaleToday){
        $update = add7DaysAndBannerUpdate($freeSaleToday, $date, $banner);
        if ($update){echo $banner['NAME'] . ' (обновлен)\n';}
    }
}
$dbItems = \Bitrix\Sale\Internals\DiscountTable::getList([
    'select' => ['ID', 'ACTIVE_FROM', 'ACTIVE_TO'],
    'filter' => ['ID' => $blackFriday]
]);
$saleDiscount = $dbItems->fetch();
if ($saleDiscount){
    $update = add7DaysAndBasketRulesUpdate ($saleDiscount, $date);
    if ($update and $update->isSuccess()){echo 'Правило корзины ' . $update->getId() . ' (обновлено)\n';}
}
//Обновляем Даты в инфоблоках
function add7DaysAndBannerUpdate ($idBanner, $date, $banner){
    if ($date->getTimestamp() > $banner['ACTIVE_TO']->getTimestamp()){
        $dateFrom = $banner['ACTIVE_FROM']->add('7 day');
        $dateTo = $banner['ACTIVE_TO']->add('7 day');
        $el = new CIBlockElement;
        $res = $el->Update($idBanner,
            ['ACTIVE_FROM' => $dateFrom, 'ACTIVE_TO' => $dateTo]
        );
        return $res;
    }
}
//Обновляем Даты в правилах корзины
function add7DaysAndBasketRulesUpdate ($saleDiscount, $date){
    if ($date->getTimestamp() > $saleDiscount['ACTIVE_TO']->getTimestamp()){
        $dateFrom = $saleDiscount['ACTIVE_FROM']->add('7 day');
        $dateTo = $saleDiscount['ACTIVE_TO']->add('7 day');
        $res = \Bitrix\Sale\Internals\DiscountTable::Update(
            $saleDiscount['ID'],
            ['ACTIVE_FROM' => $dateFrom, 'ACTIVE_TO' => $dateTo]
        );
        return $res;
    }
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>