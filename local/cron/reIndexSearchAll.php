<?
//переиндексируем поиск полностью, нужен после добавления новых словоформ.
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
set_time_limit(1000);
$totalCnt = 0;
if (CModule::IncludeModule("search")) {
    $res = [];
    do {
        $res = CSearch::ReIndexAll(true, 60, $res);
        $totalCnt = $res['CNT'];
        echo "$totalCnt <br>\r\n";
    } while (is_array($res));
}
echo 'обновлено '+$totalCnt;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
