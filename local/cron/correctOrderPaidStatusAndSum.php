<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


use \Bitrix\Main\Loader;
use \Custom\OrderPaycheck\OrderPaycheckPull;

if (Loader::includeModule('custom.orderpaycheck')) {
    $pull = OrderPaycheckPull::getInstance();
    $pull->checkPullAndCorrectPaidStatus();
    $pull->removeIsPaidOrdersFromPull();
    die('статусы обновлены');
}
