<?
//Выставляем свойство "Новинка"
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$productLimit = 50;
$iblockId = 114;
$YvalueId = '475';


//текущие товары с флагом "Новинка"
$oldNewProducts = [];
$filter = [
    'PROPERTY_NEW_VALUE' => 'Y',
    'IBLOCK_ID' => $iblockId
];
$productsQ = CIBlockElement::getList(
    [],
    $filter,
    false,
    false,
    ['ID', 'PROPERTY_NEW', 'NAME', 'DATE_CREATE']
);
while ($productInfo = $productsQ->fetch()) {
    $oldNewProducts[$productInfo['ID']] = $productInfo;
}
////////////////////////////////

//Выставляем новинки
$filter = [
    '!PREVIEW_PICTURE' => false,
    'IBLOCK_ID' => $iblockId,
    'ACTIVE' => 'Y'
];
$productsQ = CIBlockElement::getList(
    ['DATE_CREATE' => 'DESC'],
    $filter,
    false,
    array("nPageSize" => $productLimit),
    ['ID', 'PROPERTY_NEW', 'DATE_CREATE', 'PREVIEW_PICTURE', 'NAME']
);
while ($productInfo = $productsQ->fetch()) {
    if ($productInfo['PROPERTY_NEW_VALUE'] !== 'Y') {
        CIBlockElement::SetPropertyValuesEx($productInfo['ID'], $iblockId, array('NEW' => $YvalueId));
        echo $productInfo['ID'] . ' ' . $productInfo['NAME'] . ' (' . $productInfo['DATE_CREATE'] . ') - добавлен - <br>';
    }
    if (isset($oldNewProducts[$productInfo['ID']])) {
        unset($oldNewProducts[$productInfo['ID']]);
    }
}
////////////////////////////////

//снимаем флаг для старых товаров////////////////
if ($oldNewProducts) {
    foreach ($oldNewProducts as $oldNewProduct) {
        echo $oldNewProduct['ID'] . ' - убран<br>';
        CIBlockElement::SetPropertyValuesEx($oldNewProduct['ID'], $iblockId, array('NEW' => false));
    }
}
////////////////////////////////

echo 'uspeh';

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");