<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\ExternalApi\B24CRM\B24CRM::updateUsersInCrm();
\ExternalApi\B24CRM\B24CRM::updateOrdersInCrm();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
