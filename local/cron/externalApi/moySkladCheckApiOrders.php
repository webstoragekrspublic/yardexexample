<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use ExternalApi\MoySklad\MoySklad;
MoySklad::checkAndCarryFinishedOrdersInApi();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");