<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Handlers\ProductDiscount\ProductDiscountController;

ProductDiscountController::unMarkOutdateDiscountProducts();
ProductDiscountController::markAllDiscountProducts();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
