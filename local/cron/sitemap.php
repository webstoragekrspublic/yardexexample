<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;
$USER->Authorize(24);//cron_bot

$_REQUEST['ID'] = '1';
$_REQUEST['NS'] = '';
$_REQUEST['action'] = 'sitemap_run';
$_REQUEST['lang'] = 'ru';
$_REQUEST['pid'] = '';
$_REQUEST['sessid'] = bitrix_sessid();
$_REQUEST['value'] = '0';

$step = 0;
do {
    $step++;
    ob_start();
//    require $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin/seo_sitemap_run.php";
    require $_SERVER["DOCUMENT_ROOT"] . "/local/cron/seo_sitemap_run_modified.php";

    $html = ob_get_clean();

    if (preg_match('/runSitemap\((.*)\)/', $html, $matches)) {
        $nextRequestParams = json_decode('[' . str_replace("'", '"', $matches[1]) . ']', true);
        echo $nextRequestParams[1]."%<br>\r\n";
        $_REQUEST['value'] = $nextRequestParams[1];
        $_REQUEST['pid'] = $nextRequestParams[2];
        $_REQUEST['NS'] = $nextRequestParams[3];
    } elseif (preg_match('/finishSitemap\((.*)\)/', $html)) {
        echo 'успех!';
        break;
    } else {
        echo 'Ошибка! нет данных!';
        break;
    }

} while ($step < 100);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");