<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$productsIBlockId = 114;
$allTorgovajaMarka;
setIndexedTorgovajaMarka();

$timestampFromFilePath = __DIR__ . '/timestampFrom.txt';
//file_put_contents($timestampFromFilePath, time() - 60 * 60 * 24 * 7);
$timestampFrom = (int)file_get_contents($timestampFromFilePath);
$timestampTo = time();
$dateFilterFormat = 'd.m.Y H:i:s';

$DATE_CREATE_FROM = date($dateFilterFormat, $timestampFrom);
$DATE_CREATE_TO = date($dateFilterFormat, $timestampTo);

$csvFilePath = __DIR__ . '/send.csv';
$csvDelimiter = ';';
$fp = fopen($csvFilePath, 'w');


$columns = [
    'ID' => 'Ид',
    'NAME' => 'Имя товара',
    'PROPERTY_TORGOVAJAMARKA' => 'Торговая марка',
    'DATE_CREATE' => 'Дата создания',
    'DETAIL_PAGE_URL' => 'Ссылка на товар',
    'DETAIL_PICTURE' => 'Ссылка на картинку'
];
fputcsv($fp, $columns, $csvDelimiter);

$filter = [
    "IBLOCK_ID" => $productsIBlockId,
    ">DATE_CREATE" => $DATE_CREATE_FROM,
    "<=DATE_CREATE" => $DATE_CREATE_TO
];

$productsQ = CIBlockElement::getList(
    ['PROPERTY_TORGOVAJAMARKA' => 'ASC', 'ID' => 'ASC'],
    $filter,
    false,
    false,
    array_keys($columns)
);
$productInfoOut = $columns;
while ($productInfo = $productsQ->GetNext()) {
    foreach ($productInfo as $fieldName => $value) {
        if (substr($fieldName, -9) == '_VALUE_ID') {
            unset($productInfo[$fieldName]);
        } else if ($fieldName == 'PROPERTY_TORGOVAJAMARKA_VALUE') {
            $productInfo[$fieldName] = getTorgovajaMarkaName($value);
            $productInfoOut['PROPERTY_TORGOVAJAMARKA'] = $productInfo[$fieldName];
        }
        if (array_key_exists($fieldName, $columns)) {
            if ($fieldName == 'DETAIL_PAGE_URL'){
                $productInfoOut[$fieldName] =  SITE_SERVER_NAME . $productInfo[$fieldName];
            }
            elseif ($fieldName == 'DETAIL_PICTURE')
            {
                if ($productInfo[$fieldName] == null){
                    $productInfoOut[$fieldName] = 'нет картинки';}
                else {
                    $productInfoOut[$fieldName] = SITE_SERVER_NAME . CFile::GetPath($productInfo[$fieldName]);
                }
            }
            else {
                $productInfoOut[$fieldName] = $productInfo[$fieldName];
            }
        }
    }
    fputcsv($fp, $productInfoOut, $csvDelimiter);
}

fclose($fp);

convertFileToWin1251($csvFilePath);

$sendFiles = [$csvFilePath];
$sendProps = [
    'DATE_FROM' => $DATE_CREATE_FROM,
    'DATE_TO' => $DATE_CREATE_TO,
];
$messageId = '101';
$sended = \Helpers\CustomTools::sendServicesEmail($sendProps, $messageId, $sendFiles);
if ($sended) {
    file_put_contents($timestampFromFilePath, $timestampTo);
}
echo $sended;


function setIndexedTorgovajaMarka() {
    global $allTorgovajaMarka;
    $torgovajaMarkaIBlockId = 118;
    $columns = ['ID', 'NAME'];
    $filter = [
        "IBLOCK_ID" => $torgovajaMarkaIBlockId
    ];


    $tmQ = CIBlockElement::getList(
        ['DATE_CREATE' => 'ASC', 'ID' => 'ASC'],
        $filter,
        false,
        false,
        $columns
    );

    while ($tm = $tmQ->fetch()) {
        $allTorgovajaMarka[$tm['ID']] = $tm;
    }
}

function getTorgovajaMarkaName($tmId) {
    global $allTorgovajaMarka;
    return $allTorgovajaMarka[$tmId]['NAME'] ?? 'Не заполнено';
}

function convertFileToWin1251($filePath) {
    $data = file_get_contents($filePath);
    $win1251data = mb_convert_encoding($data, "Windows-1251", "UTF-8");
    file_put_contents($filePath,$win1251data);
}
