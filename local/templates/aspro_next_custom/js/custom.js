$('html').on('click', 'a.slow_slide', function (e) {
    e.preventDefault();
    var fixed_offset = 100;
    var hash = this.hash.replace(/(:|\.|\[|\]|,|=|@|%)/g, '\\$1');
    $('html,body')
        .stop()
        .animate({ scrollTop: $(hash).offset().top - fixed_offset }, 1000);
});

BX.ready(function () {
    customLogictimBonusOrder();
});

function customLogictimBonusOrder(type) {
    if (!BX.Sale || BX.Sale.OrderAjaxComponent == undefined) return;

    if (!('result' in BX.Sale.OrderAjaxComponent) || !('LOGICTIM_BONUS' in BX.Sale.OrderAjaxComponent.result)) return;

    var result = BX.Sale.OrderAjaxComponent.result.LOGICTIM_BONUS;

    if (BX('ORDER_PROP_' + result.ORDER_PROP_PAYMENT_BONUS_ID)) return;

    /* Dobavlyaem blok s polem dlya oplati */
    var payment_block = BX.create('DIV', {
        props: { id: 'bonus_payment_block', className: 'bx-soa-section' },
    });
    var payment_title = BX.create('DIV', {
        attrs: { className: 'bx-soa-section-title-container' },
        html:
            '<h2 class="bx-soa-section-title col-sm-9"><span class="bx-soa-section-title-count"></span>' +
            result.MODULE_LANG.TEXT_BONUS_FOR_PAYMENT +
            '</h2>',
    });
    payment_block.appendChild(payment_title);
    var payment_block_content = BX.create('DIV', {
        props: { className: 'bx-soa-section-content' },
    });
    payment_block.appendChild(payment_block_content);

    var comment_block = BX.create('DIV', {
        props: { className: 'bonus_comment' },
        children: [
            BX.create('strong', {
                text: result.MODULE_LANG.HAVE_BONUS_TEXT + ' ' + result.USER_BONUS,
            }),
        ],
    });
    if (parseFloat(result.MIN_BONUS) > 0)
        comment_block.appendChild(
            BX.create('span', { html: '<br />' + result.MODULE_LANG.MIN_BONUS_TEXT })
        );
    if (
        parseFloat(result.MAX_BONUS) > 0 &&
        parseFloat(result.USER_BONUS) >= parseFloat(result.MIN_BONUS)
    )
        comment_block.appendChild(
            BX.create('span', { html: '<br />' + result.MODULE_LANG.MAX_BONUS_TEXT })
        );

    comment_block.appendChild(BX.create('div', { html: '<br />' }));
    if (parseFloat(result.USER_BONUS) > 0) payment_block_content.appendChild(comment_block);

    var pay_field_block = BX.create('DIV', {
        props: { id: 'bonus_payfield_block' },
        children: [BX.create('strong', { text: result.MODULE_LANG.PAY_BONUS_TEXT })],
    });

    if (type == 'ajax') {
        var logictimPayBonus = result.PAY_BONUS;
        if (result.INPUT_BONUS == '-' || result.INPUT_BONUS == '00') var logictimPayBonus = '0';
    } else var logictimPayBonus = result.PAY_BONUS;

    var input_field_block = BX.create('DIV', { props: { className: 'bx-soa-coupon-input' } });
    var input_field = BX.create('input', {
        attrs: {
            type: 'text',
            id: 'paybonus_input',
            className: 'form-control bx-ios-fix',
            onchange: 'updateBonusField();',
            value: logictimPayBonus,
            name: 'ORDER_PROP_' + result.ORDER_PROP_PAYMENT_BONUS_ID,
        },
    });
    if (
        logictimPayBonus > 0 &&
        parseFloat(result.USER_BONUS) >= parseFloat(result.MIN_BONUS) &&
        result.DISCOUNT_TO_PRODUCTS != 'B'
    ) {
        input_field_block.appendChild(input_field);
        pay_field_block.appendChild(input_field_block);
        payment_block_content.appendChild(pay_field_block);
    }

    BX.remove(BX('bonus_payment_block'));

    //if(parseFloat(result.PAY_BONUS) > 0 || type == 'ajax' && result.PAY_BONUS != undefined)
    if (parseFloat(result.USER_BONUS) > 0 && parseFloat(result.MAX_BONUS) > 0) {
        //$(payment_block).insertAfter($(".bx-soa-section.bx-active:last")); metod jquery
        var last_block = document.querySelectorAll('.bx-soa-section.bx-active');
        last_block = last_block[last_block.length - 1];
        BX.insertAfter(payment_block, last_block);
    }

    //BX.insertAfter(payment_block, BX('bx-soa-orderSave'));
    /*if(BX('bx-soa-paysystem') && BX.isNodeHidden(BX('bx-soa-paysystem')) != true)
		$(payment_block).insertAfter($("#bx-soa-paysystem"));
	else*/

    /* Dobavlyaem blok s polem dlya oplati */

    /* Dobavlyaem infu - ckolkimi bonusami oplatili zakaz */
    // if (logictimPayBonus > 0 && result.DISCOUNT_TO_PRODUCTS != 'B') {
    //     BX.remove(BX('bonus_pay_block'));
    //     var addPay_info_block = BX.create('DIV', {
    //         attrs: { className: 'bx-soa-cart-total-line', id: 'bonus_pay_block' },
    //         html:
    //             '<div id="bonus_pay_sum"><span class="bx-soa-cart-t">' +
    //             result.TEXT_BONUS_PAY +
    //             '</span><span class="bx-soa-cart-d">' +
    //             logictimPayBonus +
    //             '</span></div>',
    //     });
    //     var before_itog_block = BX.findPreviousSibling(itog_block, {
    //         tag: 'div',
    //         class: 'bx-soa-cart-total-line',
    //     });
    //     BX.insertAfter(addPay_info_block, before_itog_block);
    //
    //     //for mobile
    //     BX.remove(BX('bonus_pay_block_mobile'));
    //     var addPay_info_block_mobile = BX.create('DIV', {
    //         attrs: { className: 'bx-soa-cart-total-line', id: 'bonus_pay_block_mobile' },
    //         html:
    //             '<div id="bonus_pay_sum_mobile"><span class="bx-soa-cart-t">' +
    //             result.TEXT_BONUS_PAY +
    //             '</span><span class="bx-soa-cart-d">' +
    //             logictimPayBonus +
    //             '</span></div>',
    //     });
    //     var before_itog_block_mobile = BX.findPreviousSibling(itog_block_mobile, {
    //         tag: 'div',
    //         class: 'bx-soa-cart-total-line',
    //     });
    //     BX.insertAfter(addPay_info_block_mobile, before_itog_block_mobile);
    // }
    /* Dobavlyaem infu - ckolkimi bonusami oplatili zakaz */

    //console.log(result);
}
$(document).ready(function(){
   /*
    const lazyLoad = (picture) => {
    const img = picture.querySelector('img');
     if(img.getAttribute('data-visible') != 1){
           const sources = picture.querySelectorAll('source');

           sources.forEach((source) => {
             source.srcset = source.dataset.srcset;
             source.removeAttribute('data-srcset');
           });
           img.src = img.dataset.src;
           img.setAttribute('data-visible', 1);
           img.removeAttribute('data-src');
       }
    }
    const imgObserver = new IntersectionObserver((entries, self) => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          lazyLoad(entry.target);
          self.unobserve(entry.target);
        }
      });
    });
    

    var Visible = function (target) {
      // Все позиции элемента
      var targetPosition = {
          top: window.pageYOffset + target.getBoundingClientRect().top,
          left: window.pageXOffset + target.getBoundingClientRect().left,
          right: window.pageXOffset + target.getBoundingClientRect().right,
          bottom: window.pageYOffset + target.getBoundingClientRect().bottom
        },
        // Получаем позиции окна
        windowPosition = {
          top: window.pageYOffset,
          left: window.pageXOffset,
          right: window.pageXOffset + document.documentElement.clientWidth,
          bottom: window.pageYOffset + document.documentElement.clientHeight
        };

      if (targetPosition.bottom > windowPosition.top && // Если позиция нижней части элемента больше позиции верхней чайти окна, то элемент виден сверху
        targetPosition.top < windowPosition.bottom && // Если позиция верхней части элемента меньше позиции нижней чайти окна, то элемент виден снизу
        targetPosition.right > windowPosition.left && // Если позиция правой стороны элемента больше позиции левой части окна, то элемент виден слева
        targetPosition.left < windowPosition.right) { // Если позиция левой стороны элемента меньше позиции правой чайти окна, то элемент виден справа
        // Если элемент полностью видно, то запускаем следующий код
        console.clear();
        imgObserver.observe(target);
      } else {
        // Если элемент не видно, то запускаем этот код
        console.clear();
      };
    };

    // Запускаем функцию при прокрутке страницы
    window.addEventListener('scroll', function() {
        // Получаем нужный элемент
        var elements = document.querySelectorAll('.lazy-picture');
        elements.forEach(element => Visible (element));
    });
     * 
    */
});

function setCookieAgeRestriction16Pluse(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; path=/;" + expires;

    var btns = document.getElementsByClassName(cname+'_btn_off');
    for (var i=0, max=btns.length; i < max; i++) {
        btns[i].style.display = 'none';
    }
    var btns = document.getElementsByClassName(cname+'_btn_on');
    for (var i=0, max=btns.length; i < max; i++) {
        btns[i].style.display = 'block';
    }
    var btns = document.getElementsByClassName('image_blure_custom');
    for (var i=0, max=btns.length; i < max; i++) {
        btns[i].style.filter = 'blur(0px)';
    }
    var btns = document.getElementsByClassName('text_blure_custom');
    for (var i=0, max=btns.length; i < max; i++) {
        btns[i].style.filter = 'blur(0px)';
    }
}