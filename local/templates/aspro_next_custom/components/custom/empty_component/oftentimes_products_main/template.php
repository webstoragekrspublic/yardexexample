<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use \Bitrix\Main\Loader;

?>
<? if (Loader::includeModule('custom.oftentimesproducts')) : ?>
    <? if ($USER->IsAuthorized() && !empty($arResult['ITEMS'])) { ?>

        <div class='podborki-ajax-sliders-loading ya_lublu_podborki'>

        </div>
        <script>
            $.ajax({
                type: "POST",
                url: '<?= $templateFolder . '/ajax.php' ?>',
                data: <?= json_encode(['arResult' => $arResult]) ?>,
                success: function(response) {
                    $response = $(response);

                    $('.ya_lublu_podborki').before($response);

                    $('.ya_lublu_podborki').remove();

                }
            });
        </script>

    <? } ?>
<? endif; ?>