<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arResult */

/** @var array $arParams */

use Bitrix\Main\Loader;
use Custom\OftentimesProducts\OftentimesProductsHelper;


if (Loader::includeModule('custom.oftentimesproducts')) {
    $otpHelper = OftentimesProductsHelper::getInstance();
    $curUserOTPs = $otpHelper->loadCurUserOftentimeProducts();

    $arResult['ITEMS'] = $curUserOTPs;
    $arResult['TITLE'] = 'Я ЛЮБЛЮ!';
    $arResult['LIMIT'] = OftentimesProductsHelper::$limit;
    $arResult['ITEMS'] = ['ID' => array_keys($arResult['ITEMS'])];
}
