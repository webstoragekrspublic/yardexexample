<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div class="messengers-links">
    <div class="messengers-links-title">
        Товары по 1 рублю, промокоды на продукты и скидки до 3000 рублей!
    </div>
    <div class="messengers-links-text">
        Выбирайте удобный мессенджер и первым узнавайте о скидках,<br> получайте промо-коды, а также участвуйте в акциях товары по 1 рублю.
    </div>
    <div class="messengers-links-container">
        <div class="messengers-link vk">
            <a href="https://vk.com/app6379730_-118900144#l=5&ml=5&auto=1">
                <img src="<?= SITE_DIR . 'images/messengers_icons/vk-50.png' ?>" alt="">
                <span>Вконтакте</span>
            </a>
        </div>
        <div class="messengers-link viber">
            <a href="viber://pa?chatURI=yardexbot&context=ml5">
                <img src="<?= SITE_DIR . 'images/messengers_icons/viber-50.png' ?>" alt="">
                <span>Viber</span>
            </a>
        </div>
        <div class="messengers-link telegram">
            <a href="tg://resolve?domain=Yardexbot&start=ml5">
                <img src="<?= SITE_DIR . 'images/messengers_icons/telegram-50.png' ?>" alt="">
                <span>Telegram</span>
            </a>
        </div>
    </div>
</div>