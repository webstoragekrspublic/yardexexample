<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Catalog\PriceTable;
use Bitrix\Mail\Integration\Main\UISelector\Handler;

/** @var array $arResult */

/** @var array $arParams */
