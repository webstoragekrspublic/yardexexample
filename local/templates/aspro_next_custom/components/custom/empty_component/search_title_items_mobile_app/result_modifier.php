<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Catalog\PriceTable;
use Bitrix\Mail\Integration\Main\UISelector\Handler;

/** @var array $arResult */

/** @var array $arParams */

$arResult = [
    'ITEMS' => [],
    'SECTIONS' => []
];
if (CModule::IncludeModule('search')) {
    $iBlockId = 114;
    $limit = 10;
    $catalogGroupPriceId = 2;
    $imageSizes = ['width' => 80, 'height' => 80];

    $query = $_REQUEST['q'];

    $obSearch = new CSearch;
    $obSearch->Search(array(
        'QUERY' => $query,
        'SITE_ID' => SITE_ID,
        'MODULE_ID' => 'iblock',
        'PARAM2' => $iBlockId
    ));
    $k = 0;

    $productIds = [];
    $productsInfo = [];
    while ($row = $obSearch->fetch()) {
        $itemInfo = [
            'ID' => $row['ITEM_ID'],
            'NAME' => htmlspecialcharsEx($row['TITLE'])
        ];
        if (is_numeric($row['ITEM_ID'])) {
            $productsInfo[$row['ITEM_ID']] = $itemInfo;
            $productIds[] = $row['ITEM_ID'];
        } else {
            $arResult['SECTIONS'][] = $itemInfo;
        }
    }

    if ($productIds) {

        $arSelect = array("ID", "ACTIVE", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID, "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE" => "Y", 'ID' => $productIds);

        $res = CIBlockElement::GetList(
            array($_REQUEST['sort_field'] ?? 'NAME' => $_REQUEST['sort_order'] ?? 'asc'),
            $arFilter,
            false,
            array("nPageSize" => 6, "iNumPage" => $_REQUEST['page_num'] ?? 1),
            $arSelect
        );

        $arItems = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
            $arItem = [
                'ID' => $arFields['ID'],
                'ACTIVE' => $arFields['ACTIVE'],
                'NAME' => $arFields['NAME'],
                'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
                'PREVIEW_PICTURE_SRC' =>  CFile::GetPath($arFields["PREVIEW_PICTURE"]),
                'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
                'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
                'RATIO' => $measure['RATIO'],
            ];
            $arItems[] = $arItem;
        }

        $arResult['CUR_PAGE'] = $res->NavPageNomer;
        $arResult['PAGE_COUNT'] = $res->NavPageCount;
        $arResult['SORT_FIELDS'] = Helpers\Events::getCatalogAllowSortFields();
        $arResult['ITEMS'] = $arItems;
    }
}
