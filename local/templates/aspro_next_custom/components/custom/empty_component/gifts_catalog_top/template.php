<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$products = \Bitrix\Sale\Internals\DiscountTable::getList(
    ['filter'=>['XML_ID'=>'PROGRESS_BAR%', 'ACTIVE'=>'Y'],
        'select'=>['ACTIONS_LIST']]
);
while($item = $products->fetch()) {
    foreach ($item['ACTIONS_LIST']['CHILDREN'] as $childrenAct) {
        if ($childrenAct['CLASS_ID'] == 'ActSaleBsktGrp') {
            foreach ($childrenAct['CHILDREN'] as $isProdact) {
                if ($isProdact['CLASS_ID'] == 'CondBsktFldProduct'){
                    $arrId[] = $isProdact['DATA']['value'];
                }
            }
            break;
        }
    }
}
if ($arrId):
    global $productProgressBarGiftFilter;
    $productProgressBarGiftFilter= [
        'ID' => $arrId,
        'IBLOCK_ID'=>114,
    ];
    $TITLE = "Выберите подарок";
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.top",
        "basket_gifts",
        Array(
            "ACTION_VARIABLE" => "action",
            "ADD_PICT_PROP" => "MORE_PHOTO",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "ADD_TO_BASKET_ACTION" => "ADD",
            "BASKET_URL" => "/basket.php",
            "BRAND_PROPERTY" => "BRAND_REF",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "N",
            "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
            "COMPARE_PATH" => "",
            "COMPATIBLE_MODE" => "N",
            "CONVERT_CURRENCY" => "Y",
            "CURRENCY_ID" => "RUB",
            "CUSTOM_FILTER" => '',
            "DATA_LAYER_NAME" => "dataLayer",
            "DISCOUNT_PERCENT_POSITION" => "bottom-right",
            "DISPLAY_COMPARE" => "N",
            "ELEMENT_SORT_FIELD" => "sort",
            "ELEMENT_SORT_FIELD2" => "id",
            "ELEMENT_SORT_ORDER" => "asc",
            "ELEMENT_SORT_ORDER2" => "desc",
            "ENLARGE_PRODUCT" => "STRICT",
            "USE_FILTER" => "Y",
            "FILTER_NAME" => 'productProgressBarGiftFilter',
            "HIDE_NOT_AVAILABLE" => "L",
            "HIDE_NOT_AVAILABLE_OFFERS" => "L",
            "IBLOCK_ID" => $productProgressBarGiftFilter['IBLOCK_ID'],
            "IBLOCK_TYPE" => "",
            "LABEL_PROP" => array(),
            "LABEL_PROP_MOBILE" => array(),
            "LABEL_PROP_POSITION" => "",
            "LINE_ELEMENT_COUNT" => "",
            "MESS_BTN_ADD_TO_BASKET" => "Добавить",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_COMPARE" => "Сравнить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_NOT_AVAILABLE" => "Нет в наличии",
            "MESS_RELATIVE_QUANTITY_FEW" => "мало",
            "MESS_RELATIVE_QUANTITY_MANY" => "много",
            "MESS_SHOW_MAX_QUANTITY" => "Наличие",
            "OFFERS_CART_PROPERTIES" => array(),
            "OFFERS_FIELD_CODE" => array("", ""),
            "OFFERS_LIMIT" => "",
            "OFFERS_PROPERTY_CODE" => array(),
            "OFFERS_SORT_FIELD" => "sort",
            "OFFERS_SORT_FIELD2" => "id",
            "OFFERS_SORT_ORDER" => "asc",
            "OFFERS_SORT_ORDER2" => "desc",
            "OFFER_ADD_PICT_PROP" => "",
            "OFFER_TREE_PROPS" => array(),
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "PRICE_CODE" => array(
                0 => "Цены единицы"
            ),
            "PRICE_VAT_INCLUDE" => "Y",
            "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
            "PRODUCT_DISPLAY_MODE" => "Y",
            "PRODUCT_ID_VARIABLE" => "id",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PRODUCT_QUANTITY_VARIABLE" => "",
            "PRODUCT_ROW_VARIANTS" => "",
            "PRODUCT_SUBSCRIPTION" => "Y",
            "PROPERTY_CODE" => array(),
            "PROPERTY_CODE_MOBILE" => array(),
            "RELATIVE_QUANTITY_FACTOR" => "5",
            "ROTATE_TIMER" => "",
            "SEF_MODE" => "N",
            "SEF_RULE" => "",
            "SHOW_CLOSE_POPUP" => "N",
            "SHOW_DISCOUNT_PERCENT" => "Y",
            "SHOW_MAX_QUANTITY" => "M",
            "SHOW_OLD_PRICE" => "Y",
            "SHOW_PAGINATION" => "N",
            "SHOW_PRICE_COUNT" => "1",
            "SHOW_SLIDER" => "N",
            "SLIDER_INTERVAL" => "3000",
            "SLIDER_PROGRESS" => "N",
            "TEMPLATE_THEME" => "blue",
            "USE_ENHANCED_ECOMMERCE" => "N",
            "USE_PRICE_COUNT" => "N",
            "USE_PRODUCT_QUANTITY" => "N",
            "VIEW_MODE" => "SECTION",
            "TITLE" => $TITLE,
        )
    );
endif;
?>