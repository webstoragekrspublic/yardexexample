<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$totalStars = 5;
$commentsCount = (int)$arParams['COMMENTS_CNT'] > 0 ? (int)$arParams['COMMENTS_CNT'] : 0;
$commentsAverageScore = round((float)$arParams['RATING'], \Custom\ProductComments\ProductComments::AVERAGE_SCORE_ROUND_PRECISION);

$starsImgSrc = [
    'full' => '/images/rating/product_star_full.png',
    'half' => '/images/rating/product_star_half.png',
    'empty' => '/images/rating/product_star_empty.png',
];


?>


<div class="rating_in_catalog_product_block">
    <div class="rating_in_catalog_product_block_rating">
        <?
        $currentStarIndex = 0;
        while ($currentStarIndex < $totalStars): ?>
            <?
            $scoreDiff = $commentsAverageScore - $currentStarIndex;
            if ($scoreDiff >= 0.75) {
                $starType = 'full';
            } elseif ($scoreDiff > 0.25 and $scoreDiff < 0.75) {
                $starType = 'half';
            } else {
                $starType = 'empty';
            }
            $currentStarIndex++;
            ?>
            <img class="rating_in_catalog_product_block_star" src="<?=$starsImgSrc[$starType]; ?>"/>
        <? endwhile; ?>
    </div>
    <div class="rating_in_catalog_product_text">
        <? if ($commentsCount > 0): ?>
            <?= $commentsCount . ' ' . \Helpers\CustomTools::declensionWords($commentsCount, ['отзыв', 'отзыва', 'отзывов']);; ?>
        <? else: ?>
            нет отзывов
        <? endif; ?>
    </div>
</div>