function addClickCookieAprove () {
    var cross = document.querySelector('.cookies-aprove-cross');
    document.addEventListener('click', function(event){
        if (event.target == cross) {
            var oneMonth = 30*24*60*60*1000;
            var datetime = new Date(Date.now() + oneMonth);
            var name = 'cookie_approve';
            var value = 'Approve';
            var data = {
                expires: datetime,
                path: '/'
            };
            $.cookie(name, value, data);
            if ( $.cookie('cookie_approve') == null ) {
                console.log("согласие не было получено.");
            }
            else {
                console.log("согласие истекает - " + datetime);
                document.querySelector('.cookies-aprove-container').style.display = 'none';
            }
        }
    });
}

document.addEventListener('DOMContentLoaded', addClickCookieAprove);