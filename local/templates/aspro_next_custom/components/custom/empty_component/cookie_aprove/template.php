<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */

use Bitrix\Main\Application;
$request = Application::getInstance()->getContext()->getRequest();
$isApprove = $request->getCookieRaw('cookie_approve');

//отключаем всплывашку
$isApprove = 'Approve';
?>

<div class="cookies-aprove-container <?= $isApprove == 'Approve' ? 'hidden' : ''?>">
    <div class="cookies-aprove-text">
        Мы используем файлы cookie. Они помогают нам работать лучше. Продолжая пользоваться сайтом, вы соглашаетесь с использованием cookie.
        <a href="/company/agreement-personal-data/" target="_blank">Подробнее.</a>
    </div>
    <div class="cookies-aprove-cross"></div>
</div>
