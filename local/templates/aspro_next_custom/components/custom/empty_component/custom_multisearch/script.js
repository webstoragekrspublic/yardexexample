var customMultiSearch = {
    ajaxXhr: false,
    ajaxUrl: 'https://api.multisearch.io/',
    ajaxPricesXhr: false,
    ajaxPricesUrl: '/api/v1/site/productsPrice',

    renderMaxCats: 6,
    lastSearchProducts: [],

    addressesSuggested: [],
    selectedAddressIndex: 0,
    $parentBlock: false,
    timer: false,
    timerDelay: 150, //задержка перед отправкой запроса (чтобы не спамить на каждый ввод с клавиатуры)
    $gisDataBlock: {},
    siteId: 11427,
    uid: 0,
    fast_delivery_value: '',
    limit: 18,
    resultNode: false,
    lastQueryParams: {},
    categoryInSearch: false,
    limitInCells: 4,
    offsetInSearch: 0,
    countItemsInSearch: 12,

    init: function (params) {
        var self = this;

        if (typeof params.uid !== 'undefined') {
            self.uid = params.uid;
        }
        if (typeof params.fast_delivery_value !== 'undefined') {
            self.fast_delivery_value = params.fast_delivery_value;
        }

        self.resultNode = document.getElementById('custom_multisearch_result');

        $(document).on('click', '[data-role="show_multisearch_tool"]', function () {
            self.showSearchTools();
        });

        $(document).on('click', '[data-role="custom_multisearch_open_categories_list"]', function (e) {
            e.preventDefault();
            self.toggleViewCats(this);
        });


        $(document).on('click', '[data-role="close_custom_multisearch"]', function () {
            self.hideSearchTools();
            BX.onCustomEvent('OnBasketChange');
            if (BX && BX.Sale && BX.Sale.BasketComponent) {
                BX.Sale.BasketComponent.sendRequest('refreshAjax', {fullRecalculation: 'Y'});
            }
        });

        $(document).on('click', '[data-role="custom_multisearch_category"]', function (e) {
            e.preventDefault();
            var categoryId = $(this).data('category');
            if (categoryId) {
                self.changeCategory(categoryId);
            }
        });

        $(document).on('click', '[data-role="custom_multisearch_change_page"]', function (e) {
            e.preventDefault();
            var categoryId = $(this).data('category');
            var page = $(this).data('page');
            if (categoryId && page) {
                self.nextPage(categoryId, page);
            }
        });

        $(document).on('click', '[data-role="custom_mulstisearch_corrected_phrase"]', function (e) {
            e.preventDefault();
            var phrase = $(this).data('phrase');
            if (phrase) {
                $('#custom_multisearch_q').val(phrase);
                self.newSearch(phrase);
            }
        });

        $(document).on('input propertychange', '#custom_multisearch_q', function () {
            var query = $(this).val();
            self.newSearch(query);
        });
    },

    isFastDeliveryOn: function(){
        return $('#multisearch_fast_delivery_input').is(':checked');
    },

    showSearchTools: function () {
        $('#custom_multisearch').fadeIn(0);
        document.getElementById("custom_multisearch_q").focus();
        $('html, body').css('overflowY', 'hidden');
    },

    toggleViewCats: function (obj) {
        var $arrow = $(obj).find('.multi-icon');
        if ($(obj).hasClass('multi-open')) {
            $('.custom_multisearch_mobile_category').fadeOut(0);
            $(obj).removeClass('multi-open');
            $(obj).addClass('multi-active');
            $arrow.addClass('multi-chevron-downIcon');
            $arrow.removeClass('multi-chevron-upIcon');
            $('#svg_multi-chevron-upIcon').fadeOut(0)
            $('#svg_multi-chevron-downIcon').fadeIn(0)
        } else {
            $('.custom_multisearch_mobile_category').fadeIn(0);
            $(obj).addClass('multi-open');
            $(obj).removeClass('multi-active');
            $arrow.removeClass('multi-chevron-downIcon');
            $arrow.addClass('multi-chevron-upIcon');
            $('#svg_multi-chevron-downIcon').fadeOut(0)
            $('#svg_multi-chevron-upIcon').fadeIn(0)
        }
    },

    hideSearchTools: function () {
        $('#custom_multisearch').fadeOut(0);
        $('html, body').css('overflowY', 'auto');
    },

    reSearchQuery: function (){
        var self = this;
        var query = $('#custom_multisearch_q').val();
        if (query) {
            self.newSearch(query);
        }
    },

    newSearch: function (query, filterCats, page) {
        var self = this;
        if (typeof page === 'undefined' || parseInt(page) < 1 || isNaN(parseInt(page))) {
            page = 1;
        }
        if (self.ajaxXhr) {
            self.ajaxXhr.abort();
        }
        if (self.ajaxPricesXhr) {
            self.ajaxPricesXhr.abort();
        }
        if (self.timer) {
            clearTimeout(self.timer);
        }
        self.lastQueryParams = {};
        if (query.length > 0) {
            var t = [];
            // var groupCats = 6;
            if (typeof filterCats !== 'undefined') {
                if (Array.isArray(filterCats)) {
                    t = filterCats;
                    // groupCats = 0;
                } else if (parseInt(filterCats) > 0) {
                    t = parseInt(filterCats);
                    // groupCats = 0;
                }
            }

            //[https://api.multisearch.io/?
            // id=11427&
            // query=%D1%87%D0%B0%D0%B9&
            // uid=cc441f080&
            // categories=all&
            // fields=true&
            // limit=4&
            // filters=%7B%22brand%22:[%22Tess%22]%7D]
            // var queryParams = {
            //     query: query,
            //     id: self.siteId,
            //     uid: self.uid,
            //     p: page,
            //     t: t,
            //     q: 'thuuz9',
            //     s: self.getPageSizeName(),
            //     m: 1618542371541
            // }

            var limitInSearch = 12;
            customMultiSearch.categoryInSearch = true;
            let categories = "all";
            if(t.length == 0){
                customMultiSearch.categoryInSearch = false;
                limitInSearch = 4;
            }
            customMultiSearch.offsetInSearch = 0;
            if(page != 1) {
                customMultiSearch.offsetInSearch = (page - 1) * limitInSearch;
            }
            var queryParams = {
                query: query,
                id: self.siteId,
                uid: self.uid,
                categories: categories,
                fields: true,
                limit: limitInSearch,
                offset: customMultiSearch.offsetInSearch,
                t: t,
            }

            if (self.isFastDeliveryOn()) {
                queryParams.filters = `{"FAST_DELIVERY":["`+self.fast_delivery_value+`"]}`;
            }
            self.lastQueryParams = queryParams;
            self.ajaxMsApi(query, queryParams);
        } else {
            self.clearResultBlock();
        }
    },

    changeCategory: function (categoryId) {
        var self = this;
        var query = $('#custom_multisearch_q').val();
        self.newSearch(query, categoryId);
    },

    nextPage: function (categoryId, page) {
        var self = this;
        var query = $('#custom_multisearch_q').val();
        self.newSearch(query, categoryId, page);
    },

    ajaxMsApi: function (query, params) {
        var self = this;

        self.timer = setTimeout(function () {
            self.ajaxXhr = $.ajax({
                type: "GET",
                url: self.ajaxUrl,
                dataType: 'json',
                data: params,
                success: function (data) {
                    self.setLastSearchProducts(data);
                    self.render(data);
                },
                error: function (jqXHR, exception) {
                    self.setLastSearchProducts({});
                    self.render({});
                }
            });
        }, self.timerDelay);
    },

    setLastSearchProducts: function (responseData) {
        var self = this;
        self.lastSearchProducts = [];
        if (!responseData || !responseData.total || !responseData.results) {
            return
        }
        var groupedInfo = {};
        if(responseData.results.categories == undefined){
            groupedInfo = responseData.results;
        }
        else {
            groupedInfo = responseData.results.categories;
        }
        for (var k = 0; k < groupedInfo.length; k++) {
            if (k >= self.renderMaxCats) {
                break;
            }
            var productsGroupedArr = {};
            if(groupedInfo[k].items != undefined) {
                productsGroupedArr = groupedInfo[k].items;
            }
            else{
                productsGroupedArr = groupedInfo[k];
            }

            if (!Array.isArray(productsGroupedArr[0])) {
                productsGroupedArr = [productsGroupedArr];
            }
            for (var i = 0; i < productsGroupedArr.length; i++) {
                var products = productsGroupedArr[i];

                for (var p = 0; p < products.length; p++) {
                    var product = products[p];
                    if (typeof product == "object") {
                        self.lastSearchProducts.push(product)
                    }
                }
            }
        }
    },

    render: function (responseData) {
        var self = this;
        if (self.resultNode) {
            if (!responseData || responseData.total == 0) {
                var mainNode = self.renderEmptyResult();
            } else {
                var mainNode = document.createElement('div');
                mainNode.className = 'multi-grid';
                var leftNode = document.createElement('div');
                leftNode.className = 'multi-cell multi-sidebar';
                if (responseData.corrected && responseData.corrected.phrase) {
                    var correctNode = self.renderCorrectedPhrase(responseData.corrected.phrase);
                    leftNode.append(correctNode);
                }

                if(responseData.results && responseData.results.length > 0){
                    var categoryNode = self.renderCategories(responseData.results, responseData.total);
                    leftNode.append(categoryNode);
                }
                else if (responseData.results && responseData.results.categories && responseData.results.categories.length > 0) {
                    var categoryNode = self.renderCategories(responseData.results.categories, responseData.total);
                    leftNode.append(categoryNode);
                }

                var rightNode = document.createElement('div');
                rightNode.className = 'multi-cell multi-lists';

                var itemGroups = [];
                if (responseData.results && (responseData.results.categories != undefined)) {
                    itemGroups = responseData.results.categories;
                }
                else{
                    itemGroups = responseData.results;
                }
                var productsNode = self.renderGroupedProducts(itemGroups);
                rightNode.append(productsNode);

                mainNode.append(leftNode);
                mainNode.append(rightNode);
            }
            self.resultNode.innerHTML = '';
            self.resultNode.appendChild(mainNode);
            self.updatePrices();
            setBasketStatusBtn()
        }
    },

    updatePrices: function () {
        var self = this;

        // if (self.lastSearchProducts.length === 0) {
        //     return
        // }

        var productIDs = self.lastSearchProducts.map(function (product) {
            return product.id;
        });

        self.ajaxPricesXhr = $.ajax({
            type: "GET",
            url: self.ajaxPricesUrl,
            dataType: 'json',
            data: {'product_ids': productIDs.join(',')},
            success: function (data) {
                if (data && data.result && data.result.prices) {
                    self.renderUpdatePrices(data.result.prices);
                }
            },
            error: function (jqXHR, exception) {
                self.renderUpdatePrices({});
            }
        });
    },

    renderUpdatePrices: function (prices) {
        var self = this;

        if (prices && prices.length > 0) {
            for (var k = 0; k < prices.length; k++) {
                var priceInfo = prices[k];
                var productID = priceInfo.PRODUCT_ID;
                var productInfo = self.lastSearchProducts.find(function (product) {
                    return product.id === productID.toString();
                })
                if (productInfo) {
                    if (priceInfo.BASE_PRICE > priceInfo.DISCOUNT_PRICE) {
                        productInfo.oldprice = priceInfo.BASE_PRICE.toFixed(2);
                        productInfo.price = priceInfo.DISCOUNT_PRICE.toFixed(2);
                    } else {
                        productInfo.oldprice = "";
                        productInfo.price = priceInfo.BASE_PRICE.toFixed(2);
                    }
                    var newPriceHTML = self.priceBlockHTML(productInfo);
                    // $('[data-role="multiserach_item"][data-product_id="' + productID + '"] [data-role="multisearch_price_block"]').html(newPriceHTML)
                }
            }
        } else {
            prices = []
        }

        var withoutPrices = self.lastSearchProducts.filter(function (product) {
            return prices.findIndex(function (price) {
                return price.PRODUCT_ID.toString() === product.id
            }) === -1;
        })
        if (withoutPrices.length > 0) {
            for (var k = 0; k < withoutPrices.length; k++) {
                var productWithoutPrice = withoutPrices[k]
                var productID = productWithoutPrice.id;
                $('[data-role="multiserach_item"][data-product_id="' + productID + '"] [data-role="multisearch_price_block"]').fadeOut(0)
            }
        }

        $('#custom_multisearch_result .custom_multisearch_prices_loading').fadeOut(0);
        $('#custom_multisearch_result [data-role="multisearch_price_block"]').removeClass('loading');
    },

    priceBlockHTML: function (product) {
        var self = this;
        var html = ''

        var priceCurrencyInfo = ' руб.';

        if (product.params_data &&
            product.params_data.RATIO &&
            product.params_data.IS_WEIGHTED &&
            product.params_data.SYMBOL_RUS &&
            product.params_data.IS_WEIGHTED == '1') {
            priceCurrencyInfo = ' руб./' + product.params_data.SYMBOL_RUS;
        }

        if ((product.oldprice !== '') && (product.oldprice !== null)) {
            html += `<div class="multi-snippet" data-role="multisearch_price_block">
                                        <span class="multi-oldPrice">${product.oldprice}</span>
                                        <span class="multi-price multi-newPrice">${product.price}<span class="multi-currency"> ${priceCurrencyInfo}</span></span>
                                  </div>`
        } else {
            html += `<div class="multi-snippet" data-role="multisearch_price_block">
                                        <span class="multi-price"  style="${product.params_data.RATIO != 1 ? '' : 'color:#1d2029!important;font-weight:bold;'}">${product.price}<span class="multi-currency"> ${priceCurrencyInfo}</span></span>
                                  </div>`
        }
        if (product.params_data.RATIO != 1) {
            if ((product.oldprice !== '') && (product.oldprice !== null)) {
                html += `<div class="multi-snippet price1qty_block" data-role="multisearch_price_1qty">
                                            <span class="multi-oldPrice">${(product.oldprice * product.params_data.RATIO).toFixed(2)}</span>
                                            <span class="multi-price multi-newPrice">${(product.price * product.params_data.RATIO).toFixed(2)}<span class="multi-currency"> руб.</span></span>
                                    </div>`
            } else {
                html += `<div class="multi-snippet price1qty_block" data-role="multisearch_price_1qty">
                                            <span class="multi-price">${(product.price * product.params_data.RATIO).toFixed(2)}<span class="multi-currency"> руб.</span></span>
                                    </div>`
            }
        }
        html += self.buyProductButtonHtml(product);
        return html
    },

    renderRemoveLoadingPrices: function () {

    },

    renderCategories: function (categories, ttlCnt) {
        var self = this;
        var resNode = document.createElement('div');
        var filteredByCategoryId = self.getLastQueryFilteredByCategory();

        resNode.className = "multi-filters";
        var html = '<div class="multi-taxons">';
        if (self.getPageSizeName() == 'small') {

            var firstCategoryCnt = ttlCnt;
            var firstCategoryName = 'Все категории';
            if (filteredByCategoryId) {
                for (var k = 0; k < categories.length; k++) {
                    var categoryInfo = categories[k];
                    if (filteredByCategoryId === parseInt(categoryInfo.id)) {
                        firstCategoryName = categoryInfo.name;
                        firstCategoryCnt = categoryInfo.count;
                        break;
                    }
                }
            }


            html += `<div>
                        <a href="#" class="multi-taxon multi-active" data-role="custom_multisearch_open_categories_list">
                            <span class="multi-filterByCategory">${firstCategoryName}
                                <div class="multi-icon multi-chevron-downIcon">
                                    <svg id="svg_multi-chevron-downIcon" class="multi-svg" viewBox="0 0 50 50"><path class="multi-svg-path" d="M25 32.4l-9.7-9.7 1.4-1.4 8.3 8.3 8.3-8.3 1.4 1.4z"></path></svg>
                                    <svg id="svg_multi-chevron-upIcon" style="display: none" class="multi-svg" viewBox="0 0 50 50"><path class="multi-svg-path" d="M33.3 28.7L25 20.4l-8.3 8.3-1.4-1.4 9.7-9.7 9.7 9.7z"></path></svg>
                                </div>
                            </span>
                            <span class="multi-badge" style="--color:rgb(240,131,35); --hover-color:rgba(240,131,35,0.6);">${firstCategoryCnt}</span>
                        </a>
                    </div>`
            var addClass = '';
            if (!filteredByCategoryId) {
                addClass = 'multi-active';
            }
            html += `<a href="#" class="multi-taxon ${addClass} custom_multisearch_mobile_category" data-role="custom_multisearch_category" data-category ='all' style="display: none">
                        <span>Все результаты</span>
                        <span class="multi-badge" style="--color:rgb(240,131,35); --hover-color:rgba(240,131,35,0.6);">${ttlCnt}</span>
                    </a>`

            for (var k = 0; k < categories.length; k++) {
                var categoryInfo = categories[k];
                var addClass = '';
                if (filteredByCategoryId === parseInt(categoryInfo.id)) {
                    addClass = 'multi-active';
                }

                html += `<a href="#" class="multi-taxon ${addClass} custom_multisearch_mobile_category" data-role="custom_multisearch_category" data-category ='${categoryInfo.id}' style="display: none">
                    <span>${self.escapeHtml(categoryInfo.name)}</span>
                    <span class="multi-badge" style="--color:rgb(240,131,35); --hover-color:rgba(240,131,35,0.6);">${categoryInfo.count}</span>
                </a>`
            }
        }
        html += '<div>';


        if (self.getPageSizeName() != 'small') {
            if (categories) {
                var addClass = 'multi-active';
                if (filteredByCategoryId) {
                    addClass = '';
                }
                if(customMultiSearch.categoryInSearch){
                    ttlCnt = $(".main-multi-badge").html();
                }
                html += `<a href="#" class="multi-taxon ${addClass}" data-role="custom_multisearch_category" data-category ='all'>
                        <span>Все результаты</span>
                        <span class="multi-badge main-multi-badge" style="--color:rgb(240,131,35); --hover-color:rgba(240,131,35,0.6);">${ttlCnt}</span>
                    </a>`;
                for (var k = 0; k < categories.length; k++) {
                    var categoryInfo = categories[k];
                    if(categoryInfo.category != undefined) {
                        let count = categoryInfo.count;
                        if(count == undefined) {
                            count = categoryInfo.category.count;
                        }
                        categoryInfo = categoryInfo.category;
                        categoryInfo.count = count;
                    }


                    var addClass = '';
                    if (filteredByCategoryId === parseInt(categoryInfo.id)) {
                        addClass = 'multi-active';
                    }

                    html += `<a href="#" class="multi-taxon ${addClass} " data-role="custom_multisearch_category" data-category ='${categoryInfo.id}'>
                    <span>${self.escapeHtml(categoryInfo.name)}</span>
                    <span class="multi-badge" style="--color:rgb(240,131,35); --hover-color:rgba(240,131,35,0.6);">${categoryInfo.count}</span>
                </a>`
                }
            }
            html += '</div></div>';
        }

        resNode.innerHTML = html;
        return resNode;
    },

    renderCorrectedPhrase: function (phrase) {
        var self = this;
        var resNode = document.createElement('div');
        resNode.className = 'multi-corrected';
        resNode.innerHTML = `Показаны результаты по запросу<br>
<a class="multi-phrase" data-role="custom_mulstisearch_corrected_phrase" href="#" data-phrase="${self.escapeHtml(phrase)}"><b>${self.escapeHtml(phrase)}</b></a>`;
        return resNode;
    },

    renderGroupedProducts: function (groupedInfo) {
        var self = this;
        var groupedCatsMaxProductsShow = 4;
        var resultNode = document.createElement('div');
        resultNode.className = 'multi-grid';
        var filteredByCategoryId = self.getLastQueryFilteredByCategory();
        var html = '';

        customMultiSearch.offsetInSearch = 0;
        for (var k = 0; k < groupedInfo.length; k++) {
            if(groupedInfo[k].items == undefined){
                continue;
            }
            if(!customMultiSearch.categoryInSearch) {
                if (k >= self.renderMaxCats) {
                    break;
                }
            }
            if(groupedInfo[k].category != undefined) {
                var category = groupedInfo[k].category;
            }
            else{
                var category = groupedInfo[k];
            }
            if(groupedInfo[k].items != undefined) {
                if(customMultiSearch.categoryInSearch) {
                    var productsGroupedArr = [];
                    let i = 0;
                    while(i < (groupedInfo[k].items.length/customMultiSearch.limitInCells)) {
                        productsGroupedArr.push(groupedInfo[k].items.slice(customMultiSearch.offsetInSearch, customMultiSearch.offsetInSearch+customMultiSearch.limitInCells));
                        customMultiSearch.offsetInSearch += customMultiSearch.limitInCells;
                        i++;
                    }
                }
                else{
                    var productsGroupedArr = groupedInfo[k].items;
                }
                //var productsGroupedArr = groupedInfo[k].items;;
            }
            else{
                var productsGroupedArr = groupedInfo[k];
            }

            if (!Array.isArray(productsGroupedArr[0])) {
                productsGroupedArr = [productsGroupedArr];
            }
            for (var i = 0; i < productsGroupedArr.length; i++) {
                var products = productsGroupedArr[i];

                html += '<div class="multi-cell">';
                if (i == 0) {
                    if (!category.url) {
                        html += '<div class="multi-title"><span>' + self.escapeHtml(category.name) + '</span></div>'
                    } else {
                        html += `<a href="${self.escapeHtml(category.url)}" class="multi-title">
                                    <span>${self.escapeHtml(category.name)}</span>
                                    <div class="multi-titleIcon">
                                        <div class="multi-icon multi-arrow-rightIcon">
                                            <svg class="multi-svg" viewBox="0 0 50 50"><path class="multi-svg-path" d="M24.7 34.7l-1.4-1.4 8.3-8.3-8.3-8.3 1.4-1.4 9.7 9.7z"></path><path class="multi-svg-path" d="M16 24h17v2H16z"></path></svg>
                                        </div>
                                    </div>
                                </a>`;
                    }
                }
                for (var p = 0; p < products.length; p++) {
                    var product = products[p];
                    if (typeof product !== "object") {
                        return false;
                    }

                    var pictureSrc = '/local/templates/aspro_next_custom/images/no_photo_medium.png';
                    if (product.picture) {
                        pictureSrc = product.picture;
                    }
                    var fastDeliveryHtml = '';
                    if (product.params_data.FAST_DELIVERY === self.fast_delivery_value){
                        fastDeliveryHtml = `<div><img class="fast_delivery_icon_small" src="/images/icons/fast_delivery_icon_black.svg"/></div>`
                    }
                    html += `<div class="multi-item" data-role="multiserach_item" data-product_id="${product.id}">
                                <a href="${product.url}">
                                    <div class="multi-thumbnail">
                                        <img class="multi-image" src="${pictureSrc}">
                                    </div>
                                    ${fastDeliveryHtml}
                                </a>
                                <div class="multi-content">
                                    <a href="${product.url}">
                                        <span>${self.escapeHtml(product.name)}</span>
                                    </a>`;


                    html += `<img src="/images/simple_loading.gif" class="custom_multisearch_prices_loading"/>`
                    html += `<div class="custom_multisearch_prices_info loading" data-role="multisearch_price_block">`
                    html += self.priceBlockHTML(product)
                    html += ` </div>
                             </div>
                            </div>`;

                }


                if (!filteredByCategoryId && category.count > groupedCatsMaxProductsShow) {
                    html += `<div><a href="#" class="multi-more" data-role="custom_multisearch_category" data-category ='${category.id}'>еще ${category.count - groupedCatsMaxProductsShow} ...</a></div>`;
                }
                if (filteredByCategoryId && i === (productsGroupedArr.length - 1)) {
                    var currentPage = self.getLastQueryPage();
                    var productsPerPage = self.getProductsPerPage();
                    var maxPages = Math.ceil(parseInt(category.count) / productsPerPage);

                    if (maxPages > 1) {
                        html += '<div class="multi-pagination">';
                        html += self.prevPageButtonHtml(currentPage, maxPages, category.id);
                        html += self.nextPageButtonHtml(currentPage, maxPages, category.id);
                        if (currentPage > 1) {
                            html += '<span class="multi-counter" style="--color:rgb(240,131,35);">' + currentPage + '</span>';
                        }
                        html += '</div>';
                    }
                }

                html += '</div>';
            }

        }
        resultNode.innerHTML = html;
        return resultNode;
    },

    renderEmptyResult: function () {
        var resultNode = document.createElement('div');
        resultNode.className = 'multi-noResults';
        resultNode.textContent = 'Ничего не найдено';
        return resultNode;
    },

    nextPageButtonHtml: function (currentPage, maxPage, categoryId) {
        var html = '';
        if (maxPage > 1) {
            var nextPage = currentPage + 1;
            if (nextPage <= maxPage) {
                html += `<a href="#" class="multi-page" data-role="custom_multisearch_change_page" data-category ='${categoryId}' data-page ='${nextPage}'>
                            <div class="multi-icon multi-arrow-rightIcon">
                                <svg class="multi-svg" viewBox="0 0 50 50"><path class="multi-svg-path" d="M24.7 34.7l-1.4-1.4 8.3-8.3-8.3-8.3 1.4-1.4 9.7 9.7z"></path><path class="multi-svg-path" d="M16 24h17v2H16z"></path></svg>
                            </div>
                        </a>`;
            } else {
                html += `<div class="multi-page multi-disabled">
                            <div class="multi-icon multi-arrow-rightIcon">
                                <svg class="multi-svg" viewBox="0 0 50 50"><path class="multi-svg-path" d="M24.7 34.7l-1.4-1.4 8.3-8.3-8.3-8.3 1.4-1.4 9.7 9.7z"></path><path class="multi-svg-path" d="M16 24h17v2H16z"></path></svg>
                            </div>
                        </div>`;
            }
        }
        return html;
    },

    prevPageButtonHtml: function (currentPage, maxPage, categoryId) {
        var html = '';
        if (maxPage > 1) {
            var prevPage = currentPage - 1;
            var hiddenClass = 'multi-hidden';
            if (currentPage > 1) {
                hiddenClass = '';
            }

            html += `<a href="#" class="multi-previous multi-page ${hiddenClass}" data-role="custom_multisearch_change_page" data-category ='${categoryId}' data-page ='${prevPage}'>
                        <div class="multi-icon multi-arrow-leftIcon">
                            <svg class="multi-svg" viewBox="0 0 50 50"><path class="multi-svg-path" d="M25.3 34.7L15.6 25l9.7-9.7 1.4 1.4-8.3 8.3 8.3 8.3z"></path><path class="multi-svg-path" d="M17 24h17v2H17z"></path></svg>
                        </div>
                    </a>`
        }
        return html;
    },

    buyProductButtonHtml: function (productInfo) {
        var minBuyQty = 1;
        if (productInfo.params_data && productInfo.params_data.RATIO) {
            minBuyQty = productInfo.params_data.RATIO;
        }
        var html = `
            <div class="counter_wrapp" data-role="custom_multisearch_buy_button_block">                
                <div class="counter_block" data-offers="N" data-item="${productInfo.id}" >
                    <span class="minus" id="bx_1457176589_${productInfo.id}_quant_down">-</span>
                    ${productInfo.params_data.SYMBOL_RUS == 'кг'
            ?
            `<input type="text" class="text input_quantity_weighted" id="bx_1457176589_${productInfo.id}_quantity" name="quantity" value="${minBuyQty}" />
                        <label class="input_quantity_measure_info" for="bx_1457176589_${productInfo.id}_quantity">${productInfo.params_data.SYMBOL_RUS}</label>`
            :
            `<input type="text" class="text " id="bx_1457176589_${productInfo.id}_quantity" name="quantity" value="${minBuyQty}" />`
        }
                    <span class="plus" id="bx_1457176589_${productInfo.id}_quant_up" data-max='100000'>+</span>
                </div>
                <div id="bx_1457176589_${productInfo.id}_basket_actions" class="button_block custom_multisearch_buy_button ">
                    <span data-value="83.1" data-currency="RUB" 
                    class="small to-cart btn btn-default transition_bg animate-load" 
                    data-item="${productInfo.id}" data-float_ratio="1" data-ratio="${minBuyQty}" 
                    data-bakset_div="bx_basket_div_${productInfo.id}" data-props="" data-part_props="Y" 
                    data-add_props="Y"  data-empty_props="Y" data-offers="" 
                    data-iblockID="114"  data-quantity="${minBuyQty}"><i></i><span>В корзину</span></span>
                    <a rel="nofollow" href="/basket/" class="small in-cart btn btn-default transition_bg" data-item="${productInfo.id}"  style="display:none;">
                        <i></i><span>Добавлено</span>
                    </a>
                </div>
            </div>`;
        return html;
    },

    showBuyButtons: function () {
        $('.custom_multisearch_prices_info').removeClass('loading');
    },

    getLastQueryFilteredByCategory: function () {
        var filteredByCategoryId = false;
        var self = this;
        if (typeof self.lastQueryParams.t !== 'undefined' && self.lastQueryParams.t && parseInt(self.lastQueryParams.t) > 0) {
            filteredByCategoryId = parseInt(self.lastQueryParams.t);
        }
        return filteredByCategoryId;
    },

    getLastQueryPage: function () {
        var page = 1;
        var self = this;
        if (typeof self.lastQueryParams.offset !== 'undefined' && parseInt(self.lastQueryParams.offset) > 0 && !isNaN(parseInt(self.lastQueryParams.offset))) {
            page = parseInt(self.lastQueryParams.offset/customMultiSearch.countItemsInSearch) + 1;
        }
        return page;
    },

    getProductsPerPage: function () {
        var self = this;
        var pageLimit = 13;
        switch (self.getPageSizeName()) {
            case "large":
                pageLimit = 13;
                break;
            case "medium":
                pageLimit = 8;
                break;
            case "small":
                pageLimit = 4;
                break;
        }
        return pageLimit;
    },

    getPageSizeName: function () {
        var sizeName = 'small'
        if (document.documentElement.clientWidth > 1260) {
            sizeName = 'large';
        } else if (document.documentElement.clientWidth > 960) {
            sizeName = 'medium';
        }
        return sizeName;
    },

    clearResultBlock: function () {
        var self = this;
        if (self.resultNode) {
            self.resultNode.innerHTML = '';
        }
    },

    escapeHtml: function (text) {
        return text
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    }

}