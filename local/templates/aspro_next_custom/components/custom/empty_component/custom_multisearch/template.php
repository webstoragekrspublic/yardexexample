<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$q = htmlspecialchars($request->get('q'));
$this->setFrameMode(true);
$userPhone = '';
if ($USER->IsAuthorized()) {
    $by = 'id';
    $order = 'asc';
    $dbRes = CUser::GetList($by, $order, array("ID" => $USER->GetID()), array("FIELDS" => array("ID", "PERSONAL_PHONE")));
    $arUser = $dbRes->Fetch();
    $userPhone = $arUser['PERSONAL_PHONE'];
}

$checkedFastDelivery = '';
if (\Helpers\CustomTools::isFastDeliveryEnabled()) {
    $checkedFastDelivery = 'checked="checked"';
}

?>
<!--<div id="custom_multisearch" class="multi-search multi-theme-compact" style="z-index: 1230;display:none;">
    <div id="custom_multisearch_background" data-role="close_custom_multisearch"></div>
    <div class="multi-wrapper">
        <div data-turbolinks="false" class="multi-layout multi-fontSmoothing">
            <div class="multi-header">
                <div id="multi-form" class="multi-form">
                    <div class="multi-icon multi-searchIcon">
                        <svg class="multi-svg" viewBox="0 0 50 50">
                            <path class="multi-svg-path"
                                  d="M23 36c-7.2 0-13-5.8-13-13s5.8-13 13-13 13 5.8 13 13-5.8 13-13 13zm0-24c-6.1 0-11 4.9-11 11s4.9 11 11 11 11-4.9 11-11-4.9-11-11-11z"></path>
                            <path class="multi-svg-path" d="M32.682 31.267l5.98 5.98-1.414 1.414-5.98-5.98z"></path>
                        </svg>
                    </div>
                    <input type="text" name="q" id="custom_multisearch_q" class="multi-input" autocomplete="off"
                           maxlength="90">
                    <div class="multi-icon multi-closeIcon" data-role="close_custom_multisearch">
                        <svg class="multi-svg" viewBox="0 0 50 50">
                            <path class="multi-svg-path"
                                  d="M37.304 11.282l1.414 1.414-26.022 26.02-1.414-1.413z"></path>
                            <path class="multi-svg-path"
                                  d="M12.696 11.282l26.022 26.02-1.414 1.415-26.022-26.02z"></path>
                        </svg>
                    </div>
                </div>
            </div>
            <div class="custom_multisearch_fast_delivery">
                <label class="switch">
                    <input type="checkbox" data-role="fast_delivery_input"
                           id="multisearch_fast_delivery_input" <?= $checkedFastDelivery; ?> >
                    <span class="slider round"></span>
                </label>
                <label for="multisearch_fast_delivery_input">Доставка на ближайшее время</label>
                <img class="fast_delivery_icon_small" src="/images/icons/fast_delivery_icon_black.svg">
            </div>
            <div class="multi-results" id="custom_multisearch_result"></div>
        </div>
    </div>
    <div></div>
</div>-->
<script>
    customMultiSearch.init({
        'uid': '<?=$USER->GetID() > 0 ? $USER->GetID() : bitrix_sessid(); ?>',
        'fast_delivery_value': '<?=\Helpers\Constants::PROP_FAST_DELIVERY_VALUE_EXPRESS?>',
    });
</script>