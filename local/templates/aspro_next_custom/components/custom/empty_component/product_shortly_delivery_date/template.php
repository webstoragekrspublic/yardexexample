<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$weekShortlySlotText = \Custom\DeliveryTime\DeliveryTime::getInstance()->dayArrToDeliveryInfo($arParams['SHORTLY_WEEK_DAY']);
$productShortlySlotText = \Custom\DeliveryTime\DeliveryTime::getInstance()->dayArrToDeliveryInfo($arParams['SHORTLY_DAY_PRODUCT']);
$isProductSlotDifferent = ($weekShortlySlotText !== $productShortlySlotText);

?>

<? if ($productShortlySlotText): ?>
    <? if ($arParams['VIEW'] == 'productDetail'): // !!!!!!выводится в <script></script>?>
        <div class="col-xs-12 element_detail_options_item shortly_date">
            <div class="element_detail_options_item_header">Ближайшая доставка</div>
            <div class="element_detail_options_item_value"><?= $productShortlySlotText; ?></div>
        </div>
    <? elseif ($arParams['VIEW'] == 'cart'): ?>
        <? if ($isProductSlotDifferent): // для корзины выводим только со спец условиями ?>
            <?php
            $text = 'Ожидаемая дата поставки не ранее чем';
            ?>
            <div class="element_detail_options wrap_md">
                <div class="col-xs-12 element_detail_options_item shortly_date">
                    <div class="element_detail_options_item_header"><?php echo $text; ?></div>
                    <div class="element_detail_options_item_value"><?= $productShortlySlotText; ?></div>
                </div>
            </div>
        <? endif; ?>
    <? elseif ($arParams['VIEW'] == 'productLabel'): ?>
        <? if (!empty($deliveryShortlyDay['usedConditions'])): // для корзины выводим только со спец условиями ?>
            <?php
            $text = 'Ожидаемая дата поставки не ранее чем';
            ?>
            <div class="element_detail_options wrap_md">
                <div class="col-xs-12 element_detail_options_item shortly_date">
                    <div class="element_detail_options_item_header"><?php echo $text; ?></div>
                    <div class="element_detail_options_item_value"><?= $productShortlySlotText; ?></div>
                </div>
            </div>
        <? endif; ?>
    <? endif; ?>
<? endif; ?>