function sortButtonsClick() {
    this.sortButtonsSelector = 'a[data-sort_buttons]';
    this.productSelector = '#search_product_container.json_ajax';
    this.productCatalogSelector = '#catalog_product_container';
    this.currentAjaxFetch = false;

    this.init();
}
sortButtonsClick.prototype.init = function () {
    var self = this;
    $('html').on('click', self.sortButtonsSelector, function (e) {
        e.preventDefault();

        var href = $(this).attr('href');
        var clickedElementParent = $(this).parent();
        if (href) {
            if (window.History.enabled || window.history.pushState != null) {
                window.History.pushState(null, document.title, decodeURIComponent(href));
            } else {
                location.href = href;
            }

            if (self.currentAjaxFetch) {
                self.currentAjaxFetch.abort();
            }

            if ($(self.productSelector).length) {
                self.currentAjaxFetch = $.ajax({
                    type: 'GET',
                    url: href,
                    dataType: 'json',
                    data: {
                        ajax_get: 'Y',
                        ajax_get_filter: 'Y',
                    },
                    beforeSend: function (xhr) {
                        var productContainer = $(self.productSelector);
                        if (productContainer.length) productContainer.css('opacity', '0.5');
                    },
                    success: function (data) {
                        var productContainer = $(self.productSelector);

                        var filtersContainer = $(self.filtersSelector);
                        if (productContainer.length && data && 'productContainer' in data) {
                            if ('sortButtonsContainer' in data) {
                                $('#sort_buttons_container').replaceWith(data.sortButtonsContainer);
                            }

                            if ('filtersContainer' in data) {
                                filtersContainer.replaceWith(data.filtersContainer);
                            }

                            productContainer.html(data.productContainer);

                            $(self.sortButtonsSelector).parent().removeClass('current');
                            clickedElementParent.addClass('current');

                            productContainer.css('opacity', '1');
                        }
                    },
                    error: function (jqXHR, exception) {
                        if ($('#search_product_container').length) {
                            $('#search_product_container').css('opacity', '1');
                        }
                    },
                });
            } else if ($(self.productCatalogSelector).length) {
                self.currentAjaxFetch = $.ajax({
                    type: 'GET',
                    url: href,
                    data: {
                        ajax_get: 'Y',
                        ajax_get_filter: 'Y',
                    },
                    beforeSend: function (xhr) {
                        var productContainer = $(self.productCatalogSelector);
                        if (productContainer.length) productContainer.css('opacity', '0.5');
                    },
                    success: function (data) {
                        var productContainer = $(self.productCatalogSelector);
                        var $data = $(data);
                        var dataProductContainer = $data.find('#catalog_product_container');

                        if (productContainer.length && data && dataProductContainer.length) {
                            var sortButtonsContainer = $data.find('#sort_buttons_container');
                            var filtersContainer = $data.find('#sort_buttons_container');

                            if (sortButtonsContainer.length) {
                                $('#sort_buttons_container').replaceWith(sortButtonsContainer);
                            }

                            productContainer.html(dataProductContainer);

                            $(self.sortButtonsSelector).parent().removeClass('current');
                            clickedElementParent.addClass('current');

                            productContainer.css('opacity', '1');
                        }
                        initProductsAfterAjax();
                    },
                    error: function (jqXHR, exception) {
                        if ($('#search_product_container').length) {
                            $('#search_product_container').css('opacity', '1');
                        }
                    },
                });
            }
        }
    });
};

new sortButtonsClick();
