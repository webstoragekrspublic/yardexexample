<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    'SORT_PRICES' => Array(
        "NAME" => 'SORT_PRICES',
        "TYPE" => "STRING",
        "DEFAULT" => "0",
    ),
);
