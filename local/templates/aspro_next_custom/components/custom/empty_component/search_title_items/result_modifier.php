<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Catalog\PriceTable;

/** @var array $arResult */

/** @var array $arParams */
$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$arResult = [
    'PRODUCTS' => [],
    '~q' => $request->get('q'),
    'q' => htmlspecialchars($request->get('q')),
];
if (CModule::IncludeModule('search')) {
    $iBlockId = 114;
    $limit = 10;
    $catalogGroupPriceId = 2;
    $imageSizes = ['width' => 80, 'height' => 80];

    $query = $request->get('q');

    $obSearch = new CSearch;
    $obSearch->Search(array(
        'QUERY' => $query,
        'SITE_ID' => SITE_ID,
        'MODULE_ID' => 'iblock',
        'PARAM2' => $iBlockId
    ));
    $k = 0;

    $productIds = [];
    $productsInfo = [];
    while ($row = $obSearch->fetch()) {
        $itemInfo = [
            'ID' => $row['ITEM_ID'],
            'NAME' => htmlspecialcharsEx($row['TITLE']),
            'URL' => htmlspecialcharsEx($row['URL'])
        ];
        if (is_numeric($row['ITEM_ID'])) {
            $productsInfo[$row['ITEM_ID']] = $itemInfo;
            $productIds[] = $row['ITEM_ID'];
        } else {
            $arResult['CATEGORIES'][$row['ITEM_ID']] = $itemInfo;
        }
    }

    if ($productIds) {

        $measuresInfo = [];
        $measuresQ = CCatalogMeasure::getList(
            [],
            [],
            false,
            false,
            ['ID', 'SYMBOL_RUS']
        );
        while ($row = $measuresQ->Fetch()) {
            $measuresInfo[$row['ID']] = $row;
        }

        $productsQ = \CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => $iBlockId, '=AVAILABLE' => 'Y', 'ID' => $productIds],
            false,
            ['iNumPage' => 1, 'nPageSize' => $limit],
            ['ID', 'PREVIEW_PICTURE', 'NAME', 'PROPERTY_WEIGHT_UNIT', 'MEASURE']
        );

        while ($row = $productsQ->fetch()) {
            $productInfo = $productsInfo[$row['ID']];

            $optimalPrice = CCatalogProduct::GetOptimalPrice($row['ID']);

            $productInfo['PRICE_INFO'] = $optimalPrice;

            $productInfo['NAME'] = htmlspecialcharsEx($row['NAME']);
            $productInfo['IS_WEIGHTED'] = (int)$row['PROPERTY_WEIGHT_UNIT_VALUE'] > 0;
            $productInfo['MEASURE'] = ['ID' => $row['MEASURE'], 'SYMBOL_RUS' => $measuresInfo[$row['MEASURE']]['SYMBOL_RUS'] ?? ''];
            $productInfo['SHOW_MEASURE'] = $productInfo['IS_WEIGHTED'] && $productInfo['MEASURE']['SYMBOL_RUS'];
            $imageInfo = false;
            if ($row['PREVIEW_PICTURE']) {
                $imageInfo = CFile::ResizeImageGet($row['PREVIEW_PICTURE'], $imageSizes, BX_RESIZE_IMAGE_PROPORTIONAL);
            }
            $productInfo['PREVIEW_PICTURE'] = $imageInfo;
            $arResult['PRODUCTS'][] = $productInfo;
        }
    }
}