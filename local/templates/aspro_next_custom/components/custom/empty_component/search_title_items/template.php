<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>

<? if ($arParams["MOBILE_VERSION"] == "y"): ?>

    <div class="bx_searche scrollbar">

        <? if (!empty($arResult["PRODUCTS"]) or !empty($arResult["CATEGORIES"])): ?>
            <? foreach ($arResult['CATEGORIES'] as $arItem): ?>
                <div class="bx_item_block all_result">
                    <div class="maxwidth-theme">
                        <div class="bx_item_element">
                            <a class="all_result_title btn btn-default white bold"
                               href="<?= $arItem["URL"]; ?>"><?= $arItem["NAME"] ?></a>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            <? endforeach; ?>

            <? foreach ($arResult['PRODUCTS'] as $arItem): ?>
                <? $priceInfo = $arItem["PRICE_INFO"]; ?>
                <a class="bx_item_block" href="<?= $arItem["URL"] ?>">
                    <div class="maxwidth-theme">
                        <div class="bx_item_element">
                            <span><?= $arItem["NAME"] ?></span>
                            <div class="price cost prices">
                                <div class="custom-title-search-price">

                                    <? if (!($priceInfo['DISCOUNT_LIST'])): ?>
                                        <div class="title_search_price">
                                            <?
                                            $out = CCurrencyLang::CurrencyFormat($priceInfo['PRICE']['PRICE'], $priceInfo['PRICE']['CURRENCY']);
                                            if ($arItem['SHOW_MEASURE']) {
                                                $out .= '/' . $arItem['MEASURE']['SYMBOL_RUS'];
                                            }
                                            echo $out;
                                            ?>
                                        </div>
                                    <? else: ?>
                                        <div class="title_search_price">
                                            <?
                                            $out = CCurrencyLang::CurrencyFormat($priceInfo["RESULT_PRICE"]['DISCOUNT_PRICE'], $priceInfo["RESULT_PRICE"]['CURRENCY']);

                                            if ($arItem['SHOW_MEASURE']) {
                                                $out .= '/' . $arItem['MEASURE']['SYMBOL_RUS'];
                                            }
                                            echo $out;
                                            ?>
                                        </div>
                                        <div class="title_search_price_discount"><?= CCurrencyLang::CurrencyFormat($priceInfo["RESULT_PRICE"]['BASE_PRICE'], $priceInfo["RESULT_PRICE"]['CURRENCY']); ?></div>
                                    <? endif; ?>

                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </a>

            <? endforeach; ?>

            <div class="bx_item_block all_result">
                <div class="maxwidth-theme">
                    <div class="bx_item_element">
                        <a class="all_result_title btn btn-default white bold"
                           href="/catalog/?q=<?= $arResult['q']; ?>">Все результаты</a>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
        <? else: ?>
            <div class="bx_item_block all_result">
                <div class="maxwidth-theme">
                    <div class="bx_item_element">
                        <div class="bx_searche_not_found">
                            <div class="bx_searche_not_found_title">
                                Ничего не найдено!
                            </div>
                            <div class="bx_searche_not_found_description">
                                Попробуйте изменить запрос или напишите нам, если не можете найти нужные продукты.
                            </div>
                            <div class="bx_searche_not_found_button">
                                <a data-fancybox href="#not_found_products">Написать в Ярдекс</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>
    </div>


<? else: ?>
    <div class="bx_searche scrollbar">

        <? if (!empty($arResult["PRODUCTS"]) or !empty($arResult["CATEGORIES"])): ?>
            <? foreach ($arResult['CATEGORIES'] as $arItem): ?>
                <div class="bx_item_block all_result">
                    <div class="maxwidth-theme">
                        <div class="bx_item_element">
                            <a class="all_result_title btn btn-default white bold"
                               href="<?= $arItem["URL"] ?>"><?= $arItem["NAME"] ?></a>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            <? endforeach; ?>

            <? foreach ($arResult['PRODUCTS'] as $arItem): ?>
                <? $priceInfo = $arItem["PRICE_INFO"]; ?>
                <a class="bx_item_block" href="<?= $arItem["URL"] ?>">
                    <div class="maxwidth-theme">
                        <div class="bx_img_element">
                            <? if (is_array($arItem["PREVIEW_PICTURE"])): ?>
                                <img src="<?= $arItem["PREVIEW_PICTURE"]["src"] ?>">
                            <? else: ?>
                                <img src="<?= SITE_TEMPLATE_PATH ?>/images/no_photo_small.png" width="38" height="38">
                            <? endif; ?>
                        </div>
                        <div class="bx_item_element">
                            <span><?= $arItem["NAME"] ?></span>
                            <div class="price cost prices">
                                <div class="custom-title-search-price">
                                    <? if (!($priceInfo['DISCOUNT_LIST'])): ?>
                                        <div class="title_search_price">
                                            <?
                                            $out = CCurrencyLang::CurrencyFormat($priceInfo['PRICE']['PRICE'], $priceInfo['PRICE']['CURRENCY']);
                                            if ($arItem['SHOW_MEASURE']) {
                                                $out .= '/' . $arItem['MEASURE']['SYMBOL_RUS'];
                                            }
                                            echo $out;
                                            ?>
                                        </div>
                                    <? else: ?>
                                        <div class="title_search_price">
                                            <?
                                            $out = CCurrencyLang::CurrencyFormat($priceInfo["RESULT_PRICE"]['DISCOUNT_PRICE'], $priceInfo["RESULT_PRICE"]['CURRENCY']);

                                            if ($arItem['SHOW_MEASURE']) {
                                                $out .= '/' . $arItem['MEASURE']['SYMBOL_RUS'];
                                            }
                                            echo $out;
                                            ?>

                                        </div>
                                        <div class="title_search_price_discount"><?= CCurrencyLang::CurrencyFormat($priceInfo["RESULT_PRICE"]['BASE_PRICE'], $priceInfo["RESULT_PRICE"]['CURRENCY']); ?></div>
                                        <div class="title_search_price_economy_block">
                                            экономия
                                            <div class="title_search_price_economy"><?= CCurrencyLang::CurrencyFormat($priceInfo["RESULT_PRICE"]['DISCOUNT'], $priceInfo["RESULT_PRICE"]['CURRENCY']); ?></div>
                                        </div>
                                    <? endif; ?>

                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                </a>

            <? endforeach; ?>

            <div class="bx_item_block all_result">
                <div class="maxwidth-theme">
                    <div class="bx_item_element">
                        <a class="all_result_title btn btn-default white bold"
                           href="/catalog/?q=<?= $arResult['q']; ?>">Все результаты</a>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>

        <? else: ?>
            <div class="bx_item_block all_result">
                <div class="maxwidth-theme">
                    <div class="bx_item_element">
                        <div class="bx_searche_not_found">
                            <div class="bx_searche_not_found_title">
                                Ничего не найдено!
                            </div>
                            <div class="bx_searche_not_found_description">
                                Попробуйте изменить запрос или напишите нам, если не можете найти нужные продукты.
                            </div>
                            <div class="bx_searche_not_found_button">
                                <a data-fancybox href="#not_found_products">Написать в Ярдекс</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endif; ?>
    </div>
<? endif; ?>
<script>
    $('#not_found_products_product_name').val(<?=CUtil::PhpToJSObject($arResult['~q']); ?>);
</script>