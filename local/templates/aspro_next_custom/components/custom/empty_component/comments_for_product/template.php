<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?
/** @var array $arParams */
/** @var array $arResult */

$month = array(
    "01" => "Января",
    "02" => "Февраля",
    "03" => "Марта",
    "04" => "Апреля",
    "05" => "Мая",
    "06" => "Июня",
    "07" => "Июля",
    "08" => "Августа",
    "09" => "Сентября",
    "10" => "Октября",
    "11" => "Ноября",
    "12" => "Декабря"
);
?>
<? if ($arParams['INIT_AJAX'] == 'Y'): //первый блок в который потом кидается контент аяксом?>

    <div class="product_comments">
    </div>

    <script>
        function CommentsForProductsRefreshComments(page) {
            $.ajax({
                url: "/ajax/comments/get.php",
                data: {idProduct: <?=$arResult['ID_PRODUCT']; ?>, page: page},
                type: 'POST',
                beforeSend: function (xhr) {
                },
                success: function (data) {
                    if (data) {
                        if ($('.product_comments_info').length) {
                            var commentsBlock = ($(data).find('.all_product_comments').html());
                            if (commentsBlock) {
                                $('.all_product_comments').html(commentsBlock);
                            }
                        } else {
                            $('.product_comments').html(data);
                        }
                        CommentsForProductsInitDetailPage();
                    }
                },
                error: function () {
                }
            });
        }
        function CommentsForProductsInitDetailPage() {
            $(".all_product_comments_info_author_block_rating").starRating({
                totalStars: 5,
                starShape: 'rounded',
                starSize: 15,
                readOnly: true,
                strokeWidth: 0,
                emptyColor: '#e6e2e2',
                hoverColor: '#ff6600',
                activeColor: '#ff6600',
                ratedColors: ['#ff6600', '#ff6600', '#ff6600', '#ff6600', '#ff6600'],
                useFullStars: true,
                useGradient: false
            });
            $(".all_product_comments_info_author_block_rating_mobile").starRating({
                totalStars: 5,
                starShape: 'rounded',
                starSize: 18,
                readOnly: true,
                strokeWidth: 0,
                emptyColor: '#e6e2e2',
                hoverColor: '#ff6600',
                activeColor: '#ff6600',
                ratedColors: ['#ff6600', '#ff6600', '#ff6600', '#ff6600', '#ff6600'],
                useFullStars: true,
                useGradient: false
            });

            $("#product_comments_info_rating_block_current").starRating({
                totalStars: 5,
                starShape: 'rounded',
                starSize: 22,
                readOnly: true,
                strokeWidth: 0,
                emptyColor: '#e6e2e2',
                hoverColor: '#ff6600',
                activeColor: '#ff6600',
                ratedColors: ['#ff6600', '#ff6600', '#ff6600', '#ff6600', '#ff6600'],
                useFullStars: true,
                useGradient: false
            });

            $("#product_comments_info_rating_block_current_mobile").starRating({
                totalStars: 5,
                starShape: 'rounded',
                starSize: 35,
                readOnly: true,
                strokeWidth: 0,
                emptyColor: '#e6e2e2',
                hoverColor: '#ff6600',
                activeColor: '#ff6600',
                ratedColors: ['#ff6600', '#ff6600', '#ff6600', '#ff6600', '#ff6600'],
                useFullStars: true,
                useGradient: false
            });

            $("#product_new_comment_form_rating").starRating({
                totalStars: 5,
                starShape: 'rounded',
                starSize: 22,
                strokeWidth: 0,
                minRating: 1,
                readOnly: false,
                disableAfterRate: false,
                emptyColor: '#e6e2e2',
                hoverColor: '#ff6600',
                activeColor: '#ff6600',
                ratedColors: ['#ff6600', '#ff6600', '#ff6600', '#ff6600', '#ff6600'],
                useFullStars: true,
                useGradient: false,
                callback: function (currentRating, $el) {
                    $("#product_new_comment_form_rating path").css('fill', '');
                    $("#product_new_comment_form_rating_mobile path").css('fill', '');
                    $("#product_new_comment_form_rating").starRating('setRating', currentRating);
                    $("#product_new_comment_form_rating_mobile").starRating('setRating', currentRating);
                    $("#product_new_comment_form_rating_input").val(currentRating);
                }
            });
            $("#product_new_comment_form_rating_mobile").starRating({
                totalStars: 5,
                starShape: 'rounded',
                starSize: 50,
                strokeWidth: 0,
                minRating: 1,
                readOnly: false,
                disableAfterRate: false,
                emptyColor: '#e6e2e2',
                hoverColor: '#ff6600',
                activeColor: '#ff6600',
                ratedColors: ['#ff6600', '#ff6600', '#ff6600', '#ff6600', '#ff6600'],
                useFullStars: true,
                useGradient: false,
                callback: function (currentRating, $el) {
                    $("#product_new_comment_form_rating path").css('fill', '');
                    $("#product_new_comment_form_rating_mobile path").css('fill', '');
                    $("#product_new_comment_form_rating").starRating('setRating', currentRating);
                    $("#product_new_comment_form_rating_mobile").starRating('setRating', currentRating);
                    $("#product_new_comment_form_rating_input").val(currentRating);
                }
            });

            $('#product_new_comment_btn').unbind('click').bind('click', function () {
                $(this).css('display', 'none');
                $('#product_new_comment_form').css('display', 'flex');
            });
            $('#product_new_comment_form_cancel').unbind('click').bind('click', function (e) {
                $('#product_new_comment_form').css('display', 'none');
                $('#product_new_comment_btn').css('display', 'flex');
            });

            $('#product_new_comment_form').unbind('submit').bind("submit", function (e) {
                e.preventDefault();

                $.ajax({
                    url: "/ajax/comments/add.php",
                    data: $(this).serialize(),
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        $('#product_new_comment_form input').attr('disabled', 'disabled');
                        $('#product_new_comment_form textarea').attr('disabled', 'disabled');
                    },
                    success: function (data) {
                        if (data.error) {
                            modalError(data.errorText);
                        } else {
                            CommentsForProductsRefreshComments(1);
                            $('#product_new_comment_form_cancel').click();
                            modalSuccess('Спасибо за ваш отзыв о товаре.', 'Отзыв оставлен!');
                        }
                        $('#product_new_comment_form input').removeAttr('disabled');
                        $('#product_new_comment_form textarea').removeAttr('disabled');

                    },
                    error: function () {
                        modalError('Произошла ошибка, попробуйте повторить действие позже.');
                        $('#product_new_comment_form input').removeAttr('disabled');
                        $('#product_new_comment_form textarea').removeAttr('disabled');
                    }
                });

            });
        }
        CommentsForProductsRefreshComments(1);
    </script>

<? else: ?>
    <div class="product_comments_info">
        <div class="product_comments_info_text product_comments_info_left_block hidden-xs">
            <?= $arResult['COMMENTS_CNT'] . ' ' . \Helpers\CustomTools::declensionWords($arResult['COMMENTS_CNT'], ['отзыв', 'отзыва', 'отзывов']); ?>
            о товаре
        </div>
        <div class="product_comments_info_rating product_comments_info_right_block product_comments_info_xs_full_width">
            <div class="product_comments_info_rating_block">
                <div id="product_comments_info_rating_block_current" class="product_comments_info_rating_stars hidden-xs"
                     data-rating="<?= $arResult['COMMENTS_AVERAGE_SCORE']; ?>"></div>
                <div id="product_comments_info_rating_block_current_mobile" class="product_comments_info_rating_stars visible-xs"
                     data-rating="<?= $arResult['COMMENTS_AVERAGE_SCORE']; ?>"></div>
                <div class="product_comments_info_rating_average">
                    средняя оценка <span
                            class="product_comments_info_rating_average_score"><?= $arResult['COMMENTS_AVERAGE_SCORE']; ?></span>
                </div>
            </div>

        </div>

        <div class="product_comments_info_left_block hidden-xs"></div>
        <div class="product_comments_info_right_block product_comments_info_xs_full_width">
            <? if (\CUser::IsAuthorized()): ?>
                <div id="product_new_comment_btn"
                     class="product_comments_info_rating_new_comment_button btn btn-default btn-lg">
                    Оставить отзыв
                </div>
            <? else: ?>
                <div class="product_comments_info_rating_new_comment_need_auth">
                    Оставить комментарий может только зарегестрированный пользователь.
                    (<a rel="nofollow" class="animate-load" data-event="jqm" data-param-type="auth"
                        data-param-backurl="/?login=yes" data-name="auth" href="/personal/">
                        <img src="/images/icons/personal.png">
                        <span class="">Войти</span></a>/<a rel="nofollow" href="/auth/registration/?register=yes">
                        <span class="">Регистрация</span>
                    </a>)
                </div>

            <? endif; ?>
        </div>



        <form id="product_new_comment_form" class="product_new_comment_form" style="display: none">
            <input type="hidden" name="idProduct" value="<?= $arResult["ID_PRODUCT"]; ?>"/>
            <div class="product_comments_row product_comments_row_hr">
                <div class="product_new_comment_form_text hidden-xs product_comments_info_left_block">
                    Ваша оценка
                </div>
                <div class="product_new_comment_form_rating_block product_comments_info_right_block product_comments_info_xs_full_width">
                    <div id="product_new_comment_form_rating" class="product_comments_info_rating_stars hidden-xs"
                         data-rating="3"></div>
                    <div id="product_new_comment_form_rating_mobile" class="product_comments_info_rating_stars visible-xs"
                         data-rating="3"></div>
                    <span class="product_new_comment_form_rating_mobile_info visible-xs">Оцените товар</span>
                    <input type="hidden" id="product_new_comment_form_rating_input" name="rating" value="3">
                </div>
            </div>

            <div class="product_comments_row">
                <div class="product_new_comment_form_text_2 hidden-xs product_comments_info_left_block">
                    Комментарий
                </div>
                <div class="product_comments_info_right_block product_comments_info_xs_full_width">
                    <textarea class="product_new_comment_form_textarea" name="comment" placeholder="Ваш комментарий"
                              required></textarea>
                    <div class="product_new_comment_form_buttons">
                        <input value="Оставить отзыв" type="submit" id="product_new_comment_form_confirm"
                               class="btn btn-default btn-lg"/>
                        <div id="product_new_comment_form_cancel" class="btn btn-default btn-lg btn-grey">
                            Отмена
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="all_product_comments">
            <? if ($arResult['COMMENTS']): ?>

                <div class="product_comments_row product_comments_row_hr"></div>
                <? foreach ($arResult['COMMENTS'] as $arItem): ?>
                    <div class="product_comments_row product_comments_row_hr">
                        <div class="product_comments_info_left_block all_product_comments_info_author_block hidden-xs">
                            <div class="all_product_comments_info_author_block_rating"
                                 data-rating="<?= $arItem['RATING']; ?>"></div>
                            <div class="all_product_comments_info_author_block_user">
                                <?= $arItem['USER_INFO']['NAME'] . ' ' . $arItem['USER_INFO']['LAST_NAME']; ?>
                            </div>
                        </div>
                        <div class="product_comments_info_right_block all_product_comments_info_comment_block product_comments_info_xs_full_width">
                            <div class="visible-xs">
                                <div class="all_product_comments_info_author_block_mobile">
                                    <div class="all_product_comments_info_author_block_user_mobile">
                                        <?= $arItem['USER_INFO']['NAME'] . ' ' . $arItem['USER_INFO']['LAST_NAME']; ?>
                                    </div>
                                    <div class="all_product_comments_info_author_block_rating_mobile"
                                         data-rating="<?= $arItem['RATING']; ?>"></div>
                                </div>
                            </div>
                            <div class="all_product_comments_info_comment_block_comment">
                                <?= $arItem['COMMENT']; ?>
                            </div>
                            <div class="all_product_comments_info_comment_block_data">
                                <?= $arItem['TIME_ADDED']->format('d') . ' ' . $month[$arItem['TIME_ADDED']->format('m')] . ' ' . $arItem['TIME_ADDED']->format('Y'); ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>

            <? endif; ?>
        </div>


    </div>

<? endif; ?>
