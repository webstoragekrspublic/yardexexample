<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    'ID_PRODUCT' => Array(
        "NAME" => 'ID_PRODUCT',
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
    'PAGE' => Array(
        "NAME" => 'PAGE',
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
    'INIT_AJAX' => Array(
        "NAME" => 'INIT_AJAX',
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
);
