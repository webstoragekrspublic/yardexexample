<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arResult */

/** @var array $arParams */

$idProduct = (int)$arParams['ID_PRODUCT'];
$arResult['ID_PRODUCT'] = $idProduct;
if ($arParams['INIT_AJAX'] != 'Y') {

    $productInfo = CIBlockElement::GetList([], ['ID' => $idProduct], false, [], ['ID', 'PROPERTY_COMMENTS_COUNT','PROPERTY_COMMENTS_AVERAGE_SCORE'])->Fetch();

    $page = (int)$arParams['PAGE'];
    $arResult['COMMENTS'] = [];

    if (\Bitrix\Main\Loader::includeModule('custom.productcomments')) {
        $arResult['COMMENTS_CNT'] = (int)$productInfo['PROPERTY_COMMENTS_COUNT_VALUE'];
        $arResult['COMMENTS_AVERAGE_SCORE'] = round((float)$productInfo['PROPERTY_COMMENTS_AVERAGE_SCORE_VALUE'],\Custom\ProductComments\ProductComments::AVERAGE_SCORE_ROUND_PRECISION);
        $arResult['CURRENT_PAGE'] = $page;
        if ($idProduct > 0 && $page > 0) {
            $arResult['COMMENTS'] = \Custom\ProductComments\ProductComments::getActualForProduct($idProduct, $page);
        }
    }

}


