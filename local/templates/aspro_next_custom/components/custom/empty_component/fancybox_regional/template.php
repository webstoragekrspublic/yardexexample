<div class="fast_regional_popup" id="fast_regional_popup">
    <div class="modal-header">
        <div class="fast_delivery_popup_header">
            <strong>Выберете локацию</strong>
        </div>
    </div>
    <div class="modal-body">
         <div id="map" style="width: 600px; height: 400px"></div>
    </div>
    <input type="text" value="Введите адрес"></p>
</div>

<div class="fast_regional_mobile" id="fast_regional_mobile">
    <div class="fast_delivery_popup_mobile_header">
        Выберете локацию
    </div>
        <div class="modal-body">
             <div id="map" style="width: 600px; height: 400px"></div>
        </div>
        <input type="text" value="Введите адрес"></p>
</div>
<script type = "text/javascript">
    ymaps.ready(init);
    var myMap;

    function init(){   
        myMap = new ymaps.Map ("map", {
            center: [55.76, 37.64],
            zoom: 7
        });
    }
</script>