<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @global CMain $APPLICATION */
/** @global CDatabase $DB */
/** @var array $arResult */
/** @var array $arParams */
/** @var CBitrixComponent $this */

$yandexMapActiveOnPages = [
    '/order/'=>true,
];
?>

<? if (isset($yandexMapActiveOnPages[$APPLICATION->GetCurPage()])): ?>
    <script src="https://api-maps.yandex.ru/2.1/?apikey=<?=urlencode(\ExternalApi\YandexMaps\YandexMapsApi::getApiKey()); ?>&lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            ymaps.ready(init);
            function init() {
                window.yandexMap = new ymaps.Map('ymap', {
                    center: [56.00,92.88],
                    zoom: 12,
                    controls: ['geolocationControl','zoomControl'],
                }, {
                    suppressMapOpenBlock: true,
                    suppressObsoleteBrowserNotifier: true
                });
                if (typeof CustomLocationChangeYandex == 'object'){
                    CustomLocationChangeYandex.initYandexMapEvents();
                } else {
                    console.log('CustomLocationChangeYandex undefined!!!');
                }
            }
        });
    </script>
<? endif; ?>
