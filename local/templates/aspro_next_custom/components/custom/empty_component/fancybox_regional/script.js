$(document).ready(function () {
    var popup = '#fast_regional_popup';
    if (window.innerWidth < 1000) {
        popup = '#fast_regional_mobile';
        var popup_width = 740;
        if (window.innerWidth < 780) {
            popup_width = window.innerWidth - 40;
            $(popup).css('width', popup_width)
        }
    }
    $.fancybox({
        href: popup,
        scrolling: 'no',
        padding: 0,
        closeBtn: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        speedIn : 0,
        speedOut : 0,
        helpers: {
            overlay: {closeClick: false}
        }
    });
});