<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


$checkedFastDelivery = '';
if (\Helpers\CustomTools::isFastDeliveryEnabled()) {
    $checkedFastDelivery = 'checked="checked"';
}

?>
<div id="rees46-modal" class="rees46" style="display: none;">
	<div class="rees46__modal">
		<div class="rees46__modal__tool">
					<label>
						<input type="checkbox" name="only_fast_delivery_products"<?php echo $checkedFastDelivery;?> data-value="<?\Helpers\Constants::PROP_FAST_DELIVERY_VALUE_EXPRESS?>">
						<span>Доставка на ближайшее время</span>
						<img src="https://yarbox.ru/images/icons/fast_delivery_icon_black.svg">
					</label>
				</div>
				<div class="rees46__modal__form">
					<input id="rees46-modal-input" type="text" placeholder="Введите запрос">
					<button id="rees46-modal-button" class="close">&times;</button>
				</div>
				<div class="rees46__modal__results">
					<div id="rees46-modal-filters" class="rees46__modal__results__filters"></div>
					<div id="rees46-modal-products" class="rees46__modal__results__products"></div>
				</div>
			</div>
			<div id="rees46-mask" class="rees46__mask"></div>
		</div>


