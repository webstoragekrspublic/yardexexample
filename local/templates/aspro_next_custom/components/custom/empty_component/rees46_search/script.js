document.addEventListener("DOMContentLoaded", function(event){
	/**
	 * Инициализация поиска
	 * @type {{searchInput: Element, productsBlock: Element, init: rees46CustomSearch.init, currentCategory: null, offset: number, classes: *[], setCookie: rees46CustomSearch.setCookie, filters: {}, maskBlock: Element, deliveryInput: Element, getCookie: (function(*): *|undefined), filtersBlock: Element, timer: boolean, total: number, search: rees46CustomSearch.search, closeButton: Element, limit: number, createItem: (function(*=, *=, *=, *=): HTMLDivElement), timerDelay: number, categories: *[], modal: Element, addToCartCls: string}}
	 */
	var rees46CustomSearch = {
		// Переменные
		modal           : document.querySelector('#rees46-modal'),
		searchInput     : document.querySelector('#rees46-modal-input'),
		filtersBlock    : document.querySelector('#rees46-modal-filters'),
		productsBlock   : document.querySelector('#rees46-modal-products'),
		closeButton     : document.querySelector('#rees46-modal-button'),
		maskBlock       : document.querySelector('#rees46-mask'),
		deliveryInput   : document.querySelector('#rees46-modal input[name="only_fast_delivery_products"]'),
		categories      : [], // Массив категорий для поиска
		classes         : [], // Массив классов для создания элементов
		currentCategory : null, // Текущая категория
		filters         : {}, // Фильтры для поиска
		limit           : 12, // Количество товаров для одной страницы
		offset          : 0, // Стартовая страница для пагинации
		timer           : false,
		timerDelay      : 300, // Задержка в мс. для блокировки запроса, защита от спама
		total           : 0, // Общее количество товаров в ответе
		addToCartCls    : '.to-cart.btn', // Класс элемента, который отвечает за добавление в корзину

		// Вспомогательные функции
		getCookie       : function(name) {
			var matches = document.cookie.match(new RegExp(
				"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
			));
			return matches ? decodeURIComponent(matches[1]) : undefined;
		},
		setCookie       : function(name, value, options = {}) {

			options = {
				path: '/',
				...options
			};

			if (options.expires instanceof Date) {
				options.expires = options.expires.toUTCString();
			}

			let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

			for (let optionKey in options) {
				updatedCookie += "; " + optionKey;
				let optionValue = options[optionKey];
				if (optionValue !== true) {
					updatedCookie += "=" + optionValue;
				}
			}

			document.cookie = updatedCookie;
		},
		createItem      : function(classNames = [], tagName = 'div', value = '', params = {}) {
			var item = document.createElement(tagName);
			if (classNames.length > 0) classNames.forEach(c => item.classList.add(c));
			if (tagName !== 'input') {
				item.innerHTML = value;
			} else {
				item.value = value
			}
			if (params.categoryId)    item.setAttribute('data-category-id', params.categoryId);
			if (params.productsCount) item.setAttribute('style', '--count:' + params.productsCount);
			if (params.itemUrl)       item.setAttribute('href', params.itemUrl)

			return item
		},

		// Инициализация
		init            : function () {
			var self = this;

			// Достаем текущую настройку доставки
			self.filters = self.deliveryInput.checked ? {'Быстрая доставка': [self.deliveryInput.dataset.value]} : {};

			// Отслеживание настроек доставки
			self.deliveryInput.addEventListener('click', function(){
				if (self.deliveryInput.checked) {
					self.setCookie(self.deliveryInput.name, 'true');
					self.filters = {'Быстрая доставка': [self.deliveryInput.dataset.value]};
				} else {
					self.setCookie(self.deliveryInput.name, 'false');
					self.filters = {};
				}
				self.search();
			});

			// Отслеживание поля ввода
			self.searchInput.addEventListener('keyup', function() {
				self.categories = [];
				self.currentCategory = null;
				self.offset = 0;
				self.search();
			});

			// Отслеживание элементов закрытия
			[self.closeButton, self.maskBlock].forEach(function(el) {
				el.addEventListener('click', function(){
					self.modal.style.display = 'none';
				})
			})

			// Инициализация триггеров вызова окна поиска
			if (document.querySelector('.rees46-modal-trigger')) {
				document.querySelectorAll('.rees46-modal-trigger').forEach(function(el){
					el.addEventListener('click', function() {
						self.modal.style.display = 'block';
						self.searchInput.focus();
					})
				})
			}
		},

		// Поиск
		search          : function() {
			var self = this;
			if (self.searchInput.value.length > 0) {
				if (self.timer) clearTimeout(self.timer);
				self.timer = setTimeout(function () {
					r46("search", {
							search_query: self.searchInput.value,
							categories: self.categories,
							limit: self.limit,
							offset: self.offset,
							filters: self.filters
						},
						function (response) {
							if (self.offset > 0) {
								self.productsBlock.insertAdjacentHTML('beforeend', response.html);
							} else {
								self.productsBlock.innerHTML = response.html;
							}

							// Очищаем блок категорий
							if (!self.currentCategory) self.filtersBlock.innerHTML = '';
							if (response.categories.length > 0) {
								if (!self.currentCategory) {
									// Запоминаем количество товаров в ответе
									self.total = response.products_total;

									// Выводим кнопку для сброса категорий в запросе
									self.filtersBlock.appendChild(self.createItem(['category'], 'button', 'Все результаты', {productsCount: self.total}));

									// Рисуем блок категорий
									response.categories.forEach(function (cat) {
										if (cat.parent !== null) {
											self.filtersBlock.appendChild(self.createItem(['category'], 'button', cat.name, {
												categoryId: cat.id,
												productsCount: cat.count
											}))
										}
									});

									// Выводим заголовок результатов
									if (self.offset === 0) self.productsBlock.prepend(self.createItem(['title'], 'div', 'Все результаты'));

									// Вешаем запрос к поиску на клик
									self.filtersBlock.querySelectorAll('button').forEach(function (button) {
										button.addEventListener('click', function (evt) {
											self.filtersBlock.querySelectorAll('button').forEach(function (button) {
												button.classList.remove('active');
											});
											evt.currentTarget.classList.add('active');
											self.categories = [button.dataset.categoryId];
											self.currentCategory = button.dataset.categoryId;
											self.offset = 0;
											self.search();
										})
									})
								} else {
									// Выводим заголовок категории
									response.categories.forEach(function (cat) {
										if (cat.id === self.currentCategory && self.offset === 0) {
											if (cat.url) {
												self.productsBlock.prepend(self.createItem(['link'], 'a', cat.name, {itemUrl: cat.url}));
											} else {
												self.productsBlock.prepend(self.createItem(['title'], 'div', cat.name));
											}
										}
									});
								}
								// Добавляем пагинацию вывода
								if (response.products_total > self.limit + self.offset) {
									self.productsBlock.appendChild(self.createItem(['load-more'], 'button', 'Загрузить ещё'));
									self.productsBlock.querySelector('.load-more').addEventListener('click', function (evt) {
										evt.currentTarget.remove();
										self.offset += self.limit;
										self.search();
									});
								}
							}
							if (response.products_total === 0) {
								self.productsBlock.prepend(self.createItem(['title'], 'div', 'По Вашему запросу <b>' + self.searchInput.value + '</b> ничего не' +
									' найдено.'));
							}

							// Добавляем обработчик добавления в корзину
							if (self.addToCartCls) {
								self.productsBlock.querySelectorAll(self.addToCartCls).forEach(function (btn) {
									if (btn.classList.contains('rees46-listen') === false) {
										btn.addEventListener('click', function () {
											var count = btn.closest('.counter_wrapp').querySelector('.counter_block input').value,
												id = btn.dataset.item;

											r46('track', 'cart', {
												id: id,
												amount: count,
												recommended_by: 'full_search',
												recommended_code: self.searchInput.value
											});
										})
									}
									btn.classList.add('rees46-listen');
								})
							}
						},
						function (error) {
							console.log('Search query is empty');
						}
					)
				}, self.timerDelay);
			}
		}
	}
	rees46CustomSearch.init();

	/**
	 * Инициализация блоков рекомендаций
	 */
	$('.rees46-recommend-custom').each(function(){
		var code          = $(this).attr('id'),
			item          = $(this).attr('data-rees46-item'),
			category      = $(this).attr('data-rees46-category'),
			search_query  = $(this).attr('data-rees46-search'),
			component     = $(this).attr('data-rees46-component'),
			template      = $(this).attr('data-rees46-template'),
			iblockId      = $(this).attr('data-rees46-iblock-id'),
			iblockType    = $(this).attr('data-rees46-iblock-type');

		r46(
			'recommend',
			code,
			{
				item: item,
				category: category,
				search_query: search_query
			},
			function (response) {
				$.ajax({
					url: '/bitrix/components/rees46/custom/ajax.php',
					method: 'post',
					dataType: 'html',
					data: {
						recommends:  response.recommends,
						title:       response.title,
						component:   component,
						template:    template,
						iblock_id:   iblockId,
						iblock_type: iblockType
					},
					success: function (data) {
						$('#' + code).html(data);
						console.log(code);
						$('#' + code + ' a').each(function(){
							$(this).attr('href', $(this).attr('href') + '?recommended_by=dynamic&recommended_code=' + code);
						});
						$('#' + code + ' .owl-carousel').owlCarousel({
							loop: true,
							nav: true,
							items: 4,
							dots: false,
							margin: 20,
							lazyload: true,
							touchDrag: true,
							mouseDrag: true,
							navText: ["<img src='/local/templates/main/img/icons/banner_left_arrow.svg'>", "<img src='/local/templates/main/img/icons/banner_right_arrow.svg'>"],
							responsive: {
								320: {
									items: 2,
									stagePadding: 0,
									nav: false,
									margin: 0,
									stagePadding: 0,
								},
								400: {
									items: 1,
									stagePadding: 0,
									nav: false,
									margin: 0,
									stagePadding: 0,
								},
								480: {
									items: 2,
									stagePadding: 0,
									nav: false,
									margin: 0,
									stagePadding: 0,
								},
								600: {
									items: 2,
									stagePadding: 20,
									margin: 10,
									nav: false,
								},
								700: {
									items: 3,
									stagePadding: 0,
									margin: 0,
									nav: false,
								},
								900: {
									items: 3,
									stagePadding: 20,
									nav: false,
									margin: 10,
								},
								1000: {
									items: 3,
									stagePadding: 50,
									nav: false,
								},
								1100: {
									items: 4,
									stagePadding: 0,
								},
							}
						});
						$('#' + code).on('click', '.js_add2cart', function (){
							var id = $(this).attr('data-id');

							r46('track', 'cart', {
								id: id,
								amount: 1,
								recommended_by: 'dynamic',
								recommended_code: code
							});
						})
					},
				});
			},
			function (error) {
				console.log(error);
			}
		);
	});
})
