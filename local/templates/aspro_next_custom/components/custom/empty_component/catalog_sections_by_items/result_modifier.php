<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arResult */

/** @var array $arParams */

use Bitrix\Main\Loader;

$arResult = [];

$TTL = !empty($arParams['CACHE_TIME']) ? (int)$arParams['CACHE_TIME'] : 3600;
$uniqueString = json_encode($arParams);

$cache = Bitrix\Main\Data\Cache::createInstance();

$arResult = [];

if ($cache->initCache($TTL, $uniqueString)) {
    $arResult = $cache->getVars();
} elseif ($cache->startDataCache()) {
    $itemsId = $arParams['ITEMS_ID'];
    $arResult['SECTIONS'] = [];
    $arResult['TOTAL_CNT'] = count($itemsId);
    if (!empty($itemsId) and is_array($itemsId)) {
        $sectionsQ = CIBlockElement::GetElementGroups(
            $itemsId,
            true,
            ['ID', 'ACTIVE', 'SORT', 'NAME', 'CODE']
        );
        while ($row = $sectionsQ->GetNext()) {
            $sectionId = $row['ID'];
            if (isset($arResult['SECTIONS'][$sectionId])) {
                $arResult['SECTIONS'][$sectionId]['COUNT']++;
            } else {
                $row['COUNT'] = 1;
                $arResult['SECTIONS'][$sectionId] = $row;
            }
        }
        usort($arResult['SECTIONS'], function ($a, $b) {
            return strcmp($a['NAME'], $b['NAME']);
        });
    }

    $cache->endDataCache($arResult);
}
