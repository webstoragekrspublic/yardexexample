<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"IBLOCK_ID" => Array(
		"NAME" => 'IBLOCK_ID',
		"TYPE" => "STRING",
		"DEFAULT" => "0",
	),
	"ITEMS_ID" => Array(
		"NAME" => 'ITEMS_ID',
		"TYPE" => "STRING",
		"DEFAULT" => [],
	),
    "CONTAINERS_FOR_AJAX" => Array(
        "NAME" => 'CONTAINERS_FOR_AJAX',
        "TYPE" => "STRING",
        "DEFAULT" => [],
    ),
    "PAGE_BASE_URL" => Array(
        "NAME" => 'PAGE_BASE_URL',
        "TYPE" => "STRING",
        "DEFAULT" => '',
    ),
    "USE_SECTION_CODE" => Array(
        "NAME" => 'ITEMS_ID',
        "TYPE" => "STRING",
        "DEFAULT" => 'N',
    ),
    "SELECTED_SECTION_ID" => Array(
        "NAME" => 'SELECTED_SECTION_ID',
        "TYPE" => "STRING",
        "DEFAULT" => '0',
    ),
);
