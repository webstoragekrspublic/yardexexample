<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<? if ($arResult['SECTIONS']): ?>

    <?
    $selectedSectionId = (isset($arParams["SELECTED_SECTION_ID"]) ? (int)$arParams["SELECTED_SECTION_ID"] : 0);
    $baseSectionUrl = $arParams['PAGE_BASE_URL'];
    ?>

    <div class="section_block">
        <div class="sections_wrapper">

            <div class="list items">
                <div class="row margin0 flexbox">


                    <div class="catalog_sections_item <?= !$selectedSectionId ? 'current' : ''; ?>">
                        <a href="<?= $baseSectionUrl; ?>"
                           data-section_search_select>
                            Все <span class="badge"><?= $arResult['TOTAL_CNT']; ?></span>
                        </a>
                    </div>

                    <?
                    foreach ($arResult['SECTIONS'] as $section):?>
                        <?
                        $sectionUrl = $arParams['USE_SECTION_CODE'] == 'Y' ? $baseSectionUrl . $section['CODE'] . '/' : $baseSectionUrl . '?section_id=' . $section['ID'];
                        ?>
                        <div class="catalog_sections_item <?= ($section['ID'] == $selectedSectionId ? 'current' : ''); ?>">
                            <a href="<?= $sectionUrl ?>" data-section_search_select>
                                <?= $section["NAME"]; ?> <span class="badge"><?= $section["COUNT"]; ?></span>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>

<? endif; ?>
