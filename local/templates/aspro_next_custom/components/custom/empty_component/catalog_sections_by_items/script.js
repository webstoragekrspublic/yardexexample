function sectionSearchSelect() {
    this.sectionsSelector = 'a[data-section_search_select]';
    this.productContainerSelector = '#search_product_container';
    this.filtersContainerSelector = '.visible_mobile_filter';
    this.currentAjaxFetch = false;

    this.init();
}

sectionSearchSelect.prototype.init = function () {
    var self = this;
    $('html').on("click", self.sectionsSelector, function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var clickedElementParent = $(this).parent();
        if (href) {
            if (window.History.enabled || window.history.pushState != null) {
                window.History.pushState(null, document.title, decodeURIComponent(href));
            } else {
                location.href = href;
            }

            if (self.currentAjaxFetch) {
                self.currentAjaxFetch.abort();
            }

            self.currentAjaxFetch = $.ajax({
                type: 'GET',
                url: href,
                dataType: 'json',
                data: {
                    ajax_get: 'Y',
                    ajax_get_filter: 'Y'
                },
                beforeSend: function (xhr) {
                    var productContainer = $(self.productContainerSelector);
                    if (productContainer.length)
                        productContainer.css('opacity', '0.5');
                },
                success: function (data) {
                    var productContainer = $(self.productContainerSelector);
                    var filtersContainer = $(self.filtersContainerSelector);
                    if (productContainer.length && data && 'productContainer' in data) {

                        if ('filtersContainer' in data) {
                            filtersContainer.replaceWith(data.filtersContainer);
                        }

                        if ($('#sort_buttons_container').length && 'sortButtonsContainer' in data) {
                            $('#sort_buttons_container').replaceWith(data.sortButtonsContainer);
                        }

                        productContainer.html(data.productContainer);


                        $(self.sectionsSelector).parent().removeClass('current');
                        clickedElementParent.addClass('current');

                        productContainer.css('opacity', '1');
                    }
                    initProductsAfterAjax();
                },
                error: function (jqXHR, exception) {
                    console.log('error '+exception);
                    if ($(this.productContainerSelector).length){
                        $(this.productContainerSelector).css('opacity', '1');
                    }
                }
            });
        }
    });
}

new sectionSearchSelect();