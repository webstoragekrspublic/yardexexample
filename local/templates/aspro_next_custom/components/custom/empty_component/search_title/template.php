<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?
/** @var array $arParams */
/** @var array $arResult */
$ajaxUrl = '/ajax/search_preview.php';
$ajaxUrlMobile = '/ajax/search_preview.php?mobile_version=y';
$INPUT_ID_TMP = $INPUT_ID = "title-search-input-custom";
$CONTAINER_ID = "title-search-custom";
$INPUT_ID_TMP_MOBILE = $INPUT_ID_MOBILE = "title-search-input";
$CONTAINER_ID_MOBILE = "title-search";

?>

<? if($arParams["MOBILE_VERSION"] == "Y"): ?>
    <div class="inline-search-block with-close fixed big">
        <div class="maxwidth-theme">
            <div class="col-md-12">
                <div class="search-wrapper">
                    <div id="<?=$CONTAINER_ID_MOBILE?>">
<!--data-role="show_multisearch_tool"-->
                        <form action="" method="get" class="search" >
                            <div class="search-input-div">
                                <input class="search-input rees46-modal-trigger" id="<?=$INPUT_ID_MOBILE?>" type="text" name="q" value="" placeholder="<?=GetMessage("CT_BST_SEARCH_BUTTON")?>" size="20" maxlength="50" autocomplete="off" />
                            </div>
                            <div class="search-button-div">
                                <button class="btn btn-search btn-default bold btn-lg" type="submit" name="s" value="Найти">Найти</button>
                                <span class="close-block inline-search-hide"><span class="svg svg-close close-icons"></span></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var jsControl = new JCTitleSearch2({
            // 'WAIT_IMAGE': '/bitrix/themes/.default/images/wait.gif',
            'AJAX_PAGE': '<?=CUtil::JSEscape($ajaxUrlMobile)?>',
            'CONTAINER_ID': '<?=$CONTAINER_ID_MOBILE?>',
            'INPUT_ID': '<?=$INPUT_ID_MOBILE?>',
            'INPUT_ID_TMP': '<?=$INPUT_ID_TMP_MOBILE?>',
            'MIN_QUERY_LEN': 2
        });
    </script>
<? else: ?>
    <div id="<?= $CONTAINER_ID ?>">
        <form action="" method="get" class="custom_search_form">
            <input class="custom_search_input rees46-modal-trigger" id="<?= $INPUT_ID ?>" type="text" name="q"
                   value="<?= htmlspecialchars($_REQUEST['q']); ?>"
                   placeholder="Поиск..." size="20" maxlength="50"                   
                   autocomplete="off"/>
            <input class="custom_search_button" type="submit" name="s" value=""/>
        </form>
    </div>
    <script type="text/javascript">
        var jsControl = new JCTitleSearch2({
            // 'WAIT_IMAGE': '/bitrix/themes/.default/images/wait.gif',
            'AJAX_PAGE': '<?=CUtil::JSEscape($ajaxUrl)?>',
            'CONTAINER_ID': '<?=$CONTAINER_ID?>',
            'INPUT_ID': '<?=$INPUT_ID?>',
            'INPUT_ID_TMP': '<?=$INPUT_ID_TMP?>',
            'MIN_QUERY_LEN': 2
        });
    </script>
<?endif;?>