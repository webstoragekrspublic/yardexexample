<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
    'MOBILE_VERSION' => Array(
        "NAME" => 'MOBILE_VERSION',
        "TYPE" => "STRING",
        "DEFAULT" => "",
    ),
);
