<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$q = htmlspecialchars($request->get('q'));
$this->setFrameMode(true);
$userPhone = '';
if ($USER->IsAuthorized()) {
    $by = 'id';
    $order = 'asc';
    $dbRes = CUser::GetList($by, $order, array("ID" => $USER->GetID()), array("FIELDS" => array("ID", "PERSONAL_PHONE")));
    $arUser = $dbRes->Fetch();
    $userPhone = $arUser['PERSONAL_PHONE'];
}

$nearestDeliveryDay = \Custom\DeliveryTime\DeliveryTime::getInstance()->getShortlyWeekDay();
$nearestDeliveryTimeSlot = \Custom\DeliveryTime\DeliveryTime::getInstance()->getFirstNotDisabledTimeSlot($nearestDeliveryDay['timeSlots']);

$nearestAllProductsDeliveryDay = \Custom\DeliveryTime\DeliveryTime::getInstance()->getShortlyWeekDayWithAllConditions();

//pr($nearestAllProductsDeliveryDay);

$nearestAllProductsDeliveryDay = \Custom\DeliveryTime\DeliveryTime::getInstance()->getShortlyWeekDayWithAllConditions();

//if($nearestAllProductsDeliveryDay == NULL){
    
    $d = strtotime("+1 day");

    $arr = [
        'Января',
        'Февраля',
        'Марта',
        'Апреля',
        'Мая',
        'Июня',
        'Июля',
        'Августа',
        'Сентября',
        'Октября',
        'Ноября',
        'Декабря'
    ];
    $month = date('n')-1;

    $dayWeek = date("N", $d);

    if($dayWeek == 5){
        $d = strtotime("+3 day");
    }
    else if($dayWeek == 6){
        $d = strtotime("+2 day");
    }
    else{
        $d = strtotime("+1 day");
    }

    $month = date('n', $d) - 1;

    $nearestAllProductsDeliveryDay['infoDate2'] = date("d", $d)." ".$arr[$month];
    $nearestAllProductsDeliveryDay["timeSlots"] = [Array
        (
            'time' => "11:00-13:00",
            'disabled' => 0
        ),

    Array
        (
            'time' => "13:00-15:00",
            'disabled' => 0
        ),

    Array
        (
            'time' => "15:00-17:00",
            'disabled' => 0
        ),

    Array
        (
            'time' => "17:00-19:00",
            'disabled' => 0
        ),

    Array
        (
            'time' => "19:00-21:00",
            'disabled' => 0
        ),

    Array
        (
            'time' => "21:00-23:00",
            'disabled' => 0
        )];


$nearestAllProductsDeliveryTimeSlot = \Custom\DeliveryTime\DeliveryTime::getInstance()->getFirstNotDisabledTimeSlot($nearestAllProductsDeliveryDay["timeSlots"]);
?>
<div style="display: none;" id="not_found_products" class="col-sm-12">
    <h2 class="not_found_products_h">Что-то не нашли на нашем сайте?</h2>
    <p class="not_found_products_description">Сообщить вам, когда товар появится наличии?</p>
    <form id="not_found_products_form">
        <div class="not_found_products_block">
            <label class="not_found_products_label" for="not_found_products_product_name">Что не нашли?<span
                        class="required-star">*</span></label>
            <div class="input not_found_products_input_block">
                <textarea type="text" name="product_name" id="not_found_products_product_name"
                          class="required valid not_found_products_input"
                          placeholder="Пример: сосиски мираторг отборные, яйцо куриное 2 сорт ..." required
                          maxlength="50"><?= $q; ?></textarea>
            </div>
        </div>
        <div class="not_found_products_block">
            <label class="not_found_products_label" for="not_found_products_phone">Телефон<span
                        class="required-star">*</span></label>
            <div class="input">
                <input type="text" name="phone" id="not_found_products_phone"
                       class="form-control required valid not_found_products_input" value="<?= $userPhone; ?>" required
                       maxlength="50">
            </div>
        </div>
        <div class="not_found_products_submit buttons">
            <input id="not_found_products_submit" type="submit" class="btn btn-default bold" name="send"
                   value="Отправить">
        </div>
    </form>
</div>

<div style="display: none;" id="modal_success" class="col-sm-12">
    <h2 id="modal_success_h" class="modal_header">Успех</h2>
    <p id="modal_success_description" class="modal_description"></p>
</div>

<div style="display: none;" id="modal_error" class="col-sm-12">
    <h2 id="modal_error_h" class="modal_header">Ошибка</h2>
    <p id="modal_error_description" class="modal_description"></p>
</div>
<div style="display: none;" class="exit_popup" id="exit_popup">
    <img class="exit_popup_percent" src="/images/popups/percent.png"/>
    <div class="exit_popup_descr">
        Хочешь получать промо коды,
        быть первым в курсе акций и скидок
        и даже иметь возможность купить
        товары по 1 руб.?
    </div>
    <div class="exit_popup_bold">подписывайся на мессенджеры</div>
    <div class="exit_popup_messenger">
        <a class="exit_popup_messenger_item" href="https://vk.com/app6379730_-118900144#l=5&ml=5&auto=1">
            <img src="<?= SITE_DIR . 'images/messengers_icons/vk_round.png' ?>" alt="">
        </a>
        <a class="exit_popup_messenger_item" href="viber://pa?chatURI=yardexbot&context=ml5">
            <img src="<?= SITE_DIR . 'images/messengers_icons/viber_round.png' ?>" alt="">
        </a>
        <a class="exit_popup_messenger_item" href="tg://resolve?domain=Yardexbot&start=ml5">
            <img src="<?= SITE_DIR . 'images/messengers_icons/telegram_round.png' ?>" alt="">
        </a>
    </div>
    <div class="exit_popup_white_bg"></div>
</div>
<div style="display: none;" class="applink_popup" id="applink_popup">
    <picture>
        <source type="image/webp" srcset="<?= Helpers\CustomTools::getWebpImgSrc('/images/popups/appicon-75.png'); ?>">
        <source type="image/png" srcset="/images/popups/appicon-75.png">
        <img class="applink_popup_image" srcset="/images/popups/appicon-75.png" src="/images/popups/appicon-75.png"/>
    </picture>
    <div class="applink_popup_descr">
        В приложении удобнее
    </div>
    <div class="applink_popup_btn">
        <a href="https://apps.apple.com/ru/app/com.yardex.mobile-app/id1555057094" class="btn btn-default">Открыть</a>
    </div>
</div>

<div style="display: none;" class="ymap_address_suggestion" id="ymap_address_suggestion">
    <h2 class="modal_header">Введите адрес доставки</h2>
    <div class="ymap_address_suggestion_address_input_block">
        <div class="ymap_address_suggestion_address_input_block_suggestion">
            <div class="ymap_address_suggestion_address_input_block_suggestion_info">
                <input type="text" placeholder="Введите адрес" class="ymap_address_suggestion_address_input"
                       data-role="suggested_address_input">
                <input type="hidden" id="ymap_address_suggestion_geo_coords" value="">
            </div>
            <div class="ymap_address_suggestion_address_input_block_appartament_info">
                <label for="ymap_address_suggestion_address_apartament_input">Номер квартиры:</label>
                <input type="text" placeholder="" class="ymap_address_suggestion_address_apartament_input"
                       id="ymap_address_suggestion_address_apartament_input">
            </div>
            <div class="ymap_address_suggestion_error" id="ymap_address_suggestion_error"></div>
            <div class="ymap_address_suggestion_hint" id="ymap_address_suggestion_hint"></div>
        </div>
        <button class="ymap_address_suggestion_address_btn btn btn-lg btn-default" id="choose_ymap_address">Выбрать
        </button>
    </div>
    <div class="ymap_address_suggestion_ymap_description">или выберите дом на карте</div>
    <div id="ymap"></div>
</div>

<div style="display: none;" class="ingridients_added_popup" id="ingridients_added_popup">
    <h2 class="modal_header">В корзину добавлено:</h2>
    <div class="ingridients_added_popup-list added"></div>
    <h2 class="modal_header">К сожалению, следующие продукты закончились:</h2>
    <div class="ingridients_added_popup-list not_added"></div>
    <div class="ingridients_added_popup-to_catalog">
        <a href="/catalog/" class="ingridients_added_popup-to_catalog_btn btn btn-default transition_bg">Искать продукты
            в каталоге</a>
    </div>
    <div class="ingridients_added_popup-close">
        <a class="ingridients_added_popup-close_btn btn transition_bg">Закрыть</a>
    </div>
</div>

<div style="display: none;" class="dozakaz_checkout_popup" id="dozakaz_checkout_popup">
</div>

<div style="display: none;" class="fast_delivery_popup" id="fast_delivery_popup">
    <div class="modal-header">
        <div class="fast_delivery_popup_header">
            <strong>ВЫБИРАЙТЕ УДОБНЫЙ РЕЖИМ ДОСТАВКИ</strong>
        </div>
    </div>
    <div class="modal-body">
        <div class="fast_delivery_popup_buttons_block">
            <div class="fast_delivery_popup_buttons_block_item">
                <div class="fast_delivery_popup_descr_header"><strong>САМОЕ НЕОБХОДИМОЕ</strong></div>

                <div>
                    <img class="fast_delivery_popup_descr_img"
                         src="/images/icons/fast_delivery_shop.png">
                </div>

                <div class="fast_delivery_popup_button_desc_small">
                    Выбирайте быструю доставку в течение дня из 3 500 товаров.
                </div>
                <div class="fast_delivery_popup_button_desc">Ближайшее доступное время:
                    <strong> <?= $nearestDeliveryDay['infoDate2'] . ', ' . $nearestDeliveryTimeSlot; ?></strong>
                </div>

                <button onclick="fastDeliveryPopupChoice(true);"
                        class="btn btn-lg btn-default" data-entity="basket-checkout-button">Выбрать
                </button>
            </div>
            <div class="fast_delivery_popup_buttons_block_item">
                <div class="fast_delivery_popup_descr_header"><strong>ШИРОКИЙ АССОРТИМЕНТ</strong></div>

                <div>
                    <img class="fast_delivery_popup_descr_img"
                         src="/images/icons/fast_delivery_storage.png">
                </div>

                <div class="fast_delivery_popup_button_desc_small">
                    Выбирайте не срочную доставку в прок из 15 500 товаров.
                </div>
                <div class="fast_delivery_popup_button_desc">Ближайшее доступное время:
                    <strong>
                        <?= $nearestAllProductsDeliveryDay['infoDate2'] . ', ' . $nearestAllProductsDeliveryTimeSlot; ?>
                    </strong>
                </div>

                <button onclick="fastDeliveryPopupChoice(false);"
                        class="btn btn-lg btn-default" data-entity="basket-checkout-button">Выбрать
                </button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="fast_delivery_popup_mobile" id="fast_delivery_popup_mobile">
    <div class="fast_delivery_popup_mobile_header">
        ВЫБИРАЙ УДОБНЫЙ ФОРМАТ
    </div>
    <div class="modal-body">
        <div class="fast_delivery_popup_mobile_buttons">

            <div class="fast_delivery_popup_mobile_buttons_item active"
                 id="fast_delivery_popup_mobile_shop_page"
                 onclick="$(this).addClass('active');
                 $('#fast_delivery_popup_mobile_store_page').removeClass('active');
                 $('#fast_delivery_popup_mobile_blocks_item_1').addClass('active');
                 $('#fast_delivery_popup_mobile_blocks_item_2').removeClass('active');
"
            >
                <svg class="svgicon" id="svg67" preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 60 60">
                    <rect x="0" y="0" width="60" height="60" style="fill: transparent" rx="0%" ry="0%"></rect>
                    <g transform="scale(1) translate(0, 0)">
                        <path fill="rgb(0, 0, 0)"
                              d="M20.863,10.741c-0.996,0-1.805,0.807-1.805,1.803s0.809,1.804,1.805,1.804H27.6 c0.996,0,1.803-0.808,1.803-1.804s-0.807-1.803-1.803-1.803H20.863z"></path>
                        <path fill="rgb(0, 0, 0)"
                              d="M53.201,10.318H42.447c0.871-0.218,1.467-1.046,1.355-1.96c-0.061-0.488-0.156-0.973-0.287-1.441 C42.381,2.845,38.717,0,34.602,0c-0.873,0-1.74,0.126-2.576,0.374c-0.955,0.284-1.498,1.288-1.215,2.242 c0.285,0.954,1.285,1.498,2.242,1.215c0.502-0.148,1.023-0.225,1.549-0.225c2.502,0,4.738,1.759,5.439,4.278 c0.082,0.296,0.143,0.603,0.182,0.911c0.092,0.759,0.646,1.344,1.346,1.522h-0.004h-5.764c-0.996,0-1.803,0.807-1.803,1.803 s0.807,1.804,1.803,1.804h4.596v42.469H8.602V13.925h3.852c0.996,0,1.803-0.808,1.803-1.804s-0.807-1.803-1.803-1.803H6.797 c-0.996,0-1.803,0.807-1.803,1.803v46.076C4.994,59.193,5.801,60,6.797,60h35.402c0,0,0,0,0.002,0l0,0 c0.107,0,0.217-0.013,0.324-0.033c0.033-0.006,0.066-0.015,0.1-0.022c0.072-0.018,0.143-0.039,0.213-0.066 c0.035-0.014,0.07-0.026,0.105-0.042c0.066-0.03,0.129-0.066,0.193-0.105c0.029-0.018,0.061-0.033,0.088-0.054 c0.008-0.004,0.014-0.007,0.02-0.012l4.109-2.925l4.908,2.996c0.559,0.34,1.256,0.352,1.822,0.033 c0.568-0.319,0.922-0.921,0.922-1.572V12.121C55.006,11.125,54.197,10.318,53.201,10.318z M51.398,54.983l-3.182-1.942 c-0.617-0.375-1.396-0.349-1.986,0.07l-2.229,1.588V13.925h7.396V54.983z"></path>
                        <path fill="rgb(0, 0, 0)"
                              d="M16.709,17.153c0.996,0,1.805-0.808,1.805-1.804V9.559c0-3.282,2.541-5.952,5.664-5.952 c3.125,0,5.666,2.67,5.666,5.952v5.791c0,0.996,0.807,1.804,1.803,1.804s1.805-0.808,1.805-1.804V9.559 c0-5.271-4.16-9.559-9.273-9.559c-5.111,0-9.271,4.288-9.271,9.559v5.791C14.906,16.346,15.713,17.153,16.709,17.153z"></path>
                    </g>
                </svg>
                <span>МАГАЗИН</span>
            </div>


            <div class="fast_delivery_popup_mobile_buttons_item"
                 id="fast_delivery_popup_mobile_store_page"
                 onclick="$(this).addClass('active');
                 $('#fast_delivery_popup_mobile_shop_page').removeClass('active');
                 $('#fast_delivery_popup_mobile_blocks_item_2').addClass('active');
                 $('#fast_delivery_popup_mobile_blocks_item_1').removeClass('active');
"

            >
                <svg class="svgicon" id="svg73" preserveAspectRatio="xMidYMid meet" xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 60 60">
                    <rect x="0" y="0" width="60" height="60" style="fill: transparent" rx="0%" ry="0%"></rect>
                    <g transform="scale(1) translate(0, 0)">
                        <path fill="rgb(0, 0, 0)"
                              d="M58.236,3.445h-8.067c-0.891,0-1.641,0.663-1.751,1.546l-4.417,35.538H1.764C0.79,40.529,0,41.318,0,42.293 s0.79,1.764,1.764,1.764H45.56c0.89,0,1.641-0.663,1.75-1.546l4.418-35.538h6.509C59.21,6.973,60,6.183,60,5.209 C60,4.234,59.21,3.445,58.236,3.445z"></path>
                        <path fill="rgb(0, 0, 0)"
                              d="M9.469,45.388c-3.079,0-5.583,2.505-5.583,5.584c0,3.078,2.504,5.583,5.583,5.583s5.583-2.505,5.583-5.583 C15.052,47.893,12.548,45.388,9.469,45.388z M9.469,53.026c-1.134,0-2.056-0.922-2.056-2.055c0-1.135,0.922-2.057,2.056-2.057 s2.056,0.922,2.056,2.057C11.524,52.104,10.603,53.026,9.469,53.026z"></path>
                        <path fill="rgb(0, 0, 0)"
                              d="M39.526,45.388c-3.078,0-5.582,2.505-5.582,5.584c0,3.078,2.504,5.583,5.582,5.583 c3.079,0,5.584-2.505,5.584-5.583C45.11,47.893,42.605,45.388,39.526,45.388z M39.526,53.026c-1.133,0-2.055-0.922-2.055-2.055 c0-1.135,0.922-2.057,2.055-2.057c1.134,0,2.057,0.922,2.057,2.057C41.583,52.104,40.66,53.026,39.526,53.026z"></path>
                        <path fill="rgb(0, 0, 0)"
                              d="M5.004,36.656h21.001h13.916c0.975,0,1.764-0.79,1.764-1.764V6.72c0-0.975-0.789-1.765-1.764-1.765H26.005 c-0.975,0-1.764,0.79-1.764,1.765v8.948H11.15c-0.974,0-1.764,0.789-1.764,1.764V25.7H5.004c-0.975,0-1.765,0.79-1.765,1.765v7.428 C3.239,35.866,4.029,36.656,5.004,36.656z M27.769,8.483h10.389v24.645H27.769v-5.663V17.432V8.483z M12.915,19.195h11.326V25.7 H12.915V19.195z M6.768,29.229h4.383h13.091v3.899H6.768V29.229z"></path>
                    </g>
                </svg>
                <span>СКЛАД</span>
            </div>

        </div>

        <div class="fast_delivery_popup_mobile_blocks_wrapper">
            <div class="fast_delivery_popup_mobile_blocks">

                <div class="fast_delivery_popup_mobile_blocks_item active"
                     id="fast_delivery_popup_mobile_blocks_item_1">
                    <div class="fast_delivery_popup_mobile_blocks_item_h">
                        <strong>МАГАЗИН: САМОЕ НЕОБХОДИМОЕ</strong>
                    </div>
                    <div>
                        <img class="fast_delivery_popup_mobile_blocks_item_img"
                             src="/images/icons/fast_delivery_shop.png">
                    </div>
                    <div class="fast_delivery_popup_mobile_blocks_item_desc_small">
                        Выбирайте быструю доставку в течение дня из 3 500 товаров.
                    </div>
                    <div class="fast_delivery_popup_mobile_blocks_item_desc">Ближайшее доступное время:
                        <strong> <?= $nearestDeliveryDay['infoDate2'] . ', с ' . $nearestDeliveryTimeSlot; ?></strong>
                    </div>
                    <button onclick="fastDeliveryPopupChoice(true);"
                            class="btn btn-lg btn-default" data-entity="basket-checkout-button">Выбрать
                    </button>
                </div>


                <div class="fast_delivery_popup_mobile_blocks_item second"
                     id="fast_delivery_popup_mobile_blocks_item_2">
                    <div class="fast_delivery_popup_mobile_blocks_item_h">
                        <strong>СКЛАД: ШИРОКИЙ АССОРТИМЕНТ</strong>
                    </div>
                    <div>
                        <img class="fast_delivery_popup_mobile_blocks_item_img"
                             src="/images/icons/fast_delivery_storage.png">
                    </div>
                    <div class="fast_delivery_popup_mobile_blocks_item_desc_small">
                        Выбирайте не срочную доставку в прок из 15 500 товаров.
                    </div>
                    <div class="fast_delivery_popup_mobile_blocks_item_desc">Ближайшее доступное время:
                        <strong> <?= $nearestAllProductsDeliveryDay['infoDate2'] . ', с ' . $nearestAllProductsDeliveryTimeSlot; ?></strong>
                    </div>
                    <button onclick="fastDeliveryPopupChoice(false);"
                            class="btn btn-lg btn-default" data-entity="basket-checkout-button">Выбрать
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="fast_delivery_popup" id="fast_delivery_popup_express">
    <div class="modal-header">
        <div class="fast_delivery_popup_header">
            <strong>ЗАГОЛОВОК ТЕСТОВЫЙ</strong>
        </div>
    </div>
    <div class="modal-body">
        <div class="fast_delivery_popup_buttons_block">
            <div>
                <img class=""
                     src="/images/popups/express_modal.png" style="max-width: 100%;">
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="fast_delivery_popup_mobile" id="fast_delivery_popup_express_mobile">
    <div class="fast_delivery_popup_mobile_header">
        ЗАГОЛОВОК ТЕСТОВЫЙ
    </div>
    <div class="modal-body">
        <div class="fast_delivery_popup_mobile_blocks_wrapper">
            <div>
                <img class=""
                     src="/images/popups/express_modal.png" style="max-width: 100%;">
            </div>
        </div>
    </div>
</div>