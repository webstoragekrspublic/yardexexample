$(document).ready(function () {
    $('[data-fancybox], [data-fancybox_no_scroll]').each(function () {
        var dataSrc = this.getAttribute('data-src');
        if (this.href && dataSrc) {
            this.href = dataSrc;
        }
    });
    $('.modal-header .close').click(function () {
        $.fancybox.close();
    })

    $('[data-fancybox]').fancybox({});

    $('[data-fancybox_no_scroll]').fancybox({
        scrolling: 'no',
    });

    $('[data-role="suggested_address"]').click(function (e) {
        e.preventDefault();
        showSuggestedAddressFancybox();
    });

    $('#not_found_products_form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            url: '/ajax/notFoundProductsForm.php',
            data: $(this).serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function (xhr) {
                $('#not_found_products_form input').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data.error) {
                    modalError('Произошла ошибка, попробуйте повторить действие позже.');
                } else {
                    modalSuccess('', 'Запрос успешно отправлен!');
                }
                $('#not_found_products_form input').removeAttr('disabled');
            },
            error: function () {
                modalError('Произошла ошибка, попробуйте повторить действие позже.');
                $('#not_found_products_form input').removeAttr('disabled');
            },
        });
    });

    function exitPopUp(event) {
        // If the mouse is near the top of the window, show the popup
        // Also, do NOT trigger when hovering or clicking on selects
        if (
            event.clientY < 50 &&
            event.relatedTarget == null &&
            event.target.nodeName.toLowerCase() !== 'select'
        ) {
            // Remove this event listener
            document.removeEventListener('mouseout', exitPopUp);

            if (!$.cookie('exitPopupShowed')) {
                var localKey = 'exitPopupShowed';
                var cookieLiveTime = 4 * 24 * 60 * 60 * 1000; //в мс
                var expiresIn = new Date(+new Date() + cookieLiveTime);
                var cookieParams = {
                    path: '/',
                    expires: expiresIn,
                };

                $.cookie(localKey, true, cookieParams);
                $.fancybox({
                    href: '#exit_popup',
                    scrolling: 'no',
                    padding: 0,
                });
            }
        }
    }

    if (!isShowFastDeliveryPopup()) {
        document.addEventListener('mouseout', exitPopUp);
    }

    // if (isShowFastDeliveryPopup()) {
    //     showFastDeliveryPopup()
    // }
});

function readCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}


function showFastDeliveryPopup() {
    var popup = '#fast_delivery_popup';
    if (window.innerWidth < 1000) {
        popup = '#fast_delivery_popup_mobile';
        var popup_width = 740;
        if (window.innerWidth < 780) {
            popup_width = window.innerWidth - 40;
            $(popup).css('width', popup_width)
        }
    }

    let cookieFastDelivery = readCookie("only_fast_delivery_products");
    setTimeout(function(){
        fastDeliveryPopupChoice(cookieFastDelivery != 'true');
    }, 100);
    
//    $.fancybox({
//        href: popup,
//        scrolling: 'no',
//        padding: 0,
//        closeBtn: false,
//        closeClick: false, // prevents closing when clicking INSIDE fancybox
//        openEffect: 'none',
//        closeEffect: 'none',
//        speedIn : 0,
//        speedOut : 0,
//        helpers: {
//            overlay: {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
//        }
//    });
}
function showExpressDeliveryPopup() {
    var popup = '#fast_delivery_popup_express';
    if (window.innerWidth < 1000) {
        popup = '#fast_delivery_popup_express_mobile';
        var popup_width = 740;
        if (window.innerWidth < 780) {
            popup_width = window.innerWidth - 40;
            $(popup).css('width', popup_width)
        }
    }

    if(!$(popup).length) {
        return;
    }
    $.fancybox({
        href: popup,
        scrolling: 'no',
        padding: 0,
        // closeBtn: false,
        // closeClick: false, // prevents closing when clicking INSIDE fancybox
        openEffect: 'none',
        closeEffect: 'none',
        speedIn : 0,
        speedOut : 0,
        helpers: {
            // overlay: {closeClick: false} // prevents closing when clicking OUTSIDE fancybox
        }
    });
    setCookieFastDeliveryPopup();
}

function fastDeliveryPopupChoice(val) {
    if(val && isShowFastDeliveryPopup()) {
        showExpressDeliveryPopup();
    } else {
        $.fancybox.close();
    }
    val = Boolean(val)
    setFastDeliveryCookie(val);
    setFastDeliveryCheckbox(val);
    reloadIfProductPage();
    customMultiSearch.reSearchQuery();
}

function setCookieFastDeliveryPopup() {
    var localKey = 'fastDeliveryPopupShowed';
    var cookieLiveTime = 60 * 60 * 1000; //в мс
    var expiresIn = new Date(+new Date() + cookieLiveTime);
    var cookieParams = {
        path: '/',
        expires: expiresIn,
    };
    $.cookie(localKey, true, cookieParams);
}
function isShowFastDeliveryPopup() {
    const urlParams = new URLSearchParams(window.location.search);
    const devParam = urlParams.get('dev');
    if (
        (!$.cookie('fastDeliveryPopupShowed') || devParam == 'fd')
        && document.location.pathname === "/"
    ) {
        return true;
    }
    return false;
}

const appLinkPopup = () => {
    // Remove this event listener
    document.removeEventListener('DOMContentLoaded', appLinkPopup);

    if (/Android|iPhone/i.test(navigator.userAgent)) {
        if (!$.cookie('appLinkPopupShowed')) {
            var localKey = 'appLinkPopupShowed';
            var cookieLiveTime = 3 * 1440 * 60 * 1000; //в мс
            var expiresIn = new Date(+new Date() + cookieLiveTime);

            var cookieParams = {
                path: '/',
                expires: expiresIn,
            };
            $.cookie(localKey, true, cookieParams);
            $.fancybox({
                href: '#applink_popup',
                scrolling: 'no',
                topRatio: 0.9,
                beforeShow: () => {
                    if (/Android/i.test(navigator.userAgent)) {
                        document
                            .querySelector('#applink_popup .applink_popup_btn a')
                            .setAttribute('href', 'market://details?id=com.yardex.mobile_app');
                    } else {
                        document
                            .querySelector('#applink_popup .applink_popup_btn a')
                            .setAttribute(
                                'href',
                                'https://apps.apple.com/ru/app/com.yardex.mobile-app/id1555057094'
                            );
                    }
                },
            });
        }
    }
};
// Отключаем попап "в приложении удобней"
// document.addEventListener('DOMContentLoaded', appLinkPopup);


const ingridientsPopUpShow = (items) => {
    $.fancybox({
        href: '#ingridients_added_popup',
        scrolling: 'no',
        topRatio: 0.3,

        beforeShow: () => {
            $('.ingridients_added_popup-list.added').html('');
            $('.ingridients_added_popup-list.not_added').html('');
            $('.ingridients_added_popup-to_catalog').css('display', 'none');
            items.forEach((item) => {
                if (item.image == '') {
                    item.image = '/local/templates/aspro_next_custom/images/no_photo_medium.png';
                }
                if (item.added == true) {
                    $('.ingridients_added_popup-list.added').append(
                        `<a class="ingridients_added_popup-list_link" href="${item.url}">
                            <img class="ingridients_added_popup-list_image" src="${item.image}" >
                            <div class="ingridients_added_popup-list_name">${item.name}</div>
                            <div class="ingridients_added_popup-list_count">${item.count} ${item.measure} x ${item.price} руб.</div>
                        </a>`
                    );
                } else {
                    $('.ingridients_added_popup-list.not_added').append(
                        `<a class="ingridients_added_popup-list_link" href="${item.url}"><img class="ingridients_added_popup-list_image" src="${item.image}" ><div class="ingridients_added_popup-list_name">${item.name}</div></a>`
                    );
                    $('.ingridients_added_popup-to_catalog').css('display', 'flex');
                }
            });
        },
        afterClose: () => {
            $(document).off('click', '.ingridients_added_popup-close_btn');
        },
    });
    $(document).on('click', '.ingridients_added_popup-close_btn', () => {
        $.fancybox.close();
    });
};

const showDozakazPopup = (order) => {
    let cart = order.CART;
    let orderProps = order.ORDER_PROPS;
    if (cart.items?.length) {
        $.fancybox({
            href: '#dozakaz_checkout_popup',
            scrolling: 'auto',
            height: 'autoSize',
            width: 'autorSize',
            topRatio: 0.1,
            autoResize: true,
            beforeShow: () => {
                var cartHtml = ''
                cart.items.forEach((item) => {
                    cartHtml += `<div class="dozakaz_checkout_popup__cart_item">
                            <a class="dozakaz_checkout_popup__cart_item-url" href="${item.url}">
                                <img class="dozakaz_checkout_popup__cart_item-image" height="90" src="${
                        item.image
                    }" >
                                <div class="dozakaz_checkout_popup__cart_item-name">${item.name}</div>
                            </a>
                            <div class="dozakaz_checkout_popup__cart_item-price">${item.price} руб. x ${
                        item.count + ' ' + item.measure
                    }</div>
                        <div class="dozakaz_checkout_popup__cart_item-finalPrice">
                        ${item.finalPrice} руб.
                        </div>
                        </div>`
                });

                var html = `
                <h2 class="modal_header">Оформление дозаказа</h2>
                <h3>Содержимое корзины:</h3>
                <div class="dozakaz_checkout_popup__cart">${cartHtml}</div>
                <div class="dozakaz_checkout_popup__props">
                    <div class="dozakaz_checkout_popup__prop payment">
                        <b>Тип оплаты:</b><span>${orderProps.PAYMENT.NAME}</span>
                    </div>
                    <div class="dozakaz_checkout_popup__prop delivery">
                        <b>Тип доставки:</b><span>${orderProps.DELIVERY.NAME}</span>
                    </div>
                    <div class="dozakaz_checkout_popup__prop address">
                        <b>Адрес доставки:</b><span>${orderProps.ADDRESS}</span>
                    </div>
                    <div class="dozakaz_checkout_popup__prop timeslot">
                        <b>Время доставки:</b><span>${orderProps.TIMESLOT.DATE} в ${orderProps.TIMESLOT.TIME}</span>
                    </div>
                </div>
                <div class="dozakaz_checkout_popup__total">
                    <b>Итого:</b><span>${cart.SUM} руб.</span>
                </div>
                <div class="dozakaz_checkout_popup__cart-checkout_btn">
                    <button class="btn btn-lg btn-default">Оформить дозаказ</button>
                </div>
                `
                $('#dozakaz_checkout_popup').html(html);
            },
            afterShow: () => {
                $('.dozakaz_checkout_popup__cart-checkout_btn').off('click').on('click', function (event) {
                    let element = $(event.target);
                    if (!element.hasClass('loadings')) {
                        element.addClass('loadings');
                        $.ajax({
                            type: "GET",
                            url: `/ajax/dozakaz_checkout.php?order_id=${order.LAST_ORDER_ID}`,
                            dataType: "json",
                            success: function (response) {
                                if (response.status) {
                                    $('#dozakaz_checkout_popup').html(`
                                <h2 class="modal_header">Дозаказ успешно оформлен</h2>
                                <div class="dozakaz_checkout_popup__final">
                                    <div class="dozakaz_checkout_popup__final-img">
                                        <img src="/images/final_thx.png" alt="Спасибо за покупку">
                                    </div>
                                    <h3><b>Номер вашего заказа:</b></h3>
                                    <span class="dozakaz_checkout_popup__final-order_id">${response.orderID}</span>
                                    <div class="dozakaz_checkout_popup__props">
                                        <div class="dozakaz_checkout_popup__prop address">
                                            <b>Адрес доставки:</b><span>${orderProps.ADDRESS}</span>
                                        </div>
                                        <div class="dozakaz_checkout_popup__prop timeslot">
                                            <b>Время доставки:</b><span>${orderProps.TIMESLOT.DATE} в ${orderProps.TIMESLOT.TIME}</span>
                                        </div>
                                    </div>
                                    <div class="dozakaz_checkout_popup__final-counter" style="display:none;" >Вы будете перенаправлены на страницу оплаты через <span>3</span> сек.</div>
                                </div>
                            `)
                                    if (response.orderPaymentUrl) {
                                        counterRedirect(response.orderPaymentUrl, 3);
                                    }
                                } else {
                                    $('#dozakaz_checkout_popup').html(`
                                <h2 class="modal_header">Произошла ошибка, попробуйте позже.</h2>
                            `)
                                }
                            }
                        });
                    }
                });
            }
        });
    }
};

function showSuggestedAddressFancybox() {
    $.fancybox({
        href: '#ymap_address_suggestion',
        // scrolling: 'no',
        afterShow: function () {
            $('[data-role="suggested_address_input"]').focus();
        },
    });
}

function modalSuccess(description, header) {
    if (header === undefined) {
        header = 'Успех!';
    }

    if (description === undefined) {
        description = '';
    }

    $('#modal_success_h').html(header);
    $('#modal_success_description').html(description);

    $.fancybox({
        href: '#modal_success',
        scrolling: 'no',
    });
}

function modalError(description, header) {
    if (header === undefined) {
        header = 'Ошибка!';
    }

    if (description === undefined) {
        description = '';
    }

    $('#modal_error_h').html(header);
    $('#modal_error_description').html(description);

    $.fancybox({
        href: '#modal_error',
        scrolling: 'no',
    });
}

function counterRedirect(url, timeSec) {
    $('.dozakaz_checkout_popup__final-counter').show();
    setTimeout(() => {
        document.location.href = url;
    }, timeSec * 1000);
}


