<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Context;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;

$request = Context::getCurrent()->getRequest();
$get_array = $request->getQueryList()->toArray();
$utm_keys = preg_grep("/utm_.*/", array_keys($get_array));

$all_utm_string = '';
foreach ($utm_keys as $utm_key) {
    $all_utm_string .= $utm_key . '=' . $get_array[$utm_key] . ' ';
}
if (!empty($all_utm_string)) {
    $utm_cookie = new Cookie("utm_string", $all_utm_string, time() + 86400 * 30);
    $utm_cookie->setSpread(Cookie::SPREAD_DOMAIN);
    $utm_cookie->setPath("/");
    Application::getInstance()->getContext()->getResponse()->addCookie($utm_cookie);
}
