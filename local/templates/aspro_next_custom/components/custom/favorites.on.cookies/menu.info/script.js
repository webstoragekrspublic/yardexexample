if (typeof favoritesOnCookies == 'undefined') {
    $(document).ready(function () {
        favoritesOnCookies.updateProductsDisplayInfo();
    })

    $('html').on("click", '.wish_item', function (e) {
        e.preventDefault();
        if (!$(this).hasClass("clicked")) {
            var itemId = $(this).attr('data-item');
            if ($(this).hasClass("added")) {
                favoritesOnCookies.removeItemId(itemId);
            } else {
                favoritesOnCookies.addItemId(itemId);
            }
        }
    });

    var favoritesOnCookies = {
        addItemId: function (itemId) {
            var self = this;

            if (itemId && this.ajaxUrl) {
                var $item = $('.wish_item').filter('[data-item="' + itemId + '"]');

                var data = {
                    'action': 'add',
                    'itemId': itemId
                };
                $.ajax({
                    type: "GET",
                    url: this.ajaxUrl,
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $item.addClass("clicked");
                    },
                    success: function (data) {
                        if (!data.error) {
                            $item.addClass("added");
                            self.items = data.itemIds;
                            self.updateCounterDisplayInfo();

                        } else {
                            alert(data.errorText);
                        }
                        $item.removeClass("clicked");
                    },
                    error: function (jqXHR, exception) {
                        alert('Произошла ошибка, попробуйте позже.');
                        $item.removeClass("clicked");
                    }
                });
            }
        },
        removeItemId: function (itemId) {
            var self = this;
            if (itemId && this.ajaxUrl) {
                var $item = $('.wish_item').filter('[data-item="' + itemId + '"]');

                var data = {
                    'action': 'remove',
                    'itemId': itemId
                };
                $.ajax({
                    type: "GET",
                    url: this.ajaxUrl,
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $item.addClass("clicked");
                    },
                    success: function (data) {
                        if (!data.error) {
                            $item.removeClass("added");
                            self.items = data.itemIds;
                            self.updateCounterDisplayInfo();

                        } else {
                            alert(data.errorText);
                        }
                        $item.removeClass("clicked");
                    },
                    error: function (jqXHR, exception) {
                        alert('Произошла ошибка, попробуйте позже.');
                        $item.removeClass("clicked");
                    }
                });
            }
        },
        updateCounterDisplayInfo: function () {
            var itemsNumber = Object.keys(this.items).length
            $('[data-role="favorites.on.cookies.counter"]').html(itemsNumber);
            if (itemsNumber > 0) {
                $('[data-role="favorites.on.cookies.block"]').addClass('have_items');
                $('[data-role="favorites.on.cookies.counter"]').fadeIn();
            } else {
                $('[data-role="favorites.on.cookies.block"]').removeClass('have_items');
                $('[data-role="favorites.on.cookies.counter"]').fadeOut();
            }
        },
        updateProductsDisplayInfo: function () {
            if (Object.keys(this.items).length > 0) {
                var selectors = [];
                for (var itemId in this.items) {
                    selectors.push('.wish_item[data-item="' + itemId + '"]');
                }
                $(selectors.join(',')).not(".added").addClass("added");
            }
        },
        init: function (props) {
            this.ajaxUrl = props.ajaxUrl;
            this.items = props.itemIds;
        }


    };
}