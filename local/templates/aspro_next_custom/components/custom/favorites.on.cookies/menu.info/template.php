<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */

use \Bitrix\Main\Loader;

Loader::includeModule('custom.favoritesoncookies');
$currentUserFavorites = \Custom\FavoritesOnCookies\CurrentUserFavorites::getInstance();
$ajaxUrl = $templateFolder . '/ajax.php';
?>
<a rel="nofollow"
   class="custom_favoritesoncookies"
   href="<?= $arParams['PATH_TO_FULL_FAV_PAGE']; ?>"
   title="Избранное">
    <div class="custom_favoritesoncookies_image <?= $currentUserFavorites->getTotalCnt() > 0 ? 'have_items' : ''; ?>"
         data-role="favorites.on.cookies.block"
    ></div>
</a>
<script>
    if (typeof favoritesOnCookies != 'undefined') {
        favoritesOnCookies.init({
            'ajaxUrl': '<?=$ajaxUrl; ?>',
            'itemIds': <?=json_encode($currentUserFavorites->getItemIds()); ?>
        });
    }
</script>
