<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader;
use \Custom\FavoritesOnCookies\CurrentUserFavorites;
use Bitrix\Main\Application;

if (Loader::includeModule('custom.favoritesoncookies')) {
    $currentUserFavorites = CurrentUserFavorites::getInstance();

    $request = Application::getInstance()->getContext()->getRequest();
    $action = $request->get('action');
    $itemId = $request->get('itemId');
    $answer = [
        'error' => 1,
        'errorText' => 'произошла ошибка'
    ];
    if ($itemId) {
        switch ($action) {
            case 'add':
                global $USER;
                if ($currentUserFavorites->isLimitIsReachedForItemIds()){
                    $errorText = 'Лимит избранного в '.$currentUserFavorites->getItemIdsLimit().' шт. достигнут.';
                    if (!$currentUserFavorites->isAuthorizedType()){
                        $errorText .= ' Авторизуйтесь, чтобы добавлять больше.';
                    }
                    $answer = [
                        'error' => 2,
                        'errorText' => $errorText,
                        'itemIds' => $currentUserFavorites->getItemIds()
                    ];
                } else if ($currentUserFavorites->addItemId($itemId)) {
                    $answer = [
                        'error' => 0,
                        'errorText' => '',
                        'itemIds' => $currentUserFavorites->getItemIds()
                    ];
                }
                break;
            case 'remove':
                $currentUserFavorites->removeItemId($itemId);
                $answer = [
                    'error' => 0,
                    'errorText' => '',
                    'itemIds' => $currentUserFavorites->getItemIds()
                ];
                break;
        }
    }

    echo json_encode($answer, JSON_UNESCAPED_UNICODE);
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");