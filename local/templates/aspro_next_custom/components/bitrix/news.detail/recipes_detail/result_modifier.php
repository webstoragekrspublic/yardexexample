<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION;

if (file_exists(__DIR__ . "/template_nc.php")) {
    $cp = $this->__component; // объект компонента
    if (is_object($cp)) {
        $cp->arResult["NOCACHE_TEMPLATE"] = "Y";
        foreach ($arResult as $key => $arValue)
            $cp->arResult[$key] = $arValue;

        $cp->SetResultCacheKeys(array_keys($arResult));
    }
}
