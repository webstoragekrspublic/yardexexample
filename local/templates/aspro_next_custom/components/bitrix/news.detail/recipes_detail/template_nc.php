<?

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Context;

const CATALOG_IBLOCK_ID = 114;

$request = Context::getCurrent()->getRequest();
$isAjaxAddIngridients = $request->get('ajax_addingridients') ?? false;

global $recipeProductsFilter;
$recipeProductsFilter = ['ID' => $arResult['PROPERTIES']['RECIPE_PRODUCTS']['VALUE']];

if (!$isAjaxAddIngridients) {
    $this->setFrameMode(true);
    $imgSrc = \Helpers\CustomTools::getResizedPictSrc($arResult['~DETAIL_PICTURE'], ['width' => 770, 'height' => 440]);


    $difficultIcon = '/normal.svg';
    if (isset($arResult['PROPERTIES']['COOK_DIFFICULT']['VALUE'])) {
        switch ($arResult['PROPERTIES']['COOK_DIFFICULT']['VALUE_SORT']) {
            case 1:
                $difficultIcon = '/easy.svg';
                break;
            case 3:
                $difficultIcon = '/hard.svg';
                break;
            default:
                $difficultIcon = '/normal.svg';
                break;
        }
    }

?>
    <div class="recipe-detail">
        <div class="recipe-head">
            <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])) : ?>
                <div class="recipe-detail_image_wrapper col-12 col-lg-6">
                    <picture class="recipe-detail_image">
                        <source type="image/webp" srcset="<?= Helpers\CustomTools::getWebpImgSrc($imgSrc); ?>">
                        <source type="<?= $arResult['DETAIL_PICTURE']['CONTENT_TYPE'] ?>" srcset="<?= $imgSrc; ?>">
                        <img class="recipe-detail_image" srcset="<?= $imgSrc; ?>" src="<?= $imgSrc; ?>" alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>" title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>" />
                    </picture>
                </div>
            <? endif ?>
            <div class="recipe-info col-12 col-lg-6">
                <div class="recipe-props">
                    <? if (isset($arResult['PROPERTIES']['COOK_TIME']['VALUE'])) { ?>
                        <div class="recipe-props_item cook_time">
                            <div class="recipe-props_item-icon">
                                <img src="<?= $templateFolder . '/images/clock.svg' ?>">
                            </div>
                            <div class="recipe-props_item-info">
                                <div class="recipe-props_item-info-name"><?= $arResult['PROPERTIES']['COOK_TIME']['NAME'] ?></div>
                                <div class="recipe-props_item-info-value"><?= $arResult['PROPERTIES']['COOK_TIME']['VALUE'] ?></div>
                            </div>
                        </div>
                    <? } ?>
                    <? if (isset($arResult['PROPERTIES']['COOK_DIFFICULT']['VALUE'])) { ?>
                        <div class="recipe-props_item difficult">
                            <div class="recipe-props_item-icon">
                                <img src="<?= $templateFolder . '/images' . $difficultIcon ?>">
                            </div>
                            <div class="recipe-props_item-info">
                                <div class="recipe-props_item-info-name"><?= $arResult['PROPERTIES']['COOK_DIFFICULT']['NAME'] ?></div>
                                <div class="recipe-props_item-info-value"><?= $arResult['PROPERTIES']['COOK_DIFFICULT']['VALUE'] ?></div>
                            </div>
                        </div>
                    <? } ?>
                </div>
                <? if (count($arResult['PROPERTIES']['INGREDIENTS']['VALUE'])) { ?>
                    <div class="recipe-servings_count">
                        Ингредиенты на <b><?= $arResult['PROPERTIES']['SERVINGS_COUNT']['VALUE'] ?></b><?= ' ' . getInclinationByNumber($arResult['PROPERTIES']['SERVINGS_COUNT']['VALUE'], ['порцию', 'порции', 'порций']) ?>
                    </div>
                    <div class="recipe-ingredients">

                        <? foreach ($arResult['PROPERTIES']['INGREDIENTS']['VALUE'] as $arIngredient) { ?>
                            <div class="recipe-ingredient">
                                <span class="recipe-ingredient_name">
                                    <?= $arIngredient['TYPE'] ?>
                                </span>
                                <span class="recipe-ingredient__dashed_line"></span>
                                <span class="recipe-ingredient_measure">
                                    <b>
                                        <?= $arIngredient['NAME'] ?>
                                    </b>
                                </span>
                            </div>
                        <? } ?>
                    </div>
                <? } ?>
                <? if (!empty($recipeProductsFilter['ID'])) { ?>
                    <div class="recipe-add_ingridients2basket">
                        <div>
                            <a class="recipe-add_ingridients2basket-btn btn btn-default transition_bg animate-load">Добавить ингридиенты в корзину</a>
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>
        <? if (!empty($recipeProductsFilter['ID'])) { ?>
            <div class="recipe-products">
                <div class="recipe-products_title">
                    Продукты для рецепта
                </div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:catalog.top",
                    "products_slider",
                    array(
                        "ACTION_VARIABLE" => "action",
                        "ADD_PICT_PROP" => "MORE_PHOTO",
                        "ADD_PROPERTIES_TO_BASKET" => "Y",
                        "ADD_TO_BASKET_ACTION" => "ADD",
                        "BASKET_URL" => "/basket.php",
                        "BRAND_PROPERTY" => "BRAND_REF",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "N",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "N",
                        "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
                        "COMPARE_PATH" => "",
                        "COMPATIBLE_MODE" => "N",
                        "CONVERT_CURRENCY" => "Y",
                        "CURRENCY_ID" => "RUB",
                        "CUSTOM_FILTER" => "{\"CLASS_ID\":\"CondGroup\",\"DATA\":{\"All\":\"AND\",\"True\":\"True\"},\"CHILDREN\":[]}",
                        "DATA_LAYER_NAME" => "dataLayer",
                        "DETAIL_URL" => "",
                        "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                        "DISPLAY_COMPARE" => "N",
                        "ELEMENT_COUNT" => count($recipeProductsFilter['ID']),
                        "ELEMENT_SORT_FIELD" => "sort",
                        "ELEMENT_SORT_FIELD2" => "id",
                        "ELEMENT_SORT_ORDER" => "asc",
                        "ELEMENT_SORT_ORDER2" => "desc",
                        "ENLARGE_PRODUCT" => "STRICT",
                        "FILTER_NAME" => "recipeProductsFilter",
                        "HIDE_NOT_AVAILABLE" => "L",
                        "HIDE_NOT_AVAILABLE_OFFERS" => "L",
                        "HIDE_SLIDER_PAGINATION" =>   'Y',
                        "IBLOCK_ID" => "114",
                        "IBLOCK_TYPE" => "catalog1c77",
                        "LABEL_PROP" => array(),
                        "LABEL_PROP_MOBILE" => array(),
                        "LABEL_PROP_POSITION" => "",
                        "LINE_ELEMENT_COUNT" => "",
                        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                        "MESS_BTN_BUY" => "Купить",
                        "MESS_BTN_COMPARE" => "Сравнить",
                        "MESS_BTN_DETAIL" => "Подробнее",
                        "MESS_NOT_AVAILABLE" => "Нет в наличии",
                        "MESS_RELATIVE_QUANTITY_FEW" => "мало",
                        "MESS_RELATIVE_QUANTITY_MANY" => "много",
                        "MESS_SHOW_MAX_QUANTITY" => "Наличие",
                        "OFFERS_CART_PROPERTIES" => array(),
                        "OFFERS_FIELD_CODE" => array("", ""),
                        "OFFERS_LIMIT" => "",
                        "OFFERS_PROPERTY_CODE" => array(),
                        "OFFERS_SORT_FIELD" => "sort",
                        "OFFERS_SORT_FIELD2" => "id",
                        "OFFERS_SORT_ORDER" => "asc",
                        "OFFERS_SORT_ORDER2" => "desc",
                        "OFFER_ADD_PICT_PROP" => "",
                        "OFFER_TREE_PROPS" => array(),
                        "PARTIAL_PRODUCT_PROPERTIES" => "N",
                        "PRICE_CODE" => array("Цены единицы"),
                        "PRICE_VAT_INCLUDE" => "Y",
                        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
                        "PRODUCT_DISPLAY_MODE" => "Y",
                        "PRODUCT_ID_VARIABLE" => "id",
                        "PRODUCT_PROPS_VARIABLE" => "prop",
                        "PRODUCT_QUANTITY_VARIABLE" => "",
                        "PRODUCT_ROW_VARIANTS" => "",
                        "PRODUCT_SUBSCRIPTION" => "Y",
                        "PROPERTY_CODE" => array(),
                        "PROPERTY_CODE_MOBILE" => array(),
                        "RELATIVE_QUANTITY_FACTOR" => "5",
                        "ROTATE_TIMER" => "",
                        "SECTION_URL" => "",
                        "SEF_MODE" => "N",
                        "SEF_RULE" => "",
                        "SHOW_CLOSE_POPUP" => "N",
                        "SHOW_DISCOUNT_PERCENT" => "Y",
                        "SHOW_MAX_QUANTITY" => "M",
                        "SHOW_OLD_PRICE" => "Y",
                        "SHOW_PAGINATION" => "N",
                        "SHOW_PRICE_COUNT" => "1",
                        "SHOW_SLIDER" => "Y",
                        "SLIDER_INTERVAL" => "3000",
                        "SLIDER_PROGRESS" => "N",
                        "TEMPLATE_THEME" => "blue",
                        "TITLE" => "",
                        "TITLE_IMG_ID" => '',
                        "USE_ENHANCED_ECOMMERCE" => "N",
                        "USE_PRICE_COUNT" => "N",
                        "USE_PRODUCT_QUANTITY" => "N",
                        "VIEW_MODE" => "SECTION"
                    )
                ); ?>
            </div>
        <? } ?>
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#steps" data-toggle="tab"><span>Приготовление</span></a>
                </li>

            </ul>
            <div class="tab-content">
                <div id="steps" class="tab-pane additional_block active">
                    <? foreach ($arResult['PROPERTIES']['COOKING_STEPS']['VALUE'] as $i => $stepText) { ?>
                        <div class="recipe-step_item">
                            <? if (isset($arResult['PROPERTIES']['COOKING_IMAGES']["VALUE"][$i])) { ?>
                                <? $stepImageSrc = \Helpers\CustomTools::getResizedPictSrc($arResult['PROPERTIES']['COOKING_IMAGES']["VALUE"][$i], ['width' => 350, 'height' => 350]); ?>
                                <div class="recipe-step_item-image_wrapper">
                                    <picture class="recipe-step_item-image">
                                        <source type="image/webp" srcset="<?= Helpers\CustomTools::getWebpImgSrc($stepImageSrc); ?>">
                                        <source type="JPEG" srcset="<?= $stepImageSrc; ?>">
                                        <img class="recipe-step_item-image" srcset="<?= $stepImageSrc; ?>" src="<?= $stepImageSrc; ?>" />
                                    </picture>
                                </div>
                            <? } ?>
                            <div class="recipe-step_item-info">
                                <div class="recipe-step_item-info-step_num">
                                    Шаг <?= $i + 1 ?>
                                </div>
                                <div class="recipe-step_item-info-text">
                                    <?= $stepText['TEXT'] ?>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                </div>
            </div>
        </div>
    <?
} else {
    ob_clean();

    $recipeProductsCountPropValues = [];
    foreach ($arResult['PROPERTIES']['RECIPE_PRODUCTS_COUNT']['VALUE'] as $recipeProductsCountPropItemValue) {
        $recipeProductsCountPropValues[$recipeProductsCountPropItemValue['TYPE']] = $recipeProductsCountPropItemValue['NAME'] ?? 0;
    }
    $recipeProductsRatioWithMesure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($recipeProductsFilter['ID']);
    $arSelect = array("ID", "NAME", "DETAIL_PAGE_URL", 'PREVIEW_PICTURE', 'ACTIVE',);
    $arFilter = array("IBLOCK_ID" => CATALOG_IBLOCK_ID, 'ID' => $recipeProductsFilter['ID']);
    $res = CIBlockElement::GetList(array(), $arFilter, false, array("nPageSize" => 50), $arSelect);
    $response = [];
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        if ($arFields['ACTIVE'] == 'Y') {
            if (CModule::IncludeModule("catalog")) {
                if ($recipeProductsCountPropValues[$arFields['ID']] > 0) {
                    Add2BasketByProductID($arFields['ID'], $recipeProductsCountPropValues[$arFields['ID']]);
                }
            }
            $recipeProductsPrice = CCatalogProduct::GetOptimalPrice($arFields['ID']);
            if ($recipeProductsCountPropValues[$arFields['ID']] > 0) {
                $responseItem = [
                    'added' => true,
                    'name' => $arFields['NAME'],
                    'url' => $arFields['DETAIL_PAGE_URL'],
                    'id' => $arFields['ID'],
                    'image' =>  \Helpers\CustomTools::getResizedPictSrc($arFields["PREVIEW_PICTURE"], ['width' => 85, 'height' => 100]),
                    'count' => $recipeProductsCountPropValues[$arFields['ID']],
                    'measure' => $recipeProductsRatioWithMesure[$arFields['ID']]['MEASURE']['SYMBOL_RUS'],
                    'price' => $recipeProductsPrice['RESULT_PRICE']['DISCOUNT_PRICE']
                ];
                $response[] = $responseItem;
            }
        } else {
            $responseItem = [
                'added' => false,
                'name' => $arFields['NAME'],
                'url' => $arFields['DETAIL_PAGE_URL'],
                'id' => $arFields['ID'],
                'image' =>  \Helpers\CustomTools::getResizedPictSrc($arFields["PREVIEW_PICTURE"], ['width' => 85, 'height' => 100])
            ];
            $response[] = $responseItem;
        }
    }
    echo json_encode($response);
} ?>