<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$imgSrc = \Helpers\CustomTools::getResizedPictSrc($arResult['~DETAIL_PICTURE'], ['width' => 770, 'height' => 440]);
?>
<div class="article-detail">
	<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])) : ?>
		<picture class="article-detail_picture">
			<source type="image/webp" srcset="<?= Helpers\CustomTools::getWebpImgSrc($imgSrc); ?>">
			<source type="<?= $arResult['DETAIL_PICTURE']['CONTENT_TYPE'] ?>" srcset="<?= $imgSrc; ?>">
			<img class="article-detail_picture" srcset="<?= $imgSrc; ?>" src="<?= $imgSrc; ?>" alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>" title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>" />
		</picture>
	<? endif ?>
	<? if ($arParams["DISPLAY_NAME"] != "N" && $arResult["NAME"]) : ?>
		<h3><?= $arResult["NAME"] ?></h3>
	<? endif; ?>
	<? if (strlen($arResult["DETAIL_TEXT"]) > 0) : ?>
		<div class="article-detail_descr">
			<? echo $arResult["DETAIL_TEXT"]; ?>
		</div>
	<? else : ?>
		<div class="article-detail_descr">
			<? echo $arResult["PREVIEW_TEXT"]; ?>
		</div>
	<? endif ?>
</div>