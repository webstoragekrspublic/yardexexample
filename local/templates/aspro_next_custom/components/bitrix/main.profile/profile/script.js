$(document).ready(function(){
	$('a.cancel').click(function(e){
		e.preventDefault()
		document.form1.reset();
	});
});

function addDatePicker(){
	if (document.querySelector('#inputDatePersonel')) {
		var dateElement = document.querySelector('#inputDatePersonel');
		$(document).on('click', function (event) {
			if (event.target == document.querySelector('#inputDatePersonel')) {
				BX.calendar({
					node: dateElement,
					field: dateElement,
					bTime: false
				});
			}
		});
	}
}

document.addEventListener('DOMContentLoaded', addDatePicker);