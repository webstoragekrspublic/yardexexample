function autoClickShowMoreBtn() {
    if (typeof autoClickShowMoreBtn.clickCounter == 'undefined') {
        autoClickShowMoreBtn.clickCounter = 0;
    }
    var clickLimit = 1;

    if (autoClickShowMoreBtn.clickCounter < clickLimit) {

        var btn = $(".ajax_load_btn:visible")[0];
        if (typeof btn != 'undefined' && btn) {
            var coords = btn.getBoundingClientRect();
            if (coords.top < document.documentElement.clientHeight) {
                autoClickShowMoreBtn.clickCounter++;
                clickShowMoreBtn(btn);
            }
        }
    }
}

function autoClickShowMoreBtnResetClicks(){
    autoClickShowMoreBtn.clickCounter = 0;
}

$(document).ready(function () {
    autoClickShowMoreBtn();
});
$(window).scroll(function () {
    autoClickShowMoreBtn();
});
$(window).resize(function () {
    autoClickShowMoreBtn();
});
