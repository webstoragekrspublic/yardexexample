<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"TITLE" => Array(
		"NAME" => 'Заголовок слайдера',
		"TYPE" => "STRING",
		"DEFAULT" => "",
	),
	'HIDE_SLIDER_PAGINATION' => array(
		'PARENT' => 'VISUAL',
		'NAME' => 'HIDE_SLIDER_PAGINATION',
		"TYPE" => "STRING",
		"DEFAULT" => 'N'
	),
);
