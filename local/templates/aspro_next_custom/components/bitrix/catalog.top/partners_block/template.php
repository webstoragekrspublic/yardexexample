<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<? if ($arResult["ITEMS"]): ?>
    <div class="partners_block">
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <?
            $itemBlockId = 'partners_block_'.$arItem['ID'];
            $this->AddEditAction($itemBlockId, $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
            $this->AddDeleteAction($itemBlockId, $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'));

            $imageSrc = $arItem['~PREVIEW_PICTURE'] ?
                \Helpers\CustomTools::getResizedPictSrc($arItem['~PREVIEW_PICTURE'], ['width' => 200, 'height' => 160])
                : false;
            ?>
            <div class="partners_block_item col-sm-6 col-md-3" id="<?= $this->GetEditAreaId($itemBlockId) ?>">
                <div class="partners_block_item_image">
                    <? if($imageSrc): ?>
                        <img src="<?=$imageSrc; ?>"/>
                    <? endif; ?>
                </div>
                <div class="partners_block_item_text">
                    <?=$arItem['PREVIEW_TEXT']; ?>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>

