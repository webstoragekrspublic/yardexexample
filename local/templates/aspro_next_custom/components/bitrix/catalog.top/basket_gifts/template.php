<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
$sliderID = "product_swiper_slider_" . $this->randString();
?>
<? if ($arResult["ITEMS"]): ?>
    <div class="basket-gift-products">
        <div class="basket-gift-products-header-info">
            <? if (strlen($arParams['TITLE'])): ?>
                <div class="basket-gift-products-header">
                    <h3 class="basket-gift-products-header-h"><?= $arParams['TITLE']; ?></h3>
                </div>
            <? endif; ?>
        </div>

        <table class="basket-gift-products-container">
            <tbody>
                <? foreach ($arResult["ITEMS"] as $key => $arItem):
                    $APPLICATION->IncludeComponent(
                        'bitrix:catalog.item',
                        'basket_gift',
                        array(
                            'RESULT' => array(
                                'ITEM' => $arItem,
                            ),
                        ),
                        array('HIDE_ICONS' => 'Y')
                    );
                endforeach; ?>
            </tbody>
        </table>
    </div>
<? endif; ?>
