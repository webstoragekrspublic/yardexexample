<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
$sliderID = "smi_swiper_slider_" . $this->randString();
?>
<? if ($arResult["ITEMS"]): ?>
    <div class="smi_slider">
        <div class="swiper-container" id="<?= $sliderID ?>">
            <div class="swiper-wrapper">
                <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

                    ?>
                    <div class="swiper-slide">
                        <div class="swiper_smi_card">
                            <div class="swiper_smi_card_text" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                                <?= $arItem["PREVIEW_TEXT"]; ?>
                            </div>

                            <?
                            $imageSrc = $arItem['~PREVIEW_PICTURE'] ?
                                \Helpers\CustomTools::getResizedPictSrc($arItem["~PREVIEW_PICTURE"], ['width' => 300, 'height' => 40])
                                : false;
                            ?>

                            <div class="swiper_smi_card_image">
                                <? if($imageSrc): ?>
                                    <img src="<?=$imageSrc; ?>"/>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="swiper-button-next black_arrow"></div>
        <div class="swiper-button-prev black_arrow"></div>

        <script>
            var swiper = new Swiper('#<?=$sliderID; ?>', {
                slidesPerView: 1,
                spaceBetween: 5,
                // init: false,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    800: {
                        slidesPerView: 2,
                        spaceBetween: 30,
                    },
                    1200: {
                        slidesPerView: 3,
                        spaceBetween: 30,
                    },
                    1600: {
                        slidesPerView: 4,
                        spaceBetween: 30,
                    },
                }
            });
        </script>
    </div>
<? endif; ?>
