<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
$sliderID = "main_swiper_slider_" . $this->randString();
?>
<div class="landing_top_slider">
    <div class="background_promo">
        <video autoplay muted loop class="video">
            <source src="/landing/videos/promo.mp4" type="video/mp4">
        </video>
    </div>
    <? if ($arResult["ITEMS"]) : ?>
        <div class="maxwidth-theme swiper_landing_top">
            <div class="swiper-container" id="<?= $sliderID ?>">
                <div class="swiper-wrapper">
                    <? foreach ($arResult["ITEMS"] as $key => $arItem) : ?>
                        <div class="swiper-slide">
                            <div class="swiper_landing_top_card">
                                <? if (!empty($arItem["PREVIEW_TEXT"])) : ?>
                                    <div class="swiper_landing_top_card_header">
                                        <?= $arItem["PREVIEW_TEXT"]; ?>
                                    </div>
                                <? endif; ?>


                                <?
                                $imgId = $arItem["~PREVIEW_PICTURE"] ?? false;
                                if ($imgId) {
                                    $imgSrc = \Helpers\CustomTools::getResizedPictSrc($imgId, ['width' => 555, 'height' => 310]);
                                } else {
                                    $imgSrc = SITE_TEMPLATE_PATH . '/images/no_photo_medium.png';
                                }
                                $imgWebpPath = \Helpers\CustomTools::getWebpImgSrc($imgSrc);
                                ?>

                                <picture>
                                    <? if ($imgWebpPath) : ?>
                                        <source srcset="<?= $imgWebpPath; ?>" type="image/webp">
                                    <? endif; ?>
                                    <source srcset="<?= $imgSrc ?>">
                                    <img src="<?= $imgSrc ?>" alt="<?= $a_alt; ?>" title="<?= $a_title; ?>" class="swiper_landing_top_card_img" />
                                </picture>
                                <div class="swiper_landing_top_card_img_shadow"></div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>

        <script>
            var swiper = new Swiper('#<?= $sliderID; ?>', {
                slidesPerView: 'auto',
                // centeredSlides: true,
                freeMode: true,
                spaceBetween: 30,
                // init: false,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                // breakpoints: {
                //     1200: {
                //         slidesPerView: 2,
                //         spaceBetween: 5,
                //     },
                // }
            });
        </script>

    <? endif; ?>

</div>