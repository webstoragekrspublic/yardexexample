<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
$bytes = random_bytes(5);

$sliderID = "product_swiper_slider_" . bin2hex($bytes);
?>
<? if ($arResult["ITEMS"]) : ?>
    <div class="swiper_products">
        <div class="swiper_products_header_info">
            <? $imgSrc = $templateFolder . '/images/heart.svg'; ?>
            <? if ($imgSrc) : ?>
                <div class="swiper_products_header_img">
                    <picture>
                        <source type="image/swg" srcset="<?= $imgSrc; ?>">
                        <img srcset="<?= $imgSrc; ?>" src="<?= $imgSrc; ?>" />
                    </picture>
                </div>
            <? endif; ?>
            <? if (strlen($arParams['TITLE'])) : ?>
                <div class="swiper_products_header">
                    <h3 class="swiper_products_header_h"><?= $arParams['TITLE']; ?></h3>
                    <a class="swiper_products_header_all" href="<?= $arParams['SECTION_URL']; ?>">Смотреть все</a>
                </div>
            <? endif; ?>
            <? $imgSrc = $templateFolder . '/images/help-circle.svg'; ?>
            <? if ($imgSrc) : ?>
                <div class="swiper_products_header_img otp_help">
                    <picture>
                        <source type="image/swg" srcset="<?= $imgSrc; ?>">
                        <img srcset="<?= $imgSrc; ?>" src="<?= $imgSrc; ?>" />
                    </picture>
                    <div class="otp_help_descr">
                        Это подборка товаров, которые вы <b>заказываете чаще всего</b>.<br>
                        <?/*  При добавлении в корзину вы получаете дополнительную <b>скидку</b> на них. */ ?>
                    </div>
                </div>
            <? endif; ?>
        </div>
        <div class="swiper-container catalog_block items block_list" id="<?= $sliderID ?>">
            <div class="swiper-wrapper">
                <? $cnt = 0; ?>
                <? foreach ($arResult["ITEMS"] as $key => $arItem) :
                    $cnt++;
                    $SHOW_MODE = $cnt > 5 ? 'SWIPER_LAZY_LOAD' : '';
                ?>
                    <div class="swiper-slide">
                        <? $APPLICATION->IncludeComponent(
                            'bitrix:catalog.item',
                            'catalog',
                            array(
                                'RESULT' => array(
                                    'ITEM' => $arItem,
                                    'AREA_ID' => $areaIds[$arItem['ID']],
                                    'TYPE' => $rowData['TYPE'],
                                    'BIG_LABEL' => 'N',
                                    'BIG_DISCOUNT_PERCENT' => 'N',
                                    'BIG_BUTTONS' => 'N',
                                    'SCALABLE' => 'N',
                                    'SET_BOOTSTRAP_COL' => 'N'
                                ),
                                'PARAMS' => [
                                    'SHOW_MODE' => $SHOW_MODE
                                ]
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );
                        ?>
                    </div>
                <? endforeach; ?>
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <? if (!isset($arParams['HIDE_SLIDER_PAGINATION']) or !$arParams['HIDE_SLIDER_PAGINATION'] == 'Y') : ?>
                <div class="swiper-pagination"></div>
            <? endif; ?>
        </div>
    </div>


    <script>
        new Swiper('#<?= $sliderID; ?>', {
            slidesPerView: 1,
            spaceBetween: 10,
            lazy: true,
            // init: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 5,
                },
                840: {
                    slidesPerView: 3,
                    spaceBetween: 5,
                },
                1110: {
                    slidesPerView: 4,
                    spaceBetween: 5,
                },
                1320: {
                    slidesPerView: 5,
                    spaceBetween: 5,
                },
                1560: {
                    slidesPerView: 6,
                    spaceBetween: 5,
                },
            }
        });
    </script>

<? endif; ?>