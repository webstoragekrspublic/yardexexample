<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $componentPath
 * @var CBitrixComponent $component
 */
$h1 =
$title = 'Товары со сроком годности';

global ${$arParams['FILTER_NAME']};
$filter = &${$arParams['FILTER_NAME']};
$filter['!PROPERTY_'.\Helpers\Constants::PROP_EXPIRE_SOON] = [false, 0];

$itemIds = [];
$rs = \CIBlockElement::GetList([], ['ACTIVE' => 'Y', '!PROPERTY_'.\Helpers\Constants::PROP_EXPIRE_SOON => [false, 0]], false, ['nTopCount' => 1], ['ID']);
$hasItems = (bool) $rs->Fetch();

$itemsCnt = $hasItems > 0 ? 1 : 0;
$iSectionsCount = 0;
if($hasItems) {
    $arParams['SHOW_ALL_WO_SECTION'] = 'Y';
    
} else {
    $arParams['INCLUDE_SUBSECTIONS'] = 'N';
}
$arParams['USE_FILTER'] = 'N';
$arParams['SHOW_MANUFACTURE_DATE'] = 'Y';
$arParams['SHOW_EXPIRATION_QUANTITY'] = 'Y';
$arParams['CUSTOM_PRICE_PROP'] = \Helpers\Constants::PROP_EXPIRE_PRICE;


$arParams["SECTIONS_TYPE_VIEW"] = $arParams["SECTION_ELEMENTS_TYPE_VIEW"];


include 'sections.php';

$APPLICATION->SetTitle($h1);
$APPLICATION->SetPageProperty('title', $title);
$APPLICATION->AddChainItem($title);