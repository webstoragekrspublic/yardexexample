<?
$display = "block";
$template = "catalog_" . $display;
?>

<? $APPLICATION->IncludeComponent(
	"custom:empty_component",
	"catalog_sort_block",
	[
		'SORT_PRICES' => $arParams['SORT_PRICES'],
	],
	false
); ?>

<?
$templateData = \Helpers\GlobalStorage::get('catalog_sort_block_templateData');
$sort = $templateData['sort'] ?? 'NAME';
$sort_order = $templateData['sort_order'] ?? 'asc';
