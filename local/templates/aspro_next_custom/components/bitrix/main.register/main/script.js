function addDatePicker(){
    if (document.querySelector('#input_PERSONAL_BIRTHDAY')) {
        var dateElement = document.querySelector('#input_PERSONAL_BIRTHDAY');
        $(document).on('click', function (event) {
            if (event.target == document.querySelector('#input_PERSONAL_BIRTHDAY')) {
                BX.calendar({
                    node: dateElement,
                    field: dateElement,
                    bTime: false
                });
            }
        });
    }
}

function dashaMailClientRegistration(){
    var clientName, clientEmail, clientPhone = null;
    if (document.querySelector('#input_NAME') && document.querySelector('#input_EMAIL') && document.querySelector('#input_PERSONAL_PHONE')){
        clientName = document.querySelector('#input_NAME').value;
        clientEmail = document.querySelector('#input_EMAIL').value;
        clientPhone = document.querySelector('#input_PERSONAL_PHONE').value;

        dashamail("async", {
            "operation": "UserRegistration",
            "data": {
                "customer": {
                    "name": clientName,
                    "email": clientEmail,
                    "mobilePhone": clientPhone
                }
            }
        });
    }
}

document.addEventListener('DOMContentLoaded', addDatePicker);

$(document).ready(function() {
    $("#registraion-page-form button[type=submit]").on('click', dashaMailClientRegistration);
});