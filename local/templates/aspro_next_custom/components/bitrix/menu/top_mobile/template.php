<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true);?>
<?if($arResult):?>
	<div class="menu top">
		<ul class="top">
			<?foreach($arResult as $arItem):?>
				<?$bShowChilds = $arParams['MAX_LEVEL'] > 1;?>
				<?$bParent = $arItem['CHILD'] && $bShowChilds;?>
				<li<?=($arItem['SELECTED'] ? ' class="selected"' : '')?>>
					<a class="dark-color<?=($bParent ? ' parent' : '')?> <?=$arItem["PARAMS"]["CLASS"]; ?>" href="<?=$arItem["LINK"]?>" title="<?=$arItem["TEXT"]?>">

						<span><?=$arItem['TEXT']?></span>
						<?if($bParent):?>
							<span class="arrow"><i class="svg svg_triangle_right"></i></span>
						<?endif;?>
					</a>
					<?if($bParent):?>
						<ul class="dropdown">
							<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><i class="svg svg-arrow-right"></i><?=GetMessage('NEXT_T_MENU_BACK')?></a></li>
							<li class="menu_title">
                                <a href="<?=$arItem['LINK'];?>"><?=$arItem['TEXT']?></a>
                            </li>
							<?foreach($arItem['CHILD'] as $arSubItem):?>
								<?$bShowChilds = $arParams['MAX_LEVEL'] > 2;?>
								<?$bParent = $arSubItem['CHILD'] && $bShowChilds;?>
								<li<?=($arSubItem['SELECTED'] ? ' class="selected"' : '')?>>
									<a class="dark-color<?=($bParent ? ' parent' : '')?>" href="<?=$arSubItem["LINK"]?>" title="<?=$arSubItem["TEXT"]?>">

                                        <?
                                        $img = $arSubItem['PARAMS']['PICTURE']?CFile::ResizeImageGet($arSubItem['PARAMS']['PICTURE'], array( "width" => 40, "height" => 40 ), BX_RESIZE_IMAGE_PROPORTIONAL):false;
                                        $imgx2 = $arSubItem['PARAMS']['PICTURE']?CFile::ResizeImageGet($arSubItem['PARAMS']['PICTURE'], array( "width" => 80, "height" => 80 ), BX_RESIZE_IMAGE_PROPORTIONAL):false;  //srcset="image/mushroom_retina.jpg 2x" alt=""
                                        ?>
                                        <? if($img): ?>
                                                <div class="menu_img"><img src="<?=$img['src']; ?>" srcset="<?=$imgx2['src']; ?> 2x"><span>&nbsp;&nbsp;<?=$arSubItem['TEXT']?></span></div>
                                            <? else: ?>
                                                <span><?=$arSubItem['TEXT']?></span>
                                        <? endif; ?>

										<?if($bParent):?>
											<span class="arrow"><i class="svg svg_triangle_right"></i></span>
										<?endif;?>
									</a>
									<?if($bParent):?>
										<ul class="dropdown">
											<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><i class="svg svg-arrow-right"></i><?=GetMessage('NEXT_T_MENU_BACK')?></a></li>
											<li class="menu_title">
                                                <a href="<?=$arSubItem['LINK'];?>">
                                                    <? if($img): ?>
                                                        <div class="menu_img"><img src="<?=$img['src']; ?>" srcset="<?=$imgx2['src']; ?> 2x">&nbsp;&nbsp;<?=$arSubItem['TEXT']?></div>
                                                    <? else: ?>
                                                        <?=$arSubItem['TEXT']?>
                                                    <? endif; ?>
                                                </a>
                                            </li>
											<?foreach($arSubItem["CHILD"] as $arSubSubItem):?>
												<?$bShowChilds = $arParams['MAX_LEVEL'] > 3;?>
												<?$bParent = $arSubSubItem['CHILD'] && $bShowChilds;?>
												<li<?=($arSubSubItem['SELECTED'] ? ' class="selected"' : '')?>>
													<a class="dark-color<?=($bParent ? ' parent' : '')?>" href="<?=$arSubSubItem["LINK"]?>" title="<?=$arSubSubItem["TEXT"]?>">
														<span><?=$arSubSubItem['TEXT']?></span>
														<?if($bParent):?>
															<span class="arrow"><i class="svg svg_triangle_right"></i></span>
														<?endif;?>
													</a>
													<?if($bParent):?>
														<ul class="dropdown">
															<li class="menu_back"><a href="" class="dark-color" rel="nofollow"><i class="svg svg-arrow-right"></i><?=GetMessage('NEXT_T_MENU_BACK')?></a></li>
															<li class="menu_title"><a href="<?=$arSubSubItem['LINK'];?>"><?=$arSubSubItem['TEXT']?></a></li>
															<?foreach($arSubSubItem["CHILD"] as $arSubSubSubItem):?>
																<li<?=($arSubSubSubItem['SELECTED'] ? ' class="selected"' : '')?>>
																	<a class="dark-color" href="<?=$arSubSubSubItem["LINK"]?>" title="<?=$arSubSubSubItem["TEXT"]?>">
																		<span><?=$arSubSubSubItem['TEXT']?></span>
																	</a>
																</li>
															<?endforeach;?>
														</ul>
													<?endif;?>
												</li>
											<?endforeach;?>
										</ul>
									<?endif;?>
								</li>
							<?endforeach;?>
						</ul>
					<?endif;?>
				</li>
			<?endforeach;?>
		</ul>
	</div>
<?endif;?>