<?
//$arResult = CNext::getChilds($arResult);
global $arRegion, $arTheme;

//собираем подпункты на основании что у родителя ссылка = ''
if ($arResult) {
    $parentKey = false;
    foreach ($arResult as $key => $arItem) {
        if (!trim($arItem['LINK'])) {
            $parentKey = $key;
        } else if($parentKey !== false) {
            $arResult[$parentKey]['CHILD'][] = $arItem;
            unset($arResult[$key]);
        }
    }
}
