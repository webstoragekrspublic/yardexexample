$(document).ready(function() {
    $('.footer_mobile_top .footer_mobile_top_header').off('click').on('click',function(e){
        var $parent = $(this).closest('.footer_mobile_top');
        $(this).toggleClass('opened');
        $parent.children('.footer_mobile_common_items').stop().slideToggle(400);
    })
});