<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$this->setFrameMode(true);
?>
<? if ($arResult): ?>

    <div class="footer_mobile_top">
        <div class="footer_mobile_top_header">
            <?=$arResult[0]['TEXT']; ?>
            <? unset($arResult[0]); ?>
        </div>
        <div class="footer_mobile_common_items">
            <? foreach ($arResult as $i => $arItem): ?>
                <?
                $bLink = strlen($arItem['LINK']);
                ?>
                <div class="footer_mobile_common_item <?= ($arItem["SELECTED"] ? " active" : "") ?>">
                    <? if ($bLink): ?>
                        <a href="<?= $arItem['LINK'] ?>" class="a_standart_color"><?= $arItem['TEXT'] ?></a>
                    <? else: ?>
                        <span><?= $arItem['TEXT'] ?></span>
                    <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>


<? endif; ?>