<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$this->setFrameMode(true);
?>
<? if ($arResult): ?>
    <? foreach ($arResult as $i => $arItem): ?>
        <?
        $class = $i === 0 ? 'footer_top_logo' : 'footer_common_item';
        $bLink = strlen($arItem['LINK']);
        ?>
        <div class="<?=$class; ?> <?= ($arItem["SELECTED"] ? " active" : "") ?>">
            <? if ($bLink): ?>
                <a href="<?= $arItem['LINK'] ?>" class="a_standart_color"><?= $arItem['TEXT'] ?></a>
            <? else: ?>
                <span><?= $arItem['TEXT'] ?></span>
            <? endif; ?>
        </div>
    <? endforeach; ?>
<? endif; ?>