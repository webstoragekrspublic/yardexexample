<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>

<? foreach ($arResult as $arItem): ?>
    <div class="mobile_menu_list_top_item ">
        <a class="a_standart_color" href="<?=$arItem['LINK']; ?>">
            <?=$arItem['TEXT']; ?>
        </a>
    </div>
<? endforeach; ?>
