BX.ready(function () {
    renderDozakazBtn();
    const ctx = this;
    BX.addCustomEvent('OnBasketChange', function () {
        ctx.renderDozakazBtn();
    });
});

function renderDozakazBtn() {
    const orders_wpapper = $('.orders_wrapper .dozakaz-section');
    if (orders_wpapper.length) {
        $.ajax({
            type: 'GET',
            url: '/ajax/dozakaz_available.php',
            dataType: 'JSON',
            success: function (response) {
                if (response.CAN_DOZAKAZ) {
                    orders_wpapper.html(
                        '<div class="dozakaz-section">' +
                            '<div class="dozakaz-info">' +
                            '<span class="dozakaz-info_text">' +
                            `${
                                response.NOT_EMPTY_CART
                                    ? `У вас есть недавний заказ №${response.LAST_ORDER_ID}, вы можете оформить заказ-дополнение к этому заказу, на ${response.DALIVERY_DATE} в ${response.DALIVERY_TIME}. Текущая корзина объединится в предыдущим заказом.`
                                    : `У вас есть недавний заказ №${response.LAST_ORDER_ID}, вы можете оформить заказ-дополнение к этому заказу, на ${response.DALIVERY_DATE} в ${response.DALIVERY_TIME}. Для этого выберите и добавьте в корзину необходимые товары, после этого кнопка станет активной.`
                            }` +
                            '</span>' +
                            '</div>' +
                            `<div class="dozakaz-btn  ${
                                response.NOT_EMPTY_CART ? '' : 'disabled'
                            }">` +
                            '<button class="btn btn-lg btn-default">' +
                            'Оформить дозаказ' +
                            '</button>' +
                            '</div>' +
                            '</div>'
                    );
                    $(document).off('click', '.dozakaz-btn .btn');
                    if (response.NOT_EMPTY_CART) {
                        
                        $(document).on('click', '.dozakaz-btn .btn', function () {
                            if(response.CART.items && response.CART.items.length && response.ORDER_PROPS){
                                showDozakazPopup(response);
                            }                           
                        });
                    }
                }
            },
            error: function () {
                console.log('ошибка проверки дозаказа');
            },
        });
    }
}
