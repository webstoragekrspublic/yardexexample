<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;

$defaultCurrency = 'RUB';
if ($arParams['GUEST_MODE'] !== 'Y')
{
	Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
	Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
}
$this->addExternalCss("/bitrix/css/main/bootstrap.css");

CJSCore::Init(array('clipboard', 'fx'));

$APPLICATION->SetTitle("");

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach ($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}

	$component = $this->__component;

	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}
}
else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach ($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	?>
    <div class="container-fluid sale-order-detail">
        <div class="sale-order-detail-title-container">
            <h1 class="sale-order-detail-title-element">
                <?= Loc::getMessage('SPOD_LIST_MY_ORDER', array(
                    '#ACCOUNT_NUMBER#' => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
                    '#DATE_ORDER_CREATE#' => $arResult["DATE_INSERT_FORMATED"]
                )) ?>
            </h1>
        </div>
        <?
        if ($arParams['GUEST_MODE'] !== 'Y')
        {
            ?>
            <a class="sale-order-detail-back-to-list-link-up" href="/personal/orders/">
                &larr; <?= Loc::getMessage('SPOD_RETURN_LIST_ORDERS') ?>
            </a>
            <?
        }
        ?>
        <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-general">
            <div class="row">
                <div class="col-md-12 cols-sm-12 col-xs-12 sale-order-detail-general-head">
					<span class="sale-order-detail-general-item">
						<?= Loc::getMessage('SPOD_SUB_ORDER_TITLE', array(
                            "#ACCOUNT_NUMBER#"=> htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"]),
                            "#DATE_ORDER_CREATE#"=> $arResult["DATE_INSERT_FORMATED"]
                        ))?>
                        <?= count($arResult['BASKET']);?>
                        <?
                        $count = count($arResult['BASKET']) % 10;
                        if ($count == '1')
                        {
                            echo Loc::getMessage('SPOD_TPL_GOOD');
                        }
                        elseif ($count >= '2' && $count <= '4')
                        {
                            echo Loc::getMessage('SPOD_TPL_TWO_GOODS');
                        }
                        else
                        {
                            echo Loc::getMessage('SPOD_TPL_GOODS');
                        }
                        ?>
                        <?=Loc::getMessage('SPOD_TPL_SUMOF')?>
                        <?=$arResult["PRICE_FORMATED"]?>
					</span>
                </div>
            </div>

            <div class="row sale-order-detail-about-order">

                <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-title">
                            <h3 class="sale-order-detail-about-order-title-element">
                                <?= Loc::getMessage('SPOD_LIST_ORDER_INFO') ?>
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-inner-container">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 sale-order-detail-about-order-inner-container-name">
                                    <div class="sale-order-detail-about-order-inner-container-name-title">
                                        <?
                                        $userName = $arResult["USER_NAME"];
                                        if (strlen($userName) || strlen($arResult['FIO']))
                                        {
                                            echo Loc::getMessage('SPOD_LIST_FIO').':';
                                        }
                                        else
                                        {
                                            echo Loc::getMessage('SPOD_LOGIN').':';
                                        }
                                        ?>
                                    </div>
                                    <div class="sale-order-detail-about-order-inner-container-name-detail">
                                        <?
                                        if (strlen($userName))
                                        {
                                            echo htmlspecialcharsbx($userName);
                                        }
                                        elseif (strlen($arResult['FIO']))
                                        {
                                            echo htmlspecialcharsbx($arResult['FIO']);
                                        }
                                        else
                                        {
                                            echo htmlspecialcharsbx($arResult["USER"]['LOGIN']);
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6 sale-order-detail-about-order-inner-container-status">
                                    <div class="sale-order-detail-about-order-inner-container-status-title">
                                        <?= Loc::getMessage('SPOD_LIST_CURRENT_STATUS', array(
                                            '#DATE_ORDER_CREATE#' => $arResult["DATE_INSERT_FORMATED"]
                                        )) ?>
                                    </div>
                                    <div class="sale-order-detail-about-order-inner-container-status-detail">
                                        <?
                                        if ($arResult['CANCELED'] !== 'Y')
                                        {
                                            echo htmlspecialcharsbx($arResult["STATUS"]["NAME"]);
                                        }
                                        else
                                        {
                                            echo Loc::getMessage('SPOD_ORDER_CANCELED');
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?
                                $indexBasePrice = 0;
                                foreach ($arResult['BASKET'] as $basketItem)
                                {
                                ?>
                                    <? if ($basketItem['BASE_PRICE'] > $basketItem['PRICE']): ?>
                                        <? $indexBasePrice+=$basketItem['BASE_PRICE']*$basketItem['QUANTITY']?>
                                    <? else: ?>
                                        <? $indexBasePrice+=$basketItem['BASE_PRICE']*$basketItem['QUANTITY']?>
                                    <? endif; ?>
                                <?
                                }
                                ?>
                                <?
                                $couponList = \Bitrix\Sale\Internals\OrderCouponsTable::getList(array(
                                    'select' => array('COUPON'),
                                    'filter' => array('=ORDER_ID' => $arResult['ID'])
                                ));
                                $couponFormat = "Промокод: ";
                                if ($coupon = $couponList->fetch()) $couponFormat .= $coupon['COUPON'];
                                else $couponFormat = "";
                                ?>

                                <div class="col-md-<?=($arParams['GUEST_MODE'] !== 'Y') ? 2 : 4?> col-sm-6 sale-order-detail-about-order-inner-container-price">
                                    <div class="sale-order-detail-about-order-inner-container-price-title">
                                        <?= Loc::getMessage('SPOD_ORDER_PRICE')?>:
                                    </div>
                                    <div class="sale-order-detail-about-order-inner-container-price-detail">
                                        <?= $arResult["PRICE_FORMATED"]?>
                                    </div>
                                    <div style="text-decoration: line-through; color: #a3a7a7;" class="sale-order-detail-about-order-inner-container-price-detail"><?= CurrencyFormat($indexBasePrice, 'RUB')?></div>
                                    <div style="color: #545555;" class="sale-order-detail-about-order-inner-container-price-detail"><?= $couponFormat?></div>
                                </div>
                                <?
                                if ($arParams['GUEST_MODE'] !== 'Y')
                                {
                                    ?>
                                    <div class="col-md-2 col-sm-6 sale-order-detail-about-order-inner-container-repeat">
                                        <a href="<?=$arResult["URL_TO_COPY"]?>" class="sale-order-detail-about-order-inner-container-repeat-button">
                                            <?= Loc::getMessage('SPOD_ORDER_REPEAT') ?>
                                        </a>
                                        <?
                                        /* if ($arResult["CAN_CANCEL"] === "Y")
                                        {
                                            ?>
                                            <a href="<?=$arResult["URL_TO_CANCEL"]?>" class="sale-order-detail-about-order-inner-container-repeat-cancel">
                                                <?= Loc::getMessage('SPOD_ORDER_CANCEL') ?>
                                            </a>
                                            <?
                                        } */
                                        ?>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-about-order-inner-container-details">
                                <h4 class="sale-order-detail-about-order-inner-container-details-title">
                                    <?= Loc::getMessage('SPOD_USER_INFORMATION') ?>
                                </h4>
                                <ul class="sale-order-detail-about-order-inner-container-details-list">
                                    <?
                                    if (strlen($arResult["USER"]["LOGIN"]) && !in_array("LOGIN", $arParams['HIDE_USER_INFO']))
                                    {
                                        ?>
                                        <li class="sale-order-detail-about-order-inner-container-list-item">
                                            <?= Loc::getMessage('SPOD_LOGIN')?>:
                                            <div class="sale-order-detail-about-order-inner-container-list-item-element">
                                                <?= htmlspecialcharsbx($arResult["USER"]["LOGIN"]) ?>
                                            </div>
                                        </li>
                                        <?
                                    }
                                    if (strlen($arResult["USER"]["EMAIL"]) && !in_array("EMAIL", $arParams['HIDE_USER_INFO']))
                                    {
                                        ?>
                                        <li class="sale-order-detail-about-order-inner-container-list-item">
                                            <?= Loc::getMessage('SPOD_EMAIL')?>:
                                            <a class="sale-order-detail-about-order-inner-container-list-item-link"
                                               href="mailto:<?= htmlspecialcharsbx($arResult["USER"]["EMAIL"]) ?>"><?= htmlspecialcharsbx($arResult["USER"]["EMAIL"]) ?></a>
                                        </li>
                                        <?
                                    }
                                    if (strlen($arResult["USER"]["PERSON_TYPE_NAME"]) && !in_array("PERSON_TYPE_NAME", $arParams['HIDE_USER_INFO']))
                                    {
                                        ?>
                                        <li class="sale-order-detail-about-order-inner-container-list-item">
                                            <?= Loc::getMessage('SPOD_PERSON_TYPE_NAME') ?>:
                                            <div class="sale-order-detail-about-order-inner-container-list-item-element">
                                                <?= htmlspecialcharsbx($arResult["USER"]["PERSON_TYPE_NAME"]) ?>
                                            </div>
                                        </li>
                                        <?
                                    }
                                    if (isset($arResult["ORDER_PROPS"]))
                                    {
                                        foreach ($arResult["ORDER_PROPS"] as $property)
                                        {
                                            ?>
                                            <li class="sale-order-detail-about-order-inner-container-list-item">
                                                <?= htmlspecialcharsbx($property['NAME']) ?>:
                                                <div class="sale-order-detail-about-order-inner-container-list-item-element">
                                                    <?
                                                    if ($property["TYPE"] == "Y/N")
                                                    {
                                                        echo Loc::getMessage('SPOD_' . ($property["VALUE"] == "Y" ? 'YES' : 'NO'));
                                                    }
                                                    else
                                                    {
                                                        if ($property['MULTIPLE'] == 'Y'
                                                            && $property['TYPE'] !== 'FILE'
                                                            && $property['TYPE'] !== 'LOCATION')
                                                        {
                                                            $propertyList = unserialize($property["VALUE"]);
                                                            foreach ($propertyList as $propertyElement)
                                                            {
                                                                echo $propertyElement . '</br>';
                                                            }
                                                        }
                                                        elseif ($property['TYPE'] == 'FILE')
                                                        {
                                                            echo $property["VALUE"];
                                                        }
                                                        else
                                                        {
                                                            echo htmlspecialcharsbx($property["VALUE"]);
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </li>
                                            <?
                                        }
                                    }
                                    ?>
                                </ul>
                                <?
                                if (strlen($arResult["USER_DESCRIPTION"]))
                                {
                                    ?>
                                    <h4 class="sale-order-detail-about-order-inner-container-details-title sale-order-detail-about-order-inner-container-comments">
                                        <?= Loc::getMessage('SPOD_ORDER_DESC') ?>
                                    </h4>
                                    <div class="col-xs-12 sale-order-detail-about-order-inner-container-list-item-element">
                                        <?=nl2br(htmlspecialcharsbx($arResult["USER_DESCRIPTION"]))?>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row sale-order-detail-payment-options-order-content">

                <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-payment-options-order-content-container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 sale-order-detail-payment-options-order-content-title">
                            <h3 class="sale-order-detail-payment-options-order-content-title-element">
                                <?= Loc::getMessage('SPOD_ORDER_BASKET')?>
                            </h3>
                        </div>
                        <div class="sale-order-detail-order-section bx-active">
                            <div class="sale-order-detail-order-section-content container-fluid">
                                <div class="sale-order-detail-order-table-fade sale-order-detail-order-table-fade-right">
                                    <div style="width: 100%; overflow-x: auto; overflow-y: hidden;">
                                        <div class="sale-order-detail-order-item-table">
                                            <div class="sale-order-detail-order-item-tr hidden-sm hidden-xs">
                                                <div class="sale-order-detail-order-item-td" style="padding-bottom: 5px;">
                                                    <div class="sale-order-detail-order-item-td-title">
                                                        <?= Loc::getMessage('SPOD_NAME')?>
                                                    </div>
                                                </div>
                                                <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right" style="padding-bottom: 5px;">
                                                    <div class="sale-order-detail-order-item-td-title">
                                                        <?= Loc::getMessage('SPOD_PRICE')?>
                                                    </div>
                                                </div>
                                                <div class="sale-order-detail-order-item-nth-4p1"></div>
                                                <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-center" style="padding-bottom: 5px;">
                                                    <div class="sale-order-detail-order-item-td-title">
                                                        <?= Loc::getMessage('SPOD_QUANTITY')?>
                                                    </div>
                                                </div>
                                                <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right" style="padding-bottom: 5px;">
                                                    <div class="sale-order-detail-order-item-td-title">
                                                        <?= Loc::getMessage('SPOD_ORDER_PRICE')?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?
                                            foreach ($arResult['BASKET'] as $basketItem)
                                            {
                                                $arItem = $arResult['ORDER_CHANGE_INFO']['PRODUCTS'][$basketItem['PRODUCT_ID']];
                                                $isChangedQty = (isset($arItem['CURRENT_INFO']['QUANTITY'], $arItem['PAST_INFO']['QUANTITY']) and $arItem['CURRENT_INFO']['QUANTITY'] != $arItem['PAST_INFO']['QUANTITY']) ? true : false;
                                                $isAdded = (isset($arItem['CURRENT_INFO']['QUANTITY']) && !isset($arItem['PAST_INFO']['QUANTITY'])) ? true : false;
                                                $isOk = (!$isChangedQty && !$isAdded) ? true : false;



                                                $detailPageUrl = $basketItem['DETAIL_PAGE_URL'];
                                                if(mb_substr($detailPageUrl,0,1,"UTF-8") != '/')
                                                {
                                                    $detailPageUrl = '/'.$detailPageUrl;
                                                }
                                                ?>
                                                <div class="sale-order-detail-order-item-tr sale-order-detail-order-basket-info sale-order-detail-order-item-tr-first">
                                                    <div class="sale-order-detail-order-item-td" style="min-width: 300px;">
                                                        <div class="sale-order-detail-order-item-block">
                                                            <div class="sale-order-detail-order-item-img-block">
                                                                <a href="<?=$detailPageUrl?>">
                                                                    <?
                                                                    if (strlen($basketItem['PICTURE']['SRC']))
                                                                    {
                                                                        $imageSrc = $basketItem['PICTURE']['SRC'];
                                                                    }
                                                                    else
                                                                    {
                                                                        $imageSrc = $this->GetFolder().'/images/no_photo.png';
                                                                    }
                                                                    ?>
                                                                    <div class="sale-order-detail-order-item-imgcontainer"
                                                                         style="background-image: url(<?=$imageSrc?>);
                                                                                 background-image: -webkit-image-set(url(<?=$imageSrc?>) 1x,
                                                                                 url(<?=$imageSrc?>) 2x)">
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            <div class="sale-order-detail-order-item-content">
                                                                <div class="sale-order-detail-order-item-title">
                                                                    <a href="<?=$detailPageUrl;?>">
                                                                        <?=htmlspecialcharsbx($basketItem['NAME'])?>
                                                                    </a>
                                                                </div>
                                                                <?
                                                                if (isset($basketItem['PROPS']) && is_array($basketItem['PROPS']))
                                                                {
                                                                    foreach ($basketItem['PROPS'] as $itemProps)
                                                                    {
                                                                        ?>

                                                                        <? if($itemProps['CODE'] == 'UNIT'): ?>
                                                                        <div class="sale-order-detail-order-item-color">
                                                                            <span class="sale-order-detail-order-item-color-name">
                                                                                <?=htmlspecialcharsbx($itemProps['NAME'])?>:</span>
                                                                            <span class="sale-order-detail-order-item-color-type">
                                                                                <?=htmlspecialcharsbx($itemProps['VALUE'])?></span>
                                                                        </div>
                                                                    <? endif; ?>

                                                                        <?
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right">
                                                        <div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm">
                                                            <?= Loc::getMessage('SPOD_PRICE')?>
                                                        </div>
                                                        <div class="sale-order-detail-order-item-td-text">

                                                            <? if ($basketItem['BASE_PRICE'] > $basketItem['PRICE']): ?>
                                                                <div class="sale-order-detail-order-item-price-old">
                                                                    <?=$basketItem['BASE_PRICE_FORMATED']?>
                                                                </div>
                                                                <div class="sale-order-detail-order-item-price">
                                                                    <?=$basketItem['PRICE_FORMATED']?>
                                                                </div>
                                                            <? else: ?>
                                                                <div class="sale-order-detail-order-item-price">
                                                                    <?=$basketItem['BASE_PRICE_FORMATED']?>
                                                                </div>
                                                            <? endif; ?>

                                                        </div>
                                                    </div>
                                                    <div class="sale-order-detail-order-item-nth-4p1"></div>
                                                    <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-center">
                                                        <div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm">
                                                            <?= Loc::getMessage('SPOD_QUANTITY')?>
                                                        </div>
                                                        <div class="sale-order-detail-order-item-td-text">
                                                            <? if ($isChangedQty): ?>
                                                                <span style='text-decoration: line-through;'>
                                                                <?= $arItem['PAST_INFO']['QUANTITY']; ?>
                                                            </span>
                                                            <? endif; ?>
                                                            <span>
                                                            <?=$basketItem['QUANTITY']?>&nbsp;
                                                            <?
                                                            if (strlen($basketItem['MEASURE_NAME']))
                                                            {
                                                                echo htmlspecialcharsbx($basketItem['MEASURE_NAME']);
                                                            }
                                                            else
                                                            {
                                                                echo Loc::getMessage('SPOD_DEFAULT_MEASURE');
                                                            }
                                                            ?>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right">
                                                        <div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm"><?= Loc::getMessage('SPOD_ORDER_PRICE')?></div>
                                                        <div class="sale-order-detail-order-item-td-text">
                                                            <strong class="sale-order-detail-order-item-price all"><?=$basketItem['FORMATED_SUM']?></strong>
                                                            <? if (($arResult['STATUS']['ID'] !== 'F') && ($arResult['STATUS']['ID'] !== 'O') && $isOk): ?>
                                                                <br>
                                                                <span style="color: #147628;">Товар зарезервирован</span>
                                                            <? elseif ($isChangedQty): ?>
                                                                <br>
                                                                <span style="color: #d5a933;">Количество изменено</span>
                                                            <? elseif ($isAdded): ?>
                                                                <br>
                                                                <span style="color: #0076b3;">Добавлен новый товар</span>
                                                            <? endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?
                                            }
                                            ?>
                                            <? foreach ($arResult['ORDER_CHANGE_INFO']['PRODUCTS'] as $arItem): ?>
                                                <?
                                                $isDeleted = (!isset($arItem['CURRENT_INFO']['QUANTITY'])) ? true : false;
                                                if ($isDeleted):?>
                                                    <?$productName = (isset($arItem['CURRENT_INFO'])) ? $arItem['CURRENT_INFO']['NAME'] : $arItem['PAST_INFO']['NAME'];
                                                    $imgSrc = false;?>
                                                    <div class="sale-order-detail-order-item-tr sale-order-detail-order-basket-info sale-order-detail-order-item-tr-first">
                                                        <div class="sale-order-detail-order-item-td" style="min-width: 300px;">
                                                            <div class="sale-order-detail-order-item-block">
                                                                <div class="sale-order-detail-order-item-img-block">
                                                                    <?
                                                                    if ($arItem['PREVIEW_PICTURE_ID']) {
                                                                        $imgInfo = \CFile::ResizeImageGet($arItem['PREVIEW_PICTURE_ID'], ['width' => 160, 'height' => 160], BX_RESIZE_IMAGE_PROPORTIONAL, false);
                                                                        if ($imgInfo) {
                                                                            $imgSrc = $imgInfo['src'];
                                                                        }
                                                                    }

                                                                    if (!$imgSrc)
                                                                        $imgSrc = $this->GetFolder().'/images/no_photo.png';
                                                                    ?>
                                                                    <div class="sale-order-detail-order-item-imgcontainer"
                                                                         style="background-image: url(<?=$imgSrc?>);
                                                                                 background-image: -webkit-image-set(url(<?=$imgSrc?>) 1x,
                                                                                 url(<?=$imgSrc?>) 2x)">
                                                                    </div>
                                                                </div>
                                                                <div class="sale-order-detail-order-item-content">
                                                                    <div class="sale-order-detail-order-item-title"style="text-decoration: line-through;">
                                                                        <span style="font-size: 13px;color: #383838; font-weight: 400;"><?=$productName?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right">
                                                            <div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm">
                                                                <?= Loc::getMessage('SPOD_PRICE')?>
                                                            </div>
                                                            <div class="sale-order-detail-order-item-td-text">
                                                                <strong class="sale-order-detail-order-item-price">
                                                                    <?=CCurrencyLang::CurrencyFormat(0, $defaultCurrency)?>
                                                                </strong>
                                                            </div>
                                                        </div>
                                                        <div class="sale-order-detail-order-item-nth-4p1"></div>
                                                        <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-center">
                                                            <div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm">
                                                                <?= Loc::getMessage('SPOD_QUANTITY')?>
                                                            </div>
                                                            <div class="sale-order-detail-order-item-td-text">
                                                            <span style='text-decoration: line-through;'>
                                                                <?= $arItem['PAST_INFO']['QUANTITY']; ?>
                                                            </span>
                                                            </div>
                                                        </div>
                                                        <div class="sale-order-detail-order-item-td sale-order-detail-order-item-properties bx-text-right">
                                                            <div class="sale-order-detail-order-item-td-title col-xs-7 col-sm-5 visible-xs visible-sm"><?= Loc::getMessage('SPOD_ORDER_PRICE')?></div>
                                                            <div class="sale-order-detail-order-item-td-text">
                                                                <strong class="sale-order-detail-order-item-price all"><?=CCurrencyLang::CurrencyFormat(0, $defaultCurrency)?></strong>
                                                                <br>
                                                                <span style="color: #e83333;">Товар убран</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row sale-order-detail-total-payment">
                <div class="col-md-7 col-md-offset-5 col-sm-12 col-xs-12 sale-order-detail-total-payment-container">
                    <div class="row">
                        <? $orderProperties = \Helpers\CustomTools::getPublicOrderProperties($arResult, 'order');?>
                        <?
                        $indexOrderPropertiesCount = count($orderProperties);
                        $indexOrderProperties = 0;
                        ?>
                        <?foreach ($orderProperties as $orderProperty): ?>
                            <?
                            $indexOrderProperties++;
                            if ($indexOrderProperties >= $indexOrderPropertiesCount) {
                                ?>
                                <div class="col-md-8 col-sm-6 col-xs-6 sale-order-detail-total-payment-list-left-item">
                                    Итого без скидки:
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-6 sale-order-detail-total-payment-list-right-item">
                                    <?= CurrencyFormat($indexBasePrice, 'RUB')?>
                                </div>
                                <?
                            }
                            ?>
                            <div class="col-md-8 col-sm-6 col-xs-6 sale-order-detail-total-payment-list-left-item<? if($orderProperty['CODE'] == 'Bold'){echo'-bold';}?>">
                                <?= $orderProperty['NAME'] . ':'; ?>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-6 sale-order-detail-total-payment-list-right-item<? if($orderProperty['CODE'] == 'Bold'){echo'-bold';}?>">
                                <?= $orderProperty['VALUE']; ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div><!--sale-order-detail-general-->
        <?
        if ($arParams['GUEST_MODE'] !== 'Y' && $arResult['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
        {
            ?>
            <a class="sale-order-detail-back-to-list-link-down" href="/personal/orders/">&larr; <?= Loc::getMessage('SPOD_RETURN_LIST_ORDERS')?></a>
            <?
        }
        ?>
    </div>

	<?
	$javascriptParams = array(
		"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
		"templateFolder" => CUtil::JSEscape($templateFolder),
		"templateName" => $this->__component->GetTemplateName(),
		"paymentList" => $paymentData
	);
	$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
	?>
	<script>
		BX.Sale.PersonalOrderComponent.PersonalOrderDetail.init(<?=$javascriptParams?>);
	</script>
<?
}
?>

