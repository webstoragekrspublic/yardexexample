<?

$sections = [];
$parentSectionId = '';
foreach ($arResult['SECTIONS'] as $section) {
    if ($section['DEPTH_LEVEL'] == 1) {
        $parentSectionId = $section['ID'];
        $section['CHILDRENS'] = [];
        $sections[$parentSectionId] = $section;
    } else {
        if (isset($sections[$parentSectionId])){
            $sections[$parentSectionId]['CHILDRENS'][] = $section;
        }
    }
}
$arResult['SECTIONS'] = $sections;