<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>

<? $childrenShowLimit = 10; ?>


<? if ($arResult['SECTIONS']): ?>
    <div class="main_page_categories_list">
        <div class="main_page_categories_list_title">
            <div class="main_page_categories_list_h">Каталог</div>
            <div class="main_page_categories_list_title_popular">Популярные категории</div>
            <a class="main_page_categories_list_title_all" href="/catalog/">Все</a>
        </div>
        <div class="row main_page_categories_list_items">
            <? foreach ($arResult['SECTIONS'] as $arSection): ?>
                <?
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_SECTION_DELETE_CONFIRM')));
                $imgSrc = \Helpers\CustomTools::getResizedPictSrc($arSection['~PICTURE'], ['width' => 100, 'height' => 120]);
                ?>
                <div class="col-lg-4 col-md-4 col-xs-6 main_page_categories_list_item_wrap">
                    <div class="main_page_categories_list_item">

                        <? if ($imgSrc): ?>
                            <div class="main_page_categories_list_item_left">
                                <a href="<?= $arSection['SECTION_PAGE_URL']; ?>">
                                <picture>
                                    <source type="image/webp" srcset="<?=Helpers\CustomTools::getWebpImgSrc($imgSrc);?>">
                                    <source type="<?=$arSection['PICTURE']['CONTENT_TYPE']?>" srcset="<?=$imgSrc; ?>">
                                    <img srcset="<?=$imgSrc; ?>" src="<?=$imgSrc; ?>"  />
                                </picture>
                                </a>
                            </div>
                        <? endif; ?>


                        <div class="main_page_categories_list_item_right">
                            <a class="main_page_categories_list_item_right_title a_standart_color"
                               href="<?= $arSection['SECTION_PAGE_URL']; ?>"
                               id="<?= $this->GetEditAreaId($arSection['ID']); ?>"
                            >
                                <?= $arSection['NAME'] ?>
                            </a>
                            <? if ($arSection['CHILDRENS']): ?>
                                <div class="main_page_categories_list_item_right_childs">
                                    <? $childSectionsShowCnt = 0; ?>
                                    <? foreach ($arSection['CHILDRENS'] as $arSectionChildren): ?>
                                        <?
                                        $childSectionsShowCnt++;
                                        $this->AddEditAction($arSectionChildren['ID'], $arSectionChildren['EDIT_LINK'], CIBlock::GetArrayByID($arSectionChildren["IBLOCK_ID"], "SECTION_EDIT"));
                                        $this->AddDeleteAction($arSectionChildren['ID'], $arSectionChildren['DELETE_LINK'], CIBlock::GetArrayByID($arSectionChildren["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_SECTION_DELETE_CONFIRM')));

                                        ?>
                                        <a class="main_page_categories_list_item_right_childs_item a_standart_color"
                                           href="<?= $arSectionChildren['SECTION_PAGE_URL']; ?>"
                                           id="<?= $this->GetEditAreaId($arSection['ID']); ?>"
                                        >
                                            <?= $arSectionChildren['NAME']; ?>
                                        </a>
                                    <? endforeach; ?>
                                </div>
                                <div class="main_page_categories_list_item_right_childs_show_all hidden">
                                    <div></div>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>

    <script>
        $('.main_page_categories_list_item_right').each(function () {
            var catsBlock = $(this).find('.main_page_categories_list_item_right_childs');
            if (catsBlock.prop('scrollHeight') > catsBlock.prop('clientHeight')) {
                $(this).find('.main_page_categories_list_item_right_childs_show_all').removeClass('hidden');
            }
        });
        $('.main_page_categories_list_item_right_childs_show_all').on('click', function () {
            $(this).closest('.main_page_categories_list_item_right').find('.main_page_categories_list_item_right_childs').css('max-height', 'none');
            $(this).addClass('hidden');
        });
    </script>


<? endif; ?>