<?
//ищем количество товаров учитывая 'AVAILABLE'
//$ttlCnt = 0;
//foreach ($arResult['SECTIONS'] as $key => $arItem) {
//    $elementCnt = (int)CIBlockElement::GetList(
//        [],
//        ['IBLOCK_ID' => (int)$arParams['IBLOCK_ID'], 'SECTION_ID' => $arItem['ID'], 'INCLUDE_SUBSECTIONS' => 'Y',
//            'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', 'AVAILABLE' => 'Y'],
//        [],
//        false,
//        ['ID', 'NAME', 'IBLOCK_ID']
//    );
//    if ($elementCnt > 0) {
//        $arResult['SECTIONS'][$key]['ELEMENT_CNT'] = $arResult['SECTIONS'][$key]['~ELEMENT_CNT'] = $elementCnt;
//        $ttlCnt += $elementCnt;
//    } else {
//        unset($arResult['SECTIONS'][$key]);
//    }
//}
//$arResult['SECTION']['ELEMENT_CNT'] = $ttlCnt;


if ($arParams['TOP_DEPTH'] > 1) {
    $arSections = array();
    $arSectionsDepth3 = array();
    foreach ($arResult['SECTIONS'] as $arItem) {
        if ($arItem['DEPTH_LEVEL'] == 1) {
            $arSections[$arItem['ID']] = $arItem;
        } elseif ($arItem['DEPTH_LEVEL'] == 2) {
            $arSections[$arItem['IBLOCK_SECTION_ID']]['SECTIONS'][$arItem['ID']] = $arItem;
        } elseif ($arItem['DEPTH_LEVEL'] == 3) {
            $arSectionsDepth3[] = $arItem;
        }
    }
    if ($arSectionsDepth3) {
        foreach ($arSectionsDepth3 as $arItem) {
            foreach ($arSections as $key => $arSection) {
                if (is_array($arSection['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]) && !empty($arSection['SECTIONS'][$arItem['IBLOCK_SECTION_ID']])) {
                    $arSections[$key]['SECTIONS'][$arItem['IBLOCK_SECTION_ID']]['SECTIONS'][$arItem['ID']] = $arItem;
                }
            }
        }
    }
    $arResult['SECTIONS'] = $arSections;
}
?>