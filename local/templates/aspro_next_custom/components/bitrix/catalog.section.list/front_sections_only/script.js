function sectionCatalogSelect(){
    this.currentAjaxFetch = false;
    this.sectionsSelector = 'a[data-section_search_select]';
    this.init();
}

sectionCatalogSelect.prototype.init = function(){
    var self = this;
    $('html').on("click", this.sectionsSelector, function (e) {
        e.preventDefault();

        self.productContainer = $('#catalog_product_container');
        self.filtersContainer = $('#catalog_filters');
        if (!self.productContainer.length)
            self.productContainer = false;
        if (!self.filtersContainer.length)
            self.filtersContainer = false;

        var href = $(this).attr('href');

        if (href){

            if(window.History.enabled || window.history.pushState != null){
                window.History.pushState( null, document.title, decodeURIComponent(href) );
            }else{
                location.href = href;
            }

            if (self.currentAjaxFetch){
                self.currentAjaxFetch.abort();
            }

            var clickedElementParent = $(this).parent();
            self.currentAjaxFetch = $.ajax({
                type: 'GET',
                url: href,
                data: {
                    getHeader: 'Y'
                },
                beforeSend: function (xhr) {
                    if (self.productContainer)
                        self.productContainer.css('opacity','0.5');
                },
                success: function (data) {

                    if (self.productContainer && data){
                        var $breadcrumb = $('#navigation');
                        var $breadcrumbNewHtml = $(data).find('#navigation');
                        if ($breadcrumb.length && $breadcrumbNewHtml.length){
                            $breadcrumb.html($breadcrumbNewHtml.html());
                        }

                        var $pagetitle = $('#pagetitle');
                        var $pagetitleNewHtml = $(data).find('#pagetitle');
                        if ($pagetitle.length && $pagetitleNewHtml.length){
                            $pagetitle.html($pagetitleNewHtml.html());
                        }

                        if ($(data).find('.sort_filter').length && $('.sort_filter').length == 1){
                            $('.sort_filter').html($(data).find('.sort_filter').html());
                        }

                        var productsHtmlMatch = data.match(/<!--catalog_product_container-->([\s\S]*)<!--\/catalog_product_container-->/);
                        var filtersHtmlMatch = data.match(/<!--filters-->([\s\S]*)<!--\/filters-->/);

                        if (self.filtersContainer && filtersHtmlMatch){
                            var filtersHtml = filtersHtmlMatch[1];
                            self.filtersContainer.html(filtersHtml);
                        }


                        if ($('.bottom_nav').length){
                            $('.bottom_nav').html('');
                        }
                        var $bottomNav = $('.bottom_nav').last();

                        if (productsHtmlMatch ){
                            var productsHtml = productsHtmlMatch[1];

                            $(self.sectionsSelector).parent().removeClass('current');
                            clickedElementParent.addClass('current');

                            self.productContainer.html(productsHtml);
                            $('.catalog_block').equalize({children: '.catalog_item .cost', reset: true});
                            $('.catalog_block').equalize({children: '.catalog_item .item-title', reset: true});
                            $('.catalog_block').equalize({children: '.catalog_item .counter_block', reset: true});
                            $('.catalog_block').equalize({children: '.catalog_item .item_info', reset: true});
                            $('.catalog_block').equalize({children: '.catalog_item_wrapp', reset: true});
                        }

                        if ($bottomNav.length){
                            $bottomNav.html($.trim($(data).find('.bottom_nav').html()));
                            autoClickShowMoreBtnResetClicks();
                        }
                        initProductsAfterAjax();

                        self.productContainer.css('opacity','1');
                        $(window).trigger('resize');
                    }
                },
                error: function (jqXHR, exception) {

                }
            });
        }
    });
}

new sectionCatalogSelect();
