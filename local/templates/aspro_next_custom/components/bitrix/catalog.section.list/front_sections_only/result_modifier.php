<?

//перерасчет количества товаров в категориях
if (
    $GLOBALS[$arParams['FILTER_NAME']]['PROPERTY_'.\Helpers\Constants::PROP_FAST_DELIVERY] === \Helpers\Constants::PROP_FAST_DELIVERY_VALUE_EXPRESS
) {
    $allSections = array_column($arResult['SECTIONS'], 'ID');

    $sectionsCnt = [];

    $sectionAllItemsFilter = [
        "IBLOCK_ID" => $GLOBALS[$arParams['FILTER_NAME']]['IBLOCK_ID'],
        "=SECTION_ID" => $allSections,
        'PROPERTY_FAST_DELIVERY' => $GLOBALS[$arParams['FILTER_NAME']]['PROPERTY_FAST_DELIVERY'],
        'ACTIVE' => 'Y',
        'ACTIVE_DATE' => 'Y',
        'CATALOG_AVAILABLE' => 'Y'
    ];

    $sectionsAllItems = CNextCache::CIBlockElement_GetList(
        array(
            'CACHE' =>
                array(
                    "MULTI" => "Y",
                    "TAG" => CNextCache::GetIBlockCacheTag($GLOBALS[$arParams['FILTER_NAME']]['IBLOCK_ID'])
                )
        ),
        $sectionAllItemsFilter,
        false,
        false,
        array("ID", "IBLOCK_SECTION_ID")
    );
    foreach ($sectionsAllItems as $item) {
        if (!isset($sectionsCnt[$item['IBLOCK_SECTION_ID']])) {
            $sectionsCnt[$item['IBLOCK_SECTION_ID']] = 0;
        }
        $sectionsCnt[$item['IBLOCK_SECTION_ID']]++;
    }

    $sections = [];
    foreach ($arResult['SECTIONS'] as $key => $section) {
        if (!isset($sectionsCnt[$section['ID']])) {
            unset($arResult['SECTIONS'][$key]);
        } else {
            $arResult['SECTIONS'][$key]['ELEMENT_CNT'] = $arResult['SECTIONS'][$key]['~ELEMENT_CNT'] = $sectionsCnt[$section['ID']];
        }
    }
}
