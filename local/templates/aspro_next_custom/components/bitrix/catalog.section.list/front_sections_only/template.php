<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?
$selectedSection = (int)$arParams['CURRENT_SECTION_ID'];

usort($arResult['SECTIONS'], function ($a, $b) {
    return strcmp($a['NAME'], $b['NAME']);
});
?>

<? if ($arResult['SECTIONS']): ?>
    <div class="sections_wrapper">
        <? if ($arParams["TITLE_BLOCK"] || $arParams["TITLE_BLOCK_ALL"]): ?>
            <div class="top_block">
                <h3 class="title_block"><?= $arParams["TITLE_BLOCK"]; ?></h3>
                <a href="<?= SITE_DIR . $arParams["ALL_URL"]; ?>"><?= $arParams["TITLE_BLOCK_ALL"]; ?></a>
            </div>
        <? endif; ?>
        <div class="list items">
            <div class="row margin0 flexbox">

                <div class="catalog_sections_item <?=$arResult['SECTION']['ID']==$selectedSection?'current':''; ?>">
                    <a data-section_search_select href="<?= $arResult['SECTION']['SECTION_PAGE_URL']; ?>">
                        Все <span class="badge"><?= array_sum(array_column($arResult['SECTIONS'], 'ELEMENT_CNT')); ?></span>
                    </a>
                </div>


                <? foreach ($arResult['SECTIONS'] as $arSection):
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_SECTION_DELETE_CONFIRM')));
                    ?>
                    <div class="catalog_sections_item <?=$arSection['ID']==$selectedSection?'current':''; ?>"
                         id="<?= $this->GetEditAreaId($arSection['ID']); ?>">
                        <a data-section_search_select href="<?= $arSection['SECTION_PAGE_URL']; ?>">
                            <?= $arSection['NAME'] ?> <span class="badge"><?= $arSection['ELEMENT_CNT']; ?></span>
                        </a>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
<? endif; ?>