<?php
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(CModule::IncludeModule("sale")) {
    $arBasketItems = array();

    $dbBasketItems = CSaleBasket::GetList(
        array(
            "ID" => "ASC"
        ),
        array(
            "FUSER_ID" => CSaleBasket::GetBasketUserID(),
            "LID" => SITE_ID,
            "ORDER_ID" => "NULL"
        ),
        false,
        false,
        array("ID", "QUANTITY", "DISCOUNT_PRICE", "PRICE")
    );
    while ($arItems = $dbBasketItems->Fetch()) {
        $arBasketItems[$arItems["ID"]] = $arItems;
    }
    
    if(count($arBasketItems) > 0){
        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(
            \Bitrix\Sale\Fuser::getId(),
            \Bitrix\Main\Context::getCurrent()->getSite()
          ); 
        $fuser = new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId(true));
        $discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, $fuser);
        $discounts->calculate();
        $result = $discounts->getApplyResult(true);
        $prices = $result['PRICES']['BASKET'];

        foreach($arBasketItems as $id => $item){
            $arBasket["SUM"] += $item["QUANTITY"] * $prices[$id]["PRICE"];
        }

        echo json_encode($arBasket);
    }
    else{
        $arBasket["SUM"] = 0;
        echo json_encode($arBasket);
    }
}

?>