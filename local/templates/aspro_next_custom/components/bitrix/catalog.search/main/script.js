function sectionSearchSelect(params){
    this.sectionsSelector = params.sectionsSelector;
    this.productContainer = $(params.productContainerSelector);
    this.filtersContainer = $(params.filtersContainerSelector);
    this.currentAjaxFetch = false;
    if (!this.productContainer.length)
        this.productContainer = false;
    if (!this.filtersContainer.length)
        this.filtersContainer = false;

    this.init();
}

sectionSearchSelect.prototype.init = function(){
    var self = this;
    $('html').on("click", this.sectionsSelector, function (e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var clickedElementParent = $(this).parent();
        if (href){

            if(window.History.enabled || window.history.pushState != null){
                window.History.pushState( null, document.title, decodeURIComponent(href) );
            }else{
                location.href = href;
            }

            if (self.currentAjaxFetch){
                self.currentAjaxFetch.abort();
            }

            self.currentAjaxFetch = $.ajax({
                type: 'GET',
                url: href,
                data: {
                    ajax_get: 'Y',
                    ajax_get_filter: 'Y'
                },
                beforeSend: function (xhr) {
                    if (self.productContainer)
                        self.productContainer.css('opacity','0.5');
                },
                success: function (data) {
                    if (self.productContainer && data){
                        var productsHtmlMatch = data.match(/<!--search_product_container-->([\s\S]*)<!--\/search_product_container-->/);
                        var filtersHtmlMatch = data.match(/<!--filters-->([\s\S]*)<!--\/filters-->/);

                        if (self.filtersContainer && filtersHtmlMatch){
                            var filtersHtml = filtersHtmlMatch[1];
                            self.filtersContainer.html(filtersHtml);
                        }

                        if (productsHtmlMatch ){
                            var productsHtml = productsHtmlMatch[1];
                            self.productContainer.html(productsHtml);
                            $(self.sectionsSelector).parent().removeClass('current');
                            clickedElementParent.addClass('current');
                        }

                        self.productContainer.css('opacity','1');
                        $(window).trigger('resize');
                        //выравнивание блоков
                        $('.catalog_block .catalog_item_wrapp .catalog_item .item_info:visible .item-title').sliceHeight({item:'.catalog_item', mobile: true});
                        $('.catalog_block .catalog_item_wrapp .catalog_item .item_info:visible').sliceHeight({item:'.catalog_item', mobile: true});
                        $('.catalog_block .catalog_item_wrapp .catalog_item:visible').sliceHeight({classNull: '.footer_button', item:'.catalog_item', mobile: true});

                        $('.catalog_block').equalize({children: '.catalog_item .cost', reset: true});
                        $('.catalog_block').equalize({children: '.catalog_item .item-title', reset: true});
                        $('.catalog_block').equalize({children: '.catalog_item .counter_block', reset: true});
                        $('.catalog_block').equalize({children: '.catalog_item .item_info', reset: true});
                        $('.catalog_block').equalize({children: '.catalog_item_wrapp', reset: true});
                    }
                    initProductsAfterAjax();
                },
                error: function (jqXHR, exception) {

                }
            });
        }
    });
}
