<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult['SHOW_ORDER_PROPS'] = ['ADDRESS' => false, 'DELIVERY_TIME' => false, 'DELIVERY_DATE' => false, 'PREPARE_CHANGE_FROM_AMOUNT' => false];

$historyQ = \Bitrix\Sale\Internals\OrderChangeTable::getList(array(
    'order' => array('DATE_CREATE' => 'DESC', 'ID' => 'DESC'),
    'filter' => array('ORDER_ID' => $arResult['ID'])
));

$changedInfo = [
    'ORDER_INFO' => [],
    'PRODUCTS' => [],
];
$changeStart = false;
$initialLogStart = false;
$productIDs = [];

while ($historyItem = $historyQ->fetch()) {
    if (($historyItem['TYPE'] == 'ORDER_STATUS_CHANGED' or $historyItem['TYPE'] == 'ORDER_ADDED') && $changeStart) {
        $changeStart = false;
    }

    if ($historyItem['TYPE'] == 'ORDER_ADDED') {
        $initialLogStart = true;
    }

    if ($historyItem['TYPE'] == 'ORDER_STATUS_CHANGED' and !$changeStart) {
        $changeStart = true;
        continue;
    }

    if ($changeStart and $historyItem['TYPE'] == 'ORDER_PRICE_CHANGED') {
        $productInfo = \Helpers\CustomTools::unserializeHistoryData($historyItem['DATA']);
        if ($productInfo) {
            $changedInfo['ORDER_INFO']['PRICE_CHANGED'] = $productInfo;
        }
    }

    if ($initialLogStart and $historyItem['TYPE'] == 'SHIPMENT_ITEM_BASKET_ADDED') {
        $productInfo = \Helpers\CustomTools::unserializeHistoryData($historyItem['DATA']);
        if ($productInfo) {
            $changedInfo['PRODUCTS'][$productInfo['PRODUCT_ID']]['PAST_INFO'] = $productInfo;
            $productIDs[$productInfo['PRODUCT_ID']] = $productInfo['PRODUCT_ID'];
        }
    }

    if ($initialLogStart and $historyItem['TYPE'] == 'SHIPMENT_ADDED') {
        $productInfo = \Helpers\CustomTools::unserializeHistoryData($historyItem['DATA']);
        if ($productInfo) {
            $changedInfo['ORDER_INFO']['SHIPMENT_ADDED'] = $productInfo;
        }
    }

    if ($initialLogStart and $historyItem['TYPE'] == 'PAYMENT_ADDED') {
        $productInfo = \Helpers\CustomTools::unserializeHistoryData($historyItem['DATA']);
        if ($productInfo) {
            $changedInfo['ORDER_INFO']['PAYMENT_ADDED'] = $productInfo;
        }
    }

}

$order = \Bitrix\Sale\Order::load($arResult['ID']);

$currentBasketItems = $order->getBasket()->getBasketItems();

foreach ($currentBasketItems as $basketItem) {
    $productInfo = [
        'PRODUCT_ID' => $basketItem->getProductId(),
        'QUANTITY' => $basketItem->getQuantity(),
        'NAME' => $basketItem->getField('NAME'),
        'PRICE' => $basketItem->getPrice(),
        'PRICE_FINAL' => $basketItem->getFinalPrice(),
        'CAN_BUY' => $basketItem->canBuy(),
        'IS_DELAY' => $basketItem->isDelay(),
        'CURRENCY' => $basketItem->getCurrency(),
        'MEASURE_NAME' => $basketItem->getField('MEASURE_NAME'),
    ];

    $productIDs[$productInfo['PRODUCT_ID']] = $productInfo['PRODUCT_ID'];
    $changedInfo['PRODUCTS'][$productInfo['PRODUCT_ID']]['CURRENT_INFO'] = $productInfo;
}

if ($productIDs) {
    $res = CIBlockElement::GetList([], ['ID' => $productIDs], false, [], ['ID', 'PREVIEW_PICTURE']);
    while ($row = $res->fetch()) {
        $changedInfo['PRODUCTS'][$row['ID']]['PREVIEW_PICTURE_ID'] = $row['PREVIEW_PICTURE'];
    }
}

if ($arResult['ORDER_PROPS']) {
    foreach ($arResult['ORDER_PROPS'] as $orderProp) {
        if (isset($arResult['SHOW_ORDER_PROPS'][$orderProp['CODE']]) and !empty($orderProp['VALUE'])) {
            $arResult['SHOW_ORDER_PROPS'][$orderProp['CODE']] = $orderProp;
        }
    }
}
$arResult['ORDER_CHANGE_INFO'] = $changedInfo;

