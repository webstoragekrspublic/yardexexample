<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$urlToOrder = '/personal/orders/' . (int)$arResult['ID'];
$defaultCurrency = 'RUB';
?>

<div style='font-size: 15px; background-color: white;'>
    <h3 style="text-align: center;background-color: white;">
        Заказ <a href="<?= $urlToOrder; ?>" target="_blank">#<?= (int)$arResult['ID']; ?></a>
    </h3>
    <div style="text-align: center;background-color: white;">
        От: <?= $arResult['DATE_INSERT_FORMATED']; ?>
    </div>
<!--    <h3 style="text-align: center;background-color: white;">
        Товары в заказе
    </h3>-->
    <div style="text-align: center;background-color: white;">
        <p>Также вы можете оставить пожелание или жалобу с помощью <a href="https://yardex.ru/zhaloba/" target="_blank">формы обратной связи</a>.</p>
    </div>
<!--    <table width="100%" style='background-color: white;max-width: 1000px;'>
        <tr>
            <th style="font-size: 13px;line-height: 20px;color: #999999;"></th>
            <th style="font-size: 13px;line-height: 20px;color: #999999;">Наименование</th>
            <th style="font-size: 13px;line-height: 20px;color: #999999;">Цена</th>
            <th style="font-size: 13px;line-height: 20px;color: #999999;">Количество</th>
            <th style="font-size: 13px;line-height: 20px;color: #999999;">Сумма</th>
        </tr>

        <? foreach ($arResult['ORDER_CHANGE_INFO']['PRODUCTS'] as $arItem): ?>
            <?
            $isDeleted = (!isset($arItem['CURRENT_INFO']['QUANTITY'])) ? true : false;
            $isChangedQty = (isset($arItem['CURRENT_INFO']['QUANTITY'], $arItem['PAST_INFO']['QUANTITY']) and $arItem['CURRENT_INFO']['QUANTITY'] != $arItem['PAST_INFO']['QUANTITY']) ? true : false;

            $isAdded = (isset($arItem['CURRENT_INFO']['QUANTITY']) && !isset($arItem['PAST_INFO']['QUANTITY'])) ? true : false;


            $isOk = (!$isDeleted && !$isChangedQty && !$isAdded) ? true : false;
            $productName = (isset($arItem['CURRENT_INFO'])) ? $arItem['CURRENT_INFO']['NAME'] : $arItem['PAST_INFO']['NAME'];
            $price = (isset($arItem['CURRENT_INFO']['PRICE'])) ? CCurrencyLang::CurrencyFormat($arItem['CURRENT_INFO']['PRICE'], $arItem['CURRENT_INFO']['CURRENCY']) : '';
            $priceFinal = (isset($arItem['CURRENT_INFO']['PRICE_FINAL'])) ? CCurrencyLang::CurrencyFormat($arItem['CURRENT_INFO']['PRICE_FINAL'], $arItem['CURRENT_INFO']['CURRENCY']) : '';
            $imgSrc = false;
            if ($arItem['PREVIEW_PICTURE_ID']) {
                $imgInfo = \CFile::ResizeImageGet($arItem['PREVIEW_PICTURE_ID'], ['width' => 160, 'height' => 160], BX_RESIZE_IMAGE_PROPORTIONAL, false);
                if ($imgInfo) {
                    $imgSrc = $imgInfo['src'];
                }
            }

            if (!$imgSrc)
                $imgSrc = $arParams['NO_IMAGE_PATH'];

            ?>

            <tr>
                <td style="text-align: center;">
                    <img height="70" src="<?= $imgSrc; ?>">
                </td>
                <td <?= $isDeleted ? 'style=\'text-decoration: line-through;\'' : ''; ?>><?= $productName; ?></td>
                <td style="min-width: 100px;text-align: center;">
                    <span style="font-size: 18px;line-height: 16px;color: #333;"><?= $isDeleted ? CCurrencyLang::CurrencyFormat(0, $defaultCurrency) : $price; ?></span>
                </td>
                <td style="min-width: 50px;text-align: center;">

                    <? if ($isChangedQty or $isDeleted): ?>
                        <span style='text-decoration: line-through;'><?= $arItem['PAST_INFO']['QUANTITY']; ?></span>
                    <? endif; ?>

                    <? if (isset($arItem['CURRENT_INFO']['QUANTITY']) && !$isDeleted): ?>
                        <span><?=$arItem['CURRENT_INFO']['QUANTITY']; ?> <?=$arItem['CURRENT_INFO']['MEASURE_NAME']; ?></span>
                    <? endif; ?>

                </td>
                <td style="min-width: 100px;text-align: center;">
                    <span style="font-size: 18px;line-height: 16px;color: #333;"><?= $isDeleted ? CCurrencyLang::CurrencyFormat(0, $defaultCurrency) : $priceFinal; ?></span>

                    <? if ($arParams['SHOW_RESERVE_STATUS'] == 'Y'): ?>
                        <br>
                        <? if ($isDeleted): ?>
                            <span style="color: #e83333;">Товар убран</span>
                        <? elseif ($isChangedQty): ?>
                            <span style="color: #d5a933;">Количество изменено</span>
                        <? elseif ($isAdded): ?>
                            <span style="color: #0076b3;">Добавлен новый товар</span>
                        <? elseif ($isOk): ?>
                            <span style="color: #147628;">Товар зарезервирован</span>
                        <? endif; ?>
                    <? endif; ?>

                </td>
            </tr>
        <? endforeach; ?>
    </table>-->

    <p style = "text-align:center;">Актуальную информацию о составе заказа Вы можете посмотреть в <a href = "<?=$urlToOrder?>/">личном кабинете</a></p>
    
    <? $orderProperties = \Helpers\CustomTools::getPublicOrderProperties($arResult, 'mail');?>
    <table>
        <? foreach ($orderProperties as $orderProperty): ?>
            <tr>
                <td><?= $orderProperty['NAME'] . ':'; ?></td>
                <td><?= $orderProperty['VALUE']; ?></td>
            </tr>
        <? endforeach; ?>
    </table>
    
    <div style="text-align: left">
        <p>Если вы выбрали способ оплаты заказа "Картой онлайн (на сайте)", убедитесь, что заказ оплачен. Оплатить заказ можно <a href="/personal/orders/">здесь</a>.</p>
        <p>Когда вы оплачиваете заказ банковской картой на сайте, на вашем счету холдируется (не списывается, а замораживается) полная стоимость корзины покупок. Банк уведомляет вас об этой сумме по SMS.</p>
        <p>Если при сборке заказа окажется, что какой-либо товар отсутствует на складе или у какого-либо товара будет уменьшен вес, мы скорректируем состав и итоговую сумму заказа в меньшую сторону.
            Эти изменения будут отражены в вашем личном кабинете и в товарном чеке.
            На следующий день после доставки мы спишем итоговую сумму заказа с вашего счета, при этом разницу между замороженной и списанной суммами разморозит банк.
            При разблокировке средств вы <b>не получите</b> SMS-уведомление, т.к. эта операция не является пополнением счёта.
            Пожалуйста, проверяйте выписку в своем онлайн-банке, либо обратитесь на горячую линию банка.</p>
    </div>
</div>