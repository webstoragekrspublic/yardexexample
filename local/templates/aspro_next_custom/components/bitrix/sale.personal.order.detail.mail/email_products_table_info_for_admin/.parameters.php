<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// дополнительная настройка списка параметров (может быть обработана в result_modifier например)

$arTemplateParameters = array(
	"SHOW_RESERVE_STATUS" => Array(
		"NAME" => 'Показывать статус резервирования',
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "Y",
	),
	"NO_IMAGE_PATH" => Array(
		"NAME" => 'путь до картинки для товаров без изображений',
		"TYPE" => "STRING",
		"DEFAULT" => "/local/templates/aspro_next_custom/images/no_photo_medium.png",
	)
);
?>
