<?
use App\Helpers\UserHelper;
use Handlers\ExternalSuppliers\ExternalSuppliers;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$urlToOrder = '/personal/orders/' . (int)$arResult['ID'];
$defaultCurrency = 'RUB';
$orderSuppliers = ExternalSuppliers::getRowsForOrderId($arResult['ID']);
$orderSuppliers = \Helpers\CustomTools::indexArrayByKey($orderSuppliers,'SUPPLIER_CODE');
?>

<div style='font-size: 15px; background-color: white;'>
    <h3 style="text-align: center;background-color: white;">
        Заказ <a href="<?= $urlToOrder; ?>" target="_blank">#<?= (int)$arResult['ID']; ?></a>
    </h3>
    <div style="text-align: center;background-color: white;">
        От: <?= $arResult['DATE_INSERT_FORMATED']; ?>
    </div>
    <h3 style="text-align: center;background-color: white;">
        Товары в заказе
    </h3>
    <table width="100%" style='background-color: white;max-width: 1000px;'>
        <tr>
            <th style="font-size: 13px;line-height: 20px;color: #999999;"></th>
            <th style="font-size: 13px;line-height: 20px;color: #999999;">Наименование</th>
            <th style="font-size: 13px;line-height: 20px;color: #999999;">Цена</th>
            <th style="font-size: 13px;line-height: 20px;color: #999999;">Количество</th>
            <th style="font-size: 13px;line-height: 20px;color: #999999;">Сумма</th>
        </tr>

        <? foreach ($arResult['ORDER_CHANGE_INFO']['PRODUCTS'] as $arItem): ?>
            <?
            $isDeleted = (!isset($arItem['CURRENT_INFO']['QUANTITY'])) ? true : false;
            $isChangedQty = (isset($arItem['CURRENT_INFO']['QUANTITY'], $arItem['PAST_INFO']['QUANTITY']) and $arItem['CURRENT_INFO']['QUANTITY'] != $arItem['PAST_INFO']['QUANTITY']) ? true : false;

            $isAdded = (isset($arItem['CURRENT_INFO']['QUANTITY']) && !isset($arItem['PAST_INFO']['QUANTITY'])) ? true : false;


            $isOk = (!$isDeleted && !$isChangedQty && !$isAdded) ? true : false;
            $productName = (isset($arItem['CURRENT_INFO'])) ? $arItem['CURRENT_INFO']['NAME'] : $arItem['PAST_INFO']['NAME'];
            $price = (isset($arItem['CURRENT_INFO']['PRICE'])) ? CCurrencyLang::CurrencyFormat($arItem['CURRENT_INFO']['PRICE'], $arItem['CURRENT_INFO']['CURRENCY']) : '';
            $priceFinal = (isset($arItem['CURRENT_INFO']['PRICE_FINAL'])) ? CCurrencyLang::CurrencyFormat($arItem['CURRENT_INFO']['PRICE_FINAL'], $arItem['CURRENT_INFO']['CURRENCY']) : '';
            $imgSrc = false;
            if ($arItem['PREVIEW_PICTURE_ID']) {
                $imgInfo = \CFile::ResizeImageGet($arItem['PREVIEW_PICTURE_ID'], ['width' => 160, 'height' => 160], BX_RESIZE_IMAGE_PROPORTIONAL, false);
                if ($imgInfo) {
                    $imgSrc = $imgInfo['src'];
                }
            }

            if (!$imgSrc)
                $imgSrc = $arParams['NO_IMAGE_PATH'];

            ?>

            <tr>
                <td style="text-align: center;">
                    <img height="70" src="<?= $imgSrc; ?>">
                    <input type="hidden" data-role="pr_cart_info_id" value="<?=$arItem['CURRENT_INFO']['PRODUCT_ID'] ?>"/>
                    <input type="hidden" data-role="pr_cart_info_xml_id" value="<?=$arItem['CURRENT_INFO']['PRODUCT_IDPRODUCT_XML_ID'] ?>"/>
                </td>
                <td
                    <?= $isDeleted ? 'style=\'text-decoration: line-through;\'' : ''; ?>><?= $productName; ?> (<?=$arItem['CURRENT_INFO']['PRODUCT_XML_ID']; ?>)
                    <? if ($arItem['SUPPLIER_ID']): ?>
                        <?
                        $statusId = $orderSuppliers[$arItem['SUPPLIER_ID']]['STATUS_ID'];
                        $statusDescr = \Handlers\ExternalSuppliers\ExternalSuppliersOrderTable::getDescriptionByCode($statusId);
                        ?>
                        <p style="font-size: 13px;color: grey;">
                        Поставщик товара - <?=$arItem['SUPPLIER_ID']; ?>. Код товара поставщика - <?=$arItem['SUPPLIER_GOODS_ID']; ?>
                        <br>
                        статус отправки - <?=$statusDescr."($statusId)"; ?>
                        </p>
                    <? endif; ?>
                </td>
                <td style="min-width: 100px;text-align: center;">
                    <span style="font-size: 18px;line-height: 16px;color: #333;"><?= $isDeleted ? CCurrencyLang::CurrencyFormat(0, $defaultCurrency) : $price; ?></span>
                </td>
                <td style="min-width: 50px;text-align: center;">

                    <? if ($isChangedQty or $isDeleted): ?>
                        <span style='text-decoration: line-through;'><?= $arItem['PAST_INFO']['QUANTITY']; ?></span>
                    <? endif; ?>

                    <? if (isset($arItem['CURRENT_INFO']['QUANTITY']) && !$isDeleted): ?>
                        <span><?=$arItem['CURRENT_INFO']['QUANTITY']; ?> <?=$arItem['CURRENT_INFO']['MEASURE_NAME']; ?></span>
                    <? endif; ?>

                </td>
                <td style="min-width: 100px;text-align: center;">
                    <span style="font-size: 18px;line-height: 16px;color: #333;"><?= $isDeleted ? CCurrencyLang::CurrencyFormat(0, $defaultCurrency) : $priceFinal; ?></span>

                    <? if ($arParams['SHOW_RESERVE_STATUS'] == 'Y'): ?>
                        <br>
                        <? if ($isDeleted): ?>
                            <span style="color: #e83333;">Товар убран</span>
                        <? elseif ($isChangedQty && ($arResult['STATUS']['ID'] !== 'F') && ($arResult['STATUS']['ID'] !== 'O')): ?>
                            <span style="color: #d5a933;">Количество изменено</span>
                        <? elseif ($isAdded && ($arResult['STATUS']['ID'] !== 'F') && ($arResult['STATUS']['ID'] !== 'O')): ?>
                            <span style="color: #0076b3;">Добавлен новый товар</span>
                        <? elseif ($isOk && ($arResult['STATUS']['ID'] !== 'F') && ($arResult['STATUS']['ID'] !== 'O')): ?>
                            <span style="color: #147628;">Товар зарезервирован</span>
                        <? endif; ?>
                    <? endif; ?>

                </td>
            </tr>
        <? endforeach; ?>
    </table>

    <? $orderProperties = \Helpers\CustomTools::getPublicOrderProperties($arResult, 'order');?>
    <table>
        <? foreach ($orderProperties as $orderProperty): ?>
            <tr>
                <td><?= $orderProperty['NAME'] . ':'; ?></td>
                <td><?= $orderProperty['VALUE']; ?></td>
            </tr>
        <? endforeach; ?>
    </table>
</div>