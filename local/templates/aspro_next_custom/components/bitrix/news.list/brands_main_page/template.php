<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$this->setFrameMode(true);
$sliderID = "brands_main_page" . $this->randString();
$svgArrowRight = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/images/svg/right-arrow.svg');
$minSlidersCount = 10; // дублируем количество если меньше, нужно чтобы слайдер нормально входил в блок
?>

<? if ($arResult['ITEMS']): ?>
    <? if (count($arResult['ITEMS']) < $minSlidersCount): ?>
        <? $arResult['ITEMS'] = array_merge($arResult['ITEMS'], $arResult['ITEMS']);  ?>
    <? endif; ?>

    <div class="maxwidth-theme brands_main_page">

        <div class="swiper-container brands_main_page_swiper" id="<?= $sliderID ?>">
            <div class="swiper-wrapper">
                <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

                    $href = $arItem['DETAIL_PAGE_URL'];
                    $imgSrc = \Helpers\CustomTools::getResizedPictSrc($arItem['~PREVIEW_PICTURE'], ["width" => 165, "height" => 100]);
                    $title = $arItem['NAME'];
                    ?>
                    <? if ($imgSrc): ?>
                        <a class="swiper-slide brands_main_page_slide"
                           href="<?= $href; ?>"
                           id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <picture>
                                <source type="image/webp" srcset="<?=Helpers\CustomTools::getWebpImgSrc($imgSrc);?>">
                                <source type="<?=$arItem['PREVIEW_PICTURE']['CONTENT_TYPE']?>" srcset="<?=$imgSrc; ?>">
                                <img class="brands_main_page_slide_image" srcset="<?=$imgSrc; ?>" src="<?=$imgSrc; ?>"  <?= $title ? 'title="' . $title . '"' : ''; ?> />
                            </picture>
                        </a>
                    <? endif; ?>

                <? endforeach; ?>
            </div>
            <div class="brands_main_page_nav">
                <div class="swiper-button-prev svg-arrow brands_main_page_nav_arrow">
                    <div class="hidden-xs">
                        <?= $svgArrowRight; ?>
                    </div>
                </div>
                <div class="swiper-pagination big_points brands_main_page_nav_points"></div>
                <div class="swiper-button-next svg-arrow brands_main_page_nav_arrow">
                    <div class="hidden-xs">
                        <?= $svgArrowRight; ?>
                    </div>
                </div>
            </div>

            <a class="brands_main_page_all_brands a_standart_color" href="<?= SITE_DIR . $arParams['ALL_URL']; ?>">
                Все бренды
            </a>
        </div>
    </div>


    <script>
        var swiper = new Swiper('#<?=$sliderID; ?>', {
            slidesPerView: 'auto',
            spaceBetween: 40,
            loop: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>

<? endif; ?>
