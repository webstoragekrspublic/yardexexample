function CustomSettingsEdit(params)
{
    var jsOptions = JSON.parse(params.data);
    var showArray = {
        "element": params.oCont,
        "options": jsOptions
    };

    if (params.oInput.value != "" && params.oInput.value != "[]")
    {
        this.inputData = JSON.parse(params.oInput.value);
        this.inputData.forEach(function (page) {
            CustomSettingsEdit.prototype.addInputBlock(showArray, page);
        }, this);
    } else
    {
        CustomSettingsEdit.prototype.addInputBlock(showArray);
    }
    addPageParams = params.oCont.appendChild(BX.create('input', {
        props: {
            "value": "+",
            "type": "button",
            "className": "addPageParams"
        }
    }));

    BX.bind(addPageParams, 'click', function () {
        CustomSettingsEdit.prototype.addInputBlock(showArray);
        params.oCont.appendChild(addPageParams);
    });
}

CustomSettingsEdit.prototype.addInputBlock = function (params, values)
{
    var name = "";
    var id = "";
    var hidden = params.element.querySelector("input[type='hidden']");

    if (typeof (values) !== "undefined")
    {
        name = values[1];
        id = values[0];
    }

    var select_block = '<select class="sps-params-select-values sps-params-values">';
    select_block += '<option value="0"></option>';
    for (var k = 0; k < params.options['ALL_CATS'].length; k++) {
        var selected = '';
        var cat = params.options['ALL_CATS'][k];
        if (cat.id == id) {
            selected = 'selected=selected';
        }
        select_block += '<option ' + selected + ' value="' + cat.id + '" >' + cat.name + ' ( id - ' + cat.id + ')</option>'
    }
    select_block += '</select>';

    var block = params.element.appendChild(BX.create('div', {
        props: {
            "className": "sps-params-input-block"
        },
        html: [
            '<div>',
            '<label>' + params.options['CATEGORY_ID'] + '</label>',
            select_block,
            '</div>',
            '<div >',
            '<label>' + params.options['CATEGORY_SORT'] + '</label>',
            '<input type="text" class="sps-params-input-values sps-params-values" value="' + name + '">',
            '</div>',
            '<br>'
        ].join('')
    }));

    BX.bindDelegate(block, 'change', {'class': 'sps-params-values'}, BX.proxy(function ()
    {
        
        var valuesArray = [];
        var inputBlocks = params.element.getElementsByClassName("sps-params-input-block");
        Array.prototype.forEach.call(inputBlocks, function (inputBlock)
        {
            inputs = inputBlock.getElementsByClassName("sps-params-input-values");
            select = inputBlock.getElementsByClassName("sps-params-select-values");
            select_val = select[0].options[select[0].selectedIndex].value;
            if (select_val != 0 && select_val != '') {
                valuesArray.push([select_val,inputs[0].value]);
            }
        });
        hidden.value = JSON.stringify(valuesArray);
    }, this));
};

