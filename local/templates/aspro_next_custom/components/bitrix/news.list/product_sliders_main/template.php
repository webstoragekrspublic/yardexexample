<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<div class='podborki-ajax-sliders-loading main_podborki'>

</div>
<script>
    let arResult = [];
    <? foreach ($arResult['ITEMS'] as $arItem) { ?>
        arResult.push(<?= json_encode([
                            'arItem' => [
                                'ADDITIONAL_BLOCK' => $arItem['ADDITIONAL_BLOCK'],
                                'PROPERTIES' => [
                                    'LINK_GOODS_FILTER' => ["~VALUE" => $arItem['PROPERTIES']['LINK_GOODS_FILTER']["~VALUE"]],
                                    'LINK_GOODS' => ["~VALUE" => $arItem['PROPERTIES']['LINK_GOODS']["~VALUE"]]
                                ],
                                'NAME' => $arItem['NAME'],
                                '~PREVIEW_PICTURE' => $arItem["~PREVIEW_PICTURE"],
                                'DETAIL_PAGE_URL' => $arItem['DETAIL_PAGE_URL']
                            ],
                            'arParams' => [
                                "PRODUCT_COUNT" => $arParams["PRODUCT_COUNT"],
                                "HIDE_TITLE" => $arParams['HIDE_TITLE'],
                                'HIDE_SLIDER_PAGINATION' => $arParams['HIDE_SLIDER_PAGINATION']
                            ]
                        ]) ?>);
    <? } ?>

    function loadSliders(i = 0) {
        if (i == arResult.length) {
            $('.main_podborki').remove();
            return;
        }
        $.ajax({
            type: "POST",
            url: '<?= $templateFolder . '/ajax.php' ?>',
            data: arResult[i],
            success: function(response) {
                $response = $(response);
                $('.main_podborki').before($response);
            }
        }).always(function() {
            loadSliders(i + 1);
        });
    }
    loadSliders();
</script>