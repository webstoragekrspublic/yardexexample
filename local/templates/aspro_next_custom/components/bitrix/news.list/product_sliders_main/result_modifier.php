<?
$addBlocksParam = CUtil::JsObjectToPhp($arParams['~ADDITIONAL_BLOCKS']);
$additionalBlocksIBlockId = (int)$arParams['ADDITIONAL_IBLOCK_ID'];

if ($additionalBlocksIBlockId and $addBlocksParam) {
    $addBlocks = [];
    $sectionIds = array_column($addBlocksParam, 0);
    if ($sectionIds) {
        $qSection = CIBlockSection::GetList(
            [],
            ['IBLOCK_ID' => $additionalBlocksIBlockId, 'ID' => $sectionIds],
            false,
            ['ID', 'NAME', 'ACTIVE', 'SECTION_PAGE_URL']
        );
        $sectionsInfo = [];
        while ($row = $qSection->getNext()) {
            $sectionsInfo[$row['ID']] = $row;
        }


        foreach ($addBlocksParam as $blockParam) {
            $sectionId = $blockParam[0];
            if ($sectionsInfo[$sectionId]['ACTIVE'] == 'Y') {
                $addBlocks[$blockParam[1]][] = [
                    'ADDITIONAL_BLOCK' => true,
                    'SORT' => $blockParam[1],
                    'NAME' => $sectionsInfo[$sectionId]['NAME'],
                    'DETAIL_PAGE_URL' => $sectionsInfo[$sectionId]['SECTION_PAGE_URL'],
                    'IBLOCK_ID' => $additionalBlocksIBlockId,
                    'IBLOCK_SECTION_ID' => $sectionId,
                    'FILTER' => [
                        'IBLOCK_SECTION_ID' => $sectionId
                    ]
                ];
            }
        }
        if ($addBlocks) {
            ksort($addBlocks);
            $nextSortAddBlock = (array_key_first($addBlocks));

            $items = [];
            foreach ($arResult['ITEMS'] as $arItem) {
                if ($arItem['SORT'] > $nextSortAddBlock) {
                    foreach ($addBlocks[$nextSortAddBlock] as $addBlockItem) {
                        $items[] = $addBlockItem;
                    }
                    unset($addBlocks[$nextSortAddBlock]);
                    $nextSortAddBlock = (array_key_first($addBlocks));
                }
                $arItem['ADDITIONAL_BLOCK'] = false;
                $items[] = $arItem;
            }

            if ($addBlocks) {
                foreach ($addBlocks as $nextSortAddBlock => $addBlockItems) {
                    foreach ($addBlockItems as $addBlockItem) {
                        $items[] = $addBlockItem;
                    }
                }
            }

            $arResult['ITEMS'] = $items;
        }
    }
}
