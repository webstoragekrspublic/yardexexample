<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$IBQuery = CIBlock::GetList(
    [
        'NAME' => 'ASC',
        'ID' => 'ASC'
    ],
    [
        'ACTIVE' => 'Y'
    ]
);
$allIB = [];
while ($arProp = $IBQuery->Fetch()) {
    $allIB[$arProp['ID']] = '(' . $arProp['ID'] . ') ' . $arProp['NAME'];
}

$arTemplateParameters = array(
    'ADDITIONAL_IBLOCK_ID' => Array(
        "NAME" => "Выберите инфоблок Категорий",
        'PARENT' => 'VISUAL',
        "TYPE" => "LIST",
        "VALUES" => $allIB,
        'REFRESH' => 'Y'
    ),
    'PRODUCT_COUNT' => array(
        'PARENT' => 'VISUAL',
        'NAME' => 'Количество товаров в слайдерах',
        "TYPE" => "STRING",
        "DEFAULT" => '15'
    ),
    'TITLE_IMG_ID' => array(
        'PARENT' => 'VISUAL',
        'NAME' => 'TITLE_IMG_ID',
        "TYPE" => "STRING",
        "DEFAULT" => ''
    ),
    'HIDE_TITLE' => array(
        'PARENT' => 'VISUAL',
        'NAME' => 'HIDE_TITLE',
        "TYPE" => "STRING",
        "DEFAULT" => 'N'
    ),
    'HIDE_SLIDER_PAGINATION' => array(
        'PARENT' => 'VISUAL',
        'NAME' => 'HIDE_SLIDER_PAGINATION',
        "TYPE" => "STRING",
        "DEFAULT" => 'N'
    ),
);

if ($arCurrentValues['ADDITIONAL_IBLOCK_ID']) {
    $catsQuery = CIBlockSection::GetList(
        [
            'NAME' => 'ASC',
            'ID' => 'ASC'
        ],
        [
            'IBLOCK_ID' => $arCurrentValues['ADDITIONAL_IBLOCK_ID'],
            'ACTIVE' => 'Y'
        ]
    );
    $allCats = [];
    while ($arProp = $catsQuery->Fetch()) {
        $allCats[] = ['name' => $arProp['NAME'], 'id' => $arProp['ID']];
    }

    $arTemplateParameters['ADDITIONAL_BLOCKS'] = array(
        'NAME' => 'Дополнительные блоки категорий',
        'TYPE' => 'CUSTOM',
        'JS_FILE' => $templateFolder . "/settings.js",
        'JS_EVENT' => 'CustomSettingsEdit',
        'JS_DATA' => json_encode(
            array(
                'CATEGORY_ID' => 'Категория',
                'CATEGORY_SORT' => 'Сортировка',
                'ALL_CATS' => $allCats
            )
        ),
        'DEFAULT' => "",
        'PARENT' => 'VISUAL',
    );
}