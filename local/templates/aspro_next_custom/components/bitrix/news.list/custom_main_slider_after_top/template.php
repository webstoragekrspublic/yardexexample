<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
$sliderID = "slider_after_top_" . $this->randString();
?>
<? if ($arResult["ITEMS"]): ?>
    <div class="swiper-container slider_after_top" id="<?= $sliderID ?>">
        <div class="swiper-wrapper">
            <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
                $href = !empty($arItem['PROPERTIES']['HREF']['VALUE']) ? $arItem['PROPERTIES']['HREF']['VALUE'] : false;
                ?>
                <? if ($href): ?>
                    <a href="<?=$href; ?>" class="swiper-slide slider_after_top_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <? else: ?>
                    <div class="swiper-slide slider_after_top_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <? endif; ?>

                    <?
                    $imgId = $arItem["~PREVIEW_PICTURE"] ?? false;
                    if ($imgId) {
                        $imgSrc = \Helpers\CustomTools::getResizedPictSrc($imgId, ['width' => 360, 'height' => 180]);
                    }
                    ?>
                    <? if ($imgSrc): ?>
                        <div class="slider_after_top_item_img">
                            <picture>
                                <source type="image/webp" srcset="<?=Helpers\CustomTools::getWebpImgSrc($imgSrc);?>">
                                <source type="<?=$arItem['PREVIEW_PICTURE']['CONTENT_TYPE']?>" srcset="<?=$imgSrc; ?>">
                                <img class="slider_after_top_card_img" srcset="<?=$imgSrc; ?>" src="<?=$imgSrc; ?>"  alt="<?= $arItem["PREVIEW_TEXT"] ? $arItem["PREVIEW_TEXT"] : $arItem['NAME']; ?>" />
                            </picture>                                 
                        </div>
                    <? endif; ?>
                    <div class="slider_after_top_card_footer">
                        <? if (!empty($arItem["PREVIEW_TEXT"])): ?>
                            <?= $arItem["PREVIEW_TEXT"]; ?>
                        <? endif; ?>
                    </div>
                <? if ($href): ?>
                    </a>
                <? else: ?>
                    </div>
                <? endif; ?>
            <? endforeach; ?>
        </div>
    </div>

    <script>
        var swiper = new Swiper('#<?=$sliderID; ?>', {
            slidesPerView: 'auto',
            // centeredSlides: true,
            freeMode: true,
            spaceBetween: 30,
            // init: false,
            // breakpoints: {
            //     1200: {
            //         slidesPerView: 2,
            //         spaceBetween: 5,
            //     },
            // }
        });
    </script>


<? endif; ?>
