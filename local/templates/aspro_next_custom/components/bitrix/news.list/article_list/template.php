<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="article-list">
	<? if ($arParams["DISPLAY_TOP_PAGER"]) : ?>
		<?= $arResult["NAV_STRING"] ?>
	<? endif; ?>
	<? if (!count($arResult["ITEMS"])) { ?>
		<div class="article-empty">
			Cтатьи в процессе написания.
		</div>
	<? } ?>
	<? foreach ($arResult["ITEMS"] as $arItem) : ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => 'Удалить элемент со всеми данными?'));
		$imgSrc = \Helpers\CustomTools::getResizedPictSrc($arItem['~PREVIEW_PICTURE'], ['width' => 170, 'height' => 90]);
		?>
		<a class="article-link" href="<? echo $arItem["DETAIL_PAGE_URL"] ?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
			<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])) : ?>
				<picture class="article-thumb">
					<source type="image/webp" srcset="<?= Helpers\CustomTools::getWebpImgSrc($imgSrc); ?>">
					<source type="<?= $arItem['PREVIEW_PICTURE']['CONTENT_TYPE'] ?>" srcset="<?= $imgSrc; ?>">
					<img class="article-thumb" srcset="<?= $imgSrc; ?>" src="<?= $imgSrc; ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>" title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>" />
				</picture>
			<? endif ?>
			<div class="article-block_text">
				<? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]) : ?>
					<div class="article-name"><? echo $arItem["NAME"] ?></div>
				<? endif; ?>
				<? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]) : ?>
					<div class="article-descr">
						<? echo $arItem["PREVIEW_TEXT"]; ?>
					</div>
				<? endif; ?>
				<? if ($arParams["DISPLAY_DATE"] != "N" && $arItem["DISPLAY_ACTIVE_FROM"]) : ?>
					<b class="article-date-time"><? echo $arItem["DISPLAY_ACTIVE_FROM"] ?></b>
				<? endif ?>
			</div>
		</a>
	<? endforeach; ?>
	<? if ($arParams["DISPLAY_BOTTOM_PAGER"]) : ?>
		<?= $arResult["NAV_STRING"] ?>
	<? endif; ?>
</div>