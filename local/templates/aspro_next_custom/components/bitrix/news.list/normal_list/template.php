<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<? if ($arResult['ITEMS']): ?>
    <div class="news_list_block">

        <? foreach ($arResult['ITEMS'] as $i => $arItem): ?>
            <?

            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'));

            $imageSrc = $arItem['~PREVIEW_PICTURE'] ?
                \Helpers\CustomTools::getResizedPictSrc($arItem['~PREVIEW_PICTURE'], ['width' => 62, 'height' => 62])
                : false;

            ?>
            <div class="news_list_block_row" id="<?= $this->GetEditAreaId($arItem['ID']) ?>">
                <? if ($imageSrc): ?>
                    <div class="news_list_block_row_image_block">
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <img src="<?= $imageSrc ?>"
                                 alt="<?= ($imageSrc ? $arItem['PREVIEW_PICTURE']['ALT'] : $arItem['NAME']) ?>"
                                 title="<?= ($imageSrc ? $arItem['PREVIEW_PICTURE']['TITLE'] : $arItem['NAME']) ?>"
                                 class="img-responsive"/>
                        </a>
                    </div>
                <? endif; ?>
                <div class="news_list_block_row_text">
                    <div class="news_list_block_row_text_title">
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                            <?= $arItem['NAME'] ?>
                        </a>
                    </div>
                    <div class="news_list_block_row_text_more">
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">Смотреть все</a>
                    </div>
                </div>
            </div>
        <? endforeach; ?>

        <div class="bottom_nav"><?= $arResult['NAV_STRING'] ?></div>
    </div>
<? endif; ?>
