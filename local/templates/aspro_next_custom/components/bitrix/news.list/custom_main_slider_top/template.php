<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?

//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
$sliderID = "custom_main_slider_top" . $this->randString();
$svgArrowRight = file_get_contents($_SERVER["DOCUMENT_ROOT"].'/images/svg/right-arrow.svg');
?>
<? if ($arResult["ITEMS"]): ?>
    <div class="main_slider_top">
        <div class="swiper-container" id="<?= $sliderID ?>">
            <div class="swiper-wrapper">
                <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

                    $href = !empty($arItem['PROPERTIES']['HREF']['VALUE']) ? $arItem['PROPERTIES']['HREF']['VALUE'] : false;
                    $imgSrc = \Helpers\CustomTools::getResizedPictSrc($arItem['~PREVIEW_PICTURE'], ["width"=>1650, "height"=>539], BX_RESIZE_IMAGE_EXACT);
                    $mobileImgSrc = \Helpers\CustomTools::getResizedPictSrc($arItem['~DETAIL_PICTURE'], ["width"=>768, "height"=>567], BX_RESIZE_IMAGE_EXACT);
                    $title = !empty($arItem['PREVIEW_TEXT']) ? $arItem['PREVIEW_TEXT'] : false;
                    ?>
                    <? if ($href):
                        $isYardexDomen = preg_match("/((http\S+yardex)|yardex|^\/)/", $href);
                    ?>
                        <a href="<?=$href; ?>" class="swiper-slide main_slider_top_slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>" <?= $isYardexDomen == false ? 'rel="nofollow"' : ''?>>
                    <? else: ?>
                        <div class="swiper-slide main_slider_top_slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <? endif; ?>
                             <picture>
                                <source type="image/webp" srcset="<?=Helpers\CustomTools::getWebpImgSrc($imgSrc);?>">
                                <source type="<?=$arItem['PREVIEW_PICTURE']['CONTENT_TYPE']?>" srcset="<?=$imgSrc; ?>">
                                <img class="hidden-xs main_slider_top_slide_image" srcset="<?=$imgSrc; ?>" src="<?=$imgSrc; ?>"  <?=$title?'title="'.$title.'"':''; ?> />
                            </picture>
                            <picture>
                                <source type="image/webp" srcset="<?=Helpers\CustomTools::getWebpImgSrc($mobileImgSrc);?>">
                                <source type="<?=$arItem['DETAIL_PICTURE']['CONTENT_TYPE']?>" srcset="<?=$mobileImgSrc; ?>">
                                <img class="visible-xs main_slider_top_slide_image" srcset="<?=$mobileImgSrc; ?>" src="<?=$mobileImgSrc; ?>"   <?=$title?'title="'.$title.'"':''; ?>  />
                            </picture>
                    <? if ($href): ?>
                        </a>
                    <? else: ?>
                        </div>
                    <? endif; ?>

                <? endforeach; ?>
            </div>
            <div class="swiper-button-next svg-arrow">
                <div class="hidden-xs">
                    <?=$svgArrowRight; ?>
                </div>
            </div>
            <div class="swiper-button-prev svg-arrow">
                <div class="hidden-xs">
                    <?=$svgArrowRight; ?>
                </div>
            </div>
            <? if (!isset($arParams['HIDE_SLIDER_PAGINATION']) or !$arParams['HIDE_SLIDER_PAGINATION'] == 'Y'): ?>
                <div class="swiper-pagination big_points"></div>
            <? endif; ?>
        </div>
    </div>


    <script>
        var swiper = new Swiper('#<?=$sliderID; ?>', {
            slidesPerView: 1,
            spaceBetween: 0,
            loop: true,
            autoplay: {
                delay: 4000,
                disableOnInteraction: false,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            on: {
                init() {
                    this.el.addEventListener('mouseenter', () => {
                        this.autoplay.stop();
                    });

                    this.el.addEventListener('mouseleave', () => {
                        this.autoplay.start();
                    });
                }
            },
            breakpoints: {}
        });
    </script>

<? endif; ?>
