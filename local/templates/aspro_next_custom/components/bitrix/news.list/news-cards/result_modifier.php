<?
foreach($arResult['ITEMS'] as $key => $arItem){
	if($SID = $arItem['IBLOCK_SECTION_ID']){
		$arSectionsIDs[] = $SID;
	}
	$arResult['ITEMS'][$key]['DETAIL_PAGE_URL'] = CNext::FormatNewsUrl($arItem);
	
	if(strlen($arItem['DISPLAY_PROPERTIES']['REDIRECT']['VALUE']))
		unset($arResult['ITEMS'][$key]['DISPLAY_PROPERTIES']['REDIRECT']);
	
	CNext::getFieldImageData($arResult['ITEMS'][$key], array('PREVIEW_PICTURE'));

	if ($arItem['DATE_ACTIVE_TO']){
        $arResult['ITEMS'][$key]['DISPLAY_COUNT_TO'] = downcounter($arItem['DATE_ACTIVE_TO']);
    }
}

if($arSectionsIDs){
	$arResult['SECTIONS'] = CNextCache::CIBLockSection_GetList(array('SORT' => 'ASC', 'NAME' => 'ASC', 'CACHE' => array('TAG' => CNextCache::GetIBlockCacheTag($arParams['IBLOCK_ID']), 'GROUP' => array('ID'), 'MULTI' => 'N')), array('ID' => $arSectionsIDs));
}
//Подсчет количества оставшихся дней
function downcounter($date){
    $check_time = strtotime($date) - time();
    if($check_time <= 0){
        return false;
    }

    $days = floor($check_time/86400);
    $hours = floor(($check_time%86400)/3600);
    $minutes = floor(($check_time%3600)/60);

    $str = [];
    if($days > 0) {
        $str['DAYS_VALUE'] = $days;
        $str['DAYS_STR'] = declension($days,array('день','дня','дней'), true);
    }
    if($hours > 0) {
        $str['HOURS_VALUE'] = $hours;
        $str['HOURS_STR'] = 'ч';
    }
    if($minutes > 0) {
        $str['MINUTES_VALUE'] = $minutes;
        $str['MINUTES_STR'] = 'мин';
    }
//    if($days > 0) $str .= declension($days,array('день','дня','дней')).' ';
//    if($hours > 0) $str .= declension($hours,array('час','часа','часов')).' ';
//    if($minutes > 0) $str .= declension($minutes,array('минута','минуты','минут'));

    return $str;
}
//Склоняем слова
function declension($digit,$expr,$onlyword){
    if(!is_array($expr)) $expr = array_filter(explode(' ', $expr));
    if(empty($expr[2])) $expr[2]=$expr[1];
    $i=preg_replace('/[^0-9]+/s','',$digit)%100;
    if($onlyword) $digit='';
    if($i>=5 && $i<=20) $res=$digit.' '.$expr[2];
    else
    {
        $i%=10;
        if($i==1) $res=$digit.' '.$expr[0];
        elseif($i>=2 && $i<=4) $res=$digit.' '.$expr[1];
        else $res=$digit.' '.$expr[2];
    }
    return trim($res);
}
?>