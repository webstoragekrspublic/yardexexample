<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="recipes-list">
	<? if ($arParams["DISPLAY_TOP_PAGER"]) : ?>
		<?= $arResult["NAV_STRING"] ?>
	<? endif; ?>
	<? if (!count($arResult["ITEMS"])) { ?>
		<div class="recipes-empty">
			Рецепты в процессе написания.
		</div>
	<? } ?>
	<? foreach ($arResult["ITEMS"] as $arItem) : ?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => 'Удалить элемент со всеми данными?'));
		$imgSrc = \Helpers\CustomTools::getResizedPictSrc($arItem['~PREVIEW_PICTURE'], ['width' => 260, 'height' => 180]);
		?>
		<a class="recipes-item col-5 col-sm-4 col-xs-12 col-lg-3" href="<? echo $arItem["DETAIL_PAGE_URL"] ?>" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
			<? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arItem["PREVIEW_PICTURE"])) : ?>
				<div class="recipes-preview_image_wrapper">
					<picture class="recipes-preview_image ">
						<source type="image/webp" srcset="<?= Helpers\CustomTools::getWebpImgSrc($imgSrc); ?>">
						<source type="<?= $arItem['PREVIEW_PICTURE']['CONTENT_TYPE'] ?>" srcset="<?= $imgSrc; ?>">
						<img class="recipes-preview_image" srcset="<?= $imgSrc; ?>" src="<?= $imgSrc; ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>" title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>" />
					</picture>
				</div>
			<? endif ?>
			<div class="recipes-block_text">
				<div class="recipes-props">
					<div class="recipes-prop cook_time">
						<? echo $arItem['PROPERTIES']['COOK_TIME']['VALUE'] ?>
					</div>
					<div class="recipes-props-dot"></div>
					<div class="recipes-prop ingredients_count">
						<? echo count($arItem['PROPERTIES']['INGREDIENTS']['VALUE']) . ' ' . getInclinationByNumber(count($arItem['PROPERTIES']['INGREDIENTS']['VALUE']), ['ингредиент', 'ингредиента', 'ингредиентов']) ?>
					</div>
				</div>
				<? if ($arParams["DISPLAY_NAME"] != "N" && $arItem["NAME"]) : ?>
					<div class="recipes-name"><? echo $arItem["NAME"] ?></div>
				<? endif; ?>

			</div>

		</a>
	<? endforeach; ?>
	<? if ($arParams["DISPLAY_BOTTOM_PAGER"]) : ?>
		<br /><?= $arResult["NAV_STRING"] ?>
	<? endif; ?>
</div>