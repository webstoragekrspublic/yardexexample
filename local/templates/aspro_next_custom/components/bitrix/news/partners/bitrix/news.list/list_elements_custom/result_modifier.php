<?
$itemsStartFromRusLetter = [];
$itemsStartFromEngLetter = [];
$itemsStartFromNumbersLetter = [];
$itemsStartFromOtherLetter = [];
$allRusLetters = [];
$brandsQty = [];


global $arTheme;
$catalogIBlockId = $arTheme['CATALOG_IBLOCK_ID']['VALUE'];

$arFilter = ['IBLOCK_ID' => $catalogIBlockId, 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', '>CATALOG_QUANTITY' => 0];
$productsQ = CIBlockElement::GetList(
    [],
    $arFilter,
    ['PROPERTY_TORGOVAJAMARKA'],
    []
);

while ($row = $productsQ->fetch()) {
    if ($row['PROPERTY_TORGOVAJAMARKA_VALUE']){
        $brandsQty[$row['PROPERTY_TORGOVAJAMARKA_VALUE']] = $row['CNT'];
    }
}

foreach ($arResult['ITEMS'] as $key => $arItem) {
    if (isset($brandsQty[$arItem['ID']])){
        $itemNameFirstLetter = strtoupper(mb_substr($arItem['NAME'], 0, 1,'UTF-8'));
        $arItem['BRAND_PRODUCTS_CNT'] = (int)$brandsQty[$arItem['ID']];

        if (preg_match('/[А-ЯЁ]/', $itemNameFirstLetter)) {
            $itemsStartFromRusLetter[$itemNameFirstLetter][] = $arItem;
            $allRusLetters[$itemNameFirstLetter] = $itemNameFirstLetter;
        } else if (preg_match('/[A-Z]/', $itemNameFirstLetter)) {
            $itemsStartFromEngLetter['A-Z'][] = $arItem;
        } else if (preg_match('/[0-9]/', $itemNameFirstLetter)) {
            $itemsStartFromNumbersLetter['0-9'][] = $arItem;
        } else {
            $itemsStartFromOtherLetter['Прочее'][] = $arItem;
        }
    }
}
$arResult['SORTED_ITEMS'] = $itemsStartFromRusLetter + $itemsStartFromEngLetter + $itemsStartFromNumbersLetter + $itemsStartFromOtherLetter + $allRusLetters;
