<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>

<div class="row partners_all_letters">
    <div class="col-xs-12">
        <? foreach ($arResult['SORTED_ITEMS'] as $letter => $arItems): ?>
            <a class="partners_all_letters_item slow_slide" href="#letter_<?=urlencode($letter); ?>"><?= $letter; ?></a>
        <? endforeach; ?>
    </div>
</div>

<? foreach ($arResult['SORTED_ITEMS'] as $letter => $arItems): ?>

    <div id="letter_<?=urlencode($letter); ?>" class="row partners_letter">
        <div class="col-xs-12">
            <?= $letter; ?>
        </div>
    </div>

    <div class="row partners_row">
        <? foreach ($arItems as $arItem): ?>
            <div class="col-xs-6 col-sm-4 col-md-3 partners_row_item">
                <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>" class="dark-color">
                    <?= $arItem['NAME'].' ('.$arItem['BRAND_PRODUCTS_CNT'].')'; ?>
                </a>
            </div>
        <? endforeach; ?>
    </div>

<? endforeach; ?>