<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>
<?

use \Bitrix\Main\Application;
use \Bitrix\Main\Localization\Loc;

$request = Application::getInstance()->getContext()->getRequest();

$arItemFilter = CNext::GetCurrentElementFilter($arResult["VARIABLES"], $arParams);
$selectedSectionId = $request->get('section_id') ? (int)$request->get('section_id') : 0;

$pageItemQ = CIBlockElement::GetList(
    [],
    $arItemFilter,
    false,
    false,
    ['ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_LINK_GOODS_FILTER', 'PROPERTY_LINK_GOODS', 'ACTIVE']
);
$pageItem = [];
$pageItemLinkGoods = [];
while ($row = $pageItemQ->getNext()) {
    if (!$pageItem) {
        $pageItem = [
            'ID' => $row['ID'],
            'IBLOCK_ID' => $row['IBLOCK_ID'],
            'ACTIVE' => $row['ACTIVE'],
            'NAME' => $row['NAME'],
            'LINK_GOODS_CUSTOM_FILTER' => $row['~PROPERTY_LINK_GOODS_FILTER_VALUE']
        ];
    }
    if ($row['PROPERTY_LINK_GOODS_VALUE']) {
        $pageItemLinkGoods[] = $row['PROPERTY_LINK_GOODS_VALUE'];
    }
}
$pageItem['LINK_GOODS'] = $pageItemLinkGoods;
if (!isset($pageItem['ACTIVE']) or $pageItem['ACTIVE'] != 'Y') {
    LocalRedirect("/404.php");
}
?>

<?
$isAjax = "N";
if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest" && isset($_GET["ajax_get"]) && $_GET["ajax_get"] == "Y" || (isset($_GET["ajax_basket"]) && $_GET["ajax_basket"] == "Y")) {
    $isAjax = "Y";
}
$isJsonAjax = 'N';
if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == "xmlhttprequest" && isset($_GET["ajax_get_filter"]) && $_GET["ajax_get_filter"] == "Y") {
    $isJsonAjax = "Y";
    $ajaxJsonAnswer = [];
}
?>

<?
if ($arParams["FILTER_NAME"] == '') {
    $arParams["FILTER_NAME"] = "arrFilter";
}

$catalogIBlockID = $arParams["IBLOCK_CATALOG_ID"];

$arParams["AJAX_FILTER_CATALOG"] = "N";
$pageAllItemsId = [];
$start = microtime(true);


$pageAllItemsId = \Helpers\Cache::getProductsIdForCustomFilters($pageItem['LINK_GOODS_CUSTOM_FILTER'], json_encode($pageItem['LINK_GOODS']));
if ($pageAllItemsId && \Helpers\CustomTools::isFastDeliveryEnabled()){
    $pageAllItemsFilter = [
        'ID' => $pageAllItemsId,
        'PROPERTY_'.\Helpers\Constants::PROP_FAST_DELIVERY => \Helpers\Constants::PROP_FAST_DELIVERY_VALUE_EXPRESS
    ];
    $pageAllItems = CNextCache::CIBLockElement_GetList(
        array(
            'CACHE' =>
                array(
                    "MULTI" => "Y",
                    "TAG" => CNextCache::GetIBlockCacheTag($catalogIBlockID)
                )
        ),
        $pageAllItemsFilter,
        false,
        false,
        array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID")
    );
    $pageAllItemsId = [];
    foreach ($pageAllItems as $pageAllItem){
        $pageAllItemsId[$pageAllItem['ID']] = $pageAllItem['ID'];
    }
}


?>


<? $APPLICATION->IncludeComponent(
    "bitrix:news.detail",
    "partners",
    array(
        "DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
        "DISPLAY_NAME" => $arParams["DISPLAY_NAME"],
        "SHOW_GALLERY" => $arParams["SHOW_GALLERY"],
        "T_GALLERY" => $arParams["T_GALLERY"],
        "DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
        "DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
        "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
        "FIELD_CODE" => $arParams["DETAIL_FIELD_CODE"],
        "PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
        "DETAIL_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
        "SECTION_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "META_KEYWORDS" => $arParams["META_KEYWORDS"],
        "META_DESCRIPTION" => $arParams["META_DESCRIPTION"],
        "BROWSER_TITLE" => $arParams["BROWSER_TITLE"],
        "DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
        "SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
        "SET_TITLE" => $arParams["SET_TITLE"],
        "SET_STATUS_404" => $arParams["SET_STATUS_404"],
        "INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
        "ADD_SECTIONS_CHAIN" => $arParams["ADD_SECTIONS_CHAIN"],
        "ADD_ELEMENT_CHAIN" => $arParams["ADD_ELEMENT_CHAIN"],
        "ACTIVE_DATE_FORMAT" => $arParams["DETAIL_ACTIVE_DATE_FORMAT"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
        "GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
        "DISPLAY_TOP_PAGER" => $arParams["DETAIL_DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER" => $arParams["DETAIL_DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE" => $arParams["DETAIL_PAGER_TITLE"],
        "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => $arParams["DETAIL_PAGER_TEMPLATE"],
        "PAGER_SHOW_ALL" => $arParams["DETAIL_PAGER_SHOW_ALL"],
        "CHECK_DATES" => $arParams["CHECK_DATES"],
        "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
        "ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
        "IBLOCK_URL" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
        "USE_SHARE" => $arParams["USE_SHARE"],
        "SHARE_HIDE" => $arParams["SHARE_HIDE"],
        "SHARE_TEMPLATE" => $arParams["SHARE_TEMPLATE"],
        "SHARE_HANDLERS" => $arParams["SHARE_HANDLERS"],
        "SHARE_SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
        "SHARE_SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
        "SHOW_TOP_BANNER" => "Y",
    ),
    $component
); ?>

<? if ($isJsonAjax == "Y" or $isAjax == 'Y') : ?>
    <? $APPLICATION->RestartBuffer(); ?>
<? endif; ?>
<? if ($isJsonAjax == "Y") : ?>
    <? ob_start(); ?>
<? endif; ?>

<? if ($pageAllItemsId) { ?>

    <? $APPLICATION->IncludeComponent(
        "custom:empty_component",
        "catalog_sections_by_items",
        [
            'IBLOCK_ID' => $catalogIBlockID,
            'ITEMS_ID' => $pageAllItemsId,
            'PAGE_BASE_URL' => $APPLICATION->GetCurPage(),
            'USE_SECTION_CODE' => 'N',
            'SELECTED_SECTION_ID' => (int)$selectedSectionId,
        ],
        false
    );
    ?>

    <?
    $GLOBALS[$arParams["FILTER_NAME"]]['ID'] = $pageAllItemsId;
    if ($selectedSectionId) {
        $GLOBALS[$arParams["FILTER_NAME"]][] = array("SECTION_ID" => $selectedSectionId);
    }
    ?>

    <? if ($isJsonAjax == 'Y') : ?>
        <? ob_start(); ?>
    <? endif; ?>

    <? $APPLICATION->IncludeComponent(
        "aspro:catalog.smart.filter",
        'main_compact_ajax_default',
        array(
            "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID" => $catalogIBlockID,
            "FILTER_NAME" => $arParams["FILTER_NAME"],
            "PRICE_CODE" => ($arParams["USE_FILTER_PRICE"] == 'Y' ? $arParams["FILTER_PRICE_CODE"] : $arParams["PRICE_CODE"]),
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_NOTES" => "",
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "SECTION_IDS" => [$selectedSectionId],
            "ELEMENT_IDS" => $pageAllItemsId,
            "SAVE_IN_SESSION" => "N",
            "XML_EXPORT" => "Y",
            "SECTION_TITLE" => "NAME",
            "HIDDEN_PROP" => array("BRAND"),
            "SECTION_DESCRIPTION" => "DESCRIPTION",
            "SHOW_HINTS" => $arParams["SHOW_HINTS"],
            'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
            'CURRENCY_ID' => $arParams['CURRENCY_ID'],
            'DISPLAY_ELEMENT_COUNT' => $arParams['DISPLAY_ELEMENT_COUNT'],
            "INSTANT_RELOAD" => "Y",
            "SEF_MODE" => (strlen($arResult["URL_TEMPLATES"]["smart_filter"]) ? "Y" : "N"),
            "SEF_RULE" => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["smart_filter"],
            "SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
            "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
        ),
        $component
    );
    ?>

    <? if ($isJsonAjax == 'Y') : ?>
        <? $ajaxJsonAnswer['filtersContainer'] = ob_get_clean(); ?>
    <? endif; ?>


    <? if ($isJsonAjax == "Y") : ?>
        <? ob_start(); ?>
    <? endif; ?>
    <? $APPLICATION->IncludeComponent(
        "custom:empty_component",
        "catalog_sort_block",
        [
            'SORT_PRICES' => $arParams['SORT_PRICES'],
        ],
        false
    );
    ?>
    <? if ($isJsonAjax == "Y") : ?>
        <? $ajaxJsonAnswer['sortButtonsContainer'] = ob_get_clean(); ?>
    <? endif; ?>

    <?
    $templateData = \Helpers\GlobalStorage::get('catalog_sort_block_templateData');
    $sort = $templateData['sort'] ?? 'NAME';
    $sort_order = $templateData['sort_order'] ?? 'asc';
    ?>

    <div id="search_product_container" class="clearfix catalog compact inner_wrapper json_ajax">
        <? if ($isJsonAjax == 'Y') : ?>
            <? ob_start(); ?>
        <? endif; ?>

        <? if ($isAjax != "Y") {
            $frame = new \Bitrix\Main\Page\FrameHelper("viewtype-brand-block");
            $frame->begin();
        } ?>


        <? if (isset($GLOBALS[$arParams["FILTER_NAME"]]["FACET_OPTIONS"]))
            unset($GLOBALS[$arParams["FILTER_NAME"]]["FACET_OPTIONS"]); ?>

        <? $arTransferParams = array(
            "SHOW_ABSENT" => $arParams["SHOW_ABSENT"],
            "HIDE_NOT_AVAILABLE_OFFERS" => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
            "PRICE_CODE" => $arParams["PRICE_CODE"],
            "OFFER_TREE_PROPS" => $arParams["OFFER_TREE_PROPS"],
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
            "CURRENCY_ID" => $arParams["CURRENCY_ID"],
            "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
            "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
            "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
            "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
            "LIST_OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],
            "LIST_OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
            "SHOW_DISCOUNT_TIME" => $arParams["SHOW_DISCOUNT_TIME"],
            "SHOW_COUNTER_LIST" => $arParams["SHOW_COUNTER_LIST"],
            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
            "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
            "SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
            "SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
            "SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"],
            "SHOW_DISCOUNT_PERCENT_NUMBER" => $arParams["SHOW_DISCOUNT_PERCENT_NUMBER"],
            "USE_REGION" => "N",
            "DEFAULT_COUNT" => $arParams["DEFAULT_COUNT"],
            "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
            "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
            "PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
            "ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
            "SHOW_DISCOUNT_TIME_EACH_SKU" => $arParams["SHOW_DISCOUNT_TIME_EACH_SKU"],
            "SHOW_ARTICLE_SKU" => $arParams["SHOW_ARTICLE_SKU"],
            "OFFER_ADD_PICT_PROP" => $arParams["OFFER_ADD_PICT_PROP"],
            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
            "OFFER_SHOW_PREVIEW_PICTURE_PROPS" => $arParams["OFFER_SHOW_PREVIEW_PICTURE_PROPS"],
        ); ?>

        <div class="ajax_load block js_wrapper_items"
             data-params='<?= str_replace('\'', '"', CUtil::PhpToJSObject($arTransferParams, false)) ?>'>
            <? if ($isAjax == "Y" && $isJsonAjax != "Y") : ?>
                <? $APPLICATION->RestartBuffer(); ?>
            <? endif; ?>


            <? $APPLICATION->IncludeComponent(
                "custom:catalog.section",
                'catalog_block',
                array(
                    "USE_REGION" => "N",
                    "SHOW_UNABLE_SKU_PROPS" => $arParams["SHOW_UNABLE_SKU_PROPS"],
                    "ALT_TITLE_GET" => $arParams["ALT_TITLE_GET"],
                    "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"],
                    "IBLOCK_TYPE" => $arParams["IBLOCK_CATALOG_TYPE"],
                    "IBLOCK_ID" => $catalogIBlockID,
                    "SHOW_COUNTER_LIST" => "Y",
                    "SECTION_ID" => $selectedSectionId ? $selectedSectionId : '',
                    "SECTION_CODE" => '',
                    "AJAX_REQUEST" => (($isAjax == "Y" && $isJsonAjax != "Y") ? "Y" : "N"),
                    "ELEMENT_SORT_FIELD" => $sort,
                    "ELEMENT_SORT_ORDER" => $sort_order,
                    "SHOW_DISCOUNT_TIME_EACH_SKU" => $arParams["SHOW_DISCOUNT_TIME_EACH_SKU"],
                    "ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                    "ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                    "FILTER_NAME" => $arParams["FILTER_NAME"],
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "SHOW_ALL_WO_SECTION" => "Y",
                    "PAGE_ELEMENT_COUNT" => $arParams["LINKED_ELEMENST_PAGE_COUNT"] ? $arParams["LINKED_ELEMENST_PAGE_COUNT"] : 40,
                    "LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
                    "DISPLAY_TYPE" => 'block',
                    "PROPERTY_CODE" => $arParams["LIST_PROPERTY_CATALOG_CODE"],
                    "SHOW_ARTICLE_SKU" => $arParams["SHOW_ARTICLE_SKU"],
                    "SHOW_MEASURE_WITH_RATIO" => $arParams["SHOW_MEASURE_WITH_RATIO"],

                    "OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
                    "OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
                    "OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
                    "OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
                    "OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
                    'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],

                    "OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

                    "SECTION_URL" => "",
                    "DETAIL_URL" => "",
                    "BASKET_URL" => '/basket/',
                    "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                    "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
                    "SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
                    "AJAX_MODE" => $arParams["AJAX_MODE"],
                    "AJAX_OPTION_JUMP" => $arParams["AJAX_OPTION_JUMP"],
                    "AJAX_OPTION_STYLE" => $arParams["AJAX_OPTION_STYLE"],
                    "AJAX_OPTION_HISTORY" => $arParams["AJAX_OPTION_HISTORY"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                    "CACHE_FILTER" => "Y",
                    "META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
                    "META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
                    "BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
                    "ADD_SECTIONS_CHAIN" => "N",
                    "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                    'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],
                    "SET_TITLE" => "N",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "MESSAGE_404" => "",
                    "FILE_404" => "",
                    "PRICE_CODE" => $arParams['PRICE_CODE'],
                    "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                    "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                    "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                    "USE_PRODUCT_QUANTITY" => $arParams["USE_PRODUCT_QUANTITY"],
                    "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
                    "DISPLAY_TOP_PAGER" => "N",
                    "DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],

                    "PAGER_TITLE" => $arParams["PAGER_TITLE"],
                    "PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
                    "PAGER_TEMPLATE" => "main",
                    "PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
                    "PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],

                    "AJAX_OPTION_ADDITIONAL" => "",
                    "ADD_CHAIN_ITEM" => "N",
                    "SHOW_QUANTITY" => $arParams["SHOW_QUANTITY"],
                    "SHOW_QUANTITY_COUNT" => $arParams["SHOW_QUANTITY_COUNT"],
                    "SHOW_DISCOUNT_PERCENT" => $arParams["SHOW_DISCOUNT_PERCENT"],
                    "SHOW_DISCOUNT_PERCENT_NUMBER" => $arParams["SHOW_DISCOUNT_PERCENT_NUMBER"],
                    "SHOW_DISCOUNT_TIME" => $arParams["SHOW_DISCOUNT_TIME"],
                    "SHOW_OLD_PRICE" => $arParams["SHOW_OLD_PRICE"],
                    "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                    "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                    "USE_STORE" => $arParams["USE_STORE"],
                    "MAX_AMOUNT" => $arParams["MAX_AMOUNT"],
                    "MIN_AMOUNT" => $arParams["MIN_AMOUNT"],
                    "USE_MIN_AMOUNT" => $arParams["USE_MIN_AMOUNT"],
                    "USE_ONLY_MAX_AMOUNT" => $arParams["USE_ONLY_MAX_AMOUNT"],
                    "DISPLAY_WISH_BUTTONS" => $arParams["DISPLAY_WISH_BUTTONS"],
                    "LIST_DISPLAY_POPUP_IMAGE" => $arParams["LIST_DISPLAY_POPUP_IMAGE"],
                    "DEFAULT_COUNT" => 1,
                    "SHOW_MEASURE" => $arParams["SHOW_MEASURE"],
                    "SHOW_HINTS" => $arParams["SHOW_HINTS"],
                    "OFFER_HIDE_NAME_PROPS" => $arParams["OFFER_HIDE_NAME_PROPS"],
                    "SHOW_SECTIONS_LIST_PREVIEW" => $arParams["SHOW_SECTIONS_LIST_PREVIEW"],
                    "SECTIONS_LIST_PREVIEW_PROPERTY" => $arParams["SECTIONS_LIST_PREVIEW_PROPERTY"],
                    "SHOW_SECTION_LIST_PICTURES" => $arParams["SHOW_SECTION_LIST_PICTURES"],
                    "USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
                    "ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
                    "PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
                    "PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
                    "SALE_STIKER" => $arParams["SALE_STIKER"],
                    "STIKERS_PROP" => $arParams["STIKERS_PROP"],
                    "SHOW_RATING" => $arParams["SHOW_RATING"],
                    "DISPLAY_COMPARE" => ($arParams["DISPLAY_COMPARE"] ? $arParams["DISPLAY_COMPARE"] : "Y"),
                    "ADD_PICT_PROP" => $arParams["ADD_PICT_PROP"],
                    "OFFER_SHOW_PREVIEW_PICTURE_PROPS" => $arParams["OFFER_SHOW_PREVIEW_PICTURE_PROPS"],
                ),
                $component,
                array("HIDE_ICONS" => $isAjax)
            ); ?>


            <? if ($isAjax == "Y" && $isJsonAjax != "Y") : ?>
                <? die(); ?>
            <? endif; ?>
        </div>
        <? if ($isAjax != "Y") {
            $frame->end();
        } ?>

        <? if ($isJsonAjax == 'Y') : ?>
            <? $ajaxJsonAnswer['productContainer'] = ob_get_clean(); ?>
        <? endif; ?>

    </div>

<? } ?>

<? if ($isJsonAjax == 'Y') : ?>
    <?
    ob_clean();
    die(json_encode($ajaxJsonAnswer));
    ?>
<? endif; ?>

<? if ($isAjax == 'Y') : ?>
    <? die(); ?>
<? endif; ?>


<div class="clearfix"></div>
<hr class="bottoms"/>
<div class="row">
    <div class="col-md-6 share">
        <? if ($arParams["USE_SHARE"] == "Y" && $arElement) : ?>
            <div class="line_block">
                <? $APPLICATION->IncludeFile(SITE_DIR . "include/share_buttons.php", array(), array("MODE" => "html", "NAME" => GetMessage('CT_BCE_CATALOG_SOC_BUTTON'))); ?>
            </div>
        <? endif; ?>
    </div>
    <div class="col-md-6">
        <a class="back-url url-block" href="<?= $arResult['FOLDER'] . $arResult['URL_TEMPLATES']['news'] ?>"><i
                    class="fa fa-angle-left"></i><span>Список Подборок товаров</span></a>
    </div>
</div>