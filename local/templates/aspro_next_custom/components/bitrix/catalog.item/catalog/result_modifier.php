<?

use Bitrix\Main\Type\Collection;
use Bitrix\Currency\CurrencyTable;
use Bitrix\Iblock;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */


$itemPrices = $arResult['ITEM']['ITEM_PRICES'][array_key_first($arResult['ITEM']['ITEM_PRICES'])];

if (isset($arResult['ITEM']['ITEM_MEASURE_RATIO_SELECTED']) and $arResult['ITEM']['ITEM_MEASURE_RATIOS'][$arResult['ITEM']['ITEM_MEASURE_RATIO_SELECTED']]) {
    $arResult['ITEM']["CATALOG_MEASURE_RATIO"] = $arResult['ITEM']['ITEM_MEASURE_RATIOS'][$arResult['ITEM']['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
}

if (!isset($arResult['ITEM']['MIN_PRICE'])) {
    $arResult['ITEM']['MIN_PRICE'] = [
        'VALUE' => $itemPrices['PRICE'],
        'CAN_BUY' => $arResult['ITEM']['CAN_BUY'],
    ];
}

if (!isset($arResult['ITEM']['PRICE_MATRIX'])) {
    $arResult['ITEM']['PRICE_MATRIX'] = [
        'ROWS' =>
            [
                $itemPrices['QUANTITY_HASH'] =>
                    [
                        'QUANTITY_FROM' => (float)$itemPrices['QUANTITY_FROM'],
                        'QUANTITY_TO' => (float)$itemPrices['QUANTITY_TO'],
                    ],
            ],
        'COLS' =>
            array(
                $itemPrices['PRICE_TYPE_ID'] =>
                    array(
                        'ID' => $itemPrices['PRICE_TYPE_ID']
                    ),
            ),
        'MATRIX' =>
            array(
                $itemPrices['PRICE_TYPE_ID'] =>
                    array(
                        $itemPrices['QUANTITY_HASH'] =>
                            array(
                                'ID' => $itemPrices['ID'],
                                'PRICE' => $itemPrices['BASE_PRICE'],
                                'DISCOUNT_PRICE' => $itemPrices['RATIO_PRICE'],
                                'UNROUND_DISCOUNT_PRICE' => $itemPrices['UNROUND_PRICE'],
                                'CURRENCY' => $itemPrices['CURRENCY'],
                                'VAT_RATE' => $arResult['ITEM']['PRODUCT']['VAT_RATE'],
                                'PRINT_PRICE' => $itemPrices['PRINT_BASE_PRICE'],
                                'PRINT_DISCOUNT_PRICE' => $itemPrices['PRINT_PRICE'],
                            ),
                    ),
            ),
        'CAN_BUY' => $arResult['ITEM']['CAN_BUY'] == 'Y'?[0 => $itemPrices['PRICE_TYPE_ID']]:[],
        'AVAILABLE' => $arResult['ITEM']['PRODUCT']['AVAILABLE'],
        'CURRENCY_LIST' => [],
    ];
}

$firstMatrixArr = $arResult['ITEM']['PRICE_MATRIX']['MATRIX'][array_key_first($arResult['ITEM']['PRICE_MATRIX']['MATRIX'])];

$arResult['ITEM']['FIRST_MATRIX'] = $firstMatrixArr[array_key_first($firstMatrixArr)];