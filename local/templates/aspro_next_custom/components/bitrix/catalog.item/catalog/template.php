<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Custom\FavoritesOnCookies\CurrentUserFavorites;
use \Helpers\CurrentCart;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$SHOW_MODE = $arParams['SHOW_MODE']??'';

$this->setFrameMode(true);

if (isset($arResult['ITEM']) && Loader::includeModule('custom.favoritesoncookies')) {
    $arParams["SHOW_OLD_PRICE"] = 'Y';
    $arItem = $arResult['ITEM'];
    $defaultCount = 1;
    $basketUrl = SITE_DIR . "basket/";
    $isInCart = CurrentCart::getInstance()->isProductInCart($arItem['ID']);

    $discountValue = round($arItem['FIRST_MATRIX']['PRICE'] - $arItem['FIRST_MATRIX']['UNROUND_DISCOUNT_PRICE'],2);
    $isDiscountItem = false;
    if ($discountValue > 0 && $arItem['FIRST_MATRIX']['PRICE'] > 0) {
        $isDiscountItem = true;
        $discountPercent = round(($arItem['FIRST_MATRIX']['PRICE'] - $arItem['FIRST_MATRIX']['UNROUND_DISCOUNT_PRICE'])/$arItem['FIRST_MATRIX']['PRICE']*100);
    }

    $setBootstrapCol = true;
    if (isset($arResult['SET_BOOTSTRAP_COL']) and $arResult['SET_BOOTSTRAP_COL'] == 'N') {
        $setBootstrapCol = false;
    }

    $weight = $arItem['PRODUCT']['WEIGHT'] > 0 ? $arItem['PRODUCT']['WEIGHT'] : 0;
    $isWeighted = ((int)$arItem['PROPERTIES']['WEIGHT_UNIT']['VALUE']) ? true : false;
    if ($isWeighted) {
        $arParams['SHOW_MEASURE'] = 'Y';
        $arParams['SHOW_MEASURE_WITH_RATIO'] = 'Y';
    } else {
        $arParams['SHOW_MEASURE'] = 'N';
        $arParams['SHOW_MEASURE_WITH_RATIO'] = 'N';
    }
    ?>

    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

    $arItem["strMainID"] = $this->GetEditAreaId($arItem['ID']);
    $arItemIDs = CNext::GetItemsIDs($arItem);

    $totalCount = CNext::GetTotalCount($arItem, $arParams);

    $arQuantityData = CNext::GetQuantityArray($totalCount, $arItemIDs["ALL_ITEM_IDS"]);

    $bLinkedItems = false;//показываем кнопку "купить"

    $elementName = ((isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME']);

    $item_id = $arItem["ID"];
    $arAddToBasketData = array();

    $strMeasure = $arItem['ITEM_MEASURE']['TITLE'] ?? '';
    CIBlockPriceTools::setRatioMinPrice($arItem, false);

    $arAddToBasketData = CNext::GetAddToBasketArray($arItem, $totalCount, $defaultCount, $basketUrl, $bLinkedItems);
    ?>
    <div class="default_product_card <?= (($_GET['q'])) ? 's' : '' ?> <?= $setBootstrapCol ? 'col-5 col-sm-4 col-xs-6 col-lg-2' : 'col-xs-12'; ?>"
         id="<?= $arItemIDs["strMainID"]; ?>">
        <div class="stickers">
            <div class="stickers_out_srok">
                <? if ($arItem['PROPERTIES']['NEW']['VALUE'] == 'Y'):?>
                    <div class="sticker_new_product sticker_item"></div>
                <? endif; ?>
                <? if ($arItem['PROPERTIES']['BESTSELLER']['VALUE'] == 1 || $arItem['PROPERTIES']['BESTSELLER']['VALUE'] == 2):?>
                    <div class="sticker_bestseller sticker_item"></div>
                <? endif; ?>
            </div>
            <? if (\Helpers\CustomTools::isValidDateExpiration($arItem['PROPERTIES']['DATE_EXPIRATION']['VALUE'])):?>
                <div class="sticker_date_expiration sticker_srok">годен до: <?=$arItem['PROPERTIES']['DATE_EXPIRATION']['VALUE']?></div>
            <? endif; ?>
        </div>
        <div class="like_icons">
            <div class="wish_item_button">
                    <span title="В избранное"
                          class="wish_item to"
                          data-item="<?= $arItem["ID"] ?>"
                          data-iblock="<?= $arItem["IBLOCK_ID"] ?>"><i></i></span>
            </div>
        </div>
        <? if ($_COOKIE['age_restriction'] != 'yes'):  ?>
            <? if (strpos($elementName, 'энергетический') !== false || strpos($elementName, 'Энергетический') !== false):  ?>
                <div class="age_restriction_btn_off" style="left: 12%;top: 130px; position: absolute; z-index: 3;">
                    <span style="color: #0A3A68; font-size: 13px;font-weight: 700;">только для лиц старше 18 лет</span>
                </div>
                <div class="default_product_image_wrapper_block image_blure_custom " style="filter: blur(10px);">
            <? else: ?>
                <div class="default_product_image_wrapper_block">
            <? endif; ?>
        <? else: ?>
            <div class="default_product_image_wrapper_block">
        <? endif; ?>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="thumb shine default_product_card_image"
               id="<? echo $arItemIDs["ALL_ITEM_IDS"]['PICT']; ?>">
                <?
                $a_alt = ($arItem["PREVIEW_PICTURE"] && strlen($arItem["PREVIEW_PICTURE"]['DESCRIPTION']) ? $arItem["PREVIEW_PICTURE"]['DESCRIPTION'] : ($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"] ? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"] : $arItem["NAME"]));
                $a_title = ($arItem["PREVIEW_PICTURE"] && strlen($arItem["PREVIEW_PICTURE"]['DESCRIPTION']) ? $arItem["PREVIEW_PICTURE"]['DESCRIPTION'] : ($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"] ? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"] : $arItem["NAME"]));
                ?>


                <?
                $imgId = $arItem["~PREVIEW_PICTURE"] ? $arItem["~PREVIEW_PICTURE"] : $arItem["~DETAIL_PICTURE"];
                if ($imgId) {
                    $imgSrc = \Helpers\CustomTools::getResizedPictSrc($imgId, ['width' => 350, 'height' => 220]);
                } else {
                    $imgSrc = SITE_TEMPLATE_PATH . '/images/no_photo_medium.png';
                }
                $imgWebpPath = \Helpers\CustomTools::getWebpImgSrc($imgSrc);
                ?>

                <picture>
                    <? if ($imgWebpPath): ?>
                        <source
                                type="image/webp"
                            <? if ($SHOW_MODE === 'SWIPER_LAZY_LOAD'):?>
                                data-srcset="<?= $imgWebpPath ?>" class="swiper-lazy"
                            <? else: ?>
                                srcset="<?= $imgWebpPath ?>"
                            <? endif; ?>
                        >
                    <? endif; ?>
                    <source
                            type="image/jpeg"
                        <? if ($SHOW_MODE === 'SWIPER_LAZY_LOAD'):?>
                            data-srcset="<?= $imgSrc ?>" class="swiper-lazy"
                        <? else: ?>
                            srcset="<?= $imgSrc ?>"
                        <? endif; ?>
                    >
                    <img
                        <? if ($SHOW_MODE === 'SWIPER_LAZY_LOAD'):?>
                            data-src="<?= $imgSrc ?>" class="swiper-lazy"
                            src=""
                        <? else: ?>
                            src="<?= $imgSrc ?>"
                        <? endif; ?>
                        alt="<?= $a_alt; ?>"
                        title="<?= $a_title; ?>"
                    />
                </picture>
            </a>
            <? if ($SHOW_MODE === 'SWIPER_LAZY_LOAD'):?>
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="swiper-lazy-preloader"></a>
            <? endif; ?>
        </div>

        <? if($isDiscountItem && $arItem['PROPERTIES']['MINIMAL_PRICE']['VALUE'] != 1): ?>
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="catalog_percent_discount">
                <div class="catalog_percent_discount_text">
                        <span>
                            -<?=$discountPercent; ?>%
                        </span>
                </div>
            </a>
        <? endif; ?>
        <?if($arItem['PROPERTIES']['MINIMAL_PRICE']['VALUE'] == 1):?>
            <div class="sticker_minimal_price minimal_price_margin_main"></div>
        <? endif; ?>

        <div class="weight_in_catalog_product"
             style="<?= !$isWeighted ? 'visibility: hidden;' : ''; ?>"><?= \Helpers\CustomTools::weightFormat($arAddToBasketData["MIN_QUANTITY_BUY"]); ?></div>

        <div class="catalog_item_left_bottom">
            <? if($arItem['PROPERTIES'][\Helpers\Constants::PROP_FAST_DELIVERY]['VALUE'] === \Helpers\Constants::PROP_FAST_DELIVERY_VALUE_EXPRESS): ?>
                <img class="fast_delivery_icon_small" src="/images/icons/fast_delivery_icon_black.svg">
            <? endif; ?>
        </div>

        <div class="item_info <?= $arParams["TYPE_SKU"] ?>">
            <? if ($_COOKIE['age_restriction'] != 'yes'):  ?>
                <? if (strpos($elementName, 'энергетический') !== false || strpos($elementName, 'Энергетический') !== false):  ?>
                    <div class="item-title text_blure_custom" style="filter: blur(5px);">
                <? else: ?>
                    <div class="item-title">
                <? endif; ?>
            <? else: ?>
            <div class="item-title">
            <? endif; ?>
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                   class="dark_link"><span class="default_product_name"><?= $elementName; ?></span></a>
            </div>
        </div>

        <div class="product_card_bottom_info">
            <div class="sa_block">
                <?= $arQuantityData["HTML"]; ?>
            </div>
            <div class="sa_block">
                <? $APPLICATION->IncludeComponent(
                    "custom:empty_component",
                    "rating_product",
                    [
                        'RATING' => $arItem['PROPERTIES']['COMMENTS_AVERAGE_SCORE']['VALUE'],
                        'COMMENTS_CNT' => $arItem['PROPERTIES']['COMMENTS_COUNT']['VALUE']
                    ],
                    false
                );
                ?>
            </div>
            <div class="cost prices clearfix">
                <?
                $item_id = $arItem["ID"];
                if (isset($arItem['PRICE_MATRIX']) && $arItem['PRICE_MATRIX']) // USE_PRICE_COUNT
                {
                    ?>

                    <div class="price_matrix_wrapper default_product_card_price_block">
                        <div class="price" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$arItem['FIRST_MATRIX']['UNROUND_DISCOUNT_PRICE']; ?>">
                            <span class="values_wrapper">
                                <span class="price_value"><?=$arItem['FIRST_MATRIX']['PRINT_DISCOUNT_PRICE']; ?></span><? if($arParams['SHOW_MEASURE'] == 'Y'): ?><span class="price_measure">/<?=$strMeasure; ?></span><? endif; ?>
                            </span>
                        </div>
                        <? if($discountValue > 0): ?>
                            <div class="price discount" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$arItem['FIRST_MATRIX']['PRICE']; ?>">
                                <span class="values_wrapper"><span class="price_value"><?=$arItem['FIRST_MATRIX']['PRINT_PRICE']; ?></span></span>
                            </div>
                        <? endif; ?>
                            <div class="sale_block" style="<?= ($discountValue > 0)?'':'visibility: hidden;' ; ?>">
                                <span class="title">Экономия</span>
                                <div class="text">
                                    <span class="values_wrapper" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$discountValue; ?>">
                                        <span class="price_value"><?=CCurrencyLang::CurrencyFormat($discountValue,$arItem['FIRST_MATRIX']['CURRENCY']); ?></span>
                                    </span>
                                </div>
                            </div>
                    </div>

                <? } ?>
            </div>
            <? if ($_COOKIE['age_restriction'] != 'yes'):  ?>
                <? if (strpos($elementName, 'энергетический') !== false || strpos($elementName, 'Энергетический') !== false):  ?>
                <div class="age_restriction_btn_off inner_content " style="">
                    <div class="counter_wrapp">
                        <div class="button_block btn btn-default transition_bg" onclick="setCookieAgeRestriction16Pluse('age_restriction', 'yes', 1)">
                            <span>
                                <span>Подтвердить возраст</span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="age_restriction_btn_on inner_content js_offers__<?=$arItem['ID'];?>" style="display:none;">
                <? else: ?>
                <div class="inner_content js_offers__<?= $arItem['ID']; ?>">
                <? endif; ?>
            <? else: ?>
            <div class="inner_content js_offers__<?= $arItem['ID']; ?>">
            <? endif; ?>
                <div class="counter_wrapp">
                    <? if (!$isInCart && ($arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_LIST"] && $arAddToBasketData["ACTION"] == "ADD") && $arAddToBasketData["CAN_BUY"]): ?>
                        <div class="counter_block"
                             data-item="<?= $arItem["ID"]; ?>">
                                        <span class="minus"
                                              id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY_DOWN']; ?>">-</span>
                            <input type="text"
                                   class="text <?= $isWeighted ? 'catalog_input_quantity_weighted' : ''; ?>"
                                   id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>"
                                   name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
                                   value="<?= $arAddToBasketData["MIN_QUANTITY_BUY"] ?>"/>
                            <? if ($isWeighted): ?>
                                <label class="catalog_input_quantity_measure_info"
                                       for="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>"><?= $strMeasure; ?></label>
                            <? endif; ?>
                            <span class="plus"
                                  id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY_UP']; ?>" <?= ($arAddToBasketData["MAX_QUANTITY_BUY"] ? "data-max='" . $arAddToBasketData["MAX_QUANTITY_BUY"] . "'" : "") ?>>+</span>

                        </div>
                        <script>
                            function addDahsaMailProductAddToBasket (){
                                dashamail("cart.addProduct", {
                                    "productId": "<?=$arItem["ID"]?>",
                                    "quantity": document.getElementById('<?=$arItemIDs["ALL_ITEM_IDS"]["QUANTITY"]?>').value,
                                    "price": "<?=$arItem['FIRST_MATRIX']['UNROUND_DISCOUNT_PRICE']?>"
                                });
                            }
                        </script>
                        <div id="<?= $arItemIDs["ALL_ITEM_IDS"]['BASKET_ACTIONS']; ?>" onclick="addDahsaMailProductAddToBasket(); this.onclick = null;"
                             class="button_block <?= (($arAddToBasketData["ACTION"] == "ORDER"/*&& !$arItem["CAN_BUY"]*/) || !$arAddToBasketData["CAN_BUY"] || !$arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_LIST"] || $arAddToBasketData["ACTION"] == "SUBSCRIBE" ? "wide" : ""); ?>">
                            <?= $arAddToBasketData["HTML"] ?>
                        </div>

                    <? elseif ($isInCart): ?>
                        <a rel="nofollow" href="<?= $arParams['~BASKET_URL']; ?>"
                           class="small in-cart btn btn-default transition_bg"
                           data-item="<?= $arItem['ID']; ?>">
                            <i></i><span>Добавлено</span>
                        </a>
                    <? endif; ?>

                </div>
            </div>
        </div>
    </div>


    <?

    $jsParams = array(
        'PRODUCT_TYPE' => $item['PRODUCT']['TYPE'],
        'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
        'SHOW_ADD_BASKET_BTN' => false,
        'SHOW_BUY_BTN' => true,
        'SHOW_ABSENT' => true,
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
        'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
        'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
        'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
        'BIG_DATA' => $item['BIG_DATA'],
        'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
        'VIEW_MODE' => $arResult['TYPE'],
        'USE_SUBSCRIBE' => $showSubscribe,
        'PRODUCT' => array(
            'ID' => $item['ID'],
            'NAME' => $productTitle,
            'DETAIL_PAGE_URL' => $item['DETAIL_PAGE_URL'],
            'PICT' => $item['SECOND_PICT'] ? $item['PREVIEW_PICTURE_SECOND'] : $item['PREVIEW_PICTURE'],
            'CAN_BUY' => $item['CAN_BUY'],
            'CHECK_QUANTITY' => $item['CHECK_QUANTITY'],
            'MAX_QUANTITY' => $item['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
            'QUANTITY_FLOAT' => is_float($item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
            'ITEM_PRICE_MODE' => $item['ITEM_PRICE_MODE'],
            'ITEM_PRICES' => $item['ITEM_PRICES'],
            'ITEM_PRICE_SELECTED' => $item['ITEM_PRICE_SELECTED'],
            'ITEM_QUANTITY_RANGES' => $item['ITEM_QUANTITY_RANGES'],
            'ITEM_QUANTITY_RANGE_SELECTED' => $item['ITEM_QUANTITY_RANGE_SELECTED'],
            'ITEM_MEASURE_RATIOS' => $item['ITEM_MEASURE_RATIOS'],
            'ITEM_MEASURE_RATIO_SELECTED' => $item['ITEM_MEASURE_RATIO_SELECTED'],
            'MORE_PHOTO' => $item['MORE_PHOTO'],
            'MORE_PHOTO_COUNT' => $item['MORE_PHOTO_COUNT']
        ),
        'BASKET' => array(
            'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => empty($item['PRODUCT_PROPERTIES']),
            'BASKET_URL' => $arParams['~BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arParams['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arParams['~BUY_URL_TEMPLATE']
        ),
        'VISUAL' => array(
            'ID' => $itemIds['ID'],
            'PICT_ID' => $item['SECOND_PICT'] ? $itemIds['SECOND_PICT'] : $itemIds['PICT'],
            'PICT_SLIDER_ID' => $itemIds['PICT_SLIDER'],
            'QUANTITY_ID' => $itemIds['QUANTITY'],
            'QUANTITY_UP_ID' => $itemIds['QUANTITY_UP'],
            'QUANTITY_DOWN_ID' => $itemIds['QUANTITY_DOWN'],
            'PRICE_ID' => $itemIds['PRICE'],
            'PRICE_OLD_ID' => $itemIds['PRICE_OLD'],
            'PRICE_TOTAL_ID' => $itemIds['PRICE_TOTAL'],
            'BUY_ID' => $itemIds['BUY_LINK'],
            'BASKET_PROP_DIV' => $itemIds['BASKET_PROP_DIV'],
            'BASKET_ACTIONS_ID' => $itemIds['BASKET_ACTIONS'],
            'NOT_AVAILABLE_MESS' => $itemIds['NOT_AVAILABLE_MESS'],
            'COMPARE_LINK_ID' => $itemIds['COMPARE_LINK'],
            'SUBSCRIBE_ID' => $itemIds['SUBSCRIBE_LINK']
        )
    );

    $jsParams['PRODUCT_DISPLAY_MODE'] = $arParams['PRODUCT_DISPLAY_MODE'];
    $jsParams['USE_ENHANCED_ECOMMERCE'] = $arParams['USE_ENHANCED_ECOMMERCE'];
    $jsParams['DATA_LAYER_NAME'] = $arParams['DATA_LAYER_NAME'];
    $jsParams['BRAND_PROPERTY'] = !empty($item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
        ? $item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
        : null;

    ?>
    <?

    unset($item, $arItem, $minOffer, $itemIds, $jsParams);
}