<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Loader;
use \Custom\FavoritesOnCookies\CurrentUserFavorites;
use \Helpers\CurrentCart;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$SHOW_MODE = $arParams['SHOW_MODE']??'';

$this->setFrameMode(true);

if (isset($arResult['ITEM']) && Loader::includeModule('custom.favoritesoncookies')) {
    //Поиск id товаров и значений к ним
    $products = \Bitrix\Sale\Internals\DiscountTable::getList(
        ['filter'=>['XML_ID'=>'PROGRESS_BAR%', 'ACTIVE'=>'Y'],
            'select'=>['ACTIONS_LIST','CONDITIONS_LIST']]
    );
    while($item = $products->fetch()) {
        foreach ($item['CONDITIONS_LIST']['CHILDREN'] as $children) {
            if ($children['CLASS_ID'] == 'CondBsktAmtGroup') {
                $val = $children['DATA']['Value'];
            }
        }
        foreach ($item['ACTIONS_LIST']['CHILDREN'] as $childrenAct) {
            if ($childrenAct['CLASS_ID'] == 'ActSaleBsktGrp') {
                foreach ($childrenAct['CHILDREN'] as $isProdact) {
                    if ($isProdact['CLASS_ID'] == 'CondBsktFldProduct'){
                        $arrId[$isProdact['DATA']['value']] = $val;
                    }
                }
            }
            break;
        }
    }
    //закончен

    $arParams["SHOW_OLD_PRICE"] = 'Y';
    $arItem = $arResult['ITEM'];
    $defaultCount = 1;
    $basketUrl = SITE_DIR . "basket/";
    $isInCart = CurrentCart::getInstance()->isProductInCart($arItem['ID']);

    $discountValue = round($arItem['FIRST_MATRIX']['PRICE'] - $arItem['FIRST_MATRIX']['UNROUND_DISCOUNT_PRICE'],2);
    $isDiscountItem = false;
    if ($discountValue > 0 && $arItem['FIRST_MATRIX']['PRICE'] > 0) {
        $isDiscountItem = true;
        $discountPercent = round(($arItem['FIRST_MATRIX']['PRICE'] - $arItem['FIRST_MATRIX']['UNROUND_DISCOUNT_PRICE'])/$arItem['FIRST_MATRIX']['PRICE']*100);
    }

    $setBootstrapCol = true;
    if (isset($arResult['SET_BOOTSTRAP_COL']) and $arResult['SET_BOOTSTRAP_COL'] == 'N') {
        $setBootstrapCol = false;
    }

    $weight = $arItem['PRODUCT']['WEIGHT'] > 0 ? $arItem['PRODUCT']['WEIGHT'] : 0;
    $isWeighted = ((int)$arItem['PROPERTIES']['WEIGHT_UNIT']['VALUE'] && $weight > 0) ? true : false;
    if ($isWeighted) {
        $arParams['SHOW_MEASURE'] = 'Y';
        $arParams['SHOW_MEASURE_WITH_RATIO'] = 'Y';
    } else {
        $arParams['SHOW_MEASURE'] = 'N';
        $arParams['SHOW_MEASURE_WITH_RATIO'] = 'N';
    }
    ?>

    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));

    $arItem["strMainID"] = $this->GetEditAreaId($arItem['ID']);
    $arItemIDs = CNext::GetItemsIDs($arItem);

    $totalCount = CNext::GetTotalCount($arItem, $arParams);

    $arQuantityData = CNext::GetQuantityArray($totalCount, $arItemIDs["ALL_ITEM_IDS"]);

    $bLinkedItems = false;//показываем кнопку "купить"

    $elementName = ((isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME']);

    $item_id = $arItem["ID"];
    $arAddToBasketData = array();

    $strMeasure = $arItem['ITEM_MEASURE']['TITLE'] ?? '';
    CIBlockPriceTools::setRatioMinPrice($arItem, false);

    $arAddToBasketData = CNext::GetAddToBasketArray($arItem, $totalCount, $defaultCount, $basketUrl, $bLinkedItems);
    ?>
    <tr class="basket-items-gift-item-container basket-items-gift-list-item-descriptions column-xs" id="<?= $arItemIDs["strMainID"]; ?>" data-id="<?=$item_id?>" data-value="<?=$arrId[$item_id]?>">
        <td class="basket-item-gift-block-image">
            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="basket-item-image-link">
                <?
                $a_alt = ($arItem["PREVIEW_PICTURE"] && strlen($arItem["PREVIEW_PICTURE"]['DESCRIPTION']) ? $arItem["PREVIEW_PICTURE"]['DESCRIPTION'] : ($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"] ? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_ALT"] : $arItem["NAME"]));
                $a_title = ($arItem["PREVIEW_PICTURE"] && strlen($arItem["PREVIEW_PICTURE"]['DESCRIPTION']) ? $arItem["PREVIEW_PICTURE"]['DESCRIPTION'] : ($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"] ? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"] : $arItem["NAME"]));
                ?>


                <?
                $imgId = $arItem["~PREVIEW_PICTURE"] ? $arItem["~PREVIEW_PICTURE"] : $arItem["~DETAIL_PICTURE"];
                if ($imgId) {
                    $imgSrc = \Helpers\CustomTools::getResizedPictSrc($imgId, ['width' => 133, 'height' => 133]);
                } else {
                    $imgSrc = SITE_TEMPLATE_PATH . '/images/no_photo_medium.png';
                }
                ?>

                <img class="gift-item-img" src="<?= $imgSrc ?>" alt="<?= $a_alt; ?>" title="<?= $a_title; ?>"/>
            </a>
        </td>
        <td class="basket-item-gift-block-info">
            <span class="sum-to-gift">
                Добавьте в корзину товаров на <?=$arrId[$item_id]?> рублей, чтобы получить этот подарок!
            </span>
            <h2 class="basket-item-gift-info-name">
                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"
                   class="dark_link"><span class="default-product-name"><?= $elementName; ?></span></a>
            </h2>
            <div class="row-item-gift-block-info">
                <div class="price">
                    <?
                    $item_id = $arItem["ID"];
                    if (isset($arItem['PRICE_MATRIX']) && $arItem['PRICE_MATRIX']) // USE_PRICE_COUNT
                    {
                        ?>

                        <div class="price_matrix_wrapper default_product_card_price_block">
                            <div class="price" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$arItem['FIRST_MATRIX']['UNROUND_DISCOUNT_PRICE']; ?>">
                            <span class="gift-item-price" style="display:flex;flex-direction:column;">
                                <span class="gift-item-price-current-text">1 руб.</span>
                                <span class="price_measure" style="text-decoration:line-through;font-size: 13px;"><?=$arItem['FIRST_MATRIX']['PRINT_DISCOUNT_PRICE']; ?></span>
                            </span>
                            </div>
                            <? if($discountValue > 0): ?>
                                <div class="price discount" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$arItem['FIRST_MATRIX']['PRICE']; ?>">
                                    <span class="values_wrapper"><span class="price_value"><?=$arItem['FIRST_MATRIX']['PRINT_PRICE']; ?></span></span>
                                </div>
                            <? endif; ?>
                            <div class="sale_block" style="<?= ($discountValue > 0)?'':'visibility: hidden;' ; ?>">
                                <span class="title">Экономия</span>
                                <div class="text">
                                    <span class="values_wrapper" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$discountValue; ?>">
                                        <span class="price_value"><?=CCurrencyLang::CurrencyFormat($discountValue,$arItem['FIRST_MATRIX']['CURRENCY']); ?></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                    <? } ?>
                </div>
                <div class="inner_content js_offers__<?= $arItem['ID']; ?> add-in-info">
                    <div class="counter_wrapp">
                        <? if (!$isInCart && ($arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_LIST"] && $arAddToBasketData["ACTION"] == "ADD") && $arAddToBasketData["CAN_BUY"]): ?>
                            <div class="counter_block"
                                 data-item="<?= $arItem["ID"]; ?>" style="display:none;">
                                        <span class="minus"
                                              id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY_DOWN']; ?>">-</span>
                                <input type="text"
                                       class="text <?= $isWeighted ? 'catalog_input_quantity_weighted' : ''; ?>"
                                       id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>"
                                       name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
                                       value="<?= $arAddToBasketData["MIN_QUANTITY_BUY"] ?>"/>
                                <? if ($isWeighted): ?>
                                    <label class="catalog_input_quantity_measure_info"
                                           for="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>"><?= $strMeasure; ?></label>
                                <? endif; ?>
                                <span class="plus"
                                      id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY_UP']; ?>" <?= ($arAddToBasketData["MAX_QUANTITY_BUY"] ? "data-max='" . $arAddToBasketData["MAX_QUANTITY_BUY"] . "'" : "") ?>>+</span>

                            </div>

                            <div id="<?= $arItemIDs["ALL_ITEM_IDS"]['BASKET_ACTIONS']; ?>"
                                 class="gift-add button_block <?= (($arAddToBasketData["ACTION"] == "ORDER"/*&& !$arItem["CAN_BUY"]*/) || !$arAddToBasketData["CAN_BUY"] || !$arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_LIST"] || $arAddToBasketData["ACTION"] == "SUBSCRIBE" ? "wide" : ""); ?>">
                                <?= $arAddToBasketData["HTML"] ?>
                            </div>

                        <? elseif ($isInCart): ?>
                            <a rel="nofollow" href="<?= $arParams['~BASKET_URL']; ?>"
                               class="small in-cart btn btn-default transition_bg gift-added"
                               data-item="<?= $arItem['ID']; ?>">
                                <i></i><span>Добавлено</span>
                            </a>
                        <? endif; ?>

                    </div>
                </div>
            </div>
        </td>
        <td class="basket-items-gift-list-item-price basket-items-gift-list-item-price-for-one hidden-xs">
            <div class="price">
                <?
                $item_id = $arItem["ID"];
                if (isset($arItem['PRICE_MATRIX']) && $arItem['PRICE_MATRIX']) // USE_PRICE_COUNT
                {
                    ?>

                    <div class="price_matrix_wrapper default_product_card_price_block">
                        <div class="price" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$arItem['FIRST_MATRIX']['UNROUND_DISCOUNT_PRICE']; ?>">
                        <span class="gift-item-price" style="display:flex;flex-direction:column;">
                            <span class="gift-item-price-current-text">1 руб.</span>
                            <span class="price_measure" style="text-decoration:line-through;font-size: 13px;"><?=$arItem['FIRST_MATRIX']['PRINT_DISCOUNT_PRICE']; ?></span>
                        </span>
                        </div>
                        <? if($discountValue > 0): ?>
                            <div class="price discount" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$arItem['FIRST_MATRIX']['PRICE']; ?>">
                                <span class="values_wrapper"><span class="price_value"><?=$arItem['FIRST_MATRIX']['PRINT_PRICE']; ?></span></span>
                            </div>
                        <? endif; ?>
                        <div class="sale_block" style="<?= ($discountValue > 0)?'':'visibility: hidden;' ; ?>">
                            <span class="title">Экономия</span>
                            <div class="text">
                                <span class="values_wrapper" data-currency="<?=$arItem['FIRST_MATRIX']['CURRENCY']; ?>" data-value="<?=$discountValue; ?>">
                                    <span class="price_value"><?=CCurrencyLang::CurrencyFormat($discountValue,$arItem['FIRST_MATRIX']['CURRENCY']); ?></span>
                                </span>
                            </div>
                        </div>
                    </div>

                <? } ?>
            </div>
        </td>
        <td class="product-card-add-info basket">
            <div class="inner_content js_offers__<?= $arItem['ID']; ?>">
                <div class="counter_wrapp">
                    <? if (!$isInCart && ($arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_LIST"] && $arAddToBasketData["ACTION"] == "ADD") && $arAddToBasketData["CAN_BUY"]): ?>
                        <div class="counter_block"
                             data-item="<?= $arItem["ID"]; ?>" style="display:none;">
                                    <span class="minus"
                                          id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY_DOWN']; ?>">-</span>
                            <input type="text"
                                   class="text <?= $isWeighted ? 'catalog_input_quantity_weighted' : ''; ?>"
                                   id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>"
                                   name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>"
                                   value="<?= $arAddToBasketData["MIN_QUANTITY_BUY"] ?>"/>
                            <? if ($isWeighted): ?>
                                <label class="catalog_input_quantity_measure_info"
                                       for="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY']; ?>"><?= $strMeasure; ?></label>
                            <? endif; ?>
                            <span class="plus"
                                  id="<? echo $arItemIDs["ALL_ITEM_IDS"]['QUANTITY_UP']; ?>" <?= ($arAddToBasketData["MAX_QUANTITY_BUY"] ? "data-max='" . $arAddToBasketData["MAX_QUANTITY_BUY"] . "'" : "") ?>>+</span>

                        </div>

                        <div id="<?= $arItemIDs["ALL_ITEM_IDS"]['BASKET_ACTIONS']; ?>"
                             class="gift-add button_block <?= (($arAddToBasketData["ACTION"] == "ORDER"/*&& !$arItem["CAN_BUY"]*/) || !$arAddToBasketData["CAN_BUY"] || !$arAddToBasketData["OPTIONS"]["USE_PRODUCT_QUANTITY_LIST"] || $arAddToBasketData["ACTION"] == "SUBSCRIBE" ? "wide" : ""); ?>">
                            <?= $arAddToBasketData["HTML"] ?>
                        </div>

                    <? elseif ($isInCart): ?>
                        <a rel="nofollow" href="<?= $arParams['~BASKET_URL']; ?>"
                           class="small in-cart btn btn-default transition_bg gift-added"
                           data-item="<?= $arItem['ID']; ?>">
                            <i></i><span>Добавлено</span>
                        </a>
                    <? endif; ?>

                </div>
            </div>
        </td>
    </tr>


    <?

    $jsParams = array(
        'PRODUCT_TYPE' => $item['PRODUCT']['TYPE'],
        'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
        'SHOW_ADD_BASKET_BTN' => false,
        'SHOW_BUY_BTN' => true,
        'SHOW_ABSENT' => true,
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
        'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
        'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
        'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
        'BIG_DATA' => $item['BIG_DATA'],
        'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
        'VIEW_MODE' => $arResult['TYPE'],
        'USE_SUBSCRIBE' => $showSubscribe,
        'PRODUCT' => array(
            'ID' => $item['ID'],
            'NAME' => $productTitle,
            'DETAIL_PAGE_URL' => $item['DETAIL_PAGE_URL'],
            'PICT' => $item['SECOND_PICT'] ? $item['PREVIEW_PICTURE_SECOND'] : $item['PREVIEW_PICTURE'],
            'CAN_BUY' => $item['CAN_BUY'],
            'CHECK_QUANTITY' => $item['CHECK_QUANTITY'],
            'MAX_QUANTITY' => $item['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
            'QUANTITY_FLOAT' => is_float($item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
            'ITEM_PRICE_MODE' => $item['ITEM_PRICE_MODE'],
            'ITEM_PRICES' => $item['ITEM_PRICES'],
            'ITEM_PRICE_SELECTED' => $item['ITEM_PRICE_SELECTED'],
            'ITEM_QUANTITY_RANGES' => $item['ITEM_QUANTITY_RANGES'],
            'ITEM_QUANTITY_RANGE_SELECTED' => $item['ITEM_QUANTITY_RANGE_SELECTED'],
            'ITEM_MEASURE_RATIOS' => $item['ITEM_MEASURE_RATIOS'],
            'ITEM_MEASURE_RATIO_SELECTED' => $item['ITEM_MEASURE_RATIO_SELECTED'],
            'MORE_PHOTO' => $item['MORE_PHOTO'],
            'MORE_PHOTO_COUNT' => $item['MORE_PHOTO_COUNT']
        ),
        'BASKET' => array(
            'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => empty($item['PRODUCT_PROPERTIES']),
            'BASKET_URL' => $arParams['~BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arParams['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arParams['~BUY_URL_TEMPLATE']
        ),
        'VISUAL' => array(
            'ID' => $itemIds['ID'],
            'PICT_ID' => $item['SECOND_PICT'] ? $itemIds['SECOND_PICT'] : $itemIds['PICT'],
            'PICT_SLIDER_ID' => $itemIds['PICT_SLIDER'],
            'QUANTITY_ID' => $itemIds['QUANTITY'],
            'QUANTITY_UP_ID' => $itemIds['QUANTITY_UP'],
            'QUANTITY_DOWN_ID' => $itemIds['QUANTITY_DOWN'],
            'PRICE_ID' => $itemIds['PRICE'],
            'PRICE_OLD_ID' => $itemIds['PRICE_OLD'],
            'PRICE_TOTAL_ID' => $itemIds['PRICE_TOTAL'],
            'BUY_ID' => $itemIds['BUY_LINK'],
            'BASKET_PROP_DIV' => $itemIds['BASKET_PROP_DIV'],
            'BASKET_ACTIONS_ID' => $itemIds['BASKET_ACTIONS'],
            'NOT_AVAILABLE_MESS' => $itemIds['NOT_AVAILABLE_MESS'],
            'COMPARE_LINK_ID' => $itemIds['COMPARE_LINK'],
            'SUBSCRIBE_ID' => $itemIds['SUBSCRIBE_LINK']
        )
    );

    $jsParams['PRODUCT_DISPLAY_MODE'] = $arParams['PRODUCT_DISPLAY_MODE'];
    $jsParams['USE_ENHANCED_ECOMMERCE'] = $arParams['USE_ENHANCED_ECOMMERCE'];
    $jsParams['DATA_LAYER_NAME'] = $arParams['DATA_LAYER_NAME'];
    $jsParams['BRAND_PROPERTY'] = !empty($item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
        ? $item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
        : null;

    ?>
    <?

    unset($item, $arItem, $minOffer, $itemIds, $jsParams);
}