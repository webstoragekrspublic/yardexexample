function imageTabCatalogItem (){
    if (document.querySelector('.basket-item-block-image')) {
        var imageWidth = 0;
        var elem = document.querySelector('.basket-item-block-image');
        imageWidth = elem.offsetWidth;
        document.querySelectorAll('.basket-item-gift-block-image').forEach(elem => {
            elem.setAttribute('style', 'max-width:' + imageWidth + 'px;');
        });
    }
}

function addStyle(){
    imageTabCatalogItem();
}

function refreshBasket(){
    BX.Sale.BasketComponent.sendRequest('refreshAjax', {fullRecalculation: 'Y'});
}

function checkChange(){
    function callback(mutatuion, observer){
        refreshBasket();
    }
    let observer = new MutationObserver(callback);
    document.querySelectorAll('.counter_wrapp .in-cart').forEach(elem => {
        observer.observe(elem, {
            childList: true,
            subtree: true,
            attributes: true
        });
    })
}

document.addEventListener("DOMContentLoaded", checkChange);
window.addEventListener('load', addStyle)
window.addEventListener("resize",addStyle);