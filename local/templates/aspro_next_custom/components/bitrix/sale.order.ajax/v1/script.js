BX.saleOrderAjax = { // bad solution, actually, a singleton at the page

    BXCallAllowed: false,

    options: {},
    indexCache: {},
    controls: {},

    modes: {},
    properties: {},

    // called once, on component load
    init: function (options) {
        var ctx = this;
        this.options = options;

        window.submitFormProxy = BX.proxy(function () {
            ctx.submitFormProxy.apply(ctx, arguments);
        }, this);

        BX(function () {
            ctx.initDeferredControl();
        });
        BX(function () {
            ctx.BXCallAllowed = true; // unlock form refresher
        });

        this.controls.scope = BX('bx-soa-order');

        // user presses "add location" when he cannot find location in popup mode
        BX.bindDelegate(this.controls.scope, 'click', {className: '-bx-popup-set-mode-add-loc'}, function () {

            var input = BX.create('input', {
                attrs: {
                    type: 'hidden',
                    name: 'PERMANENT_MODE_STEPS',
                    value: '1'
                }
            });

            BX.prepend(input, BX('bx-soa-order'));

            ctx.BXCallAllowed = false;
            BX.Sale.OrderAjaxComponent.sendRequest();
        });

    },

    cleanUp: function () {

        for (var k in this.properties) {
            if (this.properties.hasOwnProperty(k)) {
                if (typeof this.properties[k].input != 'undefined') {
                    BX.unbindAll(this.properties[k].input);
                    this.properties[k].input = null;
                }

                if (typeof this.properties[k].control != 'undefined')
                    BX.unbindAll(this.properties[k].control);
            }
        }

        this.properties = {};
    },

    addPropertyDesc: function (desc) {
        this.properties[desc.id] = desc.attributes;
        this.properties[desc.id].id = desc.id;
    },

    // called each time form refreshes
    initDeferredControl: function () {
        var ctx = this,
            k,
            row,
            input,
            locPropId,
            m,
            control,
            code,
            townInputFlag,
            adapter;

        // first, init all controls
        if (typeof window.BX.locationsDeferred != 'undefined') {

            this.BXCallAllowed = false;

            for (k in window.BX.locationsDeferred) {

                window.BX.locationsDeferred[k].call(this);
                window.BX.locationsDeferred[k] = null;
                delete (window.BX.locationsDeferred[k]);

                this.properties[k].control = window.BX.locationSelectors[k];
                delete (window.BX.locationSelectors[k]);
            }
        }

        for (k in this.properties) {

            // zip input handling
            if (this.properties[k].isZip) {
                row = this.controls.scope.querySelector('[data-property-id-row="' + k + '"]');
                if (BX.type.isElementNode(row)) {

                    input = row.querySelector('input[type="text"]');
                    if (BX.type.isElementNode(input)) {
                        this.properties[k].input = input;

                        // set value for the first "location" property met
                        locPropId = false;
                        for (m in this.properties) {
                            if (this.properties[m].type == 'LOCATION') {
                                locPropId = m;
                                break;
                            }
                        }

                        if (locPropId !== false) {
                            BX.bindDebouncedChange(input, function (value) {

                                var zipChangedNode = BX('ZIP_PROPERTY_CHANGED');
                                zipChangedNode && (zipChangedNode.value = 'Y');

                                input = null;
                                row = null;

                                if (BX.type.isNotEmptyString(value) && /^\s*\d+\s*$/.test(value) && value.length > 3) {

                                    ctx.getLocationsByZip(value, function (locationsData) {
                                        ctx.properties[locPropId].control.setValueByLocationIds(locationsData);
                                    }, function () {
                                        try {
                                            // ctx.properties[locPropId].control.clearSelected();
                                        } catch (e) {
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
            }

            // location handling, town property, etc...
            if (this.properties[k].type == 'LOCATION') {

                if (typeof this.properties[k].control != 'undefined') {

                    control = this.properties[k].control; // reference to sale.location.selector.*
                    code = control.getSysCode();

                    // we have town property (alternative location)
                    if (typeof this.properties[k].altLocationPropId != 'undefined') {
                        if (code == 'sls') // for sale.location.selector.search
                        {
                            // replace default boring "nothing found" label for popup with "-bx-popup-set-mode-add-loc" inside
                            control.replaceTemplate('nothing-found', this.options.messages.notFoundPrompt);
                        }

                        if (code == 'slst')  // for sale.location.selector.steps
                        {
                            (function (k, control) {

                                // control can have "select other location" option
                                control.setOption('pseudoValues', ['other']);

                                // insert "other location" option to popup
                                control.bindEvent('control-before-display-page', function (adapter) {

                                    control = null;

                                    var parentValue = adapter.getParentValue();

                                    // you can choose "other" location only if parentNode is not root and is selectable
                                    if (parentValue == this.getOption('rootNodeValue') || !this.checkCanSelectItem(parentValue))
                                        return;

                                    var controlInApater = adapter.getControl();

                                    if (typeof controlInApater.vars.cache.nodes['other'] == 'undefined') {
                                        controlInApater.fillCache([{
                                            CODE: 'other',
                                            DISPLAY: ctx.options.messages.otherLocation,
                                            IS_PARENT: false,
                                            VALUE: 'other'
                                        }], {
                                            modifyOrigin: true,
                                            modifyOriginPosition: 'prepend'
                                        });
                                    }
                                });

                                townInputFlag = BX('LOCATION_ALT_PROP_DISPLAY_MANUAL[' + parseInt(k) + ']');

                                control.bindEvent('after-select-real-value', function () {

                                    // some location chosen
                                    if (BX.type.isDomNode(townInputFlag))
                                        townInputFlag.value = '0';
                                });
                                control.bindEvent('after-select-pseudo-value', function () {

                                    // option "other location" chosen
                                    if (BX.type.isDomNode(townInputFlag))
                                        townInputFlag.value = '1';
                                });

                                // when user click at default location or call .setValueByLocation*()
                                control.bindEvent('before-set-value', function () {
                                    if (BX.type.isDomNode(townInputFlag))
                                        townInputFlag.value = '0';
                                });

                                // restore "other location" label on the last control
                                if (BX.type.isDomNode(townInputFlag) && townInputFlag.value == '1') {

                                    // a little hack: set "other location" text display
                                    adapter = control.getAdapterAtPosition(control.getStackSize() - 1);

                                    if (typeof adapter != 'undefined' && adapter !== null)
                                        adapter.setValuePair('other', ctx.options.messages.otherLocation);
                                }

                            })(k, control);
                        }
                    }
                }
            }
        }

        this.BXCallAllowed = true;

        //set location initialized flag and refresh region & property actual content
        if (BX.Sale.OrderAjaxComponent)
            BX.Sale.OrderAjaxComponent.locationsCompletion();
    },

    checkMode: function (propId, mode) {

        //if(typeof this.modes[propId] == 'undefined')
        //	this.modes[propId] = {};

        //if(typeof this.modes[propId] != 'undefined' && this.modes[propId][mode])
        //	return true;

        if (mode == 'altLocationChoosen') {

            if (this.checkAbility(propId, 'canHaveAltLocation')) {

                var input = this.getInputByPropId(this.properties[propId].altLocationPropId);
                var altPropId = this.properties[propId].altLocationPropId;

                if (input !== false && input.value.length > 0 && !input.disabled && this.properties[altPropId].valueSource != 'default') {

                    //this.modes[propId][mode] = true;
                    return true;
                }
            }
        }

        return false;
    },

    checkAbility: function (propId, ability) {

        if (typeof this.properties[propId] == 'undefined')
            this.properties[propId] = {};

        if (typeof this.properties[propId].abilities == 'undefined')
            this.properties[propId].abilities = {};

        if (typeof this.properties[propId].abilities != 'undefined' && this.properties[propId].abilities[ability])
            return true;

        if (ability == 'canHaveAltLocation') {

            if (this.properties[propId].type == 'LOCATION') {

                // try to find corresponding alternate location prop
                if (typeof this.properties[propId].altLocationPropId != 'undefined' && typeof this.properties[this.properties[propId].altLocationPropId]) {

                    var altLocPropId = this.properties[propId].altLocationPropId;

                    if (typeof this.properties[propId].control != 'undefined' && this.properties[propId].control.getSysCode() == 'slst') {

                        if (this.getInputByPropId(altLocPropId) !== false) {
                            this.properties[propId].abilities[ability] = true;
                            return true;
                        }
                    }
                }
            }

        }

        return false;
    },

    getInputByPropId: function (propId) {
        if (typeof this.properties[propId].input != 'undefined')
            return this.properties[propId].input;

        var row = this.getRowByPropId(propId);
        if (BX.type.isElementNode(row)) {
            var input = row.querySelector('input[type="text"]');
            if (BX.type.isElementNode(input)) {
                this.properties[propId].input = input;
                return input;
            }
        }

        return false;
    },

    getRowByPropId: function (propId) {

        if (typeof this.properties[propId].row != 'undefined')
            return this.properties[propId].row;

        var row = this.controls.scope.querySelector('[data-property-id-row="' + propId + '"]');
        if (BX.type.isElementNode(row)) {
            this.properties[propId].row = row;
            return row;
        }

        return false;
    },

    getAltLocPropByRealLocProp: function (propId) {
        if (typeof this.properties[propId].altLocationPropId != 'undefined')
            return this.properties[this.properties[propId].altLocationPropId];

        return false;
    },

    toggleProperty: function (propId, way, dontModifyRow) {

        var prop = this.properties[propId];

        if (typeof prop.row == 'undefined')
            prop.row = this.getRowByPropId(propId);

        if (typeof prop.input == 'undefined')
            prop.input = this.getInputByPropId(propId);

        if (!way) {
            if (!dontModifyRow)
                BX.hide(prop.row);
            prop.input.disabled = true;
        } else {
            if (!dontModifyRow)
                BX.show(prop.row);
            prop.input.disabled = false;
        }
    },

    submitFormProxy: function (item, control) {
        var propId = false;
        for (var k in this.properties) {
            if (typeof this.properties[k].control != 'undefined' && this.properties[k].control == control) {
                propId = k;
                break;
            }
        }

        // turning LOCATION_ALT_PROP_DISPLAY_MANUAL on\off

        if (item != 'other') {

            if (this.BXCallAllowed) {

                this.BXCallAllowed = false;
                setTimeout(function () {
                    BX.Sale.OrderAjaxComponent.sendRequest()
                }, 20);
            }

        }
    },

    getPreviousAdapterSelectedNode: function (control, adapter) {

        var index = adapter.getIndex();
        var prevAdapter = control.getAdapterAtPosition(index - 1);

        if (typeof prevAdapter !== 'undefined' && prevAdapter != null) {
            var prevValue = prevAdapter.getControl().getValue();

            if (typeof prevValue != 'undefined') {
                var node = control.getNodeByValue(prevValue);

                if (typeof node != 'undefined')
                    return node;

                return false;
            }
        }

        return false;
    },
    getLocationsByZip: function (value, successCallback, notFoundCallback) {
        if (typeof this.indexCache[value] != 'undefined') {
            successCallback.apply(this, [this.indexCache[value]]);
            return;
        }

        var ctx = this;

        BX.ajax({
            url: this.options.source,
            method: 'post',
            dataType: 'json',
            async: true,
            processData: true,
            emulateOnload: true,
            start: true,
            data: {'ACT': 'GET_LOCS_BY_ZIP', 'ZIP': value},
            //cache: true,
            onsuccess: function (result) {
                if (result.result) {
                    ctx.indexCache[value] = result.data;
                    successCallback.apply(ctx, [result.data]);
                } else {
                    notFoundCallback.call(ctx);
                }
            },
            onfailure: function (type, e) {
                // on error do nothing
            }
        });
    },
};

CustomLocationChangeYandex = {
    addressesSuggested: [],
    selectedAddressIndex: 0,
    $parentBlock: false,
    timer: false,
    minlength: 3,
    $addressInput: false,
    suggestBoundedBy: [[55.898952, 92.491655], [56.294369, 93.717311]],

    init: function () {
        var self = this;

        self.initKeyDownEvents();
        self.initClickEvents();

        $(document).on('input propertychange', '[data-role="suggested_address_input"]', function () {
            self.$addressInput = $(this);
            self.$parentBlock = self.$addressInput.parent();
            // $('#choose_ymap_address').attr('disabled', 'disabled');
            $('[data-role="suggested_address_geo_coords"]').val('');
            $('#ymap_address_suggestion_geo_coords').val('');

            var query = self.$addressInput.val();
            self.ajaxLocationCall(query);
        });

        $(document).on('input propertychange', '#ymap_address_suggestion_address_apartament_input', function () {
            self.apartamentDefaultView();
        });

        $(document).ready(function () {
            if (!self.$addressInput) {
                self.$addressInput = $('[data-role="suggested_address_input"]').first();
            }
            if ($('textarea[data-role="suggested_address"]').length && $('[data-role="suggested_address_input"]').length) {
                self.setFieldsInitValues();
            }
        });
    },

    initYandexMapEvents: function () {
        var self = this;
        yandexMap.events.add('click', function (e) {
            var coords = e.get('coords');
            self.mapClearObjects();
            self.geocode(coords);
        });
        $('#choose_ymap_address').off('click').on('click', function () {
            // if (self.validateApartamentField()){
                self.setAddressFormFields();
                $.fancybox.close();
            // }
        });
    },

    setFieldsInitValues: function () {
        var geoCoords = $('[data-role="suggested_address_geo_coords"]').val();
        if (!geoCoords){
            $('textarea[data-role="suggested_address"]').val('')
        }
        $('#ymap_address_suggestion_geo_coords').val(geoCoords);

        var address = $('textarea[data-role="suggested_address"]').val();
        var appartament = '';
        var addressSplit = address.split('кв.');
        if (addressSplit.length > 1) {
            appartament = addressSplit[addressSplit.length - 1].trim()
            addressSplit.pop();
            address = addressSplit.join('кв.').trim();
        }

        $('[data-role="suggested_address_input"]').val(address);
        $('#ymap_address_suggestion_address_apartament_input').val(appartament);
    },

    setAddressFormFields: function () {
        var fullAddress = $('[data-role="suggested_address_input"]').val().trim();
        var apartament = $('#ymap_address_suggestion_address_apartament_input').val().trim();
        if (apartament) {
            fullAddress += ' кв. ' + apartament;
        }
        $('textarea[data-role="suggested_address"]').val(fullAddress);

        var geoCoords = $('#ymap_address_suggestion_geo_coords').val().trim();
        $('[data-role="suggested_address_geo_coords"]').val(geoCoords);
        $('[data-role="suggested_address_geo_info"]').val('');
    },

    validateApartamentField: function (){
        var self = this;
        var apartament = $('#ymap_address_suggestion_address_apartament_input').val().trim();
        var res = false;
        if (!apartament){
            var error = 'введите номер квартиры или поставте "-"';
            var hint = '';
            self.showError({error: error, hint: hint});
            self.apartamentErrorView();
            res = false;
        } else {
            self.apartamentDefaultView();
            res = true;
        }
        return res;
    },

    apartamentErrorView: function(){
        $('#ymap_address_suggestion_address_apartament_input').addClass('error');
    },
    apartamentDefaultView: function(){
        $('#ymap_address_suggestion_address_apartament_input').removeClass('error');
    },

    ajaxLocationCall: function (query) {
        var self = this;

        if (self.timer) {
            clearTimeout(self.timer);
        }

        if (query.length >= self.minlength) {
            self.timer = setTimeout(function () {
                ymaps.suggest(query,
                    {
                        boundedBy: self.suggestBoundedBy
                    }
                ).then(function (items) {
                    self.addressesSuggestionsYandex(items);

                    // if (!items.length) {
                    //     var error = 'Адрес не найден, введите адрес без квартиры.';
                    //     var hint = '';
                    //     self.showError({error: error, hint: hint})
                    // }

                    self.renderAddressDropdown();
                });
            }, 200);
        } else {
            self.addressesSuggested = [];
            self.renderAddressDropdown();
        }
    },

    addressesSuggestionsYandex: function (addressesSuggestion) {
        var self = this;
        if (addressesSuggestion.length > 0) {
            for (var k = 0; k < addressesSuggestion.length; k++) {
                addressesSuggestion[k]['yandexValue'] = addressesSuggestion[k]['value'];
                addressesSuggestion[k]['value'] = addressesSuggestion[k]['displayName'];
            }
            this.addressesSuggested = addressesSuggestion;
        } else {
            this.addressesSuggested = [];
        }
    },

    initKeyDownEvents: function () {
        var self = this;
        $(document).keydown(function (e) {
            if (self.isDropMenuInit()) {
                switch (e.which) {
                    case 38: // up
                        self.menuMoveUp();
                        e.preventDefault();
                        break;

                    case 40: // down
                        self.menuMoveDown();
                        e.preventDefault();
                        break;

                    case 13: // enter
                        self.menuSelect(self.selectedAddressIndex);
                        e.preventDefault();
                        break;
                }
            }
        });
    },

    initClickEvents: function () {
        var self = this;
        document.addEventListener('click', function (e) {
            var dropdown = document.querySelector('[data-role="address_dropdown"]');
            if (dropdown && !dropdown.contains(e.target)) {
                dropdown.remove();
            }
        });
    },

    isDropMenuInit: function () {
        return this.addressesSuggested.length;
    },

    menuMoveUp: function () {
        if (this.isDropMenuInit() && this.selectedAddressIndex > 0) {
            this.selectedAddressIndex--;
            this.reSelectRow();
        }
    },

    menuMoveDown: function () {
        if (this.isDropMenuInit() && this.selectedAddressIndex < (this.addressesSuggested.length - 1)) {
            this.selectedAddressIndex++;
            this.reSelectRow();
        }
    },

    menuSelect: function (index) {
        var self = this;
        var address = this.addressesSuggested[index]['value'];

        self.geocode(address)
        this.addressesSuggested = [];
        this.renderAddressDropdown();
    },

    reSelectRow: function () {
        this.$parentBlock.find('[data-role="address_dropdown_item"]').removeClass('selected');
        this.$parentBlock.find('[data-role="address_dropdown_item"][data-row_index="' + this.selectedAddressIndex + '"]').addClass('selected');
    },

    renderAddressDropdown: function () {
        this.$parentBlock.find('[data-role="address_dropdown"]').remove();
        if (this.isDropMenuInit()) {
            var html = '<div class="address_dropdown" data-role="address_dropdown">';
            for (var k = 0; k < this.addressesSuggested.length; k++) {
                var addressSuggested = this.addressesSuggested[k];
                var addClass = '';
                if (k == 0) {
                    addClass = 'selected';
                    this.selectedAddressIndex = k;
                }

                var addressText = addressSuggested['value'];
                if (typeof addressSuggested.hl != 'undefined' && addressSuggested.hl.length > 0 && Array.isArray(addressSuggested.hl)) {
                    var offset = 0;
                    for (var j = 0; j < addressSuggested.hl.length; j++) {
                        var hl = addressSuggested.hl[j];

                        if (hl.length == 2) {
                            addressText = addressText.substring(0, hl[1] + offset) + '</b>' + addressText.substring(hl[1] + offset);
                            addressText = addressText.substring(0, hl[0] + offset) + '<b>' + addressText.substring(hl[0] + offset);
                            offset += '<b></b>'.length;
                        }

                    }
                }
                html += '<div class="address_dropdown-item ' + addClass + '" data-row_index="' + k + '" data-role="address_dropdown_item">' + addressText + '</div>';

            }
            html += '</div>';
            this.$parentBlock.append(html);
            this.initMenuItemClick();
        }
    },

    initMenuItemClick: function () {
        var self = this;
        self.$parentBlock.find('[data-role="address_dropdown_item"]').off("click").on('click', function () {
            var clickedRowIndex = $(this).attr('data-row_index');
            self.menuSelect(clickedRowIndex);
        });
    },

    showError: function (message) {
        $('#ymap_address_suggestion_error').html(message.error);
        $('#ymap_address_suggestion_hint').html(message.hint);
    },

    clearErrors: function () {
        $('#ymap_address_suggestion_error').html('');
        $('#ymap_address_suggestion_hint').html('');
    },

    chooseAddress: function (geoApiObj) {
        this.mapAddChosenAddress(geoApiObj.geometry.getCoordinates());
        this.mapNewCenter(geoApiObj.geometry.getCoordinates());
        $('#ymap_address_suggestion_geo_coords').val(geoApiObj.geometry.getCoordinates().join(','));
        // $('#choose_ymap_address').removeAttr('disabled');
    },

    mapAddChosenAddress: function (coords) {
        var placemark = new ymaps.Placemark(coords);
        this.mapClearObjects();
        yandexMap.geoObjects.add(placemark);
    },

    mapClearObjects: function () {
        yandexMap.geoObjects.removeAll();
    },

    mapNewCenter: function (coords) {
        var zoom = 17;
        yandexMap.setCenter(coords, zoom);
    },

    geocode: function (address) {
        var self = this;
        ymaps.geocode(address).then(function (res) {
            var obj = res.geoObjects.get(0),
                error, hint;

            if (obj) {
                switch (obj.properties.get('metaDataProperty.GeocoderMetaData.precision')) {
                    case 'exact':
                        break;
                    case 'number':
                    case 'near':
                    case 'range':
                        // error = 'Неточный адрес, требуется уточнение';
                        // hint = 'Уточните номер дома';
                        break;
                    case 'street':
                        // error = 'Неполный адрес, требуется уточнение';
                        // hint = 'Уточните номер дома';
                        break;
                    case 'other':
                    default:
                        // error = 'Неточный адрес, требуется уточнение';
                        // hint = 'Уточните адрес';
                }
                var shortAddress = obj.properties.getAll().name;
                self.$addressInput.val(shortAddress);
            } else {
                // error = 'Адрес не найден, проверьте адрес или выберите дом на карте.';
                // hint = '';
            }

            if (error) {
                self.showError({error: error, hint: hint});
            } else {
                self.clearErrors();
                self.chooseAddress(obj);
            }
        }, function (e) {
            console.log(e)
        })

    },
}

// CustomLocationChangeYandex.init(); //yandex карты