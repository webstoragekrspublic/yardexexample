<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);

$arResult['DELIVERY_TIME_SLOTS'] = \Custom\DeliveryTime\DeliveryTime::getInstance()->getDaysInfoForCurrentCart();

$arResult['HIDDEN_INPUTS'] = [];
$hasExpiredItems = $hasNormalItems = false;
foreach ($arResult['BASKET_ITEMS'] as $item) {
    foreach ($item['PROPS'] as $prop) {
        if($prop['CODE'] === \Helpers\Constants::BASKET_PROP_EXPIRE_CODE) {
            $hasExpiredItems = true;
            continue 2;
        }
    }
    $hasNormalItems = true;
}
if($hasExpiredItems && $hasNormalItems) {
    LocalRedirect($arParams['PATH_TO_BASKET']);
}

if($hasExpiredItems) {
    $arResult['SHOW_SIMPLE_TEMPLATE'] = true;
    $needProps = [
        1,2,3
    ];
    $payId = 9;
    $deliveryId = 1;
    foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as $key => $property){
        if(!in_array((int) $property['ID'], $needProps, true)) {
            if(in_array($property['CODE'], [
                'DELIVERY_DATE',
                'DELIVERY_TIME',
                'ADDRESS',
            ], true)) {
                $arResult['HIDDEN_INPUTS']['ORDER_PROP_'.$property['ID']] = 'Товары СГ';
            }
            
            unset($arResult['JS_DATA']['ORDER_PROP']['properties'][$key]);
        }
    }
    foreach ($arResult['JS_DATA']['PAY_SYSTEM'] as $key => &$pay){
        if((int) $pay['ID'] !== $payId) {
            
            unset($arResult['JS_DATA']['PAY_SYSTEM'][$key]);
        } else {
            $pay['NAME'] = 'При получении';
            $pay['CHECKED'] = 'Y';
        }
    }
    unset($pay);

    $arResult['JS_DATA']['DELIVERY'] = [];
}
foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as &$property){
    if ($property['CODE'] == 'NOT_CALLING_BACK'){
        $property['NAME'] = 'Не перезванивать';
        $property['TEXT_UNDER_NAME'] = 'Перезвоним только в случае изменения условий заказа';
        global $USER;
        if ($userId = $USER->GetID()){
            $parameters = [
                'select' => ['USER_ID','CNT'],
                'filter' => ["USER_ID" => $userId],
                'runtime' => array(
                    new Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(*)')
                )
            ];

            $orders = \Bitrix\Sale\Order::getList($parameters);
            $order = $orders->fetch();
            if ($order['CNT'] >= 1){
                $property['IS_MORE_ONE_ORDER'] = 'Y';
                //$property['VALUE'][0] == 'Y' ? $property['VALUE'][0] = 'N' : $property['VALUE'][0] = 'Y';//меняем галочку, чтобы отрабатывала логика "не перезванивать" "перезванивать"
            }
        }
    }
}

/**
 * Проверим использовал ли пользователь одноразовый купон
 */
global $USER;
$userId = $USER->GetID();
$arUserCoupons = \CUser::GetByID($userId)->Fetch()['UF_USED_COUPON'];

if (in_array(mb_strtolower($arResult['JS_DATA']['COUPON_LIST'][0]['COUPON']), $arUserCoupons)) {
    $arResult['REFRESH_ORDER'] = true;
    \Bitrix\Main\Context::getCurrent()->getResponse()->addCookie(
        new \Bitrix\Main\Web\Cookie('DELETE_COUPON', 'Y')
    );
}

if ($arResult['REFRESH_ORDER']) {
    \Bitrix\Sale\DiscountCouponsManager::init();
    \Bitrix\Sale\DiscountCouponsManager::clear(true);

    \Bitrix\Sale\DiscountCouponsManager::delete($arResult['JS_DATA']['COUPON_LIST'][0]['COUPON']);
    LocalRedirect($_REQUEST['backurl']);
}

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$arResult['DELETE_COUPON'] = $request->getCookie("DELETE_COUPON");
