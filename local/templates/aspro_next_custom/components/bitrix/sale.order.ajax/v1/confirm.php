<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 */

if ($arParams["SET_TITLE"] == "Y") {
    $APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_COMPLETE"));
}

$needPaymentInputSystemsId = [
    '23' => true //QIWI
];

$redirectUser = false;
$context = \Bitrix\Main\Application::getInstance()->getContext();
$request = $context->getRequest();
?>

<? if (!empty($arResult["ORDER"])): ?>

    <?
    if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y') {

//&& $request->get('FROM') != 'sberbank' - костыль, т.к. после оплаты переводится снова в заказ где еще не всегда сформирована оплата для заказа,
//что приводит к очередной переадресации клиента на оплату (хотя она была проведена)
        if (!empty($arResult["PAYMENT"])  && $request->get('FROM') != 'sberbank') {
            foreach ($arResult["PAYMENT"] as $payment) {
                $arPaySystem = $arResult['PAY_SYSTEM_LIST_BY_PAYMENT_ID'][$payment["ID"]] ?? false;
                if ($arPaySystem and $arPaySystem['IS_CASH'] != 'Y') {

                    if ($payment["PAID"] != 'Y') {
                        if (!empty($arResult['PAY_SYSTEM_LIST'])
                            && array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
                        ) {
                            $arPaySystem = $arResult['PAY_SYSTEM_LIST_BY_PAYMENT_ID'][$payment["ID"]];
                            $isNeedPaymentInput = isset($needPaymentInputSystemsId[$arPaySystem["PAY_SYSTEM_ID"]]);


                            if (empty($arPaySystem["ERROR"])) {
                                $redirectUser = true;
                                ?>

                                <table class="sale_order_full_table">
                                    <tr>
                                        <td class="ps_logo">
                                            <div class="pay_name"><?= Loc::getMessage("SOA_PAY") ?></div>
                                            <?= CFile::ShowImage($arPaySystem["LOGOTIP"], 100, 100, "border=0\" ", "", false) ?>
                                            <div class="paysystem_name"><?= $arPaySystem["NAME"] ?></div>
                                            <br/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <? if (strlen($arPaySystem["ACTION_FILE"]) > 0 && $arPaySystem["NEW_WINDOW"] == "Y" && $arPaySystem["IS_CASH"] != "Y"): ?>
                                                <?
                                                $orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
                                                $paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
                                                ?>
                                                <script>
                                                    window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=$orderAccountNumber?>&PAYMENT_ID=<?=$paymentAccountNumber?>');
                                                </script>
                                            <?= Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . $orderAccountNumber . "&PAYMENT_ID=" . $paymentAccountNumber)) ?>
                                            <? if (CSalePdf::isPdfAvailable() && $arPaySystem['IS_AFFORD_PDF']): ?>
                                            <br/>
                                                <?= Loc::getMessage("SOA_PAY_PDF", array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . $orderAccountNumber . "&pdf=1&DOWNLOAD=Y")) ?>
                                            <? endif ?>
                                            <? else: ?>
                                                <?= $arPaySystem["BUFFERED_OUTPUT"] ?>
                                            <? endif ?>
                                        </td>
                                    </tr>
                                </table>

                                <? if (!$isNeedPaymentInput): ?>
                                    <script>
                                        if ($('.sale-paysystem-wrapper .sale-paysystem-yandex-button-item').length) {
                                            setTimeout(() => window.location = $('.sale-paysystem-wrapper .sale-paysystem-yandex-button-item').attr('href'), 200);
                                        }
                                    </script>
                                <? endif; ?>

                                <?
                            } else {
                                ?>

                                <?

                                $sendParams = [
                                    'THEME' => 'ошибка Оплаты заказа №' . $arResult["ORDER"]["ACCOUNT_NUMBER"],
                                    'BODY_TEXT' => print_r($arPaySystem["ERROR"], 1),
                                ];


                                \Helpers\CustomTools::sendErrorsReport($sendParams);
                                ?>

                                <span style="color:red;"><?= Loc::getMessage("SOA_ORDER_PS_ERROR") ?></span>
                                <?
                            }
                        } else {
                            ?>

                            <?
                            $sendParams = [
                                'THEME' => 'ошибка Оплаты заказа №' . $arResult["ORDER"]["ACCOUNT_NUMBER"],
                                'BODY_TEXT' => 'не выполнены блок с условиями оплаты',
                            ];

                            \Helpers\CustomTools::sendErrorsReport($sendParams);
                            ?>

                            <span style="color:red;"><?= Loc::getMessage("SOA_ORDER_PS_ERROR") ?></span>
                            <?
                        }
                    }
                }
            }
        }
    } else {
        ?>
        <br/><strong><?= $arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR'] ?></strong>
        <?
    }
    ?>

    <? if (!$redirectUser): ?>
        <? //скрипт на конверсию ?>
        <script>
            $(document).ready(function(){
                gtag('event', 'conversion', {'send_to': 'AW-623620625/R7HJCPWD5OkBEJHkrqkC'});
            })
        </script>
        <div class="sale_order_finish_block-success">
            <div class='sale_order_finish_block-border'>
                <div class="sale_order_finish_block-success__img_block">
                    <img class="sale_order_finish_block-success__thx_img" src="/local/templates/aspro_next_custom/components/bitrix/sale.order.ajax/v1/images/thx_big.png"/>
                    <img class="sale_order_finish_block-success__thx_img_small" src="/local/templates/aspro_next_custom/components/bitrix/sale.order.ajax/v1/images/thx.png"/>
                </div>

                <div class="sale_order_finish_block-success__success_info">
                    Ваш заказ №<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]; ?> успешно оформлен
                </div>
                <div class="sale_order_finish_block-mobile_app">
                    <div class="sale_order_finish_block-mobile_app-text">                
                        Для более удобного совершения заказа можете воспользоваться приложением:
                    </div>
                    <div class="sale_order_finish_block-mobile_app-links">
                        <a href="https://apps.apple.com/ru/app/com.yardex.mobile-app/id1555057094"><img width="180" alt="Скачать в App Store" src="/upload/medialibrary/0ba/0ba84a6cff05d7eb60fc88984bccec13.png" height="59" title="Скачать в App Store"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.yardex.mobile_app"><img width="180" alt="Скачать в Google Play" src="/upload/medialibrary/c93/c9320e3610efb611153cf765583619f3.png" height="59" title="Скачать в Google Play"></a>
                    </div>
                </div>
                <div style="width: 100%; text-align: center;">
                    <? $APPLICATION->IncludeComponent(
                        "custom:empty_component",
                        "messengers_links",
                        array(
                            "CACHE_TIME" => "360000",
                            "CACHE_TYPE" => "A"
                        )
                    ); ?>
                </div>
            </div>

            <div style="width: 100%; text-align: center; margin-top: 20px;">
                Что-то не нашли на нашем сайте?
            </div>
            <div style="width: 100%; text-align: center;">
                <a data-fancybox href="#not_found_products">Напишите в Ярдекс</a>
            </div>

            <? if ($request->get('FROM') == 'sberbank'): ?>
                <hr>
                <div class="sale_order_finish_block-success__success_sber_info">
                    <p>
                        При оплате заказа на сайте деньги на вашей карте холдируются (блокируются).
                        Списание средств с карты происходит после получения заказа от курьера в сумме, соответствующей стоимости фактически принятых товаров.
                        Остальные средства разблокируются на счете и становятся доступны вам в течение 30 рабочих дней.
                        Срок зависит от банка, выдавшего вам карту.
                    </p>
                    <p>
                        Банк уведомляет по смс о сумме, которая временно замораживается.
                        На момент фактического списания смс-уведомление от банка не поступает.
                        При возврате разницы между замороженной и списанной суммами смс от банка также не приходит, т. к. это не является пополнением счёта.
                        Для того что бы узнать информацию о размораживании остатка суммы на карте, необходимо посмотреть детализацию по счету вашей карты через личный кабинет (не через мобильное приложение), либо позвонить на горячую линию банка.
                    </p>
                </div>
            <? endif; ?>

        </div>

    <? endif; ?>


<? else: ?>

    <b><?= Loc::getMessage("SOA_ERROR_ORDER") ?></b>
    <br/><br/>

    <table class="sale_order_full_table">
        <tr>
            <td>
                <?= Loc::getMessage("SOA_ERROR_ORDER_LOST", ["#ORDER_ID#" => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"])]) ?>
                <?= Loc::getMessage("SOA_ERROR_ORDER_LOST1") ?>
            </td>
        </tr>
    </table>

<? endif ?>

<script>
	const couponsCookie = 'BITRIX_SM_ORDER_CURRENT_COUPONS';
	let curCookies = [];
	BX.setCookie(couponsCookie, JSON.stringify(curCookies), {
		expires: -86400,
		path: '/',
	});
</script>




