<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
IncludeTemplateLangFile(__FILE__);
global $APPLICATION, $arRegion, $arSite, $arTheme, $USER, $arBasketPrices, $arBackParametrs;
$arSite = CSite::GetByID(SITE_ID)->Fetch();
$arBackParametrs = CNext::GetBackParametrsValues(SITE_ID);
$phone = $arBackParametrs['HEADER_PHONES_array_PHONE_VALUE_0'];
$htmlClass = ($_REQUEST && isset($_REQUEST['print']) ? 'print' : false);
$bIncludedModule = (\Bitrix\Main\Loader::includeModule("aspro.next"));
$currentSiteInfo = CSite::GetByID(SITE_ID)->getNext();

$styles = [
    SITE_TEMPLATE_PATH . '/fonts.css',
    SITE_TEMPLATE_PATH . '/js/swiper/swiper.min.css',
    SITE_TEMPLATE_PATH . '/css/animation/animate.min.css',
    SITE_TEMPLATE_PATH . '/css/star-rating-svg.css',
];
$scripts = [
    SITE_TEMPLATE_PATH . '/js/swiper/swiper.min.js',
    SITE_TEMPLATE_PATH . '/js/jquery.history.js',
    SITE_TEMPLATE_PATH . '/js/jquery.star-rating-svg.js'
];
$asset = Bitrix\Main\Page\Asset::getInstance();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= LANGUAGE_ID ?>"
      lang="<?= LANGUAGE_ID ?>" <?= ($htmlClass ? 'class="' . $htmlClass . '"' : '') ?>>
    <head>
        
        <title><? $APPLICATION->ShowTitle() ?></title>
        <? $APPLICATION->ShowMeta("viewport"); ?>
        <? $APPLICATION->ShowMeta("HandheldFriendly"); ?>
        <? $APPLICATION->ShowMeta("apple-mobile-web-app-capable", "yes"); ?>
        <? $APPLICATION->ShowMeta("apple-mobile-web-app-status-bar-style"); ?>
        <? $APPLICATION->ShowMeta("SKYPE_TOOLBAR"); ?>
        <? $APPLICATION->ShowHead(); ?>
       
        <? $APPLICATION->AddHeadString('<script>BX.message(' . CUtil::PhpToJSObject($MESS, false) . ')</script>', true); ?>

        <? foreach ($styles as $style) : ?>

            <? $asset->addCss($style); ?>

        <? endforeach; ?>

        <? foreach ($scripts as $script) : ?>

            <? $asset->addJs($script); ?>

        <? endforeach; ?>

        <?
            CNext::AddMeta(
                array(
                    'og:image' => '/images/mainpage/promo_text_bg.jpg',
                )
            );
        ?>
        <? if ($bIncludedModule)
            CNext::Start(SITE_ID); ?>
        <meta name="yandex-verification" content="561991574b0e9e32"/>
        <meta name="facebook-domain-verification" content="9zhke7qew1sbeqj9zzodnc26c0mg4q" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
              rel="stylesheet">
    </head>
<?php if(IsModuleInstalled("mk.rees46") ):
    $APPLICATION->IncludeComponent('rees46:handler', '.default');
endif; ?>
<? $bIndexBot = (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Lighthouse') !== false); // is indexed yandex/google bot?>
<body class="site_<?= SITE_ID ?> <?= ($bIncludedModule ? "fill_bg_" . strtolower(CNext::GetFrontParametrValue("SHOW_BG_BLOCK")) : ""); ?> <?= ($bIndexBot ? "wbot" : ""); ?>"
      id="main">
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? if (!$bIncludedModule): ?>
    <? $APPLICATION->SetTitle(GetMessage("ERROR_INCLUDE_MODULE_ASPRO_NEXT_TITLE")); ?>
    <center><? $APPLICATION->IncludeFile(SITE_DIR . "include/error_include_module.php"); ?></center></body></html><? die(); ?>
<? endif; ?>

<? $arTheme = $APPLICATION->IncludeComponent("aspro:theme.next", ".default", array("COMPONENT_TEMPLATE" => ".default"), false, array("HIDE_ICONS" => "Y")); ?>

<? include_once('defines.php'); ?>
<? CNext::SetJSOptions(); ?>

<div class="wrapper1 <?= ($isIndex && $isShowIndexLeftBlock ? "with_left_block" : ""); ?> <?= CNext::getCurrentPageClass(); ?> <?= CNext::getCurrentThemeClasses(); ?>">
<? CNext::get_banners_position('TOP_HEADER'); ?>

    <div class="header_wrap visible-lg visible-md title-v<?= $arTheme["PAGE_TITLE"]["VALUE"]; ?><?= ($isIndex ? ' index' : '') ?>">
        <header id="header">            
            <div class="utm_to_cookie">
                <? $APPLICATION->IncludeComponent(
                    "custom:empty_component",
                    "utm_to_cookie",
                    [],
                    false
                );
                ?>
            </div>
            <div class="maxwidth-theme no_before_after_content header_top_info">
                <div class="header_top_info_left">
                    <div class="header_main_logo">
                        <a href="/"><img class="header_main_logo_img" src="/logo.png" alt="<?= $currentSiteInfo['NAME']; ?>"
                                         title="<?= $currentSiteInfo['NAME']; ?>"></a>
                        <span class="header_main_logo_title">Онлайн-супермаркет <br>регулярных закупок!</span>
                    </div>
                </div>
                <div class="header_top_info_right">
                    <a class="header_top_info_right_contacts" href="/contacts/">Сделай заказ по тел.</a>
                    <div class="header_top_info_right_phone">
                        <a rel="nofollow" href="tel:+<?= preg_replace("/[^\d]+/", "", $phone); ?>">
                            <div class="header_top_info_right_phone_block">
                                <img class="header_top_info_right_phone_image" src="/images/icons/phone_top.png"/>
                                <div class="header_top_info_right_phone_value_block">
                                    <span class="header_top_info_right_phone_value">
                                        <? preg_match("/[^\+7 391 ][\d\- ]{7,10}$/", $phone, $match);
                                        echo $match[0];
                                        ?>
                                    </span>
                                    <span class="header_top_info_right_phone_descr">
                                        с 8:00 до 20:00
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="header_top_info_right_enter">
                        <? if (!$USER->IsAuthorized()): ?>
                            <a rel="nofollow" class="animate-load" data-event="jqm" data-param-type="auth"
                               data-param-backurl="/?login=yes" data-name="auth" href="/personal/">
                                <img src="/images/icons/personal.png"/>
                                <span class="header_top_info_right_enter_text">Войти</span>
                            </a>
                            <a rel="nofollow" href="/auth/registration/?register=yes">
                                <span class="header_top_info_right_enter_text_registration">Регистрация</span>
                            </a>
                        <? else: ?>
                            <a href="/auth/personal/">
                                <img src="/images/icons/personal.png"/>
                                <span class="header_top_info_right_enter_text">Личный кабинет</span>
                            </a>
                        <? endif; ?>
                    </div>
                </div>
            </div>
            <div class="header_main">
                <div class="header_main_bg_right"></div>
                <div class="maxwidth-theme header_main_maxwidth">

                    <?
                        $checkedFastDelivery = '';
                        if (\Helpers\CustomTools::isFastDeliveryEnabled()){
                            $checkedFastDelivery = 'checked="checked"';
                        }
                    ?>

                    <div class="header_main_fast_delivery">
                        <label class="switch">
                            <input type="checkbox" data-role="fast_delivery_input" id="fast_delivery_main_input" <?=$checkedFastDelivery;?> >
                            <span class="slider round"></span>
                        </label>
                        <label class="fast_delivery_main_input_label" for="fast_delivery_main_input">Экспресс доставка в течении 60 минут</label>
                        <div class="fast_delivery_main_icon_wrap">
                            <i class="fa fa-question-circle fast_delivery_main_icon_question" title="Подробности экспресс доставки" data-role="express_delivery_question"></i>
                            <img class="fast_delivery_icon_small" src="/images/icons/fast_delivery_icon_black.svg">
                        </div>
                    </div>

                    <div class="header_main_search">

                        <? $APPLICATION->IncludeComponent(
                            "custom:empty_component",
                            "search_title",
                            [],
                            false
                        );
                        ?>

                    </div>
                   
                    <div class="header_main_right">
                        <? $APPLICATION->IncludeComponent(
                            "custom:favorites.on.cookies",
                            "menu.info",
                            array(
                                "PATH_TO_FULL_FAV_PAGE" => "/favorites/",
                            ),
                            false
                        );
                        ?>
                        <a rel="nofollow"
                           class="basket-link basket <?= ($arBasketPrices['BASKET_COUNT'] ? 'basket-count' : ''); ?>"
                           href="<?= $arTheme['BASKET_PAGE_URL']['VALUE']; ?>"
                           title="Перейти в корзину">
							<span class="js-basket-block">
                                <span class="js-basket-block_icon_block">
                                    <img src="/images/icons/shoping_cart.png"/>
                                </span>
                                <span class="wrap no_wrap_text">
                                    <span class="basket-link_cart_text">Корзина</span>
                                    <span class="prices"><?= ($arBasketPrices['BASKET_COUNT'] ? $arBasketPrices['BASKET_SUMM'] : $arBasketPrices['BASKET_SUMM_TITLE_SMALL']) ?></span>
                                </span>
								<span class="count"><?= $arBasketPrices['BASKET_COUNT']; ?></span>
							</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="header-v4 header-wrapper">
                <div class="menu-row middle-block bg<?= strtolower($arTheme["MENU_COLOR"]["VALUE"]); ?>">
                    <div class="maxwidth-theme">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="menu-only">
                                    <nav class="mega-menu sliced">
                                        <? $APPLICATION->IncludeComponent("bitrix:main.include", ".default",
                                            array(
                                                "COMPONENT_TEMPLATE" => ".default",
                                                "PATH" => SITE_DIR . "include/menu/menu.top.php",
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "",
                                                "AREA_FILE_RECURSIVE" => "Y",
                                                "EDIT_TEMPLATE" => "include_area.php"
                                            ),
                                            false, array("HIDE_ICONS" => "Y")
                                        ); ?>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>
    </div>
<? CNext::get_banners_position('TOP_UNDERHEADER'); ?>

    <div id="headerfixed">
        <? CNext::ShowPageType('header_fixed'); ?>
    </div>

    <div id="mobileheader" class="visible-xs visible-sm">
        <? CNext::ShowPageType('header_mobile'); ?>
        <div id="mobilemenu"
             class="<?= ($arTheme["HEADER_MOBILE_MENU_OPEN"]["VALUE"] == '1' ? 'leftside' : 'dropdown') ?>">
            <? CNext::ShowPageType('header_mobile_menu'); ?>
        </div>
    </div>

<?
if ($isIndex) {
    $GLOBALS['arrPopularSections'] = array('UF_POPULAR' => 1);
    $GLOBALS['arrFrontElements'] = array('PROPERTY_SHOW_ON_INDEX_PAGE_VALUE' => 'Y');
} ?>

    <div class="wraps hover_shine" id="content">

        <div class="main_fast_delivery_mobile">
            <label class="switch">
                <input type="checkbox" data-role="fast_delivery_input" id="fast_delivery_main_input_mobile" <?=$checkedFastDelivery;?> >
                <span class="slider round"></span>
            </label>
            <label class="fast_delivery_main_input_label" for="fast_delivery_main_input">Экспресс доставка в течении 60 минут</label>
            <img class="fast_delivery_icon_small" src="/images/icons/fast_delivery_icon_white.svg">
            <i class="fa fa-question-circle fast_delivery_main_icon_question" title="Подробности экспресс доставки" data-role="express_delivery_question"></i>
        </div>

<? if (!$is404 && !$isForm && !$isIndex): ?>
    <? $APPLICATION->ShowViewContent('section_bnr_content'); ?>
    <? if ($APPLICATION->GetProperty("HIDETITLE") !== 'Y'): ?>
        <!--title_content-->
        <? CNext::ShowPageType('page_title'); ?>
        <!--end-title_content-->
    <? endif; ?>
    <? $APPLICATION->ShowViewContent('top_section_filter_content'); ?>
<? endif; ?>

<? if ($isIndex): ?>
    <div class="wrapper_inner front <?= ($isShowIndexLeftBlock ? "" : "wide_page"); ?>">
    <? elseif (!$isWidePage): ?>
    <div class="wrapper_inner <?= ($isHideLeftBlock ? "wide_page" : ""); ?>">
<? endif; ?>

<?if(($isIndex && $isShowIndexLeftBlock) || (!$isIndex && !$isHideLeftBlock)):?>
    <div class="right_block <?=(defined("ERROR_404") ? "error_page" : "");?> wide_<?=CNext::ShowPageProps("HIDE_LEFT_BLOCK");?>">
<?endif;?>

<div class="middle <?= ($is404 ? 'error-page' : ''); ?>">
<? CNext::get_banners_position('CONTENT_TOP'); ?>
<? if (!$isIndex): ?>
    <div class="container">
    <? //h1?>
    <? if ($isHideLeftBlock && !$isWidePage): ?>
    <div class="maxwidth-theme">
    <? endif; ?>
<? endif; ?>
<?
if (!isset($_REQUEST['getHeader'])) {
    CNext::checkRestartBuffer();
}
