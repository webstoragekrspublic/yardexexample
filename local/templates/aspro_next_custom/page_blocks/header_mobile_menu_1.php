<?
global $arBackParametrs, $USER, $arBasketPrices, $arTheme, $APPLICATION;
$phone = $arBackParametrs['HEADER_PHONES_array_PHONE_VALUE_0'];
?>

<div class="mobilemenu-v1 scroller">
    <div class="wrap mobile_menu_list">
        <div class="mobile_menu_list_top_items">
            <div class="mobile_menu_list_top_item phone_top">
                <a rel="nofollow" href="tel:+<?= preg_replace("/[^\d]+/", "", $phone); ?>">
                    <div class="mobile_menu_list_top_item_phone_block">
                        <div class="mobile_menu_list_top_item_left">
                            <img class="mobile_menu_list_top_item_left_image" src="/images/icons/phone_top.png">
                        </div>
                        <div class="mobile_menu_list_top_item_phone_value_block">
                            <span class="mobile_menu_list_phone">
                                <? preg_match("/[^\+7 391 ][\d\- ]{7,10}$/", $phone, $match);
                                echo $match[0];
                                ?>
                            </span>
                            <span class="mobile_menu_list_phone_descr">
                                с 8:00 до 20:00
                            </span>
                        </div>
                    </div>
                </a>
            </div>

            <div class="mobile_menu_list_top_item">
                <? if (!$USER->IsAuthorized()): ?>
                    <a rel="nofollow" href="/personal/">
                        <div class="mobile_menu_list_top_item_left">
                            <img src="/images/icons/personal.png"/>
                        </div>
                        <span class="header_top_info_right_enter_text">Войти</span>
                    </a>
                <? else: ?>
                    <a href="/auth/personal/">
                        <div class="mobile_menu_list_top_item_left">
                            <img src="/images/icons/personal.png"/>
                        </div>
                        <span class="header_top_info_right_enter_text">Личный кабинет</span>
                    </a>
                <? endif; ?>
            </div>

            <a rel="nofollow"
               class="mobile_menu_list_top_item basket-link basket <?= ($arBasketPrices['BASKET_COUNT'] ? 'basket-count' : ''); ?>"
               href="<?= $arTheme['BASKET_PAGE_URL']['VALUE']; ?>"
               title="Перейти в корзину">
                <span class="js-basket-block">
                    <div class="mobile_menu_list_top_item_left">
                        <span class="js-basket-block_icon_block">
                                <img src="/images/icons/shoping_cart.png"/>
                        </span>
                    </div>
                    <span class="wrap no_wrap_text">
                        <span class="basket-link_cart_text">Корзина</span>
                        <span class="prices"><?= ($arBasketPrices['BASKET_COUNT'] ? $arBasketPrices['BASKET_SUMM'] : $arBasketPrices['BASKET_SUMM_TITLE_SMALL']) ?></span>
                    </span>
                    <span class="count"><?= $arBasketPrices['BASKET_COUNT']; ?></span>
                </span>
            </a>

            <? $favoritesOnCookies = \Custom\FavoritesOnCookies\CurrentUserFavorites::getInstance(); ?>
            <div class="mobile_menu_list_top_item ">
                <a rel="nofollow"
                   class="custom_favoritesoncookies_mobile_menu a_standart_color" href="/favorites/"
                   title="Избранное">
                    <div class="mobile_menu_list_top_item_left">
                        <div class="custom_favoritesoncookies_image <?= $favoritesOnCookies->isHaveItems() ? 'have_items' : ''; ?>"
                             data-role="favorites.on.cookies.block"
                        ></div>
                    </div>
                    <span>Избранное
                        <span class="custom_favoritesoncookies_counter" data-role="favorites.on.cookies.counter">
                            <? if ($favoritesOnCookies->isHaveItems()): ?>
                                <?= $favoritesOnCookies->getTotalCnt(); ?>
                            <? endif; ?>
                        </span>
                    </span>
                </a>
            </div>

        </div>


        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "top_mobile",
            array(
                "COMPONENT_TEMPLATE" => "top_mobile",
                "MENU_CACHE_TIME" => "3600000",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_USE_GROUPS" => "N",
                "MENU_CACHE_GET_VARS" => array(),
                "DELAY" => "N",
                "MAX_LEVEL" => \Bitrix\Main\Config\Option::get("aspro.next", "MAX_DEPTH_MENU", 2),
                "ALLOW_MULTI_SELECT" => "Y",
                "ROOT_MENU_TYPE" => "top_content_multilevel_mobile",
                "CHILD_MENU_TYPE" => "left",
                "CACHE_SELECTED_ITEMS" => "N",
                "ALLOW_MULTI_SELECT" => "Y",
                "USE_EXT" => "Y"
            )
        ); ?>


        <div class="mobile_menu_list_top_items_second">
            <div class="mobile_menu_list_top_item ">
                <a class="a_standart_color" href="/sale/">
                    <div class="sale_menu_item">
                        Акции
                    </div>
                </a>
            </div>
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "mobile_menu_bottom",
                array(
                    "COMPONENT_TEMPLATE" => "mobile_menu_bottom",
                    "MENU_CACHE_TIME" => "3600000",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_USE_GROUPS" => "N",
                    "MENU_CACHE_GET_VARS" => array(),
                    "DELAY" => "N",
                    "MAX_LEVEL" => 2,
                    "ALLOW_MULTI_SELECT" => "Y",
                    "ROOT_MENU_TYPE" => "top_content_multilevel_mobile_bottom",
                    "CHILD_MENU_TYPE" => "left",
                    "CACHE_SELECTED_ITEMS" => "N",
                    "ALLOW_MULTI_SELECT" => "Y",
                    "USE_EXT" => "Y"
                )
            ); ?>
        </div>


    </div>
</div>