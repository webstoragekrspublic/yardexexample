<?
global $arTheme, $arRegion, $arBasketPrices;
$logoClass = ($arTheme['COLORED_LOGO']['VALUE'] !== 'Y' ? '' : ' colored');
?>
<div class="mobileheader-v1">
    <div class="burger pull-left">
        <?= CNext::showIconSvg("burger dark", SITE_TEMPLATE_PATH . "/images/svg/Burger_big_white.svg"); ?>
        <?= CNext::showIconSvg("close dark", SITE_TEMPLATE_PATH . "/images/svg/Close.svg"); ?>
    </div>
    <div class="logo-block pull-left">
        <div class="logo<?= $logoClass ?>">
            <a href="/"><img src="/logo_mobile.png" alt="Yardex.ru" title="Yardex.ru"></a>
        </div>
    </div>
    <div class="right-icons">
        <div  class = "mob-search">
            <form action = "">
                <input type = "text" class="rees46-modal-trigger input-search" value = "">
                <button class="top-btn inline-search-show twosmallfont mobile_search_top button-search" data-role="show_multisearch_tool"></button>
            </form>
        </div>
        <div class="pull-right">
            <a rel="nofollow"
               class="basket-link basket <?= ($arBasketPrices['BASKET_COUNT'] ? 'basket-count' : ''); ?>"
               href="<?= $arTheme['BASKET_PAGE_URL']['VALUE']; ?>"
               title="Перейти в корзину">
							<span class="js-basket-block">
                                <span class="js-basket-block_icon_block">
                                    <img src="/images/icons/shoping_cart.png"/>
                                </span>
								<span class="count"><?= $arBasketPrices['BASKET_COUNT']; ?></span>
							</span>
            </a>
        </div>
    </div>
</div>