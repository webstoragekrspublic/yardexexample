<?
global $arBackParametrs;
$phone = $arBackParametrs['HEADER_PHONES_array_PHONE_VALUE_0'];
?>
<?
if (!isset($_REQUEST['getHeader'])) {
    CNext::checkRestartBuffer();
} ?>

<? IncludeTemplateLangFile(__FILE__); ?>
<? if (!$isIndex): ?>
    <? if ($isHideLeftBlock && !$isWidePage): ?>
        </div> <? // .maxwidth-theme?>
    <? endif; ?>
    </div> <? // .container?>
<? else: ?>
    <? CNext::ShowPageType('indexblocks'); ?>
<? endif; ?>
<? CNext::get_banners_position('CONTENT_BOTTOM'); ?>
</div> <? // .middle?>
<? //if(!$isHideLeftBlock && !$isBlog):?>
<? if (($isIndex && $isShowIndexLeftBlock) || (!$isIndex && !$isHideLeftBlock) && !$isBlog): ?>
    </div> <? // .right_block?>
    <? if ($APPLICATION->GetProperty("HIDE_LEFT_BLOCK") != "Y" && !defined("ERROR_404")): ?>
        <div class="left_block">
            <? CNext::ShowPageType('left_block'); ?>
        </div>
    <? endif; ?>
<? endif; ?>
<? if ($isIndex): ?>
    </div>
<? elseif (!$isWidePage): ?>
    </div> <? // .wrapper_inner?>
<? endif; ?>
</div> <? // #content?>
<? CNext::get_banners_position('FOOTER'); ?>
</div><? // .wrapper?>
<footer id="footer">
    <? if ($APPLICATION->GetCurPage(false) === '/' || $GLOBALS['404_page_slider'] == true): ?>
        <div class="maxwidth-theme podborki-ajax-sliders">
            <div id="c6a704449ba05fb988e1503989847409" class="rees46-recommend-custom"
             data-rees46-component="bitrix:catalog.top"
             data-rees46-template="products_slider"></div>
        </div>
    <?
    endif;
    ?>
    <? if (!($APPLICATION->GetProperty("block_comp_viewed")) and ($APPLICATION->GetProperty("viewed_show") == "Y" || $is404)): ?>
        <? /*$APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "basket",
            array(
                "COMPONENT_TEMPLATE" => "basket",
                "PATH" => SITE_DIR . "include/footer/comp_viewed.php",
                "AREA_FILE_SHOW" => "file",
                "AREA_FILE_SUFFIX" => "",
                "AREA_FILE_RECURSIVE" => "Y",
                "EDIT_TEMPLATE" => "standard.php",
                "PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "STORES" => array(
                    0 => "",
                    1 => "",
                ),
                "BIG_DATA_RCM_TYPE" => "bestsell"
            ),
            false
        ); 
         * 
         */?>
    <? endif; ?>

    <div class="footer_inner fill footer-light ext_view">
        <div class="bottom_wrapper white_backgroud">
            <div class="wrapper_inner white_backgroud">
                <div class="row bottom-middle">
                    <div class="col-xs-12">
                    <div class="footer_messengers_link">
                        <? $APPLICATION->IncludeComponent(
                            "custom:empty_component",
                            "messengers_links",
                            array(
                                "CACHE_TIME" => "360000",
                                "CACHE_TYPE" => "A"
                            )
                        ); ?>
                    </div>
                        <div class="row">
                            <div class="col-xs-12 visible-xs">
                                <? $APPLICATION->IncludeComponent("bitrix:menu", "footer_mobile", array(
                                    "ROOT_MENU_TYPE" => "bottom_company",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "3600000",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "CACHE_SELECTED_ITEMS" => "N",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MAX_LEVEL" => "1",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y"
                                ),
                                    false
                                ); ?>
                            </div>
                            <div class="col-xs-12 visible-xs">
                                <? $APPLICATION->IncludeComponent("bitrix:menu", "footer_mobile", array(
                                    "ROOT_MENU_TYPE" => "bottom_info",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "3600000",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "CACHE_SELECTED_ITEMS" => "N",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y"
                                ),
                                    false
                                ); ?>
                            </div>
                            <div class="col-xs-12 visible-xs">
                                <? $APPLICATION->IncludeComponent("bitrix:menu", "footer_mobile", array(
                                    "ROOT_MENU_TYPE" => "bottom_help",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "3600000",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "CACHE_SELECTED_ITEMS" => "N",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y"
                                ),
                                    false
                                ); ?>
                            </div>
                            <div class="col-xs-12 visible-xs">
                                <? $APPLICATION->IncludeComponent(
                                    "custom:form.feedback_rees46",
                                    "in_menu_footer_right",
                                    array(
                                        "COMPONENT_TEMPLATE" => "in_menu_footer_right",
                                        "TEXT_SUCCESS" => "вы подписаны",
                                        "TEXT_ERROR" => "произошла ошибка попробуйте подписаться попозже",
                                        "TEXT_SLOGAN" => "Лучшие предложения у вас в почте",
                                        "TEXT_BTN" => "Подписаться"
                                    ),
                                    false
                                );?>
                            </div>


                            <div class="col-xs-12 col-sm-3">
                                <img class="footer_top_item hidden-xs" src="/logo.png"/>
                                <a class="footer_common_item a_standart_color" rel="nofollow"
                                   href="tel:+<?= preg_replace("/[^\d]+/", "", $phone); ?>">
                                    <?= $phone; ?>
                                </a>
                                <div class="footer_common_item">
                                    г. Красноярск, ул. Рязанская, 65г
                                </div>

                                <div class="footer_common_item">
                                    <? $APPLICATION->IncludeFile(SITE_DIR . "include/footer/site-email.php", array(), array(
                                            "MODE" => "html",
                                            "NAME" => "Address",
                                            "TEMPLATE" => "include_area.php",
                                        )
                                    ); ?>
                                </div>
                                <div class="footer_socials_left">
                                    <? $APPLICATION->IncludeComponent(
                                        "aspro:social.info.next",
                                        ".default",
                                        array(
                                            "CACHE_TYPE" => "A",
                                            "CACHE_TIME" => "3600000",
                                            "CACHE_GROUPS" => "N",
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "SOCIAL_TITLE" => ''
                                        ),
                                        false
                                    ); ?>
                                </div>
                                <div class="footer_common_item footer_mobile_app_links">
                                <a href="https://apps.apple.com/ru/app/com.yardex.mobile-app/id1555057094"><img width="150"  height="49" alt="Ярбокс AppStore" title="Приложение Ярбокс в AppStore" src="/upload/medialibrary/0ba/0ba84a6cff05d7eb60fc88984bccec13.png" title="app (1).png"></a>
                                <a href="https://play.google.com/store/apps/details?id=com.yardex.mobile_app"><img width="150"  height="49" alt="Ярбокс GooglePlay" title="Приложение Ярбокс в GooglePlay" src="/upload/medialibrary/c93/c9320e3610efb611153cf765583619f3.png" title="google2.png"></a>
                                </div>
                            </div>
                            <div class="col-sm-3 hidden-xs">
                                <? $APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
                                    "ROOT_MENU_TYPE" => "bottom_company",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "3600000",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "CACHE_SELECTED_ITEMS" => "N",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MAX_LEVEL" => "1",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y"
                                ),
                                    false
                                ); ?>
                            </div>
                            <div class="col-sm-3 hidden-xs">
                                <? $APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
                                    "ROOT_MENU_TYPE" => "bottom_info",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "3600000",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "CACHE_SELECTED_ITEMS" => "N",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y"
                                ),
                                    false
                                ); ?>
                            </div>
                            <div class="col-sm-3 hidden-xs">
                                <? $APPLICATION->IncludeComponent("bitrix:menu", "footer", array(
                                    "ROOT_MENU_TYPE" => "bottom_help",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_TIME" => "3600000",
                                    "MENU_CACHE_USE_GROUPS" => "N",
                                    "CACHE_SELECTED_ITEMS" => "N",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "left",
                                    "USE_EXT" => "N",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y"
                                ),
                                    false
                                ); ?>
                                <? $APPLICATION->IncludeComponent(
                                    "custom:form.feedback_rees46",
                                    "in_menu_footer_right",
                                    array(
                                        "COMPONENT_TEMPLATE" => "in_menu_footer_right",
                                        "TEXT_SUCCESS" => "вы подписаны",
                                        "TEXT_ERROR" => "произошла ошибка попробуйте подписаться попозже",
                                        "TEXT_SLOGAN" => "Лучшие предложения у вас в почте",
                                        "TEXT_BTN" => "Подписаться"
                                    ),
                                    false
                                );?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-under">
                    <div class="row">
                        <div class="col-md-12 outer-wrapper">
                            <div class="inner-wrapper row">
                                <div class="copy-block">
                                    <div class="copy">
                                        <? $APPLICATION->IncludeFile(SITE_DIR . "include/footer/copy/copyright.php", array(), array(
                                                "MODE" => "php",
                                                "NAME" => "Copyright",
                                                "TEMPLATE" => "include_area.php",
                                            )
                                        ); ?>
                                    </div>
                                    <div class="print-block"><?= CNext::ShowPrintLink(); ?></div>
                                    <div id="bx-composite-banner"></div>
                                </div>
                                <div class="pull-right pay_system_icons">
								<span class="">
									<? $APPLICATION->IncludeFile(SITE_DIR . "include/footer/copy/pay_system_icons.php", array(), array("MODE" => "html", "NAME" => GetMessage("PHONE"), "TEMPLATE" => "include_area.php",)); ?>
								</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer>
<!--<div class="mobile_white_shadow visible-xs"></div>-->
<div class="bx_areas">
    <? CNext::ShowPageType('bottom_counter'); ?>
</div>

<? $APPLICATION->IncludeComponent(
    "custom:empty_component",
    "fancybox_modals",
    ['MOBILE_VERSION' => 'Y'],
    false
);
?>

<? $APPLICATION->IncludeComponent(
    "custom:empty_component",
    "custom_multisearch",
    ['MOBILE_VERSION' => 'Y'],
    false
);
?>

<? $APPLICATION->IncludeComponent(
    "custom:empty_component",
    "rees46_search",
    ['MOBILE_VERSION' => 'Y'],
    false
);
?>

<? $APPLICATION->IncludeComponent(
    "custom:empty_component",
    "search_title",
    ['MOBILE_VERSION' => 'Y'],
    false
);
?>

<? $APPLICATION->IncludeComponent(
    "custom:empty_component",
    "cookie_aprove",
    [],
    false
);
?>

<? CNext::setFooterTitle();
CNext::showFooterBasket(); ?>
</body>
</html>