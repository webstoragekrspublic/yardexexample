function addDatePicker(){
    var dateElement = document.querySelector('input[name="PROPERTIES[20]"]');
    dateElement.setAttribute('placeholder', 'Нажмите чтобы выбрать дату');
    dateElement.setAttribute('readonly','');
    dateElement.style.opacity = 1;
    $(document).on('click', function (event){
        if (event.target == document.querySelector('input[name="PROPERTIES[20]"]')) {
            BX.calendar({
                node: dateElement,
                field: dateElement,
                bTime: false
            });
        }
    });
}

document.addEventListener('DOMContentLoaded', function () {
    addDatePicker();
});
