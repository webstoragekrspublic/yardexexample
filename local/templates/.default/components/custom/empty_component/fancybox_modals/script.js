function showSuggestedAddressFancybox() {
    $.fancybox({
        href: '#ymap_address_suggestion',
        // scrolling: 'no',
        afterShow: function () {
            $('[data-role="suggested_address_input"]').focus()
        }
    });
}

function modalSuccess(description, header) {
    if (header === undefined) {
        header = 'Успех!';
    }

    if (description === undefined) {
        description = '';
    }

    $('#modal_success_h').html(header);
    $('#modal_success_description').html(description);

    $.fancybox({
        href: '#modal_success',
        scrolling: 'no',
    });
}

function modalError(description, header) {
    if (header === undefined) {
        header = 'Ошибка!';
    }

    if (description === undefined) {
        description = '';
    }

    $('#modal_error_h').html(header);
    $('#modal_error_description').html(description);

    $.fancybox({
        href: '#modal_error',
        scrolling: 'no',
    });
}

//модалка временных слотов
$(document).ready(function () {
    var $timeElement = $('input[name="PROPERTIES[21]"]');
    if ($timeElement.length) {
        $timeElement.attr('placeholder', 'Нажмите чтобы выбрать время');
        $timeElement.attr('readonly', '');
        $timeElement.css('opacity', 1);

        $timeElement.on('click', function (e) {
            $.fancybox({
                href: '#time-slots-in-admin-container',
                margin: 0,
                padding: 20,
                autoSize: false,
                width: 620,
                transitionIn: 'none',
                transitionOut: 'none',
                helpers: {
                    overlay: {
                        locked: false
                    }
                },
            });
        });
    }

    $('[data-role="popup_timeslots_day"]').click(function (e) {
        $('[data-role="popup_timeslots_day"]').removeClass('active');
        $('[data-role="popup_timeslots_slots_block"]').removeClass('active');
        $(this).addClass('active');
        var dayIndex = $(this).data('index');

        var $showBlock = $('[data-role="popup_timeslots_slots_block"][data-day_index="' + dayIndex + '"]');
        $showBlock.addClass('active');
        $('[data-role="popup_timeslots_slots"]').removeClass('active');
    })

    $('[data-role="popup_timeslots_slots"]').click(function (e) {
        $('[data-role="popup_timeslots_slots"]').removeClass('active');
        $(this).addClass('active');
    })

    $('[data-role="popup_timeslots_slots_choose_btn"]').click(function (e) {
        var $inputDeliveryDate = $('input[name="PROPERTIES[20]"]').first();
        var $inputDeliverySlot = $('input[name="PROPERTIES[21]"]').first();
        var $activeSlot = $('[data-role="popup_timeslots_slots"].active').first();
        var $activeDay = $('[data-role="popup_timeslots_day"].active').first();
        if (!$activeSlot.length) {
            alert('выберите слот!')
            return
        }
        if (!$activeDay.length) {
            alert('выберите день!')
            return
        }
        if (!$inputDeliveryDate.length || !$inputDeliverySlot.length) {
            alert('на странице нет нужных форм для заполнения!')
            return
        }

        $inputDeliveryDate.val($activeDay.data('delivery_date'));
        $inputDeliverySlot.val($activeSlot.data('slot'));
        $.fancybox.close();

    })

});
