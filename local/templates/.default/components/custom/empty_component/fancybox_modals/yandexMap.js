CustomLocationChangeYandex = {
    addressesSuggested: [],
    selectedAddressIndex: 0,
    $parentBlock: false,
    timer: false,
    minlength: 3,
    $addressInput: false,
    suggestBoundedBy: [[55.898952, 92.491655], [56.294369, 93.717311]],

    addressFieldInfo: {
        name: 'Адрес доставки:',
        selector: 'textarea[name="PROPERTIES[7]"]'
    },

    geoCoordsFieldInfo: {
        name: 'ГеоКоординаты:',
        selector: 'input[name="PROPERTIES[38]"]'
    },

    geoKladrFieldInfo: {
        name: 'Гео Данные:',
        selector: 'input[name="PROPERTIES[26]"]'
    },

    init: function () {
        var self = this;

        self.initKeyDownEvents();
        self.initClickEvents();
        self.initReadOnly();

        $(document).on('input propertychange', '[data-role="suggested_address_input"]', function () {
            self.$addressInput = $(this);
            self.$parentBlock = self.$addressInput.parent();
            $('#choose_ymap_address').attr('disabled', 'disabled');

            var query = self.$addressInput.val();
            self.ajaxLocationCall(query);
        });

        $(document).on('click', self.geoCoordsFieldInfo.selector + ',' + self.geoKladrFieldInfo.selector + ',' + self.addressFieldInfo.selector, function () {
            $(this).attr('readonly','readonly');
            self.setPopupInitValues();
            showSuggestedAddressFancybox()
        });

        $(document).ready(function () {
            if (!self.$addressInput) {
                self.$addressInput = $('[data-role="suggested_address_input"]').first();
            }
        });
    },

    initYandexMapEvents: function () {
        var self = this;
        yandexMap.events.add('click', function (e) {
            var coords = e.get('coords');
            self.mapClearObjects();
            self.geocode(coords);
        });
        $('#choose_ymap_address').off('click').on('click', function () {
            self.setAddressFormFields();
            $.fancybox.close();
        });
    },

    setPopupInitValues: function () {
        var self = this;
        var geoCoords = $(self.geoCoordsFieldInfo.selector).val();
        $('#ymap_address_suggestion_geo_coords').val(geoCoords);

        var address = $(self.addressFieldInfo.selector).val();
        var appartament = '';
        var addressSplit = address.split('кв.');
        if (addressSplit.length > 1) {
            appartament = addressSplit[addressSplit.length - 1].trim()
            addressSplit.pop();
            address = addressSplit.join('кв.').trim();
        }

        $('[data-role="suggested_address_input"]').val(address);
        $('#ymap_address_suggestion_address_apartament_input').val(appartament);
    },

    setAddressFormFields: function () {
        var self = this;
        var fullAddress = $('[data-role="suggested_address_input"]').val().trim();
        var apartament = $('#ymap_address_suggestion_address_apartament_input').val().trim();
        if (apartament) {
            fullAddress += ' кв. ' + apartament;
        }
        $(self.addressFieldInfo.selector).val(fullAddress);

        var geoCoords = $('#ymap_address_suggestion_geo_coords').val().trim();
        $(self.geoCoordsFieldInfo.selector).val(geoCoords);
        $(self.geoKladrFieldInfo.selector).val('');
    },

    ajaxLocationCall: function (query) {
        var self = this;

        if (self.timer) {
            clearTimeout(self.timer);
        }

        if (query.length >= self.minlength) {
            self.timer = setTimeout(function () {
                ymaps.suggest(query,
                    {
                        boundedBy: self.suggestBoundedBy
                    }
                ).then(function (items) {
                    self.addressesSuggestionsYandex(items);

                    if (!items.length) {
                        var error = 'Адрес не найден, введите адрес без квартиры.';
                        var hint = '';
                        self.showError({error: error, hint: hint})
                    }

                    self.renderAddressDropdown();
                });
            }, 200);
        } else {
            self.addressesSuggested = [];
            self.renderAddressDropdown();
        }
    },

    addressesSuggestionsYandex: function (addressesSuggestion) {
        var self = this;
        if (addressesSuggestion.length > 0) {
            for (var k = 0; k < addressesSuggestion.length; k++) {
                addressesSuggestion[k]['yandexValue'] = addressesSuggestion[k]['value'];
                addressesSuggestion[k]['value'] = addressesSuggestion[k]['displayName'];
            }
            this.addressesSuggested = addressesSuggestion;
        } else {
            this.addressesSuggested = [];
        }
    },

    initKeyDownEvents: function () {
        var self = this;
        $(document).keydown(function (e) {
            if (self.isDropMenuInit()) {
                switch (e.which) {
                    case 38: // up
                        self.menuMoveUp();
                        e.preventDefault();
                        break;

                    case 40: // down
                        self.menuMoveDown();
                        e.preventDefault();
                        break;

                    case 13: // enter
                        self.menuSelect(self.selectedAddressIndex);
                        e.preventDefault();
                        break;
                }
            }
        });
    },

    initReadOnly: function (){
        var self = this;
        document.addEventListener('DOMContentLoaded', function (){
            $(this).find(self.geoCoordsFieldInfo.selector).attr('readonly','readonly');
            $(this).find(self.geoKladrFieldInfo.selector).attr('readonly','readonly');
            $(this).find(self.addressFieldInfo.selector).attr('readonly','readonly');
        });
    },

    initClickEvents: function () {
        var self = this;
        document.addEventListener('click', function (e) {
            var dropdown = document.querySelector('[data-role="address_dropdown"]');
            if (dropdown && !dropdown.contains(e.target)) {
                dropdown.remove();
            }
        });
    },

    isDropMenuInit: function () {
        return this.addressesSuggested.length;
    },

    menuMoveUp: function () {
        if (this.isDropMenuInit() && this.selectedAddressIndex > 0) {
            this.selectedAddressIndex--;
            this.reSelectRow();
        }
    },

    menuMoveDown: function () {
        if (this.isDropMenuInit() && this.selectedAddressIndex < (this.addressesSuggested.length - 1)) {
            this.selectedAddressIndex++;
            this.reSelectRow();
        }
    },

    menuSelect: function (index) {
        var self = this;
        var address = this.addressesSuggested[index]['value'];

        self.geocode(address)
        this.addressesSuggested = [];
        this.renderAddressDropdown();
    },

    reSelectRow: function () {
        this.$parentBlock.find('[data-role="address_dropdown_item"]').removeClass('selected');
        this.$parentBlock.find('[data-role="address_dropdown_item"][data-row_index="' + this.selectedAddressIndex + '"]').addClass('selected');
    },

    renderAddressDropdown: function () {
        this.$parentBlock.find('[data-role="address_dropdown"]').remove();
        if (this.isDropMenuInit()) {
            var html = '<div class="address_dropdown" data-role="address_dropdown">';
            for (var k = 0; k < this.addressesSuggested.length; k++) {
                var addressSuggested = this.addressesSuggested[k];
                var addClass = '';
                if (k == 0) {
                    addClass = 'selected';
                    this.selectedAddressIndex = k;
                }

                var addressText = addressSuggested['value'];
                if (typeof addressSuggested.hl != 'undefined' && addressSuggested.hl.length > 0 && Array.isArray(addressSuggested.hl)) {
                    var offset = 0;
                    for (var j = 0; j < addressSuggested.hl.length; j++) {
                        var hl = addressSuggested.hl[j];

                        if (hl.length == 2) {
                            addressText = addressText.substring(0, hl[1] + offset) + '</b>' + addressText.substring(hl[1] + offset);
                            addressText = addressText.substring(0, hl[0] + offset) + '<b>' + addressText.substring(hl[0] + offset);
                            offset += '<b></b>'.length;
                        }

                    }
                }
                html += '<div class="address_dropdown-item ' + addClass + '" data-row_index="' + k + '" data-role="address_dropdown_item">' + addressText + '</div>';

            }
            html += '</div>';
            this.$parentBlock.append(html);
            this.initMenuItemClick();
        }
    },

    initMenuItemClick: function () {
        var self = this;
        self.$parentBlock.find('[data-role="address_dropdown_item"]').off("click").on('click', function () {
            var clickedRowIndex = $(this).attr('data-row_index');
            self.menuSelect(clickedRowIndex);
        });
    },

    showError: function (message) {
        $('#ymap_address_suggestion_error').html(message.error);
        $('#ymap_address_suggestion_hint').html(message.hint);
    },

    clearErrors: function () {
        $('#ymap_address_suggestion_error').html('');
        $('#ymap_address_suggestion_hint').html('');
    },

    chooseAddress: function (geoApiObj) {
        this.mapAddChosenAddress(geoApiObj.geometry.getCoordinates());
        this.mapNewCenter(geoApiObj.geometry.getCoordinates());
        $('#ymap_address_suggestion_geo_coords').val(geoApiObj.geometry.getCoordinates().join(','));
        $('#choose_ymap_address').removeAttr('disabled');
    },

    mapAddChosenAddress: function (coords) {
        var placemark = new ymaps.Placemark(coords);
        this.mapClearObjects();
        yandexMap.geoObjects.add(placemark);
    },

    mapClearObjects: function () {
        yandexMap.geoObjects.removeAll();
    },

    mapNewCenter: function (coords) {
        var zoom = 17;
        yandexMap.setCenter(coords, zoom);
    },

    geocode: function (address) {
        var self = this;
        ymaps.geocode(address).then(function (res) {
            var obj = res.geoObjects.get(0),
                error, hint;

            if (obj) {
                switch (obj.properties.get('metaDataProperty.GeocoderMetaData.precision')) {
                    case 'exact':
                        break;
                    case 'number':
                    case 'near':
                    case 'range':
                        error = 'Неточный адрес, требуется уточнение';
                        hint = 'Уточните номер дома';
                        break;
                    case 'street':
                        error = 'Неполный адрес, требуется уточнение';
                        hint = 'Уточните номер дома';
                        break;
                    case 'other':
                    default:
                        error = 'Неточный адрес, требуется уточнение';
                        hint = 'Уточните адрес';
                }
                var shortAddress = obj.properties.getAll().name;
                self.$addressInput.val(shortAddress);
            } else {
                error = 'Адрес не найден, проверьте адрес или выберите дом на карте.';
                hint = '';
            }

            if (error) {
                self.showError({error: error, hint: hint});
            } else {
                self.clearErrors();
                self.chooseAddress(obj);
            }
        }, function (e) {
            console.log(e)
        })

    },
}

CustomLocationChangeYandex.init();