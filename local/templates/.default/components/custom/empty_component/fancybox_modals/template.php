<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
//Шаблон создан чисто для вывода в админскую часть


/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$q = htmlspecialchars($request->get('q'));
$this->setFrameMode(true);
$userPhone = '';
if ($USER->IsAuthorized()) {
    $by = 'id';
    $order = 'asc';
    $dbRes = CUser::GetList($by, $order, array("ID" => $USER->GetID()), array("FIELDS" => array("ID", "PERSONAL_PHONE")));
    $arUser = $dbRes->Fetch();
    $userPhone = $arUser['PERSONAL_PHONE'];
}
if (\Bitrix\Main\Loader::includeModule('custom.deliverytime')) {
    $deliveryDays = \Custom\DeliveryTime\DeliveryDateAvailable::getNextDaysSetting(7);

    function getUsedSlotsCnt($date_dmY, $slot)
    {
        $usedSlots = \Custom\DeliveryTime\DeliveryTime::getInstance()->getUsedSlots();
        return $usedSlots[$date_dmY][$slot] ?? 0;
    }
}
$allTimeSlotsInWeek = [];

?>

    <div style="display: none;" id="modal_success" class="col-sm-12">
        <h2 id="modal_success_h" class="modal_header">Успех</h2>
        <p id="modal_success_description" class="modal_description"></p>
    </div>

    <div style="display: none;" id="modal_error" class="col-sm-12">
        <h2 id="modal_error_h" class="modal_header">Ошибка</h2>
        <p id="modal_error_description" class="modal_description"></p>
    </div>

    <div style="display: none;" class="ymap_address_suggestion" id="ymap_address_suggestion">
        <h2 class="modal_header">Введите адрес доставки</h2>
        <div class="ymap_address_suggestion_address_input_block">
            <div class="ymap_address_suggestion_address_input_block_suggestion">
                <div class="ymap_address_suggestion_address_input_block_suggestion_info">
                    <input type="text" placeholder="Введите адрес" class="ymap_address_suggestion_address_input"
                           data-role="suggested_address_input">
                    <input type="hidden" id="ymap_address_suggestion_geo_coords" value="">
                </div>
                <div class="ymap_address_suggestion_address_input_block_appartament_info">
                    <label for="ymap_address_suggestion_address_apartament_input">Номер квартиры:</label>
                    <input type="text" placeholder="" class="ymap_address_suggestion_address_apartament_input"
                           id="ymap_address_suggestion_address_apartament_input">
                </div>
                <div class="ymap_address_suggestion_error" id="ymap_address_suggestion_error"></div>
                <div class="ymap_address_suggestion_hint" id="ymap_address_suggestion_hint"></div>
            </div>
            <button class="ymap_address_suggestion_address_btn btn btn-lg btn-default" disabled
                    id="choose_ymap_address">Выбрать
            </button>
        </div>
        <div class="ymap_address_suggestion_ymap_description">или выберите дом на карте</div>
        <div id="ymap"></div>
    </div>

    <div class="time-slots-in-admin-container"
         style="display:none;"
         id="time-slots-in-admin-container">
        <? $firstDayIndex = array_key_first($deliveryDays); ?>
        <div class="time-slots-in-admin-day_block">
            <? foreach ($deliveryDays as $dayIndex => $day):
                $addClass = '';
                $deliveryDate = '';
                if ($firstDayIndex == $dayIndex) {
                    $addClass = 'active';
                }
                $dayDate = '';
                if ($dayDT = $day->getStartDateTime()) {
                    $dayDate = $dayDT->format('d.m');
                    $deliveryDate = $dayDT->format('d.m.Y');
                }
                ?>
                <div class="time-slots-in-admin-day <?= $addClass ?>"
                     data-role="popup_timeslots_day"
                     data-index="<?= $dayIndex; ?>"
                     data-delivery_date="<?= $deliveryDate; ?>">
                    <p><?= $day->getName(); ?></p>
                    <p><?= $dayDate; ?></p>
                </div>
            <? endforeach; ?>
        </div>
        <? foreach ($deliveryDays as $dayIndex => $day):
            $addClass = '';
            if ($firstDayIndex == $dayIndex) {
                $addClass = 'active';
            }
            ?>

            <div class="time-slots-in-admin-slot_block <?= $addClass; ?>"
                 data-role="popup_timeslots_slots_block"
                 data-day_index="<?= $dayIndex ?>"
            >
                <? foreach ($day->getTimeSlots() as $timeSlot) :
                    $usedCntText = '';
                    $addClass = '';
                    if ($timeSlot->isDisabled()) {
                        $addClass = 'disabled';
                    }

                    if ($dayDT = $day->getStartDateTime()) {
                        if ($usedCnt = getUsedSlotsCnt($dayDT->format('d.m.Y'), $timeSlot->getName())) {
                            $usedCntText = 'Слот использован раз: ' . $usedCnt;
                        } else {
                            $usedCntText = 'Слот полностью свободен';
                        }
                    }
                    ?>
                    <div class="time-slots-in-admin-slot <?= $addClass; ?>"
                         data-role="popup_timeslots_slots"
                         data-slot="<?= htmlspecialchars($timeSlot->getName()); ?>">
                        <p><?= $timeSlot->getName(); ?></p>
                        <p><?= $usedCntText; ?></p>
                    </div>
                <? endforeach; ?>
            </div>
        <? endforeach; ?>
        <button data-role="popup_timeslots_slots_choose_btn">
            Установить выбранный слот и дату
        </button>
        <div data-role="popup_timeslots_slots_alert" class="time-slots-in-admin-slot_alert"></div>
    </div>