var YandexRouting = {
    apiUrl: '/local/templates/.default/components/custom/empty_component/yandex_routing/api',

    showOrdersForRouting: function ($btn) {
        var self = this;
        $.ajax({
            url: self.apiUrl + '/getOrders.php',
            type: 'GET',
            dataType: 'json',
            beforeSend: function (xhr) {
                self.clearPopupContent()
                $($btn).attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data.error) {
                    modalError(data.error);
                } else {
                    self.showOrdersPopup(data.answer.orders, data.answer.couriers, data.answer.date)
                }
                $($btn).removeAttr('disabled');
            },
            error: function () {
                modalError('Произошла ошибка, попробуйте повторить действие позже.');
                $($btn).removeAttr('disabled');
            },
        });
    },

    clearPopupContent: function () {
        $('#yandeRoutingOrdersPopup .yandeRoutingDeliveryDate').html('');
        $('#yandeRoutingOrdersPopup .yandeRoutingOrdersList').html('');
        $('#yandeRoutingOrdersPopup .yandeRoutingCouriersList').html('');
        $('#yandeRoutingOrdersPopup .yandeRoutingSendedOrdersList').html('');
    },

    showOrdersPopup: function (orders, couriers, deliveryDate) {
        $('#yandeRoutingOrdersPopup .yandeRoutingDeliveryDate').html(deliveryDate);

        var ordersHtml = '';
        var ordersSendedHtml = '';
        if (orders.length > 0) {
            for (var k = 0; k < orders.length; k++) {
                var order = orders[k];

                var orderHtml = '<li>';
                if (!order.ERRORS.length) {
                    var data_slot_type = 'normal';
                    if (order.WARNINGS.length > 0 || order.YANDEX_ROUTING_SENDED) {
                        data_slot_type = 'no_auto_check';
                    }
                    var slot_time = this.escapeHtml(order.SALE_INTERNALS_ORDER_PROPERTY_DELIVERY_TIME_VALUE);
                    orderHtml += `<input data-slot_type="${data_slot_type}"
                                         data-slot_time="${slot_time}"
                                         type="checkbox" name="order_${order.ID}" />`
                }

                var orderName = `<a target="_blank" href="/bitrix/admin/sale_order_view.php?ID=${order.ID}">${order.ID}</a>`;
                orderHtml += ` ${orderName} (${order.SALE_INTERNALS_ORDER_PROPERTY_DELIVERY_TIME_VALUE}) - ${order.USER_NAME}`;


                orderHtml += `<br> Адрес доставки: ${order.SALE_INTERNALS_ORDER_PROPERTY_ADDRESS_VALUE}`;
                if (
                    order.YANDEX_ADDRESS_INFO &&
                    order.YANDEX_ADDRESS_INFO.GeoObject &&
                    order.YANDEX_ADDRESS_INFO.GeoObject.metaDataProperty &&
                    order.YANDEX_ADDRESS_INFO.GeoObject.metaDataProperty.GeocoderMetaData
                ) {
                    orderHtml += `<br> <span class="warning">Адрес от Яндекса:</span> ${order.YANDEX_ADDRESS_INFO.GeoObject.metaDataProperty.GeocoderMetaData.text}`;
                }
                if (order.ERRORS.length > 0) {
                    var errorsStr = order.ERRORS.join(', ');
                    orderHtml += `<br><span class="error">${errorsStr}</span>`
                }

                if (order.WARNINGS.length > 0) {
                    var warningStr = order.WARNINGS.join(', ');
                    orderHtml += `<br><span class="warning">${warningStr}</span>`
                }
                if (order.YANDEX_ROUTING_SENDED) {
                    orderHtml += `<br><span>Id маршрутизации Яндекса: ${order.SALE_INTERNALS_ORDER_PROPERTY_ROUTING_API_CODE_VALUE}</span>`
                }

                orderHtml += '</li>';
                if (order.YANDEX_ROUTING_SENDED) {
                    ordersSendedHtml += orderHtml
                } else {
                    ordersHtml += orderHtml;
                }
            }
        } else {
            ordersHtml += `<li>Нет заказов!</li>`
        }
        $('#yandeRoutingOrdersPopup .yandeRoutingOrdersList').html(ordersHtml);
        if (ordersSendedHtml == '') {
            ordersSendedHtml = 'Нет отправленных заказов на маршрутизацию!';
        }
        $('#yandeRoutingOrdersPopup .yandeRoutingSendedOrdersList').html(ordersSendedHtml);

        var couriersHtml = ''
        if (couriers) {
            for (var k = 0; k < couriers.length; k++) {
                var courier = couriers[k];
                couriersHtml += `
                        <li>
                            <input type="checkbox" name="courier_${courier.id}" checked='checked'/>
                            ${courier.number} - ${courier.name}
                        </li>`
            }
        } else {
            couriersHtml += `<li>Нет курьеров!!!</li>`
        }
        $('#yandeRoutingOrdersPopup .yandeRoutingCouriersList').html(couriersHtml);


        $.fancybox({
            href: '#yandeRoutingOrdersPopup',
        });
    },

    sendOrdersToYandex: function ($form) {
        var self = this;
        $.ajax({
            url: self.apiUrl + '/sendToYandex.php',
            data: $($form).serialize(),
            type: 'POST',
            dataType: 'json',
            beforeSend: function (xhr) {
                $($form).find('input, button').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data.error) {
                    modalError(data.error);
                } else {
                    modalSuccess(data.answer)
                }
                $($form).find('input, button').removeAttr('disabled');
            },
            error: function () {
                modalError('Произошла ошибка, попробуйте повторить действие позже.');
                $($form).find('input, button').removeAttr('disabled');
            },
        });
    },

    escapeHtml: function (text) {
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    }
}
