<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
?>

<div style="display: none;" class="yandeRoutingOrdersPopup" id="yandeRoutingOrdersPopup">
    <p>
        Заказы для отправки в яндекс маршрутизацию <span class="yandeRoutingDeliveryDate"></span>
        <button onclick="YandexRouting.showOrdersForRouting(this);return false;" class="adm-btn">обновить заказы
        </button>
    </p>

    <form class="yandeRoutingOrdersForm">
        <div class="yandeRoutingUpBlock">
            <div>
                <h4>Курьеры:</h4>
                <ul class="yandeRoutingCouriersList">

                </ul>
            </div>
            <div>
                <h4>отметить Слоты</h4>
                <ul class="yandeRoutingSlotsList">
                    <?
                    $races = [
                        [
                            'name' => 'рейс 1 (любое время - 11:00-15:00)',
                            'slots' => ['любое время', '11:00-13:00', '13:00-15:00']
                        ],
                        [
                            'name' => 'рейс 2 (любое время - 15:00-19:00)',
                            'slots' => ['любое время', '15:00-17:00', '17:00-19:00']
                        ],
                        [
                            'name' => 'рейс 3 (любое время - 19:00-23:00)',
                            'slots' => ['любое время', '19:00-21:00', '21:00-23:00']
                        ],
                    ]
                    ?>
                    <? foreach ($races as $race): ?>
                        <li data-role="yandeRoutingSlots" class="yandeRoutingSlotChoose"
                            data-slots="<?= htmlspecialchars(json_encode($race['slots'], JSON_UNESCAPED_UNICODE)); ?>">
                            <?= $race['name']; ?>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
        <h4>Список заказов:</h4>
        <ul class="yandeRoutingOrdersList">

        </ul>
        <h4>Список отправленных в яндекс заказов:</h4>
        <ul class="yandeRoutingSendedOrdersList">

        </ul>
        <input type="submit" value="Отправить в Яндекс!">
    </form>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        if ($('#tbl_sale_order_result_div .adm-list-table-top').length > 0) {
            var btnHtml = `
                <button class="adm-btn" onclick="YandexRouting.showOrdersForRouting(this);return false;">
                    Показать заказы для ЯндексМаршрутизации
                </button>
            `;
            $('#tbl_sale_order_result_div .adm-list-table-top').append(btnHtml)


            $('#yandeRoutingOrdersPopup .yandeRoutingOrdersForm').on('submit', function (e) {
                e.preventDefault()
                console.log(this);
                YandexRouting.sendOrdersToYandex(this);
            })

            $('#yandeRoutingOrdersPopup [data-role="yandeRoutingSlots"]').on('click', function () {
                var slots = $(this).data('slots');
                if (!Array.isArray(slots)) {
                    alert('Ошибка, слоты не массив!');
                    return false
                }

                $('.yandeRoutingOrdersList li input[type="checkbox"]').each(function(){
                    var slot_type = $(this).data('slot_type');
                    var slot_time = $(this).data('slot_time');
                    if (slot_type == 'normal' && slots.indexOf(slot_time) >= 0){
                        $(this).prop('checked', true);
                    } else {
                        $(this).prop('checked', false);
                    }
                })

            })

        }

    });

</script>
