<?php

use ExternalApi\YandexRouting\YandexRouting;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$answer = [
    'error' => 'произошла ошибка, попробуйте перезагрузить страницу',
    'answer' => []
];


if (\Helpers\SafetyValidation::isAdminOrManger(\CUser::GetID())) {
    $couriers = [];
    $orders = [];

    foreach ($_POST as $key => $value) {
        if (preg_match('/courier_(.+)/',$key,$matches)){
            $couriers[] = $matches[1];
        }
        if (preg_match('/order_(.+)/',$key,$matches)){
            $orders[] = $matches[1];
        }
    }

    
    writeAppendLog(array("orders" => $orders));
    writeAppendLog(array("couriers" => $couriers));
    if ($couriers && $orders) {
        $answer = YandexRouting::addOrders($orders,$couriers);
    } else {
        $answer['error'] = 'Не выбраны курьеры или заказы!';
    }
}

die(json_encode($answer, JSON_UNESCAPED_UNICODE));
