<?php

use ExternalApi\YandexRouting\YandexRouting;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$answer = [
    'error' => 'произошла ошибка, попробуйте перезагрузить страницу',
    'answer' => []
];


if (\Helpers\SafetyValidation::isAdminOrManger(\CUser::GetID())) {
    $todayDeliveryDate = YandexRouting::getTodayDeliveryDate();
//    $todayDeliveryDate = '27.09.2021';
//    $todayDeliveryDate = '14.09.2021';
    $answer['answer'] = [
        'date' => $todayDeliveryDate,
        'orders' => YandexRouting::getOrdersForDeliveryDate($todayDeliveryDate),
        'couriers' => YandexRouting::getVehicles($todayDeliveryDate)
    ];
    $answer['error'] = false;
}

die(json_encode($answer, JSON_UNESCAPED_UNICODE));
