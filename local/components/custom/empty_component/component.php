<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */

/** @global CMain $APPLICATION */

use Bitrix\Main\Context,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\Loader,
    Bitrix\Iblock;
if (isset($arParams['RETURN_HTML']) && $arParams['RETURN_HTML']){
    ob_start();
    $this->IncludeComponentTemplate();
    $out = ob_get_clean();
    return $out;
} else {
    $this->IncludeComponentTemplate();
}