<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arResult */

/** @var array $arParams */

use Bitrix\Main\Loader;

$arResult = [];

$TTL = $arParams['CACHE_TIME'];
$uniqueString = __FILE__;

$cache = Bitrix\Main\Data\Cache::createInstance();

$arResult = [];

if ($cache->initCache($TTL, $uniqueString)) {

    $arResult = $cache->getVars();

} elseif ($cache->startDataCache()) {

    $cache->endDataCache($arResult);

}