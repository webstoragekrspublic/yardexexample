<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("форма подписки REES46"),
    "DESCRIPTION" => GetMessage("Отправка почты подписчика в rees46"),
    "PATH" => array(
        "ID" => "custom_rees46_components",
        "CHILD" => array(
            "ID" => "форма подписки REES46",
            "NAME" => "форма подписки REES46"
        )
    ),
    "ICON" => "/images/icon.gif",
);
