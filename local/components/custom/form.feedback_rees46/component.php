<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult['TEXT_SUCCESS'] = $arParams["TEXT_SUCCESS"];
$arResult['TEXT_ERROR'] = $arParams["TEXT_ERROR"];
$arResult['TEXT_SLOGAN'] = $arParams["TEXT_SLOGAN"];
$arResult['TEXT_BTN'] = $arParams["TEXT_BTN"];
$this->IncludeComponentTemplate();
