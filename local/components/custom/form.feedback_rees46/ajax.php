<?php
//echo print_r($_POST);
$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.rees46.ru/import/audience',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
    "shop_id": "'.$_POST['shop_id'].'",
    "shop_secret": "'.$_POST['shop_secret'].'",
    "segment_id": "15697",
    "audience": '. json_encode($_POST['audience']).'
}',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
));

$response = curl_exec($curl);
curl_close($curl);
$json = json_decode($response);

if ($json->status == "success") echo '{"status": "success"}';
else echo '{"status": "error"}';