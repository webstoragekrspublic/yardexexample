<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
\Bitrix\Main\UI\Extension::load("ui.notification");

global $USER;
$email = '';
if ($USER->IsAuthorized()) $email = $USER->GetEmail();
?>
<script>
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    function ajaxForm(e) {
        BX.PreventDefault(e);
        var email = document.getElementById('staticEmail').value;
        if (!isEmail(email)) {
            BX.UI.Notification.Center.notify({
                content: "Почта указана не верно",
                autoHideDelay: 1500,
            });
            return;
        }
        BX.ajax({
            url: '/local/components/custom/form.feedback_rees46/ajax.php',
            data: {
                "shop_id": "60eec8bb4851421cbbcdac54907c00",
                "shop_secret": "1a7394f1a45a26b68d4f01751a2fee3b",
                "audience": [
                    {
                        "email": email
                    }
                ]
            },
            method: 'POST',
            dataType: 'json',
            /*timeout: 30,
            async: true,
            processData: true,
            scriptsRunFirst: true,
            emulateOnload: true,
            start: true,
            cache: false,*/
            onsuccess: function(data){
                console.log(data);
                if (data.status == 'success') {
                    BX.UI.Notification.Center.notify({
                        content: email + " <?=$arResult['TEXT_SUCCESS']?>",
                        autoHideDelay: 1500,
                    });
                } else {
                    BX.UI.Notification.Center.notify({
                        content: email + " <?=$arResult['TEXT_ERROR']?>",
                        autoHideDelay: 3000,
                    });
                }
            },
            onfailure: function(){
                BX.UI.Notification.Center.notify({
                    content: email + " <?=$arResult['TEXT_ERROR']?>",
                    autoHideDelay: 3000,
                });
            }
        });
    }
</script>

</script>

<form id="bx_custom_form" style="background-color: #7f858b;padding: 10px; color: #ffffff;margin-bottom: 10px;">
    <div class="form-group row">
        <label for="staticEmail" class="col-sm-12 col-form-label w-100"><?=$arResult['TEXT_SLOGAN']?></label>
        <div class="col-sm-12" style="padding-bottom: 5px;">
            <input type="text" class="form-control-plaintext" id="staticEmail" value="<?=$email?>" placeholder="email@example.com">
        </div>
        <div class="col-sm-12">
            <button class="btn btn-default short" style="width:100%;" onclick="ajaxForm(event)" name="button" value="<?=$arResult['TEXT_BTN']?>"><?=$arResult['TEXT_BTN']?></button>
        </div>
    </div>
</form>