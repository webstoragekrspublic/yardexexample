<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "TEXT_SUCCESS" => array(
            "PARENT" => "BASE",
            "NAME" => "Текс при Положительной отправки",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "вы подписаны",
        ),
        "TEXT_ERROR" => array(
            "PARENT" => "BASE",
            "NAME" => "Текс при Ошибке",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "произошла ошибка попробуйте подписаться попозже",
        ),
        "TEXT_SLOGAN" => array(
            "PARENT" => "BASE",
            "NAME" => "Слоган формы подписки",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "Лучшие предложения у вас в почте",
        ),
        "TEXT_BTN" => array(
            "PARENT" => "BASE",
            "NAME" => "Текс кнопки",
            "TYPE" => "STRING",
            "MULTIPLE" => "N",
            "DEFAULT" => "Подписаться",
        ),
    ),
);