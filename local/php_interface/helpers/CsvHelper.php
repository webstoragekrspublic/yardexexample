<?php

namespace Helpers;

class CsvHelper
{
    public const RULE_DOT_REPLACE = 'dotReplace';
    public const RULE_ARRAY_IMPLODE = 'arrayImplode';
    public const RULE_PERCENTS = 'percents';
    public const RULE_ADD_BEFORE = 'addBefore';

    public static function toCsvArr(array $headers, array $data): array
    {
        $resCsvArr = [array_column($headers, 'headerName')];
        foreach ($data as $row) {
            $addCsvRow = [];
            foreach ($headers as $key => $header) {
                $rowValue = $row[$key];
                $prepareRule = $header['prepare'] ?? false;
                if ($prepareRule) {
                    $rowValue = self::prepareColumnValue($prepareRule, $rowValue);
                }

                $addCsvRow[] = (string)$rowValue;
            }
            $resCsvArr[] = $addCsvRow;
        }

        return $resCsvArr;
    }

    public static function array2csvString(array $fileArr, $delimiter = ';', $enclosure = '"', $escape_char = "\\"): string
    {
        $f = fopen('php://memory', 'r+');
        foreach ($fileArr as $item) {
            fputcsv($f, $item, $delimiter, $enclosure, $escape_char);
        }
        rewind($f);
        return stream_get_contents($f);
    }

    protected static function prepareColumnValue($rule, $rowValue)
    {
        $ruleName = $rule['rule'];
        $ruleValue = $rule['value'];

        switch ($ruleName) {
            case self::RULE_DOT_REPLACE:
                $rowValue = str_replace('.', ',', (string)$rowValue);
                break;
            case self::RULE_ARRAY_IMPLODE:
                $rowValue = implode($ruleValue, $rowValue);
                break;
            case self::RULE_PERCENTS:
                $rowValue = number_format($rowValue * 100, 2, ',', '') . "%";
                break;
            case self::RULE_ADD_BEFORE:
                $rowValue = $ruleValue . $rowValue;
                break;
            default:
                break;
        }

        return $rowValue;
    }
}