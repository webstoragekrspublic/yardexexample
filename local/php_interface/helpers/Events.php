<?php

namespace Helpers;

use Custom\DeliveryTime\DeliveryTime;
use ExternalApi\B24CRM\B24CRM;
use ExternalApi\Exchange1c\Exchange1c;
use Handlers\ExternalSuppliers\ExternalSuppliers;
use Handlers\SpecPrice;
use Handlers\ProductDiscount;
use Handlers\TotalPrice;

class Events
{
    const MORE_PHOTO_CODE = 'MORE_PHOTO';
    const MORE_PHOTO_ID = 204;
    const CATALOG_IBLOCK_ID = 114;

    /**
     * Restrict image update if same image size given
     * @param $arFields
     */
    public function checkImagesOnUpdate(&$arFields)
    {
        $arImageInfoDetail = \CFile::MakeFileArray($arFields['DETAIL_PICTURE']['old_file']);
        if ($arFields['DETAIL_PICTURE']['size'] == $arImageInfoDetail['size']) {
            unset($arFields['DETAIL_PICTURE']);
        }

        $arImageInfoPreview = \CFile::MakeFileArray($arFields['PREVIEW_PICTURE']['old_file']);
        if ($arFields['PREVIEW_PICTURE']['size'] == $arImageInfoPreview['size']) {
            unset($arFields['PREVIEW_PICTURE']);
        }

        if (!$arFields['PROPERTY_VALUES']) {
            return;
        }
        if ($arFields['PROPERTY_VALUES'][self::MORE_PHOTO_CODE]) {
            $morePhoto = &$arFields['PROPERTY_VALUES'][self::MORE_PHOTO_CODE];
        } elseif ($arFields['PROPERTY_VALUES'][self::MORE_PHOTO_ID]) {
            $morePhoto = &$arFields['PROPERTY_VALUES'][self::MORE_PHOTO_ID];
        } else {
            return;
        }

        if (!$morePhoto) {
            return;
        }
        $newFilesKeys = $oldFilesKeys = [];
        if ($morePhoto['tmp_name']) {
            $morePhoto = [$morePhoto];
        }
        foreach ($morePhoto as $key => &$arPhoto) {
            if (is_numeric($key) && (!$arPhoto['VALUE']['tmp_name'] && !$arPhoto['tmp_name'])) {
                $oldFilesKeys[] = $key;
            } else {
                $newFilesKeys[] = $key;
            }
        }

        if (!$newFilesKeys) {
            return;
        }

        $res = \CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], false, false, ['CODE' => self::MORE_PHOTO_CODE]);
        while ($photo = $res->Fetch()) {

            $oldFiles[$photo['PROPERTY_VALUE_ID']] = \CFile::MakeFileArray($photo['VALUE']);
        }

        if (!$oldFiles) {
            return;
        }

        foreach ($newFilesKeys as $newKey) {
            $newFileArray = $morePhoto[$newKey]['VALUE'] ?: $morePhoto[$newKey];
            foreach ($oldFiles as $oldKey => $oldFile) {

                if ($oldFile['size'] == $newFileArray['size']) {
                    if ($morePhoto[$oldKey]['VALUE']['del']) {
                        unset($morePhoto[$oldKey]['VALUE']['del']);
                    }
                    if ($morePhoto[$oldKey]['del']) {
                        unset($morePhoto[$oldKey]['del']);
                    }
                    unset($morePhoto[$newKey]);
                    break;
                }
            }
        }
    }
    
    function onSaleOrderSaved(\Bitrix\Main\Event $event)
    {
        $order = $event->getParameter("ENTITY");
        $isNew = $event->getParameter("IS_NEW");

        $propertyCollection = $order->getPropertyCollection();
        $deliveryTimeProp = $propertyCollection->getItemByOrderPropertyId(21);
        $shipmentTimeProp = $propertyCollection->getItemByOrderPropertyId(43);
        $deliveryTimePropValue = $deliveryTimeProp->getValue();
        
        $shipmentTimePropValue = $shipmentTimeProp->getValue();
        
        
        $hourOld = "";
        if(!empty($shipmentTimePropValue)){
            $shipmentDateTime = \DateTime::createFromFormat('d.m.Y H:i:s', $shipmentTimePropValue);

            $hourOld = $shipmentDateTime->format("H");
        }
        
        //writeAppendLog($hourOld);
        
        $hourShipment = 11;
        if($deliveryTimePropValue == "11:00-13:00" || $deliveryTimePropValue == "13:00-15:00"){
            $hourShipment = 11;
        }
        else if($deliveryTimePropValue == "12:00-14:00"){
            $hourShipment = 12;
        }
        else if($deliveryTimePropValue == "15:00-17:00" || $deliveryTimePropValue == "17:00-19:00"){
            $hourShipment = 15;
        }
        else if($deliveryTimePropValue == "16:00-18:00"){
            $hourShipment = 16;
        }
        else if($deliveryTimePropValue == "19:00-21:00" || $deliveryTimePropValue == "21:00-23:00"){
            $hourShipment = 19;
        }
        
        //writeAppendLog($hourShipment);
        
        if ($isNew || ($hourOld != $hourShipment))
        {
            $dateTime = new \DateTime();
            $dateTime = \DateTime::createFromFormat('d.m.Y H:i:s',"{$dateTime->format('d')}.{$dateTime->format('m')}.{$dateTime->format('Y')} {$hourShipment}:00:00");
            $dateTime = \Bitrix\Main\Type\DateTime::createFromTimestamp($dateTime->getTimestamp());

            $shipmentTimeProp->setValue($dateTime);
            
            //writeAppendLog($dateTime);
            
            $order->save();
        }
    }

    public function onBeforeIBlockElementUpdate(&$arFields)
    {
        global $USER;
        if ($arFields['IBLOCK_ID'] == self::CATALOG_IBLOCK_ID) {
            if (!empty($arFields['NAME'])) { //добавляем имя в сео "Заголовок элемента", решаем баг не бновления имени после обновления с помощью CIBlockElement::Update
                $arFields["IPROPERTY_TEMPLATES"]['ELEMENT_PAGE_TITLE'] = $arFields['NAME'];
            }
        }

        if ($arFields['IBLOCK_ID'] == $recipeIblockID = 140) {
            $recipeProductsPropID = 1194;
            $recipeProductsCountPropID = 1195;

            $recipeProductsIDs = [];
            foreach ($arFields['PROPERTY_VALUES'][$recipeProductsPropID] as $recipeProductsPropItem) {
                if($recipeProductsPropItem['VALUE']!=''){
                    $recipeProductsIDs[]=$recipeProductsPropItem['VALUE'];
                }
            }    

            $nKey = 0;
            foreach ($recipeProductsIDs as $key =>$recipeProductsID) {
                if(array_keys($arFields['PROPERTY_VALUES'][$recipeProductsCountPropID])[$key]>1000){
                    continue;
                }else{
                    $arFields['PROPERTY_VALUES'][$recipeProductsCountPropID]['n'.$nKey]['VALUE'] = ['TYPE' => '', 'NAME' => '', 'VALUE' => ''];
                    $arFields['PROPERTY_VALUES'][$recipeProductsCountPropID]['n'.$nKey]['VALUE']['TYPE'] = $recipeProductsID;
                    $nKey++;
                }
            }
        }
    }

    public function OnProductUpdate($event)
    {
        //временно решаем проблему обновления заказа, когда в наличии товаров нет но увеличить в заказе нужно
        ['id' => $id, 'fields' => $fields] = $event->getParameters("fields");
        if (key_exists('QUANTITY', $fields) && (int)$fields['QUANTITY'] == 0 && $id) {
            //делаем неактивным и выставляем снова большое количество
            $el = new \CIBlockElement;
            $arFields = array(
                'ACTIVE' => 'N',
            );
            $el->update($id, $arFields);
        }
    }

    public function updateSpecPricesAfterChangeIBlockElement(&$arFields)
    {
        //срабатывает при обновлении или при добавлении нового товара
        global $USER;
        if ($arFields['IBLOCK_ID'] == self::CATALOG_IBLOCK_ID) {
            if (isset($arFields['PROPERTY_VALUES'][SpecPrice\SpecPriceController::ELEMENT_SPEC_PRICE_ID])) {
                $specPrice = $arFields['PROPERTY_VALUES'][SpecPrice\SpecPriceController::ELEMENT_SPEC_PRICE_ID];
                $specPriceValue = is_array($specPrice) ? $specPrice[array_key_first($specPrice)]['VALUE'] : $specPrice;
                SpecPrice\SpecPriceController::setActualSpecPriceRule($arFields['ID'], $specPriceValue, $arFields['ACTIVE']);
            }
        }
    }

    public function onAfterIBlockElementDelete(&$arFields)
    {
        //срабатывает при удалении товара
        global $USER;
        if ($arFields['IBLOCK_ID'] == self::CATALOG_IBLOCK_ID) {
            SpecPrice\SpecPriceController::delete($arFields['ID']);
        }
    }

    public function onAfterIBlockElementSetPropertyValuesEx($id, $iblockId, $props, $flags)
    {
        //срабатывает при обновлении свойств через CIBlockElement::SetPropertyValuesEx
        global $USER;
        if ($iblockId == self::CATALOG_IBLOCK_ID) {
            if (!isset($props['DISCOUNTED'])) {
                //ProductDiscount\ProductDiscountController::markDiscountProduct($id);
            }
            if (!isset($props['TOTAL_PRICE_ALL'])) {
                $totalPrice = ProductDiscount\ProductDiscountParser::getProductPrice($id)['RESULT_PRICE']['DISCOUNT_PRICE'];
                TotalPrice\TotalPrice::set($id, $iblockId, $totalPrice);
            }
            if (isset($props['SPEC_PRICE'])) {
                $specPriceValue = is_array($props['SPEC_PRICE']) ? $props['SPEC_PRICE'][array_key_first($props['SPEC_PRICE'])]['VALUE'] : $props['SPEC_PRICE'];
                SpecPrice\SpecPriceController::setActualSpecPriceRule($id, $specPriceValue);
            }
        }
    }

    public function onOrderSave($orderId, &$fields, &$orderFields, $isNew)
    {
        if ($isNew == true){
            $isAdminSection = \Bitrix\Main\Context::getCurrent()->getRequest()->isAdminSection();
            static::turnPropertyNotCallBack($orderId, $isAdminSection);
        }
        static::updateTotalCountsAndSku($orderId, $orderFields);

        $arOrderPropertyWithUserField = [
            'MANAGER_COMMENT_FOR_USER' => 'UF_MANAGER_COMMENT', //комментарий менеджера
            'BUYERS_CATEGORY' => 'UF_BUYERS_CATEGORY' //категория пользователя
        ];
        static::updateUserFieldOrOrderProperty($orderId, $arOrderPropertyWithUserField);//Обновляем комментарий менеджера и Категорию пользователя
        DeliveryTime::getInstance()->updateUsedSlots();
        Exchange1c::putOrderInUpdateQueue($orderId);
        B24CRM::putOrderInUpdateQueue($orderId);
        ExternalSuppliers::onOrderSave($orderId);
        return true;
    }
    
    public function onSaleOrderBeforeSaved(\Bitrix\Main\Event $event)
    {
        $order = $event->getParameter("ENTITY");
        
        $basket = $order->getBasket();
        $propertyCollection = $order->getPropertyCollection();

        global $USER;
//        if(!$USER->isAdmin()){
            $utm_string = \Bitrix\Main\Context::getCurrent()->getRequest()->getCookie('utm_string');
            $sourcePropValue = $propertyCollection->getItemByOrderPropertyId($orderSourcePropId = 37);
            if (!empty($utm_string) && empty($sourcePropValue->getValue())) {            
                $sourcePropValue->setValue($utm_string);
            }
       
            $curOrderUserCoupons = \Bitrix\Main\Context::getCurrent()->getRequest()->getCookie('ORDER_CURRENT_COUPONS');
            $curOrderUserCouponsPropValue = $propertyCollection->getItemByOrderPropertyId($curOrderUserCouponsPropId = 39);

            if (!empty($curOrderUserCoupons) && empty($curOrderUserCouponsPropValue->getValue())) {
                \Bitrix\Sale\DiscountCouponsManager::init();
                \Bitrix\Sale\DiscountCouponsManager::clear(true);
                $orderCoupoons = json_decode($curOrderUserCoupons, true);

                if (!empty($orderCoupoons)) {
                    /**
                     * Получим список купонов из HL (пользовательские купоны)
                     */
                    \Bitrix\Main\Loader::includeModule("highloadblock");

                    $hlblock = \Bitrix\Highloadblock\HighloadBlockTable::getById(\Helpers\Constants::USERS_COUPON_HL_ID)->fetch();

                    $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
                    $entity_data_class = $entity->getDataClass();

                    $rsData = $entity_data_class::getList();
                    $arCoupons = [];
                    while ($res = $rsData->Fetch()) {
                        $arCoupons[] = mb_strtolower($res['UF_NAME']);
                    }

                    /**
                     * Получим список использованных купонов пользователем
                     */
                    $userId = $USER->GetID();
                    $arUserCoupons = \CUser::GetByID($userId)->Fetch()['UF_USED_COUPON'];

                    foreach ($orderCoupoons as $coupon) {
                        \Bitrix\Sale\DiscountCouponsManager::add($coupon);
                        /**
                         * Если купон в списке пользовательских купонов, добавим его пользователю
                         */
                        if (in_array(mb_strtolower($coupon), $arCoupons)) {
                            $arUserCoupons[] = $coupon;
                        }
                    }
                    /**
                     * Обновим список использованных купонов пользователем
                     */
                    $USER->Update($userId, ['UF_USED_COUPON' => $arUserCoupons]);
                }

                $discounts = \Bitrix\Sale\Discount::buildFromOrder($order);
                $basket->refreshData(['PRICE',  'COUPONS']);

                try {
                    $r = $discounts->calculate();
                    $basket->applyDiscount($r->getData());
                } catch (\Throwable $th) {
                }

                $curOrderUserCouponsPropValue->setValue($curOrderUserCoupons);
                \Bitrix\Main\Context::getCurrent()->getResponse()->addCookie(
                    new \Bitrix\Main\Web\Cookie('ORDER_CURRENT_COUPONS', '[]', time() - 3600)
                );
            }
//        }
        \Bitrix\Sale\DiscountCouponsManager::init();
        \Bitrix\Sale\DiscountCouponsManager::clear(true);
        $event->addResult(
            new \Bitrix\Main\EventResult(
                \Bitrix\Main\EventResult::SUCCESS,
                $order
            )
        );
    }

    public function updateTotalCountsAndSku($orderId, &$orderFields)
    {
        $arrayPropertiesName = ['TOTAL_ORDERS', 'SKU_QUANTITY'];
        $dbRes = \Bitrix\Sale\Property::getList([
            'filter' => ['CODE' => $arrayPropertiesName]
        ]);

        if($arOrderPropsAll = $dbRes->fetchAll()) {
            foreach ($arOrderPropsAll as $property){
                $arOrderPropsId[] = $property['ID'];

                if ($property['CODE'] == 'TOTAL_ORDERS'){//поиск последнего значения свойства в заказах
                    $TOTAL_ORDERS_PROPERTY_ID = $property['ID'];
                    $cntOrders = static::getLastOrderCount($TOTAL_ORDERS_PROPERTY_ID, $orderId);
                    if ($cntOrders == "") {
                        $userId = \CSaleOrder::GetByID($orderId);
                        $userId = $userId['USER_ID'];

                        $parameters = [
                            'select' => ['CNT'],
                            'filter' => ["USER_ID" => $userId],
                            'group' => ['USER_ID'],
                            'runtime' => array(
                                new \Bitrix\Main\Entity\ExpressionField('CNT', 'COUNT(*)')
                            )
                        ];
                        $orders = \Bitrix\Sale\Order::getList($parameters);
                        $cntOrders = $orders->fetch();
                    } else {
                        $cntOrders += 1;
                    }
                    foreach ($cntOrders as $key => $value) {
                        $cntOrders = $value;
                    }
                    $arOrderProps['TOTAL_ORDERS'] = $property;
                } elseif ($property['CODE'] == 'SKU_QUANTITY') {
                    $sku_quantity = count($orderFields['BASKET_ITEMS']);
                    $arOrderProps['SKU_QUANTITY'] = $property;
                }
                else{
                    $arOrderProps[$property['CODE']] = $property;
                }
            }
            unset($arOrderPropsAll);

            $dbOrderProps = \CSaleOrderPropsValue::GetList(array("SORT" => "ASC"), array("ORDER_ID" => $orderId, 'ORDER_PROPS_ID' => $arOrderPropsId));

            while ($arVals = $dbOrderProps->GetNext()) {
                if ($arVals['CODE'] == 'SKU_QUANTITY'){
                    static::updateOrderProperty($arVals, $sku_quantity);
                }
                elseif ($arVals['CODE'] == 'TOTAL_ORDERS'){
                    static::updateOrderProperty($arVals, $cntOrders);
                }
                unset($arOrderProps[$arVals['CODE']]);
            }
            foreach ($arOrderProps as $arOrderPropsItem) {
                $arOrderPropsItem['CODE'] == 'TOTAL_COUNTS' ? static::addOrderProperty($arOrderPropsItem['CODE'], $orderId, $cntOrders) : null;
                $arOrderPropsItem['CODE'] == 'SKU_QUANTITY' ? static::addOrderProperty($arOrderPropsItem['CODE'], $orderId, $sku_quantity) : null;
            }
        }
    }

    public function getLastOrderCount($TOTAL_ORDERS_PROPERTY_ID, $orderId)
    {
        $userId = \CSaleOrder::GetByID($orderId);
        $userId = $userId['USER_ID'];

        $parameters = [
            'select' => ['ID'],
            'filter' => ["USER_ID" => $userId],
            'order' => ['ID' => 'DESC']
        ];
        $orders = \Bitrix\Sale\Order::getList($parameters);
        while ($lastOrder = $orders->fetch()) {
            if ($lastOrder['ID'] == $orderId) {
                break;
            }
        }
        $lastOrder = $orders->fetch();

        $property = \CSaleOrderPropsValue::GetList(
            ["SORT" => "ASC"],
            ["ORDER_ID" => $lastOrder,
                'ORDER_PROPS_ID' => $TOTAL_ORDERS_PROPERTY_ID
            ]
        );
        $prop = $property->fetch();
        return $prop['VALUE'];
    }

    public function updateUserFieldOrOrderProperty($orderId, $arOrderPropertyWithUserField)
    {
        foreach ($arOrderPropertyWithUserField as $orderProperty => $userField){
            $orderPropertyName[] = $orderProperty;
            $userFieldNames [] = $userField;
        }
        $dbRes = \Bitrix\Sale\Property::getList([
            'filter' => ['CODE' => $orderPropertyName]
        ]);

        if ($arOrderPropsAll = $dbRes->fetchAll()) {
            foreach ($arOrderPropsAll as $property) {
                $arOrderPropsId[] = $property['ID'];

                $arOrderProps[$property['CODE']] = $property;
            }
            unset($arOrderPropsAll);

            $dbOrderProps = \CSaleOrderPropsValue::GetList(array("SORT" => "ASC"), array("ORDER_ID" => $orderId, 'ORDER_PROPS_ID' => $arOrderPropsId));

            $userId = \CSaleOrder::GetByID($orderId);
            $userId = $userId['USER_ID'];

            global $USER_FIELD_MANAGER;
            $userFields = $USER_FIELD_MANAGER->GetUserFields('USER', $userId);
            foreach ($userFieldNames as $usrFldName) {
                $userFieldValue = $userFields[$usrFldName]['VALUE'];

                //Проверяем свойство заказа на то, список это или нет и если список проверяем пользовательское свойство на то, есть ли оно в списке или удалено
                if ($isList = \CSaleOrderPropsVariant::GetList(array("SORT" => "ASC"), array('ORDER_PROPS_ID' => $arOrderProps['ID']))->fetch()) {
                    $isExistsSelect = \CSaleOrderPropsVariant::GetList(array("SORT" => "ASC"), array('ORDER_PROPS_ID' => $arOrderProps['ID'], 'VALUE' => $userFieldValue))->fetch();
                    if ($isExistsSelect == false) {
                        $userFieldValue = null;
                    }
                }
                //проверили

                if ($arVals = $dbOrderProps->GetNext()) {
                    if ($arVals['VALUE'] == null) {
                        static::updateOrderProperty($arVals, $userFieldValue);
                    } else if ($arVals['VALUE'] != $userFieldValue) {
                        $fields = [$usrFldName => $arVals['VALUE']];
                        $USER_FIELD_MANAGER->Update('USER', $userId, $fields);
                    }
                } else {
                    if ($arVals['VALUE'] == null) {
                        static::addOrderProperty($arOrderProps, $orderId, $userFieldValue);
                    } else if ($arVals['VALUE'] != $userFieldValue) {
                        $fields = [$usrFldName => $arVals['VALUE']];
                        $USER_FIELD_MANAGER->Update('USER', $userId, $fields);
                    }
                }
            }
        }
    }

    public function onSaleStatusOrder($id, $val)
    {
        static::sendSmsOnOrderStatus($id, $val);
    }

    protected static function sendSmsOnOrderStatus($idOrder, $newStatusCode)
    {
        if ($newStatusCode == 'IS') { //SALE_STATUS_CHANGED_IS_SMS
            $order = \Bitrix\Sale\Order::load($idOrder);
            $totalSum = $order->getPrice();
            $props = \Bitrix\Sale\Internals\OrderPropsValueTable::getList([
                'select' => ['CODE', 'VALUE'],
                'filter' => [
                    'ORDER_ID' => $idOrder,
                    'CODE' => ['DELIVERY_TIME', 'PHONE', 'DELIVERY_DATE']
                ]
            ])->fetchAll();
            $sendProps = [
                'PHONE_NUMBER' => '',
                'DELIVERY_TIME' => '',
                'ORDER_TOTAL' => sprintf('%0.2f', $totalSum)
            ];
            $deliveryDate = '';
            if ($props) {
                foreach ($props as $prop) {
                    if ($prop['CODE'] == 'PHONE') {
                        $sendProps['PHONE_NUMBER'] = CustomTools::phoneStringToDigits($prop['VALUE']);
                    } elseif ($prop['CODE'] == 'DELIVERY_TIME') {
                        $sendProps['DELIVERY_TIME'] = trim($prop['VALUE']);
                    } elseif ($prop['CODE'] == 'DELIVERY_DATE') {
                        $deliveryDate = trim($prop['VALUE']);
                    }
                }
            }
            $today = date("d.m.Y");
            if ($sendProps['PHONE_NUMBER'] && $deliveryDate === $today) {
                if (!preg_match('/\d+/', $sendProps['DELIVERY_TIME'])) {
                    $sendProps['DELIVERY_TIME'] = 'в течение дня';
                }

                $sms = new \Bitrix\Main\Sms\Event(
                    'SALE_STATUS_CHANGED_IS_SMS',
                    $sendProps
                );
                $sms->setSite('s1');
                $sms->send(true);
            }
        }
        if ($newStatusCode == 'N') { //SALE_NEW_ORDER_SMS
            $props = \Bitrix\Sale\Internals\OrderPropsValueTable::getList([
                'select' => ['CODE', 'VALUE'],
                'filter' => [
                    'ORDER_ID' => $idOrder,
                ]
            ])->fetchAll();
            $sendProps = [
                'PHONE_NUMBER' => '',
                'ORDER_REAL_ID' => $idOrder
            ];
            if ($props) {
                foreach ($props as $prop) {
                    if ($prop['CODE'] == 'PHONE') {
                        $sendProps['PHONE_NUMBER'] = CustomTools::phoneStringToDigits($prop['VALUE']);
                    } else if ($prop['CODE'] !== 'PHONE_NUMBER') {
                        $sendProps[$prop['CODE']] = $prop['VALUE'];
                    }
                }
            }
            if ($sendProps['PHONE_NUMBER']) {
                $sms = new \Bitrix\Main\Sms\Event(
                    'SALE_NEW_ORDER_SMS',
                    $sendProps
                );
                $sms->setSite('s1');
                $sms->send(true);
            }
        }
    }

    public function turnPropertyNotCallBack($orderId, $isAdminSection){//Логика для "перезванивать" в админке и "не перезванивать" на фронте у клиента
        $dbRes = \Bitrix\Sale\Property::getList([
            'filter' => ['CODE' => 'NOT_CALLING_BACK']
        ]);

        if ($arOrderProps = $dbRes->fetch()) {
            $TOTAL_ORDERS_PROPERTY_ID = $arOrderProps['ID'];
            $dbOrderProps = \CSaleOrderPropsValue::GetList(array("SORT" => "ASC"), array("ORDER_ID" => $orderId, 'ORDER_PROPS_ID' => $arOrderProps['ID']));

            if ($arVals = $dbOrderProps->GetNext()) {
                if(!$isAdminSection){//если заказ создается не из админки, тогда заменять значение
                    $arVals['VALUE'] == 'Y' ? $arVals['VALUE'] = 'N' : $arVals['VALUE'] = 'Y';
                }
                static::updateOrderProperty($arVals, $arVals['VALUE']);
            } else {
                if(!$isAdminSection) {//если заказ создается не из админки, тогда заменять значение
                    $arVals['VALUE'] == 'Y' ? $arVals['VALUE'] = 'N' : $arVals['VALUE'] = 'Y';
                }
                static::addOrderProperty($arOrderProps, $orderId, $arVals['VALUE']);
            }
        }
    }

    public static function updateOrderProperty($arVals, $value){
        \CSaleOrderPropsValue::Update($arVals['ID'], array(
            'NAME' => $arVals['NAME'],
            'CODE' => $arVals['CODE'],
            'ORDER_PROPS_ID' => $arVals['ORDER_PROPS_ID'],
            'ORDER_ID' => $arVals['ORDER_ID'],
            'VALUE' => $value,
        ));
    }
    public static function addOrderProperty($arOrderProps, $orderId, $value){
        \CSaleOrderPropsValue::Add(array(
            'NAME' => $arOrderProps['NAME'],
            'CODE' => $arOrderProps['CODE'],
            'ORDER_PROPS_ID' => $arOrderProps['ID'],
            'ORDER_ID' => $orderId,
            'VALUE' => $value,
        ));
    }

    public static function getCatalogAllowSortFields()
    {
        return [
            [
                'field' => 'NAME',
                'name' => 'По названию'
            ],
            [
                'field' => 'PROPERTY_TOTAL_PRICE_ALL',
                'name' => 'По цене'
            ],
            [
                'field' => 'PROPERTY_DISCOUNT_AMOUNT_PERCENT',
                'name' => 'По размеру скидки'
            ]
        ];
    }
    
    
    public function _Check404Error()
    {
//        if (defined('ERROR_404') && ERROR_404 == 'Y') {
//            $currentUrl = (\CMain::IsHTTPS()) ? "https://" : "http://";
//            $currentUrl .= $_SERVER["HTTP_HOST"];
//            $currentUrl = $currentUrl.$_SERVER['REQUEST_URI'];
//            $recipients = array("fallerd@sibkon.ru", "polskyn@sibkon.ru");
//            mail(implode(',', $recipients), '404 ошибка', $currentUrl);
//        }
    }
}
