<?php

namespace Helpers\ReportsAdmin;

use Helpers\CsvHelper;
use Helpers\CustomTools;
use Helpers\GlobalStorage;

class ReportsAdmin
{
    protected function getProductPropsForReports(array $prIds): array
    {
        if (!$prIds) {
            return [];
        }

        $arSort = ["ID" => "DESC"];
        $arSelect = ["ID", "PROPERTY_PURCHASING_PRICE"];
        $arFilter = ["IBLOCK_ID" => \Helpers\Constants::PRODUCTS_IBLOCK_ID, 'ID' => $prIds];

        $res = [];
        $propsQ = \CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
        while ($row = $propsQ->fetch()) {
            $res[$row['ID']] = $row;
        }
        return $res;
    }

    protected function enrichOrdersWithProperties(array &$orders, array $addProps = [])
    {
        $ordersId = array_column($orders, 'ID');
        if (!$ordersId) {
            return;
        }
        $arSelect = ['ID', 'ORDER_ID', 'CODE', 'VALUE'];
        $filter = [
            'ORDER_ID' => $ordersId,
        ];
        if ($addProps) {
            $filter['CODE'] = $addProps;
        }
        $propsQ = \Bitrix\Sale\PropertyValueCollection::getList(
            [
                'select' => $arSelect,
                'filter' => $filter,
                'order' => ['ID' => 'ASC']
            ]
        );
        $props = [];
        while ($row = $propsQ->fetch()) {
            $props[$row['ORDER_ID']]['PROPERTIES'][$row['CODE']] = $row['VALUE'];
        }
        foreach ($orders as $key => $o) {
            $orderId = $o['ID'];
            if (isset($props[$orderId])) {
                $orders[$key] = array_merge($o, $props[$orderId]);
            }
        }
    }

    protected function enrichOrdersWithCoupons(array &$orders)
    {
        $ordersId = array_column($orders, 'ID');
        if (!$ordersId) {
            return;
        }
        $couponQ = \Bitrix\Sale\Internals\OrderCouponsTable::getList([
            'select' => array('COUPON_ID', 'ID', 'ORDER_ID', 'COUPON'),
            'filter' => array('=ORDER_ID' => $ordersId)
        ]);
        $coupons = [];
        while ($row = $couponQ->fetch()) {
            $coupons[$row['ORDER_ID']]['COUPONS'][] = $row;
        }
        foreach ($orders as $key => $o) {
            $orderId = $o['ID'];
            if (isset($coupons[$orderId])) {
                $orders[$key] = array_merge($o, $coupons[$orderId]);
            }
        }
    }

    protected function enrichOrdersWithUserInfo(array &$orders)
    {
        $usersId = array_column($orders, 'USER_ID');
        if (!$usersId) {
            return;
        }
        $userQ = \Bitrix\Main\UserTable::getList([
            "select" => ['ID','NAME','SECOND_NAME','LAST_NAME'],
            "filter" => ['ID' => $usersId],
        ]);
        $users = [];
        while ($row = $userQ->fetch()) {
            $row['FIO'] = '';
            $row['FIO'] .= !empty($row['FIO'])?$row['NAME']:'';
            $users[$row['ID']]['USER_INFO'] = $row;
        }
        foreach ($orders as $key => $o) {
            $userId = $o['USER_ID'];
            if (isset($users[$userId])) {
                $orders[$key] = array_merge($o, $users[$userId]);
            }
        }
    }

    protected function getOrdersRowsForReport($orderFilters, $select = ['ID', 'USER_ID', 'PAY_SYSTEM_ID', 'DELIVERY_ID', 'PAYED', 'CANCELED', 'STATUS_ID', 'PRICE']): array
    {
        return \Bitrix\Sale\Order::getList(['select' => $select, 'filter' => $orderFilters, 'order' => ['ID' => 'DESC']])->fetchAll();
    }

    protected function getCurrentProductsInOrders(array $orders): array
    {
        if (!$orders) {
            return [];
        }
        $prQ = \Bitrix\Sale\Basket::getList([
            'select' => ['ORDER_ID', 'PRODUCT_ID', 'NAME', 'PRICE', 'QUANTITY', 'MEASURE_NAME', 'PRODUCT_XML_ID', 'BASE_PRICE'],
            'filter' => [
                '=ORDER_ID' => array_column($orders, 'ID'),
            ]
        ]);
        $productsForOrder = [];
        $prIds = [];
        while ($row = $prQ->fetchRaw()) {
            $prIds[] = $row['PRODUCT_ID'];
            $productsForOrder[$row['ORDER_ID']][$row['PRODUCT_ID']] = $row;
        }
        $productAddProps = $this->getProductPropsForReports($prIds);
        foreach ($productsForOrder as $orderId => $cart) {
            foreach ($cart as $productId => $item) {
                if (isset($productAddProps[$productId])) {
                    $productsForOrder[$orderId][$productId] += $productAddProps[$productId];
                }
            }
        }

        return $productsForOrder;
    }

    /*
     * Отчет по вычеркнутым товарам в заказах
     */
    public function getReportROD(array $ordersFilter): array
    {
        $orders = $this->getOrdersRowsForReport($ordersFilter);
        if (!$orders) {
            return [];
        }
        $currentProductsInOrders = $this->getCurrentProductsInOrders($orders);
        if (!$currentProductsInOrders) {
            return [];
        }

        $firstHistoryProductsInOrders = $this->getFirstHistoryProductsInOrders($orders);

        $reportProducts = [];
        foreach ($firstHistoryProductsInOrders as $orderId => $historyBasket) {
            foreach ($historyBasket as $productId => $addedItemInfo) {

                $dbElement = \CIBlockElement::GetByID($productId);
                
                if($arElement = $dbElement->GetNext()){
                    $dbSection = \CIBlockSection::GetByID($arElement["IBLOCK_SECTION_ID"]);
                    if($arSection = $dbSection->GetNext()){
                        $sectionName = $arSection['NAME'];
                    }
                    $dbProps = \CIBlockElement::GetProperty(114, $productId, array(), Array("CODE"=>"TORGOVAJAMARKA"));
                    if($arProps = $dbProps->Fetch()){
                        $manufacturerID = $arProps["VALUE"];
                    }
                    if($manufacturerID != NULL) {
                        $dbManufacturer = \CIBlockElement::GetByID($manufacturerID);
                        if($arManufacturer = $dbManufacturer->GetNext()) {
                            $manufacturerName = $arManufacturer["NAME"];
                        }
                    }
                    else{
                        $manufacturerName = "";
                    }
                    $dbProps = \CIBlockElement::GetProperty(114, $productId, array(), Array("CODE"=>"SUPPLIER_ID"));
                    if($arProps = $dbProps->Fetch()){
                        $supplier = $arProps["VALUE"];
                    }
                }


                if (!isset($reportProducts[$productId])) {
                    $reportProducts[$productId] = $addedItemInfo;
                    $reportProducts[$productId]['TTL_BOUGHT_QTY'] = 0;
                    $reportProducts[$productId]['TTL_ORDERED_QTY'] = 0;
                    $reportProducts[$productId]['TTL_DELETED_QTY'] = 0;
                    $reportProducts[$productId]['TTL_PRICE_ORDERED'] = 0;
                    $reportProducts[$productId]['TTL_PRICE_AVR'] = 0;
                    $reportProducts[$productId]['TTL_PRICE_DELETED'] = 0;
                    $reportProducts[$productId]['TTL_PRICE_DELETED_PROFIT'] = 0;
                    $reportProducts[$productId]['TTL_ORDERS_CNT'] = 0;
                    $reportProducts[$productId]['TTL_ORDERS_WITH_DELETED_CNT'] = 0;
                    $reportProducts[$productId]['TTL_ORDERS_DELETED_PERCENT'] = 0;
                    $reportProducts[$productId]['TTL_DELETED_PERCENT'] = 0;
                    $reportProducts[$productId]['TTL_ORDERS_DELETED_ID'] = [];
                }
                $reportProducts[$productId]['TTL_ORDERS_CNT']++;

                $reportProducts[$productId]['GROUP_PRODUCT'] = $sectionName;
                $reportProducts[$productId]['MANUFACTURER'] = $manufacturerName;
                $reportProducts[$productId]['SUPPLIER'] = $supplier;

                $currentBasketItemInfo = $currentProductsInOrders[$orderId][$productId] ?? false;

                $ORDERED_QTY = ($currentBasketItemInfo && $currentBasketItemInfo['QUANTITY'] > $addedItemInfo['QUANTITY']) ? $currentBasketItemInfo['QUANTITY'] : $addedItemInfo['QUANTITY'];
                $PRICE = ($currentBasketItemInfo) ? $currentBasketItemInfo['PRICE'] : $addedItemInfo['PRICE'];
                $reportProducts[$productId]['PURCHASING_PRICE'] = $this->getPurchasingPrice($addedItemInfo['PROPERTY_PURCHASING_PRICE_VALUE'], $PRICE);


                if (!$currentBasketItemInfo || $this->isProductWasDeleted($addedItemInfo, $currentBasketItemInfo)) {
                    $reportProducts[$productId]['TTL_ORDERS_WITH_DELETED_CNT']++;
                    $reportProducts[$productId]['TTL_ORDERS_DELETED_ID'][] = $addedItemInfo['ORDER_ID'];
                    $DELETED_QTY = $addedItemInfo['QUANTITY'] - (float)$currentBasketItemInfo['QUANTITY'];
                    $reportProducts[$productId]['TTL_DELETED_QTY'] += $DELETED_QTY;
                    $reportProducts[$productId]['TTL_PRICE_DELETED'] += $DELETED_QTY * $PRICE;
                    $reportProducts[$productId]['TTL_PRICE_DELETED_PROFIT'] += ($DELETED_QTY * ($PRICE - $this->getPurchasingPrice($addedItemInfo['PROPERTY_PURCHASING_PRICE_VALUE'], $PRICE)));
                }

                $reportProducts[$productId]['TTL_BOUGHT_QTY'] += (float)$currentBasketItemInfo['QUANTITY'];
                $reportProducts[$productId]['TTL_ORDERED_QTY'] += $ORDERED_QTY;
                $reportProducts[$productId]['TTL_PRICE_ORDERED'] += $ORDERED_QTY * $PRICE;

                $reportProducts[$productId]['PRODUCT_ID'] = $addedItemInfo['PRODUCT_ID'];
            }
        }

        foreach ($reportProducts as $key => $reportProduct) {
            // убираем из отчета товары без вычерков
//            if ($reportProduct['TTL_ORDERS_WITH_DELETED_CNT'] == 0) {
//                unset($reportProducts[$key]);
//                continue;
//            }
            if ($reportProduct['TTL_ORDERED_QTY'] > 0) {
                $reportProducts[$key]['TTL_PRICE_AVR'] = $reportProduct['TTL_PRICE_ORDERED'] / $reportProduct['TTL_ORDERED_QTY'];
            }
            if ($reportProduct['TTL_ORDERS_CNT'] > 0) {
                $reportProducts[$key]['TTL_ORDERS_DELETED_PERCENT'] = ((float)$reportProduct['TTL_ORDERS_WITH_DELETED_CNT'] / (float)$reportProduct['TTL_ORDERS_CNT']);
            }
            if ($reportProduct['TTL_ORDERED_QTY'] > 0) {
                $reportProducts[$key]['TTL_DELETED_PERCENT'] = ((float)$reportProduct['TTL_DELETED_QTY'] / (float)$reportProduct['TTL_ORDERED_QTY']);
            }
        }

        $headers = [
            'PRODUCT_ID' => ['headerName' => 'ID товара'],
            'XML_ID' => ['headerName' => 'Код товара в 1с'],
            'NAME' => ['headerName' => 'Название товара'],

            'TTL_ORDERS_CNT' => ['headerName' => 'Всего заказов'],
            'TTL_ORDERS_WITH_DELETED_CNT' => ['headerName' => 'Всего заказов с вычерками'],
            'TTL_ORDERS_DELETED_PERCENT' => [
                'headerName' => 'процент заказов с вычерками',
                'prepare' => ['rule' => CsvHelper::RULE_PERCENTS, 'value' => '']
            ],

            'TTL_PRICE_ORDERED' => [
                'headerName' => 'Общая изначальная стоимость продажи товаров (руб)',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'TTL_PRICE_AVR' => [
                'headerName' => 'Средняя стоимость заказанного товара (руб)',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PURCHASING_PRICE' => [
                'headerName' => 'Себестоимость на момент отчета (руб)',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'TTL_PRICE_DELETED' => [
                'headerName' => 'Потери в обороте (руб)',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'TTL_PRICE_DELETED_PROFIT' => [
                'headerName' => 'Потери в прибыли (руб)',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'TTL_ORDERED_QTY' => [
                'headerName' => 'Всего заказали товаров',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'TTL_DELETED_QTY' => [
                'headerName' => 'Всего вычеркнули товаров',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'TTL_DELETED_PERCENT' => [
                'headerName' => 'процент вычеркнутых товаров',
                'prepare' => ['rule' => CsvHelper::RULE_PERCENTS, 'value' => '']
            ],

            'MEASURE_NAME' => ['headerName' => 'Единица измерения'],

            'TTL_ORDERS_DELETED_ID' => [
                'headerName' => 'заказы с вычерками',
                'prepare' => ['rule' => CsvHelper::RULE_ARRAY_IMPLODE, 'value' => ', ']
            ],
            
            'GROUP_PRODUCT' => [
                'headerName' => 'группа товаров'
            ],
            'MANUFACTURER' => [
                'headerName' => 'производитель'
            ],
            'SUPPLIER' => [
                'headerName' => 'код поставщика'
            ],
        ];

        return CsvHelper::toCsvArr($headers, $reportProducts);
    }

    /*
     * Отчет по заказам с товарами
     */
    public function getReportROG(array $ordersFilter): array
    {
        $orders = $this->getOrdersRowsForReport(
            $ordersFilter,
            ['ID', 'USER_ID', 'PAY_SYSTEM_ID', 'DELIVERY_ID', 'PAYED', 'CANCELED', 'STATUS_ID', 'PRICE', 'DATE_INSERT', 'COMMENTS', 'USER_DESCRIPTION']
        );
        if (!$orders) {
            return [];
        }
        $this->enrichOrdersWithProperties($orders, ['ADDRESS','PHONE','DELIVERY_DATE', 'DELIVERY_TIME', 'TOTAL_ORDERS', 'SKU_QUANTITY', 'MANAGER_COMMENT_FOR_USER','FIO']);
        $this->enrichOrdersWithCoupons($orders);
        $this->enrichOrdersWithUserInfo($orders);
        $orders = CustomTools::indexArrayByKey($orders, 'ID');

        $currentProductsInOrders = $this->getCurrentProductsInOrders($orders);
        if (!$currentProductsInOrders) {
            return [];
        }

        $reportArr = [];

        foreach ($currentProductsInOrders as $idOrder => $basket) {

            $orderInfo = $orders[$idOrder];
            $dateInsert = $orderInfo['DATE_INSERT']->format("d.m.Y H:i:s");
            $couponsName = [];
            foreach ($orderInfo['COUPONS'] as $coupon) {
                $couponsName[] = $coupon['COUPON'];
            }

            $orderPriceWithoutDiscount = 0;
            $orderPricePurchase = 0;
            foreach ($basket as $item) {
                $orderPriceWithoutDiscount += $item['BASE_PRICE'] * $item['QUANTITY'];
                $orderPricePurchase += $this->getPurchasingPrice($item['PROPERTY_PURCHASING_PRICE_VALUE'],$item['PRICE']) * $item['QUANTITY'];
            }

            foreach ($basket as $item) {
                $row = $item;
                
                $dbElement = \CIBlockElement::GetByID($item['PRODUCT_ID']);
                
                if($arElement = $dbElement->GetNext()){
                    $dbSection = \CIBlockSection::GetByID($arElement["IBLOCK_SECTION_ID"]);
                    if($arSection = $dbSection->GetNext()){
                        $sectionName = $arSection['NAME'];
                    }
                    $dbProps = \CIBlockElement::GetProperty(114, $item['PRODUCT_ID'], array(), Array("CODE"=>"TORGOVAJAMARKA"));
                    if($arProps = $dbProps->Fetch()){
                        $manufacturerID = $arProps["VALUE"];
                    }
                    if($manufacturerID != NULL) {
                        $dbManufacturer = \CIBlockElement::GetByID($manufacturerID);
                        if ($arManufacturer = $dbManufacturer->GetNext()) {
                            $manufacturerName = $arManufacturer["NAME"];
                        }
                    }
                    else{
                        $manufacturerName = "";
                    }
                    $dbProps = \CIBlockElement::GetProperty(114, $item['PRODUCT_ID'], array(), Array("CODE"=>"SUPPLIER_ID"));
                    if($arProps = $dbProps->Fetch()){
                        $supplier = $arProps["VALUE"];
                    }
                }
                                
                $row['GROUP_PRODUCT'] = $sectionName;
                $row['MANUFACTURER'] = $manufacturerName;
                $row['SUPPLIER'] = $supplier;
                
                $row['PRODUCT_ID'] = $item['PRODUCT_ID'];
                $row['ORDER_ID'] = $idOrder;
                $row['CUSTOMER_ID'] = $orderInfo['USER_ID'];
                $row['PAYMENT_NAME'] = $this->getPaymentName($orderInfo['PAY_SYSTEM_ID']);
                $row['DELIVERY_NAME'] = $this->getDeliveryName($orderInfo['DELIVERY_ID']);
                $row['PAYED'] = $this->getYesNoField($orderInfo['PAYED']);
                $row['CANCELED'] = $orderInfo['CANCELED'];
                $row['STATUS_NAME'] = $this->getStatusName($orderInfo['STATUS_ID']);

                $row['ORDER_PRICE'] = $orderInfo['PRICE'];

                $row['ORDER_PRICE_WITHOUT_DISCOUNT'] = $orderPriceWithoutDiscount;
                $row['ORDER_PRICE_PURCHASE'] = $orderPricePurchase;

                $row['ORDER_DATE_INSERT'] = $dateInsert;
                $row['DELIVERY_TIME'] = $orderInfo['PROPERTIES']['DELIVERY_TIME'];
                $row['DELIVERY_DATE'] = $orderInfo['PROPERTIES']['DELIVERY_DATE'];
                $row['TOTAL_ORDERS'] = $orderInfo['PROPERTIES']['TOTAL_ORDERS'];
                $row['PHONE'] = $orderInfo['PROPERTIES']['PHONE'];
                $row['ADDRESS'] = $orderInfo['PROPERTIES']['ADDRESS'];
                $row['COUPONS_NAME'] = $couponsName;
                $row['SKU_QUANTITY'] = $orderInfo['PROPERTIES']['SKU_QUANTITY'];
                $row['USER_DESCRIPTION'] = $orderInfo['USER_DESCRIPTION'];
                $row['COMMENTS'] = $orderInfo['COMMENTS'];
                $row['MANAGER_COMMENT_FOR_USER'] = $orderInfo['PROPERTIES']['MANAGER_COMMENT_FOR_USER'];
                $row['FIO'] = $orderInfo['PROPERTIES']['FIO'];
                $row['CUSTOMER'] = implode(' ',[$orderInfo['USER_INFO']['NAME'],$orderInfo['USER_INFO']['LAST_NAME']]);
                $row['PRICE_TTL'] = $item['PRICE'] * $item['QUANTITY'];
                $row['BASE_PRICE_TTL'] = $item['BASE_PRICE'] * $item['QUANTITY'];
                $row['PURCHASING_PRICE'] = $this->getPurchasingPrice($item['PROPERTY_PURCHASING_PRICE_VALUE'],$item['PRICE']);
                $row['PURCHASING_PRICE_TTL'] = $this->getPurchasingPrice($item['PROPERTY_PURCHASING_PRICE_VALUE'],$item['PRICE']) * $item['QUANTITY'];

                $reportArr[] = $row;
            }
        }

        $headers = [
            'ORDER_ID' => [
                'headerName' => 'ID',
                'prepare' => ['rule' => CsvHelper::RULE_ADD_BEFORE, 'value' => '№'],
            ],
            'ORDER_DATE_INSERT' => ['headerName' => 'Дата и время заказа'],
            'STATUS_NAME' => ['headerName' => 'Статус'],
            'DELIVERY_DATE' => ['headerName' => 'Дата доставки'],
            'DELIVERY_TIME' => ['headerName' => 'Время доставки'],


            'FIO' => ['headerName' => 'ФИО'],
            'CUSTOMER' => ['headerName' => 'Покупатель'],
            'MANAGER_COMMENT_FOR_USER' => ['headerName' => 'Комментарий о Клиенте'],

            'TOTAL_ORDERS' => ['headerName' => 'Кол-во заказов'],
            'PHONE' => ['headerName' => 'Телефон'],
            'ADDRESS' => ['headerName' => 'Адрес доставки'],

            'COUPONS_NAME' => [
                'headerName' => 'Купоны заказа',
                'prepare' => ['rule' => CsvHelper::RULE_ARRAY_IMPLODE, 'value' => ', ']
            ],
            'ORDER_PRICE' => [
                'headerName' => 'Сумма',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],

            'SKU_QUANTITY' => ['headerName' => 'Количество SCU'],
            'PAYED' => ['headerName' => 'Оплачен'],

            'USER_DESCRIPTION' => ['headerName' => 'Комментарии покупателя'],
            'COMMENTS' => [
                'headerName' => 'Комментарии',
                'prepare' => ['rule' => CsvHelper::RULE_ADD_BEFORE, 'value' => ' ']
            ],


//            'PAYMENT_NAME' => ['headerName' => 'Платежная система'],
//            'DELIVERY_NAME' => ['headerName' => 'Способ доставки'],

//            'CANCELED' => ['headerName' => 'Заказ отменен'],
//
            'PRODUCT_ID' => ['headerName' => 'Товар ID'],
            'NAME' => ['headerName' => 'Товар Название'],
            'QUANTITY' => [
                'headerName' => 'Товар Количество',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PRICE' => [
                'headerName' => 'Товар Стоимость',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'BASE_PRICE' => [
                'headerName' => 'Товар Стоимость без скидки',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PURCHASING_PRICE' => [
                'headerName' => 'Товар Закуп',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'BASE_PRICE_TTL' => [
                'headerName' => 'стоимость sku без скидки',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PRICE_TTL' => [
                'headerName' => 'стоимость sku',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PURCHASING_PRICE_TTL' => [
                'headerName' => 'себестоимость sku',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            
            'GROUP_PRODUCT' => [
                'headerName' => 'группа товаров'
            ],
            'MANUFACTURER' => [
                'headerName' => 'производитель'
            ],
            'SUPPLIER' => [
                'headerName' => 'код поставщика'
            ],

        ];

        return CsvHelper::toCsvArr($headers, $reportArr);
    }

    /*
     * Отчет по заказам
     */
    public function getReportRO(array $ordersFilter): array
    {
        $orders = $this->getOrdersRowsForReport(
            $ordersFilter,
            ['ID', 'USER_ID', 'PAY_SYSTEM_ID', 'DELIVERY_ID', 'PAYED', 'CANCELED', 'STATUS_ID', 'PRICE', 'DATE_INSERT', 'COMMENTS', 'USER_DESCRIPTION']
        );
        
        if (!$orders) {
            return [];
        }
        $this->enrichOrdersWithProperties($orders, ['ADDRESS','PHONE','DELIVERY_DATE', 'DELIVERY_TIME', 'TOTAL_ORDERS', 'SKU_QUANTITY', 'MANAGER_COMMENT_FOR_USER','FIO']);
        $this->enrichOrdersWithCoupons($orders);
        $this->enrichOrdersWithUserInfo($orders);
        $orders = CustomTools::indexArrayByKey($orders, 'ID');

        $currentProductsInOrders = $this->getCurrentProductsInOrders($orders);
        if (!$currentProductsInOrders) {
            return [];
        }

        $reportArr = [];

        foreach ($currentProductsInOrders as $idOrder => $basket) {

            $orderInfo = $orders[$idOrder];
            $dateInsert = $orderInfo['DATE_INSERT']->format("d.m.Y H:i:s");
            $couponsName = [];
            foreach ($orderInfo['COUPONS'] as $coupon) {
                $couponsName[] = $coupon['COUPON'];
            }

            $orderPriceWithoutDiscount = 0;
            $orderPricePurchase = 0;
            foreach ($basket as $item) {
                $orderPriceWithoutDiscount += $item['BASE_PRICE'] * $item['QUANTITY'];
                $orderPricePurchase += $this->getPurchasingPrice($item['PROPERTY_PURCHASING_PRICE_VALUE'],$item['PRICE']) * $item['QUANTITY'];
            }

            $row['ORDER_ID'] = $idOrder;
            $row['CUSTOMER_ID'] = $orderInfo['USER_ID'];
            $row['PAYMENT_NAME'] = $this->getPaymentName($orderInfo['PAY_SYSTEM_ID']);
            $row['DELIVERY_NAME'] = $this->getDeliveryName($orderInfo['DELIVERY_ID']);
            $row['PAYED'] = $this->getYesNoField($orderInfo['PAYED']);
            $row['CANCELED'] = $orderInfo['CANCELED'];
            $row['STATUS_NAME'] = $this->getStatusName($orderInfo['STATUS_ID']);

            $row['ORDER_PRICE'] = $orderInfo['PRICE'];

            $row['ORDER_PRICE_WITHOUT_DISCOUNT'] = $orderPriceWithoutDiscount;
            $row['ORDER_PRICE_PURCHASE'] = $orderPricePurchase;

            $row['ORDER_DATE_INSERT'] = $dateInsert;
            $row['DELIVERY_TIME'] = $orderInfo['PROPERTIES']['DELIVERY_TIME'];
            $row['DELIVERY_DATE'] = $orderInfo['PROPERTIES']['DELIVERY_DATE'];
            $row['TOTAL_ORDERS'] = $orderInfo['PROPERTIES']['TOTAL_ORDERS'];
            $row['PHONE'] = $orderInfo['PROPERTIES']['PHONE'];
            $row['ADDRESS'] = $orderInfo['PROPERTIES']['ADDRESS'];
            $row['COUPONS_NAME'] = $couponsName;
            $row['SKU_QUANTITY'] = $orderInfo['PROPERTIES']['SKU_QUANTITY'];
            $row['USER_DESCRIPTION'] = $orderInfo['USER_DESCRIPTION'];
            $row['COMMENTS'] = $orderInfo['COMMENTS'];
            $row['MANAGER_COMMENT_FOR_USER'] = $orderInfo['PROPERTIES']['MANAGER_COMMENT_FOR_USER'];
            $row['FIO'] = $orderInfo['PROPERTIES']['FIO'];
            $row['CUSTOMER'] = implode(' ',[$orderInfo['USER_INFO']['NAME'],$orderInfo['USER_INFO']['LAST_NAME']]);
            $reportArr[] = $row;
        }

        $headers = [
            'ORDER_ID' => [
                'headerName' => 'ID',
                'prepare' => ['rule' => CsvHelper::RULE_ADD_BEFORE, 'value' => '№'],
            ],
            'ORDER_DATE_INSERT' => ['headerName' => 'Дата и время заказа'],
            'STATUS_NAME' => ['headerName' => 'Статус'],
            'DELIVERY_DATE' => ['headerName' => 'Дата доставки'],
            'DELIVERY_TIME' => ['headerName' => 'Время доставки'],


            'FIO' => ['headerName' => 'ФИО'],
            'CUSTOMER' => ['headerName' => 'Покупатель'],
            'MANAGER_COMMENT_FOR_USER' => ['headerName' => 'Комментарий о Клиенте'],

            'TOTAL_ORDERS' => ['headerName' => 'Кол-во заказов'],
            'PHONE' => ['headerName' => 'Телефон'],
            'ADDRESS' => ['headerName' => 'Адрес доставки'],

            'COUPONS_NAME' => [
                'headerName' => 'Купоны заказа',
                'prepare' => ['rule' => CsvHelper::RULE_ARRAY_IMPLODE, 'value' => ', ']
            ],
            'ORDER_PRICE' => [
                'headerName' => 'Сумма',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],

            'SKU_QUANTITY' => ['headerName' => 'Количество SCU'],
            'PAYED' => ['headerName' => 'Оплачен'],

            'USER_DESCRIPTION' => ['headerName' => 'Комментарии покупателя'],
            'COMMENTS' => [
                'headerName' => 'Комментарии',
                'prepare' => ['rule' => CsvHelper::RULE_ADD_BEFORE, 'value' => ' ']
            ],


//            'PAYMENT_NAME' => ['headerName' => 'Платежная система'],
//            'DELIVERY_NAME' => ['headerName' => 'Способ доставки'],

//            'CANCELED' => ['headerName' => 'Заказ отменен'],
//
            'ORDER_PRICE_WITHOUT_DISCOUNT' => [
                'headerName' => 'Заказ - стоимость без скидки (руб)',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'ORDER_PRICE_PURCHASE' => [
                'headerName' => 'Заказ - стоимость закупа (руб)',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
        ];

        return CsvHelper::toCsvArr($headers, $reportArr);
    }


    protected function getFirstHistoryProductsInOrders(array $orders): array
    {
        if (!$orders) {
            return [];
        }

        $historyQ = \Bitrix\Sale\Internals\OrderChangeTable::getList(array(
            'order' => ['ORDER_ID' => 'DESC', 'ID' => 'DESC'],
            'select' => ['ORDER_ID', 'DATA', 'TYPE'],
            'filter' => [
                '=ORDER_ID' => array_column($orders, 'ID'),
                '=TYPE' => 'SHIPMENT_ITEM_BASKET_ADDED'
            ]
        ));

        $historyProducts = [];


        while ($historyItem = $historyQ->fetch()) {
            if ($historyItem['TYPE'] == 'SHIPMENT_ITEM_BASKET_ADDED') {
                $productInfo = CustomTools::unserializeHistoryData($historyItem['DATA']);
                $productsId[] = $productInfo['PRODUCT_ID'];
                $productInfo['ORDER_ID'] = $historyItem['ORDER_ID'];
                $historyProducts[$historyItem['ORDER_ID']][$productInfo['PRODUCT_ID']] = $productInfo;
            }
        }

        $prices = \Bitrix\Catalog\PriceTable::getList([
            "select" => ["PRICE", "PRODUCT_ID"],
            "filter" => ['=PRODUCT_ID' => $productsId],
        ]);
        $prPrices = [];

        while ($row = $prices->fetch()) {
            $prPrices[$row['PRODUCT_ID']] = $row['PRICE'];
        }
        $measure = \CCatalogMeasure::getList();
        $measureNames = [];
        while ($measureTemp = $measure->Fetch()) {
            $measureNames[$measureTemp['ID']] = $measureTemp['SYMBOL_RUS'];
        }


        $arSelect = ["ID", "PROPERTY_PURCHASING_PRICE", "XML_ID", "MEASURE"];
        $arFilter = ["IBLOCK_ID" => \Helpers\Constants::PRODUCTS_IBLOCK_ID, 'ID' => $productsId];

        $productsAdditionalInfo = [];
        $propsQ = \CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
        while ($row = $propsQ->fetch()) {
            $productsAdditionalInfo[$row['ID']] = $row;
            $productsAdditionalInfo[$row['ID']]['MEASURE_NAME'] = $measureNames[$row['MEASURE']];
        }

        foreach ($historyProducts as $orderId => $basket) {
            foreach ($basket as $productId => $item) {
                if (isset($productsAdditionalInfo[$productId])) {
                    $historyProducts[$orderId][$productId] += $productsAdditionalInfo[$productId];
                    $historyProducts[$orderId][$productId]['PRICE'] = $prPrices[$productId];
                }
            }
        }

        return $historyProducts;
    }

    protected function isProductWasDeleted(array $addedPrInfo, array $currentPrInfo)
    {
        if ($addedPrInfo['MEASURE'] == 4) { //кг
            if ($currentPrInfo['QUANTITY'] < $addedPrInfo['QUANTITY'] * 0.67) {
                return true;
            } else {
                return false;
            }
        } elseif ($currentPrInfo['QUANTITY'] < $addedPrInfo['QUANTITY']) {
            return true;
        }
        return false;
    }

    protected function getPaymentName($id)
    {
        $cacheKey = 'ReportAdmin::getPaymentName';
        $cachePayments = GlobalStorage::get($cacheKey);
        if (!$cachePayments) {
            $paySystemsResult = \Bitrix\Sale\PaySystem\Manager::getList(array(
                'select' => ['PAY_SYSTEM_ID', 'NAME'],
                'order' => ['ACTIVE' => 'DESC', 'ID' => 'DESC']
            ));
            $cachePayments = [];
            while ($row = $paySystemsResult->fetch()) {
                $cachePayments[$row['PAY_SYSTEM_ID']] = $row;
            }
        }
        return $cachePayments[$id]['NAME'] ?? '';
    }

    protected function getDeliveryName($id)
    {
        $cacheKey = 'ReportAdmin::getDeliveryName';
        $cacheDeliveries = GlobalStorage::get($cacheKey);
        if (!$cacheDeliveries) {
            $deliverySystemsResult = \Bitrix\Sale\Delivery\Services\Table::getList(array(
                'select' => ['CODE', 'NAME'],
                'order' => ['ACTIVE' => 'DESC', 'ID' => 'DESC']
            ));
            while ($row = $deliverySystemsResult->fetch()) {
                $cacheDeliveries[$row['CODE']] = $row;
            }
            GlobalStorage::set($cacheKey, $cacheDeliveries);
        }
        return $cacheDeliveries[$id]['NAME'] ?? '';
    }

    protected function getStatusName($id)
    {
        $cacheKey = 'ReportAdmin::getStatusName';
        $cacheStatuses = GlobalStorage::get($cacheKey);
        if (!$cacheStatuses) {
            $statusResult = \Bitrix\Sale\Internals\StatusLangTable::getList(array(
                'order' => array('STATUS.SORT' => 'ASC'),
                'filter' => array('STATUS.TYPE' => 'O', 'LID' => LANGUAGE_ID),
            ));
            while ($row = $statusResult->fetch()) {
                $cacheStatuses[$row['STATUS_ID']] = $row;
            }
            GlobalStorage::set($cacheKey, $cacheStatuses);
        }
        return $cacheStatuses[$id]['NAME'] ?? '';
    }

    protected function getYesNoField(string $value): string
    {
        switch ($value) {
            case 'Y':
                return 'Да';
            case 'N':
                return 'Нет';
        }
        return '';
    }

    /**
     * если закупочная цена = 0, то выбираем обычную цену товара
     */
    protected function getPurchasingPrice($propertyPurchasingPrice, $price){
        if ($propertyPurchasingPrice == 0){
            return $price;
        }

        return $propertyPurchasingPrice;
    }

    /**
     * выборка заказов по дате даставки
     * @param string $deliveryDate
     * @return array
     */

    function getOrdersId($deliveryDateFrom,$deliveryDateTo)
    {
        $period = new \DatePeriod(
            new \DateTime($deliveryDateFrom),
            new \DateInterval('P1D'),
            new \DateTime($deliveryDateTo." 23:59:59")
        );
        $periodDays = array();
        foreach ($period as $key => $value) {
            $periodDays[] = $value->format('d.m.Y');
        }

        $arSelect = ['ID', 'ORDER_ID', 'CODE', 'VALUE'];
        $filter = [
            'VALUE' => $periodDays,
        ];
        $propsQ = \Bitrix\Sale\PropertyValueCollection::getList(
            [
                'select' => $arSelect,
                'filter' => $filter,
                'order' => ['ID' => 'ASC'],
            ]
        );
        $orderId = [];
        while ($row = $propsQ->fetch()) {
            $orderId[] = $row['ORDER_ID'];
        }
        return $orderId;
    }

}