<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$mondayWeekNumber = 1;

$daySeconds = 60 * 60 * 24;

$messageId = '147';
$csvFilePath = __DIR__ . '/reportOrders.csv';
$reportAdmin = new \Helpers\ReportsAdmin\ReportsAdmin();
$csvDelimiter = ';';
$fp = fopen($csvFilePath, 'w');
if (!$fp){
    die('ошибка открытия файла');
}

////////////даты с прошлого понедельника 00:00:00 по воскресенье 23:59:59////////////////////////
$dateTime = new \DateTime();
$dateTime->setTimestamp(time() - 7 * $daySeconds);
$weekNumber = (int)$dateTime->format('w');
if ($weekNumber == 0) {
    $weekNumber = 7;//меняем воскресенье на 7.
}
$mondayTimeStamp = $dateTime->getTimestamp() - ($weekNumber - $mondayWeekNumber) * $daySeconds;
$mondayDateTime = new \DateTime();
$mondayDateTime->setTimestamp($mondayTimeStamp);
//пн 00:00:00
$mondayDateTime = \DateTime::createFromFormat('!d.m.Y',"{$mondayDateTime->format('d')}.{$mondayDateTime->format('m')}.{$mondayDateTime->format('Y')}");

$sundayDateTime = new \DateTime();
//вс 23:59:59
$sundayDateTime->setTimestamp($mondayDateTime->getTimestamp() + (7 * $daySeconds) - 1);

$DATE_CREATE_FROM = \Bitrix\Main\Type\DateTime::createFromTimestamp($mondayDateTime->getTimestamp());
$DATE_CREATE_TO = \Bitrix\Main\Type\DateTime::createFromTimestamp($sundayDateTime->getTimestamp());
//////////////////////////////////////////////////////////////////////////////////////////////////////

$orderFilters = [
    '<=DATE_INSERT' => $DATE_CREATE_TO,
    '>=DATE_INSERT' => $DATE_CREATE_FROM,
    '!=STATUS_ID' => 'O'
];
$orderFilter['=STATUS_ID'] = $_POST['statusSystems'];
$data = $reportAdmin->getReportRO($orderFilters);

if ($data) {
    foreach ($data as $csvRow) {
        fputcsv($fp, $csvRow, $csvDelimiter);
    }
}
fclose($fp);

convertFileToWin1251($csvFilePath);


$sendFiles = [$csvFilePath];
$sendProps = [
    'DATE_FROM' => $DATE_CREATE_FROM->format("d.m.Y H:i:s"),
    'DATE_TO' => $DATE_CREATE_TO->format("d.m.Y H:i:s"),
];
$sended = \Helpers\CustomTools::sendServicesEmail($sendProps, $messageId, $sendFiles);
var_dump("sended = ".$sended);


function convertFileToWin1251($filePath)
{
    $data = file_get_contents($filePath);
    $win1251data = mb_convert_encoding($data, "Windows-1251", "UTF-8");
    file_put_contents($filePath, chr(239).chr(187).chr(191).$data);
}
