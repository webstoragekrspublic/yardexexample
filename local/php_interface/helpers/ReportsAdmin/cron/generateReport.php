<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$mondayWeekNumber = 1;

$daySeconds = 60 * 60 * 24;

$messageId = '147';
$csvFilePath = __DIR__ . '/routesReports.csv';
$reportAdmin = new \Helpers\ReportsAdmin\ReportsAdmin();
$csvDelimiter = ';';
$fp = fopen($csvFilePath, 'w');
if (!$fp){
    die('ошибка открытия файла');
}

////////////даты за позапрошлый день, с 00:00-23:59////////////////////////
$dateTime = new \DateTime();
$dateTime->setTimestamp(time());

$beforeTimeStamp = $dateTime->getTimestamp() - 2 * $daySeconds;
$beforeDateTime = new \DateTime();
$beforeDateTime->setTimestamp($beforeTimeStamp);
$beforeDateTime = \DateTime::createFromFormat('!d.m.Y',"{$beforeDateTime->format('d')}.{$beforeDateTime->format('m')}.{$beforeDateTime->format('Y')}");

$finalDateTime = new \DateTime();
$finalDateTime->setTimestamp($beforeTimeStamp);
$finalDateTime = \DateTime::createFromFormat('!d.m.Y H:i:s',"{$finalDateTime->format('d')}.{$finalDateTime->format('m')}.{$finalDateTime->format('Y')}"." "."23".":59".":59");

$DATE_CREATE_FROM = \Bitrix\Main\Type\DateTime::createFromTimestamp($beforeDateTime->getTimestamp());
$DATE_CREATE_TO = \Bitrix\Main\Type\DateTime::createFromTimestamp($finalDateTime->getTimestamp());
//////////////////////////////////////////////////////////////////////////////////////////////////////

$orderFilters = [
    'STATUS_ID' => array('F', 'IS')
];
$orderFilter['=STATUS_ID'] = $_POST['statusSystems'];
$data = $reportAdmin->getReportRO($orderFilters);

//pr($data);
//exit;

$keyDateDelivery = 3;
foreach($data as $key=>$row){
    foreach($row as $cellKey=>$cell){
        if($key == 0 && $cell == "Дата доставки"){
            $keyDateDelivery = $cellKey;
            break;
        }
    }
    if($key != 0){
        if(!((trim($row[$keyDateDelivery]) != "") && (strtotime($row[$keyDateDelivery]) >= strtotime($DATE_CREATE_FROM->toString())) && (strtotime($row[$keyDateDelivery]) <= strtotime($DATE_CREATE_TO->toString())))){
            unset($data[$key]);
        }
    }
}

if ($data) {
    foreach ($data as $csvRow) {
        fputcsv($fp, $csvRow, $csvDelimiter);
    }
}
fclose($fp);

convertFileToWin1251($csvFilePath);

//$sendFiles = [$csvFilePath];
//$sendProps = [
//    'DATE_FROM' => $DATE_CREATE_FROM->format("d.m.Y H:i:s"),
//    'DATE_TO' => $DATE_CREATE_TO->format("d.m.Y H:i:s"),
//];
//$sended = \Helpers\CustomTools::sendServicesEmail($sendProps, $messageId, $sendFiles);
//var_dump("sended = ".$sended);


function convertFileToWin1251($filePath)
{
    $data = file_get_contents($filePath);
    $win1251data = mb_convert_encoding($data, "Windows-1251", "UTF-8");
    file_put_contents($filePath, chr(239).chr(187).chr(191).$data);
}
