<?php
namespace Helpers;

class CustomAdminPage
{
    public static function notificationsAdminMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        global $USER;

        if ($USER->IsAdmin()) {

            $aGlobalMenu['global_menu_custom'] = [
                'menu_id' => 'custom',
                'text' => 'YARDEX TOOLS',
                'title' => 'Наши инструменты',
                'sort' => 10000,
                'items_id' => 'global_menu_custom',
                'help_section' => 'custom',
                'items' => [
                    [
                        'global_menu_basket_rule_for_admin.php',
                        'parent_menu' => 'global_menu_custom',
                        'sort' => 10,
                        'url' => '/bitrix/admin/global_menu_notifications__from_infoblock.php?lang=ru',
                        'text' => 'Найстройка уведомлений',
                        'title' => 'Уведомления',
                        'icon' => 'fav_menu_icon',
                        'page_icon' => 'fav_menu_icon',
                        'items_id' => 'menu_custom',
                    ],
                    [
                        'parent_menu' => 'global_menu_custom',
                        'sort' => 10,
                        'url' => '/bitrix/admin/global_menu_basket_rule_for_admin.php?lang=ru',
                        'text' => 'Найстройка правил корзины',
                        'title' => 'Правила корзины',
                        'icon' => 'fav_menu_icon',
                        'page_icon' => 'fav_menu_icon',
                        'items_id' => 'menu_custom',
                    ],
                    [
                        'parent_menu' => 'global_menu_custom',
                        'sort' => 10,
                        'url' => '/bitrix/admin/global_menu_reports_by_order.php?lang=ru',
                        'text' => 'Отчеты',
                        'title' => 'Отчеты по заказам',
                        'icon' => 'fav_menu_icon',
                        'page_icon' => 'fav_menu_icon',
                        'items_id' => 'menu_custom',
                    ],
                    [
                        'parent_menu' => 'global_menu_custom',
                        'sort' => 100,
                        'url' => '/bitrix/admin/settings.php?lang=ru&mid=custom.deliverytime',
                        'text' => 'модуль временных слотов',
                        'title' => 'модуль временных слотов',
                        'icon' => 'fav_menu_icon',
                        'page_icon' => 'fav_menu_icon',
                        'items_id' => 'menu_custom',
                    ],
                    [
                        'parent_menu' => 'global_menu_custom',
                        'sort' => 100,
                        'url' => '/bitrix/admin/settings.php?lang=ru&mid=custom.suppliersorders',
                        'text' => 'просмотр списка заказов поставщиков',
                        'title' => 'просмотр списка заказов поставщиков',
                        'icon' => 'fav_menu_icon',
                        'page_icon' => 'fav_menu_icon',
                        'items_id' => 'menu_custom',
                    ],
                    [
                        'parent_menu' => 'global_menu_custom',
                        'sort' => 100,
                        'url' => '/bitrix/admin/settings.php?lang=ru&mid=custom.regions',
                        'text' => 'регионы',
                        'title' => 'регионы',
                        'icon' => 'fav_menu_icon',
                        'page_icon' => 'fav_menu_icon',
                        'items_id' => 'menu_custom',
                    ]
                ],
            ];
        }
    }
}