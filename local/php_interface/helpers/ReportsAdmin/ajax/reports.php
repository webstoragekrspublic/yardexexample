<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!\Helpers\SafetyValidation::isAdminOrManger(\CUser::GetID())) {
    die(json_encode([
        'error' => true,
        'error_text' => 'перезагрузите страницу',
        'answer' => ''
    ]));
}

set_time_limit(1000);

$answer = [
    'error' => true,
    'error_text' => 'неизвестная ошибка',
    'answer' => ''
];

$reportData = [];

$reportAdmin = new \Helpers\ReportsAdmin\ReportsAdmin();

$orderFilter = [];

if (trim($_POST['insertDate_from'])) {
    $timeFrom = trim($_POST['insertDate_from']) . ' 00:00:00';
    try {
        $dateTime = new \Bitrix\Main\Type\DateTime($timeFrom, 'd.m.Y H:i:s');
    } catch (\Exception $exception) {

        die(json_encode([
            'error' => true,
            'error_text' => 'ошибка формата в "дате создания" ' . $exception->getMessage(),
            'answer' => ''
        ]));
    }
    $orderFilter['>=DATE_INSERT'] = $dateTime;
}

if (trim($_POST['insertDate_to'])) {

    $timeFrom = trim($_POST['insertDate_to']) . ' 23:59:59';
    try {
        $dateTime = new \Bitrix\Main\Type\DateTime($timeFrom, 'd.m.Y H:i:s');
    } catch (\Exception $exception) {

        die(json_encode([
            'error' => true,
            'error_text' => 'ошибка формата в "дате создания" ' . $exception->getMessage(),
            'answer' => ''
        ]));
    }

    $orderFilter['<=DATE_INSERT'] = $dateTime;
}

if (trim($_POST['idOrder'])) {
    $orderFilter['=ID'] = explode(',', $_POST['idOrder']);
}

if (trim($_POST['deliveryDate_from']) || trim($_POST['deliveryDate_to'])) {
    $timeFrom = trim($_POST['deliveryDate_from']);
    try {
        $dateTimeFrom = new \Bitrix\Main\Type\DateTime($timeFrom, 'd.m.Y');
    } catch (\Exception $exception) {

        die(json_encode([
            'error' => true,
            'error_text' => 'ошибка формата в "дате создания" ' . $exception->getMessage(),
            'answer' => ''
        ]));
    }
    $timeTo = trim($_POST['deliveryDate_to']);
    try {
        $dateTimeTo = new \Bitrix\Main\Type\DateTime($timeTo, 'd.m.Y');
    } catch (\Exception $exception) {

        die(json_encode([
            'error' => true,
            'error_text' => 'ошибка формата в "дате создания" ' . $exception->getMessage(),
            'answer' => ''
        ]));
    }

    $ordersDeliveryDateId = $reportAdmin->getOrdersId($timeFrom, $timeTo);
    $arrayOrdersId = [];
    if (!empty($ordersDeliveryDateId) && !empty($orderFilter['=ID']) && is_array($orderFilter['=ID'])) {
        for ($i=0; $i<count($orderFilter['=ID']); $i++) {
            for ($j=0; $j<count($ordersDeliveryDateId); $j++) {
                if ($orderFilter['=ID'][$i] == $ordersDeliveryDateId[$j]) {
                    $arrayOrdersId[] = $ordersDeliveryDateId[$j];
                }
            }
        }
        $orderFilter['=ID'] = $arrayOrdersId;
    } elseif (empty($orderFilter['=ID']) && !empty($ordersDeliveryDateId)) {
        $orderFilter['=ID'] = $ordersDeliveryDateId;
    }
}

if ($_POST['paySystems'] && is_array($_POST['paySystems'])) {
    $orderFilter['=PAY_SYSTEM_ID'] = $_POST['paySystems'];
}
if (trim($_POST['userName']) || trim($_POST['userLastName'])) {
    $arFilter = array('NAME' => trim($_POST['userName']),'LAST_NAME' => trim($_POST['userLastName']));
    $userId = CUser::GetList(false,false,$arFilter, array('ID'))->fetch()['ID'];
    $orderFilter['=USER_ID'] = $userId;
}
if ($_POST['deliverySystems'] && is_array($_POST['deliverySystems'])) {
    $orderFilter['=DELIVERY_ID'] = $_POST['deliverySystems'];
}
if ($_POST['statusSystems'] && is_array($_POST['statusSystems'])) {
    $orderFilter['=STATUS_ID'] = $_POST['statusSystems'];
}
if ($_POST['payed'] && $_POST['payed'] != 'ALL') {
    $orderFilter['=PAYED'] = $_POST['payed'];
}
if ($_POST['canceled'] && $_POST['canceled'] != 'ALL') {
    $orderFilter['=CANCELED'] = $_POST['payed'];
}

if (!$orderFilter) {
    die(json_encode([
        'error' => true,
        'error_text' => 'примените хотя бы 1 фильтр к заказу!',
        'answer' => ''
    ]));
}
$data = [];
switch ($_POST['report']) {
    case 'ROD':
        $data = $reportAdmin->getReportROD($orderFilter);
        break;
    case 'ROG':
        $data = $reportAdmin->getReportROG($orderFilter);
        break;
    case 'RO':
        $data = $reportAdmin->getReportRO($orderFilter);
        break;
    default:
        die(json_encode([
            'error' => true,
            'error_text' => 'нет обработчика для отчета',
            'answer' => ''
        ]));
}

$ordersKey = 0;
$countOrders = 0;
$orders = [];
foreach($data as $key=>$arr){
    if($key == 0) {
        foreach ($arr as $keyHeader=>$valHeader) {
            if($valHeader == "заказы с вычерками"){
                $ordersKey = $keyHeader;
            }
        }
    }
    else {
        foreach ($arr as $keyValue => $val) {
            if ($keyValue == $ordersKey) {
                if(trim($val) != "") {
                    $tempOrders = explode(",", $val);
                    foreach($tempOrders as $order) {
                        if(array_search($order, $orders) === false) {
                            $orders[] = $order;
                            $countOrders++;
                        }
                    }
                }
            }
        }
    }
}

if (!$data) {
    die(json_encode([
        'error' => true,
        'error_text' => 'нет данных для отчета, измените фильтр.',
        'answer' => '',
        'count_orders' => ''
    ]));
}
$csvString = \Helpers\CsvHelper::array2csvString($data);
$answer = [
    'error' => false,
    'error_text' => '',
    'answer' => $csvString,
    'count_orders' => $countOrders
];

die(json_encode($answer, JSON_UNESCAPED_UNICODE));
