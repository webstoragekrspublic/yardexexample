<?php

namespace Helpers;

class TorgovayaMarka
{
//    public static $cacheAllTable = false; //todo сделать кэш на всю таблицу или на отдельные записи
    protected static $indexXMLId = [];
    protected static $cacheItems = [];
    protected const torgovajaMarkaIBlockId = 118;


    static protected function initCacheForAll()
    {
        $columns = ['ID', 'XML_ID', 'NAME'];
        $filter = [
            "IBLOCK_ID" => self::torgovajaMarkaIBlockId
        ];

        $tmQ = \CIBlockElement::getList(
            [],
            $filter,
            false,
            false,
            $columns
        );

        while ($tm = $tmQ->fetch()) {
            self::$cacheItems[$tm['ID']] = $tm;
            self::$indexXMLId[$tm['XML_ID']] = $tm['ID'];
        }
    }

    static function getIdByXMLId($xmlId)
    {
        if (!self::$cacheItems){
            self::initCacheForAll();
        }
        return self::$indexXMLId[$xmlId]??null;

    }

    static function getName($id, $defaultName = null)
    {
        if (!self::$cacheItems){
            self::initCacheForAll();
        }
        return self::$cacheItems[$id]['NAME'] ?? $defaultName;
    }
}