<?php

namespace Helpers;

class Cache
{
    static protected $CACHE_TIME = 3600;

    public static function setCacheTime($seconds) {
        self::$CACHE_TIME = (int)$seconds;
    }

    public static function getProductsIdForCustomFilters($customFilterJson = false, $addProductsIdJson = false, $additionalFilters = false, $catalogIBlockID = 114) {
        $productsId = [];

        $uniqueString = $customFilterJson . $addProductsIdJson . $catalogIBlockID . print_r($additionalFilters, 1);
        $cache = \Bitrix\Main\Data\Cache::createInstance();
        if ($cache->initCache(self::$CACHE_TIME, $uniqueString)) {
            $productsId = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            if (!empty($customFilterJson)) {

                $customFilterItems = \Helpers\ParseConditionHelper::parseCondition(json_decode($customFilterJson, 1));
                if ($customFilterItems) {

                    if ($additionalFilters) {
                        $customFilterItems = array_merge($customFilterItems, $additionalFilters);
                    }

                    if ($customFilterItems) {
                        $customFilterQ = \CIBlockElement::GetList(
                            ["ID" => "ASC"],
                            $customFilterItems,
                            false,
                            false,
                            ['ID', 'ACTIVE', 'AVAILABLE']
                        );
                        while ($row = $customFilterQ->fetch()) {
                            if ($row['ACTIVE'] == 'Y' and $row['AVAILABLE'] == 'Y') {
                                $productsId[$row['ID']] = $row['ID'];
                            }
                        }
                    }
                }
            }
            if (!empty($addProductsIdJson)) {
                $addProductsId = json_decode($addProductsIdJson, 1);

                if ($addProductsId) {
                    $filter = ['ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', 'AVAILABLE' => 'Y', 'IBLOCK_ID' => $catalogIBlockID, 'ID' => $addProductsId];
                    if ($additionalFilters) {
                        $filter = array_merge($filter, $additionalFilters);
                    }

                    $linkGoodsQ = \CIBlockElement::GetList(
                        [],
                        $filter,
                        false,
                        false,
                        ['ID']
                    );

                    while ($row = $linkGoodsQ->fetch()) {
                        $productsId[$row['ID']] = $row['ID'];
                    }
                }
            }

            $cache->endDataCache($productsId);
        }

        return $productsId;
    }
}