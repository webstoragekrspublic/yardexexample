<?php

namespace Helpers;

class SafetyValidation
{
    const adminGroup = [
        '1' => true
    ];
    const managerGroup = [
        '8' => true,
        '13' => true,
        '15' => true,
        '16' => true
    ];
    public static function isAdminOrManger ($userId)
    {
        if (!$userId){
            return false;
        }
        $parameters = [
            'select' => ['GROUP_ID'],
            'filter' => ["USER_ID" => $userId, 'GROUP.ACTIVE'=>'Y']
        ];
        $dbGroup = \Bitrix\Main\UserGroupTable::getList($parameters);
        while ($dbuserGroups = $dbGroup->fetch()){
            if (self::adminGroup[$dbuserGroups['GROUP_ID']] || self::managerGroup[$dbuserGroups['GROUP_ID']]){
                return true;
            }
        }
        return false;
    }
}