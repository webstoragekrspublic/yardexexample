<?php

namespace Helpers;

class CustomTools
{
    public static function getWebpImgSrc($normalImgSrc)
    {
        $resSrc = false;
        $WebpQuality = 80;

        if ($normalImgSrc) {
            $webpImgSrc = static::getWebpRelativePath($normalImgSrc);
            $webpImgPath = $_SERVER['DOCUMENT_ROOT'] . $webpImgSrc;

            if (file_exists($webpImgPath)) {
                $resSrc = $webpImgSrc;
            } else {
                $normalImgPath = $_SERVER['DOCUMENT_ROOT'] . $normalImgSrc;
                $imgInfo = getimagesize($normalImgPath);

                if ($imgInfo) {
                    $imgTypeInt = $imgInfo[2];
                    $imgRes = static::getImageResource($normalImgPath, $imgTypeInt);

                    if ($imgRes) {
                        if (imagewebp($imgRes, $webpImgPath, $WebpQuality)) {
                            $resSrc = $webpImgSrc;
                        }
                    }
                }
            }
        }
        return $resSrc;
    }

    protected static function getImageResource($normalImgPath, $imgTypeInt)
    {
        $im = false;
        if ($normalImgPath && $imgTypeInt) {
            switch ($imgTypeInt) {
                case IMG_JPG:
                    $im = @imagecreatefromjpeg($normalImgPath);
                    break;
                case IMG_PNG:
                case 3:
                    $im = @imagecreatefrompng($normalImgPath);
                    break;
            }
        }
        return $im;
    }

    protected static function getWebpRelativePath($normalImgSrc)
    {
        return $normalImgSrc . '.webp';
    }


    public static function getResizedPictSrc($imgId, $sizes, $type = BX_RESIZE_IMAGE_PROPORTIONAL)
    {
        $imgSrc = '';
        if ($imgId) {
            $imgResized = \CFile::ResizeImageGet($imgId, $sizes, $type, false);
            $imgSrc = $imgResized['src'] ?? '';
        }
        return $imgSrc;
    }

    public static function weightFormat($minQuantityBuy)
    {
        return round($minQuantityBuy, 3) . ' кг';
    }

    /**
     * @param array $fields Array containing the necessary params.
     *    $fields = [
     *      'THEME'     => (string) тема сообщения
     *      'BODY_TEXT' => (string) тело сообщения.
     *    ]
     */
    public static function sendErrorsReport(array $fields, $messageId = 98)
    {
        $emailEventName = 'ERROR_NOTIFICATION';

        \Bitrix\Main\Mail\Event::SendImmediate(array(
            "EVENT_NAME" => $emailEventName,
            "LID" => SITE_ID,
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => $messageId
        ));
    }

    public static function sendServicesEmail(array $fields, $messageId, $files = [])
    {
        $emailEventName = 'CUSTOM_SERVICES';

        $params = [
            "EVENT_NAME" => $emailEventName,
            "LID" => SITE_ID,
            "C_FIELDS" => $fields,
            "MESSAGE_ID" => $messageId
        ];
        if ($files) {
            $params["FILE"] = [];
            foreach ($files as $filePath) {
                $filePathExploded = explode('/', $filePath);
                $fileName = (end($filePathExploded));

                $fileId = \CFile::SaveFile(
                    array(
                        "name" => $fileName,
                        "tmp_name" => $filePath,
                        "old_file" => "0",
                        "del" => "N",
                        "MODULE_ID" => "",
                        "description" => "",
                    ),
                    'mails',  // относительный путь от upload, где будут храниться файлы
                    false,    // ForceMD5
                    false     // SkipExt
                );
                $params["FILE"][] = $fileId;
            }
        }

        $res = \Bitrix\Main\Mail\Event::SendImmediate($params);
        if ($files) {
            foreach ($params["FILE"] as $fileIds) {
                \CFile::Delete($fileId);
            }
        }

        return $res;
    }

    public static function quantityRound($floatQty)
    {
        return round($floatQty, 5);
    }

    public static function getPublicOrderProperties($orderProps, $mod)
    {
        if (strlen($orderProps['PAY_SYSTEM']['NAME'])) {
            $propsOut[] = [
                'NAME' => 'Способ оплаты',
                'VALUE' => $orderProps['PAY_SYSTEM']['NAME']
            ];
        }
        if ($orderProps['SHIPMENT'][0]['DELIVERY_NAME']) {
            $propsOut[] = [
                'NAME' => $orderProps['SHIPMENT'][0]['DELIVERY_NAME'],
                'VALUE' => $orderProps['PRICE_DELIVERY'] > 0 ? $orderProps['PRICE_DELIVERY_FORMATED'] : 'Бесплатно'
            ];
        }
        if ($orderProps['SHOW_ORDER_PROPS']['ADDRESS']) {
            $propsOut[] = [
                'NAME' => $orderProps['SHOW_ORDER_PROPS']['ADDRESS']['NAME'],
                'VALUE' => $orderProps['SHOW_ORDER_PROPS']['ADDRESS']['VALUE']
            ];

        }
        if ($orderProps['SHOW_ORDER_PROPS']['DELIVERY_TIME'] and $orderProps['SHOW_ORDER_PROPS']['DELIVERY_DATE']) {
            $propsOut[] = [
                'NAME' => 'Дата доставки',
                'VALUE' => $orderProps['SHOW_ORDER_PROPS']['DELIVERY_DATE']['VALUE'] . ' ' . $orderProps['SHOW_ORDER_PROPS']['DELIVERY_TIME']['VALUE']
            ];

        }
        if ($orderProps['SHOW_ORDER_PROPS']['PREPARE_CHANGE_FROM_AMOUNT']) {
            $propsOut[] = [
                'NAME' => $orderProps['SHOW_ORDER_PROPS']['PREPARE_CHANGE_FROM_AMOUNT']['NAME'],
                'VALUE' => $orderProps['SHOW_ORDER_PROPS']['PREPARE_CHANGE_FROM_AMOUNT']['VALUE']
            ];

        }
        if (floatval($orderProps["ORDER_WEIGHT"]) && $mod == 'order') {
            $propsOut[] = [
                'NAME' => 'Общий вес',
                'VALUE' => $orderProps['ORDER_WEIGHT_FORMATED']
            ];

        }
        if ($orderProps['PRODUCT_SUM_FORMATED'] != $orderProps['PRICE_FORMATED'] && !empty($orderProps['PRODUCT_SUM_FORMATED']) && $mod == 'order') {
            $propsOut[] = [
                'NAME' => 'Товар на',
                'VALUE' => $orderProps['PRODUCT_SUM_FORMATED']
            ];

        }
        if ((float)$orderProps["TAX_VALUE"] > 0 && $mod == 'order') {
            $propsOut[] = [
                'NAME' => 'НДС',
                'VALUE' => $orderProps["TAX_VALUE_FORMATED"]
            ];

        }
        $propsOut[] = [
            'NAME' => 'Итого',
            'VALUE' => $orderProps['PRICE_FORMATED'],
            'CODE' => 'Bold'
        ];


        return $propsOut;
    }

    public static function phoneStringToDigits($phoneString)
    {
        $tel = preg_replace('/[^\d\+]/', '', $phoneString);
        $tel = preg_replace('/^\+7/', '8', $tel);
        return $tel;
    }

    public static function isDate($string)
    {
        if ($string != '' && preg_match('/(^\d{2}).(\d{2}).(\d{2,4})$/', $string)) {
            return true;
        } else {
            return false;
        }
    }

    public static function isValidDateExpiration($string){
        if (self::isDate($string)){
            $dtime = \DateTime::createFromFormat("!d.m.y", $string);
            if (!$dtime){
                return false;
            }
            $expTs = $dtime->getTimestamp();
            $curTs = time();
            if ($expTs > $curTs){
                return true;
            }
        }
        return false;
    }

    public static function logToFile($info, $infoHeader = '', $fileName = 'log.txt')
    {
        $info = print_r($info, 1);
        if (!$fileName) {
            $fileName = 'log.txt';
        }
        $filePath = __DIR__ . '/' . $fileName;
        $fp = fopen($filePath, 'a+');
        if ($fp) {
            $writeData = '';
            if ($infoHeader) {
                $writeData .= $infoHeader . "\r\n";
            }
            $date = date("Y-m-d H:i:s");
            $writeData .= $date . ' ' . $info . "\r\n\r\n";

            fwrite($fp, $writeData);
        }
    }

    /**
     * @param int $n - число элементов
     * @param array $words - массив из 3х элементов для 1,3х,5ти array('яблоко','яблока','яблок');
     * @return string
     */
    public static function declensionWords(int $n, array $words)
    {
        return ($words[($n = ($n = $n % 100) > 19 ? ($n % 10) : $n) == 1 ? 0 : (($n > 1 && $n <= 4) ? 1 : 2)]);
    }


    /**
     * @param array $array
     * @param $key
     * @return array [$key] = $array
     */
    public static function indexArrayByKey(array $array, $key): array
    {
        $res = [];
        if ($array) {
            foreach ($array as $value) {
                $res[$value[$key]] = $value;
            }
        }
        return $res;
    }

    /**
     * @param array $array
     * @param $key
     * @return array [$key][] = $array
     */
    public static function groupIndexArrayByKey(array $array, $key): array
    {
        $res = [];
        if ($array) {
            foreach ($array as $value) {
                $res[$value[$key]][] = $value;
            }
        }
        return $res;
    }



    /*
     * $data - \Bitrix\Sale\Internals\OrderChangeTable::getList([...])->fetch()['DATA']
     */
    public function unserializeHistoryData($data) {
        $productInfo = unserialize($data);
        if ($productInfo) {
            if ($productInfo['QUANTITY'] > 0) {
                $productInfo['QUANTITY'] = \Helpers\CustomTools::quantityRound($productInfo['QUANTITY']);
            }
        } else if (strlen($data) > 0) { //не валидный формат $data
            if (preg_match('/"QUANTITY";d:([^;]*)/iu', $data, $match)) {
                $quantity = \Helpers\CustomTools::quantityRound($match[1]);
                if (preg_match('/"PRODUCT_ID";s:[^:]*:"([^"]*)"/iu', $data, $match)) {
                    $productId = $match[1];
                    $productInfo = [
                        'PRODUCT_ID' => $productId,
                        'QUANTITY' => $quantity
                    ];
                }
            }
        }
        return $productInfo;
    }
    
    public static function isFastDeliveryEnabled()
    {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        
        return !$request->isAdminSection()
            && $_COOKIE["only_fast_delivery_products"]
            && $_COOKIE["only_fast_delivery_products"] === "true";
    }
}