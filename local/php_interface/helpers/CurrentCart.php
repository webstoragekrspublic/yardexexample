<?php

namespace Helpers;

class CurrentCart
{
    protected static $instance = null;
    protected $basketItems = [];
    protected $basket = [];
    protected $currentCartItems = false;


    protected function __construct()
    {
        $this->init();
    }

    protected function init()
    {
        $this->basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(), \Bitrix\Main\Context::getCurrent()->getSite());
        if ($this->basket) {
            foreach ($this->basket as $basketItem) {
                $this->basketItems[$basketItem->getField('PRODUCT_ID')] = $basketItem;
            }
        }
    }

    public function isProductInCart($productId)
    {
        return isset($this->basketItems[$productId]) ? true : false;
    }

    public function getCartProducts()
    {
        return $this->basketItems;
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new self();
        }
        return static::$instance;
    }


}