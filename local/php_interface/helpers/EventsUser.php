<?php

namespace Helpers;

use Bitrix\Main\ORM\EventResult;
use ExternalApi\B24CRM\B24CRM;

class EventsUser
{
    public function OnAfterUserUpdate(&$arFields)
    {
        if ($arFields['ID']) {
            B24CRM::putUserInUpdateQueue($arFields['ID']);
        }
    }

    public function OnAfterUserAdd(&$arFields)
    {
        if ($arFields['ID']) {
            B24CRM::putUserInUpdateQueue($arFields['ID']);
        }
    }
}
