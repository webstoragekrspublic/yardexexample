<?php

namespace Handlers\DashaMail;

class DashaMailClientSender
{
    function AddHEADClientSender(){
        \Bitrix\Main\Page\Asset::getInstance()->addString(
            '<script type="text/javascript">'.
                'dashamail = window.dashamail || function() { dashamail.queue.push(arguments); };'.
                'dashamail.queue = dashamail.queue || [];'.
                'dashamail("create");'.
                '</script>'.
                '<script src="https://directcrm.dashamail.com/scripts/v2/tracker.js" async></script>',
            true
        );
    }
}

?>