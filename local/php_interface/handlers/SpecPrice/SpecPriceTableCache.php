<?php

namespace Handlers\SpecPrice;


class SpecPriceTableCache
{
    protected $specPriceTableItems = [];
    protected static $instance = null;


    protected function __construct()
    {
        $this->specPriceTableItems = SpecPriceTable::getAllItems();
        $this->specPriceTableItems = array_combine(array_column($this->specPriceTableItems, 'ID_ITEM'), $this->specPriceTableItems);
    }

    public function getAllItems()
    {
        return  $this->specPriceTableItems;
    }

    public function getItem($id)
    {
        return $this->specPriceTableItems[$id];
    }

    public function addItem($id, $idBasketRule, $valueSpecPrice)
    {
        $this->specPriceTableItems[$id]  = array('ID_ITEM' => $id, 'ID_BASKET_RULE' => $idBasketRule, 'VALUE_SPEC_PRICE' => $valueSpecPrice);
    }

    public function updateItem($id, $idBasketRule, $valueSpecPrice)
    {
        $this->specPriceTableItems[$id]  = array('ID_ITEM' => $id, 'ID_BASKET_RULE' => $idBasketRule, 'VALUE_SPEC_PRICE' => $valueSpecPrice);
    }

    public function delItem($id)
    {
        unset($this->specPriceTableItems[$id]);
    }

    static public function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
