<?php

namespace Handlers\SpecPrice;

use Bitrix\Main\Loader;

Loader::includeModule('sale');


class SpecPriceBasketRules
{
    public static function getRuleName($ID)
    {
        return ($ID . "_SPEC_PRICE");
    }
    public static function addRule($item)
    {
        $result = false;
        if ($item) {
            $arFields = [
                "LID" => "s1",
                "NAME" => static::getRuleName($item['ID']),
                "LAST_DISCOUNT" => "N",
                "ACTIVE" => "Y",
                'ACTIONS' => [
                    'CLASS_ID' => 'CondGroup',
                    'DATA' => ['All' => 'AND'],
                    'CHILDREN' => [
                        [
                            'CLASS_ID' => 'ActSaleBsktGrp',
                            'DATA' => [
                                'Type' => 'Closeout', // фиксированная цена
                                'Value' => $item['SPEC_PRICE'], // сама цена
                                'Unit' => 'CurEach', // для каждого товара
                                'Max' => 1,
                                'All' => 'AND',
                                'True' => 'True'
                            ],
                            'CHILDREN' => [
                                [
                                    'CLASS_ID' => 'CondIBElement', // это я взял из документации по методу CCatalogDiscount::Add
                                    'DATA' => [
                                        'logic' => 'Equal', // только равно
                                        'value' => [$item['ID']]  // коду продукта
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                "CONDITIONS" => array(
                    'CLASS_ID' => 'CondGroup',
                    'DATA' =>
                    array(
                        'All' => 'AND',
                        'True' => 'True',
                    ),
                    'CHILDREN' =>
                    array(),
                ),
                "USER_GROUPS" => array(2), // группы пользователей, можно просмотреть в настройки -> пользователи -> группы
                "CURRENCY" => "RUB"
            ];

            $result = \CSaleDiscount::add($arFields);
        }
        return $result;
    }

    public static function updateRule($idBasketRule, $item)
    {
        $result = false;

        if ($idBasketRule && $item) {
            $arFields = [
                'ACTIONS' => [
                    'CLASS_ID' => 'CondGroup',
                    'DATA' => ['All' => 'AND'],
                    'CHILDREN' => [
                        [
                            'CLASS_ID' => 'ActSaleBsktGrp',
                            'DATA' => [
                                'Type' => 'Closeout',
                                'Value' => $item['SPEC_PRICE'],
                                'Unit' => 'CurEach',
                                'Max' => 1,
                                'All' => 'AND',
                                'True' => 'True'
                            ],
                            'CHILDREN' => [
                                [
                                    'CLASS_ID' => 'CondIBElement',
                                    'DATA' => [
                                        'logic' => 'Equal',
                                        'value' => [$item['ID']]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

            $result = \CSaleDiscount::update($idBasketRule, $arFields);
        }
        return $result;
    }

    public static function delRule($idBasketRule)
    {
        if ($idBasketRule) {
            \CSaleDiscount::delete($idBasketRule);
        }
    }

    public static function getRuleID($idItem)
    {
        $idBasketRule = null;
        $db_res = \CSaleDiscount::GetList(
            array("SORT" => "ASC"),
            array(
                "NAME" => static::getRuleName($idItem)
            ),
            false,
            false,
            array('ID')
        );
        if ($ar_res = $db_res->Fetch()) {
            $idBasketRule = $ar_res['ID'];
        }
        return $idBasketRule;
    }
}
