<?php

namespace Handlers\SpecPrice;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class SpecPriceTable extends Entity\DataManager
{
    public static function getTableName()
    {
        //SELECT * FROM custom_specprice_basketrules
        return 'custom_specprice_basketrules';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID_ITEM', array(
                'primary' => true,
                'required' => true,
            )),
            new Entity\IntegerField('ID_BASKET_RULE', array(
                'required' => true,
                'unique' => true
            )),
            new Entity\StringField('VALUE_SPEC_PRICE', array(
                'required' => true,
            )),

        );
    }

    public static function createDbTable()
    {
        if (!Application::getConnection(SpecPriceTable::getConnectionName())->isTableExists(
            Base::getInstance('\Handlers\SpecPrice\SpecPriceTable')->getDBTableName()
        )) {
            Base::getInstance('\Handlers\SpecPrice\SpecPriceTable')->createDbTable();
            Application::getConnection(SpecPriceTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ID_BASKET_RULE`);');
        }
    }

    public static function dropDbTable()
    {
        if (Application::getConnection(SpecPriceTable::getConnectionName())->isTableExists(
            Base::getInstance('\Handlers\SpecPrice\SpecPriceTable')->getDBTableName()
        )) {
            Application::getConnection(SpecPriceTable::getConnectionName())->queryExecute('drop table if exists ' . Base::getInstance('\Handlers\SpecPrice\SpecPriceTable')->getDBTableName());
        }
    }

    public static function addItem($idItem, $idBasketRule, $valueSpecPrice)
    {
        $result = false;
        if ($idItem and $idBasketRule) {
            try {
                $insertResult = static::add([
                    'ID_ITEM' => $idItem,
                    'ID_BASKET_RULE' => $idBasketRule,
                    'VALUE_SPEC_PRICE' => $valueSpecPrice,
                ]);
                $result = $insertResult->isSuccess();
            } catch (\Exception $exception) {;
            }
        }
        return $result;
    }
    public static function updateItem($idItem, $idBasketRule, $valueSpecPrice)
    {
        $result = false;
        if ($idItem and $idBasketRule) {
            try {
                $insertResult = static::update($idItem, [
                    'ID_BASKET_RULE' => $idBasketRule,
                    'VALUE_SPEC_PRICE' => $valueSpecPrice,
                ]);
                $result = $insertResult->isSuccess();
            } catch (\Exception $exception) {;
            }
        }
        return $result;
    }

    public static function delItem($idItem)
    {
        if ($idItem) {
            static::delete($idItem);
        }
    }

    public static function getItemFor($idItem)
    {
        return static::getByPrimary($idItem)->fetch();
    }

    public static function getAllItems()
    {
        return static::getList(array('select' => array('*')))->fetchAll();
    }
}
