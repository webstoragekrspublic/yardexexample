<?php

namespace Handlers\SpecPrice;

class SpecPriceController
{
    const ELEMENT_SPEC_PRICE_ID = '1134';

    public static function setActualSpecPriceRule($ID, $SPEC_PRICE, $ACTIVE = 'Y')
    {
        if (!empty($SPEC_PRICE) && $ACTIVE == 'Y') {
            $specPriceCache = SpecPriceTableCache::getInstance();
            if ($specPriceTableItem = $specPriceCache->getItem($ID)) {
                if ($specPriceTableItem['VALUE_SPEC_PRICE'] != $SPEC_PRICE) {
                    self::update($ID, $SPEC_PRICE, $specPriceTableItem['ID_BASKET_RULE']);
                }
            } else {
                self::add($ID, $SPEC_PRICE);
            }
        } else {
            self::delete($ID);
        }
    }

    public static function update($ID, $SPEC_PRICE, $ID_BASKET_RULE)
    {
        $specPriceCache = SpecPriceTableCache::getInstance();
        if ($ID_BASKET_RULE = SpecPriceBasketRules::updateRule($ID_BASKET_RULE, array('ID' => $ID, 'SPEC_PRICE' => $SPEC_PRICE))) {
            SpecPriceTable::updateItem($ID, $ID_BASKET_RULE, $SPEC_PRICE);
            $specPriceCache->updateItem($ID, $ID_BASKET_RULE, $SPEC_PRICE);
        }
        return $ID_BASKET_RULE;
    }

    public static function add($ID, $SPEC_PRICE)
    {
        $specPriceCache = SpecPriceTableCache::getInstance();
        if ($ID_BASKET_RULE = SpecPriceBasketRules::addRule(array('ID' => $ID, 'SPEC_PRICE' => $SPEC_PRICE))) {
            SpecPriceTable::addItem($ID, $ID_BASKET_RULE, $SPEC_PRICE);
            $specPriceCache->addItem($ID, $ID_BASKET_RULE, $SPEC_PRICE);
        }
        return $ID_BASKET_RULE;
    }

    public static function delete($ID)
    {
        $specPriceCache = SpecPriceTableCache::getInstance();
        $specPriceTableItem = $specPriceCache->getItem($ID);
        SpecPriceBasketRules::delRule($specPriceTableItem['ID_BASKET_RULE']);
        SpecPriceTable::delItem($ID);
        $specPriceCache->delItem($ID);
    }
}
