<?

namespace Handlers\ActualSalesTrack;

use Bitrix\Main\Loader;

class SaleSwitcher
{
    static function setSaleActive($SALE_ID)
    {
        $obSaleElement = new \CIBlockElement();
        $obSaleElement->Update($SALE_ID, array('ACTIVE' => 'Y')); // активация
    }
    static function setSaleUnactive($SALE_ID)
    {
        $obSaleElement = new \CIBlockElement();
        $obSaleElement->Update($SALE_ID, array('ACTIVE' => 'N')); // деактивация
    }
    static function switchSaleActiveByProductsAvailable()
    {
        $arProducts = SaleTracker::getAllProductsFromSaleProperties();
        foreach ($arProducts as $arProductsKey => $arProduct) {
            if ($arProduct['PRODUCTS_COUNT'] > 0 && $arProduct['ACTIVE'] == 'N') {
                static::setSaleActive($arProduct['ID']);
            } elseif ($arProduct['PRODUCTS_COUNT'] == 0 && $arProduct['ACTIVE'] == 'Y') {
                static::setSaleUnactive($arProduct['ID']);
            }
        }
    }
}
