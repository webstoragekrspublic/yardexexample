<?

namespace Handlers\ActualSalesTrack;

use Bitrix\Main\Loader;

class SaleTracker
{
    var $IBLOCK_IDS = ['109', '120'];
    static public function getAllSaleIDsForTrack()
    {
        $arSelect = array('ID', 'IBLOCK_ID');
        $arFilter = array("IBLOCK_ID" => $IBLOCK_IDS, '=PROPERTY_CHECK_GOODS_AVAILABLE' => '1');
        $res = \CIBlockElement::GetList(
            [],
            $arFilter,
            false,
            false,
            $arSelect
        );
        $arSalesIDs = [];
        while ($row = $res->getNext()) {
            $arSalesIDs[] = $row['ID'];
        }
        return $arSalesIDs;
    }

    static public function getSaleProperties($SALE_ID)
    {
        $arSelect = array('ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_LINK_GOODS_FILTER', 'PROPERTY_LINK_GOODS', 'ACTIVE', 'PROPERTY_CHECK_GOODS_AVAILABLE');
        $arFilter = array("IBLOCK_ID" => $IBLOCK_IDS, 'ID' => $SALE_ID);
        $res = \CIBlockElement::GetList(
            [],
            $arFilter,
            false,
            false,
            $arSelect
        );
        $arItem = [];
        $arItemLinkGoods = [];
        while ($row = $res->getNext()) {
            if (!$arItem) {
                $arItem = [
                    'ID' => $row['ID'],
                    'IBLOCK_ID' => $row['IBLOCK_ID'],
                    'ACTIVE' => $row['ACTIVE'],
                    'NAME' => $row['NAME'],
                    'LINK_GOODS_CUSTOM_FILTER' => $row['~PROPERTY_LINK_GOODS_FILTER_VALUE'],
                    'LINK_CHECK_GOODS_AVAILABLE' => $row['PROPERTY_CHECK_GOODS_AVAILABLE_VALUE']
                ];
            }
            if ($row['PROPERTY_LINK_GOODS_VALUE']) {
                $arItemLinkGoods[] = $row['PROPERTY_LINK_GOODS_VALUE'];
            }
        }
        $arItem['LINK_GOODS'] = $arItemLinkGoods;

        return $arItem;
    }

    static public function getProductsFromSaleProperties($SALE_ID)
    {
        $arItem = static::getSaleProperties($SALE_ID);
        $productIDs = \Helpers\Cache::getProductsIdForCustomFilters($arItem['LINK_GOODS_CUSTOM_FILTER'], json_encode($arItem['LINK_GOODS']));
        $arProduct = [
            'ID' => $arItem['ID'],
            'NAME' => $arItem['NAME'],
            'ACTIVE' => $arItem['ACTIVE'],
            'IBLOCK_ID' => $arItem['IBLOCK_ID'],
            'PRODUCTS_COUNT' => count($productIDs),
            'PRODUCT_IDs' => $productIDs
        ];
        return $arProduct;
    }
    static public function getAllProductsFromSaleProperties()
    {
        $arSalesIDs = static::getAllSaleIDsForTrack();
        $arProducts = array_map(['self', 'getProductsFromSaleProperties'], $arSalesIDs);
        return $arProducts;
    }

    static public function checkNewUserSaleActivity()
    {
        $IBLOCK_ID = 109;
        $SALE_ID = 42324;
        $arFilter = array(
            'IBLOCK_ID' => $IBLOCK_ID,
            'ACTIVE_DATE' => 'Y',
            'ACTIVE' => 'Y',
            'ID' => $SALE_ID
        );
        $arSelect = array('ID',);
        $res = \CIBlockElement::GetList(array(), $arFilter, false, false,  $arSelect);
        if ($res->GetNextElement()) {
            return true;
        }
        return false;
    }
}
