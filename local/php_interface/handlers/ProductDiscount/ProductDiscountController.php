<?

namespace Handlers\ProductDiscount;

use Bitrix\Main\Loader;
use Handlers\TotalPrice\TotalPrice;

Loader::includeModule('iblock');

class ProductDiscountController
{
    static public function markAllDiscountProducts() //массовая отметка товаров со скидкой из всех правил корзины
    {
        $arPrices = ProductDiscountParser::getAllDiscountProductsPrices();
        array_walk($arPrices, ['self', 'markDiscount']);
    }

    static public function markDiscountProduct($product_id) //одиночная отметка товара со скидкой
    {
        $arPrice = ProductDiscountParser::getProductPrice($product_id);
        self::markDiscount($arPrice);
    }

    static public function markDiscount($arPrice) //выполнение отметки товара со скидкой 
    {
        if ($arPrice['RESULT_PRICE']['DISCOUNT'] > 0) {
            \CIBlockElement::SetPropertyValuesEx(
                $arPrice['ID'],
                $arPrice['IBLOCK_ID'],
                array(
                    'DISCOUNTED' => [['VALUE' => 477]],
                    'DISCOUNT_AMOUNT' => [['VALUE' => $arPrice['RESULT_PRICE']['DISCOUNT']]],
                    'DISCOUNT_AMOUNT_PERCENT' => [['VALUE' => $arPrice['RESULT_PRICE']['PERCENT']]],
                )
            );
            //\CIBlockElement::SetPropertyValuesEx($arPrice['ID'], $arPrice['IBLOCK_ID'], array('DISCOUNTED' => [['VALUE' => '']], 'DISCOUNT_AMOUNT' => [['VALUE' => '']]));
        } else {
            \CIBlockElement::SetPropertyValuesEx(
                $arPrice['ID'],
                $arPrice['IBLOCK_ID'],
                array(
                    'DISCOUNTED' => [['VALUE' => '']],
                    'DISCOUNT_AMOUNT' => [['VALUE' => '']],
                    'DISCOUNT_AMOUNT_PERCENT' => [['VALUE' => '']],
                )
            );
        }
        TotalPrice::set($arPrice['ID'], $arPrice['IBLOCK_ID'], $arPrice['RESULT_PRICE']['DISCOUNT_PRICE']);
    }

    static public function unMarkOutdateDiscountProducts()
    {
        //Доп обработка для удаления отметок о скидках с товаров которые больше не по скидке
        $arSelect = array('ID', 'IBLOCK_ID',  'PROPERTY_SPEC_PRICE', 'PROPERTY_DISCOUNTED');
        $arFilter = array("IBLOCK_ID" => IntVal(114),  "PROPERTY_DISCOUNTED" => "477",);
        $res = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        $arFields = [];
        while ($ob = $res->GetNextElement()) {
            $arFields[] = $ob->GetFields();
        }
        foreach ($arFields as $arField) {
            if (!isset($arField['PROPERTY_SPEC_PRICE_VALUE'])) {
                self::markDiscountProduct($arField['ID']);
            }
        }
    }
}
