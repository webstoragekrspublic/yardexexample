<?

namespace Handlers\ProductDiscount;

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

Loader::includeModule('iblock');

class ProductDiscountParser
{
    public static function getAllDiscountProductsPrices() // возвращает массив цен для товаров из всех существующих правил корзины
    {
        $allActions = self::getAllBasketRuleActions();
        $productsIDs = self::getProductIDsFrom($allActions);
        $arrPrices = self::getProductPriceArray($productsIDs);
        return $arrPrices;
    }

    public static function getAllBasketRuleActions() // возвращает применяемые действия из правил корзины    
    {
        if (!Loader::includeModule('sale')) throw new SystemException('Не подключен модуль Sale');
        global $USER;
        $arUserGroups = $USER->GetUserGroupArray();

        if (!is_array($arUserGroups)) $arUserGroups = array($arUserGroups);
        $actionsNotTemp = \CSaleDiscount::GetList(array("ID" => "ASC"), array("USER_GROUPS" => $arUserGroups), false, false, array("ID"));
        while ($actionNot = $actionsNotTemp->fetch()) {
            $actionIds[] = $actionNot['ID'];
        }

        $actions = \Bitrix\Sale\Internals\DiscountTable::getList(array(
            'select' => array("ID", "ACTIONS_LIST",),
            'filter' => array(
                "ACTIVE" => "Y", "USE_COUPONS" => "N", "DISCOUNT_TYPE" => "P", "LID" => SITE_ID,
                "ID" => $actionIds,
            )
        ));
        $arrActions = array();
        while ($arrAction = $actions->fetch()) {
            $arrActions[$arrAction['ID']] = $arrAction['ACTIONS_LIST'];
        }
        return $arrActions;
    }

    public static function getProductIDsFrom($arrActions) // возвращает уникальные ID товаров из массива действий правил корзины 
    {
        if (!Loader::includeModule('iblock')) throw new SystemException('Не подключен модуль iblock');

        $parsedArrActionProductIDs = [];
        foreach ($arrActions as $actionID => $arAction) {
            foreach ($arAction['CHILDREN'] as $keyConditionID => $condition) {
                $productsIDs = [];
                foreach ($condition['CHILDREN'] as $keyConditionSub => $conditionSub) {
                    if ($conditionSub['CLASS_ID'] == 'ActSaleSubGrp') {
                        foreach ($conditionSub['CHILDREN'] as $keyConditionSubElem => $conditionSubElem) {
                            if ($conditionSubElem['CLASS_ID'] == 'CondIBElement') {
                                $productsIDs = $conditionSubElem['DATA']['value'];
                            } elseif ($conditionSubElem['CLASS_ID'] == 'CondIBSection') {
                                //на основном сайте есть условие в котором скидка для всего раздела, поэтому нужно достать ID товаров
                                $arSelect = array('ID');
                                $arFilter = array("IBLOCK_ID" => IntVal(114), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "SECTION_ID" => $conditionSubElem['DATA']['value']);
                                $res = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                                while ($ob = $res->GetNextElement()) {
                                    $productsIDs[] = $ob->GetFields()['ID'];
                                }
                            }
                        }
                    } elseif ($conditionSub['CLASS_ID'] == 'CondIBElement') {
                        $productsIDs = $conditionSub['DATA']['value'];
                    } elseif ($conditionSub['CLASS_ID'] == 'CondBsktAppliedDiscount') { //Условие: Были применены скидки (Y/N)
                        $arSelect = array('ID');
                        $arFilter = array("IBLOCK_ID" => IntVal(114), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y",);
                        $res = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
                        while ($ob = $res->GetNextElement()) {
                            $productsIDs[] = $ob->GetFields()['ID'];
                        }
                    }
                }
                $parsedArrActionProductIDs[] = $productsIDs;
            }
        }
        $arrProductIDs = array_unique(call_user_func_array('array_merge', $parsedArrActionProductIDs));
        return $arrProductIDs;
    }

    public static function getProductPrice($productsID) // возвращает рассчитаные цены товара
    {
        global $USER;
        $arPrice = \CCatalogProduct::GetOptimalPrice($productsID, '1', $USER->GetUserGroupArray(), "N");
        return [
            'ID' => $productsID,
            'IBLOCK_ID' => $arPrice['PRICE']['ELEMENT_IBLOCK_ID'],
            'RESULT_PRICE' => $arPrice['RESULT_PRICE']
        ];
    }

    public static function getProductPriceArray($productsIDs) // возвращает массив рассчитаных цен всех товаров 
    {
        return array_map(['self', 'getProductPrice'], $productsIDs);
    }
}
