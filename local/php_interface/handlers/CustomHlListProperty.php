<?php
	namespace Handlers;

	use Bitrix\Main\Loader;
	use Bitrix\Main\Localization\Loc;
	use Bitrix\Highloadblock as HL;

	Loader::includeModule('highloadblock');
	Loc::loadMessages(__FILE__);

	class CustomHlListProperty extends \CIBlockPropertyDirectory
	{
		/**
		 * Returns custom directory property
		 *
		 * @return array
		 */
		public static function GetUserTypeDescription()
		{
			return [
				'PROPERTY_TYPE' => 'S',
				'USER_TYPE' => 'price_directory',
				'DESCRIPTION' => 'Справочник с кол-вом', // Loc
				'GetSettingsHTML' => array(__CLASS__, 'GetSettingsHTML'),
				'GetPropertyFieldHtml' => array(__CLASS__, 'GetPropertyFieldHtml'),
				'GetPropertyFieldHtmlMulty' => array(__CLASS__, 'GetPropertyFieldHtmlMulty'),
				'PrepareSettings' => array(__CLASS__, 'PrepareSettings'),
				'GetAdminListViewHTML' => array(__CLASS__, 'GetAdminListViewHTML'),
				'GetPublicViewHTML' => array(__CLASS__, 'GetPublicViewHTML'),
				'GetPublicEditHTML' => array(__CLASS__, 'GetPublicEditHTML'),
				'GetAdminFilterHTML' => array(__CLASS__, 'GetAdminFilterHTML'),
				'GetExtendedValue' => array(__CLASS__, 'GetExtendedValue'),
				'GetSearchContent' => array(__CLASS__, 'GetSearchContent'),
				'AddFilterFields' => array(__CLASS__, 'AddFilterFields'),
				'ConvertToDB' => array(__CLASS__, 'SaveData'),
				'ConvertFromDB' => array(__CLASS__, 'GetData'),
			];
		}

		public static function GetPropertyFieldHtmlMulty ($arProperty, $value, $strHTMLControlName)
		{
			$max_n = 0;
			$values = array();
			if(is_array($value))
			{
				$match = array();
				foreach($value as $property_value_id => $arValue)
				{
					$values[$property_value_id] = $arValue["VALUE"];
					if(preg_match("/^n(\\d+)$/", $property_value_id, $match))
					{
						if($match[1] > $max_n)
							$max_n = intval($match[1]);
					}
				}
			}

			$settings = parent::PrepareSettings($arProperty);
			$size = ($settings["size"] > 1 ? ' size="'.$settings["size"].'"' : '');
			$width = ($settings["width"] > 0 ? ' style="width:'.$settings["width"].'px"' : ' style="margin: 0 10px 3px 0"');

			if($settings["multiple"]=="Y")
			{
				$options = parent::GetOptionsHtml($arProperty, $values);
				$html = '<select multiple name="'.$strHTMLControlName["VALUE"].'[]"'.$size.$width.'>';
				$html .= $options;
				$html .= '</select>';
			}
			else
			{
				if(end($values) != "" || substr(key($values), 0, 1) != "n")
					$values["n".($max_n+1)] = "";

				$name = $strHTMLControlName["VALUE"]."VALUE";

				$html = '<table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb'.md5($name).'">';
				foreach($values as $property_value_id=>$value)
				{
					$html .= '<tr><td>';

					$options = parent::GetOptionsHtml($arProperty, array($value['ID']));

					$html .= '<select name="'.$strHTMLControlName["VALUE"].'['.$property_value_id.'][VALUE][ID]"'.$size .$width.'>';
					$html .= $options;
					$html .= '</select>';

					$html .= ' Кол-во: <input name="'.$strHTMLControlName["VALUE"].'['.$property_value_id.'][VALUE][QUANTITY]"'
						. 'type="text" style="margin-bottom:3px" value="'.$value['QUANTITY'].'">';

					$html .= '</td></tr>';

				}
				$html .= '</table>';

				$html .= '<input type="button" value="'.Loc::getMessage("HIBLOCK_PROP_DIRECTORY_MORE").'" onclick="if(window.addNewRow){addNewRow(\'tb'.md5($name).'\', -1)}else{addNewTableRow(\'tb'.md5($name).'\', 1, /\[(n)([0-9]*)\]/g, 2)}">';
			}
			return  $html;
		}

		public static function SaveData($arProperty, &$value)
		{
			if(empty($value['VALUE']['QUANTITY']))
				return false;

			if(is_array($value) && array_key_exists('VALUE', $value))
			{
				$value = [
					"VALUE" => serialize($value["VALUE"])
				];

			}

			return $value;
		}

		public static function GetData($arProperty, &$value)
		{
			if(!is_array($value["VALUE"]))
			{
				$value = [
					"VALUE" => unserialize($value["VALUE"]),
				];
			}

			return $value;
		}
	}