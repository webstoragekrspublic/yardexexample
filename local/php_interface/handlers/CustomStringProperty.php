<?php
    namespace Handlers;

    use Bitrix\Main\Loader;

    Loader::includeModule('iblock');

	class CustomStringProperty
	{

		public static function GetUserTypeDescription()
		{
			return array(
				'PROPERTY_TYPE'            => 'S',
				'USER_TYPE'                => 'property_0',
				'DESCRIPTION'              => 'Наборы (Тип,Имя,Значение)',
				'GetPropertyFieldHtml'     => array(__CLASS__, 'GetPropertyFieldHtml'),
				'ConvertToDB'              => array(__CLASS__, 'ConvertToDB'),
				'ConvertFromDB'            => array(__CLASS__, 'ConvertFromDB')
			);
		}

		public static function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
		{

			$arNewInput = [
				'TYPE',
				'NAME',
				'VALUE'
			];

			$arNewInputName = [
				'TYPE' => 'Тип',
				'NAME' => 'Имя',
				'VALUE' => 'Значение'
			];


			$html = '<div
						style="margin-bottom: 15px;
						border: 1px solid #d0d7d8;
						background: #fafcfc;
						padding: 12px 10px 12px 10px;
						width: 100%%"
					>';

			foreach($arNewInput as $input){
				$html .= ' <input 
								type="text" 
								name="'.$strHTMLControlName["VALUE"].'['.$input.']" 
								placeholder="'.$arNewInputName[$input].'" 
								size="60" 
								value="'.$value["VALUE"][$input].'" 
								style="margin-bottom: 5px;"
							>  ';
			}



			$html .= '</div>';

			return $html;
	   	}

		public static function ConvertToDB($arProperty, $value)
		{

			$return = false;
			if(empty($value['VALUE']['TYPE']) && empty($value['VALUE']['NAME']) && empty($value['VALUE']['VALUE']))
				return $return;

			if(is_array($value) && array_key_exists("VALUE", $value)){
				$return = [
					"VALUE" => serialize($value["VALUE"])
				];

				if(strlen(trim($value["DESCRIPTION"])) > 0)
					$return["DESCRIPTION"] = trim($value["DESCRIPTION"]);
			}
		   	return $return;
	   	}

		public static function ConvertFromDB($arProperty, $value)
		{
			$return = false;
			if(!is_array($value["VALUE"])){
				$return = [
					"VALUE" => unserialize($value["VALUE"])
				];

				if($value["DESCRIPTION"])
					$return["DESCRIPTION"] = trim($value["DESCRIPTION"]);
			}
			return $return;
		}

	}
