<?

namespace Handlers\TotalPrice;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

class TotalPrice
{
    static public function set($productID, $iblockID, $totalPrice)
    {
        \CIBlockElement::SetPropertyValuesEx($productID, $iblockID, array('TOTAL_PRICE_ALL' => [['VALUE' => $totalPrice]]));
    }
}
