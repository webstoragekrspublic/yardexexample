<?

namespace Handlers\CouponGenerator;

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;

class CouponGenerator
{
    const REGISTER_DISCOUNT_ID = 990;
    const FIRST_ORDER_DISCOUNT_ID = 991;
    const ACTIVE_PERIOD = '14 days';

    static public function generate($userID, $discountID)
    {
        if (!Loader::includeModule('sale')) throw new SystemException('Не подключен модуль Sale');
        $activeFrom = new \Bitrix\Main\Type\DateTime();
        $activeTo = new \Bitrix\Main\Type\DateTime();
        $activeTo = $activeTo->add(static::ACTIVE_PERIOD);

        $coupon = \Bitrix\Sale\Internals\DiscountCouponTable::generateCoupon(true);
        $addDb = \Bitrix\Sale\Internals\DiscountCouponTable::add(array(
            'DISCOUNT_ID' => $discountID,
            'COUPON' => $coupon,
            'TYPE' => \Bitrix\Sale\Internals\DiscountCouponTable::TYPE_ONE_ORDER,
            'ACTIVE_FROM' => $activeFrom,
            'ACTIVE_TO' => $activeTo,
            'MAX_USE' => 1,
            'USER_ID' => $userID,
            'DESCRIPTION' => ''
        ));
        if ($addDb->isSuccess()) {
            return $coupon;
        }
    }
}
