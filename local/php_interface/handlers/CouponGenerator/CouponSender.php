<?

namespace Handlers\CouponGenerator;

use Bitrix\Main\Loader;

class CouponSender
{
    const REGISTER_TEMPLATE_ID = '122';
    const FIRST_ORDER_TEMPLATE_ID = '123';

    static public function sendCoupon($login, $email, $coupon, $templateID)
    {
        $arEventFields = array(
            "LOGIN" => $login,
            "EMAIL"  => $email,
            "COUPON" => $coupon,
            "ACTIVE_TO" => (new \Bitrix\Main\Type\DateTime())->add(CouponGenerator::ACTIVE_PERIOD)->format("d.m.Y")
        );
        \CEvent::Send("SEND_COUPON", "s1", $arEventFields, 'N', $templateID);
    }
}
