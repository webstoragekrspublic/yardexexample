<?

namespace Handlers\CouponGenerator;

use Bitrix\Main\Loader;
use Handlers\ActualSalesTrack\SaleTracker;

class CouponEvents
{
    static public function onBeforeUserUpdate(&$arFields)
    {
        if (isset($arFields['ACTIVE']) && $arFields['ACTIVE'] == 'Y' && isset($arFields['ID']) && SaleTracker::checkNewUserSaleActivity()) :
            $rsUser = \CUser::GetByID($arFields['ID']);
            $arUser = $rsUser->Fetch();
            if (!$arUser["LAST_LOGIN"]) {
                CouponSender::sendCoupon(
                    $arUser['LOGIN'],
                    $arUser['EMAIL'],
                    CouponGenerator::generate($arUser['ID'], CouponGenerator::REGISTER_DISCOUNT_ID),
                    CouponSender::REGISTER_TEMPLATE_ID
                );
            }
        endif;
    }

    static public function onSaleStatusOrder($id, $val)
    {
        if ($val == 'F' && SaleTracker::checkNewUserSaleActivity()) :
            $orderFields = \CSaleOrder::GetByID($id);
            if (!Loader::includeModule('sale')) throw new SystemException('Не подключен модуль Sale');
            $arFilter = array("USER_ID" => $orderFields['USER_ID']);
            $dbOrders = \CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
            $orderCount = 0;
            while ($arOrder = $dbOrders->Fetch()) {
                $arOrder['STATUS_ID'] == 'F' ? $orderCount++ : $orderCount; //получение количества завершенных заказов
            }
            if ($orderCount == 1) {
                $couponList = \Bitrix\Sale\Internals\OrderCouponsTable::getList(array(
                    'select' => array('DATA'),
                    'filter' => array('=ORDER_ID' => $id)
                ));
                $coupon = $couponList->fetch(); // получение купона примененного к заказу
                if ($coupon['DATA']['DISCOUNT_ID'] == CouponGenerator::REGISTER_DISCOUNT_ID) {
                    //если этот заказ единственный завершенный и применен промокод за регистрацию, то отправить второй
                    CouponSender::sendCoupon(
                        $orderFields['USER_LOGIN'],
                        $orderFields['USER_EMAIL'],
                        CouponGenerator::generate($orderFields['USER_ID'], CouponGenerator::FIRST_ORDER_DISCOUNT_ID),
                        CouponSender::FIRST_ORDER_TEMPLATE_ID
                    );
                }
            }
        endif;
    }
}
