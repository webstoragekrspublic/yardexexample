<?php

namespace Handlers;

use Bitrix\Main\Loader;
use Helpers\CustomTools;
use Helpers\GlobalStorage;

Loader::includeModule('iblock');

class SearchEvents
{
    public function BeforeIndex($arFields)
    {
        //добавляем в поисковой индекс Торговую Марку у товара
        if ($arFields["MODULE_ID"] == "iblock" and $arFields["PARAM2"] == 114 and is_numeric($arFields["ITEM_ID"])) {
            $torgMarkaCode = 'TORGOVAJAMARKA';
            $dbProps = \CIBlockElement::GetProperty(
                $arFields["PARAM2"],
                $arFields["ITEM_ID"],
                [],
                ["CODE" => $torgMarkaCode]
            );
            $arProps = $dbProps->Fetch();

            if ($arProps) {
                $name = static::getElementName($arProps["VALUE"]);

                if ($name) {
                    $arFields["TITLE"] .= " " . $name;
                }
            }

            //вырубаем поиск по всему, кроме названия
            $arFields["BODY"] = '';
        }
        return $arFields;
    }

    static protected function getElementName($idElement)
    {
        $cacheKey = 'SearchEvents::getElementName-' . $idElement;
        $name = GlobalStorage::get($cacheKey);
        if (!isset($name)) {
            $name = false;
            if ($idElement) {
                $elementQ = \CIBlockElement::GetList([], ['ID' => $idElement], false, [], ['ID', 'NAME']);

                if ($element = $elementQ->Fetch()) {
                    $name = $element["NAME"];
                }
            }
            GlobalStorage::set($cacheKey, $name);
        }
        return $name;
    }
}
