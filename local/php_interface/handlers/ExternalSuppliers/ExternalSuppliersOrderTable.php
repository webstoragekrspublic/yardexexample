<?php

namespace Handlers\ExternalSuppliers;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;
use Bitrix\Main\Loader;

class ExternalSuppliersOrderTable extends Entity\DataManager
{
    public const STATUS_NEED_PROCESS = 1;
    public const STATUS_UPDATED = 2;
    public const STATUS_COMPLETED = 3;
    public const STATUS_CROSSED_OUT = 4;

    public const apiStatusMap = [
        self::STATUS_NEED_PROCESS => [
            'id' => self::STATUS_NEED_PROCESS,
            'apiName' => 'need_process',
            'description' => 'не обработан'
        ],
        self::STATUS_UPDATED => [
            'id' => self::STATUS_UPDATED,
            'apiName' => 'updated',
            'description' => 'обработан поставщиком'
        ],
        self::STATUS_COMPLETED => [
            'id' => self::STATUS_COMPLETED,
            'apiName' => 'completed',
            'description' => 'завершен'
        ],
        self::STATUS_CROSSED_OUT => [
            'id' => self::STATUS_CROSSED_OUT,
            'apiName' => 'crossed_out',
            'description' => 'вычеркнуто все'
        ],
    ];

    public function __construct()
    {
        Loader::includeModule("sale");
    }

    public static function getTableName()
    {
        return 'custom_external_suppliers_order';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('ID_ORDER', array(
                'required' => true,
            )),
            new Entity\StringField('SUPPLIER_CODE', array(
                'required' => true,
            )),
            new Entity\ReferenceField(
                'ORDER',
                'Bitrix\Sale\Internals\Order',
                array('=this.ID_ORDER' => 'ref.ID'),
            ),
            new Entity\IntegerField('STATUS_ID', array(
                'required' => true,
                'default_value' => static::STATUS_NEED_PROCESS,
            )),

            /* Не создавалась отдельная таблица т.к. в планах полностью изменить обмен битрикс/1с/поставщик
             * ExternalSupplierBasketProductDTO[]
            */
            new Entity\TextField('PRODUCTS_INFO', array(
                'save_data_modification' => function () {
                    return array(
                        function ($arr) {
                            $res = [];
                            if (isset($arr) && is_array($arr)) {
                                foreach ($arr as $item) {
                                    if ($item instanceof ExternalSupplierBasketProductDTO) {
                                        $res[] = $item->toExternal();
                                    }
                                }
                            }
                            return json_encode($res);
                        }
                    );
                },
                'fetch_data_modification' => function () {
                    return array(
                        function ($value) {
                            $arr = json_decode($value,1);
                            $res = [];
                            if ($arr) {
                                foreach ($arr as $item){
                                    $res[] = ExternalSupplierBasketProductDTO::newFromDBRow($item);
                                }
                            } else {
                                $res = [];
                            }
                            return $res;
                        }
                    );
                }
            )),
        );
    }

    public static function getStatusCodeByName($codeName)
    {
        foreach (self::apiStatusMap as $val) {
            if ($val['apiName'] === $codeName) {
                return $val['id'];
            }
        }
        return null;
    }

    public static function getStatusNameByCode($id)
    {
        return self::getPropertyByCode($id, 'apiName');
    }

    public static function getDescriptionByCode($id)
    {
        return self::getPropertyByCode($id, 'description');
    }

    public static function getPropertyByCode($id, $propName)
    {
        return isset(self::apiStatusMap[$id]) ? self::apiStatusMap[$id][$propName] : false;
    }

    public static function createDbTable()
    {
        if (!Application::getConnection(ExternalSuppliersOrderTable::getConnectionName())->isTableExists(
            Base::getInstance('\Handlers\ExternalSuppliers\ExternalSuppliersOrderTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\Handlers\ExternalSuppliers\ExternalSuppliersOrderTable')->createDbTable();
            Application::getConnection(ExternalSuppliersOrderTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD UNIQUE `uniq` (`ID_ORDER`, `SUPPLIER_CODE`);');
        }
    }

    public static function dropDbTable()
    {
        if (Application::getConnection(ExternalSuppliersOrderTable::getConnectionName())->isTableExists(
            Base::getInstance('\Handlers\ExternalSuppliers\ExternalSuppliersOrderTable')->getDBTableName()
        )
        ) {
            Application::getConnection(ExternalSuppliersOrderTable::getConnectionName())->
            queryExecute('drop table if exists ' . Base::getInstance('\Handlers\ExternalSuppliers\ExternalSuppliersOrderTable')->getDBTableName());
        }
    }

    public static function isValidStatusId($statusId)
    {
        return (bool)self::getStatusNameByCode($statusId);
    }
}