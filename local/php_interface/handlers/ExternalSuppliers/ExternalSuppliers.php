<?

namespace Handlers\ExternalSuppliers;

use Bitrix\Sale;
use Helpers\Constants;
use Helpers\CustomTools;

class ExternalSuppliers
{
    public static function onOrderSave($orderId)
    {
        $order = Sale\Order::load($orderId);
        $basket = $order->getBasket();
        $basketProducts = [];
        $basketSuppliersProductsDTO = [];
        foreach ($basket as $basketItem) {
            $basketProducts[$basketItem->getProductId()] = $basketItem;//ExternalSupplierBasketProductDTO::newFromBasketProduct($basketItem);
        }

        $basketProductsId = array_keys($basketProducts);
        if ($basketProductsId) {
            $productsInfoQ = \CIBlockElement::GetList(
                [],
                ['IBLOCK_ID' => Constants::PRODUCTS_IBLOCK_ID, 'ID' => $basketProductsId],
                false,
                false,
                ['ID', 'PROPERTY_SUPPLIER_ID']
            );

            while ($row = $productsInfoQ->fetch()) {
                $supplierCode = trim($row['PROPERTY_SUPPLIER_ID_VALUE']);
                if (!empty($supplierCode)) {
                    $basketItem = $basketProducts[$row['ID']];
                    $basketSuppliersProductsDTO[$row['PROPERTY_SUPPLIER_ID_VALUE']][] = ExternalSupplierBasketProductDTO::newFromBasketProduct($basketItem);
                }
            }
        }

        $existingOrders = CustomTools::indexArrayByKey(
            self::getRowsForOrderId($orderId),
            'SUPPLIER_CODE'
        );

        if ($basketSuppliersProductsDTO) {
            foreach ($basketSuppliersProductsDTO as $supplierCode => $supplierOrderProducts) {
                $id = self::addOrderIFNotExist($orderId, $supplierCode);
                if ($id) {
                    self::UpdateOrderProducts($id, $supplierOrderProducts);
                }
                if ($id && isset($existingOrders[$supplierCode])) {
                    unset($existingOrders[$supplierCode]);
                }
            }
        }

        if ($existingOrders) {
            foreach ($existingOrders as $existingOrder) {
                try {
                    self::UpdateOrderProductsZeroQTY($existingOrder['ID']);
                } catch (\Exception $exception) {
                }
            }
        }
    }

    public static function UpdateOrderProductsZeroQTY($idExternalOrder)
    {
        return self::UpdateOrderProducts($idExternalOrder, []);
    }

    public static function addOrderIFNotExist($orderId, $supplierCode)
    {
        $existOrder = self::getSupplierOrder($orderId, $supplierCode);
        $resOrderId = false;
        if (!$existOrder) {

            $fields = [
                'ID_ORDER' => $orderId,
                'SUPPLIER_CODE' => $supplierCode,
                'STATUS_ID' => ExternalSuppliersOrderTable::STATUS_NEED_PROCESS,
            ];
            try {
                $res = ExternalSuppliersOrderTable::add($fields);
                $resOrderId = $res->getId();
            } catch (\Exception $exception) {
            }

        } else {
            $resOrderId = $existOrder['ID'];
        }
        return $resOrderId;
    }

    /**
     * @param $idExternalOrder
     * @param ExternalSupplierBasketProductDTO[] $products
     */
    public static function UpdateOrderProducts($idExternalOrder, array $updateProductsDTO)
    {
        $productsToUpdate = [];
        $currentExternalOrder = self::getExternalOrder($idExternalOrder);
        if (!$currentExternalOrder) {
            return false;
        }
        $currentProducts = [];
        if (is_array($currentExternalOrder['PRODUCTS_INFO'])) {
            /* @var $product ExternalSupplierBasketProductDTO */
            foreach ($currentExternalOrder['PRODUCTS_INFO'] as $product) {
                $currentProducts[$product->getIdProduct()] = $product;
            }
        }

        if ($updateProductsDTO) {
            foreach ($updateProductsDTO as $product) {
                $productsToUpdate[] = $product;
                if (isset($currentProducts[$product->getIdProduct()])) {
                    unset($currentProducts[$product->getIdProduct()]);
                }
            }
        }

        if ($currentProducts) {
            foreach ($currentProducts as $zeroQtyProduct) {
                $zeroQtyProduct->setQuantity(0);
                $productsToUpdate[] = $zeroQtyProduct;
            }
        }

        $fields = [
            'PRODUCTS_INFO' => $productsToUpdate
        ];
        try {
            $res = ExternalSuppliersOrderTable::update($idExternalOrder, $fields);
        } catch (\Exception $exception) {
            $res = false;
        }
        return $res;
    }


    public static function getExternalOrder($idExternalOrder)
    {
        try {
            $q = ExternalSuppliersOrderTable::getByPrimary($idExternalOrder);
            return $q->fetch();
        } catch (\Exception $exception) {
            return null;
        }
    }

    public static function getSupplierOrder($orderId, $supplierCode)
    {
        $qParam = [
            'filter' => [
                'ID_ORDER' => $orderId,
                'SUPPLIER_CODE' => $supplierCode,
            ]
        ];
        return self::getSupplierOrderByParameters($qParam) ? self::getSupplierOrderByParameters($qParam)[0] : null;
    }

    public static function getRowsForOrderId($orderId): array
    {
        $qParam = [
            'filter' => [
                'ID_ORDER' => $orderId
            ]
        ];
        return self::getSupplierOrderByParameters($qParam);
    }

    public static function deleteSupplierOrder($id)
    {
        return ExternalSuppliersOrderTable::delete($id);
    }

    public static function getSupplierOrderByParameters($parameters): array
    {
        $parameters['count_total'] = true;
        $q = ExternalSuppliersOrderTable::getList($parameters);
        return $q->fetchAll();
    }

    public static function getSupplierOrderByParametersWithCount($parameters)
    {
        $parameters['count_total'] = true;
        $q = ExternalSuppliersOrderTable::getList($parameters);
        return ['result' => $q->fetchAll(), 'total' => $q->getCount()];
    }

    public static function getOrdersForSupplier($supplierCode, $filter = []): array
    {
        if (!$supplierCode) {
            return [];
        }
        $filter['SUPPLIER_CODE'] = $supplierCode;
        $parameters = [
            'filter' => $filter
        ];
        return self::getSupplierOrderByParameters($parameters);
    }

    public static function updateStatusId($orderId, $supplierCode, $statusId): bool
    {
        $res = false;
        $supOrder = self::getSupplierOrder($orderId, $supplierCode);
        if ($supOrder && ExternalSuppliersOrderTable::isValidStatusId($statusId)) {
            $supOrderId = $supOrder['ID'];
            $updateFields = [
                'fields' => [
                    'STATUS_ID' => $statusId
                ]
            ];
            try {
                ExternalSuppliersOrderTable::update($supOrderId, $updateFields);
                $res = true;
            } catch (\Exception $exception) {
            }
        }
        return $res;
    }

}