<?

namespace Handlers\ExternalSuppliers;

class ExternalSupplierBasketProductDTO
{
    protected $id;
    protected $quantity;
    protected $price;

    protected final function __construct(array $params)
    {
        foreach ($params as $key => $value) {
            $this->setField($key, $value);
        }
    }


    public static function newFromBasketProduct(\Bitrix\Sale\BasketItem $basketItem): self
    {
        return new self([
            'id' => $basketItem->getProductId(),
            'quantity' => $basketItem->getQuantity(),
            'price' => $basketItem->getPrice(),
        ]);
    }

    public static function newFromDBRow(array $params): self
    {
        return new self($params);
    }

    public function getField($field)
    {
        return property_exists($this, $field) ? $this->{$field} : null;
    }

    public function setField($field, $value)
    {
        $this->{$field} = $value;
    }

    public function setQuantity($value)
    {
        $val = (float)$value;
        $this->quantity = number_format($val, 3, '.', '');
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getIdProduct()
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function toExternal(array $fields = []): array
    {
        $res = [
            'id' => (string)$this->getIdProduct(),
            'quantity' => number_format($this->getField('quantity'), 3, '.', ''),
            'price' => number_format($this->getField('price'), 2, '.', '')
        ];

        if ($fields) {
            foreach ($res as $key => $v) {
                if (!in_array($key,$fields)) {
                    unset($res[$key]);
                }
            }
        }

        return $res;
    }


}