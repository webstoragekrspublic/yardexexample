<?php
ini_set('serialize_precision',10);
//модуль отправки писем по SMTP
include_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");
include_once __DIR__ . '/include/functions.php';
include_once __DIR__ . '/include/autoloadClasses.php';
include_once __DIR__ . '/include/addEvents.php';

\Bitrix\Main\Loader::includeModule('custom.deliverytime');
\Bitrix\Main\Loader::includeModule('custom.productcomments');