<?

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

Loader::registerAutoLoadClasses(null, [
    'Helpers\GlobalStorage' => '/local/php_interface/helpers/GlobalStorage.php',
    'Helpers\CustomTools' => '/local/php_interface/helpers/CustomTools.php',
    'Helpers\CurrentCart' => '/local/php_interface/helpers/CurrentCart.php',
    'Helpers\Cache' => '/local/php_interface/helpers/Cache.php',
    'Helpers\CustomAdminPage' => '/local/php_interface/helpers/ReportsAdmin/CustomAdminPage.php',
    'Helpers\ParseConditionHelper' => '/local/php_interface/helpers/ParseConditionHelper.php',
    'Helpers\Events' => '/local/php_interface/helpers/Events.php',
    'Helpers\EventsUser' => '/local/php_interface/helpers/EventsUser.php',
    'Helpers\SafetyValidation' => '/local/php_interface/helpers/SafetyValidation.php',
    'Helpers\TorgovayaMarka' => '/local/php_interface/helpers/TorgovayaMarka.php',
    'Helpers\ReportsAdmin\ReportsAdmin' => '/local/php_interface/helpers/ReportsAdmin/ReportsAdmin.php',
    'Helpers\ReportsAdmin\ReportsProducts' => '/local/php_interface/helpers/ReportsAdmin/ReportsProducts.php',
    'Helpers\CsvHelper' => '/local/php_interface/helpers/CsvHelper.php',

    'Handlers\CustomStringProperty' => '/local/php_interface/handlers/CustomStringProperty.php',
    'Handlers\CustomHlListProperty' => '/local/php_interface/handlers/CustomHlListProperty.php',
    'Handlers\SearchEvents' => '/local/php_interface/handlers/SearchEvents.php',
    'Handlers\ProductDiscount\ProductDiscountParser' => '/local/php_interface/handlers/ProductDiscount/ProductDiscountParser.php',
    'Handlers\ProductDiscount\ProductDiscountController' => '/local/php_interface/handlers/ProductDiscount/ProductDiscountController.php',
    'Handlers\SpecPrice\SpecPriceBasketRules' => '/local/php_interface/handlers/SpecPrice/SpecPriceBasketRules.php',
    'Handlers\SpecPrice\SpecPriceTable' => '/local/php_interface/handlers/SpecPrice/SpecPriceTable.php',
    'Handlers\SpecPrice\SpecPriceTableCache' => '/local/php_interface/handlers/SpecPrice/SpecPriceTableCache.php',
    'Handlers\SpecPrice\SpecPriceController' => '/local/php_interface/handlers/SpecPrice/SpecPriceController.php',
    'Handlers\CouponGenerator\CouponGenerator' => '/local/php_interface/handlers/CouponGenerator/CouponGenerator.php',
    'Handlers\CouponGenerator\CouponSender' => '/local/php_interface/handlers/CouponGenerator/CouponSender.php',
    'Handlers\CouponGenerator\CouponEvents' => '/local/php_interface/handlers/CouponGenerator/CouponEvents.php',
    'Handlers\TotalPrice\TotalPrice' => '/local/php_interface/handlers/TotalPrice/TotalPrice.php',
    'Handlers\ActualSalesTrack\SaleTracker' => '/local/php_interface/handlers/ActualSalesTrack/SaleTracker.php',
    'Handlers\ActualSalesTrack\SaleSwitcher' => '/local/php_interface/handlers/ActualSalesTrack/SaleSwitcher.php',
    'Handlers\YandexMetrika\YandexMetrikaOrderSender' => '/local/php_interface/handlers/YandexMetrika/YandexMetrikaOrderSender.php',
    'Handlers\ExternalSuppliers\ExternalSuppliers' => '/local/php_interface/handlers/ExternalSuppliers/ExternalSuppliers.php',
    'Handlers\ExternalSuppliers\ExternalSuppliersOrderTable' => '/local/php_interface/handlers/ExternalSuppliers/ExternalSuppliersOrderTable.php',
    'Handlers\ExternalSuppliers\ExternalSupplierBasketProductDTO' => '/local/php_interface/handlers/ExternalSuppliers/ExternalSupplierBasketProductDTO.php',
    'Handlers\DashaMail\DashaMailClientSender' => '/local/php_interface/handlers/DashaMail/DashaMailClientSender.php',

    'ExternalApi\MoySklad\MoySkladApi' => '/local/php_interface/externalApi/moysklad/MoySkladApi.php',
    'ExternalApi\MoySklad\MoySkladOrderTable' => '/local/php_interface/externalApi/moysklad/MoySkladOrderTable.php',
    'ExternalApi\MoySklad\MoySkladAgentTable' => '/local/php_interface/externalApi/moysklad/MoySkladAgentTable.php',
    'ExternalApi\MoySklad\MoySkladProductTable' => '/local/php_interface/externalApi/moysklad/MoySkladProductTable.php',
    'ExternalApi\MoySklad\MoySklad' => '/local/php_interface/externalApi/moysklad/MoySklad.php',
    'ExternalApi\MoySklad\MoySkladEvents' => '/local/php_interface/externalApi/moysklad/MoySkladEvents.php',

    'ExternalApi\MCRM\McrmApi' => '/local/php_interface/externalApi/mcrm/mcrmApi.php',
    'ExternalApi\MCRM\McrmUser' => '/local/php_interface/externalApi/mcrm/mcrmUser.php',

    'ExternalApi\MultiSearch\MultiSearchApi' => '/local/php_interface/externalApi/multisearch/multisearchApi.php',

    'ExternalApi\Dadata\Dadata' => '/local/php_interface/externalApi/dadata/Dadata.php',
    'ExternalApi\Dadata\DadataApi' => '/local/php_interface/externalApi/dadata/DadataApi.php',

    'ExternalApi\SberbankPayment\SberbankApi' => '/local/php_interface/externalApi/sberbankPayment/sberbankApi.php',
    'ExternalApi\SberbankPayment\Sberbank' => '/local/php_interface/externalApi/sberbankPayment/sberbank.php',

    'ExternalApi\b24CRM\B24CRM' => '/local/php_interface/externalApi/b24crm/B24CRM.php',
    'ExternalApi\b24CRM\B24CRMApi' => '/local/php_interface/externalApi/b24crm/B24CRMApi.php',
    'ExternalApi\b24CRM\B24CRMContactTable' => '/local/php_interface/externalApi/b24crm/B24CRMContactTable.php',
    'ExternalApi\b24CRM\B24CRMOrderTable' => '/local/php_interface/externalApi/b24crm/B24CRMOrderTable.php',

    'ExternalApi\Exchange1c\Exchange1c' => '/local/php_interface/externalApi/exchange1c/Exchange1c.php',
    'ExternalApi\Exchange1c\Exchange1cOrderTable' => '/local/php_interface/externalApi/exchange1c/Exchange1cOrderTable.php',

    'ExternalApi\YandexMaps\YandexMapsApi' => '/local/php_interface/externalApi/yandexMaps/yandexMapsApi.php',

    'ExternalApi\YandexRouting\YandexRoutingApi' => '/local/php_interface/externalApi/yandexRouting/YandexRoutingApi.php',
    'ExternalApi\YandexRouting\YandexRouting' => '/local/php_interface/externalApi/yandexRouting/YandexRouting.php',
    'ExternalApi\YandexRouting\YandexRoutingDeliveryApi' => '/local/php_interface/externalApi/yandexRouting/YandexRoutingDeliveryApi.php',


    'Helpers\Constants' => '/local/php_interface/helpers/Constants.php',

]);
