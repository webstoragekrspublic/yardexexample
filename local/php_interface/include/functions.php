<?
if (!function_exists('dump')) {
    function dump($a)
    {
        echo "<pre>";
        var_dump($a);
        echo "</pre>";
    }
}


function pr($a)
{
    if($_GET["dev"] == true){
        echo "<pre>";
        print_r($a);
        echo "</pre>";
    }
}


function dumpHidden($a)
{
    echo "<pre class='dumpHidden' style='display: none;'>";
    var_dump($a);
    echo "</pre>";
}

if (!function_exists('array_key_first')) {
    function array_key_first(array $arr)
    {
        foreach ($arr as $key => $unused) {
            return $key;
        }
        return NULL;
    }
}

function getLog($content)
{
    $file = $_SERVER['CONTEXT_DOCUMENT_ROOT'] . '/dtest/logs.json';
    $fd = fopen($file, 'w') or die("не удалось создать файл");
    fputs($fd, json_encode($content));
    fclose($fd);
}

function writeAppendLog($content)
{
    $file = $_SERVER['CONTEXT_DOCUMENT_ROOT'] . '/dtest/logs.json';
    file_put_contents($file, json_encode($content), FILE_APPEND);
}

function getInclinationByNumber($n, $titles)
{
    $cases = array(2, 0, 1, 1, 1, 2);
    return $titles[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
}

function cmp($a, $b){
    if($a["time"] == "любое время" || $b["time"] == "любое время"){
        return 0;
    }
    $a = intval($a["time"][0].$a["time"][1]);
    $b = intval($b["time"][0].$b["time"][1]);
    
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

function sortTimeSlot($timeSlots){
    foreach($timeSlots as $key => $slot){
        foreach($slot as $keyItemSlot => $itemSlot){
            if($keyItemSlot == "timeSlots"){
                usort($itemSlot, 'cmp');
                $timeSlots[$key][$keyItemSlot] = $itemSlot;
            }
        }
    }
    return $timeSlots;
}