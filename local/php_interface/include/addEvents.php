<?php

use Bitrix\Main\EventManager;

$eventManager = EventManager::getInstance();


$eventManager->addEventHandler(
    'sale',
    'OnSaleOrderSaved',
    [
        'Helpers\Events',
        'onSaleOrderSaved'
    ]
);

$eventManager->addEventHandler(
    "iblock",
    "OnIBlockPropertyBuildList",
    [
        'Handlers\CustomStringProperty',
        'GetUserTypeDescription'
    ]
);

$eventManager->addEventHandler(
    'iblock',
    'OnIBlockPropertyBuildList',
    [
        'Handlers\CustomHlListProperty',
        'GetUserTypeDescription'
    ]
);

$eventManager->addEventHandler(
    'search',
    'BeforeIndex',
    [
        'Handlers\SearchEvents',
        'BeforeIndex'
    ]
);

$eventManager->addEventHandler(
    'sale',
    'OnOrderAdd',
    [
        'ExternalApi\MoySklad\MoySkladEvents',
        'onOrderChange'
    ]
);

$eventManager->addEventHandler(
    'sale',
    'OnOrderUpdate',
    [
        'ExternalApi\MoySklad\MoySkladEvents',
        'onOrderChange'
    ]
);

$eventManager->addEventHandler(
    'iblock',
    'OnBeforeIBlockElementUpdate',
    [
        'Helpers\Events',
        'checkImagesOnUpdate'
    ]
);

$eventManager->addEventHandler(
    'iblock',
    'OnBeforeIBlockElementUpdate',
    [
        'Helpers\Events',
        'onBeforeIBlockElementUpdate'
    ]
);

$eventManager->addEventHandler(
    'iblock',
    'OnAfterIBlockElementUpdate',
    [
        'Helpers\Events',
        'updateSpecPricesAfterChangeIBlockElement'
    ]
);

$eventManager->addEventHandler(
    'iblock',
    'OnAfterIBlockElementAdd',
    [
        'Helpers\Events',
        'updateSpecPricesAfterChangeIBlockElement'
    ]
);

$eventManager->addEventHandler(
    'iblock',
    'OnAfterIBlockElementDelete',
    [
        'Helpers\Events',
        'onAfterIBlockElementDelete'
    ]
);

$eventManager->addEventHandler(
    'iblock',
    'OnAfterIBlockElementSetPropertyValuesEx',
    [
        'Helpers\Events',
        'onAfterIBlockElementSetPropertyValuesEx'
    ]
);

$eventManager->addEventHandler(
    'sale',
    'OnOrderSave',
    [
        'Helpers\Events',
        'onOrderSave'
    ]
);

$eventManager->addEventHandler(
    'sale',
    'OnSaleOrderBeforeSaved',
    [
        'Helpers\Events',
        'onSaleOrderBeforeSaved'
    ]
);

$eventManager->addEventHandler(
    'main',
    'OnBeforeUserUpdate',
    [
        'Handlers\CouponGenerator\CouponEvents',
        'onBeforeUserUpdate'
    ]
);

$eventManager->addEventHandler(
    'sale',
    'OnSaleStatusOrder',
    [
        'Handlers\CouponGenerator\CouponEvents',
        'onSaleStatusOrder'
    ]
);

$eventManager->addEventHandler(
    'sale',
    'OnSaleStatusOrder',
    [
        'Helpers\Events',
        'onSaleStatusOrder'
    ]
);

$eventManager->addEventHandler(
    'catalog',
    'Bitrix\Catalog\Model\Product::OnUpdate',
    [
        'Helpers\Events',
        'OnProductUpdate'
    ]
);

$eventManager->addEventHandler(
    'main',
    'OnAfterUserUpdate',
    [
        'Helpers\EventsUser',
        'OnAfterUserUpdate'
    ]
);

$eventManager->addEventHandler(
    'main',
    'OnAfterUserAdd',
    [
        'Helpers\EventsUser',
        'OnAfterUserAdd'
    ]
);
$eventManager->addEventHandler(
    'main',
    'OnBuildGlobalMenu',
    [
        'Helpers\CustomAdminPage',
        'notificationsAdminMenu'
    ]
);

$eventManager->addEventHandler(
    'sale',
    'OnSaleComponentOrderOneStepFinal',
    [
        'Handlers\YandexMetrika\YandexMetrikaOrderSender',
        'AddYandexMetricsEcommerceCode'
    ]
);

$eventManager->addEventHandler(
    'main',
    'OnBeforeProlog',
    [
        'Handlers\DashaMail\DashaMailClientSender',
        'AddHEADClientSender'
    ]
);

$eventManager->addEventHandler(
    'main',
    'OnAfterUserAdd',
    [
        'Helpers\EventsUser',
        'OnAfterUserAdd'
    ]
);

$eventManager->addEventHandler(
    'main',
    'OnEpilog',
    [
        'Helpers\Events',
        '_Check404Error'
    ]
);