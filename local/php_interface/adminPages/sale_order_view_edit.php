<?php

//детальная страница заказа или редактирования заказа
if (
    $GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/sale_order_view.php" ||
    $GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/sale_order_edit.php"
) {
    ?>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            addPaymentInfoButton();
            disableAddAndChangeNewProduct();

            var elemetsOtherInfo = document.querySelectorAll('div.adm-bus-component-title-container.draggable');

            if (elemetsOtherInfo.length) {
                for (var i = 0; i < elemetsOtherInfo.length; i++) {
                    if (elemetsOtherInfo[i].innerText == ' Дополнительная информация') {
                        var test = document.createElement("div");
                        test.classList.add('adm-bus-component-content-container');
                        elemetsOtherInfo[i].nextElementSibling.after(test);
                        $.ajax({
                            url: "/ajax/adminOrderChangeBasket.php",
                            context: document.body,
                            data: {
                                'order_id': <?= $ID ?>
                            },
                            success: function (data) {
                                test.innerHTML = data;
                                updateFastDeliveryPopup();
                            },
                            error: function () {
                                alert('Ошибка загрузки формы')
                            }
                        });
                    }
                }
            }

            function addPaymentInfoButton() {
                var payments = document.getElementsByClassName('adm-bus-section-container-section-status-service');
                var showButton = false;
                if (payments) {
                    for (var k = 0; k < payments.length; k++) {
                        var paymentName = payments[k].textContent;
                        if (
                            paymentName.match(/Картой онлайн/i) ||
                            paymentName.match(/Apple/i) ||
                            paymentName.match(/Google/i)
                        ) {
                            showButton = true;
                            break;
                        }
                    }
                }

                if (showButton) {
                    var innerHtmlTarget = document.querySelector('[data-id="payment"] .adm-bus-component-content-container');
                    if (innerHtmlTarget) {
                        var button = `
                            <div id="sberInfo">
                                <input type="button" id="sberInfoGetStatus" class="adm-order-block-add-button" value="Узнать статус оплаты"/>
                                <div id="sberInfoContent"></div>
                            </div>
                            `;
                        innerHtmlTarget.insertAdjacentHTML('beforeend', button);

                        var sberInfoGetStatusBtn = document.querySelector('#sberInfoGetStatus');
                        var sberInfoContent = document.querySelector('#sberInfoContent');
                        sberInfoGetStatusBtn.onclick = function (e) {
                            e.preventDefault();
                            $.ajax({
                                url: "/ajax/admin/sberbankPayment.php",
                                data: {
                                    'orderId': <?= $ID ?>,
                                    'action': 'status'
                                },
                                type: 'POST',
                                dataType: 'json',
                                beforeSend: function (xhr) {
                                    var ajaxLoadingGif = '<img src="/images/ajaxLoading.gif"/>';
                                    sberInfoContent.innerHTML = ajaxLoadingGif;
                                    sberInfoGetStatusBtn.setAttribute('disabled', 'disabled');
                                },
                                success: function (data) {
                                    sberInfoGetStatusBtn.removeAttribute('disabled');
                                    if (data.error || !data.paymentInfo) {
                                        var errorText = 'Ошибка! ' + data.errorText;
                                        sberInfoContent.innerHTML = errorText;
                                    } else {
                                        addPaymentInfo(data.paymentInfo);
                                    }
                                },
                                error: function () {
                                    sberInfoGetStatusBtn.removeAttribute('disabled');
                                    sberInfoContent.innerHTML = 'Произошла ошибка в зарпосе!<br>Попробуйте снова или обратитесь к администратору!';
                                }
                            });
                        }
                    }
                }

            }

            function addPaymentInfo(paymentInfo) {
                var sberInfoContent = document.querySelector('#sberInfoContent');
                if (sberInfoContent && paymentInfo) {
                    var innerHtml = `
                        <div style="max-width: 900px;background: #ecf1d4;padding: 10px;border-radius: 3px;">
                            <table class="adm-s-result-container-itog-table">
                                <tbody>
                                    <tr>
                                        <td>номер Оплаты</td>
                                        <td>${paymentInfo.orderNumber} (${paymentInfo.orderPaymentId})</td>
                                    </tr>
                                    <tr>
                                        <td>Код ответа процессинга</td>
                                        <td>(${paymentInfo.actionCode}) ${paymentInfo.actionCodeDescription}</td>
                                    </tr>
                                    <tr>
                                        <td>состояние заказа в платёжной системе</td>
                                        <td>(${paymentInfo.orderStatusCode}) ${paymentInfo.orderStatusText}</td>
                                    </tr>
                                    <tr>
                                        <td>ссылка на оплату для данного номера (для проверки работоспособности)</td>
                                        <td><a href="${paymentInfo.orderPaymentUrl}" target="_blank">${paymentInfo.orderPaymentUrl}</a></td>
                                    </tr>
                                    <tr>
                                        <td>сумма Оплаты</td>
                                        <td>${paymentInfo.amount} ${paymentInfo.currency}</td>
                                    </tr>
                                </tbody>
                            </table>
                    `;

                    if (paymentInfo.canConfirm) {
                        innerHtml += `
                            <input type="button" id="sberInfoConfirmBtn" class="adm-order-block-add-button" value="Синхронизировать и завершить"/>
                            <div id="sberInfoConfirmContent"></div>
                        `;

                    }
                    innerHtml += '</div>';

                    sberInfoContent.innerHTML = innerHtml;

                    var sberInfoConfirmBtn = document.querySelector('#sberInfoConfirmBtn');
                    var sberInfoConfirmContent = document.querySelector('#sberInfoConfirmContent');

                    if (sberInfoConfirmBtn && sberInfoConfirmContent) {
                        sberInfoConfirmBtn.onclick = function (e) {
                            e.preventDefault();
                            if (confirm('Вы уверены что хотите синхронизировать и завершить оплату №' + paymentInfo.orderNumber + '? (действие не обратимо!)')) {
                                $.ajax({
                                    url: "/ajax/admin/sberbankPayment.php",
                                    data: {
                                        'orderId': <?= $ID ?>,
                                        'orderPaymentId': paymentInfo.orderPaymentId,
                                        'action': 'confirmOrder'
                                    },
                                    type: 'POST',
                                    dataType: 'json',
                                    beforeSend: function (xhr) {
                                        var ajaxLoadingGif = '<img src="/images/ajaxLoading.gif"/>';
                                        sberInfoConfirmContent.innerHTML = ajaxLoadingGif;
                                        sberInfoConfirmBtn.setAttribute('disabled', 'disabled');
                                    },
                                    success: function (data) {
                                        var removeDisabled = true;
                                        if (data.error || !data.confirmOrderInfo) {
                                            var errorText = 'Ошибка! ' + data.errorText;
                                            sberInfoConfirmContent.innerHTML = errorText;
                                        } else {
                                            var text = 'Ответ от АПИ:' + data.confirmOrderInfo.errorMessage + '. Код ответа: ' + data.confirmOrderInfo.errorCode;
                                            if (data.confirmOrderInfo.errorCode === "0") {
                                                var removeDisabled = false;
                                            }
                                            sberInfoConfirmContent.innerHTML = text;
                                        }
                                        if (removeDisabled) {
                                            sberInfoConfirmBtn.removeAttribute('disabled');
                                        }
                                    },
                                    error: function () {
                                        sberInfoConfirmBtn.removeAttribute('disabled');
                                        sberInfoConfirmContent.innerHTML = 'Произошла ошибка в зарпосе!<br>Попробуйте снова или обратитесь к администратору!';
                                    }
                                });
                            }
                        }
                    }
                }
            }

            function disableAddAndChangeNewProduct() {
                if (document.querySelectorAll('.adm-s-gray-title-btn-container')) {
                    document.querySelectorAll('.adm-s-gray-title-btn-container').forEach(function (elem) {
                        elem.style.pointerEvents = 'none';
                    });
                }
                if (document.querySelector('#sale_order_basketsale_order_edit_product_table')) {
                    document.querySelector('#sale_order_basketsale_order_edit_product_table').addEventListener('mouseover', function () {
                        this.querySelectorAll('input.tac').forEach(function (elem) {
                            elem.setAttribute('readonly', 'readonly');
                        });
                    });
                }
            }


            //обновляем информацию по доставке в всплывашке временных слотов
            function updateFastDeliveryPopup() {
                var cartProducts = []
                $('[data-role="pr_cart_info_id"]').each(function () {
                    var productID = parseInt($(this).val());
                    if (!isNaN(productID)) {
                        cartProducts.push(productID);
                    }
                });
                if (cartProducts.length > 0) {
                    $.ajax({
                        url: "/api/v1/site/timeslots/products",
                        method: "GET",
                        dataType: 'json',
                        data: {
                            'product_ids': cartProducts.join(',')
                        },
                        success: function (data) {
                            if (data.result) {
                                addSlotsPopupInfo(data.result);
                            } else {
                                alert(data.error.message)
                            }
                        },
                        error: function () {
                            alert('Ошибка обращения к API')
                        }
                    });
                }
            }

            function addSlotsPopupInfo(apiData) {
                var $alertBlock = $('[data-role="popup_timeslots_slots_alert"]');
                if ($alertBlock.length) {
                    for (var k = 0; k < apiData.closestProductsDay.length; k++) {
                        var item = apiData.closestProductsDay[k];
                        if (item.FAST_DELIVERY != "1"){
                            $alertBlock.html("В корзине присутсвуют товары с долгой доставкой!")
                            return
                        }
                    }
                    $alertBlock.html("")
                }
            }
        });


    </script>
    <?
}

?>