<?php

//страница списка заказов.
if ($GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/sale_order.php") {
    ?>

    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', function () {
            highLightPayedColumn();
            highLightNotCallColumn();
            highLightCouponProblemRow();
            highLightDozakazRow();
            updatePhoneNumberToTel();
            appendButtonsForOrderInfo();
            addSlotUsedToOrders();
            highLightUnpayedString();
            highLightExpressColumn();
        });

        function updatePhoneNumberToTel() {
            var elementsTableAdmin = document.querySelectorAll('.adm-list-table-cell');
            for (i = 0; i < elementsTableAdmin.length; i++) {
                var isnumber = replaceNumberPhoneToLink(elementsTableAdmin[i]);
            }
        }

        function replaceNumberPhoneToLink(element) {
            var matches;
            if (matches = element.innerHTML.match(/^(8|\+\d{1,3}[\- ]?){1}(\(?[\s]*\d{3}[\s]*\)?[\- ]?){1}[\d\- ]{7,10}$/)) {
                var telText = matches[0];
                var tel = telText.replace(/[^\d\+]/g, '');
                var tel = tel.replace(/^\+7/, '8');
                element.innerHTML = '<a href="tel:' + tel + '">' + telText + '</a>';
                return true;
            }
            return false;
        }

        function appendButtonsForOrderInfo() {
            var elements = document.querySelectorAll('#tbl_sale_order b a[href*="ID="]');
            for (i = 0; i < elements.length; i++) {
                var elementsOrderPopUp = elements[i];
                var orderId = elementsOrderPopUp.innerHTML;
                orderId = orderId.match(/\d+/g);

                var parent = elementsOrderPopUp.parentNode;

                var button = document.createElement('div');
                button.innerHTML = '<br><a class="showOrderInfo" data-id="' + orderId + '" href="javascript:;">Подробнее</a>';
                parent.append(button);
            }

            $(".showOrderInfo").click(function () {
                var ordId = $(this).attr('data-id');
                $.ajax({
                    url: "/ajax/adminOrderChangeBasket.php",
                    context: document.body,
                    data: {
                        'order_id': ordId
                    },
                    success: function (data) {
                        $.fancybox(data, {
                            margin: 0,
                            padding: 20,
                            autoScale: true,
                            transitionIn: 'none',
                            transitionOut: 'none',
                            helpers: {
                                overlay: {
                                    locked: false
                                }
                            }
                        });
                    },
                    error: function () {
                        alert('Ошибка загрузки заказа')
                    }
                });
            });
        }

        function highLightPayedColumn() {
            var elementsHighlight = document.querySelectorAll('[id^="payed_"]');

            if (elementsHighlight.length) {
                for (var k = 0; k < elementsHighlight.length; k++) {
                    if (elementsHighlight[k].innerHTML == 'Да') {
                        elementsHighlight[k].parentElement.style = 'background-color:darkorange;';
                    }
                }
            }
        }

        function highLightExpressColumn() {
            var highLightColumnName = 'Время доставки';
            let regForExpress = /(экспресс)+/gi;
            var highLightStyle = 'background-color:darkorange;';
            var ordetTableHeader = document.querySelector('.adm-list-table-header');
            if (ordetTableHeader) {
                var highLightColumnPosition = false;
                for (var k = 0; k < ordetTableHeader.children.length; k++) {
                    var columnName = ordetTableHeader.children[k].textContent.trim();
                    if (columnName == highLightColumnName) {
                        highLightColumnPosition = k;
                        break;
                    }
                }

                if (highLightColumnPosition !== false) {
                    var table = document.querySelector('#tbl_sale_order');
                    if (table) {
                        for (var k = 0; k < table.rows.length; k++) {
                            var highLightColumn = table.rows[k].cells[highLightColumnPosition];
                            var text = highLightColumn.textContent.trim();
                            let express = text.match(regForExpress);
                            if (express) {
                                highLightColumn.style = highLightStyle;
                            }
                        }
                    }
                }
            }
            
            
            var elementsHighlight = document.querySelectorAll('[id^="payed_"]');

            if (elementsHighlight.length) {
                for (var k = 0; k < elementsHighlight.length; k++) {
                    if (elementsHighlight[k].innerHTML == 'Да') {
                        elementsHighlight[k].parentElement.style = 'background-color:darkorange;';
                    }
                }
            }
        }

        function highLightCouponProblemRow() {
            var couponColumnName = 'Купоны заказа';
            var customCouponColumnName = 'Пользователь пытался применить эти промокоды';
            var highLightStyle = 'background-color:rgba(0,255,232,0.3);';
            var ordetTableHeader = document.querySelector('.adm-list-table-header');
            if (ordetTableHeader) {
                var couponColumnPosition = false;
                var customCouponColumnPosition = false;
                for (var k = 0; k < ordetTableHeader.children.length; k++) {
                    var columnName = ordetTableHeader.children[k].textContent.trim();
                    if (columnName == couponColumnName) {
                        couponColumnPosition = k;
                    } else if (columnName == customCouponColumnName) {
                        customCouponColumnPosition = k;
                    }
                }

                if (couponColumnPosition !== false && customCouponColumnPosition !== false) {
                    var table = document.querySelector('#tbl_sale_order');
                    if (table) {
                        for (var k = 0; k < table.rows.length; k++) {
                            var couponCeil = table.rows[k].cells[couponColumnPosition];
                            var customCouponCeil = table.rows[k].cells[customCouponColumnPosition];
                            if (customCouponCeil.innerText.length > 2 && couponCeil.innerText.length < 2) {
                                table.rows[k].children[2].style = highLightStyle;
                                table.rows[k].children[couponColumnPosition].style = highLightStyle;
                                table.rows[k].children[customCouponColumnPosition].style = highLightStyle;
                                table.rows[k].children[2].innerHTML += `<br>НУЖНО ПРОВЕРИТЬ ПРОМОКОДЫ ЗАКАЗА`;
                            }
                        }
                    }
                }

            }
        }

        function highLightDozakazRow() {
            var dozakazColumnName = 'Дозаказ';
            var highLightStyle = 'background-color:rgba(200,255,100,0.3);';
            var orderTableHeader = document.querySelector('.adm-list-table-header');
            if (orderTableHeader) {
                var dozakazColumnPosition = false;
                for (var k = 0; k < orderTableHeader.children.length; k++) {
                    var columnName = orderTableHeader.children[k].textContent.trim();
                    if (columnName == dozakazColumnName) {
                        dozakazColumnPosition = k;
                    }
                }

                if (dozakazColumnPosition !== false) {
                    var table = document.querySelector('#tbl_sale_order');
                    if (table) {
                        for (var k = 0; k < table.rows.length; k++) {
                            var dozakazCeil = table.rows[k].cells[dozakazColumnPosition];
                            console.log(dozakazCeil.innerText);
                            if (dozakazCeil.innerText.split(' ')[0] == 'Да') {
                                table.rows[k].children[2].style = highLightStyle;
                                table.rows[k].children[dozakazColumnPosition].style = highLightStyle;
                                table.rows[k].children[2].innerHTML += `<br>ЭТО ДОЗАКАЗ, К ЗАКАЗУ ${dozakazCeil.innerText.split(' ')[1]}`;
                            }
                        }
                    }
                }

            }
        }

        function highLightNotCallColumn() {
            var highLightColumnName = 'Перезванивать';
            var highLightText = 'Нет';
            var highLightStyle = 'background-color:rgba(255,0,0,0.6);';
            var ordetTableHeader = document.querySelector('.adm-list-table-header');
            if (ordetTableHeader) {
                var highLightColumnPosition = false;
                for (var k = 0; k < ordetTableHeader.children.length; k++) {
                    var columnName = ordetTableHeader.children[k].textContent.trim();
                    if (columnName == highLightColumnName) {
                        highLightColumnPosition = k;
                        break;
                    }
                }

                if (highLightColumnPosition !== false) {
                    var table = document.querySelector('#tbl_sale_order');
                    if (table) {
                        for (var k = 0; k < table.rows.length; k++) {
                            var highLightColumn = table.rows[k].cells[highLightColumnPosition];
                            if (highLightColumn.textContent.trim() === highLightText) {
                                highLightColumn.style = highLightStyle;
                            }
                        }
                    }
                }

            }
        }

        function addSlotUsedToOrders() {
            var orderDateName = 'Дата доставки';
            var orderTimeName = 'Время доставки';
            var ordetTableHeader = document.querySelector('.adm-list-table-header');
            if (ordetTableHeader) {
                var orderDatePosition = false;
                var orderTimePosition = false;
                for (var k = 0; k < ordetTableHeader.children.length; k++) {
                    var columnName = ordetTableHeader.children[k].textContent.trim();
                    if (columnName == orderDateName) {
                        orderDatePosition = k;
                    } else if (columnName == orderTimeName) {
                        orderTimePosition = k;
                    }
                }

                if (orderDatePosition !== false && orderTimePosition !== false) {
                    var table = document.querySelector('#tbl_sale_order');
                    if (table) {
                        $.ajax({
                            url: "/ajax/admin/usedTimeSlots.php",
                            type: 'POST',
                            dataType: 'json',
                            success: function (data) {
                                if (data && Object.keys(data).length !== 0) {
                                    var timeSlotsUsed = data;
                                    var table = document.querySelector('#tbl_sale_order');
                                    if (table) {
                                        for (var k = 0; k < table.rows.length; k++) {
                                            var orderDate = table.rows[k].cells[orderDatePosition].textContent.trim();
                                            var orderTime = table.rows[k].cells[orderTimePosition].textContent.trim();
                                            if (timeSlotsUsed[orderDate] && timeSlotsUsed[orderDate][orderTime]) {
                                                var addInfo = document.createElement('div');
                                                addInfo.innerHTML = '<br>заказов на этот слот: ' + timeSlotsUsed[orderDate][orderTime];
                                                table.rows[k].cells[orderTimePosition].append(addInfo);
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                }

            }
        }

        function highLightUnpayedString() {
            var highLightColumnName = 'Оплаты';
            let regForUnpayed = /(на сайте|Google Pay|Apple Pay)+[\s\S]*?(Не оплачен)+/g;
            var highLightStyle = 'background-color:rgb(239 166 166 / 36%)';
            var ordetTableHeader = document.querySelector('.adm-list-table-header');
            if (ordetTableHeader) {
                var highLightColumnPosition = false;
                for (var k = 0; k < ordetTableHeader.children.length; k++) {
                    var columnName = ordetTableHeader.children[k].textContent.trim();
                    if (columnName == highLightColumnName) {
                        highLightColumnPosition = k;
                        break;
                    }
                }

                if (highLightColumnPosition !== false) {
                    var table = document.querySelector('#tbl_sale_order');
                    if (table) {
                        for (var k = 0; k < table.rows.length; k++) {
                            var highLightColumn = table.rows[k].cells[highLightColumnPosition];
                            var pays = highLightColumn.textContent.trim();
                            let unPayed = pays.match(regForUnpayed);
                            if (unPayed) {
                                var parent = highLightColumn.parentNode;
                                parent.style = highLightStyle;
                                parent.querySelectorAll('td').forEach(elem => {
                                    if (elem.style.backgroundColor.length === 0) {
                                        elem.style = highLightStyle;
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }
    </script>

    <?
}
?>