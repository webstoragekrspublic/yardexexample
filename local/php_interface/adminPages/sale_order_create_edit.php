<?php

//Выбор даты и времени при создании/изменении заказа
if (
    $GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/sale_order_create.php" ||
    $GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/sale_order_edit.php"
) {
    CJSCore::Init(array('date'));
    $APPLICATION->IncludeComponent(
        "custom:empty_component",
        "admin_date_time_modals",
        [],
        false
    );
    ?>
    <script type = "text/javascript" src = "/local/templates/aspro_next_custom/js/jquery.maskedinput-1.2.2.js"></script>
    <script>
        // отслеживаем обновление корзины.
        $(document).ready(function () {
            if($("input[name='PROPERTIES[3]']").val().match(/\b\d{11}\b/gm)) {
                $("input[name='PROPERTIES[3]']").mask("89999999999");
            }
            else if($("input[name='PROPERTIES[3]']").val().match(/\b\d{7}\b/gm)){
                $("input[name='PROPERTIES[3]']").mask("2999999");
            }
            else {
                $("input[name='PROPERTIES[3]']").mask("+7(999) 999-9999");
            }

            var targetNodes = $("#sale_order_basketsale_order_edit_product_table");
            var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
            var myObserver = new MutationObserver(mutationHandler);
            var obsConfig = {
                attributes: true,
                childList: true,
                subtree: true,
                characterData: true,
                characterDataOldValue: true
            };

            targetNodes.each(function () {
                myObserver.observe(this, obsConfig);
            });

            function mutationHandler(mutationRecords) {
                mutationRecords.forEach(function (mutation) {
                    if (mutation.type === 'childList') {
                        if (typeof mutation.removedNodes == "object") {
                            var jq = $(mutation.removedNodes);
                            if (jq.is("div")) {
                                addOrderDeliveryInfo()
                            }
                        }
                    }
                });
            }

            function addOrderDeliveryInfo() {
                var cartProducts = [];
                $('#sale_order_basketsale_order_edit_product_table input[name$="[PRODUCT_ID]"]').each(function () {
                    var productID = parseInt($(this).val());
                    if (!isNaN(productID)) {
                        cartProducts.push(productID);
                    }
                })
                if (cartProducts.length > 0) {
                    $.ajax({
                        url: "/api/v1/site/timeslots/products",
                        method: "GET",
                        dataType: 'json',
                        data: {
                            'product_ids': cartProducts.join(',')
                        },
                        success: function (data) {
                            if (data.result) {
                                renderOrderDeliveryInfo(data.result);
                                addSlotsPopupInfo(data.result);
                            } else {
                                alert(data.error.message)
                            }
                        },
                        error: function () {
                            alert('Ошибка обращения к API')
                        }
                    });
                }

            }

            function renderOrderDeliveryInfo(apiData) {
                var nearestDeliveryInfo = `
                    <p id="nearest_delivery_info">
                        Доступная ближайшая доставка для магазина - ${apiData.closestDay.dayInfo.infoDate}
                        ${apiData.closestDay.dayInfo.infoDate2} ${apiData.closestDay.timeSlot}
                    </p>
                `;
                $('#nearest_delivery_info').remove();
                $('#sale_order_basketsale-order-basket-bottom td').first().append(nearestDeliveryInfo);

                $('#sale_order_basketsale_order_edit_product_table input[name$="[PRODUCT_ID]"]').each(function () {
                    var productID = parseInt($(this).val());

                    if (!isNaN(productID)) {
                        var prInfo = apiData.closestProductsDay.find(function (element, index, array) {
                            if (element.PRODUCT_ID === productID) {
                                return true;
                            }
                            return false;
                        });
                        if (prInfo) {
                            var diID = 'nearest_delivery_info_' + productID;
                            var font_weight = '400';
                            var deliveryInfo = `
                                <p id="${diID}" style="font-weight: ${font_weight};">
                                    быстрая доставка на ближайшее время
                                </p>
                            `;
                            if (prInfo.FAST_DELIVERY === '0') {
                                font_weight = '700';
                                deliveryInfo = `
                                <p id="${diID}" style="font-weight: ${font_weight};">
                                    доставка на ${prInfo.CLOSEST_DELIVERY_DAY.infoDate}
                                    ${prInfo.CLOSEST_DELIVERY_DAY.infoDate2} ${prInfo.CLOSEST_TIME_SLOT}
                                </p>
                            `;
                            }
                            $('#' + diID).remove()
                            $(this).parent().append(deliveryInfo)
                        }
                    }
                })

            }
            function addSlotsPopupInfo(apiData) {
                var $alertBlock = $('[data-role="popup_timeslots_slots_alert"]');
                if ($alertBlock.length) {
                    for (var k = 0; k < apiData.closestProductsDay.length; k++) {
                        var item = apiData.closestProductsDay[k];
                        if (item.FAST_DELIVERY != "1"){
                            $alertBlock.html("В корзине присутсвуют товары с долгой доставкой!")
                            return
                        }
                    }
                    $alertBlock.html("")
                }
            }

            addOrderDeliveryInfo();
        });

    </script>

    <?php

}

?>