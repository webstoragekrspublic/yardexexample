<?
//сравнение почт в csv файлах где 1 файл - все наши почты, 2 файл - только валидные почты отфилтрованные сервисом https://www.emaillistverify.com
$checkedEmailsDir = __DIR__ . '/checkedEmails';
$currentEmailsFile = __DIR__ . '/currentEmails/emails.csv';
$fpCurrentEmails = fopen($currentEmailsFile, 'r');
if ($fpCurrentEmails) {
    $currentEmails = [];
    while ($row = fgetcsv($fpCurrentEmails)) {
        $email = strtolower(trim($row[0]));
        if ($email) {
            $currentEmails[$email] = [
                'email' => $email,
                'checked' => 0
            ];
        }
    }

    if (!$currentEmails) {
        die('в файле ' . $currentEmailsFile . ' почта должна быть в первой колонке');
    }

    if ($handleDir = opendir($checkedEmailsDir)) {

        while (false !== ($filename = readdir($handleDir))) {
            if (substr($filename, -4) == '.csv') {
                echo "чтение файла - $filename \r\n<br>";
                $fpCheckedFile = fopen($checkedEmailsDir . '/' . $filename, 'r');
                if ($fpCheckedFile) {
                    while ($row = fgetcsv($fpCheckedFile)) {
                        $email = strtolower(trim($row[1]));
                        if ($email && isset($currentEmails[$email])) {
                            $currentEmails[$email]['checked'] = 1;
                        } else {
                            var_dump($row);
                            die('нет такой почты в общем списке файлов!');
                        }
                    }

                    fclose($fpCheckedFile);
                } else {
                    die('ошибка чтения файла!');
                }
            }
        }

        closedir($handleDir);
    }
    usort($currentEmails, function ($a, $b) {
        return ($a['checked'] - $b['checked']);
    });
    ?>
    <table>
        <tr>
            <td>#</td>
            <td>Почта</td>
            <td>Валидная</td>
        </tr>
        <tbody>

        <?
        $k = 0;
        foreach ($currentEmails as $emailInfo): ?>
            <? $k++; ?>
            <tr>
                <td><?= $k; ?></td>
                <td><?= htmlspecialchars($emailInfo['email']); ?></td>
                <td><?= htmlspecialchars($emailInfo['checked']); ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <?
}