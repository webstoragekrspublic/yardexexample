<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//скрипт для вывода почт, на которые были отправлены письма (из истории модуля smtp), + почты всех пользователей

include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");
$allEmailsInfo = [];
if (CModule::IncludeModule("wsrubi.smtp")) {
    $arFilter['MODULE_ID'] = 'wsrubi.smtp';
    /** @global string $by */
    /** @global string $order */
    $rsData = CEventLog::GetList(array("ID" => "DESC"), $arFilter, false);
    while ($row = $rsData->fetch()) {
        if (preg_match("/to-\s*'(.*?)'/", $row["DESCRIPTION"], $match)) {
            $emails = explode(',', $match[1]);
        } elseif (preg_match("/(\w+@\w+\.\w+)/", $row["DESCRIPTION"], $match)) {
            $emails = explode(',', $match[1]);
        } else {
            $emails = false;
        }
        if ($emails) {
            foreach ($emails as $email) {
                $email = strtolower(trim($email));
                if (isset($allEmailsInfo[$email])) {
                    $allEmailsInfo[$email]['sended']++;
                } else {
                    $allEmailsInfo[$email] = [
                        'email' => $email,
                        'sended' => 1,
                        'userid' => 0
                    ];
                }
            }
        }
    }
//    var_dump($allEmailsInfo);
} else {
    echo 'модуль smtp почты не подключен';
}


$tmp = 'sort';
$order = 'id';
$parameters = [
    'FIELDS' => ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'EMAIL', 'PERSONAL_PHONE', 'WORK_PHONE']
];
$usersQ = \CUser::GetList($order, $tmp, [], $parameters);

while ($row = $usersQ->fetch()) {
    $email = strtolower(trim($row['EMAIL']));
    if (isset($allEmailsInfo[$email])) {
        $allEmailsInfo[$email]['userid'] = $row['ID'];
    } else {
        $allEmailsInfo[$email] = [
            'email' => $email,
            'sended' => 0,
            'userid' => $row['ID']
        ];
    }
}
usort($allEmailsInfo, function ($a, $b) {
    return ($b['sended'] - $a['sended']);
});

?>

    <table>
        <tr>
            <td>#</td>
            <td>Почта</td>
            <td>Отправлено раз(по логам модуля)</td>
            <td>Пользователь id</td>
        </tr>
        <tbody>

        <?
        $k = 0;
        foreach ($allEmailsInfo as $emailInfo): ?>
            <? $k++; ?>
            <tr>
                <td><?= $k; ?></td>
                <td><?= htmlspecialchars($emailInfo['email']); ?></td>
                <td><?= htmlspecialchars($emailInfo['sended']); ?></td>
                <td><?= htmlspecialchars($emailInfo['userid']); ?></td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

<?

echo 'end';