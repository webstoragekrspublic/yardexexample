<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER_FIELD_MANAGER;
$fields = [
    'UF_MANAGER_COMMENT' => 'Я работаю'
];

$look = $USER_FIELD_MANAGER->Update('USER', 440, $fields);

$look2 = $USER_FIELD_MANAGER->GetUserFields('USER', 440);

$parameters = [
    'select' => ['ID', 'USER_ID', 'COMMENTS'],
    'order' => ['USER_ID' => 'ASC', 'DATE_INSERT' => 'DESC']
];
$orders = \Bitrix\Sale\Order::getList($parameters);
$user = null;
$isUpdate = false;
while ($item = $orders->fetch())
{
    if ($item['COMMENTS'] != null) {
        if ($user != $item['USER_ID'] && $isUpdate == false){
            $fields = [
                'UF_MANAGER_COMMENT' => $item['COMMENTS']
            ];
            $USER_FIELD_MANAGER->Update('USER', $item['USER_ID'], $fields);
            $isUpdate = true;
            $user = $item['USER_ID'];
        }
    }
    if ($user != $item['USER_ID'] && $isUpdate == true){$isUpdate = false;}
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>