<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//скрипт подсчитывает количество заказов каждого пользователя и добавляет эти данные в свойство TOTAL_ORDERS
//для каждого заказа с нулевой записи в базе данных

$dbRes = \Bitrix\Sale\Property::getList([
    'select' => ['ID', 'NAME', 'TYPE', 'CODE'],
    'filter' => ['CODE' => 'TOTAL_ORDERS']
]);
$property = $dbRes->fetch();

$parameters = [
    'select' => ['ID', 'USER_ID'],
    'order' => ['USER_ID' => 'ASC', 'DATE_INSERT' => 'ASC']
];
$orders = \Bitrix\Sale\Order::getList($parameters);
$user = null;
$count = null;
while ($item = $orders->fetch())
{
    if ($item['USER_ID'] != $user){
        $user = $item['USER_ID'];
        $count = 1;
    }
    $order = \Bitrix\Sale\Order::load($item['ID']);
    $collection = $order->getPropertyCollection();
    $propertyValue = $collection->getItemByOrderPropertyId($property['ID']);
    if (!$propertyValue) {
        $propertyValue = \Bitrix\Sale\PropertyValue::create($collection, [
            'ID' => $property['ID'],
            'NAME' => $property['NAME'],
            'TYPE' => $property['TYPE'],
            'CODE' => $property['CODE'],
        ]);
        $propertyValue->setField('VALUE', $count);
        $collection->addItem($propertyValue);
    }
    else{
        $propertyValue->setField('VALUE', $count);
        $order->save();
    }
    $count++;
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>