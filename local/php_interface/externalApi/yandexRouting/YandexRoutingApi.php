<?php

namespace ExternalApi\YandexRouting;

//https://yandex.ru/routing/doc/vrp/ref/MultipleVehiclesRouting/addMVRPTask.html
class YandexRoutingApi
{
    protected const LOG_FILE = __DIR__ . '/request.log';

    protected const API_KEY = '96faf6c8-533e-469d-893a-d6dfc543d8bb';

    protected const API_URL = 'https://courier.yandex.ru/vrs/api/v1/';

    static protected function request($apiPath, $method = 'GET', array $bodyParams = [], array $GETParams = [])
    {
        $urlParams = $GETParams;
        $urlParams['apikey'] = self::API_KEY;
        $url = self::API_URL . $apiPath . "?" . http_build_query($urlParams);
        $urlForLog = self::API_URL . $apiPath . "?" . http_build_query($GETParams);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        
        writeAppendLog(array("CURLOPT_URL" => $url));

        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            
            writeAppendLog(array("CURLOPT_POST" => 1));
            
            if ($bodyParams) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($bodyParams, JSON_UNESCAPED_UNICODE));
                
                writeAppendLog(array("CURLOPT_POSTFIELDS" => json_encode($bodyParams, JSON_UNESCAPED_UNICODE)));
            }
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        
        writeAppendLog(array("CURLOPT_CUSTOMREQUEST" => $method));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        writeAppendLog(array("CURLOPT_RETURNTRANSFER" => 1));

        self::log(
            [
                'url' => $urlForLog,
                'method' => $method,
                'bodyKeys' => array_keys($bodyParams)
            ],
            "Request"
        );

        $data = curl_exec($ch);
        $res = json_decode($data, 1);
        if (json_last_error()) {
            $res = [
                "error" => [
                    "incident_id" => 'json_decode',
                    "message" => "Ошибка декодирования ответа: " . $data
                ]
            ];
        }

        self::log(
            [
                'url' => $urlForLog,
                'data' => $res,
            ],
            "Response"
        );

        return $res;
    }

    protected static function log(array $data, $type = '')
    {
        return file_put_contents(self::LOG_FILE,
            date(\DateTimeInterface::W3C) . ' ' . $type . ' ' . print_r($data, 1),
            FILE_APPEND
        );
    }

    static public function addMVRP($requestBody)
    {
        $method = 'POST';
        $apiPath = 'add/mvrp';
        return self::request($apiPath, $method, $requestBody);
    }

    static public function resultMVRP($id)
    {
        $method = 'GET';
        $apiPath = 'result/mvrp/' . $id;
        return self::request($apiPath, $method);
    }
}
