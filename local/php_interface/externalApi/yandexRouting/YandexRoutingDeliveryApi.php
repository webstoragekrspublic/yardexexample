<?php

namespace ExternalApi\YandexRouting;

//https://yandex.ru/routing/doc/delivery/ref/index.html
class YandexRoutingDeliveryApi
{
    protected const TOKEN_INFO = [
        "access_token" => "AQAAAAAavHbQAAd3vm4d1MpEkEjWgfCeHcLXYpM",
        "token_type" => "bearer",
        "expires_in" => "31536000"
    ];

    protected const API_URL = 'https://courier.yandex.ru/api/v1/';

    protected const COMPANY_ID = '33690';

    static protected function request($apiPath, $method = 'GET')
    {
        $url = self::API_URL . $apiPath;

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Auth '.self::TOKEN_INFO['access_token']
        ));

        $data = curl_exec($ch);
        $res = json_decode($data, 1);
        if (json_last_error()) {
            $res = [
                "error" => [
                    "incident_id" => 'json_decode',
                    "message" => "Ошибка декодирования ответа: " . $data
                ]
            ];
        }

        return $res;
    }

    /*
     * return array[
       [ ["id"]=>
        int(3360042)
        ["company_id"]=>
        int(33690)
        ["number"]=>
        string(8) "12042021"
        ["name"]=>
        string(8) "Миша"
        ["phone"]=>
        string(12) "+79135738739"
        ["sms_enabled"]=>
        bool(false)
        ],
    ]
     */
    static public function getAllCouriers()
    {
        $method = 'GET';
        $apiPath = 'companies/'.self::COMPANY_ID.'/couriers';
        return self::request($apiPath, $method);
    }
}
