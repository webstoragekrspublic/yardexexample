<?php

namespace ExternalApi\YandexRouting;

use ExternalApi\YandexMaps\YandexMapsApi;
use Helpers\CustomTools;

class YandexRouting
{

    /*
     * ограничиваем поиск координат по этому квадрату
     */
    protected static $mapApiSuggestBoundedBy = '92.491655,55.898952~93.717311,56.294369';

    /*
     * не берем заказы с этими статусами
     */
    protected const bannedStatusId = [
        'O' => true
    ];

    protected static function addTaskFor(array $orders, $vehicles)
    {
        if (!$orders) {
            return false;
        }
        $requestBody = [
            'locations' => [],
            'options' => self::getOptions(),
            'vehicles' => $vehicles,
            'depot' => self::getDepot()
        ];

        foreach ($orders as $order) {
            $requestBody['locations'][] = [
                'id' => (int)$order['ID'],
                'title' => $order['USER_NAME'] ?? '',
                'phone' => $order['SALE_INTERNALS_ORDER_PROPERTY_PHONE_VALUE'] ?? '',
                'description' => $order['SALE_INTERNALS_ORDER_PROPERTY_ADDRESS_VALUE'] ?? '',
                'time_window' => self::convertAnyTimeToTimeWindow($order['SALE_INTERNALS_ORDER_PROPERTY_DELIVERY_TIME_VALUE']),
                'comments' => $order['USER_DESCRIPTION'] ?? '',
                'point' => self::getCoordsObjFromString($order['SALE_INTERNALS_ORDER_PROPERTY_GEO_COORDS_VALUE']),
                'hard_window' => true,
                'shared_service_duration_s' => 0,
                'service_duration_s' => 600,
                'penalty' => [
                    'late' => [
                        'fixed' => 20000,
                        'minute' => 20000
                    ]
                ],
                'type' => 'delivery'
            ];
        }

        writeAppendLog(array("requestBody" => $requestBody));
        return YandexRoutingApi::addMVRP($requestBody);
    }

    public static function addOrders(array $ordersId, array $couriersId)
    {
        $res = [
            'error' => 'что-то пошло не так',
            'answer' => ''
        ];

        $couriers = self::getVehiclesWithIdsForApi($couriersId);

        $orderFilter = [
            'ID' => $ordersId
        ];
        $orders = self::getOrdersFilterBy($orderFilter);

        $apiInfo = self::addTaskFor($orders, $couriers);

        if (isset($apiInfo['error']) || !isset($apiInfo['id'])) {
            $res['error'] = 'Ошибка отравки: ' . $apiInfo['error']['message'];
        } else {
            $yandexRoutingId = $apiInfo['id'];
            $errsWithOrders = self::setYandexRoutingIdToOrders(array_column($orders, 'ID'), $yandexRoutingId);
            $res['error'] = false;
            $res['answer'] = "Задача на маршрутизацию создана! (id:{$apiInfo['id']} message:{$apiInfo['message']})<br>
                                <a href='https://yandex.ru/courier/companies/33690/depots/all/mvrp/" . htmlspecialchars($yandexRoutingId) . "' target='_blank'>
                                    Ссылка
                                </a> на планирование";
            if ($errsWithOrders) {
                $res['answer'] .= '<br>Ошибки при обновлении заказов!!!:' . implode('<br>', $errsWithOrders);
            }
        }

        return $res;
    }

    protected static function setYandexRoutingIdToOrders(array $ordersId, $yandexRoutingId): array
    {
        $errors = [];

        $yandexRoutingPropCode = 'ROUTING_API_CODE';

        foreach ($ordersId as $orderId) {
            $order = \Bitrix\Sale\Order::load($orderId);
            if ($order) {
                $propertyCollection = $order->getPropertyCollection();
                $ar = $propertyCollection->getArray();
                foreach ($ar['properties'] as $arProp) {
                    if ($arProp['CODE'] == $yandexRoutingPropCode) {
                        $prop = $propertyCollection->getItemByOrderPropertyId($arProp['ID']);
                        $prop->setValue($yandexRoutingId);
                    }
                }
                $saveRes = $order->save();
                if (!$saveRes->isSuccess()) {
                    $errsWithOrders[] = "Не удалось сохранить данные о Маршрутизации в заказ с номером #{$orderId}!!! обязательно проверьте заказ в магазине и в яндекс маршрутизации!";
                }
            } else {
                $errsWithOrders[] = "Не удалось найти заказ с номером #{$orderId}!!! обязательно проверьте заказ в магазине и в яндекс маршрутизации!";
            }
        }

        return $errors;
    }

    public static function getOrdersForDeliveryDate(string $deliveryDate): array
    {
        $filter = [
            '=PROPERTY_DELIVERY_DATE.VALUE' => $deliveryDate,
        ];
        return self::getOrdersFilterBy($filter);
    }

    protected static function getOrdersFilterBy(array $customFilter): array
    {
        $filter = [
            '=PROPERTY_ROUTING_API_CODE.CODE' => 'ROUTING_API_CODE',
            '=PROPERTY_DELIVERY_DATE.CODE' => 'DELIVERY_DATE',
            '=PROPERTY_DELIVERY_TIME.CODE' => 'DELIVERY_TIME',
            '=PROPERTY_ADDRESS.CODE' => 'ADDRESS',
            '=PROPERTY_GEO_COORDS.CODE' => 'GEO_COORDS',
            "=PROPERTY_PHONE.CODE" => 'PHONE',
        ];
        $filter = array_merge($filter, $customFilter);
        $dbRes = \Bitrix\Sale\Order::getList([
            'select' => [
//                '*',
                'ID',
                'USER_DESCRIPTION',
                'STATUS_ID',
                'USER_ID',
                'USER_DESCRIPTION',
                "PROPERTY_ROUTING_API_CODE.VALUE",
                "PROPERTY_DELIVERY_DATE.VALUE",
                "PROPERTY_DELIVERY_TIME.VALUE",
                "PROPERTY_ADDRESS.VALUE",
                "PROPERTY_PHONE.VALUE",
                "PROPERTY_GEO_COORDS.VALUE"
            ],
            'filter' => $filter,
            'runtime' => [
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_ROUTING_API_CODE',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_DELIVERY_DATE',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_DELIVERY_TIME',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_ADDRESS',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_PHONE',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_GEO_COORDS',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
            ],
            'order' => ['ID' => 'DESC'],
        ]);
        $orders = $dbRes->fetchAll();
        foreach ($orders as $key => $value) {
            $orders[$key]['WARNINGS'] = [];
            $orders[$key]['ERRORS'] = [];
            $orders[$key]['YANDEX_ROUTING_SENDED'] = !empty($value['SALE_INTERNALS_ORDER_PROPERTY_ROUTING_API_CODE_VALUE']);
        }

        $orders = self::filterOrders($orders);
        $orders = self::sortByDeliveryTimeOrders($orders);
        $orders = self::addUserNameToOrders($orders);
        $orders = self::addGeoCoordsToOrders($orders);

        return $orders;
    }

    protected static function sortByDeliveryTimeOrders(array $orders)
    {
        if (!$orders) {
            return $orders;
        }

        usort($orders, function ($a, $b) {
            $deliveryTimeA = (int)$a['SALE_INTERNALS_ORDER_PROPERTY_DELIVERY_TIME_VALUE'];
            $deliveryTimeB = (int)$b['SALE_INTERNALS_ORDER_PROPERTY_DELIVERY_TIME_VALUE'];
            if ($deliveryTimeA == $deliveryTimeB) {
                return ($a['ID'] < $b['ID']) ? -1 : 1;
            }
            return ($deliveryTimeA < $deliveryTimeB) ? -1 : 1;
        });
        return $orders;
    }

    protected static function addUserNameToOrders(array $orders)
    {
        if (!$orders) {
            return $orders;
        }

        $userIds = array_column($orders, 'USER_ID');

        $users = \Bitrix\Main\UserTable::getList(array(
            'filter' => array(
                '=ID' => $userIds,
            ),
            'select' => array('ID', 'NAME', 'SECOND_NAME', 'LAST_NAME'),

        ))->fetchAll();

        if ($users) {
            $users = CustomTools::indexArrayByKey($users, 'ID');
            foreach ($orders as $key => $order) {
                $userName = '';
                if (isset($users[$order['USER_ID']])) {
                    $user = $users[$order['USER_ID']];
                    $userName = $user['NAME'] . ' ' . $user['SECOND_NAME'] . ' ' . $user['LAST_NAME'];
                }
                $orders[$key]['USER_NAME'] = $userName;
            }
        }

        return $orders;
    }

    protected static function addGeoCoordsToOrders(array $orders)
    {
        if ($orders) {
            foreach ($orders as $key => $order) {
                if (
                    !self::isValidCoords($order['SALE_INTERNALS_ORDER_PROPERTY_GEO_COORDS_VALUE'])
                ) {
                    $apiCoordsInfo = YandexMapsApi::getFirstAddressByParams([
                        'geocode' => $order['SALE_INTERNALS_ORDER_PROPERTY_ADDRESS_VALUE'],
                        'filter' => ['shopsName' => ['Metro', 'Continent']],
                        'bbox' => self::$mapApiSuggestBoundedBy
                    ]);
                    if ($apiCoordsInfo) {
                        $coords = self::geoCoordsChangeLatLong($apiCoordsInfo['GeoObject']['Point']['pos']);
                        if ($coords) {
                            $orders[$key]['SALE_INTERNALS_ORDER_PROPERTY_GEO_COORDS_VALUE'] = $coords;
                            $kind = $apiCoordsInfo['GeoObject']['metaDataProperty']['GeocoderMetaData']['kind'];
                            $LocalityName = $apiCoordsInfo['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName'];
                            if ($kind != 'house' && $kind != 'other' && $kind != 'entrance') {
                                $orders[$key]['WARNINGS'][] = 'получены не точные координаты! точность = ' . $kind;
                            }
                            if ($LocalityName != 'Красноярск') {
                                $orders[$key]['WARNINGS'][] = 'город не Красноярск (' . $LocalityName . ')';
                            }
                            $orders[$key]['YANDEX_ADDRESS_INFO'] = $apiCoordsInfo;

                        } else {
                            $orders[$key]['ERRORS'][] = 'ошибка получения координат адреса';
                        }
                    } else {
                        $orders[$key]['ERRORS'][] = 'ошибка обращения к апи получения координат';
                    }
                }
            }
        }

        return $orders;
    }

    protected static function isValidCoords(string $coords)
    {
        if (!empty($coords) && preg_match('/[\d]+\.[\d]+,[\d]+\.[\d]+/', $coords)) {
            return true;
        }
        return false;
    }

    protected static function filterOrders(array $orders)
    {
        $res = [];

        if ($orders) {
            foreach ($orders as $order) {
                if (!isset(self::bannedStatusId[$order['STATUS_ID']])) {
                    $res[] = $order;
                }
            }
        }

        return $res;
    }

    /*
     * меняем местами lat lon по разделителю $inSeparator и собираем по разделителю $resSeparator
     * 92.895466 56.01386 => 56.01386,92.895466
     */
    protected static function geoCoordsChangeLatLong(string $geoCoords, $inSeparator = ' ', $resSeparator = ',')
    {
        $res = false;
        $coords = explode($inSeparator, $geoCoords);
        if (count($coords) == 2) {
            $resCoords = [
                trim($coords[1]),
                trim($coords[0]),
            ];
            $res = implode($resSeparator, $resCoords);
        }
        return $res;
    }

    protected static function getCoordsObjFromString(string $latLon, $separator = ',')
    {
        $exp = explode($separator, $latLon);
        if (count($exp) != 2) {
            return false;
        }

        return [
            'lat' => (float)$exp[0],
            'lon' => (float)$exp[1]
        ];
    }

    protected static function getDepot()
    {
        return [
            'description' => 'Склад:Рязанская 65г',
            'title' => 'Склад',
            'time_window' => '08:00-23:59',
            'point' => [
                'lat' => 56.031086,
                'lon' => 93.072111
            ],
            'hard_window' => false,
            'flexible_start_time' => true,
            'service_duration_s' => 300
        ];
    }

    protected static function getOptions()
    {
        return [
            //Качество оптимизации маршрута:
            //
            //low - отладочный режим для разработки и проверки ограничений, небольшое время выполнения;
            //normal - базовое качество оптимизации, среднее время выполнения;
            //high - максимально возможное качество, большое время выполнения.
            //Обязательное поле.
            'quality' => 'normal',
            'time_zone' => 'Asia/Krasnoyarsk',
            'merge_multiorders' => true,
            'minimize_lateness_risk' => false,
            'avoid_tolls' => false,
            'date' => date('Y-m-d'),
            'balanced_groups' => [
                [
                    'id' => '1',
                    'penalty' => [
                        'hour' => 2000,
                        'stop' => 10000
                    ]
                ],
                [
                    'id' => '2',
                    'penalty' => [
                        'hour' => 200,
                        'stop' => 100
                    ]
                ],
            ]
        ];
    }

    public static function getVehicles($shiftsId): array
    {
        $couriers = YandexRoutingDeliveryApi::getAllCouriers();
        $res = [];
        if (isset($couriers['error'])) {
            return $res;
        }

        foreach ($couriers as $courier) {
            $res[] = [
                'name' => $courier['name'],
                'id' => $courier['id'],
                'number' => $courier['number'],
                'return_to_depot' => true,
                'shifts' => [
                    [
                        'balanced_group_id' => '1',
                        'hard_time_window' => '10:30-23:00',
                        'time_window' => '10:30-23:00',
                        'id' => $shiftsId,
                    ]
                ]
            ];
        }

        return $res;
    }

    protected static function getVehiclesWithIdsForApi(array $vehiclesId): array
    {
        $shiftName = self::getTodayDeliveryDate();
        $allVehicles = self::getVehicles($shiftName);
        $resVehicles = [];
        foreach ($allVehicles as $vehicle) {
            if (in_array($vehicle['id'], $vehiclesId)) {
                $resVehicles[] =
                    [
                        'id' => $vehicle['number'],
                        'ref' => $vehicle['name'],//Дополнительно вы можете указать строковый идентификатор автомобиля из вашей учетной системы в поле ref.
                        'return_to_depot' => $vehicle['return_to_depot'],
                        'shifts' => $vehicle['shifts']
                    ];
            }
        }
        return $resVehicles;
    }

    protected static function convertAnyTimeToTimeWindow($timeSlot)
    {
        $defaultTime = '11:00-23:00';
        if (preg_match('/^\d/', $timeSlot)) {
            return $timeSlot;
        }
        return $defaultTime;
    }

    public static function getTodayDeliveryDate()
    {
        return date('d.m.Y');
    }
}
