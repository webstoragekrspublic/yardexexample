<?php

namespace ExternalApi\SberbankPayment;

use Bitrix\Sale;

class SberbankApi
{
//https://securepayments.sberbank.ru/wiki/doku.php/integration:api:start#%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D1%84%D0%B5%D0%B9%D1%81_rest
//'SBERBANK_PROD_URL' => 'https://securepayments.sberbank.ru/payment/rest/',
//'SBERBANK_TEST_URL' => 'https://3dsec.sberbank.ru/payment/rest/',
    protected const BASE_API_URL = 'https://securepayments.sberbank.ru/payment/rest/';
    protected const LOGIN = 'yardex-api';
    protected const PASSWORD = 'r24E8fAG';

    static protected function request($apiPath, $fields = [])
    {
        $method = 'POST';
        $fields['userName'] = self::LOGIN;
        $fields['password'] = self::PASSWORD;
        $url = self::BASE_API_URL . $apiPath;
        $ch = curl_init();
        $preparedFields = http_build_query($fields);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $preparedFields);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        $headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: ' . strlen($preparedFields)
        ];
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $data = curl_exec($ch);
        $result = false;

        try {
            if ($errno = curl_errno($ch)) {
                $error_message = curl_strerror($errno);
                $message = "cURL error ({$errno}):\n {$error_message}<br>" . $data;
                throw new \Exception($message);
            }

            if ($data) {
                $dataDecoded = json_decode($data, 1);
                if (!$dataDecoded) {
                    throw new \Exception('ошибка декодирования ответа ' . $data);
                } else if (isset($dataDecoded['errorCode']) && $dataDecoded['errorCode'] !== '0') {
                    $errMessage = 'присутствует ошибка в ответе errorCode:' . $dataDecoded['errorCode'] .
                        ' errorMessage:' . $dataDecoded['errorMessage'];
                    throw new \Exception($errMessage);
                } else {
                    $result = $dataDecoded;
                }
            } else {
                throw new \Exception('отсутсвует ответ от сервера ' . $data);
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $result;
    }

    static public function getOrderPaymentStatus($orderNumber)
    {
        $method = 'getOrderStatusExtended.do';
        $fields = [
            'orderNumber' => $orderNumber
        ];
        return static::request($method, $fields);
    }

    static public function confirmOrder($orderId, $params)
    {
        $method = 'deposit.do';
        $fields = $params;
        $fields['orderId'] = $orderId;
        return static::request($method, $fields);
    }

}