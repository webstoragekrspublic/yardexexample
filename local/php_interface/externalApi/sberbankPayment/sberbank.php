<?php

namespace ExternalApi\SberbankPayment;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale;
use Sberbank\Payments\Gateway;

class Sberbank
{
    protected const SBER_PAY_SYSTEMS_ID = [25, 33, 34];

    protected const orderStatusDecoding = [
        0 => "заказ зарегистрирован, но не оплачен",
        1 => "предавторизованная сумма удержана (можно подтвердить оплату!)",
        2 => "проведена полная авторизация суммы заказа (оплата уже подтверждена!)",
        3 => "авторизация отменена",
        4 => "по транзакции была проведена операция возврата",
        5 => "инициирована авторизация через сервер контроля доступа банка-эмитента",
        6 => "авторизация отклонена"
    ];

    private const taxesValueCode = [
        0 => 0,
        10 => 2,
        18 => 3,
        20 => 6,
    ];

    static public function getOrderPaymentInfo($orderId)
    {
        $resInfo = [
            'error' => 1,
            'errorText' => 'что-то пошло не так',
        ];
        if (!\Helpers\SafetyValidation::isAdminOrManger(\CUser::GetID())) {
            $resInfo = [
                'error' => 1,
                'errorText' => 'у вас недостаточно прав',
            ];
            return $resInfo;
        }
        $order = Sale\Order::load($orderId);
        if ($order) {
            $paymentCollection = $order->getPaymentCollection();
            $paymentInfo = false;
            foreach ($paymentCollection as $payment) {
                if (in_array($payment->getPaymentSystemId(), self::SBER_PAY_SYSTEMS_ID)) {
                    $currentPaymentOrder = $paymentCollection->current();
                    $isReturned = $payment->isReturn();
                    if (!$isReturned) {
                        $lastPaymentInfo = false;
                        $currentPaymentOrderId = $currentPaymentOrder->getField("ID");
                        for ($i = 0; $i < 30; $i++) {
                            $orderNumber = $orderId . "_" . $currentPaymentOrderId . '_' . $i;
                            try {
                                $paymentInfo = SberbankApi::getOrderPaymentStatus($orderNumber);
                                if ($paymentInfo['orderStatus'] >= 0 && $paymentInfo['orderStatus'] < 6) {
                                    break;
                                }
                            } catch (\Exception $e) {
                                if (!$lastPaymentInfo) {
                                    $resInfo = [
                                        'error' => 1,
                                        'errorText' => $e->getMessage(),
                                    ];
                                }
                                break;
                            }
                        }
                        if (!$lastPaymentInfo) {
                            for ($i = 0; $i < 30; $i++) {
                                $orderNumber = $orderId . "_" . $i;
                                try {
                                    $paymentInfo = SberbankApi::getOrderPaymentStatus($orderNumber);
                                    if ($paymentInfo['orderStatus'] >= 0 && $paymentInfo['orderStatus'] < 6) {
                                        break;
                                    }
                                } catch (\Exception $e) {
                                    if (!$lastPaymentInfo) {
                                        $resInfo = [
                                            'error' => 1,
                                            'errorText' => $e->getMessage(),
                                        ];
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if (!$paymentInfo) {
                $resInfo = [
                    'error' => 1,
                    'errorText' => 'нет оплат требующих проверки',
                ];
            } else {
                $orderPaymentUrl = '';
                if ($paymentInfo['merchantOrderParams']) {
                    foreach ($paymentInfo['merchantOrderParams'] as $paymentParam) {
                        if ($paymentParam['name'] == 'formUrl') {
                            $orderPaymentUrl = $paymentParam['value'];
                            break;
                        }
                    }
                }
                $orderPaymentId = '';
                if ($paymentInfo['attributes']) {
                    foreach ($paymentInfo['attributes'] as $paymentParam) {
                        if ($paymentParam['name'] == 'mdOrder') {
                            $orderPaymentId = $paymentParam['value'];
                            break;
                        }
                    }
                }

                $paymentInfoToView = [
                    'orderNumber' => $paymentInfo['orderNumber'],
                    'orderPaymentId' => $orderPaymentId,
                    'orderStatusCode' => $paymentInfo['orderStatus'],
                    'orderStatusText' => static::decodeOrderStatus($paymentInfo['orderStatus']),
                    'actionCode' => $paymentInfo['actionCode'],
                    'actionCodeDescription' => $paymentInfo['actionCodeDescription'],
                    'amount' => round($paymentInfo['amount'] / 100, 2),
                    'currency' => ($paymentInfo['currency'] == 643 ? 'руб.' : 'валюта не определена!'),
                    'orderPaymentUrl' => $orderPaymentUrl,
                    'canConfirm' => ($paymentInfo['orderStatus'] == 1 ? true : false)
                ];

                $resInfo = [
                    'error' => 0,
                    'errorText' => '',
                    'paymentInfo' => $paymentInfoToView,
                ];
            }
        } else {
            $resInfo = [
                'error' => 1,
                'errorText' => 'возможно заказ #' . $orderId . ' был уже удален.',
            ];
        }

        return $resInfo;
    }

    static public function confirmOrderPayment($orderId, $orderPaymentId)
    {
        $resInfo = [
            'error' => 1,
            'errorText' => 'что-то пошло не так',
        ];
        if (!\Helpers\SafetyValidation::isAdminOrManger(\CUser::GetID())) {
            $resInfo = [
                'error' => 1,
                'errorText' => 'у вас недостаточно прав',
            ];
            return $resInfo;
        }
        $order = Sale\Order::load($orderId);
        if ($order && $orderPaymentId) {
            $totalSum = $order->getPrice(); // Сумма заказа
            if ($totalSum > 0) {
                $params = [
                    'amount' => (string)($totalSum * 100),
                    'depositItems' => json_encode(['items' => self::getDepositItemsFromOrder($order)], JSON_UNESCAPED_UNICODE)
                ];
                try {
                    $data = SberbankApi::confirmOrder($orderPaymentId, $params);
                    $resInfo = [
                        'error' => 0,
                        'errorText' => '',
                        'confirmOrderInfo' => $data
                    ];
                } catch (\Exception $e) {
                    $resInfo = [
                        'error' => 1,
                        'errorText' => $e->getMessage(),
                    ];
                }
            } else {
                $resInfo = [
                    'error' => 1,
                    'errorText' => 'Ошибка стоимости заказа! стоимость = ' . $totalSum
                ];
            }
        } else {
            $resInfo = [
                'error' => 1,
                'errorText' => 'возможно заказ #' . $orderId . ' был уже удален. Или неверный номер оплаты №' . $orderPaymentId,
            ];
        }
        return $resInfo;
    }

    static protected function getDepositItemsFromOrder(Sale\Order $order)
    {
        $items = [];
        if ($order) {
            $basket = $order->getBasket();
            $positionId = 0;
            foreach ($basket as $basketItem) {
                $newItem = [
                    'positionId' => $positionId,
                    'itemCode' => $basketItem->getProductId(),
                    'name' => $basketItem->getField('NAME'),
                    'itemAmount' => (string)($basketItem->getFinalPrice() * 100),
                    'itemPrice' => (string)($basketItem->getPrice() * 100),
                    'quantity' => [
                        'value' => (string)$basketItem->getQuantity(),
                        'measure' => $basketItem->getField('MEASURE_NAME') ?? 'шт',
                    ],
                    'tax' => [
                        'taxType' => self::getTaxCode($basketItem->getField('VAT_RATE') * 100),
                    ]
                ];
                $items[] = $newItem;
                $positionId++;
            }

            if ($order->getField('PRICE_DELIVERY') > 0) {

                Loader::includeModule('catalog');
                $deliveryInfo = \Bitrix\Sale\Delivery\Services\Manager::getById($order->getField('DELIVERY_ID'));

                $deliveryVatItem = \CCatalogVat::GetByID($deliveryInfo['VAT_ID'])->Fetch();
                $newItem = [
                    'positionId' => $positionId,
                    'itemCode' => 'DELIVERY_' . $order->getField('DELIVERY_ID'),
                    'name' => 'Доставка',
                    'itemAmount' => (string)($order->getField('PRICE_DELIVERY') * 100),
                    'itemPrice' => (string)($order->getField('PRICE_DELIVERY') * 100),
                    'quantity' => [
                        'value' => 1,
                        'measure' => 'ед.',
                    ],
                    'tax' => [
                        'taxType' => self::getTaxCode($deliveryVatItem['RATE']),
                    ],
                ];
                $items[] = $newItem;
            }
        }

        return $items;
    }

    protected static function getTaxCode($taxRate)
    {
        return self::taxesValueCode[(int)$taxRate] ?? 0;
    }

    static protected function decodeOrderStatus($orderStatusId)
    {
        return self::orderStatusDecoding[$orderStatusId] ?? 'не найдено описание статуса №' . $orderStatusId;
    }
}
