<?php

namespace ExternalApi\YandexMaps;

class YandexMapsApi
{
    //https://yandex.ru/dev/maps/geocoder/doc/desc/concepts/input_params.html
    protected const API_KEY = '84b15b93-5d24-4359-b0f1-47e70f5a9ed6';

    protected const GEOCODE_API_URL = 'https://geocode-maps.yandex.ru/1.x/';


    static protected function request($sendParams = false, $method = 'GET', $api = 'geocode')
    {
        $baseApiParams = [
            'apikey' => self::API_KEY,
            'format' => 'json'
        ];
        switch ($api) {
            case 'geocode':
                $api_url = static::GEOCODE_API_URL;
                $sendParams = array_merge($sendParams,$baseApiParams);
                $api_url .= '?'.http_build_query($sendParams);
                break;
            default:
                throw new \Exception('неверный $api в параметре');
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $api_url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        return json_decode($data, 1);
    }

    static public function getApiKey(){
        return self::API_KEY;
    }

    static public function geocodeByCoords($lat,$lon)
    {
        $sendParams = [
            'geocode' => "$lat,$lon",
            'sco' => 'latlong'
        ];
        $res = static::request($sendParams, 'GET', 'geocode');

        return $res['response']??false;
    }

    static public function coordsByParams($params)
    {
        $res = static::request($params, 'GET', 'geocode');

        return $res['response']??false;
    }

    static public function getFirstAddressByParams($params)
    {
        $res = static::coordsByParams($params);

        return $res['GeoObjectCollection']['featureMember'][0]??false;
    }


}
