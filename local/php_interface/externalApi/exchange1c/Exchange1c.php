<?php

namespace ExternalApi\Exchange1c;

use Helpers\CustomTools;

//вспомагательный класс для модуля htmls.1c77exchange
class Exchange1c
{
    static protected $errors = [];

    public static function putOrderInUpdateQueue($idOrder)
    {
        self::errorsClear();
        $orderInfo = Exchange1cOrderTable::getByPrimary($idOrder)->fetch();
        if (!$orderInfo) {
            $res = Exchange1cOrderTable::addItem($idOrder);
            if (!$res) {
                self::errorAdd('ошибка добавления пользователя в очередь обновлений #' . $idOrder);
            }
        } else {
            $res = Exchange1cOrderTable::updateItem($idOrder, Exchange1cOrderTable::EXCHANGE_STATUS_NEED_UPDATE);
            if (!$res) {
                self::errorAdd('ошибка добавления пользователя в очередь обновлений #' . $idOrder);
            }
        }
        self::errorsReport();
        return $res;
    }

    public static function getOrderIdsNeedExchange()
    {
        $orders = Exchange1cOrderTable::getListNeededUpdate();
        $orderIds = [];
        if ($orders) {
            foreach ($orders as $order) {
                $orderIds[$order['ID_ORDER']] = $order['ID_ORDER'];
            }

            //смотрим актуальность данных, не удален ли заказ из системы
            $deleteOrderIdsFromExchange = $orderIds;
            $filter = ['ID' => $orderIds];
            $ordersOnSite = \CSaleOrder::GetList([], $filter);
            while ($order = $ordersOnSite->fetch()) {
                unset($deleteOrderIdsFromExchange[$order['ID']]);
            }
            if ($deleteOrderIdsFromExchange) {
                foreach ($deleteOrderIdsFromExchange as $deleteOrderId) {
                    unset($orderIds[$deleteOrderId]);
                    Exchange1cOrderTable::delItem($deleteOrderId);
                }
            }
            /////////////////////////////////////////////////////
        }
        return $orderIds;
    }

    public static function setOrderExchangeStatusFinished($idOrder)
    {
        try {
            Exchange1cOrderTable::updateItem($idOrder, Exchange1cOrderTable::EXCHANGE_STATUS_COMPLETED);
        } catch (\Exception $exception) {
        }
    }

    protected static function errorAdd($addErrorMsg)
    {
        static::$errors[] = $addErrorMsg;
    }

    protected static function errorsClear()
    {
        static::$errors = [];
    }

    static protected function errorsGet()
    {
        return static::$errors;
    }


    static protected function errorsReport()
    {
        if ($errors = static::errorsGet()) {
            $text = '';
            $text .= implode(' <br>', $errors);
            $sendParams = [
                'THEME' => 'Ошибки ExternalApi\Exchange1c',
                'BODY_TEXT' => $text,
            ];

            CustomTools::sendErrorsReport($sendParams, 138);
        }
    }
}