<?php

namespace ExternalApi\Exchange1c;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class Exchange1cOrderTable extends Entity\DataManager
{
    public const EXCHANGE_STATUS_NEED_UPDATE = 0;
    public const EXCHANGE_STATUS_COMPLETED = 2;
    public const EXCHANGE_STATUS_ERROR = -1;

    public static function getTableName() {
        return 'custom_htmls1c77_exchange_order';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID_ORDER', array(
                'primary' => true,
                'required' => true,
            )),
            new Entity\IntegerField('EXCHANGE_STATUS', array(
                'required' => true,
                'default_value' => static::EXCHANGE_STATUS_NEED_UPDATE,
            )),

            new Entity\DatetimeField('TIME_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
            new Entity\DatetimeField('TIME_UPDATED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }

    public static function updateItem($idOrder, $status = false)
    {
        $res = false;
        if ($idOrder) {
            $fields['TIME_UPDATED'] = new Type\DateTime;
            if ($status !== false)
                $fields['EXCHANGE_STATUS'] = (int)$status;

            $result = static::update($idOrder, $fields);
            $res = $result->isSuccess();
        }
        return $res;
    }

    public static function addItem($idOrder)
    {
        $result = false;
        if ($idOrder) {
            try {
                $fields = [
                    'ID_ORDER' => $idOrder,
                ];
                $insertResult = static::add($fields);
                $result = $insertResult->isSuccess();
//                foreach ($insertResult->getErrors() as $error)
//                {
//                    echo $error->getCode() . ': ';
//                    echo $error->getMessage();
//                    echo "\n";
//                }
            } catch (\Exception $exception) {
//                var_dump($exception);
            }
        }
        return $result;
    }

    public static function delItem($idOrder)
    {
        if ($idOrder) {
            static::delete($idOrder);
        }
    }

    public static function getListNeededUpdate()
    {
        $q = static::getList(
            [
                'select' => ['*'],
                'filter' => ['=EXCHANGE_STATUS' => static::EXCHANGE_STATUS_NEED_UPDATE],
                'order' => ['ID_ORDER' => 'ASC']
            ]
        );

        $items = [];
        while ($row = $q->fetch()) {
            $items[] = $row;
        }
        return $items;
    }

    public static function createDbTable()
    {
//        dump(Exchange1cOrderTable::getEntity()->compileDbTableStructureDump()); //вывод запроса на создание таблицы
        if (!Application::getConnection(Exchange1cOrderTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\Exchange1c\Exchange1cOrderTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\ExternalApi\Exchange1c\Exchange1cOrderTable')->createDbTable();
            Application::getConnection(Exchange1cOrderTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`EXCHANGE_STATUS`);');
        }
    }

    public static function dropDbTable()
    {
        if (Application::getConnection(Exchange1cOrderTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\Exchange1c\Exchange1cOrderTable')->getDBTableName()
        )
        ) {
            Application::getConnection(Exchange1cOrderTable::getConnectionName())->
            queryExecute('drop table if exists ' . Base::getInstance('\ExternalApi\Exchange1c\Exchange1cOrderTable')->getDBTableName());
        }
    }

}