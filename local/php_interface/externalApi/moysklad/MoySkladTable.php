<?php

namespace ExternalApi\MoySklad;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class MoySkladTable extends Entity\DataManager
{
    public static function getTableName() {
        return 'custom_moysklad_orders';
    }

    public static function getMap() {
        return array(
            new Entity\IntegerField('ID_ORDER', array(
                'primary' => true,
                'required' => true,
            )),
            new Entity\IntegerField('ID_MOYSKLAD_ORDER', array(
                'required' => true,
                'unique' => true
            )),

            new Entity\IntegerField('ID_EXCHANGE_STATUS', array(
                'required' => true,
                'default_value' => 0,
            )),

            new Entity\DatetimeField('TIME_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
            new Entity\DatetimeField('TIME_UPDATED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }

    public static function createDbTable() {
//        dump(MoySkladTable::getEntity()->compileDbTableStructureDump()); //вывод запроса на создание таблицы
        if (!Application::getConnection(MoySkladTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\MoySklad\MoySkladTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\ExternalApi\MoySklad\MoySkladTable')->createDbTable();
            Application::getConnection(MoySkladTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ID_EXCHANGE_STATUS`);');
        }
    }

    public static function dropDbTable() {
        if (Application::getConnection(MoySkladTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\MoySklad\MoySkladTable')->getDBTableName()
        )
        ) {
            Application::getConnection(MoySkladTable::getConnectionName())->
            queryExecute('drop table if exists ' . Base::getInstance('\ExternalApi\MoySklad\MoySkladTable')->getDBTableName());


//            $deleteSql = 'DELETE FROM `' . static::getTableName() . '` WHERE `ID_USER` = ' . $userId . ';';
//            Application::getConnection(MoySkladTable::getConnectionName())->queryExecute($deleteSql);
        }
    }



    public static function getItemIdsForUserId($userId) {
        $q = static::getList(
            [
                'select' => ['ID_ITEM'],
                'filter' => ['=ID_USER' => (int)$userId],
            ]
        );

        $itemIds = [];
        while ($row = $q->fetch()) {
            $itemIds[$row['ID_ITEM']] = $row['ID_ITEM'];
        }
        return $itemIds;
    }
}