<?php

namespace ExternalApi\MoySklad;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class MoySkladOrderTable extends Entity\DataManager
{
    public const EXCHANGE_STATUS_NEED_UPDATE = 0;
    public const EXCHANGE_STATUS_UPDATED_NEED_CHECK = 1;
    public const EXCHANGE_STATUS_ORDER_COMPLETED = 2;
    public const EXCHANGE_STATUS_ERROR_CHECK_TIME_UPDATED = -1;


    public static function getTableName()
    {
        return 'custom_moysklad_order';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID_ORDER', array(
                'primary' => true,
                'required' => true,
            )),
            new Entity\StringField('ID_MOYSKLAD_ORDER', array(
                'required' => true,
                'default_value' => '0',
            )),

            new Entity\IntegerField('ID_EXCHANGE_STATUS', array(
                'required' => true,
                'default_value' => 0,
            )),

            new Entity\DatetimeField('TIME_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
            new Entity\DatetimeField('TIME_UPDATED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }

    public static function createDbTable()
    {
//        dump(static::getEntity()->compileDbTableStructureDump()); //вывод запроса на создание таблицы
        if (!Application::getConnection(MoySkladOrderTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\MoySklad\MoySkladOrderTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\ExternalApi\MoySklad\MoySkladOrderTable')->createDbTable();
            Application::getConnection(MoySkladOrderTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ID_EXCHANGE_STATUS`);');
            Application::getConnection(MoySkladOrderTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ID_MOYSKLAD_ORDER`);');
        }
    }

    public static function dropDbTable()
    {
        if (Application::getConnection(MoySkladOrderTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\MoySklad\MoySkladOrderTable')->getDBTableName()
        )
        ) {
            Application::getConnection(MoySkladOrderTable::getConnectionName())->
            queryExecute('drop table if exists ' . Base::getInstance('\ExternalApi\MoySklad\MoySkladOrderTable')->getDBTableName());
        }
    }


    public static function getItemFor($idOrder)
    {
        return static::getByPrimary($idOrder)->fetch();
    }

    public static function getMoySkladOrderIdFor($idOrder)
    {
        $item = static::getItemFor($idOrder);
        return $item ? $item['ID_MOYSKLAD_ORDER'] : false;
    }

    public static function getListNeededUpdate()
    {
        $q = static::getList(
            [
                'select' => ['*'],
                'filter' => ['=ID_EXCHANGE_STATUS' => static::EXCHANGE_STATUS_NEED_UPDATE],
                'order' => ['ID_ORDER' => 'ASC']
            ]
        );

        $items = [];
        while ($row = $q->fetch()) {
            $items[$row['ID_ORDER']] = $row;
        }
        return $items;
    }

    public static function getListNeededCheckFinishStatus()
    {
        $q = static::getList(
            [
                'select' => ['*'],
                'filter' => ['=ID_EXCHANGE_STATUS' => static::EXCHANGE_STATUS_UPDATED_NEED_CHECK],
                'order' => ['ID_ORDER' => 'ASC']
            ]
        );

        $items = [];
        while ($row = $q->fetch()) {
            $items[$row['ID_ORDER']] = $row;
        }
        return $items;
    }

    public static function updateItem($idOrder, $status = false, $moySkladOrderId = false)
    {
        $res = false;
        if ($idOrder) {
            $fields['TIME_UPDATED'] = new Type\DateTime;
            if ($status !== false)
                $fields['ID_EXCHANGE_STATUS'] = (int)$status;
            if ($moySkladOrderId !== false)
                $fields['ID_MOYSKLAD_ORDER'] = $moySkladOrderId;

            $result = static::update($idOrder, $fields);
            $res = $result->isSuccess();
        }
        return $res;
    }

    public static function addItem($idOrder, $moySkladOrderId = false)
    {
        $result = false;
        if ($idOrder) {
            try {
                $fields = [
                    'ID_ORDER' => $idOrder,
                ];
                if ($moySkladOrderId !== false) {
                    $fields['ID_MOYSKLAD_ORDER'] = $moySkladOrderId;
                }
                $insertResult = static::add($fields);
                $result = $insertResult->isSuccess();
            } catch (\Exception $exception) {
            }
        }
        return $result;
    }

    public static function delItem($idOrder)
    {
        if ($idOrder) {
            static::delete($idOrder);
        }
    }

}