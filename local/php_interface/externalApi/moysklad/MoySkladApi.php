<?php

namespace ExternalApi\MoySklad;

class MoySkladApi
{
    //https://dev.moysklad.ru
    protected const BASE_API_URL = 'https://online.moysklad.ru/api/remap/1.1';
    protected const BASE_API_POSAP_URL = 'https://online.moysklad.ru/api/posap/1.0';
    protected const LOGIN = 'admin@sibkon';
    protected const PASSWORD = 'slc12345678';

    protected const POSITION_LIMIT = 100;//ограничение АПИ в количестве позиций которые нужно делить на нессколько запросов

    protected const ORGANIZATION_ID = 'c77cc90e-4be6-11ea-0a80-0125001cf4c7'; //если не проставлена ORGANIZATION_META то получаем через запрос по ид
    protected const ORGANIZATION_META = [
        'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/organization/c77cc90e-4be6-11ea-0a80-0125001cf4c7',
        'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/organization/metadata',
        'type' => 'organization',
        'mediaType' => 'application/json',
        'uuidHref' => 'https://online.moysklad.ru/app/#mycompany/edit?id=c77cc90e-4be6-11ea-0a80-0125001cf4c7',
    ];

    protected const STORE_ID = 'c77e0245-4be6-11ea-0a80-0125001cf4c9';
    protected const STORE_META = [
        'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/store/c77e0245-4be6-11ea-0a80-0125001cf4c9',
        'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/store/metadata',
        'type' => 'store',
        'mediaType' => 'application/json',
        'uuidHref' => 'https://online.moysklad.ru/app/#warehouse/edit?id=c77e0245-4be6-11ea-0a80-0125001cf4c9',
    ];

    protected const RETAIL_STORE_ID = 'c78ba803-4be6-11ea-0a80-0125001cf4dd';
    protected const RETAIL_STORE_META = [
        "href" => "https://online.moysklad.ru/api/posap/1.0/entity/retailstore/c78ba803-4be6-11ea-0a80-0125001cf4dd",
        "mediaType" => "application/json",
        "type" => "retailstore",
        "id" => "c78ba803-4be6-11ea-0a80-0125001cf4dd",
        "idType" => "native"
    ];
    protected const RETAIL_STORE_META_TOKEN_FILE_PATH = __DIR__ . '/storetoken.txt';

    protected const RETAIL_SHIFT_ID = 'cfc7d226-4e60-11ea-0a80-016e0006096f';
    protected const RETAIL_SHIFT_META = [
        "href" => "https://online.moysklad.ru/api/remap/1.1/entity/retailshift/cfc7d226-4e60-11ea-0a80-016e0006096f",
        "metadataHref" => "https://online.moysklad.ru/api/remap/1.1/entity/retailshift/metadata",
        "type" => "retailshift",
        "mediaType" => "application/json",
        "uuidHref" => "https://online.moysklad.ru/app/#retailshift/edit?id=cfc7d226-4e60-11ea-0a80-016e0006096f"
    ];


    protected const DELIVERY_XML_ID = 'delivery';
    protected const DELIVERY_META = [
        'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/service/ae7dddab-5385-11ea-0a80-02e900006038',
        'metadataHref' => 'https://online.moysklad.ru/api/remap/1.1/entity/service/metadata',
        'type' => 'service',
        'mediaType' => 'application/json',
        'uuidHref' => 'https://online.moysklad.ru/app/#good/edit?id=ae7dd18b-5385-11ea-0a80-02e900006036',
    ];

    protected const AGENT_COMPANY_TYPE = 'individual';//физ лицо

    protected static $lastError = '';
    protected static $lastErrorCode = 0;

    protected static $lastRequestMicroTimeStamp = 0;
    protected const REQUEST_MIN_DELAY = 50000; //минимальная задержка между запросами в мкс


    static protected function request($apiPath, $fields = false, $method = 'GET')
    {
        static::beforeRequest();
        $url = self::BASE_API_URL . $apiPath;
        $headers = array(
            'Authorization: Basic ' . base64_encode(static::LOGIN . ':' . static::PASSWORD),
        );
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if ($fields) {
            $jsonEncodedFields = json_encode($fields);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonEncodedFields);

            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Content-Length: ' . strlen($jsonEncodedFields);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $data = curl_exec($ch);

        $result = static::getResultAfterRequest($data, $url, $fields, $method);
        return $result;
    }


    static protected function requestPosap($apiPath, $fields = false, $method = 'GET', $useToken = false)
    {
        static::beforeRequest();
        $url = self::BASE_API_POSAP_URL . $apiPath;
        if (!$useToken) {
            $headers = array(
                'Authorization: Basic ' . base64_encode(static::LOGIN . ':' . static::PASSWORD),
            );
        } else {
            $headers = array(
                'Lognex-Pos-Auth-Token: ' . static::getNewRetailStoreToken(),
                'Lognex-Pos-Auth-Cashier-Uid: ' . static::LOGIN
            );
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if ($fields) {
            $jsonEncodedFields = json_encode($fields);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonEncodedFields);
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Content-Length: ' . strlen($jsonEncodedFields);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);

        $result = static::getResultAfterRequest($data, $url, $fields, $method);
        return $result;
    }


    static protected function getResultAfterRequest($data, $url, $fields, $method)
    {
        $result = false;
        if ($data) {
            $dataDecoded = json_decode($data, 1);
            if (!$dataDecoded) {
                $errorText = $method . ' ' . $url . '<br><br><br><br>' . print_r($fields, 1) . '<br><br><br><br>' . 'ошибка декодирования ответа ' . $data;
                static::addError($errorText);
                static::addErrorCode('-1');
            } else if (isset($dataDecoded['errors'])) {
                $errorText = $method . ' ' . $url . '<br><br><br><br>' . print_r($fields, 1) . '<br><br><br><br>' . ' ' . $data;
                static::addError($errorText);
                static::addErrorCode($dataDecoded['errors'][0]['code']);
            } else {
                $result = $dataDecoded;
            }
        } else {
            $errorText = 'ошибка получения данных MoySkladApi::request' . print_r([$method, $url, $fields, $method], 1);
            static::addErrorCode('-2');
            static::addError($errorText);
        }

        return $result;
    }

    static public function isNoDataAnswerError()
    {
        return static::getErrorCode() == '-2' ? true : false;
    }


    static protected function beforeRequest()
    {
        static::clearError();
        // ожидаем перед запросами чтобы не превысить лимит
        $mksDiff = (microtime(true) - static::$lastRequestMicroTimeStamp) * 1000000;

        if ($mksDiff < static::REQUEST_MIN_DELAY) {
            usleep(static::REQUEST_MIN_DELAY - $mksDiff);
        }
        static::$lastRequestMicroTimeStamp = microtime(true);
    }

    static public function postOrder($fields)
    {
        $method = 'POST';
        $apiPath = '/entity/customerorder';
        $fields['organization']['meta'] = static::getOrganizationMeta();
        $orderInfo = false;
        if (count($fields['positions']) > static::POSITION_LIMIT) {
            $chunkPositions = array_chunk($fields['positions'], static::POSITION_LIMIT);

            $firstChunkPositions = $chunkPositions[0];
            unset($chunkPositions[0]);
            $fields['positions'] = $firstChunkPositions;

            $orderInfo = static::request($apiPath, $fields, $method);
            if ($orderInfo['id'] && $chunkPositions) {
                foreach ($chunkPositions as $currentChunk) {
                    $chunkMethod = 'POST';
                    $chunkApiPath = '/entity/customerorder/' . $orderInfo['id'] . '/positions/';
                    $chunkAddInfo = static::request($chunkApiPath, $currentChunk, $chunkMethod);
                    if (!$chunkAddInfo) {
                        return false;
                    }
                }
            }
        } else {
            writeAppendLog(array("moySklad" => array("apiPath" => $apiPath)));
            writeAppendLog(array("moySklad" => array("fields" => $fields)));
            writeAppendLog(array("moySklad" => array("method" => $method)));
            $orderInfo = static::request($apiPath, $fields, $method);
        }
        return $orderInfo;
    }

    static public function putOrder($id, $fields)
    {
        $method = 'PUT';
        $apiPath = '/entity/customerorder/' . $id;

        if (count($fields['positions']) > static::POSITION_LIMIT) {
            $chunkPositions = array_chunk($fields['positions'], static::POSITION_LIMIT);

            $firstChunkPositions = $chunkPositions[0];
            unset($chunkPositions[0]);
            $fields['positions'] = $firstChunkPositions;

            $orderInfo = static::request($apiPath, $fields, $method);
            if ($orderInfo['id'] && $chunkPositions) {

                foreach ($chunkPositions as $currentChunk) {
                    $chunkMethod = 'POST';
                    $chunkApiPath = '/entity/customerorder/' . $orderInfo['id'] . '/positions/';
                    $chunkAddInfo = static::request($chunkApiPath, $currentChunk, $chunkMethod);
                    if (!$chunkAddInfo) {
                        return false;
                    }
                }
            }
        } else {
            $orderInfo = static::request($apiPath, $fields, $method);
        }


        return $orderInfo;
    }

    static public function getOrder($orderMoySkladId)
    {
        $apiPath = '/entity/customerorder/' . $orderMoySkladId;
        return static::request($apiPath);
    }

    static public function getStoreMeta()
    {
        if (!empty(static::STORE_META)) {
            return static::STORE_META;
        }

        $meta = false;
        if (static::STORE_ID) {
            $apiPath = '/entity/store/' . static::STORE_ID;
            $data = static::request($apiPath);
            if ($data) {
                $meta = $data['meta'];
            }
        } else {
            $apiPath = '/entity/store';
            $data = static::request($apiPath);
            if ($data["rows"]) {
                $meta = $data["rows"][0]['meta'];
            }
        }
        return $meta;
    }

    static public function getOrganizationMeta()
    {
        if (!empty(static::ORGANIZATION_META)) {
            return static::ORGANIZATION_META;
        }

        $meta = false;
        if (static::ORGANIZATION_ID) {
            $apiPath = '/entity/organization/' . static::ORGANIZATION_ID;
            $data = static::request($apiPath);
            if ($data) {
                $meta = $data['meta'];
            }
        } else {
            $apiPath = '/entity/organization';
            $data = static::request($apiPath);
            if ($data["rows"]) {
                $meta = $data["rows"][0]['meta'];
            }
        }
        return $meta;
    }

    static public function getDeliveryMeta()
    {
        if (!empty(static::DELIVERY_META)) {
            return static::DELIVERY_META;
        }

        $meta = false;
        if (static::DELIVERY_XML_ID) {
            $apiPath = '/entity/service?filter=externalCode=' . urlencode(static::DELIVERY_XML_ID);
            $data = static::request($apiPath);
            if (!empty($data['rows'])) {
                $meta = $data['rows'][0]['meta'];
            }
        }
        return $meta;
    }

    static public function getRetailStoreMeta()
    {
        if (!empty(static::RETAIL_STORE_META)) {
            return static::RETAIL_STORE_META;
        } else
            return false;
    }

    static public function getRetailStoreToken()
    {
        return @file_get_contents(static::RETAIL_STORE_META_TOKEN_FILE_PATH);
    }

    static public function getNewRetailStoreToken()
    {
        return static::attachRetailStoreNewToken();
    }


    static public function getRetailShift()
    {
        $apiPath = '/entity/retailshift/';
        return static::request($apiPath);
    }


    static public function postAgent($fields)
    {
        $method = 'POST';
        $apiPath = '/entity/counterparty';
        $fields['companyType'] = static::AGENT_COMPANY_TYPE;
        return static::request($apiPath, $fields, $method);
    }

    static public function putAgent($id, $fields)
    {
        $method = 'PUT';
        $apiPath = '/entity/counterparty/' . $id;
        return static::request($apiPath, $fields, $method);
    }

    static public function getProductByXmlIds($productXmlId)
    {
        $data = false;
        if ($productXmlId) {
            $apiPath = '/entity/product?filter=externalCode=' . urlencode($productXmlId);
            $data = static::request($apiPath);
        }
        return !empty($data['rows']) ? $data['rows'][0] : false;
    }

    static public function postProduct($fields)
    {
        $method = 'POST';
        $apiPath = '/entity/product';
        return static::request($apiPath, $fields, $method);
    }

    static public function putProduct($id, $fields)
    {
        $method = 'PUT';
        $apiPath = '/entity/product/' . $id;
        return static::request($apiPath, $fields, $method);
    }

    static public function getRetailStore()
    {
        $apiPath = '/admin/retailstore';
        return static::requestPosap($apiPath);
    }

    static public function attachRetailStoreNewToken()
    {
        $method = 'POST';
        $apiPath = '/admin/attach/' . static::RETAIL_STORE_ID;
        $data = static::requestPosap($apiPath, false, $method);
        if (!empty($data['token'])) {
            file_put_contents(static::RETAIL_STORE_META_TOKEN_FILE_PATH, $data['token']);
        }
        return $data['token'];
    }

    static public function postPrepayment($fields)
    {
        $method = 'POST';
        $apiPath = '/entity/prepayment';
        $useToken = true;
        $fields['retailShift']['meta'] = static::RETAIL_SHIFT_META;

        return static::requestPosap($apiPath, $fields, $method, $useToken);
    }

    static public function deletePrepayment($id)
    {
        $method = 'DELETE';
        $apiPath = '/entity/prepayment/' . $id;
        return static::request($apiPath, false, $method);
    }

    static public function getPrepaymentByName($name)
    {
        $apiPath = '/entity/prepayment/?filter=name=' . urlencode($name);
        $data = static::request($apiPath);
        return isset($data['rows'][0]) ? $data['rows'][0] : false;
    }


    static public function getError()
    {
        return static::$lastError;
    }

    static public function getErrorCode()
    {
        return static::$lastErrorCode;
    }

    static protected function clearError()
    {
        static::$lastError = '';
        static::$lastErrorCode = 0;
    }

    static protected function addError($text)
    {
        static::$lastError = $text;
    }

    static protected function addErrorCode($errorCode)
    {
        static::$lastErrorCode = $errorCode;
    }

    static public function isLastRequestError()
    {
        return !empty(static::$lastError);
    }

}