<?php

namespace ExternalApi\MoySklad;

use Bitrix\Sale;
use Helpers\CustomTools;

class MoySklad
{
    protected static $lastErrors = [];

    protected const maxDaysCheckUpdate = 14; //максимум дней для проверки заказа

    static protected function addOrderInMoySklad($fields)
    {
        $data = MoySkladApi::postOrder($fields);
        $res = false;
        if (MoySkladApi::isLastRequestError()) {
            static::addError(MoySkladApi::getError());
        } else {
            $res = $data['id'];
        }
        return $res;
    }

    static protected function updateOrderInMoySklad($moySkladOrderId, $fields)
    {
        MoySkladApi::putOrder($moySkladOrderId, $fields);
        $res = false;
        if (MoySkladApi::isLastRequestError()) {
            static::addError(MoySkladApi::getError());
        } else {
            $res = true;
        }
        return $res;
    }

    static public function setOrderInMoySklad($order)
    {
        $orderProducts = [];
        foreach ($order->getBasket() as $basketItem) {
            $qty = $basketItem->getQuantity();
            if ($qty > 0) {
                $prId = $basketItem->getProductId();
                $orderProducts [$prId] = [
                    'ID' => $prId,
                    'QUANTITY' => $qty,
                    'PRICE' => $basketItem->getPrice(),
                    'NAME' => $basketItem->getField('NAME'),
                    'XML_ID' => $basketItem->getField('PRODUCT_XML_ID')
                ];
            }
        }
        $synchronizedProductsInOrder = static::getSynchronizedProductsInMoySklad($orderProducts);

        static::setUserInMoySklad($order->GetUserID());

        $moySkladOrderId = MoySkladOrderTable::getMoySkladOrderIdFor($order->GetID());

        $orderFields = [
            'name' => static::getMoySkladOrderNameById($order->GetID()),
            'agent' => [
                'meta' => MoySkladAgentTable::getAgentMetaByUserId($order->GetUserID())
            ],
            'organization' => [
                'meta' => MoySkladApi::getOrganizationMeta()
            ],
            'store' => [
                'meta' => MoySkladApi::getStoreMeta()
            ],
            'description' => static::getDescriptionForOrder($order),
            'positions' => []
        ];

        foreach ($synchronizedProductsInOrder as $syncProduct) {
            $orderFields['positions'][] = static::getMappedProductFieldsForOrderApi($syncProduct);
        }
        $orderFields['positions'][] = static::getOrderDeliveryFieldsForOrderApi($order);

        if (!$moySkladOrderId) {
            $moySkladOrderId = static::addOrderInMoySklad($orderFields);
            if ($moySkladOrderId) {
                if (!MoySkladOrderTable::updateItem($order->GetID(), MoySkladOrderTable::EXCHANGE_STATUS_UPDATED_NEED_CHECK, $moySkladOrderId)) {
                    static::addError('ошибка addOrderInMoySklad ' . print_r([$order->GetID(), $moySkladOrderId], 1));
                }
            }
        } else {
            if (static::updateOrderInMoySklad($moySkladOrderId, $orderFields)) {
                if (!MoySkladOrderTable::updateItem($order->GetID(), MoySkladOrderTable::EXCHANGE_STATUS_UPDATED_NEED_CHECK)) {
                    static::addError('ошибка updateOrderInMoySklad ' . print_r([$order->GetID(), $moySkladOrderId], 1));
                }
            }
        }

        static::deletePrepaymentsForOrderId($order->GetID());
        static::postPaymentForOrder($order, $orderFields);
    }

    static protected function postPaymentForOrder($order, $fields)
    {
        if (static::orderIsPaid($order)) {
            $moySkladOrderId = MoySkladOrderTable::getMoySkladOrderIdFor($order->GetID());
            if ($moySkladOrderId) {

                $orderMoySkladInfo = MoySkladApi::getOrder($moySkladOrderId);
                if ($orderMoySkladInfo) {
                    $fields['name'] = static::getMoySkladPrepaymentNameForOrderId($order->GetID());
                    $fields['customerOrder']['meta'] = $orderMoySkladInfo['meta'];
                    $fields['meta'] = [
                        "href" => "https://online.moysklad.ru/api/posap/1.0/entity/prepayment/syncid/" . static::gen_uuid()
                    ];
                    $fields["noCashSum"] = (round($order->getSumPaid() * 100));
                    MoySkladApi::postPrepayment($fields);

                    if (MoySkladApi::isLastRequestError() and !MoySkladApi::isNoDataAnswerError()) {
                        static::addError(MoySkladApi::getError());
                    }
                }
            }
        }
    }

    static protected function getMoySkladOrderNameById($orderId)
    {
        return '#' . $orderId . ' ' . SITE_SERVER_NAME;
    }

    static protected function getMoySkladPrepaymentNameForOrderId($orderId)
    {
        return 'prepayment ' . static::getMoySkladOrderNameById($orderId);
    }

    static protected function deletePrepaymentsForOrderId($orderId)
    {
        $prepaymentName = static::getMoySkladPrepaymentNameForOrderId($orderId);
        $prepaymentData = MoySkladApi::getPrepaymentByName($prepaymentName);
        if ($prepaymentData) {
            $prepaymentId = $prepaymentData["id"];
            MoySkladApi::deletePrepayment($prepaymentId);
        }
    }

    static protected function getDescriptionForOrder($order)
    {
        $descr = '';
        $orderId = $order->GetID();
        if ($orderId) {
            $obProps = \Bitrix\Sale\Internals\OrderPropsValueTable::getList(array('filter' => array('ORDER_ID' => $orderId)));
            while ($prop = $obProps->Fetch()) {
                if (trim($prop['VALUE'])) {
                    $descr .= $prop['NAME'] . ': ' . $prop['VALUE'] . "\r\n";
                }
            }
            $paymentCollection = $order->getPaymentCollection();
            if ($paymentCollection) {
                $descr .= "\r\nСпособ оплаты: \r\n";
                foreach ($paymentCollection as $payment) {
                    $sum = $payment->getSum();
                    $isPaid = $payment->isPaid();
                    $psName = $payment->getPaymentSystemName();
                    $isPaidInfo = $isPaid ? 'оплачено' : 'НЕ оплачено';
                    $descr .= $psName . ': ' . $sum . ' ' . $isPaidInfo . "\r\n";
                }
            }

        }
        return $descr;
    }

    static protected function orderIsPaid($order)
    {
        $paymentCollection = $order->getPaymentCollection();
        if ($paymentCollection) {
            foreach ($paymentCollection as $payment) {
                if ($payment->isPaid())
                    return true;
            }
        }
        return false;
    }

    static protected function getSynchronizedProductsInMoySklad($products)
    {
        static::fillProductsForApi($products);

        foreach ($products as $key => $product) {
            if (empty($product['ID_MOYSKLAD_PRODUCT']) or empty($product['META'])) {
                $productInApi = static::setProductInApi($product);
                if ($productInApi) {
                    $products[$key]['ID_MOYSKLAD_PRODUCT'] = $productInApi['id'];
                    $products[$key]['META'] = $productInApi['meta'];
                }
            } else {
                $fieldsForApi = static::getMappedProductFieldsForApi($product);
                $moySkladProductId = $product['ID_MOYSKLAD_PRODUCT'];
                MoySkladApi::putProduct($moySkladProductId, $fieldsForApi);
                if (MoySkladApi::isLastRequestError()) {
                    static::addError(MoySkladApi::getError());
                    MoySkladProductTable::delItem($product['ID']);

                    $productInApi = static::setProductInApi($product);
                    if ($productInApi) {
                        $products[$key]['ID_MOYSKLAD_PRODUCT'] = $productInApi['id'];
                        $products[$key]['META'] = $productInApi['meta'];
                    }
                }
            }
        }
        return $products;
    }

    static protected function setProductInApi($product)
    {
        $productInApi = MoySkladApi::getProductByXmlIds($product['XML_ID']);
        if ($productInApi) {
            MoySkladProductTable::addItem($product['ID'], $productInApi['id'], $productInApi['meta']);
        } else {
            $fieldsForApi = static::getMappedProductFieldsForApi($product);
            $productInApi = MoySkladApi::postProduct($fieldsForApi);
            if (MoySkladApi::isLastRequestError()) {
                static::addError(MoySkladApi::getError());
            } else {
                MoySkladProductTable::addItem($product['ID'], $productInApi['id'], $productInApi['meta']);
            }
        }
        return $productInApi;
    }

    static protected function fillProductsForApi(&$products)
    {
        if ($products) {
            $productIds = array_keys($products);
            $vatQ = \Bitrix\Catalog\VatTable::getList([
                'select' => ['ID', 'RATE']
            ]);

            $vatsInfo = [];
            while ($row = $vatQ->fetch()) {
                $vatsInfo[$row['ID']] = $row;
            }
            $productsQ = \Bitrix\Catalog\ProductTable::getList(
                [
                    'select' => ['ID', 'VAT_ID'],
                    'filter' => ['=ID' => $productIds]
                ]
            );
            while ($row = $productsQ->fetch()) {
                $products[$row['ID']]['VAT_RATE'] = $vatsInfo[$row['VAT_ID']]['RATE'];
            }

            $moySkladProductsInfo = MoySkladProductTable::getListFor($productIds);
            if ($moySkladProductsInfo) {
                foreach ($moySkladProductsInfo as $moySkladProductInfo) {
                    $products[$moySkladProductInfo['ID_PRODUCT']]['ID_MOYSKLAD_PRODUCT'] = $moySkladProductInfo['ID_MOYSKLAD_PRODUCT'];
                    $products[$moySkladProductInfo['ID_PRODUCT']]['META'] = $moySkladProductInfo['META'];
                }
            }
        }
    }

    static protected function getMappedProductFieldsForApi($product)
    {
        $fields = [
            'name' => $product['NAME'],
            'code' => $product['XML_ID'],
            'externalCode' => $product['XML_ID'],
        ];

        if (isset($product['VAT_RATE'])) {
            $fields['vat'] = (int)$product['VAT_RATE'];
            $fields['effectiveVat'] = (int)$product['VAT_RATE'];
        }
        return $fields;
    }

    static protected function getMappedProductFieldsForOrderApi($syncProduct)
    {
        $fields = [
            'quantity' => (float)(round($syncProduct['QUANTITY'], 4)),
            'price' => (int)(round($syncProduct['PRICE'] * 100)),
            'assortment' => ['meta' => $syncProduct['META']]
        ];

        if (isset($syncProduct['VAT_RATE'])) {
            $fields['vat'] = (int)$syncProduct['VAT_RATE'];
        }
        return $fields;
    }

    static protected function getOrderDeliveryFieldsForOrderApi($order)
    {
        $deliveryVatRate = 20;
        $fields = [
            'quantity' => 1,
            'price' => (int)(round($order->getDeliveryPrice() * 100)),
            'assortment' => ['meta' => MoySkladApi::getDeliveryMeta()],
            'vat' => $deliveryVatRate
        ];

        return $fields;
    }


    static public function updateOrdersNeededUpdate()
    {
        static::errorsClear();

        $moySkladOrderItems = MoySkladOrderTable::getListNeededUpdate();

        if ($moySkladOrderItems) {
            $orderIds = array_keys($moySkladOrderItems);

            $parameters = [
                'filter' => ['ID' => $orderIds]
            ];
            $orders = Sale\Order::loadByFilter($parameters);

            if ($orders) {
                foreach ($orders as $order) {
                    static::setOrderInMoySklad($order);
                }
            } else {
                foreach ($orderIds as $orderId) {
                    MoySkladOrderTable::delItem($orderId);
                }
            }
        } else {
            echo 'Нет заказов нуждающихся в обновлении';
        }

        static::errorsReport();
    }

    static public function checkAndCarryFinishedOrdersInApi()
    {
        static::errorsClear();

        $moySkladOrderItems = MoySkladOrderTable::getListNeededCheckFinishStatus();

        if ($moySkladOrderItems) {
            foreach ($moySkladOrderItems as $moySkladOrderItem) {
                $orderMoySkladId = $moySkladOrderItem['ID_MOYSKLAD_ORDER'];
                $orderId = $moySkladOrderItem['ID_ORDER'];
                $daysAfterUpdateGone = (time() - $moySkladOrderItem['TIME_UPDATED']->getTimestamp()) / 3600 / 24;
                if ($daysAfterUpdateGone > self::maxDaysCheckUpdate) {
                    MoySkladOrderTable::updateItem($orderId, MoySkladOrderTable::EXCHANGE_STATUS_ERROR_CHECK_TIME_UPDATED);
                } else {
                    $order = Sale\Order::load($orderId);
                    if (!$order) {
                        MoySkladOrderTable::delItem($orderId);
                    } else {
                        $orderStatusId = $order->getField('STATUS_ID');
                        if ($orderStatusId == 'F' or $orderStatusId == 'O') {
                            MoySkladOrderTable::updateItem($orderId, MoySkladOrderTable::EXCHANGE_STATUS_ORDER_COMPLETED);
                        } else {
                            $orderApiInfo = MoySkladApi::getOrder($orderMoySkladId);
                            if (MoySkladApi::isLastRequestError()) {
                                $errorCode = MoySkladApi::getErrorCode();
                                if ($errorCode == 1021) {//не найден в апи/удален
                                    MoySkladOrderTable::delItem($orderId);
                                } else {
                                    static::addError(MoySkladApi::getError());
                                }
                            } else {
                                $orderFinished = false;
                                if (!empty($orderApiInfo['deleted'])) {
                                    MoySkladOrderTable::delItem($orderId);
                                }
                                if (!empty($orderApiInfo['demands'])) {
                                    foreach ($orderApiInfo['demands'] as $demandDocument) {
                                        if ($demandDocument['meta']['type'] == 'retaildemand') { // у заказа проведена продажа
                                            $orderFinished = true;
                                        }
                                    }
                                }

                                if ($orderFinished) {
                                    MoySkladOrderTable::updateItem($orderId, MoySkladOrderTable::EXCHANGE_STATUS_ORDER_COMPLETED);
                                    if ($order) {
                                        $r = $order->setField('STATUS_ID', 'F');
                                        if (!$r->isSuccess()) {
                                            static::addError($r->getErrorMessages());
                                        } else {
                                            $r = $order->save();
                                            if (!$r->isSuccess()) {
                                                static::addError($r->getErrorMessages());
                                            }
                                        }
                                    } else {
                                        MoySkladOrderTable::delItem($orderId);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            echo 'Нет заказов нуждающихся в проверке статуса';
        }

        static::errorsReport();
    }

    static protected function setUserInMoySklad($idUser)
    {
        $currentAgent = MoySkladAgentTable::getAgentByUserId($idUser);
        $userInfo = \CUser::GetByID($idUser)->Fetch();
        if ($userInfo) {
            $fields = [
                'name' => '#' . $userInfo['ID'] . ' ' . $userInfo['LAST_NAME'] . ' ' . $userInfo['NAME']
            ];
            if (!empty($userInfo['PERSONAL_PHONE'])) {
                $fields['phone'] = $userInfo['PERSONAL_PHONE'];
            }
            if (!empty($userInfo['EMAIL'])) {
                $fields['email'] = $userInfo['EMAIL'];
            }
            if (!$currentAgent) {
                static::addUserInMoySklad($idUser, $fields);
            } else {
                $moySkladAgentId = $currentAgent['ID_MOYSKLAD_AGENT'];
                static::updateUserInMoySklad($moySkladAgentId, $fields);
            }
        } else {
            static::addError('ошибка setUserInMoySklad' . print_r($userInfo, 1));
        }
    }

    static protected function addUserInMoySklad($idUser, $fields)
    {
        $res = false;
        $info = MoySkladApi::postAgent($fields);
        if (MoySkladApi::isLastRequestError()) {
            static::addError(MoySkladApi::getError());
        } else {
            $res = MoySkladAgentTable::addAgent($idUser, $info['id'], $info['meta']);
        }
        return $res;
    }

    static protected function updateUserInMoySklad($moySkladAgentId, $fields)
    {
        $res = false;
        MoySkladApi::putAgent($moySkladAgentId, $fields);
        if (MoySkladApi::isLastRequestError()) {
            static::addError(MoySkladApi::getError());
        } else {
            $res = true;
        }
        return $res;
    }

    static protected function addError($text)
    {
        static::$lastErrors[] = $text;
    }

    static protected function errorsClear()
    {
        static::$lastErrors = [];
    }

    static protected function getLastErrors()
    {
        return static::$lastErrors;
    }


    static protected function errorsReport()
    {
        if ($errors = static::getLastErrors()) {
            $text = '';
            $text .= implode(' <br>', $errors);
            $sendParams = [
                'THEME' => 'Ошибки ExternalApi\MoySklad',
                'BODY_TEXT' => $text,
            ];

            CustomTools::sendErrorsReport($sendParams, 138);
        }
    }

    static protected function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

}