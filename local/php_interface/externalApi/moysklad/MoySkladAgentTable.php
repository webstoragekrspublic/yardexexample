<?php

namespace ExternalApi\MoySklad;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;
use Helpers\GlobalStorage;

class MoySkladAgentTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'custom_moysklad_agent';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID_USER', array(
                'primary' => true,
                'required' => true,
            )),
            new Entity\StringField('ID_MOYSKLAD_AGENT', array(
                'required' => true,
            )),
            new Entity\TextField('META', array(
                'serialized' => true
            )),
        );
    }

    public static function createDbTable()
    {
        //        dump(static::getEntity()->compileDbTableStructureDump()); //вывод запроса на создание таблицы
        if (!Application::getConnection(MoySkladAgentTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\MoySklad\MoySkladAgentTable')->getDBTableName()
        )) {
            Base::getInstance('\ExternalApi\MoySklad\MoySkladAgentTable')->createDbTable();
            Application::getConnection(MoySkladAgentTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ID_MOYSKLAD_AGENT`);');
        }
    }

    public static function dropDbTable()
    {
        if (Application::getConnection(MoySkladAgentTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\MoySklad\MoySkladAgentTable')->getDBTableName()
        )) {
            Application::getConnection(MoySkladAgentTable::getConnectionName())->queryExecute('drop table if exists ' . Base::getInstance('\ExternalApi\MoySklad\MoySkladAgentTable')->getDBTableName());
        }
    }

    public static function addAgent($idUser, $idMoySkladAgent, $meta)
    {
        $result = false;
        if ($idUser and $idMoySkladAgent and $meta) {
            try {
                $insertResult = static::add([
                    'ID_USER' => $idUser,
                    'ID_MOYSKLAD_AGENT' => $idMoySkladAgent,
                    'META' => $meta
                ]);
                $result = $insertResult->isSuccess();
            } catch (\Exception $exception) {;
            }
        }
        return $result;
    }

    public static function getAgentByUserId($idUser)
    {
        $cacheKey = 'MoySkladAgentTable::getAgentByUserId-' . $idUser;
        $agentInfo = GlobalStorage::get($cacheKey);
        if (!isset($agentInfo)) {
            if ($idUser) {
                $info = static::getByPrimary($idUser)->fetch();
                if ($info) {
                    $agentInfo = $info;
                }
            }
            GlobalStorage::set($cacheKey, $agentInfo);
        }
        return $agentInfo;
    }

    public static function getAgentMetaByUserId($idUser)
    {
        $meta = null;
        if ($agentInfo = static::getAgentByUserId($idUser)) {
            $meta = $agentInfo['META'];
        }
        return $meta;
    }
}
