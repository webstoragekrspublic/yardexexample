<?php

namespace ExternalApi\MoySklad;

use Bitrix\Sale;

class MoySkladEvents
{
    function onOrderChange ($ID, &$arFields){
        $orderItem = MoySkladOrderTable::getItemFor($ID);
        if ($orderItem){
            if ($orderItem['ID_EXCHANGE_STATUS'] != MoySkladOrderTable::EXCHANGE_STATUS_ORDER_COMPLETED){
                MoySkladOrderTable::updateItem($ID,MoySkladOrderTable::EXCHANGE_STATUS_NEED_UPDATE);
            }
        } else {
            MoySkladOrderTable::addItem($ID);
        }
    }
}