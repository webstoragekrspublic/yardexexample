<?php

namespace ExternalApi\MoySklad;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;
use Helpers\GlobalStorage;

class MoySkladProductTable extends Entity\DataManager
{
    public static function getTableName() {
        return 'custom_moysklad_product';
    }

    public static function getMap() {
        return array(
            new Entity\IntegerField('ID_PRODUCT', array(
                'primary' => true,
                'required' => true,
            )),
            new Entity\StringField('ID_MOYSKLAD_PRODUCT', array(
                'required' => true,
            )),
            new Entity\TextField('META', array(
                'serialized' => true
            )),
        );
    }

    public static function createDbTable() {
//        dump(static::getEntity()->compileDbTableStructureDump()); //вывод запроса на создание таблицы
        if (!Application::getConnection(MoySkladProductTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\MoySklad\MoySkladProductTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\ExternalApi\MoySklad\MoySkladProductTable')->createDbTable();
            Application::getConnection(MoySkladProductTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ID_MOYSKLAD_PRODUCT`);');
        }
    }

    public static function dropDbTable() {
        if (Application::getConnection(MoySkladProductTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\MoySklad\MoySkladProductTable')->getDBTableName()
        )
        ) {
            Application::getConnection(MoySkladProductTable::getConnectionName())->
            queryExecute('drop table if exists ' . Base::getInstance('\ExternalApi\MoySklad\MoySkladProductTable')->getDBTableName());
        }
    }

    public static function addItem($idProduct, $idMoySkladProduct, $meta) {
        $result = false;
        if ($idProduct and $idMoySkladProduct and $meta) {
            try {
                $insertResult = static::add([
                    'ID_PRODUCT' => $idProduct,
                    'ID_MOYSKLAD_PRODUCT' => $idMoySkladProduct,
                    'META' => $meta
                ]);
                $result = $insertResult->isSuccess();
            } catch (\Exception $exception) {
                ;
            }
        }
        return $result;
    }

    public static function delItem($idProduct){
        if ($idProduct){
            static::delete($idProduct);
        }
    }

    public static function getListFor($productsId){
        $items = [];
        if ($productsId){
            $q = static::getList(
                [
                    'select' => ['*'],
                    'filter' => ['=ID_PRODUCT' => $productsId],
                ]
            );
            while ($row = $q->fetch()) {
                $items[$row['ID_PRODUCT']] = $row;
            }
        }
        return $items;
    }

}