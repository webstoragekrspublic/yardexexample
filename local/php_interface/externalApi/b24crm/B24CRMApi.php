<?php

namespace ExternalApi\B24CRM;

class B24CRMApi
{
    static protected $_base_url = "https://crm.sibkon.ru/rest/58/50fj3ohqsj3c6hsl/";

    static public function request($_method, $params = [])
    {
        if (!$_method)
            return;

        $post_params = http_build_query($params);

        $url = self::$_base_url . $_method . '/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        $result = json_decode($data, 1);

        if (isset($result['error'])) {
            throw new \Exception(print_r($result, 1));
        }

        return $result;
    }

    static protected function findContactIdByFields($email, $phone)
    {
        $phoneOnlyDigits = preg_replace('/[^\d]/', '', $phone);
        $contactId = false;
        if ($phoneOnlyDigits[0] == 7) {
            $phoneOnlyDigits[0] = 8;
        }

        //ищем по номеру телефона
        if (!$contactId && $phoneOnlyDigits) {
            $contactsFromApi = self::duplicateFindByComm('PHONE', [$phoneOnlyDigits], 'CONTACT');

            if (!empty($contactsFromApi['CONTACT'])) {
                $contactId = $contactsFromApi['CONTACT'][0];
            }
        }

        //ищем по почте
        if (!$contactId && $email) {
            $contactsFromApi = self::duplicateFindByComm('EMAIL', [trim($email)], 'CONTACT');

            if (!empty($contactsFromApi['CONTACT'])) {
                $contactId = $contactsFromApi['CONTACT'][0];
            }
        }

        return $contactId;
    }

    static public function findUsersByFilters($filterFields)
    {
        $res = false;
        if ($filterFields && is_array($filterFields)) {
            $method = 'crm.contact.list';
            $params = [
                'filter' => $filterFields,
                'select' => [
                    "ID", "PHONE", "EMAIL"
                ]
            ];
            $data = self::request($method, $params);
            if (isset($data['total']) && $data['total'] > 0) {
                $res = $data['result'];
            }
        }
        return $res;
    }

    //https://dev.1c-bitrix.ru/rest_help/crm/auxiliary/duplicates/crm_duplicate_findbycomm.php
    //ищем уже созданные сущности по телефону/почте
    static public function duplicateFindByComm($type, $values, $entityType = false)
    {
        $res = false;
        $method = 'crm.duplicate.findbycomm';
        if (!is_array($values)) {
            $values = [$values];
        }
        $params = [
            'entity_type' => $entityType,
            'type' => $type,
            'values' => $values,
        ];
        $data = self::request($method, $params);
        if (!empty($data['result'])) {
            $res = $data['result'];
        }
        return $res;
    }


    static public function addContact($fields)
    {
        $method = 'crm.contact.add';
        $params = [
            'fields' => $fields
        ];
        $data = self::request($method, $params);

        return $data;
    }

    static public function updateContact($contactId, $fields)
    {
        $method = 'crm.contact.update';
        $params = [
            'id' => $contactId,
            'fields' => $fields
        ];
        $data = self::request($method, $params);

        return $data;
    }

    static public function addOrder($fields){
        $method = 'sale.order.add';
        $params = [
            'fields' => $fields
        ];
        $data = self::request($method, $params);

        return $data;
    }

    static public function updateOrder($idCrmOrder, $fields){
        $method = 'sale.order.update';
        $params = [
            'id' => $idCrmOrder,
            'fields' => $fields
        ];
        $data = self::request($method, $params);

        return $data;
    }

    static public function addProduct($fields){
        $method = 'crm.product.add';
        $params = [
            'fields' => $fields
        ];
        $data = self::request($method, $params);

        return $data;
    }


    static public function addBasketItem($fields){
        $method = 'sale.basketitem.add';
        $params = [
            'fields' => $fields
        ];
        $data = self::request($method, $params);

        return $data;
    }

    static public function addContactToOrder($fields){
        $method = 'sale.order.contact.set';
        $params = [
            'fields' => $fields
        ];
        $data = self::request($method, $params);

        return $data;
    }

    static protected function contactList($filterFields, $select = ["ID", "PHONE", "EMAIL"])
    {
        $res = [];
        if ($filterFields && is_array($filterFields)) {
            $method = 'crm.contact.list';
            $params = [
                'filter' => $filterFields,
                'select' => $select
            ];
            $data = self::request($method, $params);
            if (isset($data['total']) && $data['total'] > 0) {
                $res = $data['result'];
            }
        }
        return $res;
    }

    static public function getCrmStatusList($entityId = 'SOURCE')
    {
        $method = 'crm.status.list';
        $params = [
            'filter' =>
                [
                    "ENTITY_ID" => $entityId
                ]
        ];
        $data = self::request($method, $params);

        return $data;
    }

    static public function getProductsByFilter($filter, $select = ['*'])
    {
        $res = false;
        $method = 'crm.product.list';
        $params = [
            'order' => ['SORT' => "ASC"],
            'filter' => $filter,
            'select' => $select
        ];

        $data = self::request($method, $params);

        if (!empty($data['result'])) {
            $res = $data['result'];
        }

        return $res;
    }
}

