<?php

namespace ExternalApi\B24CRM;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class B24CRMContactTable extends Entity\DataManager
{
    public const EXCHANGE_STATUS_NEED_UPDATE = 0;
    public const EXCHANGE_STATUS_COMPLETED = 2;
    public const EXCHANGE_STATUS_ERROR = -1;

    public static function getTableName()
    {
        return 'custom_b24crm_contact';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID_USER', array(
                'primary' => true,
                'required' => true,
            )),
            new Entity\IntegerField('ID_CRM_CUSTOMER', array(
                'default_value' => 0
            )),

            new Entity\IntegerField('EXCHANGE_STATUS', array(
                'required' => true,
                'default_value' => static::EXCHANGE_STATUS_NEED_UPDATE,
            )),

            new Entity\DatetimeField('TIME_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
            new Entity\DatetimeField('TIME_UPDATED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }

    public static function updateItem($idUser, $status = false, $crmContactId = false)
    {
        $res = false;
        if ($idUser) {
            $fields['TIME_UPDATED'] = new Type\DateTime;
            if ($status !== false)
                $fields['EXCHANGE_STATUS'] = (int)$status;
            if ($crmContactId !== false)
                $fields['ID_CRM_CUSTOMER'] = $crmContactId;

            $result = static::update($idUser, $fields);
            $res = $result->isSuccess();
        }
        return $res;
    }

    public static function addItem($idUser, $crmContactId = false)
    {
        $result = false;
        if ($idUser) {
            try {
                $fields = [
                    'ID_USER' => $idUser,
                ];
                if ($crmContactId !== false) {
                    $fields['ID_CRM_CUSTOMER'] = $crmContactId;
                }
                $insertResult = static::add($fields);
                $result = $insertResult->isSuccess();
//                foreach ($insertResult->getErrors() as $error)
//                {
//                    echo $error->getCode() . ': ';
//                    echo $error->getMessage();
//                    echo "\n";
//                }
            } catch (\Exception $exception) {
//                var_dump($exception);
            }
        }
        return $result;
    }

    public static function delItem($idUser)
    {
        if ($idUser) {
            static::delete($idUser);
        }
    }

    public static function getIdCrmCustomerByIdUser($idUser)
    {
        $res = false;
        if ($idUser) {
            $contactInfo = self::getByPrimary($idUser)->fetch();
            if ($contactInfo !== false) {
                $res = $contactInfo['ID_CRM_CUSTOMER'];
            }
        }
        return $res;
    }

    public static function getListNeededUpdate()
    {
        $q = static::getList(
            [
                'select' => ['*'],
                'filter' => ['=EXCHANGE_STATUS' => static::EXCHANGE_STATUS_NEED_UPDATE],
                'order' => ['ID_USER' => 'ASC']
            ]
        );

        $items = [];
        while ($row = $q->fetch()) {
            $items[] = $row;
        }
        return $items;
    }

    public static function createDbTable()
    {
//        dump(B24CRMContactTable::getEntity()->compileDbTableStructureDump()); //вывод запроса на создание таблицы
        if (!Application::getConnection(B24CRMContactTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\B24CRM\B24CRMContactTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\ExternalApi\B24CRM\B24CRMContactTable')->createDbTable();
            Application::getConnection(B24CRMContactTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`EXCHANGE_STATUS`);');
        }
    }

    public static function dropDbTable()
    {
        if (Application::getConnection(B24CRMContactTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\B24CRM\B24CRMContactTable')->getDBTableName()
        )
        ) {
            Application::getConnection(B24CRMContactTable::getConnectionName())->
            queryExecute('drop table if exists ' . Base::getInstance('\ExternalApi\B24CRM\B24CRMContactTable')->getDBTableName());
        }
    }
}