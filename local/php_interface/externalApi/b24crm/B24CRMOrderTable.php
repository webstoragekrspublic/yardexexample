<?php

namespace ExternalApi\B24CRM;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class B24CRMOrderTable extends Entity\DataManager
{
    public const EXCHANGE_STATUS_NEED_UPDATE = 0;
    public const EXCHANGE_STATUS_COMPLETED = 2;
    public const EXCHANGE_STATUS_ERROR = -1;

    public static function getTableName()
    {
        return 'custom_b24crm_order';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID_ORDER', array(
                'primary' => true,
                'required' => true,
            )),
            new Entity\IntegerField('ID_CRM_ORDER', array(
                'default_value' => 0
            )),

            new Entity\IntegerField('EXCHANGE_STATUS', array(
                'required' => true,
                'default_value' => static::EXCHANGE_STATUS_NEED_UPDATE,
            )),

            new Entity\DatetimeField('TIME_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
            new Entity\DatetimeField('TIME_UPDATED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }

    public static function updateItem($idUser, $status = false, $crmOrderId = false)
    {
        $res = false;
        if ($idUser) {
            $fields['TIME_UPDATED'] = new Type\DateTime;
            if ($status !== false)
                $fields['EXCHANGE_STATUS'] = (int)$status;
            if ($crmOrderId !== false)
                $fields['ID_CRM_ORDER'] = $crmOrderId;

            $result = static::update($idUser, $fields);
            $res = $result->isSuccess();
        }
        return $res;
    }

    public static function addItem($idUser, $crmOrderId = false)
    {
        $result = false;
        if ($idUser) {
            try {
                $fields = [
                    'ID_ORDER' => $idUser,
                ];
                if ($crmOrderId !== false) {
                    $fields['ID_CRM_ORDER'] = $crmOrderId;
                }
                $insertResult = static::add($fields);
                $result = $insertResult->isSuccess();
//                foreach ($insertResult->getErrors() as $error)
//                {
//                    echo $error->getCode() . ': ';
//                    echo $error->getMessage();
//                    echo "\n";
//                }
            } catch (\Exception $exception) {
//                var_dump($exception);
            }
        }
        return $result;
    }

    public static function delItem($idUser)
    {
        if ($idUser) {
            static::delete($idUser);
        }
    }

    public static function getListNeededUpdate()
    {
        $q = static::getList(
            [
                'select' => ['*'],
                'filter' => ['=EXCHANGE_STATUS' => static::EXCHANGE_STATUS_NEED_UPDATE],
                'order' => ['ID_ORDER' => 'ASC']
            ]
        );

        $items = [];
        while ($row = $q->fetch()) {
            $items[] = $row;
        }
        return $items;
    }

    public static function createDbTable()
    {
//        dump(B24CRMOrderTable::getEntity()->compileDbTableStructureDump()); //вывод запроса на создание таблицы
        if (!Application::getConnection(B24CRMOrderTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\B24CRM\B24CRMOrderTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\ExternalApi\B24CRM\B24CRMOrderTable')->createDbTable();
            Application::getConnection(B24CRMOrderTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`EXCHANGE_STATUS`);');
        }
    }

    public static function dropDbTable()
    {
        if (Application::getConnection(B24CRMOrderTable::getConnectionName())->isTableExists(
            Base::getInstance('\ExternalApi\B24CRM\B24CRMOrderTable')->getDBTableName()
        )
        ) {
            Application::getConnection(B24CRMOrderTable::getConnectionName())->
            queryExecute('drop table if exists ' . Base::getInstance('\ExternalApi\B24CRM\B24CRMOrderTable')->getDBTableName());
        }
    }
}