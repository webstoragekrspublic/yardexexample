<?php

namespace ExternalApi\B24CRM;

use Helpers\CustomTools;

class B24CRM
{
    static protected $sourceStatusId = '14';//Yardex
    static protected $errors = [];

    static protected $b24ResponsibleId = 58;//;'53';//ответсвенный за заказы
    static protected $b24BuyerId = '58';//покупатель (сейчас бот, todo создавать пользователей а не клиентов, к ним привязывать заказы)

    protected const b24OrderSiteId = 's1';

    protected const otherPropertiesFieldCrmCode = 'UF_CRM_1615291659012';

    //свойства которые не заносим в поле Прочая информация
    protected const banOrderProps = [
        'DADATA_GIS' => 1
    ];


    public static function putUserInUpdateQueue($idUser)
    {
        self::errorsClear();
        $contactInfo = B24CRMContactTable::getByPrimary($idUser)->fetch();
        if (!$contactInfo) {
            $res = B24CRMContactTable::addItem($idUser);
            if (!$res) {
                self::errorAdd('ошибка добавления пользователя в очередь обновлений #' . $idUser);
            }
        } else {
            $res = B24CRMContactTable::updateItem($idUser, B24CRMContactTable::EXCHANGE_STATUS_NEED_UPDATE);
            if (!$res) {
                self::errorAdd('ошибка добавления пользователя в очередь обновлений #' . $idUser);
            }
        }
        self::errorsReport();
        return $res;
    }


    public static function putOrderInUpdateQueue($idOrder)
    {
        self::errorsClear();
        $orderInfo = B24CRMOrderTable::getByPrimary($idOrder)->fetch();
        if (!$orderInfo) {
            $res = B24CRMOrderTable::addItem($idOrder);
            if (!$res) {
                self::errorAdd('ошибка добавления заказа в очередь обновлений #' . $idOrder);
            }
        } else {
            $res = B24CRMOrderTable::updateItem($idOrder, B24CRMOrderTable::EXCHANGE_STATUS_NEED_UPDATE);
            if (!$res) {
                self::errorAdd('ошибка добавления заказа в очередь обновлений #' . $idOrder);
            }
        }
        self::errorsReport();
        return $res;
    }


    protected static function setContactStatusUpdate($idUser, $statusId)
    {
        return self::updateContactInfo($idUser, $statusId);
    }

    protected static function setOrderStatusUpdate($idOrder, $statusId)
    {
        return self::updateOrderInfo($idOrder, $statusId);
    }

    protected static function updateContactInfo($idUser, $statusId = false, $crmContactId = false)
    {
        return B24CRMContactTable::updateItem($idUser, $statusId, $crmContactId);
    }

    protected static function updateOrderInfo($idOrder, $statusId = false, $crmOrderId = false)
    {
        return B24CRMOrderTable::updateItem($idOrder, $statusId, $crmOrderId);
    }

    public static function updateUsersInCrm()
    {
        self::errorsClear();
        $usersForUpdate = B24CRMContactTable::getListNeededUpdate();
        if ($usersForUpdate) {
            foreach ($usersForUpdate as $userForUpdate) {
                $idUser = $userForUpdate['ID_USER'];
                $idCrmCustomer = $userForUpdate['ID_CRM_CUSTOMER'];
                $userFields = self::userFieldsForUpdate($idUser);

                if ($userFields) {
                    if ($crmContactId = self::pushUserToCrm($userFields, $idCrmCustomer)) {
                        echo $idUser . ' - обновлен <br>';
                        self::updateContactInfo($idUser, B24CRMContactTable::EXCHANGE_STATUS_COMPLETED, $crmContactId);
                    } else {
                        echo $idUser . ' - Ошибка! <br>';
                        self::setContactStatusUpdate($idUser, B24CRMContactTable::EXCHANGE_STATUS_ERROR);
                    }
                } else {
                    echo $idUser . ' - Ошибка! <br>';
                    self::setContactStatusUpdate($idUser, B24CRMContactTable::EXCHANGE_STATUS_ERROR);
                }
            }
        } else {
            echo 'Нет пользователей для обновления';
        }
        self::errorsReport();
    }


    public static function updateOrdersInCrm()
    {
        global $DB;
        $DB->Query("SET wait_timeout=200");
        self::errorsClear();
        $ordersForUpdate = B24CRMOrderTable::getListNeededUpdate();
        if ($ordersForUpdate) {
            foreach ($ordersForUpdate as $orderForUpdate) {
                $idOrder = $orderForUpdate['ID_ORDER'];
                $idCrmOrder = $orderForUpdate['ID_CRM_ORDER'];
                $orderFields = self::orderFieldsForUpdate($idOrder);
                $order = \Bitrix\Sale\Order::load($idOrder);

                if ($orderFields) {
                    if ($crmOrderId = self::pushOrderToCrm($orderFields, $idCrmOrder)) {
                        echo $idOrder . ' - обновлен (#' . $crmOrderId . ' в системе) <br>';
                        self::updateOrderInfo($idOrder, B24CRMOrderTable::EXCHANGE_STATUS_COMPLETED, $crmOrderId);

                        self::addBasketInOrder($idOrder, $crmOrderId);

                        $userCrmId = B24CRMContactTable::getIdCrmCustomerByIdUser($order->getUserId());
                        if ($userCrmId > 0) {
                            self::addContactToOrder($crmOrderId, $userCrmId);
                        } else {
                            static::errorAdd('пользователь для заказа еще не обновлен # заказа - ' . $idOrder);
                        }

                    } else {
                        echo $idOrder . ' - Ошибка! <br>';
                        var_dump(self::errorsGet());
                        self::setOrderStatusUpdate($idOrder, B24CRMOrderTable::EXCHANGE_STATUS_ERROR);
                    }
                } else {
                    echo $idOrder . ' - Ошибка! <br>';
                    var_dump(self::errorsGet());
                    self::setOrderStatusUpdate($idOrder, B24CRMOrderTable::EXCHANGE_STATUS_ERROR);
                }
            }
        } else {
            echo 'Нет заказов для обновления';
        }
        self::errorsReport();
    }


    protected static function userFieldsForUpdate($idUser)
    {
        $userFields = false;
        if ($idUser) {
            $order = array('id' => 'asc');
            $tmp = 'sort'; // параметр проигнорируется методом, но обязан быть
            $filter = ['ID' => $idUser];
            $parameters = [
                'FIELDS' => ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'EMAIL', 'PERSONAL_PHONE', 'WORK_PHONE']
            ];
            $userInfo = \CUser::GetList($order, $tmp, $filter, $parameters)->Fetch();
            if ($userInfo) {
                $userFields = $userInfo;
                $userFields['SOURCE_ID'] = static::$sourceStatusId;
                $email = trim($userFields['EMAIL']);
                $phone = trim($userFields['PERSONAL_PHONE']);
                if ($email) {
                    $userFields['EMAIL'] = [
                        ['VALUE' => $email, "VALUE_TYPE" => "HOME"]
                    ];
                }
                if ($phone) {
                    $userFields['PHONE'] = [
                        ['VALUE' => $phone, "VALUE_TYPE" => "HOME"]
                    ];
                }
            } else {
                static::errorAdd('нет данных пользователя #' . $idUser);
            }
        }
        return $userFields;
    }

    protected static function orderFieldsForUpdate($idOrder)
    {
        $userFields = false;
        if ($idOrder) {
            $order = \Bitrix\Sale\Order::load($idOrder);
            if ($order) {

                $userFields = [
                    'lid' => self::b24OrderSiteId,
                    'userId' => self::$b24BuyerId,
                    'orderTopic' => 'заказ с сайта Yardex #' . $idOrder,
                    'responsibleId' => self::$b24ResponsibleId,
                    'currency' => 'RUB',
                    'personTypeId' => 1,
                    'userDescription' => $order->getField('USER_DESCRIPTION')
                ];
                $propertyCollection = $order->getPropertyCollection();
                $propsToCrm = [];
                $orderProperties = $propertyCollection->getArray()['properties'];

                foreach ($orderProperties as $prop) {
                    if (!isset(self::banOrderProps[$prop['CODE']])) {
                        $propVal = $prop['NAME'] . ': ' . implode(', ', $prop['VALUE']);
                        $propsToCrm[] = $propVal;
                    }
                }

                $paymentIds = $order->getPaymentSystemId();
                $paymentInfo = 'Оплата: ';
                foreach ($paymentIds as $paymentId) {
                    $payment = \CSalePaySystem::GetByID($paymentId);
                    $paymentInfo .= $payment['NAME'];
                }
                $propsToCrm[] = $paymentInfo;
                $userFields['userDescription'] .= "\r\n\r\n\r\n Данные о Заказе:\r\n" . implode("\r\n", $propsToCrm);

            } else {
                static::errorAdd('такого заказа больше нет(возможно удален) #' . $idOrder);
            }
        }
        return $userFields;
    }

    protected static function pushUserToCrm($userFields, $crmContactId = false)
    {
        $res = false;

        try {
            if ($crmContactId) {
                $newUser = B24CRMApi::updateContact($crmContactId, $userFields);
                if ($newUser['result']) {
                    $res = $crmContactId;
                } else {
                    self::errorAdd('В ответе б24API нет true ' . print_r($newUser, 1));
                }
            } else {
                $newUser = B24CRMApi::addContact($userFields);
                if ($newUser['result']) {
                    $res = $newUser['result'];
                } else {
                    self::errorAdd('В ответе б24API нет ID пользователя ' . print_r($newUser, 1));
                }
            }

        } catch (\Exception $exception) {
            self::errorAdd(print_r($exception->getMessage(), 1));
        }
        return $res;
    }

    /**
     * @param $orderFields
     * @param false $idCrmOrder
     * @return false|integer
     */
    protected static function pushOrderToCrm($orderFields, $idCrmOrder = false)
    {
        $res = false;

        try {
            if ($idCrmOrder) {
                $newOrder = B24CRMApi::updateOrder($idCrmOrder, $orderFields);
                if ($newOrder['result']) {
                    $res = $idCrmOrder;
                } else {
                    self::errorAdd('В ответе б24API нет true ' . print_r($newOrder, 1));
                }
            } else {
                $newOrder = B24CRMApi::addOrder($orderFields);
                if ($newOrder['result']) {
                    $res = $newOrder['result']['order']['accountNumber'];
                } else {
                    self::errorAdd('В ответе б24API нет ID пользователя ' . print_r($newOrder, 1));
                }
            }

        } catch (\Exception $exception) {
            self::errorAdd(print_r($exception->getMessage(), 1));
        }
        return $res;
    }

    public static function addBasketInOrder($idOrder, $crmOrderId)
    {
        $order = \Bitrix\Sale\Order::load($idOrder);
        $basket = $order->getBasket();
        $productsFieldsToCrm = [];
        $xmlIds = [];
        foreach ($basket as $basketItem) {
            $xmlId = $basketItem->getField('PRODUCT_XML_ID');
            if ($xmlId) {
                $xmlIds[$xmlId] = $xmlId;
                $productsFieldsToCrm[$xmlId] = [
                    'orderId' => $crmOrderId,
                    'productId' => false,
                    'quantity' => $basketItem->getField('QUANTITY'),
                    'name' => $basketItem->getField('NAME'),
                    'price' => $basketItem->getField('PRICE'),
                    'basePrice' => $basketItem->getField('BASE_PRICE'),
                    'currency' => 'RUB',
                    'VAT_RATE' => (int)($basketItem->getField('VAT_RATE') * 100)
                ];
            } else {
                self::errorAdd('у товара ' . $basketItem->getField('NAME') . ' не задано xmlId1');
            }
        }

        if ($xmlIds) {
            $crmProductsInfo = B24CRMApi::getProductsByFilter(["XML_ID" => $xmlIds], ['ID', 'XML_ID']);

            if ($crmProductsInfo) {
                foreach ($crmProductsInfo as $crmProductInfo) {
                    $productsFieldsToCrm[$crmProductInfo['XML_ID']]['productId'] = $crmProductInfo['ID'];
                    unset($xmlIds[$crmProductInfo['XML_ID']]);
                }
                if ($xmlIds) {
                    foreach ($xmlIds as $xmlIdNotFound) {
                        $newProductFields = [
                            "NAME" => $productsFieldsToCrm[$xmlIdNotFound]['name'],
                            "CURRENCY_ID" => "RUB",
                            "PRICE" => $productsFieldsToCrm[$xmlIdNotFound]['price'],
                            'XML_ID' => $xmlIdNotFound,
                            'VAT_INCLUDED' => 'Y',
                        ];
                        $vatRate = $productsFieldsToCrm[$xmlIdNotFound]['VAT_RATE'];
                        if ($vatRate == '20') {
                            $newProductFields['VAT_ID'] = 2;
                        } else if ($vatRate == '10') {
                            $newProductFields['VAT_ID'] = 3;
                        } else {
                            $newProductFields['VAT_ID'] = 1;
                        }
                        $newProductId = self::addProduct($newProductFields);
                        if ($newProductId) {
                            $productsFieldsToCrm[$crmProductInfo['XML_ID']]['productId'] = $newProductId;
                        } else {
                            unset($productsFieldsToCrm[$xmlIdNotFound]);
                        }
                    }
                }
                if ($productsFieldsToCrm) {
                    foreach ($productsFieldsToCrm as $productFieldsToCrm) {
                        try {
                            $data = B24CRMApi::addBasketItem($productFieldsToCrm);
                            if (!$data['result']) {
                                self::errorAdd('ошибка B24CRMApi::addBasketItem ' . print_r($productFieldsToCrm, 1));
                            }
                        } catch (\Exception $exception) {
                            self::errorAdd('ошибка при добавлении товара в корзину ' . $exception->getMessage() . print_r($productFieldsToCrm, 1));
                        }
                    }
                }

            } else {
                self::errorAdd('у товара ' . $basketItem->getField('NAME') . ' не найдены товары по xmlId ' . print_r($xmlIds, 1));
            }
        }
    }

    protected static function addProduct($fields)
    {
        $newProductId = false;
        try {
            $data = B24CRMApi::addProduct($fields);
            if (!$data['result']) {
                self::errorAdd('ошибка addProduct ' . print_r($fields, 1));
            } else {
                $newProductId = $data['result'];
            }
        } catch (\Exception $exception) {
            self::errorAdd('ошибка addProduct ' . $exception->getMessage() . print_r($fields, 1));
        }
        return $newProductId;
    }

    public static function addContactToOrder($crmOrderId, $userCrmId)
    {
        $apiFields = [
            'id' => (int)$crmOrderId,
            'primaryContactId' => (int)$userCrmId,
        ];
        try {
            $data = B24CRMApi::addContactToOrder($apiFields);
            if (!$data['result']) {
                self::errorAdd('ошибка добавления контакта к заказу ' . print_r($apiFields, 1));
            }
        } catch (\Exception $exception) {
            self::errorAdd('ошибка добавления контакта к заказу ' . $exception->getMessage() . print_r($apiFields, 1));
        }
    }

    protected static function errorAdd($addErrorMsg)
    {
        static::$errors[] = $addErrorMsg;
    }

    protected static function errorsClear()
    {
        static::$errors = [];
    }

    static protected function errorsGet()
    {
        return static::$errors;
    }


    static protected function errorsReport()
    {
        if ($errors = static::errorsGet()) {
            $text = '';
            $text .= implode(' <br>', $errors);
            $sendParams = [
                'THEME' => 'Ошибки ExternalApi\B24CRM',
                'BODY_TEXT' => $text,
            ];

            CustomTools::sendErrorsReport($sendParams, 138);
        }
    }
}