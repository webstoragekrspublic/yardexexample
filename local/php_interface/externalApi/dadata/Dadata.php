<?php

namespace ExternalApi\Dadata;

class Dadata
{
    const minQueryLength = 4;
    const coordsRadiusSearch = 10;

    static function getMainSuggestAddresses($query, $useAllowedLocation = true)
    {
        $res = [];
        if (strlen($query) >= self::minQueryLength) {
            $sendParams = [
                'query' => $query,
                'count' => 5
            ];
            if ($useAllowedLocation) {
                $sendParams['locations'] = self::allowedLocations;
            }
            try {
                $res = DadataApi::getSuggestAddresses($sendParams);
            } catch (Exception $e) {
            }
        }
        return $res;
    }

    static function getStandartedAddress($query)
    {
        $res = [];
        if (isset($query)) {
            $sendParams = [0 => $query];
            try {
                $res = DadataApi::getStandatrizeAddress($sendParams);
            } catch (Exception $e) {
            }
        }
        return $res;
    }

    static function getFirstMainSuggestAddresses($query, $useAllowedLocation = true)
    {
        $addresses = self::getMainSuggestAddresses($query, $useAllowedLocation);
        return $addresses[0] ?? false;
    }

    static function getFirstMainSuggestAddressesByCoords($coords)
    {
        $res = false;
        if ($coords) {
            [$lat, $lon] = explode(',', $coords);
            $sendParams = [
                'lat' => $lat,
                'lon' => $lon,
                'radius_meters' => self::coordsRadiusSearch
            ];
            $addresses = DadataApi::getSuggestAddressesByCoords($sendParams);
            if (isset($addresses[0])) {
                $res = $addresses[0];
            }
        }

        return $res;
    }

    const allowedLocations = [
        [
            'city_fias_id' => '9b968c73-f4d4-4012-8da8-3dacd4d4c1bd', //Красноярск
        ],
        //            [
        //                'city_fias_id' => 'a139c8ba-831e-4ee5-87a9-1eead6984288', //Красноярский край, г Сосновоборск
        //            ],
        //            [
        //                'city_fias_id' => 'e532b3c3-d300-4585-ada6-40e7900c2abf', //Железногорск
        //            ],
        //            [
        //                'city_fias_id' => '76cc6918-6b7c-42b4-80da-4635d97d0da1', //Красноярский край, г Дивногорск
        //            ],
        // Маганск
        //            [
        //                'settlement_fias_id' => '8dfeddf6-7f00-4ace-8676-f74db5056c18'//Красноярский край, Березовский р-н, село Зыково
        //            ],
        //            [
        //                'settlement_fias_id' => 'e5ed6dfb-e32f-409e-875c-4d3a7bd8a741'//Красноярский край, Березовский р-н, деревня Свищево
        //            ],
        //            [
        //                'settlement_fias_id' => '2e1dc364-812e-4b1e-8f7c-dac5f7793c59'//Красноярский край, Березовский р-н, тер. СНТ Маганское (п Березовский)
        //            ],
        //            [
        //                'settlement_fias_id' => '9209591d-44a3-4975-8b32-35d3e6c44d48'//Красноярский край, Березовский р-н, село Маганск
        //            ],
        //            [
        //                'settlement_fias_id' => '4c23be47-d3e6-4790-a189-05e8af7ac743'//Красноярский край, Березовский р-н, поселок Березовский
        //            ],
        //            [
        //                'settlement_fias_id' => 'b77fab70-f6ed-44c4-9101-bc7f90f337e9'//Красноярский край, Березовский р-н, поселок Береть
        //            ],
        //            //Вознесенка
        //            [
        //                'settlement_fias_id' => 'c5952ac7-8a5c-442f-b09b-4a6cd2f7b78d'//Красноярский край, Березовский р-н, село Вознесенка
        //            ],
        //            [
        //                'settlement_fias_id' => '3883a6b7-9bfd-4a1b-9c25-fee21bc50357'//Красноярский край, Березовский р-н, деревня Лопатино
        //            ],
        //            [
        //                'settlement_fias_id' => '946ca18e-68b1-4a6d-92b6-cdcb24066787'//Красноярский край, Березовский р-н, деревня Малая Кускунка
        //            ],
        //            //Сосновоборск
        //            [
        //                'settlement_fias_id' => 'ea12a3a9-70e7-4fb4-8b85-8b2f260c9211'//Красноярский край, Березовский р-н, пгт Березовка
        //            ],
        //            [
        //                'settlement_fias_id' => '250a05e6-c980-4436-9b49-9c3a49adc18c'//Красноярский край, Березовский р-н, деревня Терентьево
        //            ],
        //            [
        //                'settlement_fias_id' => 'f9e13faa-8b3f-4041-97a0-9b34e4dd4f77'//Красноярский край, Березовский р-н, село Есаулово
        //            ],
        //            [
        //                'settlement_fias_id' => 'ac89e7ac-3172-4657-b5ba-34337229f69b'//Красноярский край, Березовский р-н, деревня Киндяково
        //            ],
        //            [
        //                'settlement_fias_id' => '0c62a061-ab50-4be1-8b4e-e88bd00d5093'//Красноярский край, Березовский р-н, село Бархатово
        //            ],
        //            [
        //                'settlement_fias_id' => '1c9dacfd-46da-4540-9b03-021df857c25a'//Красноярский край, Березовский р-н, деревня Челноково
        //            ],
        //            //Подгорный
        //            [
        //                'settlement_fias_id' => 'caf562f6-3c28-4137-9599-3827379f7a2d'//Красноярский край, поселок Подгорный
        //            ],
        //            [
        //                'settlement_fias_id' => '2ead2f80-f2ab-4f69-a088-30c4b6116880'//Красноярский край, г Железногорск, поселок Новый Путь
        //            ],
        //            //Железногорск
        //            [
        //                'settlement_fias_id' => '41bea9a1-7126-40b8-bd4e-1ee7952cdf7f'//Красноярский край, г Железногорск, поселок Тартат
        //            ],
        //            [
        //                'settlement_fias_id' => 'ee03f722-cd0d-43cd-9456-b5b4ecbfc79a'//Красноярский край, Емельяновский р-н, поселок Первомайский
        //            ],
        //            //Б. Мурта, Сухобузимо
        //            [
        //                'settlement_fias_id' => '6fe2191a-9698-414c-af94-f09f60d6bfe8'//Красноярский край, Емельяновский р-н, деревня Старцево
        //            ],
        //            [
        //                'settlement_fias_id' => '6fdb961b-8821-4b35-b8bf-f2ca22ea1508'//Красноярский край, Емельяновский р-н, село Шуваево
        //            ],
        //            [
        //                'settlement_fias_id' => '2bab5c8a-5242-4062-8707-58bf7aacf086'//Красноярский край, Емельяновский р-н, село Совхоз Сибиряк
        //            ],
        //            [
        //                'settlement_fias_id' => '2d9c07d9-8ff1-4be3-8946-524f2c9c0a35'//Красноярский край, Емельяновский р-н, поселок Красный Пахарь
        //            ],
        //            [
        //                'settlement_fias_id' => 'e44e90b4-7d4a-4878-bad6-2d2417ec0dd9'//Красноярский край, Емельяновский р-н, поселок Придорожный
        //            ],
        //            [
        //                'settlement_fias_id' => '628bc4a6-ff48-4ceb-9d24-8ef474e2bc99'//Красноярский край, Емельяновский р-н, деревня Таскино
        //            ],
        //            [
        //                'settlement_fias_id' => '5acef914-bae8-4920-b607-1a868c882cab'//Красноярский край, Сухобузимский р-н, село Сухобузимское?
        //            ],
        //            [
        //                'settlement_fias_id' => 'a9f015f7-84ae-48b5-a8bd-be18892a7940'//Красноярский край, Сухобузимский р-н, село Миндерла
        //            ],
        //            [
        //                'settlement_fias_id' => 'ef028136-9c41-403a-8810-8cf27e5c03b3'//Красноярский край, Сухобузимский р-н, поселок Бузим
        //            ],
        //            [
        //                'settlement_fias_id' => '3ccd1623-9fe8-4710-b41d-2c3ba12d855d'//Красноярский край, Большемуртинский р-н, пгт Большая Мурта
        //            ],
        //            [
        //                'settlement_fias_id' => '9eec2ccb-1ae0-4cf0-ba07-8b9b4b1ac081'//Красноярский край, Большемуртинский р-н, село Таловка
        //            ],
        //            [
        //                'settlement_fias_id' => '9e1b2779-e889-49b9-9ba3-bc63b6a8ccb3'//Красноярский край, Сухобузимский р-н, село Шила
        //            ],
        //            [
        //                'settlement_fias_id' => '5bd5acb3-590b-48fa-af00-bc2f168f012c'//Красноярский край, Сухобузимский р-н, поселок Шилинка
        //            ],
        //            [
        //                'settlement_fias_id' => '41aa8f55-a35a-4892-a581-550c2c1d8ce8'//Красноярский край, Большемуртинский р-н, поселок Раздольное
        //            ],
        //            [
        //                'settlement_fias_id' => 'a526ea84-40cb-47c3-86a0-6cc5724a35dc'//Красноярский край, Большемуртинский р-н, поселок Красные Ключи
        //            ],
        //            //Емельяново, Элита
        //            [
        //                'settlement_fias_id' => '647e488c-0fe2-4f19-b0c7-1d715070df44'//Красноярский край, Емельяновский р-н, поселок Логовой
        //            ],
        //            [
        //                'settlement_fias_id' => '65916bb8-2ea7-45b5-8ebd-a26472044dc7'//Красноярский край, Емельяновский р-н, деревня Творогово
        //            ],
        //            [
        //                'settlement_fias_id' => '661df5a8-2bf6-4cc2-ad8d-8688314f4baf'//Красноярский край, Козульский р-н, тер Автомобильная дорога Байкал?
        //            ],
        //            [
        //                'settlement_fias_id' => '20cb9bfe-52d1-4078-813c-848ba55af3e3'//Красноярский край, Емельяновский р-н, село Дрокино
        //            ],
        //            [
        //                'settlement_fias_id' => '878d58f0-36fa-4ef7-adac-f00add63887f'//Красноярский край, Емельяновский р-н, пгт Емельяново
        //            ],
        // //            [
        // //                'settlement_fias_id' => ''//Аэропорт "Емельяново"?
        // //            ],
        //            [
        //                'settlement_fias_id' => '5c1ca37b-9a0b-43e9-ad8d-0b5c87bd1cb8'//Красноярский край, Емельяновский р-н, село Еловое
        //            ],
        // //            [
        // //                'settlement_fias_id' => ''//Видный?
        // //            ],
        //            [
        //                'settlement_fias_id' => 'b60a121f-1fe4-4d88-84d1-6a2d0b779d5d'//Красноярский край, Емельяновский р-н, поселок Элита
        //            ],
        //            [
        //                'settlement_fias_id' => 'e5d5acd9-ca6f-44e7-baa3-9e8aaba078c0'//Красноярский край, Емельяновский р-н, село Арейское
        //            ],
        //            [
        //                'settlement_fias_id' => 'c54e72e3-0faa-4640-aaa0-e1b3091bfb00'//Красноярский край, Емельяновский р-н, деревня Плоское
        //            ],
        //            [
        //                'settlement_fias_id' => '00750cc-e762-49cb-b447-c59ce2b5fabf'//Красноярский край, Емельяновский р-н, поселок Минино
        //            ],
        //            [
        //                'settlement_fias_id' => 'b3e138f7-5d3f-4fac-b17f-6ded332dc634'//Красноярский край, Емельяновский р-н, деревня Минино
        //            ],
        //            //Кедровый
        //            [
        //                'settlement_fias_id' => 'c77b9b15-089d-4ce0-ac89-932c869ee93e'//Красноярский край, Емельяновский р-н, деревня Сухая
        //            ],
        //            [
        //                'settlement_fias_id' => '01cb6dda-0c17-45d2-9528-00ccd131afaf'//Красноярский край, Емельяновский р-н, деревня Крутая
        //            ],
        //            [
        //                'settlement_fias_id' => 'c742d582-44d4-490b-85b3-61555e3cb84c'//Красноярский край, Емельяновский р-н, поселок Памяти 13 Борцов
        //            ],
        //            [
        //                'settlement_fias_id' => 'af5a5735-c2ee-47d7-a203-1352f8c17ff8'//Красноярский край, пгт Кедровый
        //            ],
        //            //Дивногорск
        //            [
        //                'settlement_fias_id' => 'ec6f1471-47b2-4551-9da3-584e8a42b10d'//Красноярский край, г Дивногорск, поселок Слизнево
        //            ],
        // //            [
        // //                'settlement_fias_id' => ''//М. Слизнево?
        // //            ],
        //            [
        //                'settlement_fias_id' => '3b9d373d-c5e4-494c-b559-e714b0376f4e'//Красноярский край, г Дивногорск, село Овсянка
        //            ],
        // //            [
        // //                'settlement_fias_id' => ''//Молодежный?
        // //            ],
        //            [
        //                'settlement_fias_id' => 'cf2b2039-d469-4d5c-acd3-72ade149513b'//Красноярский край, г Дивногорск, поселок Усть-Мана
        //            ],
        //            [
        //                'settlement_fias_id' => ''//Манский?
        //            ],
        //            //Красноярск
        [
            'settlement_fias_id' => '59401103-144b-4fee-9a43-cbb61f23b4ad' //Красноярский край, Емельяновский р-н, поселок Солонцы
        ]
    ];
}
