<?php

namespace ExternalApi\Dadata;

class DadataApi
{
    //https://dadata.ru/suggestions/
    protected const API_KEY = 'd82b6168eba605f1b79e577180f0b0772d109976';
    protected const SECRET_KEY = 'a6f5036ef36b74ed6736a8d53c822312143e0f88';
    protected const BASE_API_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';
    protected const STANDARTIZE_API_URL = 'https://cleaner.dadata.ru/api/v1/clean/address';
    protected const GEOLOCATE_API_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/geolocate/address';

    static protected function request($sendParams = false, $method = 'GET', $api = 'base')
    {
        $headers = array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Token ' . static::API_KEY,
        );

        $ch = curl_init();


        switch ($api){
            case 'geolocate':
                $api_url =  static::GEOLOCATE_API_URL;
                break;
            case 'standartize':
                $api_url =  static::STANDARTIZE_API_URL;
                $headers[] =
                    'X-Secret: ' . static::SECRET_KEY;
                break;
            case 'base':
            default:
                $api_url = static::BASE_API_URL;
                break;
        }

        curl_setopt($ch, CURLOPT_URL, $api_url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);


        $jsonBody = json_encode($sendParams);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonBody);
        $headers[] = 'Content-Length: ' . strlen($jsonBody);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);
        return json_decode($data, 1);
    }

    static public function getSuggestAddresses($sendParams)
    {
        $data = static::request($sendParams, 'POST', 'base');
        $res = isset($data['suggestions']) ? $data['suggestions'] : [];
        return $res;
    }

    static public function getSuggestAddressesByCoords($sendParams)
    {
        $data = static::request($sendParams, 'POST', 'geolocate');
        $res = isset($data['suggestions']) ? $data['suggestions'] : [];
        return $res;
    }

    static public function getStandatrizeAddress($sendParams)
    {
        $data = static::request($sendParams, 'POST', 'standartize');
        $res = $data[0] ?? [];
        return $res;
    }
}
