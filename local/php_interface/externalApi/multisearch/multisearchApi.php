<?

namespace ExternalApi\MultiSearch;

use Bitrix\Main\Loader;

class MultiSearchApi
{
    const BASE_URL = 'https://api.multisearch.io/';
    protected const API_ID = '11427';

    static public function getQueryResult($query = '', $sectionsFilter = [], $sort = ['field' => 'name', 'order' => 'asc'], $group = '0', $limit = 50, $offset = 0)
    {
        global $USER;

        $url = static::BASE_URL . '';

        $params = http_build_query([
            'id' => static::API_ID,
            'query' => $query ?? '',
            'sort' => $sort['field'] ?? 'name' . '.' . $sort['order'] ?? 'asc',
            'limit' => $limit ?? 50,
            'offset' => $offset ?? 0,
            'categories' => $group ?? '0'
        ]);
        if (!empty($sectionsFilter)) {
            foreach ($sectionsFilter as &$sectionParam) {
                $sectionParam = '&t[]=' . $sectionParam;
            }
            $sectionsFilter = implode($sectionsFilter);
        }
        $params .= $sectionsFilter . (!empty($USER->GetID()) ? '&uid=' . $USER->GetID() : '&' . bitrix_sessid_get('uid'));

        $curlHandle = static::initCurl($url . '?' . $params);
        $result = static::execCurl($curlHandle);

        return $result;
    }

    static protected function initCurl($url)
    {
        $curlHandle = curl_init();
        $headers = ['Content-Type: application/x-www-form-urlencoded'];
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        return $curlHandle;
    }

    static protected function execCurl($curlHandle)
    {
        $execResult = curl_exec($curlHandle);
        $resultOrder = json_decode($execResult, true);

        if (isset($resultOrder)) {
            return $resultOrder;
        }
        return $execResult;
    }
}
