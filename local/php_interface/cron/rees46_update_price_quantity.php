<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "IBLOCK_SECTION_ID", "PROPERTY_*");
// Фильтровать только по изменненым за последний час товарам
$hourSeconds = 60 * 60;

$dateTime = new \DateTime();
$dateTime->setTimestamp(time());
$beforeTimeStamp = $dateTime->getTimestamp() - $hourSeconds;
$beforeDateTime = new \DateTime();
$beforeDateTime->setTimestamp($beforeTimeStamp);
//$beforeDateTime = \DateTime::createFromFormat('!d.m.Y',"{$beforeDateTime->format('d')}.{$beforeDateTime->format('m')}.{$beforeDateTime->format('Y')}");
$DATE_CREATE_FROM = \Bitrix\Main\Type\DateTime::createFromTimestamp($beforeDateTime->getTimestamp());

$currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";

$currentUrl .= $_SERVER["HTTP_HOST"];

$arFilter = Array("IBLOCK_ID"=>114, ">DATE_MODIFY_FROM" => $DATE_CREATE_FROM->toString());
$resEl = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
$items = [];
while($arEl = $resEl->GetNextElement()){
    $arFields = $arEl->GetFields();
    $arProps = $arEl->GetProperties();

    $currency = "RUB";

    $imgSrc = CFile::GetPath($arFields["DETAIL_PICTURE"]);

    $arCatalogRes = CCatalogProduct::GetByID($arFields["ID"]);
    $available = false;
    if($arCatalogRes["QUANTITY"] > 0){
        $available = true;
    }

    $price = CPrice::GetBasePrice($arFields["ID"]);
    $price = $price["PRICE"];

    $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields["ID"]);
    //$resRatio = \CCatalogMeasureRatio::getList(Array(), array('IBLOCK_ID'=>114, 'PRODUCT_ID'=>$arFields["ID"]), false, false, array());
    //$arFieldsRatio = $resRatio->Fetch();

    if($arMeasure[$arFields["ID"]]['RATIO'] != 1){
        $price = $price * $arMeasure[$arFields["ID"]]['RATIO'];
    }

    $arFields["NAME"] = str_replace('&quot;', '"', $arFields["NAME"]);

    $items[] = [
        'id' => $arFields["ID"],
        'name' => $arFields["NAME"],
        'price' => $price,
        'currency' => $currency,
        'url' => $currentUrl.$arFields["DETAIL_PAGE_URL"],
        'picture' => $currentUrl.$imgSrc,
        'available' => $available,
        'categories' => [$arFields["IBLOCK_SECTION_ID"]],
        'stock_quantity' => $arCatalogRes["QUANTITY"],
        "params" => [
            [
                "name" => "Быстрая доставка",
                "value" => [$arProps['FAST_DELIVERY']['VALUE']]
            ],
            [
                "name" => "Коэффициент единицы измерения",
                "value" => [$arMeasure[$arFields["ID"]]['RATIO']]
            ],
            [
                "name" => "measure",
                "value" => [$arMeasure[$arFields["ID"]]['MEASURE']['SYMBOL_RUS']],
            ],
            [
                "name" => "quantity",
                "value" => [$arCatalogRes["QUANTITY"]]
            ],
            [
                "name" => "ratio",
                "value" => [$arMeasure[$arFields["ID"]]['RATIO']]
            ]
        ],
    ];
}

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.rees46.ru/import/products',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'PUT',
    CURLOPT_POSTFIELDS =>'{
    "shop_id": "60eec8bb4851421cbbcdac54907c00",
    "shop_secret": "1a7394f1a45a26b68d4f01751a2fee3b",
    "items": '. json_encode($items).'
}',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json'
    ),
));

$response = curl_exec($curl);
curl_close($curl);
?>