<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}


$arTreeDescr = array(
    'js' => [
        '/bitrix/js/main/jquery/jquery-2.1.3.min.min.js',
        '/bitrix/js/catalog/core_tree.js',
        '/local/templates/aspro_next_custom/js/browser.min.js',
        '/local/templates/aspro_next_custom/js/jquery.fancybox.js',
        '/local/templates/aspro_next_custom/js/browser.js',
        '/local/templates/aspro_next_custom/js/jquery.maskedinput-1.2.2.js',
    ],
    'css' => [
        '/bitrix/panel/catalog/catalog_cond.css',
        '/local/templates/aspro_next_custom/css/jquery.fancybox.css',
        '/local/templates/.default/admin_styles.css',
    ],
    'lang' => '/bitrix/modules/catalog/lang/' . LANGUAGE_ID . '/js_core_tree.php',
    'rel' => array('core', 'date', 'window')
);
\CJSCore::RegisterExt('adminScripts', $arTreeDescr);
\CJSCore::Init('adminScripts');

$APPLICATION->IncludeComponent(
    "custom:empty_component",
    "fancybox_modals",
    ['MOBILE_VERSION' => 'Y'],
    false
);

$APPLICATION->IncludeComponent(
    "custom:empty_component",
    "yandex_routing",
    [],
    false
);

require_once __DIR__ . '/adminPages/sale_order.php';
require_once __DIR__ . '/adminPages/sale_order_create_edit.php';
require_once __DIR__ . '/adminPages/sale_order_view_edit.php';