<?php


$MESS["TORGOVIE_MARKI_TAB_SETTINGS"] = "Основные настройки";
$MESS["TORGOVIE_MARKI_FIELD_CHOSE_CATEGORY"] = "Выбор категории";
$MESS["TORGOVIE_MARKI_FIELD_CHOSE_CATEGORY_IDS"] = "Выбранные категории IDS";
$MESS["TORGOVIE_MARKI_FIELD_CHOSE_TORGOVIE_MARKS"] = "Выбор Торговые Марки";
$MESS["TORGOVIE_MARKI_FIELD_CHOSE_TORGOVIE_MARKS_IDS"] = "Выбранные Торговые Марки IDS";
$MESS["TORGOVIE_MARKI_EXPORT_EDADEAL"] = "Запуск кода";
$MESS["TORGOVIE_MARKI_FIELD_NAME_FILE_EXPORT"] = "Задайте имя файла json\n(если не задан то имя будет file.json)\n(Файл находиться по пути upload/edadeal_json/file.json )";
$MESS["TORGOVIE_MARKI_FIELD_PRODUCT_URL"] = "Задайте ссылку на элементы инфоблока Товары (символьный код: ed_products)";