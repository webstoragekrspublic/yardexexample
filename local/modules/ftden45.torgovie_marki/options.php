<?php

//namespace ;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;

\Bitrix\Main\Loader::includeModule('iblock');
Bitrix\Main\Loader::includeModule("sale");


$module_id = 'ftden45.torgovie_marki'; //обязательно, иначе права доступа не работают!

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);


\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

## выборка категорий
$iblockId = 114;

$arSelect = ['ID', "IBLOCK_ID", 'NAME', 'DETAIL_PICTURE', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', "PROPERTY_*"];
$arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y");
$resEl = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);

$currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";
$currentUrl .= $_SERVER["HTTP_HOST"];
$catalogsInfo = [];
//$catalogsInfo['ID']["0_0_0"] = 'выберите категорию';
$countProducts = 0;
while($arEl = $resEl->GetNextElement()) {
    $countProducts++;
    $arFields = $arEl->GetFields();
    //$arProps = $arEl->GetProperties();
    if (false) {
        echo $arFields['ID'].'<br>';
        echo $arFields['NAME'].'<br>';
        echo $arFields['DEPTH_LEVEL'].'<br>';
        echo "<pre>";
        print_r($arFields);
        echo "</pre>";
    }
    $space = '';
    if ($arFields['DEPTH_LEVEL'] > 0) {
        for ($i = 0; $i < $arFields['DEPTH_LEVEL'];$i++) {if ($i >= 1) $space .= '-';}
    }
    $catalogsInfo['ID'][$arFields['ID']] = $arFields['NAME'];
    $catalogsInfo['ID_PARENT'][$arFields['ID']] = $arFields['IBLOCK_SECTION_ID'];
    //if ($countProducts > 4) {die();}
}
## Выбор Торговой Марки
$iblockId = 118;

$arSelect = ['ID', "IBLOCK_ID", 'NAME', 'DETAIL_PICTURE', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', "PROPERTY_*"];
$arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y");
$resEl = CIBlockElement::GetList(array('left_margin' => 'asc'),$arFilter);

$currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";
$currentUrl .= $_SERVER["HTTP_HOST"];
$torgovieMarki = [];
while($arEl = $resEl->GetNextElement()) {
    $countProducts++;
    $arFields = $arEl->GetFields();
    $arProps = $arEl->GetProperties();
    if (false) {
        echo $arFields['ID'].'<br>';
        echo $arFields['NAME'].'<br>';
        echo "<pre>";
        //print_r($arFields);
        echo "</pre>";
    }
    $torgovieMarki['ID'][$arFields['ID']] = $arFields['NAME'];
}
##

#Описание опций

//Asset::getInstance()->addJs('/bitrix/js/'.$module_id.'/scripts.js');

$options = ['modul_id' => $module_id, 'key1' => 'value1', 'key2' => 'value2'];
$options = json_encode($options);

Asset::getInstance()->addString(
    "<script id='".str_replace('.','-',$module_id)."-params' data-params='".$options."'></script>",
    true
);
Asset::getInstance()->addJs('/local/modules/'.$module_id.'/install/assets/scripts/scripts.js');

$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('TORGOVIE_MARKI_TAB_SETTINGS'),
        'OPTIONS' => array(
            array('categories_product', 'файл по пути: https://yarbox.ru//upload/order_excel_file/(ctategories.csv,ctategories_id.csv,torgovie_marki.csv,torgovie_marki_id.csv)',
                'https://yarbox.ru//upload/order_excel_file/(ctategories.csv,ctategories_id.csv,torgovie_marki.csv,torgovie_marki_id.csv)',
                array('text')),
        )
    ),
    array(
        "DIV" => "edit2",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),
);
echo $default_group = COption::GetOptionString($module_id, "categories_product_ids", "");
#Сохранение

$name_file_output = 'file.json';
$codeLisenArraySettings = [];
if ($request->isPost() && $request['Export'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            if ($optionName == 'field_name_file_output') $codeLisenArraySettings['field_name_file_output'] = $optionValue = $request->getPost($optionName);
            else if ($optionName == 'field_product_url') $codeLisenArraySettings['field_product_url'] = $optionValue = $request->getPost($optionName);
            else if ($optionName == 'categories_product_ids') $codeLisenArraySettings['categories_product_ids'] = $optionValue = $request->getPost($optionName);
            else if ($optionName == 'torgovie_marks_product_ids') $codeLisenArraySettings['torgovie_marks_product_ids'] = $optionValue = $request->getPost($optionName);
            else $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }
    // Mobile Code start ftden45

    function print_color ($item,$die = true) {
        echo "<pre>";
        echo "st_block";
        print_r($item);
        echo "</pre>";
        echo "end_";
        if ($die) die();
    }

    // БЛОК: взятие id_category из AreaText
    // start_id_category
    echo 'start_id_category'.'<br>';
    $categoriesProductIdsString = explode(";", $codeLisenArraySettings['categories_product_ids']);
    $categoriesProductIds = []; // чистые id категорий без текста(их названий)
    foreach ($categoriesProductIdsString as $string) {
        if (preg_match('/\((.*?)\)/i',$string, $outArrayResult)) {
            echo $outArrayResult[1]."<br>";
            $categoriesProductIds[] = $outArrayResult[1];
        }
    }
    // end_id_category

    // start_id_torgovie_marki
    echo 'start_id_torgovie_marki'.'<br>';
    $torgovieMarkiIdsString = explode(";", $codeLisenArraySettings['torgovie_marks_product_ids']);
    $torgovieMarkiIds = []; // чистые id торговых марок без текста(их названий)
    foreach ($torgovieMarkiIdsString as $string) {
        if (preg_match('/\((.*?)\)/i',$string, $outArrayResult)) {
            echo $outArrayResult[1]."<br>";
            $torgovieMarkiIds[] = $outArrayResult[1];
        }
    }
    // end_id_torgovie_marki

    $debug_mode = false;

    $arSelect = Array("ID", "NAME", "ACTIVE", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "IBLOCK_SECTION_ID", "PROPERTY_*");
// Фильтровать только по изменненым за последний час товарам
    $arPropsField = ['DATE_EXPIRATION','SHELF_LIFE']; // Дата окончания срока годности | срок годности
    $dateToday = date('d.m.Y'); // текущая дата
    //echo $dateToday."<br>";
    //echo CDataBase::FormatDate($dateToday, "DD.MM.YYYY", "YYYY-MM-DD")."<br>"; // 2005-12-31
    $hourSeconds = 17 * 24 * 60 * 60;

    $dateTime = new \DateTime();
    $dateTime->setTimestamp(time());
    $beforeTimeStamp = $dateTime->getTimestamp() - $hourSeconds;
    $beforeDateTime = new \DateTime();
    $beforeDateTime->setTimestamp($beforeTimeStamp);
//$beforeDateTime = \DateTime::createFromFormat('!d.m.Y',"{$beforeDateTime->format('d')}.{$beforeDateTime->format('m')}.{$beforeDateTime->format('Y')}");
    $dateCreateFrom = \Bitrix\Main\Type\DateTime::createFromTimestamp($beforeDateTime->getTimestamp());

    $currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";

    $currentUrl .= $_SERVER["HTTP_HOST"];

    $arFilter = Array("IBLOCK_ID"=>114, "ACTIVE" => 'Y');
    $resEl = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    $items = [];
    $itemsCsv = [];
    $itemsCsvContinent = [];
    $itemsCsvOtherProduct = [];
    $counter = 0;
    $indexStart = 0;
    $index = 0;
    $ifId = false;
    $idProduct = 23699;
    //$filePath = \Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/file_'.date("Y_m_d_H_m_s").'.csv';
    $filePath2 = \Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/file2.csv';
    //список товаров, ОСГ дни, ОСГ %, Дата смерти товара, Старая цена, Размер скидки, Новая акционная цена. дата снятия признака "Без движения"
    $itemsCsvContinent[] = ['ID товара', 'название', 'url', 'Дата смерти товара', 'Срок хранения'];
    $itemsCsvOtherProduct[] = ['ID товара', 'название', 'url', 'Дата смерти товара', 'Срок хранения'];
    $itemsCsvNorm[] = ['ID товара', 'название', 'url', 'ОСГ дни', 'ОСГ %', 'Дата смерти товара', 'Старая цена', 'Размер скидки', 'Новая акционная цена', 'дата снятия признака "Без движения"', 'ошибки'];


    $torgovieMarkiAll = [];
    $catalogsInfoAll = [];
    while($arEl = $resEl->GetNextElement()){
        $itemCsvAll = [];
        $itemCsvError = [];
        $errors = [];
        $mode = 'norm';
        $arFields = $arEl->GetFields();
        $arProps = $arEl->GetProperties();
        // filter по ID_Categories
        /*if (!in_array($arFields['IBLOCK_SECTION_ID'], $categoriesProductIds)) {
            continue;
        }*/
        // проверки начало
        if (false)
        if ($arFields['IBLOCK_SECTION_ID'] != 994) {
            continue;
        }
        if (false) {
            echo "NAME ".$arFields['NAME']."<br>"; // имя товара
            echo "CATEGORY_ID ".$arFields['IBLOCK_SECTION_ID']."<br>"; // id категории
            echo "TORGOVAJAMARKA ".$arFields['PROPERTY_1070']."<br>"; // id торговой марки

            //print_color($arFields,false);
            //print_color($arProps);
        }
        if (true) {
            $torgovieMarkiAll[$arFields['PROPERTY_1070']][] = $arFields['IBLOCK_SECTION_ID'];
            $catalogsInfoAll[$arFields['IBLOCK_SECTION_ID']][] = $arFields['PROPERTY_1070'];
            continue;
        }
        if ($ifId && $arFields["ID"] != $idProduct) {
            continue;
        }
        // проверки конец

    }

    //$torgovieMarkiAll[$arFields['PROPERTY_1070']][] = $arFields['IBLOCK_SECTION_ID'];
    //$catalogsInfoAll[$arFields['IBLOCK_SECTION_ID']][] = $arFields['PROPERTY_1070'];

    // key это ID torgovoi Marki
    // value это ID category
    $torgovieMarkiAllCsv = [];
    $torgovieMarkiAllCsvWithId = [];
    $torgovieMarkiAllCsv[] = ['Торговая марка','Категории'];
    $torgovieMarkiAllCsvWithId[] = ['Торговая марка','Категории'];
    foreach ($torgovieMarkiAll as $key => $value) {
        $stringCategory = '';
        $stringCategoryId = '';
        $value = array_unique($value);
        foreach ($value as $el) {
            $stringCategory .= $catalogsInfo['ID'][$el].",";
            $stringCategoryId .= $catalogsInfo['ID'][$el]."(".$el."),";
        }
        $torgovieMarkiAllCsv[] = [$torgovieMarki['ID'][$key],$stringCategory];
        $torgovieMarkiAllCsvWithId[] = [$torgovieMarki['ID'][$key]."(".$key.")",$stringCategoryId];
    }

    $catalogsInfoAllCsv = [];
    $catalogsInfoAllCsvWithId = [];
    $catalogsInfoAllCsv[] = ['Категория','Торговые марка'];
    $catalogsInfoAllCsvWithId[] = ['Категория','Торговые марки',];
    foreach ($catalogsInfoAll as $key => $value) {
        $stringTorgovieMarki = '';
        $stringTorgovieMarkiId = '';
        $value = array_unique($value);
        foreach ($value as $el) {
            $stringTorgovieMarki .= $torgovieMarki['ID'][$el].",";
            $stringTorgovieMarkiId .= $torgovieMarki['ID'][$el]."(".$el."),";
        }
        $catalogsInfoAllCsv[] = [$catalogsInfo['ID'][$key],$stringTorgovieMarki];
        $catalogsInfoAllCsvWithId[] = [$catalogsInfo['ID'][$key]."(".$key.")",$stringTorgovieMarkiId];
    }

    //Приводим поля к формату который может прочитать 1с (т.к. стандартный формат csv она не понимает)
    function csvRFC1c7Field($fieldValue)
    {
        $res = $fieldValue;
        $res = trim($res);
        $res = str_replace(["\r", "\n"], '', $res);
        return $res;
    }
    //Приводим вывод к формату который может прочитать 1с (т.к. стандартный формат csv она не понимает)
    function csvRFC1c7Content($content)
    {
        $content = mb_convert_encoding($content, 'WINDOWS-1251', "utf-8");
        $content = str_replace("\n", "\r\n", $content);
        return $content;
    }
    function csvCreate ($itemsCsv = [], $name = 'file.csv') {

        $content = '';
        $handle = fopen('php://temp', 'r+');
        $header = array_keys($itemsCsv[0]);
        fputcsv($handle, $header, ';', '"');

        foreach ($itemsCsv as $line) {
            foreach ($line as $key => $value) {
                $line[$key] = csvRFC1c7Field($value);
            }
            fputcsv($handle, $line, ';', '"');
        }

        rewind($handle);
        while (!feof($handle)) {
            $content .= fread($handle, 8192);
        }
        fclose($handle);
        $content = csvRFC1c7Content($content);
        //return $content;
        $filePath = \Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/'.$name;
        $file = new \Bitrix\Main\IO\File($filePath);
        $file->putContents($content);
    }

    csvCreate($torgovieMarkiAllCsv,'torgovie_marki.csv');
    csvCreate($torgovieMarkiAllCsvWithId,'torgovie_marki_id.csv');
    csvCreate($catalogsInfoAllCsv,'ctategories.csv');
    csvCreate($catalogsInfoAllCsvWithId,'ctategories_id.csv');






    //$content = str_replace("\n", "\r\n", $content);



    /*$csvFile->LoadFile(\Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/file.csv');
    $csvFile->SetDelimiter(',');
    while ($arRes = $csvFile->Fetch()) {
        echo($arRes);
    }*/

    // Mobile Code end ftden45

}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<? $tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='TORGOVIE_MARKI_settings'>

    <? foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):?>
        <? $tabControl->BeginNextTab(); ?>
        <? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

    <?      endif;
        endforeach; ?>

    <?
    $tabControl->BeginNextTab();



    $tabControl->Buttons(); ?>

    <div class="notes" id="notes">
    <input type="submit" name="Export" value="<?echo Loc::getMessage('TORGOVIE_MARKI_EXPORT_EDADEAL')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>" onclick="work_with_row()">
    <?=bitrix_sessid_post();?>
</form>
<script type="text/javascript">
    function work_with_row(){
        alert('zzzzzz');
    }
</script>
<? $tabControl->End(); ?>

