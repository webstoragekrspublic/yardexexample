<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Custom\SuppliersOrders\Main;
use Bitrix\Main\Type\DateTime;
use Handlers\ExternalSuppliers\ExternalSuppliersOrderTable;

Loc::loadMessages(__FILE__);

// получаем идентификатор модуля
$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialchars($request['mid'] != '' ? $request['mid'] : $request['id']);
// подключаем наш модуль
Loader::includeModule($module_id);
$asset = Bitrix\Main\Page\Asset::getInstance();
echo $asset->insertCss('/local/modules/custom.suppliersorders/lib/admin/options/style.css');
$asset->addJs('/local/modules/custom.suppliersorders/lib/admin/options/script.js');
/*
 * Параметры модуля со значениями по умолчанию
 */
$aTabs = [
    [
        'DIV' => 'edit1',
        'TAB' => 'Просмотр заказов поставщиков',
        'TITLE' => 'Просмотр заказов поставщиков',
        'HTML' => '<div><strong>Пока реализовано отображение заказов только для континента</strong></div>
            '
    ]
];

/*
 * Создаем форму для редактирвания параметров модуля
 */
$tabControl = new CAdminTabControl(
    'tabControl',
    $aTabs
);

$tabControl->begin();
?>
    <form action="<?= $APPLICATION->getCurPage(); ?>?mid=<?= $module_id; ?>&lang=<?= LANGUAGE_ID; ?>" method="post">
        <?= bitrix_sessid_post(); ?>
        <?php
        foreach ($aTabs as $aTab) { // цикл по вкладкам
            if ($aTab['HTML']) {
                $tabControl->beginNextTab();
                echo $aTab['HTML'];
            } else if ($aTab['OPTIONS']) {
                $tabControl->beginNextTab();
                __AdmSettingsDrawList($module_id, $aTab['OPTIONS']);
            }
        }
        $tabControl->buttons();
        ?>
    </form>

<?php
$tabControl->end();

$page = 1;
$limit = 100;
$offset = 0;
if(isset($_REQUEST['PAGEN_1'])){
    $page = str_replace("page-", "", $_REQUEST['PAGEN_1']);
    if($page == "all"){
        $page = 1;
        $limit = 1000;
    }
    $offset = ($page-1) * $limit;
}
$oMain = new Main();
if (!empty($_GET["date_from"]) && !empty($_GET["date_to"])) {
    $dateFrom = new DateTime(date_create_from_format('d.m.Y', $_GET["date_from"])->format('d.m.Y 00:00:00'));
    $dateTo = new DateTime(date_create_from_format('d.m.Y', $_GET["date_to"])->format('d.m.Y 23:59:59'));
}
if (!empty($_GET["order_id"])) {
    $orderId = (int) $_GET["order_id"];
}
if (!empty($_GET["status_id"])) {
    $statusOrders = (int) $_GET["status_id"];
}

$orders = $oMain->getOrders($limit, $offset, $dateFrom, $dateTo, $orderId, $statusOrders);
$field["name"] = "date";
?>
    <form class="orders-ajax-container" action = "" method="get" id="orders-ajax-container">
        <input type = "hidden" name = "lang" value="ru">
        <input type = "hidden" name = "mid" value="custom.suppliersorders">
        <table class = "filters">
            <td class="adm-filter-item-left">Фильтр по дате:</td>
            <td class="adm-filter-item-center">
                <div class="adm-calendar-block adm-filter-alignment">
                    <div class="adm-filter-box-sizing">
                        <div class="adm-input-wrap adm-calendar-inp adm-calendar-first"
                             style="display: inline-block;"><input readonly type="text"
                                                                   class="adm-input adm-calendar-from"
                                                                   id="<?= $field['name']; ?>_from"
                                                                   name="<?= $field['name']; ?>_from"
                                                                   size="15"
                                                                   value="<?=$_GET["date_from"]?>"><span
                                    class="adm-calendar-icon" title="Нажмите для выбора даты"
                                    onclick="BX.calendar({node:this, field:'<?= $field['name']; ?>_from', form: '', bTime: false, bHideTime: false});">

                                                            </span>
                        </div>
                        <span class="adm-calendar-separate" style="display: inline-block;"></span>
                        <div class="adm-input-wrap adm-calendar-second" style="display: inline-block;">
                            <input readonly
                                    type="text" class="adm-input adm-calendar-to"
                                    id="<?= $field['name']; ?>_to" name="<?= $field['name']; ?>_to"
                                    size="15"
                                    value="<?=$_GET["date_to"]?>"><span class="adm-calendar-icon" title="Нажмите для выбора даты"
                                                   onclick="BX.calendar({node:this, field:'<?= $field['name']; ?>_to', form: '', bTime: false, bHideTime: false});"></span>
                        </div>
                    </div>
                </div>
            </td>
        </table>
        <table class = "filters">
            <td class="adm-filter-item-left">Фильтр по ID:</td>
            <td class="adm-filter-item-center">
                <div class="adm-calendar-block adm-filter-alignment">
                    <div class="adm-filter-box-sizing">
                        <div class="adm-input-wrap adm-calendar-inp adm-calendar-first"
                             style="display: inline-block;"><input type="text"
                                                                   class="adm-input adm-calendar-from"
                                                                   id="order_id"
                                                                   name="order_id"
                                                                   size="15"
                                                                   value="<?=$_GET["order_id"]?>">

                        </div>
                    </div>
                </div>
            </td>
        </table>
        <table class = "filters">
            <td class="adm-filter-item-left">Фильтр по Статусу Доставки:</td>
            <td class="adm-filter-item-center">
                <div class="adm-calendar-block adm-filter-alignment">
                    <div class="adm-filter-box-sizing">
                        <div class="adm-input-wrap adm-calendar-inp adm-calendar-first"
                             style="display: inline-block;">
                            <select size="1" name="status_id" id="status_id">
                                <option value="0">Выбрать все статусы</option>
                            <?php
                            $suppliersOrder = new ExternalSuppliersOrderTable();
                            foreach ($suppliersOrder::apiStatusMap as $key => $statusItem) {
                                if (!empty($_GET["status_id"]) && $_GET["status_id"] == $key) echo "<option selected value='".$key."'>".$statusItem['description']."</option>";
                                else echo "<option value='".$key."'>".$statusItem['description']."</option>";
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                </div>
            </td>
        </table>
        <div>
            <input type="submit" class="adm-btn adm-button-suppliers-orders" value="Сформировать отчет">
            <input type="submit" class="adm-btn adm-button-generate-report" value="Скачать отчет">
        </div>
        </div>
    </form>
    <table class = "user-table">
    <tr>
        <th>ID заказа</th>
        <th>Дата и время создания заказа</th>
        <th>Поставщик</th>
        <th>Статус доставки</th>
        <th>Название товара</th>
        <th>Кол-во товара в заказе поставщика</th>
        <th>Кол-во товара в заказе ИМ</th>
        <th>Цена за единицу</th>
    </tr>
    <tr>
<?php
foreach($orders as $order){

    $arrProductInfo = $oMain->getProductsInfo($order);
    $idOrder = $order["ID_ORDER"];
    echo '<tr>';
    echo "<td><a target = '_blank' href = '/bitrix/admin/sale_order_edit.php?ID=$idOrder&lang=ru&filter=Y&set_filter=Y'>".$idOrder."</a></td>";
    echo "<td>".$order["DATE_CREATE"]."</td>";
    echo "<td>".$order["SUPPLIER_CODE"]."</td>";
    echo "<td>".$oMain->getStatusInfo($order["STATUS_ID"])."</td>";

    echo "<td>";
    foreach($arrProductInfo as $idProduct => $product){
        $className = "";
        if($product["quantity"] == 0) {
            $className = "cross";
        }
       echo "<a class = '$className'  target = '_blank' href = '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=114&type=catalog1c77&lang=ru&ID=$idProduct&find_section_section=0&WF=Y'>" . $product["name"] . "</a><br>";
    }
    echo "</td>";
    echo "<td>";
    foreach($arrProductInfo as $idProduct => $product){
        echo $product["quantity"]." ".$product["measure"]."<br>";
    }
    echo "</td>";
    echo "<td>";
    foreach($arrProductInfo as $idProduct => $product){
        echo $product["quantityInBasket"]." ".$product["measure"]."<br>";
    }
    echo "</td>";
    echo "<td>";
    foreach($arrProductInfo as $idProduct => $product){
        echo $product["price"]." ".$product["currency"]."<br>";
    }
    echo "</td>";
    echo '</tr>';
}
?>
</table>
<div class = "totaL-price">
    Итого: <?=$oMain->getPricePosition();?>
</div>
<?php
$nav = new \Bitrix\Main\UI\PageNavigation("PAGEN_1");
$nav->allowAllRecords(true)
    ->setPageSize($limit)
    ->initFromUri();

$nav->setRecordCount($oMain->getCountAllOrders());

global $APPLICATION;

$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $nav,

    ),
    false
);
?>