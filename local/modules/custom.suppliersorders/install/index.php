<?
IncludeModuleLangFile(__FILE__);

class custom_suppliersorders extends CModule
{
    var $MODULE_ID = "custom.suppliersorders";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $MODULE_GROUP_RIGHTS = "Y";

    var $errors;

    function custom_suppliersorders()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->PARTNER_NAME = GetMessage("YARDEX_SALES_SUPPLIERS_COMPANY_NAME");
        $this->PARTNER_URI = GetMessage("YARDEX_SALES_SUPPLIERS_PARTNER_URI");
        $this->MODULE_NAME = GetMessage("YARDEX_SALES_SUPPLIERS_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("YARDEX_SALES_SUPPLIERS_INSTALL_DESCRIPTION");
    }


    function InstallFiles()
    {
        return true;
    }


    function UnInstallFiles()
    {
        return true;
    }
    function DoInstall()
    {
        $this->InstallFiles();
        $this->InstallDB(false);
        return true;
    }

    function DoUninstall()
    {
        $this->UnInstallFiles();
        $this->UnInstallDB();
        return true;
    }

    function InstallDB()
    {
        RegisterModule($this->MODULE_ID);
        return true;
    }

    function UnInstallDB()
    {
        UnRegisterModule($this->MODULE_ID);
        return true;
    }
}
?>