<?
$MESS ['YARDEX_SALES_SUPPLIERS_COMPANY_NAME'] = "yardex";
$MESS ['YARDEX_SALES_SUPPLIERS_PARTNER_URI'] = "https://www.1c-bitrix.ru/";
$MESS ['YARDEX_SALES_SUPPLIERS_INSTALL_NAME'] = "Свой модуль просмотра заказов поставщиков";
$MESS ['YARDEX_SALES_SUPPLIERS_INSTALL_DESCRIPTION'] = "Свой модуль просмотра заказов поставщиков";
$MESS ['YARDEX_SALES_SUPPLIERS_INST_INSTALL_TITLE'] = "Установка модуля";
$MESS ['YARDEX_SALES_SUPPLIERS_INST_UNINSTA_TITLE'] = "Удаление модуля";
$MESS ["YARDEX_SALES_SUPPLIERS_STEP1_INSTALL_MESS_1"] = "Вы уверены что хотите установить модуль?";
$MESS ["YARDEX_SALES_SUPPLIERS_STEP1_INSTALL_MESS_2"] = "Установить";
$MESS ["YARDEX_SALES_SUPPLIERS_SUCC_INST"] = "Модуль успешно установлен";
$MESS ["YARDEX_SALES_SUPPLIERS_WIZARD_LIST"] = "Перейти в список";
$MESS ["YARDEX_SALES_SUPPLIERS_ERR_INST"] = "Ошибки, возникшие при установке";
$MESS ['YARDEX_SALES_SUPPLIERS_BACK_TO_LIST'] = "Вернуться к списку модулей";
$MESS ['YARDEX_SALES_SUPPLIERS_CAUTION_MESS'] = 'Внимание! Модуль будет удален из системы';
$MESS ['YARDEX_SALES_SUPPLIERS_UNINST_MOD'] = "Удалить";
$MESS ['YARDEX_SALES_SUPPLIERS_UNINSTALL_SUCCESS'] = "Мастера модуля";
$MESS ['YARDEX_SALES_SUPPLIERS_UNINSTALL_ERROR'] = "Ошибка деинсталляции модуля";
$MESS ['YARDEX_SALES_SUPPLIERS_BACK_TO_MOD_LIST'] = "Вернуться к списку модулей";
?>