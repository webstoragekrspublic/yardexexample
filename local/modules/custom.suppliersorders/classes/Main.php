<?

namespace Custom\SuppliersOrders;

use Handlers\ExternalSuppliers\ExternalSuppliersOrderTable;
use Bitrix\Main\Type\DateTime;

/**
 * Класс для получения заказов от поставщиков
 */
class Main
{
    /**
     * @var $orders array
     */
    private $orders;
    /**
     * @var $countAllOrders integer
     */
    private $countAllOrders;
    /**
     * @var $arrProductInfo array
     */
    private $arrProductInfo;

    /**
     * @var $pricePositions float
     */
    private $pricePositions = 0;

    function __construct()
    {
    }

    /**
     * @param integer $limit сколько выбрать заказов
     * @param integer $offset смещение для выбора заказов
     * @param DateTime $dateFrom с какой даты выводить заказы
     * @param DateTime $dateTo до какой даты выводить заказы
     * @return array заказы
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getOrders(int $limit, int $offset, $dateFrom, $dateTo, $orderId = null, $statusOrders = 0): array
    {
        $this->orders = [];
        $suppliersOrder = new ExternalSuppliersOrderTable();
        if (!empty($dateFrom) && !empty($dateTo)) {
            $arrFilter = [">=ORDER.DATE_INSERT" => $dateFrom,
                "<=ORDER.DATE_INSERT" => $dateTo];
        } else {
            $arrFilter = [];
        }
        if (!empty($orderId)) {
            $arrFilter[] = ["=ID_ORDER" => $orderId];
        }
        if (!empty($statusOrders) && $statusOrders != 0) {
            $arrFilter[] = ["=STATUS_ID" => $statusOrders];
        }
        $dbList = $suppliersOrder->getList([
                "filter" => ["SUPPLIER_CODE" => "Continent",
                    $arrFilter
                ],
                "order" => ["ID" => "DESC"],
                "limit" => $limit,
                "offset" => $offset,
                "count_total" => 1,
            ]
        );
        if (\Bitrix\Main\Loader::includeModule('sale')) {
            while ($row = $dbList->fetch()) {
                $orderInfo = \Bitrix\Sale\Order::load($row["ID_ORDER"]);
                $dateCreateOrder = $orderInfo->getField('DATE_INSERT')->toString();
                $this->orders[$row["ID_ORDER"]] = $row;
                $this->orders[$row["ID_ORDER"]]["DATE_CREATE"] = $dateCreateOrder;
            }
        }
        $this->countAllOrders = $dbList->getCount();
        return $this->orders;
    }

    /**
     * @param array $order заказ
     * @return array информация о составе заказа
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getProductsInfo(array $order): array
    {
        $this->arrProductInfo = [];
        foreach ($order["PRODUCTS_INFO"] as $product) {
            $idProduct = $product->getIdProduct();
            $dbProduct = \Bitrix\Iblock\ElementTable::getList([
                'filter' => array(
                    "ID" => $idProduct
                ),
                'select' => array(
                    'ID',
                    'NAME'
                )]);
            if ($productInfo = $dbProduct->fetch()) {
                $this->arrProductInfo[$idProduct]["name"] = $productInfo['NAME'];
            }

            if (\Bitrix\Main\Loader::includeModule('catalog')) {
                $dbProduct = \Bitrix\Catalog\ProductTable::getList(array(
                    'filter' => array('=ID' => $idProduct),
                    'select' => array('MEASURE')
                ));
                if ($productInfoMeasure = $dbProduct->fetch()) {
                    $resMeasure = \CCatalogMeasure::getList(array(), array('=ID' => $productInfoMeasure["MEASURE"]));
                    while ($measure = $resMeasure->fetch()) {
                        $this->arrProductInfo[$idProduct]["measure"] = $measure["MEASURE_TITLE"];
                    }
                }

                $dbProductPrices = \Bitrix\Catalog\PriceTable::getList([
                    "select" => ["*"],
                    "filter" => [
                        "=PRODUCT_ID" => $idProduct,
                    ]
                ]);
                if ($priceInfo = $dbProductPrices->fetch()) {
                    $this->arrProductInfo[$idProduct]["currency"] = $priceInfo["CURRENCY"];
                }

            }

            $this->arrProductInfo[$idProduct]["quantity"] = $product->getQuantity();
            $this->arrProductInfo[$idProduct]["price"] = $product->getPrice();

            if (\Bitrix\Main\Loader::includeModule('sale')) {
                $orderInfo = \Bitrix\Sale\Order::load($order["ID_ORDER"]);
                $basket = $orderInfo->getBasket();
                foreach ($basket as $basketItem) {
                    $idBasketProduct = $basketItem->getField("PRODUCT_ID");
                    if ($idBasketProduct == $idProduct) {
                        $this->arrProductInfo[$idProduct]["quantityInBasket"] = $product->getQuantity();
                        $pricePosition = $this->arrProductInfo[$idProduct]["quantityInBasket"] * $this->arrProductInfo[$idProduct]["price"];
                        $this->pricePositions += $pricePosition;
                    }
                }
            }
        }
        return $this->arrProductInfo;
    }

    /**
     * @return int общее кол-во заказов от поставщиков
     */
    public function getCountAllOrders(): int
    {
        return $this->countAllOrders;
    }

    /**
     * @param int $id ид статуса заказа
     * @return string описание статуса
     */
    public function getStatusInfo(int $id): string
    {
        $suppliersOrder = new ExternalSuppliersOrderTable();
        return $suppliersOrder::apiStatusMap[$id]["description"];
    }

    /**
     * @return string сумма позиций по всем заказам
     */
    public function getPricePosition(): string
    {
        $firstKey = array_key_first($this->arrProductInfo);
        return $this->pricePositions . " " . $this->arrProductInfo[$firstKey]["currency"];
    }
}

?>