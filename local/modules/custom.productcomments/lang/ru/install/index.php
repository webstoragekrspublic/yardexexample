<?
$MESS["CUSTOM_PRODUCT_COMMENTS_MODULE_NAME"] = "Свой модуль комментариев";
$MESS["CUSTOM_PRODUCT_COMMENTS_MODULE_DESC"] = "Модуль, заполняющий свою таблицу с комментариями.";
$MESS["CUSTOM_PRODUCT_COMMENTS_PARTNER_NAME"] = "Кастомный компонент";
$MESS["CUSTOM_PRODUCT_COMMENTS_PARTNER_URI"] = "";

$MESS["CUSTOM_PRODUCT_COMMENTS_INSTALL_TITLE"] = "Установка модуля";
$MESS["CUSTOM_PRODUCT_COMMENTS_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
