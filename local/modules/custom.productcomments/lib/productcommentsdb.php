<?php

namespace Custom\ProductComments;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class ProductCommentsDBTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'custom_product_comments';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('ID_USER', array(
                'required' => true,
            )),
            new Entity\IntegerField('ID_PRODUCT', array(
                'required' => true,
            )),
            new Entity\IntegerField('RATING', array(
                'required' => true,
            )),
            new Entity\BooleanField('ACTIVE', array(
                'required' => true,
                'default_value' => true
            )),
            new Entity\BooleanField('MODERATED', array(
                'required' => true,
                'default_value' => true
            )),
            new Entity\TextField('COMMENT', array(
                'required' => true,
            )),

            new Entity\DatetimeField('TIME_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }

    public static function addNewComment($idUser, $idProduct, $comment, $rating)
    {
        $res = false;
        $insertResult = static::add([
            'ID_USER' => $idUser,
            'ID_PRODUCT' => $idProduct,
            'COMMENT' => $comment,
            'RATING' => $rating
        ]);

        if ($insertResult->isSuccess()) {
            $res = $insertResult->getId();
        }
        return $res;
    }

    public static function createDbTable()
    {
        if (!Application::getConnection(ProductCommentsDBTable::getConnectionName())->isTableExists(
            Base::getInstance('\Custom\ProductComments\ProductCommentsDBTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\Custom\ProductComments\ProductCommentsDBTable')->createDbTable();
            Application::getConnection(ProductCommentsDBTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ACTIVE`);');
            Application::getConnection(ProductCommentsDBTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`MODERATED`);');
            Application::getConnection(ProductCommentsDBTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ID_PRODUCT`);');
        }
    }

    public static function dropDbTable()
    {
        if (Application::getConnection(ProductCommentsDBTable::getConnectionName())->isTableExists(
            Base::getInstance('\Custom\ProductComments\ProductCommentsDBTable')->getDBTableName()
        )) {
            Application::getConnection(ProductCommentsDBTable::getConnectionName())->queryExecute('drop table if exists ' . Base::getInstance('\Custom\ProductComments\ProductCommentsDBTable')->getDBTableName());
        }
    }

    public static function getTtlActualComments($productId)
    {
        $ttl = 0;
        $getListParams = [
            'select' => ['CNT'],
            'filter' => [
                'ID_PRODUCT' => (int)$productId,
                'ACTIVE' => 1,
                'MODERATED' => 1
            ],
            'runtime' => [
                new Entity\ExpressionField('CNT', 'COUNT(*)')
            ]
        ];
        $data = self::getItems($getListParams);
        if ($data[0]['CNT']) {
            $ttl = (int)$data[0]['CNT'];
        }
        return $ttl;
    }

    public static function getItemsForRecalcProps($productId)
    {
        $getListParams = [
            'select' => ['ID', 'RATING'],
            'filter' => [
                'ID_PRODUCT' => (int)$productId,
                'ACTIVE' => 1,
                'MODERATED' => 1
            ]
        ];
        return self::getItems($getListParams);
    }

    public static function getItemsForProductPage($productId, $page, $cntPerPage)
    {
        $getListParams = [
            'select' => ['*'],
            'filter' => [
                'ID_PRODUCT' => (int)$productId,
                'ACTIVE' => 1,
                'MODERATED' => 1
            ],
            'order' => ['ID' => 'DESC'],
            'limit' => (int)$cntPerPage,
            'offset' => ($page - 1) * $cntPerPage

        ];
        return self::getItems($getListParams);
    }

    protected static function getItems($getListParams)
    {
        $q = static::getList($getListParams);

        $itemIds = [];
        while ($row = $q->fetch()) {
            $itemIds[] = $row;
        }
        return $itemIds;
    }
}