<?

namespace Custom\ProductComments;

class ProductComments
{
    const CATALOG_IBLOCK_ID = 114;
    const AVERAGE_SCORE_ROUND_PRECISION = 1;
    const COMMENTS_PER_PAGE = 100;
    static protected $lastErrors = [];


    static function addCommentByCurrentUser($idProduct, $comment, $rating)
    {
        self::clearErrors();
        $res = false;
        $userId = \CUser::GetID();
        if ($userId) {
            $rating = (int)$rating;
            $comment = trim($comment);
            if ($idProduct && $rating > 0) {
                if ($comment) {
                    $newCommentId = ProductCommentsDBTable::addNewComment($userId, $idProduct, $comment, $rating);
                    if ($newCommentId) {
                        $res = $newCommentId;
                        self::recalcProductProps($idProduct);
                    } else {
                        self::addError('Что-то пошло не так, перезагрузите страницу.');
                    }
                } else {
                    self::addError('комментарий не может быть пустым.');
                }
            } else {
                self::addError('Что-то пошло не так, перезагрузите страницу.');
            }
        } else {
            self::addError('Оставлять комментарий может только зарегестрированный пользователь.');
        }

        return $res;
    }

    static public function recalcProductProps($idProduct)
    {
        $commentsRating = ProductCommentsDBTable::getItemsForRecalcProps($idProduct);
        $averageScore = 0;
        $commentsNumber = 0;
        if ($commentsRating) {
            $totalScore = 0;
            $commentsNumber = count($commentsRating);
            foreach ($commentsRating as $commentInfo) {
                $totalScore += $commentInfo['RATING'];
            }
            $averageScore = $totalScore / $commentsNumber;
        }
        $averageScore = round($averageScore, self::AVERAGE_SCORE_ROUND_PRECISION);

        //обновляем только нужные свойства (не сбрасывает кеш)
        \CIBlockElement::SetPropertyValuesEx(
            $idProduct,
            self::CATALOG_IBLOCK_ID,
            array(
                'COMMENTS_AVERAGE_SCORE' => [['VALUE' => $averageScore]],
                'COMMENTS_COUNT' => [['VALUE' => $commentsNumber]]
            )
        );
        $el = new \CIBlockElement;
        //обновляем еще раз для сброса кеша
        $el->Update($idProduct, []);
    }

    static public function getActualForProduct($idProduct, $page)
    {
        $comments = ProductCommentsDBTable::getItemsForProductPage($idProduct, $page, self::COMMENTS_PER_PAGE);
        if ($comments) {
            $usersId = [];
            foreach ($comments as $key => $comment) {
                $comments[$key]['~COMMENT'] = $comment['COMMENT'];
                $usersId[] = $comment['ID_USER'];
                $comments[$key]['COMMENT'] = self::normalizeComment($comment['COMMENT']);
            }
            $usersInfo = self::getUserNameByIds($usersId);
            foreach ($comments as $key => $comment) {
                $comments[$key]['USER_INFO'] = isset($usersInfo[$comment['ID_USER']])?$usersInfo[$comment['ID_USER']]:[];
            }
        }


        return $comments;
    }

    static protected function normalizeComment($comment)
    {
        $commentForView = str_replace(PHP_EOL, '<br>', htmlspecialchars($comment));
        return $commentForView;
    }

    static protected function getUserNameByIds($usersId)
    {
        $res = [];
        if ($usersId) {
            $by = 'id';
            $order = 'asc';
            $dbRes = \CUser::GetList($by, $order,
                array("ID" => implode(' | ', $usersId)),
                array("FIELDS" => array("*"))
            );
            while ($arUser = $dbRes->Fetch()) {
                $res[$arUser['ID']] = $arUser;
            }
        }
        return $res;
    }

    static public function getTtlActualComments($idProduct)
    {
        return ProductCommentsDBTable::getTtlActualComments($idProduct);
    }

    static protected function addError($text)
    {
        static::$lastErrors[] = $text;
    }

    static protected function clearErrors()
    {
        static::$lastErrors = [];
    }

    static public function getLastErrors()
    {
        return static::$lastErrors;
    }
}