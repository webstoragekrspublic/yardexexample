<?

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

class custom_productcomments extends CModule
{
    public $exclusionAdminFiles;

    function __construct()
    {
        $arModuleVersion = array();
        include(__DIR__ . "/version.php");

        $this->exclusionAdminFiles = array(
            '..',
            '.',
            'menu.php',
            'operation_description.php',
            'task_description.php'
        );

        $this->MODULE_ID = 'custom.productcomments';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("CUSTOM_PRODUCT_COMMENTS_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("CUSTOM_PRODUCT_COMMENTS_MODULE_DESC");

        $this->PARTNER_NAME = Loc::getMessage("CUSTOM_PRODUCT_COMMENTS_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("CUSTOM_PRODUCT_COMMENTS_PARTNER_URI");

    }

    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot = false)
    {
        if ($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

    //Проверяем что система поддерживает D7
    public function isVersionD7()
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    function InstallDB()
    {
        Loader::includeModule($this->MODULE_ID);
        \Custom\ProductComments\ProductCommentsDBTable::createDbTable();
    }

    function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        Application::getConnection(\Custom\ProductComments\ProductCommentsDBTable::getConnectionName())->
        queryExecute('drop table if exists ' . Base::getInstance('\Custom\ProductComments\ProductCommentsDBTable')->getDBTableName());

        Option::delete($this->MODULE_ID);
    }

    function InstallEvents()
    {
//        \Bitrix\Main\EventManager::getInstance()->registerEventHandler($this->MODULE_ID, 'TestEventD7', $this->MODULE_ID, '\Academy\D7\Event', 'eventHandler');
    }

    function UnInstallEvents()
    {
//        \Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler($this->MODULE_ID, 'TestEventD7', $this->MODULE_ID, '\Academy\D7\Event', 'eventHandler');
    }

    function InstallFiles($arParams = array())
    {
        return true;
    }

    function UnInstallFiles()
    {
        return true;
    }

    function DoInstall()
    {
        global $APPLICATION;

        if ($this->isVersionD7()) {
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
            $this->installPropsToProductIB();

        } else {
            $APPLICATION->ThrowException(Loc::getMessage("CUSTOM_PRODUCT_COMMENTS_INSTALL_ERROR_VERSION"));
        }

        $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_PRODUCT_COMMENTS_INSTALL_TITLE"), $this->GetPath() . "/install/step.php");
    }

    function DoUninstall()
    {
        global $APPLICATION;

        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();

        if ($request["step"] < 2) {
            $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_PRODUCT_COMMENTS_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep1.php");
        } elseif ($request["step"] == 2) {
            $this->UnInstallFiles();
            $this->UnInstallEvents();

            if ($request["savedata"] != "Y")
                $this->UnInstallDB();

            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_PRODUCT_COMMENTS_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep2.php");
        }
    }

    function installPropsToProductIB()
    {
        $productIBId = 114;

        $addProps = [
            [
                "NAME" => "Количество комментариев",
                "ACTIVE" => "Y",
                "SORT" => "2000",
                "CODE" => "COMMENTS_COUNT",
                "PROPERTY_TYPE" => "N",
                "IBLOCK_ID" => $productIBId,
            ],
            [
                "NAME" => "Средний бал комментариев",
                "ACTIVE" => "Y",
                "SORT" => "2001",
                "CODE" => "COMMENTS_AVERAGE_SCORE",
                "PROPERTY_TYPE" => "N",
                "IBLOCK_ID" => $productIBId,
            ],
        ];

        foreach ($addProps as $addPropFields) {
            if ($addPropFields['CODE']) {
                $propInIB = CIBlockProperty::GetList(
                    [],
                    [
                        'CODE' => $addPropFields['CODE'],
                        'IBLOCK_ID' => $productIBId
                    ]
                )->Fetch();
                if (!$propInIB) {
                    $iblockproperty = new CIBlockProperty;
                    $PropertyID = $iblockproperty->Add($addPropFields);
                }
            }
        }
    }
}

