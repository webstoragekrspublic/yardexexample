<?php

namespace ProductsWithoutMoved;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;
use Bitrix\Main;
use PHPMailer\PHPMailer\PHPMailer;


class CsvWithoutMoved
{
    public static $module_id = 'ftden45.listencode';
    public static $codeLisenArraySettings = [];

    public static function parserIdsAreatext ($textArea = '')
    {
        $childsIds = CsvWithoutMoved::getCategories('get_cat_depth');
        //echo "start: ".$textArea.'<br>';
        $idsString = explode(";", $textArea);
        $Ids = []; // чистые id категорий без текста(их названий)
        foreach ($idsString as $string) {
            if (preg_match('/\((.*?)\)/i',$string, $outArrayResult)) {
                //echo $outArrayResult[1]."<br>";
                // если есть в массиве то это большая категория, и добавляем все подкатегории
                // если нет то маленькая
                $childsIdsKeys = array_keys($childsIds);
                if (in_array((int)$outArrayResult[1],$childsIdsKeys)) {
                    //print_r($childsIds);
                    foreach ($childsIds[$outArrayResult[1]] as $id) {
                        $Ids[] = $id;
                    }
                } else {
                    $Ids[] = $outArrayResult[1];
                }
            }
        }
        return $Ids;
    }

    public static function subArrayNotIds ($ids,$notIds)
    {
        return array_diff($ids,$notIds);
    }

    public static function getOptionsProduct ()
    {
        CsvWithoutMoved::$codeLisenArraySettings['emails'] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "emails", "");
        CsvWithoutMoved::$codeLisenArraySettings['field_name_file_output'] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "field_name_file_output", "");
        CsvWithoutMoved::$codeLisenArraySettings['field_product_url'] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "field_product_url", "");
        CsvWithoutMoved::$codeLisenArraySettings['categories_product_ids'] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "categories_product_ids", "");
        CsvWithoutMoved::$codeLisenArraySettings['not_categories_product_ids'] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "not_categories_product_ids", "");
        CsvWithoutMoved::$codeLisenArraySettings['torgovie_marks_product_ids'] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "torgovie_marks_product_ids", "");
        CsvWithoutMoved::$codeLisenArraySettings['not_torgovie_marks_product_ids'] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "not_torgovie_marks_product_ids", "");

        CsvWithoutMoved::$codeLisenArraySettings['osg_prersent'][] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "osg_until_persent_01", "");
        CsvWithoutMoved::$codeLisenArraySettings['osg_prersent'][] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "osg_until_persent_02", "");
        CsvWithoutMoved::$codeLisenArraySettings['osg_prersent'][] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "osg_until_persent_03", "");
        CsvWithoutMoved::$codeLisenArraySettings['osg_discount'][] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "osg_until_discount_01", "");
        CsvWithoutMoved::$codeLisenArraySettings['osg_discount'][] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "osg_until_discount_02", "");
        CsvWithoutMoved::$codeLisenArraySettings['osg_discount'][] = \COption::GetOptionString(CsvWithoutMoved::$module_id, "osg_until_discount_03", "");
    }

    public static function CreateReports()
    {

        CsvWithoutMoved::getOptionsProduct();
        // var
        // var: categories, torgovie marks
        $filterCategoriesTorgMaroks = [];
        $filterCategoriesTorgMaroks['cats_ids'] = CsvWithoutMoved::parserIdsAreatext(CsvWithoutMoved::$codeLisenArraySettings['categories_product_ids']);
        $filterCategoriesTorgMaroks['torg_marks_ids'] = CsvWithoutMoved::parserIdsAreatext(CsvWithoutMoved::$codeLisenArraySettings['torgovie_marks_product_ids']);
        $filterCategoriesTorgMaroks['not_cats_ids'] = CsvWithoutMoved::parserIdsAreatext(CsvWithoutMoved::$codeLisenArraySettings['not_categories_product_ids']);
        $filterCategoriesTorgMaroks['not_torg_marks_ids'] = CsvWithoutMoved::parserIdsAreatext(CsvWithoutMoved::$codeLisenArraySettings['not_torgovie_marks_product_ids']);
        $filterCategoriesTorgMaroks['cats_ids_unic'] = CsvWithoutMoved::subArrayNotIds($filterCategoriesTorgMaroks['cats_ids'],$filterCategoriesTorgMaroks['not_cats_ids']);
        $filterCategoriesTorgMaroks['torg_marks_ids_unic'] = CsvWithoutMoved::subArrayNotIds($filterCategoriesTorgMaroks['torg_marks_ids'],$filterCategoriesTorgMaroks['not_torg_marks_ids']);
        // var: products
        $arSelect = array("ID", "NAME", "ACTIVE", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "IBLOCK_SECTION_ID", "PROPERTY_*");
        $arFilter = array("IBLOCK_ID"=>114, "ACTIVE" => 'Y', "IBLOCK_SECTION_ID" => $filterCategoriesTorgMaroks['cats_ids_unic'], "PROPERTY_1070" => $filterCategoriesTorgMaroks['torg_marks_ids_unic']);
        // var: date
        $dateToday = date('d.m.Y');
        // var: url host
        $currentUrl = (\CMain::IsHTTPS()) ? "https://" : "http://";
        $currentUrl .= $_SERVER["HTTP_HOST"];
        // var: array result
        $items = [];
        $itemsCsv = [];
        $itemsCsvContinent = [];
        $itemsCsvOtherProduct = [];
        // var: debug
        $counter = 0;
        $indexStart = 0;
        $index = 0;
        $ifId = false;
        $idProduct = 23699;
        //список товаров, ОСГ дни, ОСГ %, Дата смерти товара, Старая цена, Размер скидки, Новая акционная цена. дата снятия признака "Без движения"
        $itemsCsvContinent[] = ['ID товара', 'название', 'url', 'Дата смерти товара', 'Срок хранения'];
        $itemsCsvOtherProduct[] = ['ID товара', 'название', 'url', 'Дата смерти товара', 'Срок хранения'];
        $itemsCsvWithoutMoved[] = $itemsCsvNorm[] = ['ID товара', 'название', 'url', 'ОСГ дни', 'ОСГ %', 'Дата смерти товара', 'Старая цена', 'Размер скидки', 'Новая акционная цена', 'дата снятия признака "Без движения"', 'ошибки'];
        // var: выборка товаров
        $resEl = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        // цикл перебора товаров
        while($arEl = $resEl->GetNextElement()){
            // var: зануление
            $itemCsvAll = [];
            $itemCsvError = [];
            $errors = [];
            $mode = 'norm';
            // var: counter
            $index++;
            // parames: Product and Properties
            $arFields = $arEl->GetFields();
            $arProps = $arEl->GetProperties();
            // change: name product
            $arFields["NAME"] = str_replace('&quot;', '"', $arFields["NAME"]);
            // var: url
            $detailPageUrl = $currentUrl.$arFields["DETAIL_PAGE_URL"];
            // var: product data
            // var: shelf life
            $shelfLife = $arProps['SHELF_LIFE']['VALUE'];
            // var: date expiration
            $dateExpirationValue = $arProps['DATE_EXPIRATION']['VALUE'];
            // 2)а если нету даты смерти то их исключать? не расчитывать ОСГ? Да
            if (empty($dateExpirationValue)) {
                $dateExpirationValue = 0;
                $errors['day_of_death'] = 'нету даты истечения срока годности, не наш товар';
                $mode = 'day_of_death';
                $osgDay = 0;
            } else {
                $dateExpiration = explode(".",$dateExpirationValue);
                $dateExpiration3 = date('Y-m-d', mktime(0, 0, 0, $dateExpiration[1], $dateExpiration[0], $dateExpiration[2]));
                $dateExpiration2 = date('d.m.Y', mktime(0, 0, 0, $dateExpiration[1], $dateExpiration[0], $dateExpiration[2]));
                $date1 = new \DateTime($dateToday);$date2 = new \DateTime($dateExpiration3);$interval = $date1->diff($date2);
                // разница между текущей датой и датой истечения срока годности(дата смерти - дата сегоднищная)
                $osgDay = $interval->days;
                $todayTime = strtotime($dateToday);$expireTime = strtotime($dateExpiration2);
                // условие: если дата смерти меньще, либо равно сегоднещней даты, то продукт просрочен
                if ($expireTime < $todayTime) {$osgDay *= -1;$errors['its_freash'] = 'продукт просрочен';}
            }
            // change: убрать все буквы
            $shelfLife = preg_replace("/[a-zA-Zа-яА-Я\s]/iu", "", $shelfLife);
            // var: просрочен или нет
            $dataSign = 'не просрочен';
            // filter: categories
            if (false) if ($arFields['IBLOCK_SECTION_ID'] != 994) {continue;}
            // debug: вывод имени товара, ID категории, ID торговой марки
            if (false) {
                echo "NAME ".$arFields['NAME']."<br>"; // имя товара
                echo "CATEGORY_ID ".$arFields['IBLOCK_SECTION_ID']."<br>"; // id категории
                echo "TORGOVAJAMARKA ".$arFields['PROPERTY_1070']."<br>"; // id торговой марки
            }
            // filter: продукта по id
            if ($ifId && $arFields["ID"] != $idProduct) {continue;}
            // filter: задаеться с какого продукта по какой
            if ($index < $indexStart && $counter != 0) continue;
            if (($counter + $indexStart) <= $index && $counter != 0) break;
            // 1)если нету срока годности то это Континент? Да
            if (empty($shelfLife)) {$shelfLife = 0;$errors['shelf_life'] = "не указан срок годности это Континент";$mode = 'shelf_life';}
            if ($shelfLife == 0) {$osgPersent = 0;$shelfLifeInDays = 0;$shelfLifeInDaysTenPercent = 0;}
            else {
                $shelfLifeInDays = $shelfLife * 30;
                $shelfLifeInDaysTenPercent = round($shelfLifeInDays / 10, 0);
                $osgPersent = number_format(round($osgDay / $shelfLifeInDays * 100, 2),2,',','');
            }
            // взятие параметров продукта Ratio
            $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields["ID"]);
            $discountBool = $arProps['DISCOUNTED']['VALUE'];
            if ($discountBool == 'Y') {$discountBool = true;$discountAmount = $arProps['DISCOUNT_AMOUNT']['VALUE'];}
            $ratio = $arMeasure[$arFields["ID"]]['RATIO'];
            $totalPriceAll = $arProps['TOTAL_PRICE_ALL']['VALUE'];
            $priceNew = $totalPriceAll * $ratio;
            $priceOldWithoutDiscount = $discountBool ? $totalPriceAll * $ratio + $discountAmount * $ratio : 0;
            // 3 условия назначения скидок
            if ($osgPersent <= 0) $osgDiscount = 0;
            elseif ($osgPersent <= CsvWithoutMoved::$codeLisenArraySettings['osg_prersent'][2]) $osgDiscount = CsvWithoutMoved::$codeLisenArraySettings['osg_discount'][2];
            elseif ($osgPersent <= CsvWithoutMoved::$codeLisenArraySettings['osg_prersent'][1]) $osgDiscount = CsvWithoutMoved::$codeLisenArraySettings['osg_discount'][1];
            elseif ($osgPersent <= CsvWithoutMoved::$codeLisenArraySettings['osg_prersent'][0]) $osgDiscount = CsvWithoutMoved::$codeLisenArraySettings['osg_discount'][0];
            elseif ($osgPersent <= 100) $osgDiscount = 0;
            else $osgDiscount = 0;
            $priceOld = number_format($priceNew,2,',',''); // var: цена без скидок
            $priceNewAkcion = number_format(($priceNew * ((100 - $osgDiscount)/100)),2,',',''); // var: цена с процентной скидкой (30%,20%,10%)
            // var: статус без движения
            $withoutMovement = 'прошло меньше 10% ОСГ от даты изготовления';
            $orderUpdate = '';
            if ($osgPersent <= 90) {
                /*
                 * это условие проверяет поле Без Движения у товара
                 * и трансформирует в дату с Годом из 4 цифр вместо 2 (пример: 01.10.23 => 01.10.2023)
                 * */
                $dateWithoutMove = $arProps['WITHOUT_MOVED']['VALUE'];
                if (count(explode(".",$dateWithoutMove)) == 3) {
                    $dateWithoutMoveTime = strtotime($dateWithoutMove);
                    $todayTime = strtotime(date("d.m.Y"));
                } else {
                    $dateWithoutMoveTime = 0;
                    $todayTime = 1;
                }

                /*
                 * этот блок высчитывает товары бездвижения
                 * если товара нет в заказах товара последних 10% от срока годности
                 * то товар помечаеться как без движения
                 * если товару назначена дата в поле $arProps['DATE_WITHOUT_MOVE']['VALUE']
                 * то он без движения до даты которая указана в этом поле
                 * идет проверка на истечение срока Без Движения
                 *
                 * */
                if ($dateWithoutMoveTime < $todayTime) {
                    //echo "срок без движения истек или проверки еще не было";
                    $date = new \DateTimeImmutable(date("Y-m-d H:m:s"));
                    $newDate = $date->sub(new \DateInterval('P'.$shelfLifeInDaysTenPercent.'D'));
                    //echo $newDate->format('Y-m-d H:m:s') . "\n";

                    $rsOrder = \CSaleOrder::GetList(
                        array('ID' => 'DESC'),
                        array(
                            'BASKET_PRODUCT_ID' => $arFields["ID"],
                            ">DATE_UPDATE" => $newDate->getTimestamp(),
                        )
                    );
                    if ($el = $rsOrder->Fetch()) {
                        $orderUpdate = $el['DATE_UPDATE'];
                        //echo "товар в движении, товар присутсвует в заказах";
                        $withoutMovement = 'есть в заказах';
                    } else {
                        //echo "Без движения";
                        $date = new \DateTimeImmutable(date("Y-m-d"));
                        $newDate = $date->add(new \DateInterval('P7D'));
                        $withoutMovement = $newDate->format('d.m.Y');
                        $mode = 'without_moved';
                    }
                    // var: product propery
                    \CIBlockElement::SetPropertyValueCode($arFields["ID"], "WITHOUT_MOVED", $withoutMovement);
                } else {
                    //echo "товар в статусе Без движения до ".$dateWithoutMove;
                    $mode = 'without_moved';
                }

            }

            //$itemsCsv[] = ['ID товара', 'название', 'url', 'ОСГ дни', 'ОСГ %', 'Дата смерти товара', 'Старая цена', 'Размер скидки', 'Новая акционная цена', 'дата снятия признака "Без движения"', 'ошибки'];
            $out_errors = '';
            if (!empty($errors['day_of_death'])) {$out_errors .= $errors['day_of_death'] ."|";}
            if (!empty($errors['shelf_life'])) {$out_errors .= $errors['shelf_life'] ."|";}

            $itemCsvAll = [
                'id' => $arFields["ID"],
                'name' => $arFields["NAME"],
                'url' => $detailPageUrl,
                'osg_day' => $osgDay,
                'osg_persent' => $osgPersent,
                'day_of_death' => $dateExpirationValue == 0 ? 'Исключить нет даты смерти' : $dateExpirationValue,
                'old_price' => $priceOld,
                'osg_discount' => $osgDiscount,
                'new_price_akcion' => $priceNewAkcion,
                'without_move' => $withoutMovement,
                'errors' => $out_errors,
                'other_fields' => "shelfLife:".$shelfLife.","."dateExpirationValue:".$dateExpirationValue.","
            ];
            $itemCsvError = [
                'id' => $arFields["ID"],
                'name' => $arFields["NAME"],
                'url' => $currentUrl.$arFields["DETAIL_PAGE_URL"],
                'day_of_death' => empty($errors['day_of_death']) ? $dateExpirationValue : $errors['day_of_death'],
                'shelf_life' => empty($errors['shelf_life']) ? $arProps['SHELF_LIFE']['VALUE'] : $errors['shelf_life']
            ];
            // разные массивы для разных файлов
            switch ($mode) {
                case 'day_of_death':
                    //echo "нету даты смерти (Дата истечения срока годности)";
                    $itemsCsvOtherProduct[] = $itemCsvError;
                    break;
                case 'shelf_life':
                    //echo "нету срока годности (СГ) Это Континент";
                    $itemsCsvContinent[] = $itemCsvError;
                    break;
                case 'norm':
                    //echo "можн рассчитать ОСГ";
                    $itemsCsvNorm[] = $itemCsvAll;
                    break;
                case 'without_moved':
                    //echo "можн рассчитать ОСГ";
                    $itemsCsvWithoutMoved[] = $itemCsvAll;
                    break;
            }
        }
        CsvWithoutMoved::csvCreate($itemsCsvNorm,'file_norm.csv');
        CsvWithoutMoved::csvCreate($itemsCsvContinent,'file_continent.csv');
        CsvWithoutMoved::csvCreate($itemsCsvOtherProduct,'file_other_product.csv');
        CsvWithoutMoved::csvCreate($itemsCsvWithoutMoved,'file_without_moved.csv');
        CsvWithoutMoved::mail();
    }

    public static function csvCreate ($itemsCsv = [], $name = 'file.csv')
    {
        $content = '';
        $handle = fopen('php://temp', 'r+');
        $header = array_keys($itemsCsv[1]);
        fputcsv($handle, $header, ';', '"');

        foreach ($itemsCsv as $line) {
            foreach ($line as $key => $value) {
                $res = $value;$res = trim($res);$res = str_replace(["\r", "\n"], '', $res);
                $line[$key] = $res;
            }
            fputcsv($handle, $line, ';', '"');
        }
        rewind($handle);
        while (!feof($handle)) {
            $content .= fread($handle, 8192);
        }
        fclose($handle);
        $content = mb_convert_encoding($content, 'WINDOWS-1251', "utf-8");
        $content = str_replace("\n", "\r\n", $content);
        $filePath = \Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/'.$name;
        $file = new \Bitrix\Main\IO\File($filePath);
        $file->putContents($content);
    }

    public static function getCategories ($mod = 'get_cat')
    {
        $iblockId = 114;

        $arSelect = ['ID', "IBLOCK_ID", 'NAME', 'DETAIL_PICTURE', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', "PROPERTY_*"];
        $arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y");
        $resEl = \CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);

        $currentUrl = (\CMain::IsHTTPS()) ? "https://" : "http://";
        $currentUrl .= $_SERVER["HTTP_HOST"];
        $catalogsInfo = [];
        $catalogsInfoWithChilds = [];
        $countProducts = 0;
        while($arEl = $resEl->GetNextElement()) {
            $countProducts++;
            $arFields = $arEl->GetFields();
            if (false) {
                echo $arFields['ID'].'<br>';
                echo $arFields['NAME'].'<br>';
                echo $arFields['DEPTH_LEVEL'].'<br>';
                echo "<pre>";
                print_r($arFields);
                echo "</pre>";
            }
            $space = '';
            if ($arFields['DEPTH_LEVEL'] > 0) {
                for ($i = 0; $i < $arFields['DEPTH_LEVEL'];$i++) {if ($i >= 1) $space .= '-';}
            }
            if ($mod == 'get_cat') $catalogsInfo['ID'][$arFields['ID']."_".$arFields['DEPTH_LEVEL']."_".$arFields['IBLOCK_SECTION_ID']] = $space.$arFields['NAME'];
            else if ($mod == 'get_cat_depth') {
                $catalogsInfo[$arFields['ID']]['DEPTH_LEVEL'] = $arFields['DEPTH_LEVEL'];
                $catalogsInfo[$arFields['ID']]['IBLOCK_SECTION_ID'] = $arFields['IBLOCK_SECTION_ID'];
            }
        }
        if  ($mod == 'get_cat_depth') {
            $flag = false;
            foreach ($catalogsInfo as $idCat => $params) {
                foreach ($catalogsInfo as $idCatChild => $paramsChild) {
                    if ($idCat == $paramsChild['IBLOCK_SECTION_ID']) {
                        $catalogsInfoWithChilds[$idCat][] = $idCatChild;
                        $flag = true;
                    }
                }
                if ($flag) break;
            }
            return $catalogsInfoWithChilds;
        }
        return $catalogsInfo;
    }

    public static function getTorgovieMarks()
    {
        $iblockId = 118;

        $arSelect = ['ID', "IBLOCK_ID", 'NAME', 'DETAIL_PICTURE', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', "PROPERTY_*"];
        $arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y");
        $resEl = \CIBlockElement::GetList(array('left_margin' => 'asc'),$arFilter);

        $currentUrl = (\CMain::IsHTTPS()) ? "https://" : "http://";
        $currentUrl .= $_SERVER["HTTP_HOST"];
        $torgovieMarki = [];
        while($arEl = $resEl->GetNextElement()) {
            $countProducts++;
            $arFields = $arEl->GetFields();
            $arProps = $arEl->GetProperties();
            if (false) {
                echo $arFields['ID'].'<br>';
                echo $arFields['NAME'].'<br>';
                echo "<pre>";
                echo "</pre>";
            }
            $torgovieMarki['ID'][$arFields['ID']."_0_0"] = $arFields['NAME'];
        }
        return $torgovieMarki;
    }

    public static function mbConvert ($string)
    {
        return mb_convert_encoding($string, 'WINDOWS-1251', "utf-8");
    }

    public static function mail()
    {
        $path1 = '/home/v/vikuloet/sibshop.b2c/public_html/upload/order_excel_file/';
        $fileName1 = 'file_norm.csv';
        $fileName2 = 'file_continent.csv';
        $fileName3 = 'file_other_product.csv';
        $fileName4 = 'file_without_moved.csv';
        $dateNow = date('d_m_Y_H_i_s');
        $fileName1New = 'file_norm_'.$dateNow.'.csv';
        $fileName2New = 'file_continent_'.$dateNow.'.csv';
        $fileName3New = 'file_other_product_'.$dateNow.'.csv';
        $fileName4New = 'file_without_moved_'.$dateNow.'.csv';
        $dateNow = date('d.m.Y H:i:s');
        $subject = "Tovari OSG, create Report: ";
        $emails = explode(",",CsvWithoutMoved::$codeLisenArraySettings['emails']);
        $mail = new \PHPMailer\PHPMailer\PHPMailer;
        $mail->setFrom('ftforest640@gmail.com', 'Denis Kolosov');
        foreach ($emails as $email) $mail->addAddress(CsvWithoutMoved::mbConvert($email), 'User');
        $mail->Subject = $subject.$dateNow;
        $mail->msgHTML(
            'Denis Kolosov <a href="mailto:kolosovd@sibkon.ru?subject=Quest for OSG">kolosovd@sibkon.ru</a>'.
            "Date Create ".$dateNow
        );
        // Attach uploaded files
        $mail->addAttachment($path1.$fileName1,$fileName1New);
        $mail->addAttachment($path1.$fileName2,$fileName2New);
        $mail->addAttachment($path1.$fileName3,$fileName3New);
        $mail->addAttachment($path1.$fileName4,$fileName4New);
        //$mail->addAttachment($filename2);
        //$r = $mail->send();
        if(!$mail->send()) {
            //echo 'Message could not be sent.';
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            //echo 'Message has been sent';
        }
    }

}