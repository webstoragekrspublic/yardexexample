<?php

//namespace ;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;

\Bitrix\Main\Loader::includeModule('iblock');
Bitrix\Main\Loader::includeModule("sale");


$module_id = 'ftden45.listencode'; //обязательно, иначе права доступа не работают!

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);


\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

## выборка категорий
$iblockId = 114;

$arSelect = ['ID', "IBLOCK_ID", 'NAME', 'DETAIL_PICTURE', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', "PROPERTY_*"];
$arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y");
$resEl = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);

$currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";
$currentUrl .= $_SERVER["HTTP_HOST"];
$catalogsInfo = [];
while($arEl = $resEl->GetNextElement()) {
    $countProducts++;
    $arFields = $arEl->GetFields();
    $arProps = $arEl->GetProperties();
    if (false) {
        echo $arFields['ID'].'<br>';
        echo $arFields['NAME'].'<br>';
        echo $arFields['DEPTH_LEVEL'].'<br>';
        echo "<pre>";
        //print_r($arFields);
        echo "</pre>";
    }
    $catalogsInfo[$arFields['ID']] = $arFields['NAME'];
}
## Выбор Торговой Марки
$iblockId = 118;

$arSelect = ['ID', "IBLOCK_ID", 'NAME', 'DETAIL_PICTURE', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', "PROPERTY_*"];
$arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y");
$resEl = CIBlockElement::GetList(array('left_margin' => 'asc'),$arFilter);

$currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";
$currentUrl .= $_SERVER["HTTP_HOST"];
$torgovieMarki = [];
while($arEl = $resEl->GetNextElement()) {
    $countProducts++;
    $arFields = $arEl->GetFields();
    $arProps = $arEl->GetProperties();
    if (false) {
        echo $arFields['ID'].'<br>';
        echo $arFields['NAME'].'<br>';
        echo $arFields['DEPTH_LEVEL'].'<br>';
        echo "<pre>";
        //print_r($arFields);
        echo "</pre>";
    }
    $torgovieMarki[$arFields['ID']] = $arFields['NAME'];
}
##

#Описание опций

//Asset::getInstance()->addJs('/bitrix/js/'.$module_id.'/scripts.js');

$options = ['modul_id' => $module_id, 'key1' => 'value1', 'key2' => 'value2'];
$options = json_encode($options);

Asset::getInstance()->addString(
    "<script id='".str_replace('.','-',$module_id)."-params' data-params='".$options."'></script>",
    true
);
Asset::getInstance()->addJs('/local/modules/'.$module_id.'/install/assets/scripts/scripts.js');

$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('LISEN_CODE_TAB_SETTINGS'),
        'OPTIONS' => array(
            /*array('field_name_file_output', Loc::getMessage('LISEN_CODE_FIELD_NAME_FILE_EXPORT'),
                '',
                array('text', 50)),
            array('field_product_url', Loc::getMessage('LISEN_CODE_FIELD_PRODUCT_URL'),
                '/tovary.php?ELEMENT_ID=',
                array('text', 50)),
            array('field_text', Loc::getMessage('LISEN_CODE_FIELD_TEXT_TITLE'),
                '',
                array('textarea', 10, 50)),*/
            array('categories_product', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_CATEGORY'),
                '',
                array('selectbox',$catalogsInfo,'onclick="work_with_row()"')),
            array('categories_product_ids', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_CATEGORY_IDS'),
                '',
                array('textarea', 10, 50)),
            array('torgovie_marks_product', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_TORGOVIE_MARKS'),
                '',
                array('selectbox',$torgovieMarki)),
            array('torgovie_marks_product_ids', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_TORGOVIE_MARKS_IDS'),
                '',
                array('textarea', 10, 50)),
        )
    ),
    array(
        "DIV" => "edit2",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),
);
echo $default_group = COption::GetOptionString($module_id, "categories_product_ids", "");
#Сохранение

$name_file_output = 'file.json';
$edadeal_settings = [];
if ($request->isPost() && $request['Export'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            if ($optionName == 'field_name_file_output') $edadeal_settings['field_name_file_output'] = $optionValue = $request->getPost($optionName);
            else if ($optionName == 'field_product_url') $edadeal_settings['field_product_url'] = $optionValue = $request->getPost($optionName);
            else $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }

    // Mobile Code start ftden45

    function print_color ($item,$die = true) {
        echo "<pre>";
        echo "st_block";
        print_r($item);
        echo "</pre>";
        echo "end_";
        if ($die) die();
    }



    $debug_mode = false;

    $arSelect = Array("ID", "NAME", "ACTIVE", "DETAIL_PAGE_URL", "DETAIL_PICTURE", "IBLOCK_SECTION_ID", "PROPERTY_*");
// Фильтровать только по изменненым за последний час товарам
    $arPropsField = ['DATE_EXPIRATION','SHELF_LIFE']; // Дата окончания срока годности | срок годности
    $dateToday = date('d.m.Y'); // текущая дата
    //echo $dateToday."<br>";
    //echo CDataBase::FormatDate($dateToday, "DD.MM.YYYY", "YYYY-MM-DD")."<br>"; // 2005-12-31
    $hourSeconds = 17 * 24 * 60 * 60;

    $dateTime = new \DateTime();
    $dateTime->setTimestamp(time());
    $beforeTimeStamp = $dateTime->getTimestamp() - $hourSeconds;
    $beforeDateTime = new \DateTime();
    $beforeDateTime->setTimestamp($beforeTimeStamp);
//$beforeDateTime = \DateTime::createFromFormat('!d.m.Y',"{$beforeDateTime->format('d')}.{$beforeDateTime->format('m')}.{$beforeDateTime->format('Y')}");
    $dateCreateFrom = \Bitrix\Main\Type\DateTime::createFromTimestamp($beforeDateTime->getTimestamp());

    $currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";

    $currentUrl .= $_SERVER["HTTP_HOST"];

    $arFilter = Array("IBLOCK_ID"=>114, "ACTIVE" => 'Y');
    $resEl = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    $items = [];
    $itemsCsv = [];
    $itemsCsvContinent = [];
    $itemsCsvOtherProduct = [];
    $counter = 0;
    $indexStart = 0;
    $index = 0;
    $ifId = false;
    $idProduct = 23699;
    //$filePath = \Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/file_'.date("Y_m_d_H_m_s").'.csv';
    $filePath2 = \Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/file2.csv';
    //список товаров, ОСГ дни, ОСГ %, Дата смерти товара, Старая цена, Размер скидки, Новая акционная цена. дата снятия признака "Без движения"
    $itemsCsvContinent[] = ['ID товара', 'название', 'url', 'Дата смерти товара', 'Срок хранения'];
    $itemsCsvOtherProduct[] = ['ID товара', 'название', 'url', 'Дата смерти товара', 'Срок хранения'];
    $itemsCsvNorm[] = ['ID товара', 'название', 'url', 'ОСГ дни', 'ОСГ %', 'Дата смерти товара', 'Старая цена', 'Размер скидки', 'Новая акционная цена', 'дата снятия признака "Без движения"', 'ошибки'];



    while($arEl = $resEl->GetNextElement()){
        $itemCsvAll = [];
        $itemCsvError = [];
        $errors = [];
        $arFields = $arEl->GetFields();
        $arProps = $arEl->GetProperties();
        if ($ifId && $arFields["ID"] != $idProduct) {
            continue;
        }
        //echo $arFields["ID"]."<br>";
        $index++;
        if ($index < $indexStart && $counter != 0) continue;
        if (($counter + $indexStart) <= $index && $counter != 0) break;

        $currency = "RUB";

        $imgSrc = CFile::GetPath($arFields["DETAIL_PICTURE"]);

        $arCatalogRes = CCatalogProduct::GetByID($arFields["ID"]);
        $available = false;
        if($arCatalogRes["QUANTITY"] > 0){
            $available = true;
        }

        $price = CPrice::GetBasePrice($arFields["ID"]);
        $price = $price["PRICE"];

        $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields["ID"]);
        //$resRatio = \CCatalogMeasureRatio::getList(Array(), array('IBLOCK_ID'=>114, 'PRODUCT_ID'=>$arFields["ID"]), false, false, array());
        //$arFieldsRatio = $resRatio->Fetch();

        if($arMeasure[$arFields["ID"]]['RATIO'] != 1){
            $price = $price * $arMeasure[$arFields["ID"]]['RATIO'];
        }

        $arFields["NAME"] = str_replace('&quot;', '"', $arFields["NAME"]);

        $shelfLife = $arProps['SHELF_LIFE']['VALUE'];
        $shelfLife = preg_replace("/[a-zA-Zа-яА-Я\s]/iu", "", $shelfLife);

        $dataSign = 'не просрочен'; // просрочен или нет
        /*
         * 1)если нету срока годности то это Континент? Да
         * 2)а если нету даты смерти то их исключать? не расчитывать ОСГ? Да
         * */
        $dateExpirationValue = $arProps['DATE_EXPIRATION']['VALUE']; // 2)
        if (empty($dateExpirationValue)) {
            $dateExpirationValue = 0;
            $dataSign = 'нету даты истечения срока годности';
            $errors['day_of_death'] = 'нету даты истечения срока годности, не наш товар';
            $mode = 'day_of_death';
            //continue;
        }
        else {
            $dateExpiration = explode(".",$dateExpirationValue);
            $dateExpiration3 = date('Y-m-d', mktime(0, 0, 0, $dateExpiration[1], $dateExpiration[0], $dateExpiration[2]));
            $dateExpiration2 = date('d.m.Y', mktime(0, 0, 0, $dateExpiration[1], $dateExpiration[0], $dateExpiration[2]));
            //echo "<br>";
            $date1 = new DateTime($dateToday);
            $date2 = new DateTime($dateExpiration3);
            $interval = $date1->diff($date2);
            //echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";
            //echo "difference " . $interval->days . " days ";
            //echo CDataBase::CompareDates($dateToday, $dateExpiration2);
            $todayTime = strtotime($dateToday);
            //echo "<br>";
            $expireTime = strtotime($dateExpiration2);
            //echo "<br>";

            if ($expireTime < $todayTime) {
                $dataSign = 'продукт просрочен';
                $errors['its_freash'] = 'продукт просрочен';
                //echo "дата с минусом, товар просрочен";
            }
        }

        $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields["ID"]);

        if (empty($shelfLife)) {
            $shelfLife = 0;
            $errors['shelf_life'] = "не указан срок годности это Континент";
            $mode = 'shelf_life';
            //continue;
        }
        if ($dateExpirationValue == 0) $osgDay = 0;else $osgDay = $interval->days;
        if ($dataSign == 'продукт просрочен') $osgDay *= -1;

        $shelfLifeInDays = $shelfLife * 30;
        $shelfLifeInDaysTenPercent = round($shelfLifeInDays / 10, 0);
        if ($shelfLife == 0) $osgPersent = 0;
        elseif ($dateExpirationValue == 0) $osgPersent = 0;
        else $osgPersent = number_format(round($osgDay / $shelfLifeInDays * 100, 2),2,',','');

        if ($osgPersent <= 0) $osgDiscount = 0;
        elseif ($osgPersent <= 31) $osgDiscount = 30;
        elseif ($osgPersent <= 45) $osgDiscount = 20;
        elseif ($osgPersent <= 60) $osgDiscount = 10;
        elseif ($osgPersent <= 100) $osgDiscount = 0;
        else $osgDiscount = 0;

        //получить заказ с определенным товаром
        $withoutMovement = 'прошло меньше 10% ОСГ от даты изготовления';
        $orderUpdate = '';
        if ($osgPersent <= 90) {

            //$originalDate = $dateExpiration3;//"2010-03-21";// дата истечения срока
            //$startDateOrders = date("Y-m-d H:m:s", strtotime(date('Y-m-d')));
            /*
             * это условие проверяет поле Без Движения у товара
             * и трансформирует в дату с Годом из 4 цифр вместо 2 (пример: 01.10.23 => 01.10.2023)
             * */
            $dateWithoutMove = '';
            if (isset($arProps['DATE_WITHOUT_MOVE']['VALUE'])) {
                $dateWithoutMove = explode(".",$arProps['DATE_WITHOUT_MOVE']['VALUE']);
                $dateWithoutMove = date('d.m.Y', mktime(0, 0, 0, $dateWithoutMove[1], $dateWithoutMove[0], $dateWithoutMove[2]));
            }
            $todayTime = strtotime($dateToday);
            $dateWithoutMoveTime = strtotime($dateWithoutMove);

            /*
             * этот блок высчитывает товары бездвижения
             * если товара нет в заказах товара последних 10% от срока годности
             * то товар помечаеться как без движения
             * если товару назначена дата в поле $arProps['DATE_WITHOUT_MOVE']['VALUE']
             * то он без движения до даты которая указана в этом поле
             * идет проверка на истечение срока Без Движения
             *
             * */
            if ($dateWithoutMoveTime > $todayTime || $dateWithoutMove == '') {
                //echo "срок без движения истек или проверки еще не было";
                $date = new DateTimeImmutable(date("Y-m-d H:m:s"));
                $newDate = $date->sub(new DateInterval('P'.$shelfLifeInDaysTenPercent.'D'));
                //echo $newDate->format('Y-m-d H:m:s') . "\n";

                $rsOrder = CSaleOrder::GetList(
                        array('ID' => 'DESC'),
                        array(
                                'BASKET_PRODUCT_ID' => $arFields["ID"],
                                ">DATE_UPDATE" => $newDate->getTimestamp(),
                            )
                );
                if ($el = $rsOrder->Fetch()) {
                    $orderUpdate = $el['DATE_UPDATE'];
                    //print_color($el['DATE_UPDATE']);
                    //echo "товар в движении, товар присутсвует в заказах";
                    $withoutMovement = 'есть в заказах';
                } else {
                    //echo "Без движения";
                    //$withoutMovement = 'Без движения до ';
                    $date = new DateTimeImmutable(date("Y-m-d"));
                    $newDate = $date->add(new DateInterval('P7D'));
                    $withoutMovement = $newDate->format('d.m.y');
                }
            } else {
                echo "товар в статусе Без движения до ".$dateWithoutMove;
            }

        }

        $item = [
            'id' => $arFields["ID"],
            'name' => $arFields["NAME"],
            'ACTIVE' => $arFields["ACTIVE"],
            'price' => $price,
            'currency' => $currency,
            'url' => $currentUrl.$arFields["DETAIL_PAGE_URL"],
            'picture' => $currentUrl.$imgSrc,
            'available' => $available,
            //'categories' => [$arFields["IBLOCK_SECTION_ID"]],
            'stock_quantity' => $arCatalogRes["QUANTITY"],
            /*"params" => [
                [
                    "name" => "Быстрая доставка",
                    "value" => [$arProps['FAST_DELIVERY']['VALUE']]
                ],
                [
                    "name" => "Коэффициент единицы измерения",
                    "value" => [$arMeasure[$arFields["ID"]]['RATIO']]
                ],
                [
                    "name" => "measure",
                    "value" => [$arMeasure[$arFields["ID"]]['MEASURE']['SYMBOL_RUS']],
                ],
                [
                    "name" => "quantity",
                    "value" => [$arCatalogRes["QUANTITY"]]
                ],
                [
                    "name" => "ratio",
                    "value" => [$arMeasure[$arFields["ID"]]['RATIO']]
                ]
            ],*/
            'without_movement' => $withoutMovement,
            'IT_FREASH' => $dataSign,
            'DATE_EXPIRATION' => $dateExpirationValue, // дата истечения срока
            'SHELF_LIFE' => $shelfLife, // срок годности в месяцах
            'SHELF_LIFE_ORIGINAL' => $arProps['SHELF_LIFE']['VALUE'], // срок годности в месяцах
            'SHELF_LIFE_IN_DAYS' => $shelfLifeInDays, // срок годности в днях
            'PERSENT_TEN_DATE' => $shelfLifeInDaysTenPercent,
            'OSG_DAY' => $osgDay, // остаток СГ в днях= 17.10.2022 - 12.10.2022 = 5 дней.
            'OSG_PERSENT' => $osgPersent, // Далее считаем % ОСГ= 5/14*100 = 35,7%
            'OSG_DISCOUNT' => $osgDiscount, // Если ОСГ до 60% = установить скидку 10%
                                             // Если ОСГ до 45% = установить скидку 20%
                                             // Если ОСГ до 31% = установить скидку 30%
            //'arFields' => $arFields,
            'ratio' => $arMeasure[$arFields["ID"]]['RATIO'],
            'price_new_' => $arProps['TOTAL_PRICE_ALL']['VALUE']*$arMeasure[$arFields["ID"]]['RATIO'],
            //'arProps' => $arProps,
        ];
        if ($arProps['DISCOUNTED']['VALUE'] == 'Y') {
            $item["price_old_"] = $arProps['TOTAL_PRICE_ALL']['VALUE']*$arMeasure[$arFields["ID"]]['RATIO']
                + $arProps['DISCOUNT_AMOUNT']['VALUE']*$arMeasure[$arFields["ID"]]['RATIO'];
        }
        $items[] = $item;

        //$itemsCsv[] = ['ID товара', 'название', 'url', 'ОСГ дни', 'ОСГ %', 'Дата смерти товара', 'Старая цена', 'Размер скидки', 'Новая акционная цена', 'дата снятия признака "Без движения"', 'ошибки'];
        $out_errors = '';
        //if (!empty($errors['its_freash'])) {$out_errors .= "!!!".$errors['its_freash'] ."!!!|";}
        if (!empty($errors['day_of_death'])) {$out_errors .= $errors['day_of_death'] ."|";}
        if (!empty($errors['shelf_life'])) {$out_errors .= $errors['shelf_life'] ."|";}

        $itemCsvAll = [
            'id' => $arFields["ID"],
            'name' => $arFields["NAME"],
            'url' => $currentUrl.$arFields["DETAIL_PAGE_URL"],
            'osg_day' => $osgDay,
            'osg_persent' => $osgPersent,
            'day_of_death' => $dateExpirationValue == 0 ? 'Исключить нет даты смерти' : $dateExpirationValue,
            'old_price' => number_format($item['price_new_'],2,',',''),
            'osg_discount' => $osgDiscount,
            'new_price_akcion' => number_format(($item['price_new_'] * ((100 - $osgDiscount)/100)),2,',',''),
            'without_move' => $withoutMovement,
            'errors' => $out_errors,
            'other_fields' => "shelfLife:".$shelfLife.","."dateExpirationValue:".$dateExpirationValue.","
        ];
        $itemCsvError = [
            'id' => $arFields["ID"],
            'name' => $arFields["NAME"],
            'url' => $currentUrl.$arFields["DETAIL_PAGE_URL"],
            'day_of_death' => empty($errors['day_of_death']) ? $dateExpirationValue : $errors['day_of_death'],
            'shelf_life' => empty($errors['shelf_life']) ? $arProps['SHELF_LIFE']['VALUE'] : $errors['shelf_life']
        ];
        //$itemsCsv[] = [$arFields["ID"], $arFields["NAME"], $currentUrl.$arFields["DETAIL_PAGE_URL"], $osgDay, $osgPersent, $dateExpirationValue, $item['price_new_'], $osgDiscount, (($item['price_new_'] * ((100 - $osgDiscount)/100))), $withoutMovement];
        if ($debug_mode) {
            if(!empty($errors['day_of_death']) || !empty($errors['shelf_life']))
            $itemsCsv[] = $itemCsvError;
        } else {

        }
        switch ($mode) {
            case 'date_of_death':
                echo "нету даты смерти (Дата истечения срока годности)";
                $itemsCsvOtherProduct[] = $itemCsvAll;
                break;
            case 'shelf_life':
                echo "нету срока годности (СГ) Это Континент";
                $itemsCsvContinent[] = $itemCsvAll;
                break;
            case 'norm':
                echo "можн рассчитать ОСГ";
                $itemsCsvNorm[] = $itemCsvAll;
                break;
        }

        //$itemsCsvItem = [$arFields["ID"],$arFields["NAME"],$arFields["ACTIVE"]];

    }

    print_color(count($itemsCsvNorm), false);
    //print_color($items);
    //print_color($itemsCsv);
    //print_color($itemsCsv, false);

    function csvCreate ($itemsCsv = [], $name = 'file.csv') {

        //Приводим поля к формату который может прочитать 1с (т.к. стандартный формат csv она не понимает)
        function csvRFC1c7Field($fieldValue)
        {
            $res = $fieldValue;
            $res = trim($res);
            $res = str_replace(["\r", "\n"], '', $res);
            return $res;
        }
        //Приводим вывод к формату который может прочитать 1с (т.к. стандартный формат csv она не понимает)
        function csvRFC1c7Content($content)
        {
            $content = mb_convert_encoding($content, 'WINDOWS-1251', "utf-8");
            $content = str_replace("\n", "\r\n", $content);
            return $content;
        }
        $content = '';
        $handle = fopen('php://temp', 'r+');
        $header = array_keys($itemsCsv[1]);
        fputcsv($handle, $header, ';', '"');

        foreach ($itemsCsv as $line) {
            foreach ($line as $key => $value) {
                $line[$key] = csvRFC1c7Field($value);
            }
            fputcsv($handle, $line, ';', '"');
        }

        rewind($handle);
        while (!feof($handle)) {
            $content .= fread($handle, 8192);
        }
        fclose($handle);
        $content = csvRFC1c7Content($content);
        //return $content;
        $filePath = \Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/'.$name;
        $file = new \Bitrix\Main\IO\File($filePath);
        $file->putContents($content);
    }


    $num = 1;
    $string_csv = '';
    $string_csv_lines = [];
    $string_csv_lines_string = '';
    $file = new \Bitrix\Main\IO\File($filePath);
    if ($num == 0) {
        foreach ($itemsCsv as $item) {
            $string_csv_lines[] = implode(';',$item).";";
        }
        foreach ($string_csv_lines as $item) {
            //echo $item;
            $string_csv_lines_string .= $item."\r\n";
        }
        $content = mb_convert_encoding($string_csv_lines_string, 'WINDOWS-1251', "utf-8");
        $file->putContents($content);

    }elseif ($num == 1) {

    }
    $file->putContents($content);




    //$content = str_replace("\n", "\r\n", $content);



    /*$csvFile->LoadFile(\Bitrix\Main\Application::getDocumentRoot().'/upload/order_excel_file/file.csv');
    $csvFile->SetDelimiter(',');
    while ($arRes = $csvFile->Fetch()) {
        echo($arRes);
    }*/

    // Mobile Code end ftden45

}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<? $tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='LISEN_CODE_settings'>

    <? foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):?>
        <? $tabControl->BeginNextTab(); ?>
        <? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

    <?      endif;
        endforeach; ?>

    <?
    $tabControl->BeginNextTab();



    $tabControl->Buttons(); ?>

    <div class="notes" id="notes">
    <input type="submit" name="Export" value="<?echo Loc::getMessage('LISEN_CODE_EXPORT_EDADEAL')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>" onclick="work_with_row()">
    <?=bitrix_sessid_post();?>
</form>
<script type="text/javascript">
    function work_with_row(){
        alert('zzzzzz');
    }
</script>
<? $tabControl->End(); ?>

