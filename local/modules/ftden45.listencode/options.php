<?php



use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;
use ProductsWithoutMoved\CsvWithoutMoved;

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule("sale");


$module_id = 'ftden45.listencode'; //обязательно, иначе права доступа не работают!

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);


\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

## выборка категорий
$catalogsInfo = [];
$catalogsInfo = CsvWithoutMoved::getCategories();


## Выбор Торговой Марки
$torgovieMarki = [];
$torgovieMarki = \ProductsWithoutMoved\CsvWithoutMoved::getTorgovieMarks();
##

#Описание опций

$options = ['modul_id' => $module_id, 'key1' => 'value1', 'key2' => 'value2'];
$options = json_encode($options);

Asset::getInstance()->addString(
    "<script id='".str_replace('.','-',$module_id)."-params' data-params='".$options."'></script>",
    true
);
Asset::getInstance()->addJs('/local/modules/'.$module_id.'/install/assets/scripts/scripts.js');

$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('LISEN_CODE_TAB_SETTINGS'),
        'OPTIONS' => array(
            array('emails', Loc::getMessage('LISEN_CODE_FIELD_TEXT_EMAILS'),
                'ftforest640@gmail.com,kolosovd@sibkon.ru',
                array('text', 50)),
            array('osg_until_persent_01', Loc::getMessage('LISEN_CODE_FIELD_TEXT_OSG_UNTIL_PERSENT_01'),
                '60',
                array('text', 50)),
            array('osg_until_discount_01', Loc::getMessage('LISEN_CODE_FIELD_TEXT_DISCAUNT_01'),
                '10',
                array('text', 50)),
            array('osg_until_persent_02', Loc::getMessage('LISEN_CODE_FIELD_TEXT_OSG_UNTIL_PERSENT_02'),
                '45',
                array('text', 50)),
            array('osg_until_discount_02', Loc::getMessage('LISEN_CODE_FIELD_TEXT_DISCAUNT_02'),
                '20',
                array('text', 50)),
            array('osg_until_persent_03', Loc::getMessage('LISEN_CODE_FIELD_TEXT_OSG_UNTIL_PERSENT_03'),
                '31',
                array('text', 50)),
            array('osg_until_discount_03', Loc::getMessage('LISEN_CODE_FIELD_TEXT_DISCAUNT_03'),
                '30',
                array('text', 50)),

            array('categories_product', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_CATEGORY'),
                '',
                array('selectbox',$catalogsInfo['ID'])),
            array('categories_product_ids', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_CATEGORY_IDS'),
                '',
                array('textarea', 10, 50)),
            array('torgovie_marks_product', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_TORGOVIE_MARKS'),
                '',
                array('selectbox',$torgovieMarki['ID'])),
            array('torgovie_marks_product_ids', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_TORGOVIE_MARKS_IDS'),
                '',
                array('textarea', 10, 50)),

            array('not_categories_product', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_NOT_CATEGORY'),
                '',
                array('selectbox',$catalogsInfo['ID'])),
            array('not_categories_product_ids', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_NOT_CATEGORY_IDS'),
                '',
                array('textarea', 10, 50)),
            array('not_torgovie_marks_product', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_NOT_TORGOVIE_MARKS'),
                '',
                array('selectbox',$torgovieMarki['ID'])),
            array('not_torgovie_marks_product_ids', Loc::getMessage('LISEN_CODE_FIELD_CHOSE_NOT_TORGOVIE_MARKS_IDS'),
                '',
                array('textarea', 10, 50)),
        )
    ),
    array(
        "DIV" => "edit2",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),
);

#Сохранение

$name_file_output = 'file.json';
$codeLisenArraySettings = [];
if ($request->isPost() && $request['Export'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }

}

if ($request->isPost() && $request['WriteFile'] && check_bitrix_sessid()) {

    foreach ($aTabs as $aTab) {
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption) {
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
        }
    }

    // Mobile Code start ftden45
    \ProductsWithoutMoved\CsvWithoutMoved::CreateReports();

    /*
     Новое сообщение с сайта #SITE_NAME#!

    Дата создания:#CUSTOMER_DATE_RAPORT#

    Кол-во товаров в файле: #CUSTOMER_COUNT#

    Детали отчета:
    #CUSTOMER_DETAIL#
     * */
    /*$SITE_ID = 's1';
    $EVENT_TYPE = 'GOODS_OSG_AND_WITHOUT_MOVE';
    $arFeedForm = array(
        "CUSTOMER_DATE_RAPORT" => htmlspecialcharsEx(date('d.m.Y H:m:s')),
    );
    $result = CEvent::Send($EVENT_TYPE, $SITE_ID, $arFeedForm );*/

    //echo json_encode($result);
    //================================================

    // Mobile Code end ftden45

}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

//Приводим поля к формату который может прочитать 1с (т.к. стандартный формат csv она не понимает)
function csvRFC1c7Field($fieldValue)
{
    $res = $fieldValue;$res = trim($res);$res = str_replace(["\r", "\n"], '', $res);return $res;
}
//Приводим вывод к формату который может прочитать 1с (т.к. стандартный формат csv она не понимает)
function csvRFC1c7Content($content)
{
    $content = mb_convert_encoding($content, 'WINDOWS-1251', "utf-8");
    $content = str_replace("\n", "\r\n", $content);
    return $content;
}

?>
<? $tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='LISEN_CODE_settings'>

    <? foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):?>
        <? $tabControl->BeginNextTab(); ?>
        <? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

    <?      endif;
        endforeach; ?>

    <?
    $tabControl->BeginNextTab();



    $tabControl->Buttons(); ?>

    <div class="notes" id="notes">
    <input type="submit" name="Export" value="<?echo Loc::getMessage('LISEN_CODE_EXPORT_EDADEAL')?>">
    <input type="submit" name="WriteFile" value="<?echo Loc::getMessage('LISEN_CODE_WRITE_FILE')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
    <?=bitrix_sessid_post();?>
</form>
<? $tabControl->End(); ?>

