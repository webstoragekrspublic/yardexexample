<?

Bitrix\Main\Loader::registerAutoloadClasses(
    'ftden45.listencode',
    array(
        'ProductsWithoutMoved\\CsvWithoutMoved' => 'lib/CsvWithoutMoved.php',
        'PHPMailer' => 'lib/PHPMailer/PHPMailer.php',
    )
);