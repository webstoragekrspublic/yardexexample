<?php


$MESS["LISEN_CODE_TAB_SETTINGS"] = "Основные настройки";

$MESS["LISEN_CODE_FIELD_CHOSE_CATEGORY"] = "Выбор категории";
$MESS["LISEN_CODE_FIELD_CHOSE_CATEGORY_IDS"] = "Выбранные категории IDS";
$MESS["LISEN_CODE_FIELD_CHOSE_TORGOVIE_MARKS"] = "Выбор Торговые Марки";
$MESS["LISEN_CODE_FIELD_CHOSE_TORGOVIE_MARKS_IDS"] = "Выбранные Торговые Марки IDS";

$MESS["LISEN_CODE_FIELD_CHOSE_NOT_CATEGORY"] = "Выбор категории которые хотите исключить";
$MESS["LISEN_CODE_FIELD_CHOSE_NOT_CATEGORY_IDS"] = "Выбранные категории IDS которые будут исключены";
$MESS["LISEN_CODE_FIELD_CHOSE_NOT_TORGOVIE_MARKS"] = "Выбор Торговые Марки которые хотите исключить";
$MESS["LISEN_CODE_FIELD_CHOSE_NOT_TORGOVIE_MARKS_IDS"] = "Выбранные Торговые Марки IDS которые будут исключены";

$MESS["LISEN_CODE_EXPORT_EDADEAL"] = "Сохранить Настройки";
$MESS["LISEN_CODE_WRITE_FILE"] = "Пересчитать ОСГ";
$MESS["LISEN_CODE_FIELD_NAME_FILE_EXPORT"] = "Задайте имя файла json\n(если не задан то имя будет file.json)\n(Файл находиться по пути upload/edadeal_json/file.json )";
$MESS["LISEN_CODE_FIELD_PRODUCT_URL"] = "Задайте ссылку на элементы инфоблока Товары (символьный код: ed_products)";

$MESS["LISEN_CODE_FIELD_TEXT_OSG_UNTIL_PERSENT_01"] = "(Устанавливать ОСГ по убыванию) Если ОСГ до 60%";
$MESS["LISEN_CODE_FIELD_TEXT_DISCAUNT_01"] = "То установить скидку 10%";
$MESS["LISEN_CODE_FIELD_TEXT_OSG_UNTIL_PERSENT_02"] = "(Устанавливать ОСГ по убыванию) Если ОСГ до 45%";
$MESS["LISEN_CODE_FIELD_TEXT_DISCAUNT_02"] = "То установить скидку 20%";
$MESS["LISEN_CODE_FIELD_TEXT_OSG_UNTIL_PERSENT_03"] = "(Устанавливать ОСГ по убыванию) Если ОСГ до 31%";
$MESS["LISEN_CODE_FIELD_TEXT_DISCAUNT_03"] = "То установить скидку 30%";