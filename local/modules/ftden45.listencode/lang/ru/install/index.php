<?
/**
 * Created by PhpStorm
 * User: Sergey Pokoev
 * www.pokoev.ru
 * @ Академия 1С-Битрикс - 2015
 * @ academy.1c-bitrix.ru
 */
$MESS["LISEN_CODE_MODULE_NAME"] = "Модуль Просмотра данных Кода";
$MESS["LISEN_CODE_MODULE_DESC"] = "Модуль Просмотра данных Кода";
$MESS["LISEN_CODE_PARTNER_NAME"] = "ftden45";
$MESS["LISEN_CODE_PARTNER_URI"] = "http://ftden45.ru/";

$MESS["LISEN_CODE_DENIED"] = "Доступ закрыт";
$MESS["LISEN_CODE_READ_COMPONENT"] = "Доступ к компонентам";
$MESS["LISEN_CODE_WRITE_SETTINGS"] = "Изменение настроек модуля";
$MESS["LISEN_CODE_FULL"] = "Полный доступ";

$MESS["LISEN_CODE_INSTALL_TITLE"] = "Установка модуля";
$MESS["LISEN_CODE_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";

#работа с .settings.php
$MESS["LISEN_CODE_INSTALL_COUNT"] = "Количество установок модуля: ";
$MESS["LISEN_CODE_UNINSTALL_COUNT"] = "Количество удалений модуля: ";

$MESS["LISEN_CODE_NO_CACHE"] = 'Внимание, на сайте выключено кеширование!<br>Возможно замедление в работе модуля.';
#работа с .settings.php
?>