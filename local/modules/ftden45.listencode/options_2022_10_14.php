<?php

//namespace ;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\IO;
use Bitrix\Main\Application;

use \Bitrix\Iblock\Elements\ElementEdConditionsTable as EdConditions;
use \Bitrix\Iblock\Elements\ElementEdProductsTable as EdProducts;
use \Bitrix\Iblock\Elements\ElementEdRegionsTable as EdRegions;
use \Bitrix\Sale\Order;
use Helpers\CsvHelper;
use Helpers\CustomTools;


\Bitrix\Main\Loader::includeModule('iblock');
Bitrix\Main\Loader::includeModule("sale");


$module_id = 'ftden45.listencode'; //обязательно, иначе права доступа не работают!

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);


\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций

$aTabs = array(
    /*array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('LISEN_CODE_TAB_SETTINGS'),
        'OPTIONS' => array(
            array('field_name_file_output', Loc::getMessage('LISEN_CODE_FIELD_NAME_FILE_EXPORT'),
                '',
                array('text', 50)),
            array('field_product_url', Loc::getMessage('LISEN_CODE_FIELD_PRODUCT_URL'),
                '/tovary.php?ELEMENT_ID=',
                array('text', 50)),
            /*array('field_text', Loc::getMessage('LISEN_CODE_FIELD_TEXT_TITLE'),
                '',
                array('textarea', 10, 50)),
            array('field_list', Loc::getMessage('LISEN_CODE_FIELD_LIST_TITLE'),
                '',
                array('multiselectbox',array('var1'=>'var1','var2'=>'var2','var3'=>'var3','var4'=>'var4'))),
        )
    ),
    array(
        "DIV" => "edit2",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),*/
);
#Сохранение

$name_file_output = 'file.json';
$edadeal_settings = [];
if ($request->isPost() && $request['Export'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            if ($optionName == 'field_name_file_output') $edadeal_settings['field_name_file_output'] = $optionValue = $request->getPost($optionName);
            else if ($optionName == 'field_product_url') $edadeal_settings['field_product_url'] = $optionValue = $request->getPost($optionName);
            else $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }

    // Mobile Code start ftden45

    function print_color ($item,$die = true) {
        echo "<pre>";
        echo "st_";
        print_r($item);
        echo "</pre>";
        echo "end_";
        if ($die) die();
    }

    function getOrdersId($deliveryDateFrom,$deliveryDateTo)
    {
        $period = new DatePeriod(
            new \DateTime($deliveryDateFrom),
            new DateInterval('P1D'),
            new \DateTime($deliveryDateTo." 23:59:59")
        );

        $dates = array();
        foreach ($period as $key => $value) {
            $dates[] = $value->format('d.m.Y');
        }

        //print_color($dates,false);
        $arSelect = ['ID', 'ORDER_ID', 'CODE', 'VALUE'];
        $filter = [
            'VALUE' => $dates,
        ];
        $propsQ = \Bitrix\Sale\PropertyValueCollection::getList(
            [
                'select' => $arSelect,
                'filter' => $filter,
                'order' => ['ID' => 'ASC'],
            ]
        );
        $orderId = [];
        //$ordersVib = [];
        while ($row = $propsQ->fetch()) {
            //$ordersVib[] = $row['ID']."_".$row['ORDER_ID']."_".$row['CODE']."_".$row['VALUE'];
            $orderId[] = $row['ORDER_ID'];
        }
        return $orderId;
    }

    $ordersFilter = [];
    //$ordersDeliveryDateId = getOrdersId('02.08.2022');
    $timeFrom = '02.08.2022';
    $timeTo = '03.08.2022';

    $ordersDeliveryDateId = getOrdersId($timeFrom,$timeTo);
    print_color($ordersDeliveryDateId,false);
    //$ordersDeliveryDateId = [];
        //print_color($ordersDeliveryDateId);

    //$orderFilter['=DELIVERY_DATE'] = '';
    //$orderFilter['=ID'] = [13590,13517];


    $arrayOrdersId = [];
    if (!empty($ordersDeliveryDateId) && !empty($ordersFilter['=ID'])) {
        for ($i=0; $i<count($ordersFilter['=ID']); $i++) {
            for ($j=0; $j<count($ordersDeliveryDateId); $j++) {
                if ($ordersFilter['=ID'][$i] == $ordersDeliveryDateId[$j]) {
                    $arrayOrdersId[] = $ordersDeliveryDateId[$j];
                }
            }
        }
        $ordersFilter['=ID'] = $arrayOrdersId;
    } elseif (empty($ordersFilter['=ID']) && !empty($ordersDeliveryDateId)) {
        $ordersFilter['=ID'] = $ordersDeliveryDateId;
    }

    function getReportROG(array $ordersFilter): array
    {
        //print_color($ordersFilter['=ID']);
        $orders = getOrdersRowsForReport(
            $ordersFilter,
            ['ID', 'USER_ID', 'PAY_SYSTEM_ID', 'DELIVERY_ID', 'PAYED', 'CANCELED', 'STATUS_ID', 'PRICE', 'DATE_INSERT', 'COMMENTS', 'USER_DESCRIPTION']
        );
        //print_color($orders);
        if (!$orders) {
            return [];
        }
        enrichOrdersWithProperties($orders, ['ADDRESS','PHONE','DELIVERY_DATE', 'DELIVERY_TIME', 'TOTAL_ORDERS', 'SKU_QUANTITY', 'MANAGER_COMMENT_FOR_USER','FIO']);
        print_color($orders);
        $this->enrichOrdersWithCoupons($orders);
        $this->enrichOrdersWithUserInfo($orders);
        $orders = CustomTools::indexArrayByKey($orders, 'ID');

        $currentProductsInOrders = $this->getCurrentProductsInOrders($orders);
        if (!$currentProductsInOrders) {
            return [];
        }

        $reportArr = [];

        foreach ($currentProductsInOrders as $idOrder => $basket) {

            $orderInfo = $orders[$idOrder];
            //if ($orderInfo['PROPERTIES']['DELIVERY_DATE'] != '02.08.2022') continue;

            //'CODE' => 'DELIVERY_DATE',
            //'VALUE' => '02.08.2022',
            $dateInsert = $orderInfo['DATE_INSERT']->format("d.m.Y H:i:s");
            $couponsName = [];
            foreach ($orderInfo['COUPONS'] as $coupon) {
                $couponsName[] = $coupon['COUPON'];
            }

            $orderPriceWithoutDiscount = 0;
            $orderPricePurchase = 0;
            foreach ($basket as $item) {
                $orderPriceWithoutDiscount += $item['BASE_PRICE'] * $item['QUANTITY'];
                $orderPricePurchase += $this->getPurchasingPrice($item['PROPERTY_PURCHASING_PRICE_VALUE'],$item['PRICE']) * $item['QUANTITY'];
            }

            foreach ($basket as $item) {
                $row = $item;

                $dbElement = \CIBlockElement::GetByID($item['PRODUCT_ID']);

                if($arElement = $dbElement->GetNext()){
                    $dbSection = \CIBlockSection::GetByID($arElement["IBLOCK_SECTION_ID"]);
                    if($arSection = $dbSection->GetNext()){
                        $sectionName = $arSection['NAME'];
                    }
                    $dbProps = \CIBlockElement::GetProperty(114, $item['PRODUCT_ID'], array(), Array("CODE"=>"TORGOVAJAMARKA"));
                    if($arProps = $dbProps->Fetch()){
                        $manufacturerID = $arProps["VALUE"];
                    }
                    if($manufacturerID != NULL) {
                        $dbManufacturer = \CIBlockElement::GetByID($manufacturerID);
                        if ($arManufacturer = $dbManufacturer->GetNext()) {
                            $manufacturerName = $arManufacturer["NAME"];
                        }
                    }
                    else{
                        $manufacturerName = "";
                    }
                    $dbProps = \CIBlockElement::GetProperty(114, $item['PRODUCT_ID'], array(), Array("CODE"=>"SUPPLIER_ID"));
                    if($arProps = $dbProps->Fetch()){
                        $supplier = $arProps["VALUE"];
                    }
                }

                $row['GROUP_PRODUCT'] = $sectionName;
                $row['MANUFACTURER'] = $manufacturerName;
                $row['SUPPLIER'] = $supplier;

                $row['PRODUCT_ID'] = $item['PRODUCT_ID'];
                $row['ORDER_ID'] = $idOrder;
                $row['CUSTOMER_ID'] = $orderInfo['USER_ID'];
                $row['PAYMENT_NAME'] = $this->getPaymentName($orderInfo['PAY_SYSTEM_ID']);
                $row['DELIVERY_NAME'] = $this->getDeliveryName($orderInfo['DELIVERY_ID']);
                $row['PAYED'] = $this->getYesNoField($orderInfo['PAYED']);
                $row['CANCELED'] = $orderInfo['CANCELED'];
                $row['STATUS_NAME'] = $this->getStatusName($orderInfo['STATUS_ID']);

                $row['ORDER_PRICE'] = $orderInfo['PRICE'];

                $row['ORDER_PRICE_WITHOUT_DISCOUNT'] = $orderPriceWithoutDiscount;
                $row['ORDER_PRICE_PURCHASE'] = $orderPricePurchase;

                $row['ORDER_DATE_INSERT'] = $dateInsert;
                $row['DELIVERY_TIME'] = $orderInfo['PROPERTIES']['DELIVERY_TIME'];
                $row['DELIVERY_DATE'] = $orderInfo['PROPERTIES']['DELIVERY_DATE'];
                $row['TOTAL_ORDERS'] = $orderInfo['PROPERTIES']['TOTAL_ORDERS'];
                $row['PHONE'] = $orderInfo['PROPERTIES']['PHONE'];
                $row['ADDRESS'] = $orderInfo['PROPERTIES']['ADDRESS'];
                $row['COUPONS_NAME'] = $couponsName;
                $row['SKU_QUANTITY'] = $orderInfo['PROPERTIES']['SKU_QUANTITY'];
                $row['USER_DESCRIPTION'] = $orderInfo['USER_DESCRIPTION'];
                $row['COMMENTS'] = $orderInfo['COMMENTS'];
                $row['MANAGER_COMMENT_FOR_USER'] = $orderInfo['PROPERTIES']['MANAGER_COMMENT_FOR_USER'];
                $row['FIO'] = $orderInfo['PROPERTIES']['FIO'];
                $row['CUSTOMER'] = implode(' ',[$orderInfo['USER_INFO']['NAME'],$orderInfo['USER_INFO']['LAST_NAME']]);
                $row['PRICE_TTL'] = $item['PRICE'] * $item['QUANTITY'];
                $row['BASE_PRICE_TTL'] = $item['BASE_PRICE'] * $item['QUANTITY'];
                $row['PURCHASING_PRICE'] = $this->getPurchasingPrice($item['PROPERTY_PURCHASING_PRICE_VALUE'],$item['PRICE']);
                $row['PURCHASING_PRICE_TTL'] = $this->getPurchasingPrice($item['PROPERTY_PURCHASING_PRICE_VALUE'],$item['PRICE']) * $item['QUANTITY'];

                $reportArr[] = $row;
            }
        }


        $headers = [
            'ORDER_ID' => [
                'headerName' => 'ID',
                'prepare' => ['rule' => CsvHelper::RULE_ADD_BEFORE, 'value' => '№'],
            ],
            'ORDER_DATE_INSERT' => ['headerName' => 'Дата и время заказа'],
            'STATUS_NAME' => ['headerName' => 'Статус'],
            'DELIVERY_DATE' => ['headerName' => 'Дата доставки'],
            'DELIVERY_TIME' => ['headerName' => 'Время доставки'],


            'FIO' => ['headerName' => 'ФИО'],
            'CUSTOMER' => ['headerName' => 'Покупатель'],
            'MANAGER_COMMENT_FOR_USER' => ['headerName' => 'Комментарий о Клиенте'],

            'TOTAL_ORDERS' => ['headerName' => 'Кол-во заказов'],
            'PHONE' => ['headerName' => 'Телефон'],
            'ADDRESS' => ['headerName' => 'Адрес доставки'],

            'COUPONS_NAME' => [
                'headerName' => 'Купоны заказа',
                'prepare' => ['rule' => CsvHelper::RULE_ARRAY_IMPLODE, 'value' => ', ']
            ],
            'ORDER_PRICE' => [
                'headerName' => 'Сумма',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],

            'SKU_QUANTITY' => ['headerName' => 'Количество SCU'],
            'PAYED' => ['headerName' => 'Оплачен'],

            'USER_DESCRIPTION' => ['headerName' => 'Комментарии покупателя'],
            'COMMENTS' => [
                'headerName' => 'Комментарии',
                'prepare' => ['rule' => CsvHelper::RULE_ADD_BEFORE, 'value' => ' ']
            ],


//            'PAYMENT_NAME' => ['headerName' => 'Платежная система'],
//            'DELIVERY_NAME' => ['headerName' => 'Способ доставки'],

//            'CANCELED' => ['headerName' => 'Заказ отменен'],
//
            'PRODUCT_ID' => ['headerName' => 'Товар ID'],
            'NAME' => ['headerName' => 'Товар Название'],
            'QUANTITY' => [
                'headerName' => 'Товар Количество',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PRICE' => [
                'headerName' => 'Товар Стоимость',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'BASE_PRICE' => [
                'headerName' => 'Товар Стоимость без скидки',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PURCHASING_PRICE' => [
                'headerName' => 'Товар Закуп',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'BASE_PRICE_TTL' => [
                'headerName' => 'стоимость sku без скидки',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PRICE_TTL' => [
                'headerName' => 'стоимость sku',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],
            'PURCHASING_PRICE_TTL' => [
                'headerName' => 'себестоимость sku',
                'prepare' => ['rule' => CsvHelper::RULE_DOT_REPLACE, 'value' => '']
            ],

            'GROUP_PRODUCT' => [
                'headerName' => 'группа товаров'
            ],
            'MANUFACTURER' => [
                'headerName' => 'производитель'
            ],
            'SUPPLIER' => [
                'headerName' => 'код поставщика'
            ],

        ];

        return CsvHelper::toCsvArr($headers, $reportArr);
    }

    function getOrdersRowsForReport($orderFilters, $select = ['ID', 'USER_ID', 'PAY_SYSTEM_ID', 'DELIVERY_ID', 'PAYED', 'CANCELED', 'STATUS_ID', 'PRICE']): array
    {
        return Order::getList(['select' => $select, 'filter' => $orderFilters, 'order' => ['ID' => 'DESC']])->fetchAll();
    }

    function enrichOrdersWithProperties(array &$orders, array $addProps = [])
    {
        $ordersId = array_column($orders, 'ID');
        if (!$ordersId) {
            return;
        }
        $arSelect = ['ID', 'ORDER_ID', 'CODE', 'VALUE'];
        $filter = [
            'ORDER_ID' => $ordersId,
        ];
        if ($addProps) {
            $filter['CODE'] = $addProps;
        }
        $propsQ = \Bitrix\Sale\PropertyValueCollection::getList(
            [
                'select' => $arSelect,
                'filter' => $filter,
                'order' => ['ID' => 'ASC']
            ]
        );
        $props = [];
        while ($row = $propsQ->fetch()) {
            $props[$row['ORDER_ID']]['PROPERTIES'][$row['CODE']] = $row['VALUE'];
        }
        foreach ($orders as $key => $o) {
            $orderId = $o['ID'];
            if (isset($props[$orderId])) {
                $orders[$key] = array_merge($o, $props[$orderId]);
            }
        }
    }

    getReportROG($ordersFilter);

    // Mobile Code end ftden45

}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<? $tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='LISEN_CODE_settings'>

    <? foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):?>
        <? $tabControl->BeginNextTab(); ?>
        <? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

    <?      endif;
        endforeach; ?>

    <?
    $tabControl->BeginNextTab();



    $tabControl->Buttons(); ?>

    <input type="submit" name="Export" value="<?echo Loc::getMessage('LISEN_CODE_EXPORT_EDADEAL')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
    <?=bitrix_sessid_post();?>
</form>
<? $tabControl->End(); ?>
