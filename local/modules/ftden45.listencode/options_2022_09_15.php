<?php

//namespace ;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\IO;
use Bitrix\Main\Application;

use \Bitrix\Iblock\Elements\ElementEdConditionsTable as EdConditions;
use \Bitrix\Iblock\Elements\ElementEdProductsTable as EdProducts;
use \Bitrix\Iblock\Elements\ElementEdRegionsTable as EdRegions;


\Bitrix\Main\Loader::includeModule('iblock');


$module_id = 'ftden45.listencode'; //обязательно, иначе права доступа не работают!

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);


\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций

$aTabs = array(
    /*array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('LISEN_CODE_TAB_SETTINGS'),
        'OPTIONS' => array(
            array('field_name_file_output', Loc::getMessage('LISEN_CODE_FIELD_NAME_FILE_EXPORT'),
                '',
                array('text', 50)),
            array('field_product_url', Loc::getMessage('LISEN_CODE_FIELD_PRODUCT_URL'),
                '/tovary.php?ELEMENT_ID=',
                array('text', 50)),
            /*array('field_text', Loc::getMessage('LISEN_CODE_FIELD_TEXT_TITLE'),
                '',
                array('textarea', 10, 50)),
            array('field_list', Loc::getMessage('LISEN_CODE_FIELD_LIST_TITLE'),
                '',
                array('multiselectbox',array('var1'=>'var1','var2'=>'var2','var3'=>'var3','var4'=>'var4'))),
        )
    ),
    array(
        "DIV" => "edit2",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),*/
);
#Сохранение

$name_file_output = 'file.json';
$edadeal_settings = [];
if ($request->isPost() && $request['Export'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            if ($optionName == 'field_name_file_output') $edadeal_settings['field_name_file_output'] = $optionValue = $request->getPost($optionName);
            else if ($optionName == 'field_product_url') $edadeal_settings['field_product_url'] = $optionValue = $request->getPost($optionName);
            else $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }

    // Mobile Code start ftden45

    //$arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure(1515966223760356150);
    //print_r(\Bitrix\Catalog\ProductTable::getById());

    $hourSeconds = 60 * 60;

    $dateTime = new \DateTime();
    $dateTime->setTimestamp(time());
    $beforeTimeStamp = $dateTime->getTimestamp() - $hourSeconds;
    $beforeDateTime = new \DateTime();
    $beforeDateTime->setTimestamp($beforeTimeStamp);
//$beforeDateTime = \DateTime::createFromFormat('!d.m.Y',"{$beforeDateTime->format('d')}.{$beforeDateTime->format('m')}.{$beforeDateTime->format('Y')}");
    $dateCreateFrom = \Bitrix\Main\Type\DateTime::createFromTimestamp($beforeDateTime->getTimestamp());

    $currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";

    $currentUrl .= $_SERVER["HTTP_HOST"];

    $arSelect = Array("ID", "NAME", "PROPERTY_*");
    $arSelect = Array("*");
    //$arProduct = \Bitrix\Catalog\ProductTable::getList();
    //$arFilter = Array("IBLOCK_ID"=>114, ">DATE_MODIFY_FROM" => $dateCreateFrom->toString());
    $arFilter = Array("IBLOCK_ID"=>114,"ID"=>50260);
    $resEl = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    $items = [];
    $countProducts = 0;
    while($arEl = $resEl->GetNextElement()) {
        $countProducts++;
        //echo $countProducts."<br>";
        //continue;
        $arFields = $arEl->GetFields();
        $arProps = $arEl->GetProperties();
        echo "<pre>";
        print_r($arFields['NAME']);
        print_r($arProps['FAST_DELIVERY']['VALUE']);
        echo "</pre>";
        die();
        if ($arFields["ID"] != 54185) continue;
        echo $arFields["ID"];

        $currency = "RUB";

        $imgSrc = CFile::GetPath($arFields["DETAIL_PICTURE"]);

        $arCatalogRes = CCatalogProduct::GetByID($arFields["ID"]);
        $available = false;
        if($arCatalogRes["QUANTITY"] > 0){
            $available = true;
        }

        $price = CPrice::GetBasePrice($arFields["ID"]);
        //print_r($price);
        $price = $price["PRICE"];

        $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields["ID"]);
        echo "<pre>";
        print_r($arMeasure);
        echo "</pre>";
        break;

        //$resRatio = \CCatalogMeasureRatio::getList(Array(), array('IBLOCK_ID'=>114, 'PRODUCT_ID'=>$arFields["ID"]), false, false, array());
        //$arFieldsRatio = $resRatio->Fetch();

        if($arMeasure[$arFields["ID"]]['RATIO'] != 1){
            $price = $price * $arMeasure[$arFields["ID"]]['RATIO'];
        }

        $arFields["NAME"] = str_replace('&quot;', '"', $arFields["NAME"]);

        $items[] = [
            'id' => $arFields["ID"],
            'name' => $arFields["NAME"],
            'price' => $price,
            'currency' => $currency,
            'url' => $currentUrl.$arFields["DETAIL_PAGE_URL"],
            'picture' => $currentUrl.$imgSrc,
            'available' => $available,
            'categories' => [$arFields["IBLOCK_SECTION_ID"]],
            'stock_quantity' => $arCatalogRes["QUANTITY"],
            "params" => [
                [
                    "name" => "measure",
                    "values" => [(string)$arMeasure[$arFields["ID"]]['MEASURE']['SYMBOL_RUS']],
                ],
                [
                    "name" => "quantity",
                    "values" => [(string)$arCatalogRes["QUANTITY"]]
                ],
                [
                    "name" => "ratio",
                    "values" => [(string)$arMeasure[$arFields["ID"]]['RATIO']]
                ]
            ],
        ];
    }

    $string = '{
    "shop_id": "60eec8bb4851421cbbcdac54907c00",
    "shop_secret": "1a7394f1a45a26b68d4f01751a2fee3b",
    "items": '. json_encode($items,JSON_UNESCAPED_UNICODE).'
}';
    echo $countProducts."<br>";

    echo "<pre>";
    print_r($items);
    print_r($string);
    echo "</pre>";

    // Mobile Code end ftden45

}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<? $tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='LISEN_CODE_settings'>

    <? foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):?>
        <? $tabControl->BeginNextTab(); ?>
        <? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

    <?      endif;
        endforeach; ?>

    <?
    $tabControl->BeginNextTab();



    $tabControl->Buttons(); ?>

    <input type="submit" name="Export" value="<?echo Loc::getMessage('LISEN_CODE_EXPORT_EDADEAL')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
    <?=bitrix_sessid_post();?>
</form>
<? $tabControl->End(); ?>
