<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;


Loc::loadMessages(__FILE__);

class custom_orderpaycheck extends CModule
{
    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {

            $arModuleVersion = array();

            include_once(__DIR__ . "/version.php");

            $this->MODULE_ID            = 'custom.orderpaycheck';
            $this->MODULE_VERSION       = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE  = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME          = Loc::getMessage("ORDER_PAYCHECK_NAME");
            $this->MODULE_DESCRIPTION   = Loc::getMessage("ORDER_PAYCHECK_DESCRIPTION");
            $this->PARTNER_NAME         = Loc::getMessage("ORDER_PAYCHECK_PARTNER_NAME");
        }

        return false;
    }

    public function DoInstall()
    {

        global $APPLICATION;

        if (CheckVersion(ModuleManager::getVersion("main"), "14.00.00")) {

            ModuleManager::registerModule($this->MODULE_ID);
            try {
                $this->InstallFiles();
                $this->InstallEvents();
                $this->InstallDB();
            } catch (\Throwable $th) {
                ModuleManager::unRegisterModule($this->MODULE_ID);
                $APPLICATION->ThrowException(
                    Loc::getMessage("ORDER_PAYCHECK_INSTALL_ERROR_1")
                );
                pr($th->getTraceAsString());
            }
        } else {

            $APPLICATION->ThrowException(
                Loc::getMessage("ORDER_PAYCHECK_INSTALL_ERROR_VERSION")
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("ORDER_PAYCHECK_INSTALL_TITLE") . " \"" . Loc::getMessage("ORDER_PAYCHECK_NAME") . "\"",
            __DIR__ . "/step.php"
        );

        return false;
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallDB();
        $this->UnInstallEvents();
        ModuleManager::unRegisterModule($this->MODULE_ID);


        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("ORDER_PAYCHECK_UNINSTALL_TITLE") . " \"" . Loc::getMessage("ORDER_PAYCHECK_NAME") . "\"",
            __DIR__ . "/unstep.php"
        );

        return false;
    }

    function InstallFiles($arParams = array())
    {
        return true;
    }

    function UnInstallFiles()
    {

        return true;
    }

    function InstallDB()
    {
        Loader::includeModule($this->MODULE_ID);
        \Custom\OrderPaycheck\OrderPaycheckTable::createDbTable();
        \Custom\OrderPaycheck\OrderPaycheckPull::getInstance();
    }

    function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        Application::getConnection(\Custom\OrderPaycheck\OrderPaycheckTable::getConnectionName())->queryExecute(
            'drop table if exists ' . Base::getInstance('\Custom\OrderPaycheck\OrderPaycheckTable')->getDBTableName()
        );

        Option::delete($this->MODULE_ID);
    }


    function InstallEvents()
    {
        \Bitrix\Main\EventManager::getInstance()->registerEventHandler(
            'sale',
            'OnSaleOrderSaved',
            $this->MODULE_ID,
            'Custom\\OrderPaycheck\\OrderPaycheckPull',
            'onNewOrderAdded'
        );
        \Bitrix\Main\EventManager::getInstance()->registerEventHandler(
            'sale',
            'OnSaleOrderPaid',
            $this->MODULE_ID,
            'Custom\\OrderPaycheck\\OrderPaycheckPull',
            'onOrderPaidChange'
        );
    }
    function UnInstallEvents()
    {
        \Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler(
            'sale',
            'OnSaleOrderSaved',
            $this->MODULE_ID,
            'Custom\\OrderPaycheck\\OrderPaycheckPull',
            'onNewOrderAdded'
        );
        \Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler(
            'sale',
            'OnSaleOrderPaid',
            $this->MODULE_ID,
            'Custom\\OrderPaycheck\\OrderPaycheckPull',
            'onOrderPaidChange'
        );
    }

    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot = false)
    {
        if ($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        else
            return dirname(__DIR__);
    }
}
