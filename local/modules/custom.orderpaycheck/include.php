<?php

use Bitrix\Main\Loader;


Loader::registerAutoLoadClasses(
    'custom.orderpaycheck',
    array(
        '\Custom\OrderPaycheck\OrderPaycheckTable' => 'lib/OrderPaycheckTable.php',
        '\Custom\OrderPaycheck\OrderPaycheckPull' => 'lib/OrderPaycheckPull.php',
        '\Custom\OrderPaycheck\OrderPaycheckHelper' => 'lib/OrderPaycheckHelper.php',
    )
);
