<?

namespace Custom\OrderPaycheck;

class OrderPaycheckHelper
{
    protected const AVAILABLE_PAYMENT_SYSTEM = array(25, 33, 34);

    public static function getOrderPaidStatusAndSumFromSber($orderId)
    {
        $order = \Bitrix\Sale\Order::load($orderId);

        if ($order) {
            $paymentCollection = $order->getPaymentCollection();
            $paymentInfo = false;

            foreach ($paymentCollection as $payment) {
                if (in_array($payment->getPaymentSystemId(), static::AVAILABLE_PAYMENT_SYSTEM)) {
                    $currentPaymentOrder = $paymentCollection->current();
                    $isReturned = $payment->isReturn();
                    if (!$isReturned) {
                        $currentPaymentOrderId = $currentPaymentOrder->getField("ID");

                        for ($i = 0; $i < 30; $i++) {
                            $orderNumber = $orderId . "_" . $currentPaymentOrderId . '_' . $i;
                            try {
                                $paymentInfo = \ExternalApi\SberbankPayment\SberbankApi::getOrderPaymentStatus($orderNumber);

                                if ($paymentInfo['orderStatus'] >= 0 && $paymentInfo['orderStatus'] < 6) {
                                    break;
                                }
                            } catch (\Exception $e) {
                                break;
                            }
                        }
                        for ($i = 0; $i < 30; $i++) {
                            $orderNumber = $orderId . "_" . $i;
                            try {
                                $paymentInfo = \ExternalApi\SberbankPayment\SberbankApi::getOrderPaymentStatus($orderNumber);
                                if ($paymentInfo['orderStatus'] >= 0 && $paymentInfo['orderStatus'] < 6) {
                                    break;
                                }
                            } catch (\Exception $e) {
                                break;
                            }
                        }
                    }
                }
            }
            if ($paymentInfo['orderStatus'] == 1 || $paymentInfo['orderStatus'] == 2) {
                return ['STATUS' => 'IS_PAID', 'SUM' => $paymentInfo['amount'] / 100];
            }
            return ['STATUS' => 'NO_PAID'];
        }
        return ['STATUS' => 'NULL'];
    }

    public static function setOrderPaidStatusAndSum($orderId, $status, $sum)
    {
        if ($status === 'Y' || $status === 'N') {
            $order = \Bitrix\Sale\Order::load($orderId);
            $paymentCollection = $order->getPaymentCollection();
            foreach ($paymentCollection as $payment) {
                if (in_array($payment->getPaymentSystemId(), static::AVAILABLE_PAYMENT_SYSTEM)) {
                    if ($status === 'Y' && !$payment->isPaid() || $status === 'N' && $payment->isPaid())
                        $payment->setPaid($status);
                    $payment->setField('SUM', $sum);
                }
            }
            $order->save(); //Возвращает ворнинги из-за повешенных на хук функций,которые не могут отработать, но сама по себе отрабатывает корректно
        }
    }

    public static function checkOrderPaysystemIsSber($orderId)
    {
        $order = \Bitrix\Sale\Order::load($orderId);

        if ($order) {
            $paymentCollection = $order->getPaymentCollection();

            foreach ($paymentCollection as $payment) {
                if (in_array($payment->getPaymentSystemId(), static::AVAILABLE_PAYMENT_SYSTEM)) {
                    return true;
                }
            }
        }
        return false;
    }
}
