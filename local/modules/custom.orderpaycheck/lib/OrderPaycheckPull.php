<?

namespace Custom\OrderPaycheck;

class OrderPaycheckPull
{
    protected static $instance = null;
    protected static $orderPull = [];

    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new self();
        }
        return static::$instance;
    }
    protected function __construct()
    {
        $this->init();
    }

    protected function init()
    {
        static::loadOrderPullFromTable();
    }

    protected static function loadOrderPullFromTable()
    {
        static::$orderPull = \Custom\OrderPaycheck\OrderPaycheckTable::getAllOrders();
    }

    public static function addOrderToPullIfNotExist($orderId)
    {
        if (OrderPaycheckHelper::checkOrderPaysystemIsSber($orderId)) {
            if (empty(static::$orderPull)) {
                static::loadOrderPullFromTable();
            }
            if (!isset(static::$orderPull[$orderId])) {
                $insertData = [
                    'ORDER_ID' => $orderId,
                    'STATUS' => 'NO_PAID',
                ];
                $insert = \Custom\OrderPaycheck\OrderPaycheckTable::add($insertData);
                if ($insert->isSuccess()) {
                    static::$orderPull[$orderId] = $insertData;
                }
            }
        }
    }

    public function onNewOrderAdded(\Bitrix\Main\Event $event)
    {
        $order = $event->getParameter("ENTITY");
        $isNew = $event->getParameter("IS_NEW");
        if ($isNew) {
            static::addOrderToPullIfNotExist($order->getId());
        }
    }
    public function onOrderPaidChange(\Bitrix\Main\Event $event)
    {
        $order = $event->getParameter("ENTITY");
        static::addOrderToPullIfNotExist($order->getId());
    }

    public function removeOrderFromPull($orderId)
    {
        try {
            $delete = \Custom\OrderPaycheck\OrderPaycheckTable::delete(static::$orderPull[$orderId]['ID']);
            if ($delete->isSuccess()) {
                unset(static::$orderPull[$orderId]);
            }
            return true;
        } catch (\Throwable $th) {
            return false;
        }
        return false;
    }

    protected function checkAndCorrectPullItem($pullItem)
    {
        if ($pullItem['STATUS'] != 'IS_PAID') {
            $paymentInfo = \Custom\OrderPaycheck\OrderPaycheckHelper::getOrderPaidStatusAndSumFromSber($pullItem['ORDER_ID']);
            if ($paymentInfo['STATUS'] == 'IS_PAID') {
                \Custom\OrderPaycheck\OrderPaycheckHelper::setOrderPaidStatusAndSum($pullItem['ORDER_ID'], 'Y', $paymentInfo['SUM']);
                \Custom\OrderPaycheck\OrderPaycheckTable::update($pullItem['ID'], [
                    'STATUS' => 'IS_PAID',
                ]);
            } else {
                \Custom\OrderPaycheck\OrderPaycheckHelper::setOrderPaidStatusAndSum($pullItem['ORDER_ID'], 'N', $paymentInfo['SUM']);
            }
        }
    }

    public function checkPullAndCorrectPaidStatus()
    {
        static::loadOrderPullFromTable();
        array_walk(static::$orderPull, ['self', 'checkAndCorrectPullItem']);
    }

    public function removeIsPaidOrdersFromPull()
    {
        static::loadOrderPullFromTable();
        foreach (static::$orderPull as $pullItem) {
            if ($pullItem['STATUS'] == 'IS_PAID') {
                $this->removeOrderFromPull($pullItem['ORDER_ID']);
            }
        }
    }

    public function printPull()
    {
        pr(static::$orderPull);
    }
}
