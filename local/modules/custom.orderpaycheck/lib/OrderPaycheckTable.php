<?php

namespace Custom\OrderPaycheck;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class OrderPaycheckTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'custom_order_paycheck';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('ORDER_ID', array(
                'required' => true,
            )),
            new Entity\StringField('STATUS', array(
                'required' => true,
            )),
        );
    }

    public static function createDbTable()
    {
        if (!Application::getConnection(OrderPaycheckTable::getConnectionName())->isTableExists(
            Base::getInstance('\Custom\OrderPaycheck\OrderPaycheckTable')->getDBTableName()
        )) {
            Base::getInstance('\Custom\OrderPaycheck\OrderPaycheckTable')->createDbTable();
            Application::getConnection(OrderPaycheckTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD UNIQUE (`ORDER_ID`);');
        }
    }

    public static function getAllOrders()
    {
        $q = static::getList(
            [
                'select' => ['ID', 'ORDER_ID', 'STATUS'],
                'filter' => [],
                'order' => [],
            ]
        );

        $result = [];
        while ($row = $q->fetch()) {
            $result[$row['ORDER_ID']] = ['ID' => $row['ID'], 'ORDER_ID' => $row['ORDER_ID'], 'STATUS' => $row['STATUS']];
        }
        return $result;
    }
}
