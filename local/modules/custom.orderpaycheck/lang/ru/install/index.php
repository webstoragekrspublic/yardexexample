<?
$MESS["ORDER_PAYCHECK_NAME"] = "Свой модуль для проверки оплаты заказов";
$MESS["ORDER_PAYCHECK_DESCRIPTION"] = "Осуществляет проверку резервирования и оплаты в СБЕРЕ недавно созданных заказов и заказов у которых менялась оплата";
$MESS["ORDER_PAYCHECK_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["ORDER_PAYCHECK_INSTALL_ERROR_1"] = "Ошибка";
$MESS["ORDER_PAYCHECK_INSTALL_TITLE"] = "Установка модуля";
$MESS["ORDER_PAYCHECK_UNINSTALL_TITLE"] = "Деинсталляция модуля";
$MESS["ORDER_PAYCHECK_PARTNER_NAME"] = "Кастомный компонент";
