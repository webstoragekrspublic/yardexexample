<?php

namespace Custom\FavoritesOnCookies;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class FavoritesTable extends Entity\DataManager
{
    public static function getTableName() {
        return 'custom_favorites_on_cookies';
    }

    public static function getMap() {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('ID_USER', array(
                'required' => true,
            )),
            new Entity\IntegerField('ID_ITEM', array(
                'required' => true,
            )),

            new Entity\DatetimeField('TIME_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }

    public static function createDbTable() {
        if (!Application::getConnection(FavoritesTable::getConnectionName())->isTableExists(
            Base::getInstance('\Custom\FavoritesOnCookies\FavoritesTable')->getDBTableName()
        )
        ) {
            Base::getInstance('\Custom\FavoritesOnCookies\FavoritesTable')->createDbTable();
            Application::getConnection(FavoritesTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD UNIQUE (`ID_ITEM`, `ID_USER`);');
        }
    }

    public static function deleteAllItemIdsForUserId($userId) {
        $userId = (int)$userId;
        if ($userId) {
            $deleteSql = 'DELETE FROM `' . static::getTableName() . '` WHERE `ID_USER` = ' . $userId . ';';
            Application::getConnection(FavoritesTable::getConnectionName())->queryExecute($deleteSql);
        }
    }

    public static function deleteItemIdForUserId($itemId, $userId) {
        $userId = (int)$userId;
        $itemId = (int)$itemId;
        if ($userId and $itemId) {
            $deleteSql = 'DELETE FROM `' . static::getTableName() . '` WHERE `ID_USER` = ' . $userId . ' AND `ID_ITEM` = ' . $itemId . ';';
            Application::getConnection(FavoritesTable::getConnectionName())->queryExecute($deleteSql);
        }
    }

    public static function insertItemIdsForUserId($itemIds, $userId) {
        $userId = (int)$userId;
        if (is_array($itemIds) and $itemIds and $userId) {
            $insertSql = 'INSERT INTO `' . static::getTableName() . '` (`ID_USER`,`ID_ITEM`,`TIME_ADDED`) VALUES ';

            $values = [];
            foreach ($itemIds as $itemId) {
                $values[] = '(' . $userId . ',' . (int)$itemId . ',NOW())';
            }

            $insertSql .= implode(',', $values) . ' ON DUPLICATE KEY UPDATE ID_ITEM=VALUES(ID_ITEM);';

            Application::getConnection(FavoritesTable::getConnectionName())->queryExecute($insertSql);
        }
    }

    public static function getItemIdsForUserId($userId){
        $q = static::getList(
            [
                'select' => ['ID_ITEM'],
                'filter' => ['=ID_USER'=>(int)$userId],
            ]
        );

        $itemIds = [];
        while ($row = $q->fetch()) {
            $itemIds[$row['ID_ITEM']] = $row['ID_ITEM'];
        }
        return $itemIds;
    }
}