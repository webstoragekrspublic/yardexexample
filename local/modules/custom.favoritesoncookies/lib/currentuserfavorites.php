<?

namespace Custom\FavoritesOnCookies;

use Bitrix\Main\Application;
use Bitrix\Main\Web\Cookie;

class CurrentUserFavorites
{
    protected static $instance = null;
    public const COOKIE_NAME = 'FAVORITES';
    public const COOKIE_DELIMITER = '-';
    protected $itemIds = [];

    //todo вынести в настройки модуля
    protected $cookieItemLimits = 30;
    protected $authorizedItemLimits = 200;
    protected $cookieTimeLimits = 60 * 60 * 24 * 60;

    protected function __construct() {
        $this->init();
    }

    protected function init() {
        $this->itemIds = [];
        $itemsFromCookies = $this->getItemsFromCookies();
        if ($this->isAuthorizedType()) {
            $this->itemIds = $this->getItemsFromDB();
            if (count($itemsFromCookies) > 0) {
                foreach ($itemsFromCookies as $itemId) {
                    if (!isset($this->itemIds[$itemId])) {
                        $this->addItemId($itemId);
                    }
                }
            }
        } else {
            $this->itemIds = $itemsFromCookies;
        }
    }

    protected function getItemsFromCookies() {
        $request = Application::getInstance()->getContext()->getRequest();
        $favoritesCookie = trim($request->getCookie(static::COOKIE_NAME));
        $items = [];
        if ($favoritesCookie and $favoritesCookie != '') {
            $favoriteItems = explode(static::COOKIE_DELIMITER, $favoritesCookie);
            foreach ($favoriteItems as $itemId) {
                $itemId = (int)trim($itemId);
                $items[$itemId] = $itemId;
            }
        }
        return $items;
    }

    public function getItemIds() {
        return $this->itemIds;
    }

    protected function getItemsFromDB() {
        $itemIds = [];
        if ($this->isAuthorizedType()) {
            $itemIds = FavoritesTable::getItemIdsForUserId($this->getCurrentUserId());
        }
        return $itemIds;
    }

    public function getTotalCnt() {
        return count($this->itemIds);
    }

    public function removeItemId($itemId) {
        $itemId = (int)$itemId;
        if ($itemId and $this->isFavorite($itemId)) {
            unset($this->itemIds[$itemId]);
            if ($this->isAuthorizedType()) {
                FavoritesTable::deleteItemIdForUserId($itemId, $this->getCurrentUserId());
            }
            $this->setCookies();
        }
    }

    public function isHaveItems() {
        return (bool)$this->getTotalCnt();
    }

    public function isFavorite($itemId) {
        return isset($this->itemIds[$itemId]);
    }

    protected function saveItemsInDb() {
        if ($this->isAuthorizedType()) {
            FavoritesTable::insertItemIdsForUserId($this->itemIds, $this->getCurrentUserId());
        }
    }

    public function addItemId($itemId) {
        $res = false;
        $itemId = (int)$itemId;
        if (!$this->isFavorite($itemId)) {
            if ($itemId && !$this->isLimitIsReachedForItemIds()) {
                $this->itemIds[$itemId] = $itemId;
                if ($this->isAuthorizedType()) {
                    $insertResult = FavoritesTable::add([
                        'ID_USER' => $this->getCurrentUserId(),
                        'ID_ITEM' => $itemId
                    ]);

                    if ($insertResult->isSuccess()) {
                        $res = $insertResult->getId();
                    } else {
                        unset($this->itemIds[$itemId]);
                    }
                } else {
                    $res = $itemId;
                }
                $this->setCookies();
            }
        } else {
            $res = $itemId;
        }
        return $res;
    }

    public function isLimitIsReachedForItemIds() {
        return (count($this->itemIds) >= $this->getItemIdsLimit()) ? true : false;
    }

    public function getItemIdsLimit() {
        return $this->isAuthorizedType() ? $this->authorizedItemLimits : $this->cookieItemLimits;
    }

    public function isAuthorizedType(){
        global $USER;
        return $USER->IsAuthorized();
    }

    protected function getCurrentUserId() {
        global $USER;
        return $USER->GetID();
    }

    public function setActualItemIds($newItemIds) {
        $deleteItemIds = $this->itemIds;
        $addItemIds = [];
        if (is_array($newItemIds)) {
            foreach ($newItemIds as $newItemId) {
                $newItemId = (int)$newItemId;
                if (isset($this->itemIds[$newItemId])) {
                    unset($deleteItemIds[$newItemId]);
                } else {
                    $addItemIds[$newItemId] = $newItemId;
                }
            }
        }

        if ($deleteItemIds) {
            foreach ($deleteItemIds as $deleteItemId) {
                $this->removeItemId($deleteItemId);
            }
        }

        if ($addItemIds){
            foreach ($addItemIds as $addItemId){
                $this->addItemId($addItemId);
            }
        }
    }

    public function setCookies() {
        $cookieValue = '';
        if (!$this->isAuthorizedType()) {
            $cookieValue = $this->getCookieValue();
        }

        $context = Application::getInstance()->getContext();
        $cookie = new Cookie(static::COOKIE_NAME, $cookieValue, time() + $this->cookieTimeLimits);
        $cookie->setDomain($context->getServer()->getHttpHost());
        $cookie->setHttpOnly(false);

        $context->getResponse()->addCookie($cookie);
    }

    public function getCookieValue(){
        $cookieValue = implode(static::COOKIE_DELIMITER, $this->itemIds);
        return $cookieValue;
    }

    /**
     * @return self
     */
    public static function getInstance() {
        if (!static::$instance) {
            static::$instance = new self();
        }
        return static::$instance;
    }
}