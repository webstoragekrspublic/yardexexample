<?
$MESS["CUSTOM_FAVORITES_ON_COOKIES_MODULE_NAME"] = "Свой модуль избаранного";
$MESS["CUSTOM_FAVORITES_ON_COOKIES_MODULE_DESC"] = "Модуль, позволяющий добавлять 'избранное'. Если пользователь не авторизирован то информация добавляется в cookies, после авторизации все добавленые товары записываются в БД(custom_favorites_on_cookies).";
$MESS["CUSTOM_FAVORITES_ON_COOKIES_PARTNER_NAME"] = "Кастомный компонент";
$MESS["CUSTOM_FAVORITES_ON_COOKIES_PARTNER_URI"] = "";

$MESS["CUSTOM_FAVORITES_ON_COOKIES_INSTALL_TITLE"] = "Установка модуля";
$MESS["CUSTOM_FAVORITES_ON_COOKIES_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
