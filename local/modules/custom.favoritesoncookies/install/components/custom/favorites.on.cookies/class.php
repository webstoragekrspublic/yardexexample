<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Loader;
use \Bitrix\Main\Error;
use \Bitrix\Main\Localization\Loc;
use \Custom\FavoritesOnCookies\CurrentUserFavorites;


class CFavoritesOnCookies extends CBitrixComponent
{
    protected function checkModules() {
        if (!Loader::includeModule('custom.favoritesoncookies')) {
            ShowError(Loc::getMessage('CUSTOM_FAVORITES_ON_COOKIES_MODULE_NOT_INSTALLED'));
            return false;
        }

        return true;
    }

    public function executeComponent() {
        if ($this->checkModules()) {
            $this->arResult['TOTAL_CNT'] = CurrentUserFavorites::getInstance()->getTotalCnt();
            $this->includeComponentTemplate();
        }
    }
}
    