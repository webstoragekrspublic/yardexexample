$('body').on("click", '[data-role="favorites.on.cookies"]', function (e) {
    e.preventDefault();
    var pr_id = $(this).attr('data-pr_id');
    if ($(this).hasClass("active")) {
        favoritesOnCookies.DeleteProductId(pr_id);
    } else {
        favoritesOnCookies.AddProductId(pr_id);
    }
});

var favoritesOnCookies = {
    cookie_name: 'BITRIX_SM_FOP_PRODUCTS',
    products_in: [],
    AddProductId: function (product_id) {
        if (product_id) {
            var have_in_arr = false;
            for (var k = 0; k < this.products_in.length; k++) {
                if (this.products_in[k] == product_id) {
                    have_in_arr = true;
                    break;
                }
            }
            if (!have_in_arr) {
                this.products_in.push(product_id);
            }
            this.updateCookies();
            $('[data-role="favorites.on.cookies"]').filter('[data-pr_id="' + product_id + '"]').addClass("active");
        }
    },
    DeleteProductId: function (product_id) {
        if (product_id) {
            this.products_in = this.products_in.filter(function (e) {
                return e != product_id;
            });
            this.updateCookies();
            $('[data-role="favorites.on.cookies"]').filter('[data-pr_id="' + product_id + '"]').removeClass("active");
        }
    },
    updateDisplayInfo:function(){
        $('[data-role="favorites.on.cookies.counter"]').html(this.products_in.length);
        if (this.products_in.length > 0){
            $('[data-role="favorites.on.cookies.counter"]').fadeIn();
        } else {
            $('[data-role="favorites.on.cookies.counter"]').fadeOut();
        }
    },
    updateCookies: function(){
        BX.setCookie(this.cookie_name, this.products_in.join('_'), {expires: 10086400,path:'/'});
        this.updateDisplayInfo();
    },
    isFavorite: function (product_id) {
        if (this.products_in.indexOf(product_id) >= 0)
            return true;
        return false;
    },
    Init: function () {
        this.products_in = [];
        
        var fav_str = BX.getCookie(this.cookie_name);
        if (typeof fav_str !== 'undefined') {
            fav_str = fav_str.trim();
            if (fav_str != '' && fav_str) {
                var fav_arr = fav_str.split('_');
                for (var k = 0; k < fav_arr.length; k++) {
                    this.products_in.push(fav_arr[k].trim());
                }
            }
        }
    },
    SetProducts: function(products){
        this.products_in = [];
        if (typeof products !== 'undefined'){
            if (Array.isArray(products)){
                this.products_in = products;
            }
        }
        this.updateCookies();
    }
};
favoritesOnCookies.Init();