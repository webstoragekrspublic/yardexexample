<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<? if ($arParams['use_mobile_theme'] !== 'Y'): ?>
    <li class = "user-nav__item user-nav__item--favorite">
        <a href = "/personal/favorites/" class = "user-nav__link">
            <span class = "user-nav__link-quantity" style='<?= ($arResult['fav_total'] > 0) ? '' : 'display:none;'; ?>' data-role="favorites.on.cookies.counter">
                <?= $arResult['fav_total']; ?>
            </span>
            Избраное
        </a>
    </li>
<? else: ?>
    <li class="user-nav__item user-nav__item--favorite">
        <a href="/personal/favorites/" class="user-nav__link">
            <span class="user-nav__link-quantity" style='<?= ($arResult['fav_total'] > 0) ? '' : 'display:none;'; ?>' data-role="favorites.on.cookies.counter">
                <?= $arResult['fav_total']; ?>
            </span>
        </a>
    </li>
<? endif; ?>
