<?php
//ini_set('memory_limit', '500M');
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\IO;
use Bitrix\Main\Application;


use Bitrix\Main,
    Bitrix\Catalog;


\Bitrix\Main\Loader::includeModule('iblock');


$module_id = 'custom.edadealexport'; //обязательно, иначе права доступа не работают!

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);


\Bitrix\Main\Loader::includeModule($module_id);


$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Описание опций

$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('FTDEN45_EDADEALEXPJSON_TAB_SETTINGS'),
        'OPTIONS' => array(
            array('field_name_file_output', Loc::getMessage('FTDEN45_EDADEALEXPJSON_FIELD_NAME_FILE_EXPORT'),
                '',
                array('text', 50)),
            array('date_end_conditions', Loc::getMessage('FTDEN45_EDADEALEXPJSON_DATA_END_CONDITIONS'),
                date("d.m.Y"),
                array('text', 50)),
            array('torgovie_mark', Loc::getMessage('FTDEN45_EDADEALEXPJSON_TORGOVAJA_MARKA'),
                '1395,1471',
                array('text', 50)),
            /*array('field_text', Loc::getMessage('FTDEN45_EDADEALEXPJSON_FIELD_TEXT_TITLE'),
                '',
                array('textarea', 10, 50)),
            array('field_list', Loc::getMessage('FTDEN45_EDADEALEXPJSON_FIELD_LIST_TITLE'),
                '',
                array('multiselectbox',array('var1'=>'var1','var2'=>'var2','var3'=>'var3','var4'=>'var4'))),*/
        )
    ),
    array(
        "DIV" => "edit2",
        "TAB" => Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ),
);
#Сохранение

$name_file_output = 'file.json';
$edadeal_settings = [];
if ($request->isPost() && $request['Export'] && check_bitrix_sessid())
{

    foreach ($aTabs as $aTab)
    {
        //Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            if (!is_array($arOption)) //Строка с подсветкой. Используется для разделения настроек в одной вкладке
                continue;

            if ($arOption['note']) //Уведомление с подсветкой
                continue;

            //Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            if ($optionName == 'field_name_file_output') $edadeal_settings['field_name_file_output'] = $optionValue = $request->getPost($optionName);
            else if ($optionName == 'date_end_conditions') $edadeal_settings['date_end_conditions'] = $optionValue = $request->getPost($optionName);
            else if ($optionName == 'torgovie_mark') $edadeal_settings['torgovie_mark'] = $optionValue = $request->getPost($optionName);
            else $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }
    $iblockId = 114;
    // var
    $file_edadeal = []; // final array
    $ed_regions = [];
    $ed_catalogs = [];
    $ed_catalog_item = [];
    $ed_offers = [];
    $ed_offers_id = [];
    $arSelect = ['ID', 'NAME','OFFERS', 'CONDITIONS','IMAGE.FILE','TARGET_REGIONS',
        'DATA_END', 'DATA_START','IS_MAIN', 'DETAIL_PICTURE'];

    $arSelect = ['ID', "IBLOCK_ID", 'NAME', 'DETAIL_PICTURE', 'DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO', "PROPERTY_*"];
    $arSelect = ['*'];
    //37780
    $arFilter = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y"); // aspro_next_content
    $resEl = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);

    $items = [];
    $countProducts = 0;
    $offersId = [];
    $offers = [];
    $currentUrl = (CMain::IsHTTPS()) ? "https://" : "http://";
    $currentUrl .= $_SERVER["HTTP_HOST"];
    $catalogsInfo = [];
    if (true)
    while($arEl = $resEl->GetNextElement()) {
        $countProducts++;
        $arFields = $arEl->GetFields();
        $arProps = $arEl->GetProperties();
        if (false) {
            echo $arFields['ID'].'<br>';
            echo $arFields['NAME'].'<br>';
            echo $arFields['DEPTH_LEVEL'].'<br>';
            echo "<pre>";
            //print_r($arFields);
            echo "</pre>";
        }

        if (isset($edadeal_settings['date_end_conditions'])) $dateEndConditions = $edadeal_settings['date_end_conditions']." 23:59:59";
        else $dateEndConditions = date("d.m.Y")." 23:59:59";
        $dateConditions = DateFromTo($arFields['DATE_CREATE'],$dateEndConditions);



        $catalogsInfo[$arFields['ID']]['conditions'] = $arFields['NAME'];
        $catalogsInfo[$arFields['ID']]['date_end'] = $dateConditions[1];
        $catalogsInfo[$arFields['ID']]['date_start'] = $dateConditions[0];
        $catalogsInfo[$arFields['ID']]['id'] = $arFields['ID'];
        $catalogsInfo[$arFields['ID']]['image'] = "https://yarbox.ru/logo.png";
        if ($arFields['DEPTH_LEVEL'] == 1) $catalogsInfo[$arFields['ID']]['is_main'] = true;
        else $catalogsInfo[$arFields['ID']]['is_main'] = false;
        $catalogsInfo[$arFields['ID']]['target_shops'] = [];

    }

    $catalogsOffersIds = [];

    $arSelectElement = ['ID', 'NAME', 'DETAIL_PICTURE', 'DETAIL_PAGE_URL', 'PROPERTY_*'];
    $arSelectElement = ['*'];
    $arFilterElement = Array("IBLOCK_ID"=>$iblockId, "ACTIVE"=>"Y");
    $indexBlock = 0;
    $resElement = CIBlockElement::GetList(Array(), $arFilterElement, false, false, $arSelect);
    while($arEl = $resElement->GetNextElement()) {
        //if ($indexBlock >= 1000) break;

        $indexBlock++;
        $offerFields = $arEl->GetFields();
        $offerProps = $arEl->GetProperties();
        if (in_array($offerProps['TORGOVAJAMARKA']['VALUE'],explode(",",trim($edadeal_settings['torgovie_mark'])))) continue;
        $resEl = CCatalogProduct::GetByID($offerFields['ID']);

        $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($offerFields["ID"]);
        if (false) {
            //if ($offerFields['ID'] != 40348) continue;
            echo "<pre>";
            //print_r($resEl);
            print_r($offerFields);
            print_r($offerProps);
            echo "</pre>";
            echo "ID: ".$offerFields['ID']."<br>";
            echo "NAME: ".$offerFields['NAME']."<br>";
            echo "TOTAL_PRICE_ALL: ".$offerFields['TOTAL_PRICE_ALL']['VALUE']."<br>";
            echo "RATIO: ".$arMeasure[$offerFields["ID"]]['RATIO']."<br>";
            echo "price_new: ".$offerProps['TOTAL_PRICE_ALL']['VALUE']*$arMeasure[$offerFields["ID"]]['RATIO']."<br>";
            die();
        }
        $catalogsOffersIds[$offerFields['IBLOCK_SECTION_ID']]['offers_id'][] = $offerFields['ID'];
        $imgSrc = CFile::GetPath($offerFields["DETAIL_PICTURE"]);



        $offerItem = [
            //"barcode" => $offerProps["BARCODE"]['VALUE'],
            //"date_end" => $offerProps["DATE_EXPIRATION"]['VALUE'],
            //"date_start" => "2020-06-05T00:00:00+03:00",
            "description" => $offerFields['NAME'],
            //"discount_label" => "1+1",
            "id" => $offerFields['ID'],
            "image" => $currentUrl.$imgSrc,
            //"price_is_from" => false,
            "price_new" => $offerProps['TOTAL_PRICE_ALL']['VALUE']*$arMeasure[$offerFields["ID"]]['RATIO'],
        ];

        if ($offerProps['DISCOUNTED']['VALUE'] == 'Y') {
            $offerItem["price_old"] = $offerProps['TOTAL_PRICE_ALL']['VALUE']*$arMeasure[$offerFields["ID"]]['RATIO']
                + $offerProps['DISCOUNT_AMOUNT']['VALUE']*$arMeasure[$offerFields["ID"]]['RATIO'];
        }
        $offerItem["url"] = $currentUrl.$offerFields["DETAIL_PAGE_URL"];
        $offers[] = $offerItem;
    }

    foreach ($catalogsInfo as $id => $catalog) {

        /*if (empty($catalogsOffersIds[$id]['offers_id'])) $offersIdCat = [];
        else $offersIdCat = $catalogsOffersIds[$id]['offers_id'];*/

        if (!empty($catalogsOffersIds[$id]['offers_id'])) {
            $ed_catalogs[] = [
                "conditions" => $catalog['conditions'],
                "date_end" => $catalog['date_end'],
                "date_start" => $catalog['date_start'],
                "id" => $catalog['id'],
                "image" => $catalog['image'],
                "is_main" => $catalog['is_main'],
                "offers" => $catalogsOffersIds[$id]['offers_id'],
                "target_regions" => [
                    "Россия, Красноярск"
                ],
                "delivery_regions" => [
                    "Россия, Красноярск"
                ]
            ];
        }

    }

    $file_edadeal = [
        "catalogs" =>  $ed_catalogs,
        "offers" =>  $offers,
        "version" =>  2
    ];

    if(isset($edadeal_settings['field_name_file_output']) && (string)$edadeal_settings['field_name_file_output'])
        $name_file_output = trim($edadeal_settings['field_name_file_output']);

    $file = new IO\File(Application::getDocumentRoot() . "/bitrix/catalog_export/edadeal_json/".$name_file_output);
    $file->putContents(json_encode($file_edadeal));

}

function DateFromTo ($from, $to) {
    $dateStartCondition = '';
    $dateEndCondition = '';
    if (isset($from) && preg_match('/[:]/i',$from)) {
        $dateStart = DateTime::createFromFormat('d.m.Y H:i:s', $from);
        $dateStartCondition = date(DATE_RFC3339, $dateStart->getTimestamp());
    } else if (isset($from)) {
        $dateStart = DateTime::createFromFormat('d.m.Y', $from);
        $dateStartCondition = date(DATE_RFC3339, $dateStart->getTimestamp());
    }
    if (isset($to) && preg_match('/[:]/i',$to)) {
        $dateEnd = DateTime::createFromFormat('d.m.Y H:i:s', $to);
        //$dateEnd->add(new DateInterval('P10D'));
        $dateEndCondition = date(DATE_RFC3339, $dateEnd->getTimestamp());
    } else if (isset($to)) {
        $dateEnd = DateTime::createFromFormat('d.m.Y', $to);
        //$dateEnd->add(new DateInterval('P10D'));
        $dateEndCondition = date(DATE_RFC3339, $dateEnd->getTimestamp());
    }
    return [$dateStartCondition,$dateEndCondition];
}

#Визуальный вывод

$tabControl = new CAdminTabControl('tabControl', $aTabs);

?>
<? $tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='ftden45_edadealexpjson_settings'>

    <? foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):?>
        <? $tabControl->BeginNextTab(); ?>
        <? __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); ?>

    <?      endif;
        endforeach; ?>

    <?
    $tabControl->BeginNextTab();



    $tabControl->Buttons(); ?>

    <input type="submit" name="Export" value="<?echo Loc::getMessage('FTDEN45_EDADEALEXPJSON_EXPORT_EDADEAL')?>">
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>">
    <?=bitrix_sessid_post();?>
</form>
<? $tabControl->End(); ?>
