<?

namespace Custom\Regions;


/**
 * Класс для работы с регионами
 */
class Main
{
    /**
     * @var $moduleID string
     */
    private $moduleID;
    /**
     * @var $regions array
     */
    private $regions;

    function __construct()
    {
        $this->moduleID = 'custom.regions';
    }

    /**
     * @return array регионы
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getRegions():array
    {
        return \COption::GetOptionString($this->moduleID, "regions");
    }

    /**
     * @param array $regions
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function setRegions($regions)
    {
        \COption::SetOptionString($this->moduleID, "regions", $regions);
    }
}

?>