<?
$MESS ['YARDEX_REGIONS_COMPANY_NAME'] = "yarbox";
$MESS ['YARDEX_REGIONS_PARTNER_URI'] = "https://www.1c-bitrix.ru/";
$MESS ['YARDEX_REGIONS_INSTALL_NAME'] = "Свой модуль регионов";
$MESS ['YARDEX_REGIONS_INSTALL_DESCRIPTION'] = "Свой модуль регионов";
$MESS ['YARDEX_REGIONS_INST_INSTALL_TITLE'] = "Установка модуля";
$MESS ['YARDEX_REGIONS_INST_UNINSTA_TITLE'] = "Удаление модуля";
$MESS ["YARDEX_REGIONS_STEP1_INSTALL_MESS_1"] = "Вы уверены что хотите установить модуль?";
$MESS ["YARDEX_REGIONS_STEP1_INSTALL_MESS_2"] = "Установить";
$MESS ["YARDEX_REGIONS_SUCC_INST"] = "Модуль успешно установлен";
$MESS ["YARDEX_REGIONS_WIZARD_LIST"] = "Перейти в список";
$MESS ["YARDEX_SREGIONS_ERR_INST"] = "Ошибки, возникшие при установке";
$MESS ['YARDEX_REGIONS_BACK_TO_LIST'] = "Вернуться к списку модулей";
$MESS ['YARDEX_REGIONS_CAUTION_MESS'] = 'Внимание! Модуль будет удален из системы';
$MESS ['YARDEX_REGIONS_UNINST_MOD'] = "Удалить";
$MESS ['YARDEX_REGIONS_UNINSTALL_SUCCESS'] = "Мастера модуля";
$MESS ['YARDEX_REGIONS_UNINSTALL_ERROR'] = "Ошибка деинсталляции модуля";
$MESS ['YARDEX_REGIONS_BACK_TO_MOD_LIST'] = "Вернуться к списку модулей";
?>