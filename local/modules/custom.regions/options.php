<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Custom\Regions\Main;

Loc::loadMessages(__FILE__);

// получаем идентификатор модуля
$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialchars($request['mid'] != '' ? $request['mid'] : $request['id']);
// подключаем наш модуль
Loader::includeModule($module_id);
$asset = Bitrix\Main\Page\Asset::getInstance();
echo $asset->insertCss('/local/modules/custom.regions/lib/admin/options/style.css');
$asset->addJs('/local/modules/custom.regions/lib/admin/options/script.js');

if(trim($_GET["region"]) != ""){
    $region = new Main();
    $region->setRegions($_GET["region"]);
}

?>
    <form class="regions-ajax-container" action = "" method="get" id="regions-ajax-container">
        <input type = "hidden" name = "lang" value="ru">
        <input type = "hidden" name = "mid" value="custom.regions">
        <input type = "text" name = "region" value = "">
        <button>Добавить</button>
    </form>
<select>
<?php
?>
</select>