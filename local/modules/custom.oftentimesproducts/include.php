<?php

use Bitrix\Main\Loader;


Loader::registerAutoLoadClasses(
    'custom.oftentimesproducts',
    array(
        '\Custom\OftentimesProducts\OftentimesProductsTable' => 'lib/OftentimesProductsTable.php',
        '\Custom\OftentimesProducts\OftentimesProductsSale' => 'lib/OftentimesProductsSale.php',
        '\Custom\OftentimesProducts\OftentimesProductsHelper' => 'lib/OftentimesProductsHelper.php',
    )
);
