<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;


Loc::loadMessages(__FILE__);

class custom_oftentimesproducts extends CModule
{
    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {

            $arModuleVersion = array();

            include_once(__DIR__ . "/version.php");

            $this->MODULE_ID = 'custom.oftentimesproducts';
            $this->MODULE_VERSION       = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME          = Loc::getMessage("OFTENTIMES_PRODUCTS_NAME");
            $this->MODULE_DESCRIPTION  = Loc::getMessage("OFTENTIMES_PRODUCTS_DESCRIPTION");
            $this->PARTNER_NAME     = Loc::getMessage("OFTENTIMES_PRODUCTS_PARTNER_NAME");
        }

        return false;
    }

    public function DoInstall()
    {

        global $APPLICATION;

        if (CheckVersion(ModuleManager::getVersion("main"), "14.00.00")) {

            ModuleManager::registerModule($this->MODULE_ID);
            try {
                $this->InstallFiles();
                $this->InstallEvents();
                $this->InstallDB();
            } catch (\Throwable $th) {
                ModuleManager::unRegisterModule($this->MODULE_ID);
                $APPLICATION->ThrowException(
                    Loc::getMessage("OFTENTIMES_PRODUCTS_INSTALL_ERROR_1")
                );
                pr($th->getTraceAsString());
            }
        } else {

            $APPLICATION->ThrowException(
                Loc::getMessage("OFTENTIMES_PRODUCTS_INSTALL_ERROR_VERSION")
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("OFTENTIMES_PRODUCTS_INSTALL_TITLE") . " \"" . Loc::getMessage("OFTENTIMES_PRODUCTS_NAME") . "\"",
            __DIR__ . "/step.php"
        );

        return false;
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallDB();
        $this->UnInstallEvents();
        ModuleManager::unRegisterModule($this->MODULE_ID);


        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("OFTENTIMES_PRODUCTS_UNINSTALL_TITLE") . " \"" . Loc::getMessage("OFTENTIMES_PRODUCTS_NAME") . "\"",
            __DIR__ . "/unstep.php"
        );

        return false;
    }

    function InstallFiles($arParams = array())
    {
        return true;
    }

    function UnInstallFiles()
    {

        return true;
    }

    function InstallDB()
    {
        Loader::includeModule($this->MODULE_ID);
        \Custom\OftentimesProducts\OftentimesProductsTable::createDbTable();
        \Custom\OftentimesProducts\OftentimesProductsHelper::getInstance();
    }

    function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);

        Application::getConnection(\Custom\OftentimesProducts\OftentimesProductsTable::getConnectionName())->queryExecute(
            'drop table if exists ' . Base::getInstance('\Custom\OftentimesProducts\OftentimesProductsTable')->getDBTableName()
        );

        Option::delete($this->MODULE_ID);
    }


    function InstallEvents()
    {
        \Bitrix\Main\EventManager::getInstance()->registerEventHandler(
            'sale',
            'OnOrderAdd',
            $this->MODULE_ID,
            'Custom\\OftentimesProducts\\OftentimesProductsHelper',
            'addOrderedProductsByOrder'
        );
    }
    function UnInstallEvents()
    {
        \Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler(
            'sale',
            'OnOrderAdd',
            $this->MODULE_ID,
            'Custom\\OftentimesProducts\\OftentimesProductsHelper',
            'addOrderedProductsByOrder'
        );
    }


    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot = false)
    {
        if ($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        else
            return dirname(__DIR__);
    }
}
