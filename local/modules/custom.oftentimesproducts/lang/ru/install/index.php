<?
$MESS["OFTENTIMES_PRODUCTS_NAME"] = "Свой модуль подборки 'Я люблю'";
$MESS["OFTENTIMES_PRODUCTS_DESCRIPTION"] = "Добавляет отображение частоты заказов и количества заказов товара на сайт.";
$MESS["OFTENTIMES_PRODUCTS_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["OFTENTIMES_PRODUCTS_INSTALL_ERROR_1"] = "Ошибка";
$MESS["OFTENTIMES_PRODUCTS_INSTALL_TITLE"] = "Установка модуля";
$MESS["OFTENTIMES_PRODUCTS_UNINSTALL_TITLE"] = "Деинсталляция модуля";
$MESS["OFTENTIMES_PRODUCTS_PARTNER_NAME"] = "Кастомный компонент";
