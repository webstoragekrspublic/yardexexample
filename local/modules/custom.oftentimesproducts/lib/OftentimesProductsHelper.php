<?php

namespace Custom\OftentimesProducts;

use Bitrix\Main\Application;
use Bitrix\Sale;
use Bitrix\Main\Type;

class OftentimesProductsHelper
{
    protected static $instance = null;
    protected $tableCache = [];

    public static $limit = 20;
    public static $firstLoadPeriod = 90;

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new self();
        }
        return static::$instance;
    }

    protected function __construct()
    {
        $this->init();
    }

    protected function init()
    {
        if ($this->isAuthorizedType()) {
            $this->loadCurUserOftentimeProducts();
        }
    }

    public function loadCurUserOftentimeProducts()
    {
        $userId = $this->getCurrentUserId();

        if (!isset($this->tableCache[$userId])) {
            $items = \Custom\OftentimesProducts\OftentimesProductsTable::getItemsForUserId($userId, static::$limit);
            $this->tableCache[$userId] = $items;
        }

        return $this->tableCache[$userId];
    }

    public function loadCurUserAllOftentimeProducts()
    {
        $userId = $this->getCurrentUserId();
        $items = \Custom\OftentimesProducts\OftentimesProductsTable::getItemsDetailForUserId($userId);
        $this->tableCache[$userId] = $items;
        return $this->tableCache[$userId];
    }


    public function addOrderedProductsByOrder($ID, &$arFields)
    {
        $userId = $arFields['USER_ID'];
        $userOftentimesProducts = \Custom\OftentimesProducts\OftentimesProductsTable::getItemsForUserId($userId);
        $dateInsert = $arFields['DATE_INSERT'];
        $basketItems = $arFields['BASKET_ITEMS'];
        foreach ($basketItems as $item) {
            if (isset($userOftentimesProducts[$item['PRODUCT_ID']])) {
                $existOftenProduct = $userOftentimesProducts[$item['PRODUCT_ID']];
                \Custom\OftentimesProducts\OftentimesProductsTable::update($existOftenProduct['ID'], [
                    'ORDER_COUNT' => $existOftenProduct['ORDER_COUNT'] + 1,
                    'TIME_LAST_ADDED' => new Type\DateTime($dateInsert),
                ]);
            } else {
                \Custom\OftentimesProducts\OftentimesProductsTable::add([
                    'ID_USER' => $userId,
                    'ID_ITEM' => $item['PRODUCT_ID'],
                    'ORDER_COUNT' => 1,
                    'TIME_FIRST_ADDED' => new Type\DateTime($dateInsert),
                    'TIME_LAST_ADDED' => new Type\DateTime($dateInsert),
                ]);
            }
        }
    }

    public static function firstLoadForContainTable()
    {
        $arResult = [];
        if (\CModule::IncludeModule("sale")) {
            $arFilter = array(
                'STATUS_ID' => 'F',
                ">=DATE_INSERT" => date('d.m.Y', time() - 86400 * static::$firstLoadPeriod) . " 00:00:00",
            );
            $arSelect = ['USER_ID', 'ID', 'DATE_INSERT', 'STATUS_ID'];
            $rsSales = \CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter, false, false, $arSelect);

            while ($arSales = $rsSales->Fetch()) {
                if (!isset($arResult[$arSales['USER_ID']])) {
                    $arResult[$arSales['USER_ID']] = ['USER_ID' => $arSales['USER_ID'], 'ITEMS' => []];
                }
                $basket = Sale\Order::load($arSales['ID'])->getBasket();
                foreach ($basket as $basketItem) {
                    if (!isset($arResult[$arSales['USER_ID']]['ITEMS'][$basketItem->getProductId()])) {
                        $arResult[$arSales['USER_ID']]['ITEMS'][$basketItem->getProductId()] = ['ID' => $basketItem->getProductId(), 'ORDER_COUNT' => 1, 'FIRST_ORDER' => $arSales['DATE_INSERT'], 'LAST_ORDER' => $arSales['DATE_INSERT']];
                    } else {
                        $arResult[$arSales['USER_ID']]['ITEMS'][$basketItem->getProductId()]['ORDER_COUNT'] += 1;
                        $arResult[$arSales['USER_ID']]['ITEMS'][$basketItem->getProductId()]['LAST_ORDER'] = $arSales['DATE_INSERT'];
                    }
                }
            }
        }
        foreach ($arResult as $row) {
            $userId = $row['USER_ID'];
            foreach ($row['ITEMS'] as $item) {
                \Custom\OftentimesProducts\OftentimesProductsTable::add([
                    'ID_USER' => $userId,
                    'ID_ITEM' => $item['ID'],
                    'ORDER_COUNT' => $item['ORDER_COUNT'],
                    'TIME_FIRST_ADDED' => new Type\DateTime($item['FIRST_ORDER']),
                    'TIME_LAST_ADDED' => new Type\DateTime($item['LAST_ORDER']),
                ]);
            }
        }
    }

    public function  isAuthorizedType()
    {
        global $USER;
        return $USER->IsAuthorized();
    }

    protected function getCurrentUserId()
    {
        global $USER;
        return $USER->GetID();
    }
}
