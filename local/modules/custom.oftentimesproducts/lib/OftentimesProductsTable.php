<?php

namespace Custom\OftentimesProducts;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bitrix\Main\Entity\Base;

class OftentimesProductsTable extends Entity\DataManager
{

    public static function getTableName()
    {
        return 'custom_oftentimes_products';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\IntegerField('ID_USER', array(
                'required' => true,
            )),
            new Entity\IntegerField('ID_ITEM', array(
                'required' => true,
            )),
            new Entity\IntegerField('ORDER_COUNT', array(
                'required' => true,
            )),
            new Entity\DatetimeField('TIME_FIRST_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
            new Entity\DatetimeField('TIME_LAST_ADDED', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }

    public static function createDbTable()
    {
        if (!Application::getConnection(OftentimesProductsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Custom\OftentimesProducts\OftentimesProductsTable')->getDBTableName()
        )) {
            Base::getInstance('\Custom\OftentimesProducts\OftentimesProductsTable')->createDbTable();
            Application::getConnection(OftentimesProductsTable::getConnectionName())->queryExecute('ALTER TABLE `' . static::getTableName() . '` ADD INDEX (`ID_USER`,`ID_ITEM`,`ORDER_COUNT`);');
        }
    }

    public static function getItemsForUserId($userId, $limit = null)
    {
        $q = static::getList(
            [
                'select' => ['ID', 'ID_ITEM', 'ORDER_COUNT'],
                'filter' => ['=ID_USER' => (int)$userId],
                'order' => [
                    'ORDER_COUNT' => 'DESC'
                ],
                'limit' => $limit,
            ]
        );

        $result = [];
        while ($row = $q->fetch()) {
            $result[$row['ID_ITEM']] = ['ID' => $row['ID'], 'ORDER_COUNT' => $row['ORDER_COUNT'], 'ID_ITEM' => $row['ID_ITEM']];
        }
        return $result;
    }
    public static function getItemsDetailForUserId($userId, $limit = null)
    {
        $q = static::getList(
            [
                'select' => ['ID', 'ID_ITEM', 'ORDER_COUNT', 'TIME_FIRST_ADDED', 'TIME_LAST_ADDED'],
                'filter' => ['=ID_USER' => (int)$userId],
                'order' => [
                    'ORDER_COUNT' => 'DESC'
                ],
                'limit' => $limit,
            ]
        );

        $result = [];
        while ($row = $q->fetch()) {
            $result[$row['ID_ITEM']] = [
                'ID' => $row['ID'],
                'ORDER_COUNT' => $row['ORDER_COUNT'],
                'ID_ITEM' => $row['ID_ITEM'],
                'TIME_FIRST_ADDED' => $row['TIME_FIRST_ADDED'],
                'TIME_LAST_ADDED' => $row['TIME_LAST_ADDED'],
            ];
        }
        return $result;
    }
}
