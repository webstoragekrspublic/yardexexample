<?php

namespace Custom\OftentimesProducts;

class OftentimesProductsSale
{
    public static $DISCOUNT_VALUE = 80;

    public static function getUserOTPsSaleName($userId)
    {
        return 'OTP_SALE_FOR_' . $userId;
    }

    public static function getOTPSaleRuleIdIfExist($userId)
    {
        $q = \Bitrix\Sale\Internals\DiscountGroupTable::getList(
            [
                'select' => ['ID', 'DISCOUNT_ID', 'DISCOUNT'],
                'filter' => ['=SALE_INTERNALS_DISCOUNT_GROUP_DISCOUNT_NAME' => static::getUserOTPsSaleName($userId)]
            ]
        );

        if ($row = $q->fetch()) {
            if (!empty($row)) {
                return $row['DISCOUNT_ID'];
            }
        }
        return false;
    }

    public static function updateOTPsSale($ruleID, $otpItemsIDs)
    {
        \Bitrix\Main\Loader::includeModule('sale');

        $arFields = [
            'ACTIONS' => [
                'CLASS_ID' => 'CondGroup',
                'DATA' => ['All' => 'AND'],
                'CHILDREN' => [
                    [
                        'CLASS_ID' => 'ActSaleBsktGrp',
                        'DATA' => [
                            "Type" => "Discount",
                            "Value" => static::$DISCOUNT_VALUE,
                            "Unit" => "Perc",
                            "Max" => 0,
                            "All" => "OR",
                            "True" => "True",
                        ],
                        'CHILDREN' => [
                            [
                                'CLASS_ID' => 'CondIBElement',
                                'DATA' => [
                                    'logic' => 'Equal',
                                    'value' => $otpItemsIDs
                                ]
                            ]
                        ]
                    ]
                ]
            ],
        ];
        $iDiscountNumber = \CSaleDiscount::update($ruleID, $arFields);

        return $iDiscountNumber;
    }

    public static function addOTPsSale($userId, $otpItemsIDs)
    {
        \Bitrix\Main\Loader::includeModule('sale');

        $arFields = [
            "LID" => "s1",
            "NAME" => static::getUserOTPsSaleName($userId),
            "LAST_DISCOUNT" => "N",
            "ACTIVE" => "Y",
            'ACTIONS' => [
                'CLASS_ID' => 'CondGroup',
                'DATA' => ['All' => 'AND'],
                'CHILDREN' => [
                    [
                        'CLASS_ID' => 'ActSaleBsktGrp',
                        'DATA' => [
                            "Type" => "Discount",
                            "Value" => static::$DISCOUNT_VALUE,
                            "Unit" => "Perc",
                            "Max" => 0,
                            "All" => "OR",
                            "True" => "True",
                        ],
                        'CHILDREN' => [
                            [
                                'CLASS_ID' => 'CondIBElement',
                                'DATA' => [
                                    'logic' => 'Equal',
                                    'value' => $otpItemsIDs
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "CONDITIONS" => [
                'CLASS_ID' => 'CondGroup',
                'DATA' => [
                    'All' => 'AND',
                    'True' => 'True',
                ],
                'CHILDREN' => [
                    [
                        'CLASS_ID' => 'CondMainUserId',
                        'DATA' => [
                            'logic' => 'Equal',
                            'value' => [$userId]
                        ]
                    ]
                ]
            ],
            "USER_GROUPS" => array(2), // группы пользователей, можно просмотреть в настройки -> пользователи -> группы
            "CURRENCY" => "RUB"
        ];
        $iDiscountNumber = \CSaleDiscount::add($arFields);

        return $iDiscountNumber;
    }
}
