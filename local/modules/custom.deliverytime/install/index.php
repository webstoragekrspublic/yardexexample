<?

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

Class custom_deliverytime extends CModule
{
    function __construct() {
        $arModuleVersion = array();
        include(__DIR__ . "/version.php");

        $this->MODULE_ID = 'custom.deliverytime';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("CUSTOM_DELIVERY_TIME_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("CUSTOM_DELIVERY_TIME_MODULE_DESC");

        $this->PARTNER_NAME = Loc::getMessage("CUSTOM_DELIVERY_TIME_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("CUSTOM_DELIVERY_TIME_PARTNER_URI");

    }

    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot = false) {
        if ($notDocumentRoot)
            return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

    //Проверяем что система поддерживает D7
    public function isVersionD7() {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    function InstallDB() {
        Loader::includeModule($this->MODULE_ID);
    }

    function UnInstallDB() {
        Loader::includeModule($this->MODULE_ID);

        Option::delete($this->MODULE_ID);
    }

    function DoInstall() {
        global $APPLICATION;

        if ($this->isVersionD7()) {
            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallDB();

        } else {
            $APPLICATION->ThrowException(Loc::getMessage("CUSTOM_DELIVERY_TIME_INSTALL_ERROR_VERSION"));
        }

        $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_DELIVERY_TIME_INSTALL_TITLE"), $this->GetPath() . "/install/step.php");
    }

    function DoUninstall() {
        global $APPLICATION;

        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();

        if ($request["step"] < 2) {
            $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_DELIVERY_TIME_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep1.php");
        } elseif ($request["step"] == 2) {

            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(Loc::getMessage("CUSTOM_DELIVERY_TIME_UNINSTALL_TITLE"), $this->GetPath() . "/install/unstep2.php");
        }
    }
}

