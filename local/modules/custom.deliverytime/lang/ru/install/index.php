<?
$MESS["CUSTOM_DELIVERY_TIME_MODULE_NAME"] = "Свой модуль Временные слоты";
$MESS["CUSTOM_DELIVERY_TIME_MODULE_DESC"] = "Свой модуль Временные слоты";
$MESS["CUSTOM_DELIVERY_TIME_PARTNER_NAME"] = "Кастомный компонент";
$MESS["CUSTOM_DELIVERY_TIME_PARTNER_URI"] = "";

$MESS["CUSTOM_DELIVERY_TIME_INSTALL_TITLE"] = "Установка модуля";
$MESS["CUSTOM_DELIVERY_TIME_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
