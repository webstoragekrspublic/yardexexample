<?

namespace Custom\DeliveryTime\TimeSlotConditions;

class TimeSlotDisableConditionDayDelay extends TimeSlotDisableCondition
{
    protected string $description = "задержка на количество дней";

    protected function isSpecialConditionWork(TimeSlotDisableCondition $compareCondition): bool
    {
        return $compareCondition->getValue() > $this->getValue();
    }
}