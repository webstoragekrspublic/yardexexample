<?

namespace Custom\DeliveryTime\TimeSlotConditions;

class TimeSlotDisableConditionMinHour extends TimeSlotDisableCondition
{
    protected string $description = "минимальный час слота на текущий и следующий день";

    protected function isSpecialConditionWork(TimeSlotDisableCondition $compareCondition): bool
    {
        return $compareCondition->getValue() > $this->getValue();
    }
}