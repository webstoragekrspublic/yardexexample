<?

namespace Custom\DeliveryTime\TimeSlotConditions;

class TimeSlotDisableConditionSpecialType extends TimeSlotDisableCondition
{
    protected string $description = "специальный тип";

    protected function isSpecialConditionWork(TimeSlotDisableCondition $compareCondition): bool
    {
        return $compareCondition->getValue() === $this->getValue();
    }
}