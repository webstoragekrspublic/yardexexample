<?

namespace Custom\DeliveryTime\TimeSlotConditions;

abstract class TimeSlotDisableCondition
{
    protected string $type;
    protected $value;

    protected string $description = "описание не задано";

    const TIME_DIFFERENCE_TYPE = 'TimeDiff';
    const MAX_USED_TIMES_TYPE = 'UsedTimes';
    const SPECIAL_TYPE = 'SpecialType';
    const DAY_DELAY_TYPE = 'DayDelay';
    const MIN_HOUR_TYPE = 'MinHour';
    const WEEK_NUMBER_TYPE = 'WeekNumber';
    const DAY_OF_WEEK = 'DayOfWeek';
    const TIME_FROM = "TimeFrom";
    const TIME_TO = "TimeTo";
    const TIME_FORMAT = "TimeFormat";
    const TIME_SLOT = "TimeSlot";

    const ALL_TYPES = [
        self::SPECIAL_TYPE,
        self::TIME_DIFFERENCE_TYPE,
        self::MAX_USED_TIMES_TYPE,
        self::DAY_DELAY_TYPE,
        self::MIN_HOUR_TYPE,
        self::WEEK_NUMBER_TYPE
    ];

    public function getType(): string
    {
        return $this->type;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public static function getAllTypes(): array
    {
        return self::ALL_TYPES;
    }

    protected function __construct(string $type, $value)
    {
        $this->type = $type;
        $this->value = $value;
    }

    public static function NewFromType(string $type, $value): ?TimeSlotDisableCondition
    {
        try {
            //\COption::SetOptionString("custom.deliverytime", "productPropsByInterval", "");
            $className = 'Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition' . $type;
            if (class_exists($className)){
                return new $className($type, $value);
            }
        } catch (\Exception $exception) {
        }
        return null;
    }

    /**
     * @param TimeSlotDisableCondition $compareCondition
     * @return bool - возвращает true если условие отрабатывает (TimeSlot должен стать закрытым)
     */
    public function isConditionWork(TimeSlotDisableCondition $compareCondition): bool
    {
        if ($compareCondition->getType() !== $this->getType()) {
            return false;
        }

//        echo $this->isSpecialConditionWork($compareCondition);
//        echo '<br>';
        
        return $this->isSpecialConditionWork($compareCondition);
    }

    abstract protected function isSpecialConditionWork(TimeSlotDisableCondition $compareCondition): bool;
}
