<?

namespace Custom\DeliveryTime\TimeSlotConditions;

class TimeSlotDisableConditionUsedTimes extends TimeSlotDisableCondition
{
    protected string $description = "количество заказов на слот";

    protected function isSpecialConditionWork(TimeSlotDisableCondition $compareCondition): bool
    {
        return $compareCondition->getValue() >= $this->getValue();
    }
}