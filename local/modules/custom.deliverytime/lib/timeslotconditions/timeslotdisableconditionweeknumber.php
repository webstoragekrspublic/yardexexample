<?

namespace Custom\DeliveryTime\TimeSlotConditions;

class TimeSlotDisableConditionWeekNumber extends TimeSlotDisableCondition
{
    protected string $description = "ограничение по дням недели";

    protected function isSpecialConditionWork(TimeSlotDisableCondition $compareCondition): bool
    {
        return $compareCondition->getValue() == $this->getValue();
    }
}