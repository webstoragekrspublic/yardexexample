<?

namespace Custom\DeliveryTime\TimeSlotConditions;

class TimeSlotDisableConditionTimeDiff extends TimeSlotDisableCondition
{
    protected string $description = "временная задержка в секундах от начала дня (00:00:00)";

    protected function isSpecialConditionWork(TimeSlotDisableCondition $compareCondition): bool
    {
//        echo $compareCondition->getValue().'TTT'.$this->getValue();
//        echo '<br>';
        return $compareCondition->getValue() > $this->getValue();
    }
}