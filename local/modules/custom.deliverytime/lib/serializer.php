<?

namespace Custom\DeliveryTime;

use Custom\DeliveryTime\Models\ProductPropDisableCondition;
use Custom\DeliveryTime\Models\TimeSlot;
use Custom\DeliveryTime\Models\WeekEnum;
use Custom\DeliveryTime\Settings\BannedDaysSetting;
use Custom\DeliveryTime\Settings\DaySetting;
use Custom\DeliveryTime\Models\MonthEnum;
use Custom\DeliveryTime\Settings\HiddenDaysSetting;
use Custom\DeliveryTime\Settings\SpecialDaysSetting;
use Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition;

class Serializer
{

    /**
     * @param DaySetting[] $daysSetting
     * @return array
     */
    public static function daysSettingToArr(array $daysSetting): array
    {
        $res = [];
        
        foreach ($daysSetting as $ds) {
            $res[] = self::daySettingToArr($ds);
        }
        return $res;

    }

    public static function daySettingToArr(DaySetting $daySetting): array
    {
        $dayTS = $daySetting->getStartDateTime();

        if (!$dayTS) {
            $dayTS = new \DateTime();
            $dayTS->setTimestamp(0);
        }
        $monthText = MonthEnum::getMonthName((int)$dayTS->format('m'));
        return [
            'fullDate' => $dayTS->format('d.m.Y'),
            'infoDate' => $daySetting->getName(),
            'infoDate2' => (int)$dayTS->format('d') . ' ' . $monthText,
            'dateInfo' => [
                'day' => $dayTS->format('d'),
                'month' => $dayTS->format('m'),
                'year' => $dayTS->format('Y'),
                'monthText' => $monthText
            ],
            'disabled' => $daySetting->isDisabled(),
            'timeSlots' => self::timeSlotsToArr($daySetting->getTimeSlots())
        ];
    }

    public static function daySettingToAdminArr(DaySetting $daySetting): array
    {
        $dayTS = $daySetting->getStartDateTime();
        if (!$dayTS) {
            $dayTS = new \DateTime();
            $dayTS->setTimestamp(0);
        }
        return [
            'name' => $daySetting->getName(),
            'dateInfo' => [
                'day' => $dayTS->format('d'),
                'month' => $dayTS->format('m'),
                'year' => $dayTS->format('Y'),
            ],
            'weekNumber' => $daySetting->getWeekNumber(),
            'weekName' => WeekEnum::getWeekDayName($daySetting->getWeekNumber()),
            'disabled' => $daySetting->isDisabled(),
            'timeSlots' => self::timeSlotsToAdminArr($daySetting->getTimeSlots())
        ];
    }


    /**
     * @param TimeSlot[] $timeSlots
     * @return array
     */
    public static function timeSlotsToArr(array $timeSlots): array
    {
        $res = [];
        foreach ($timeSlots as $ts) {
            $res[] = self::timeSlotToArr($ts);
        }
        return $res;
    }

    public static function timeSlotToArr(TimeSlot $timeSlot): array
    {
        return [
            "time" => $timeSlot->getName(),
            "disabled" => $timeSlot->isDisabled()
        ];
    }

    /**
     * @param TimeSlot[] $timeSlots
     * @return array
     */
    public static function timeSlotsToAdminArr(array $timeSlots): array
    {
        $res = [];
        foreach ($timeSlots as $ts) {
            $res[] = self::timeSlotToAdminArr($ts);
        }
        return $res;
    }

    public static function timeSlotToAdminArr(TimeSlot $timeSlot): array
    {
        return [
            "name" => $timeSlot->getName(),
            "disabled" => $timeSlot->isDisabled(),
            "disableConditions" => self::disableConditionsToAdminArr($timeSlot->getDisabledConditions())
        ];
    }

    /**
     * @param TimeSlotDisableCondition[] $dcs
     */
    public static function disableConditionsToAdminArr(array $dcs): array
    {
        $res = [];
        if(is_array($dcs[0])){
            if(!empty($dcs[0]["DayOfWeek"])){
                foreach ($dcs as $key=>$dc) {
                    foreach($dc as $type=>$value) {
                        $res[$key][$type] = [
                            'type' => $type,
                            'value' => $value
                        ];
                    }
                }
            }
            else {
                foreach ($dcs as $dc) {
                    $res[] = [
                        'type' => $dc->getType(),
                        'value' => $dc->getValue()
                    ];
                }
            }
        }
        else{
            foreach ($dcs as $dc) {
                $res[] = [
                    'type' => $dc->getType(),
                    'value' => $dc->getValue()
                ];
            }
        }
        return $res;
    }

    public static function bannedDaysToAdminArr(BannedDaysSetting $bannedDaysSetting): array
    {
        $res = [];
        $dates = $bannedDaysSetting->getBannedDays();
        if ($dates) {
            foreach ($dates as $date) {
                $res[] = [
                    'day' => $date->format('d'),
                    'month' => $date->format('m'),
                    'year' => $date->format('Y'),
                ];
            }
        }

        return $res;
    }

    public static function hiddenDaysToAdminArr(HiddenDaysSetting $hiddenDaysSetting): array
    {
        $res = [];
        $dates = $hiddenDaysSetting->getHiddenDays();
        if ($dates) {
            foreach ($dates as $date) {
                $res[] = [
                    'day' => $date->format('d'),
                    'month' => $date->format('m'),
                    'year' => $date->format('Y'),
                ];
            }
        }

        return $res;
    }

    public static function propDisableConditionToAdminArr(ProductPropDisableCondition $productPropDisableCondition): array
    {
        return [
            'propCode' => $productPropDisableCondition->getPropCode(),
            'propValue' => $productPropDisableCondition->getPropValue(),
            "disableConditions" => self::disableConditionsToAdminArr($productPropDisableCondition->getTimeSlotDisableConditions())
        ];
    }
}
