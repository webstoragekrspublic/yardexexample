<?

namespace Custom\DeliveryTime\Settings;

use Custom\DeliveryTime\DeliveryTimeTools;
use Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition;
use Custom\DeliveryTime\Models\WeekEnum;
use Custom\DeliveryTime\Models\UsedSlots;
use Custom\DeliveryTime\Models\TimeSlot;

class DaySetting
{

    /**
     * номер дня недели [0-6] = [вс-сб]
     * @var int
     */
    protected int $weekNumber;

    /**
     * @var string WeekEnum
     */
    protected string $name;

    /**
     * @var \DateTime|null - начало дня, 10.12.2022 00:00:00
     */
    protected ?\DateTime $startDateTime = null;

    protected bool $disabled;

    /**
     * @var TimeSlot[]
     */
    protected array $timeSlots;

    public function getWeekNumber(): int
    {
        return $this->weekNumber;
    }

    /**
     * @param int $weekNumber
     * @param TimeSlot[] $timeSlots
     * @param bool $disabled
     */
    public function __construct(int $weekNumber, array $timeSlots, bool $disabled = false)
    {
        $this->setTimeSlots($timeSlots);
        $this->disabled = $disabled;
        $this->setWeekNumber($weekNumber);
    }

    function __clone()
    {
        $clonedTs = [];
        foreach ($this->timeSlots as $k => $ts) {
            $clonedTs[$k] = clone $ts;
        }
        $this->timeSlots = $clonedTs;
    }

    /**
     * @param TimeSlot[] $timeSlots
     */
    public function setTimeSlots(array $timeSlots){
        $this->timeSlots = $timeSlots;
    }

    public static function NewFromDate(\DateTime $dateTime, array $timeSlots, bool $disabled = false): DaySetting
    {
        $newObj = new self((int)$dateTime->format('w'), $timeSlots, $disabled);
        $newObj->setDateTime($dateTime);
        return $newObj;
    }

    /**
     * @return DaySetting
     */
    public static function NewEmpty(): DaySetting
    {
        return new self(0, []);
    }

    protected function setWeekNumber(int $weekNumber)
    {
        $this->weekNumber = $weekNumber;
        $this->setName(WeekEnum::getWeekDayName($weekNumber));
        $weekDC = TimeSlotDisableCondition::NewFromType(TimeSlotDisableCondition::WEEK_NUMBER_TYPE, $weekNumber);
        $this->addWeekNumberTimeSlotCondition($weekDC);
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function setDateTime(\DateTime $dateTime)
    {
        $this->startDateTime = \DateTime::createFromFormat('!d.m.Y', $dateTime->format('d.m.Y'));
        
        $this->setWeekNumber((int)$this->startDateTime->format('w'));

        $daySeconds = 24 * 60 * 60;
        $dayDelayFromCurrent = (int)(($this->startDateTime->getTimestamp() - DeliveryTimeTools::getInstance()->getCurrentTimeStamp()) / $daySeconds + 1);
        $dayDelayDC = TimeSlotDisableCondition::NewFromType(TimeSlotDisableCondition::DAY_DELAY_TYPE, $dayDelayFromCurrent);
        $this->addDayDelayTimeSlotCondition($dayDelayDC);

        if ($dayDelayDC == 0 || $dayDelayDC == 1) { //добавляем MinHour только для сегодняшнего или завтрашнего дня
            $this->addMinHourTimeSlotCondition();
        }
    }

    /**
     * не добавляем TimeSlotDisableCondition в отличии от setDateTime
     * @param \DateTime $dateTime
     */
    public function setDateTimeOnly(\DateTime $dateTime)
    {
        $this->startDateTime = \DateTime::createFromFormat('!d.m.Y', $dateTime->format('d.m.Y'));
        $this->setWeekNumberOnly((int)$this->startDateTime->format('w'));
    }

    /**
     * не добавляем TimeSlotDisableCondition в отличии от setWeekNumber
     * @param int $weekNumber
     */
    public function setWeekNumberOnly(int $weekNumber)
    {
        $this->weekNumber = $weekNumber;
        $this->setName(WeekEnum::getWeekDayName($weekNumber));
    }

    protected function addDayDelayTimeSlotCondition(TimeSlotDisableCondition $dayDelayDC)
    {
        foreach ($this->timeSlots as $ts) {
            $ts->addDisabledCondition($dayDelayDC);
        }
    }

    protected function addMinHourTimeSlotCondition()
    {
        foreach ($this->timeSlots as $ts) {
            if (!$ts->isAnyTimeSlot()) {
                $dc = TimeSlotDisableCondition::NewFromType(TimeSlotDisableCondition::MIN_HOUR_TYPE, $ts->getStartHour());
                $ts->addDisabledCondition($dc);
            }
        }
    }

    protected function addWeekNumberTimeSlotCondition(TimeSlotDisableCondition $weekDC)
    {
        foreach ($this->timeSlots as $ts) {
            $ts->addDisabledCondition($weekDC);
        }
    }

    public function getStartDateTime(): ?\DateTime
    {
        return $this->startDateTime;
    }

    /**
     * @return TimeSlot[]
     */
    public function getTimeSlots(): array
    {
        return $this->timeSlots;
    }

    public function setDisabled(bool $v)
    {
        $this->disabled = $v;
    }

    public function setDisabledDayAndSlots()
    {
        $this->disabled = true;
        foreach ($this->timeSlots as $timeSlot) {
            $timeSlot->setDisabled(true);
        }
    }

    public function applyTimeCondition(int $fromTimeStamp)
    {
        // ЗАМЕЧАНИЕ
        $timeDifference = $fromTimeStamp - $this->getStartDateTime()->getTimestamp();
        
//        echo $fromTimeStamp.'ЕЕЕ'.$this->getStartDateTime()->getTimestamp();
//        echo '<br>';
        
//        $timeDifference = abs($timeDifference);
        
        $dsCondition = TimeSlotDisableCondition::NewFromType(TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE, $timeDifference);
        foreach ($this->timeSlots as $timeSlot) {
            $timeSlot->disableByExistTimeSlotConditions($dsCondition);
        }
        $this->disableIfAllSlotsDisabled();
    }

    public function applyUsedSlotCondition(UsedSlots $usedSlots)
    {
        foreach ($this->timeSlots as $timeSlot) {
            $dsCondition = TimeSlotDisableCondition::NewFromType(
                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE,
                $usedSlots->usedSlotCount($this->getStartDateTime(), $timeSlot->getName())
            );
            $timeSlot->disableByExistTimeSlotConditions($dsCondition);
        }
        $this->disableIfAllSlotsDisabled();
    }

    /**
     * @param TimeSlotDisableCondition[] $timeSlotDisableConditions
     */
    public function applyTimeSlotDisableConditions(array $timeSlotDisableConditions)
    {
        foreach ($timeSlotDisableConditions as $dc) {
            foreach ($this->timeSlots as $timeSlot) {
                // ЗАМЕЧАНИЕ
                $timeSlot->disableByExistTimeSlotConditions($dc);
            }
        }
        $this->disableIfAllSlotsDisabled();
    }

    protected function disableIfAllSlotsDisabled()
    {
        $anyTimeSlots = [];
        $allSlotsIsDisabled = true;
        foreach ($this->timeSlots as $timeSlot) {
            if ($timeSlot->isAnyTimeSlot()) {
                $anyTimeSlots[] = $timeSlot;
            } else if (!$timeSlot->isDisabled()) {
                $allSlotsIsDisabled = false;
                break;
            }
        }
        
        if ($allSlotsIsDisabled) {
            $this->setDisabled(true);
            if ($anyTimeSlots) {
                foreach ($anyTimeSlots as $ts) {
                    $ts->setDisabled(true);
                }
            }
        }
    }
}
