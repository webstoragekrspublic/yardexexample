<?

namespace Custom\DeliveryTime\Settings;

class HiddenDaysSetting
{
    protected array $hiddenDays = [];

    public function __construct()
    {
    }

    public function addHiddenDay(\DateTime $dayDateTime)
    {
        $Y = (int)$dayDateTime->format('Y');
        $m = (int)$dayDateTime->format('m');
        $d = (int)$dayDateTime->format('d');
        $this->hiddenDays[$Y][$m][$d] = true;
    }

    public function isHiddenDay(int $d, int $m, int $Y): bool
    {
        return isset($this->hiddenDays[$Y][$m][$d]);
    }

    /**
     * @return \DateTime[]
     */
    public function getHiddenDays(): array
    {
        $res = [];
        if ($this->hiddenDays) {
            foreach ($this->hiddenDays as $Y => $infoM) {
                foreach ($infoM as $m => $infoD) {
                    foreach ($infoD as $d => $true) {
                        $res[] = \DateTime::createFromFormat('!d.m.Y', "$d.$m.$Y");
                    }
                }
            }
        }
        return $res;
    }
}
