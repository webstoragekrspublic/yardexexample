<?

namespace Custom\DeliveryTime\Settings;

class BannedProductsWeekNumberSetting
{
    protected int $productId;

    /**
     * @var int[]
     */
    protected array $bannedWeekNumbers;

    /**
     * @param int $productId
     * @param int[] $bannedWeekNumbers
     */
    public function __construct(int $productId, array $bannedWeekNumbers)
    {
        $this->productId = $productId;
        $this->bannedWeekNumbers = $bannedWeekNumbers;
    }

    /**
     * @return int[]
     */
    public function getBannedWeekNumbers(): array
    {
        return $this->bannedWeekNumbers;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }
}
