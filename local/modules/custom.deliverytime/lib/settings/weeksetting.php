<?

namespace Custom\DeliveryTime\Settings;

class WeekSetting
{
    /**
     * @var DaySetting[]
     */
    protected array $daySetting;

    /**
     * @param DaySetting[] $daySetting
     */
    public function __construct(array $daySetting)
    {
        $this->daySetting = [];
        foreach ($daySetting as $d) {
            $this->daySetting[$d->getWeekNumber()] = $d;
        }
    }

    public function getDaySettingByNumber(int $weekNumber): ?DaySetting
    {
        $copyDaySetting = null;
        if (isset($this->daySetting[$weekNumber])) {
            $copyDaySetting = clone $this->daySetting[$weekNumber];
        }
        return $copyDaySetting;
    }

    /**
     * @return DaySetting[]
     */
    public function getAllDays(): array
    {
        return $this->daySetting;
    }
}
