<?

namespace Custom\DeliveryTime\Settings;

class SpecialDaysSetting
{
    /**
     * @var DaySetting[]
     */
    protected array $daysSetting;

    public function __construct()
    {
        $this->daysSetting = [];
    }

    public function addDaySetting(DaySetting $day)
    {
        if (!$day->getStartDateTime()) {
            return;
        }
        $Y = (int)$day->getStartDateTime()->format('Y');
        $m = (int)$day->getStartDateTime()->format('m');
        $d = (int)$day->getStartDateTime()->format('d');
        $this->daysSetting[$Y][$m][$d] = $day;
    }

    public function getDaySetting(int $d, int $m, int $Y): ?DaySetting
    {
        return $this->daysSetting[$Y][$m][$d] ?? null;
    }

    public function isHaveSpecDaySetting(int $d, int $m, int $Y): bool
    {
        return isset($this->daysSetting[$Y][$m][$d]);
    }

    /**
     * @return DaySetting[]
     */
    public function getDaysSetting(): array
    {
        $res = [];
        foreach ($this->daysSetting as $Y => $daysSettingY){
            foreach ($daysSettingY as $m => $daysSettingM){
                foreach ($daysSettingM as $d => $daySetting){
                    $res[] = $daySetting;
                }
            }
        }
        return $res;
    }
}
