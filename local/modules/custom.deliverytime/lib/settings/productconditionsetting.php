<?

namespace Custom\DeliveryTime\Settings;

use Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition;
use Custom\DeliveryTime\Models\ProductPropDisableCondition;
use Custom\DeliveryTime\Models\DeliveryTimeProduct;

class ProductConditionSetting
{
    /**
     * @var ProductPropDisableCondition[][]
     */
    protected array $propsDisableCondition;

    protected array $propsByInterval;

    /**
     * @var BannedProductsWeekNumberSetting[]
     */
    protected array $bannedProductsWeekNumber;

    public function __construct()
    {
        $this->propsDisableCondition = [];
        $this->bannedProductsWeekNumber = [];
    }

    /**
     * @param BannedProductsWeekNumberSetting[] $bannedProductsWeekNumberSetting
     */
    public function setBannedProductsWeekNumber(array $bannedProductsWeekNumberSetting)
    {
        $this->bannedProductsWeekNumber = [];
        foreach ($bannedProductsWeekNumberSetting as $v) {
            $this->bannedProductsWeekNumber[$v->getProductId()] = $v;
        }
    }

    /**
     * @param ProductPropDisableCondition[] $propsDisableCondition
     */
    public function setPropsDisableCondition(array $propsDisableCondition)
    {
        $this->propsDisableCondition = [];
        foreach ($propsDisableCondition as $prop) {
            $this->propsDisableCondition[$prop->getPropCode()][$prop->getPropValue()] = $prop;
        }
    }

    public function getPropsDisableCondition(): array
    {
        $res = [];
        foreach ($this->propsDisableCondition as $propCode => $propDisableCondition) {
            foreach ($propDisableCondition as $propValue => $obj) {
                $res[] = $obj;
            }
        }
        return $res;
    }

    public function setPropsByInterval(array $propsByInterval)
    {
        $this->propsByInterval = [];
        foreach ($propsByInterval as $prop) {
            $this->propsByInterval[$prop->getPropCode()][$prop->getPropValue()] = $prop;
        }
    }

    public function getPropsByInterval(): array
    {
        $res = [];
        foreach ($this->propsByInterval as $propCode => $propDisableCondition) {
            foreach ($propDisableCondition as $propValue => $obj) {
                $res[] = $obj;
            }
        }
        return $res;
    }

    /**
     * @param DeliveryTimeProduct $deliveryTimeProduct
     * @param DaySetting[] $daysSetting
     */
    public function applyProductConditionsToDaysSetting(DeliveryTimeProduct $deliveryTimeProduct, array $daysSetting)
    {
        if (!$daysSetting) {
            return;
        }

        $productDcs = $this->disableConditionsForDeliveryTimeProduct($deliveryTimeProduct);

        if ($productDcs) {
            foreach ($daysSetting as $daySetting) {
                $daySetting->applyTimeSlotDisableConditions($productDcs);
            }
        }
    }

    /**
     * Применяем все возможные ProductConditions к дням
     * @param DaySetting[] $daysSetting
     */
    public function applyAllProductConditionsToDaysSetting(array $daysSetting){
        $propDCs = [];
        foreach ($this->propsDisableCondition as $props) {
            foreach ($props as $propCond) {
                $propDCs = array_merge($propDCs,$propCond->getTimeSlotDisableConditions());
            }
        }

        foreach ($daysSetting as $daySetting) {
            $daySetting->applyTimeSlotDisableConditions($propDCs);
        }
        
    }


    /**
     * @param DeliveryTimeProduct $deliveryTimeProduct
     * @return TimeSlotDisableCondition[]
     */
    public function disableConditionsForDeliveryTimeProduct(DeliveryTimeProduct $deliveryTimeProduct): array
    {
        $resDCs = [];

        foreach ($this->propsDisableCondition as $props) {
            foreach ($props as $propCond) {
                $prodPropValue = $deliveryTimeProduct->getPropValue($propCond->getPropCode());
                if ($prodPropValue == $propCond->getPropValue()) {
                    $resDCs = array_merge($resDCs,$propCond->getTimeSlotDisableConditions());
                }
            }
        }

        if ($this->isBannedProductsWeekNumber($deliveryTimeProduct->getId())) {
            $weekNumberSetting = $this->getBannedProductWeekNumber($deliveryTimeProduct->getId());
            if ($weekNumberSetting) {
                foreach ($weekNumberSetting->getBannedWeekNumbers() as $bannedWeekNumber) {
                    $bannedDC = TimeSlotDisableCondition::NewFromType(TimeSlotDisableCondition::WEEK_NUMBER_TYPE, $bannedWeekNumber);
                    if ($bannedDC){
                        $resDCs[] = $bannedDC;
                    }
                }
            }
        }

        return $resDCs;
    }

    protected function isBannedProductsWeekNumber(int $productId): bool
    {
        return isset($this->bannedProductsWeekNumber[$productId]);
    }

    protected function getBannedProductWeekNumber(int $productId): ?BannedProductsWeekNumberSetting
    {
        return $this->bannedProductsWeekNumber[$productId] ?? null;
    }
}
