<?

namespace Custom\DeliveryTime\Settings;

class BannedDaysSetting
{
    protected array $bannedDays = [];

    public function __construct()
    {
    }

    public function addBannedDay(\DateTime $dayDateTime)
    {
        $Y = (int)$dayDateTime->format('Y');
        $m = (int)$dayDateTime->format('m');
        $d = (int)$dayDateTime->format('d');
        $this->bannedDays[$Y][$m][$d] = true;
    }

    public function isBannedDay(int $d, int $m, int $Y): bool
    {
        return isset($this->bannedDays[$Y][$m][$d]);
    }

    /**
     * @return \DateTime[]
     */
    public function getBannedDays(): array
    {
        $res = [];
        if ($this->bannedDays) {
            foreach ($this->bannedDays as $Y => $infoM) {
                foreach ($infoM as $m => $infoD) {
                    foreach ($infoD as $d => $true) {
                        $res[] = \DateTime::createFromFormat('!d.m.Y', "$d.$m.$Y");
                    }
                }
            }
        }
        return $res;
    }
}
