<?

namespace Custom\DeliveryTime;

class DisableConditionByProvider
{
    public $propType;
    public $propValue;

    function __construct($propType, $propValue){
        $this->propType = $propType;
        $this->propValue = $propValue;
    }
}