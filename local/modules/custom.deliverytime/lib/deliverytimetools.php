<?

namespace Custom\DeliveryTime;

use Custom\DeliveryTime\Models\DeliveryTimeProduct;
use Custom\DeliveryTime\Settings\DaySetting;
use Helpers\CurrentCart;
use Custom\DeliveryTime\Models\UsedSlots;

class DeliveryTimeTools
{
    protected static ?DeliveryTimeTools $instance = null;
    protected int $currentTimeStamp;

    protected function __construct()
    {
        $this->init();
    }

    protected function init()
    {
        //tests ?dev&ts=30.05.2020 06:00:01 на странице заказа, или просто ?dev&ts для вывода текущего времеми
        if (isset($_GET['dev'], $_GET['ts'])) {
            if (trim($_GET['ts'])) {
                $this->currentTimeStamp = \DateTime::createFromFormat('d.m.Y H:i:s', $_GET['ts'])->getTimestamp();
            }
            echo '<div style="" class="test">текущая дата и время ' . date("Y-m-d D H:i:s", $currentTimeStamp) . '</div>';
        } else {
            $this->currentTimeStamp = time();
        }
    }

    public function getCurrentTimeStamp(): int
    {

        return $this->currentTimeStamp;
    }

    /**
     * @return DeliveryTimeTools
     */
    public static function getInstance(): DeliveryTimeTools
    {
        if (!static::$instance) {
            static::$instance = new self();
        }
        return static::$instance;
    }
}