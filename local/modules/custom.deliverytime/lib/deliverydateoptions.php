<?

namespace Custom\DeliveryTime;

use Custom\DeliveryTime\Models\UsedSlots;
use Custom\DeliveryTime\Models\TimeSlot;
use Custom\DeliveryTime\Models\ProductPropDisableCondition;
use Custom\DeliveryTime\Settings\DaySetting;
use Custom\DeliveryTime\Settings\BannedProductsWeekNumberSetting;
use Custom\DeliveryTime\Settings\HiddenDaysSetting;
use Custom\DeliveryTime\Settings\SpecialDaysSetting;
use Custom\DeliveryTime\Settings\ProductConditionSetting;
use Custom\DeliveryTime\Settings\BannedDaysSetting;
use Custom\DeliveryTime\Settings\WeekSetting;
use Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition;

class DeliveryDateOptions
{
    protected static ?DeliveryDateOptions $instance = null;

    public const DAYS_SHOW_NUMBER = 6;
    protected int $orderTrackingTS = 14 * 24 * 3600;//за какой период смотрим зарезервированные слоты
    protected string $orderTrackingFilePath = __DIR__ . '/DeliveryDateAvailableOrderTracking.txt';
    protected string $defaultTMSSlot = '10:00-23:00';

    protected const DEFAULT_TMS_SLOT = '10:00-23:00';

    protected UsedSlots $usedSlots;
    protected ProductConditionSetting $productConditionSetting;


    /**
     * массив настроек по умолчанию, берется если не было сохранено других настроек через админку
     */
    protected array $defaultOptions = [
        'weekDays' => [
            0 => [
                "weekNumber" => 0,
                'name' => 'Вс',
                'disabled' => false,
                'timeSlots' => [
                    [
                        'name' => 'любое время',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 14 * 3600
                            ]
                    ],
                    [
                        'name' => '11:00-13:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '13:00-15:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '15:00-17:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '17:00-19:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 6
                            ]
                    ],
                    [
                        'name' => '19:00-21:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ],
                    [
                        'name' => '21:00-23:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ]
                ]
            ],
            1 => [
                "weekNumber" => 1,
                'name' => 'Пн',
                'disabled' => false,
                'timeSlots' => [
                    [
                        'name' => 'любое время',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600
                            ]
                    ],
                    [
                        'name' => '11:00-13:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '13:00-15:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '15:00-17:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '17:00-19:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 6
                            ]
                    ],
                    [
                        'name' => '19:00-21:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ],
                    [
                        'name' => '21:00-23:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ]
                ]
            ],
            2 => [
                "weekNumber" => 2,
                'name' => 'Вт',
                'disabled' => false,
                'timeSlots' => [
                    [
                        'name' => 'любое время',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600
                            ]
                    ],
                    [
                        'name' => '11:00-13:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '13:00-15:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '15:00-17:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '17:00-19:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 6
                            ]
                    ],
                    [
                        'name' => '19:00-21:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ],
                    [
                        'name' => '21:00-23:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ]
                ]
            ],
            3 => [
                "weekNumber" => 3,
                'name' => 'Ср',
                'disabled' => false,
                'timeSlots' => [
                    [
                        'name' => 'любое время',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600
                            ]
                    ],
                    [
                        'name' => '11:00-13:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '13:00-15:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '15:00-17:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '17:00-19:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 6
                            ]
                    ],
                    [
                        'name' => '19:00-21:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ],
                    [
                        'name' => '21:00-23:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ]
                ]
            ],
            4 => [
                "weekNumber" => 4,
                'name' => 'Чт',
                'disabled' => false,
                'timeSlots' => [
                    [
                        'name' => 'любое время',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600
                            ]
                    ],
                    [
                        'name' => '11:00-13:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '13:00-15:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '15:00-17:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '17:00-19:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 6
                            ]
                    ],
                    [
                        'name' => '19:00-21:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ],
                    [
                        'name' => '21:00-23:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ]
                ]
            ],
            5 => [
                "weekNumber" => 5,
                'name' => 'Пт',
                'disabled' => false,
                'timeSlots' => [
                    [
                        'name' => 'любое время',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600
                            ]
                    ],
                    [
                        'name' => '11:00-13:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '13:00-15:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '15:00-17:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '17:00-19:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 6
                            ]
                    ],
                    [
                        'name' => '19:00-21:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ],
                    [
                        'name' => '21:00-23:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ]
                ]
            ],
            6 => [
                "weekNumber" => 6,
                'name' => 'Сб',
                'disabled' => false,
                'timeSlots' => [
                    [
                        'name' => 'любое время',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600
                            ]
                    ],
                    [
                        'name' => '11:00-13:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '13:00-15:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '15:00-17:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '17:00-19:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12.5 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 6
                            ]
                    ],
                    [
                        'name' => '19:00-21:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ],
                    [
                        'name' => '21:00-23:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 17 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 9
                            ]
                    ]
                ]
            ]
        ],
        'productProps' => [
            [
                'propCode' => 'BAKES',
                'propValue' => '1',
                'disableConditions' =>
                    [
                        ["type" => TimeSlotDisableCondition::DAY_DELAY_TYPE, "value" => 1],
                        ["type" => TimeSlotDisableCondition::MIN_HOUR_TYPE, "value" => 12],
                    ],
            ],
            [
                'propCode' => 'SUPPLIER_ID',
                'propValue' => 'Continent',
                'disableConditions' =>
                    [
                        ["type" => TimeSlotDisableCondition::DAY_DELAY_TYPE, "value" => 1],
                        ["type" => TimeSlotDisableCondition::MIN_HOUR_TYPE, "value" => 14],
                        ["type" => TimeSlotDisableCondition::WEEK_NUMBER_TYPE, "value" => 6],
                        ["type" => TimeSlotDisableCondition::WEEK_NUMBER_TYPE, "value" => 0],
                    ],
            ],
        ],
        'productPropsByInterval' => [
        ],
        'specialDays' => [
            [
                'dateInfo' => [
                    'year' => '2021',
                    'month' => '12',
                    'day' => '31'
                ], 'name' => 'Пт',
                'disabled' => false,
                'timeSlots' => [
                    [
                        'name' => 'любое время',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12 * 3600
                            ]
                    ],
                    [
                        'name' => '11:00-13:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '13:00-15:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 9 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                    [
                        'name' => '15:00-17:00',
                        'disabled' => false,
                        'disableConditions' =>
                            [
                                TimeSlotDisableCondition::TIME_DIFFERENCE_TYPE => 12 * 3600,
                                TimeSlotDisableCondition::MAX_USED_TIMES_TYPE => 8
                            ]
                    ],
                ]
            ]
        ],
        'hiddenDates' => [
            [
                'day' => '01',
                'month' => '01',
                'year' => '2022'
            ],
            [
                'day' => '02',
                'month' => '01',
                'year' => '2022'
            ],
            [
                'day' => '03',
                'month' => '01',
                'year' => '2022'
            ]
        ],
        'bannedDates' => [
            [
                'day' => '07',
                'month' => '01',
                'year' => '2022'
            ],
        ]
    ];

    /**
     * todo переписать на универсальные условия для конкретного товара
     * товары, которые нельзя закупить на выходных
     * @var string[][]
     */
    protected static array $bannedProductsWeekNumber = [
        [
            'productId' => '46612',//Пирог брусничный
            'bannedWeekNumbers' => [6, 0] //сб,вс
        ],
        [
            'productId' => '46613', //Курник с мясом птицы
            'bannedWeekNumbers' => [6, 0] //сб,вс
        ],
        [
            'productId' => '46614', //Пирог с мясом Бурек
            'bannedWeekNumbers' => [6, 0] //сб,вс
        ],
    ];

    protected array $options;

    protected function __construct()
    {
        $this->init();
    }

    protected function init()
    {
        $this->usedSlots = new UsedSlots();
        $this->productConditionSettingInit();
    }

    protected function productConditionSettingInit()
    {
        //\COption::SetOptionString("custom.deliverytime", "productProps", "");
        $this->productConditionSetting = new ProductConditionSetting();
        $bannedProductsWeekNumberSettings = [];
        foreach (self::$bannedProductsWeekNumber as $bannedInfo) {
            $bannedProductsWeekNumberSettings[] = new BannedProductsWeekNumberSetting($bannedInfo['productId'], $bannedInfo['bannedWeekNumbers']);
        }
        $this->productConditionSetting->setBannedProductsWeekNumber($bannedProductsWeekNumberSettings);
        $propsDCs = [];
        if(\Helpers\CustomTools::isFastDeliveryEnabled()) {
            $timeSlotsDCs = [
                TimeSlotDisableCondition::NewFromType(
                    \Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition::SPECIAL_TYPE,
                    \Helpers\Constants::EXPRESS_DISABLED_CONDITION_VALUE
                )
            ];
            $propsDCs[] = new ProductPropDisableCondition(\Helpers\Constants::PROP_FAST_DELIVERY, \Helpers\Constants::PROP_FAST_DELIVERY_VALUE_NORMAL, $timeSlotsDCs);
            $propsDCs[] = new ProductPropDisableCondition(\Helpers\Constants::PROP_FAST_DELIVERY, \Helpers\Constants::PROP_FAST_DELIVERY_VALUE_NORMAL_EMPTY, $timeSlotsDCs);
            $propsDCs[] = new ProductPropDisableCondition(\Helpers\Constants::PROP_FAST_DELIVERY, \Helpers\Constants::PROP_FAST_DELIVERY_VALUE_FAST, $timeSlotsDCs);
        }
        $productPropsSettings = $this->getOptionData('productProps');
        foreach ($productPropsSettings as $setting) {
            $propCode = $setting['propCode'];
            $propValue = $setting['propValue'];
            $timeSlotsDCs = [];
            foreach ($setting['disableConditions'] as $disableConditions) {
                $newTSCond = TimeSlotDisableCondition::NewFromType($disableConditions['type'], $disableConditions['value']);
                if ($newTSCond) {
                    $timeSlotsDCs[] = $newTSCond;
                }
            }
            if ($timeSlotsDCs) {
                $propsDCs[] = new ProductPropDisableCondition($propCode, $propValue, $timeSlotsDCs);
            }
        }
        $this->productConditionSetting->setPropsDisableCondition($propsDCs);

        $propsDCs = [];
        $productPropsSettings = json_decode($this->getOptionData('productPropsByInterval'), 1);

        foreach ($productPropsSettings as $setting) {
            $propCode = $setting['propCode'];
            $propValue = $setting['propValue'];
            $timeSlotsDCs = [];

            foreach($setting['disableConditions'] as $key=>$value) {
                if (!empty($setting['disableConditions'][$key]["DayOfWeek"])) {
                    $timeSlotsDCs[$key]["DayOfWeek"] = $setting['disableConditions'][$key]["DayOfWeek"]["value"];
                }
                if (!empty($setting['disableConditions'][$key]["TimeFrom"])) {
                    $timeSlotsDCs[$key]["TimeFrom"] = $setting['disableConditions'][$key]["TimeFrom"]["value"];
                }
                if (!empty($setting['disableConditions'][$key]["TimeTo"])) {
                    $timeSlotsDCs[$key]["TimeTo"] = $setting['disableConditions'][$key]["TimeTo"]["value"];
                }
                if (!empty($setting['disableConditions'][$key]["TimeFormat"])) {
                    $timeSlotsDCs[$key]["TimeFormat"] = $setting['disableConditions'][$key]["TimeFormat"]["value"];
                }
                if (!empty($setting['disableConditions'][$key]["TimeSlot"])) {
                    $timeSlotsDCs[$key]["TimeSlot"] = $setting['disableConditions'][$key]["TimeSlot"]["value"];
                }
            }
            if ($timeSlotsDCs) {
                $propsDCs[] = new ProductPropDisableCondition($propCode, $propValue, $timeSlotsDCs);
            }
        }
        $this->productConditionSetting->setPropsByInterval($propsDCs);
    }

    protected function getOptionData($optionName)
    {
        $fromOptions = \COption::GetOptionString("custom.deliverytime", $optionName, false);
        if ($fromOptions) {
            $res = json_decode($fromOptions, 1);
            if (!json_last_error()) {
                return $res;
            }
        }
        return $this->defaultOptions[$optionName] ?? [];
    }

    /**
     * @return self
     */
    public static function getInstance(): DeliveryDateOptions
    {
        if (!static::$instance) {
            static::$instance = new self();
        }
        return static::$instance;
    }

    public function getOrderTrackingTS(): int
    {
        return $this->orderTrackingTS;
    }

    public function getOrderTrackingFilePath(): string
    {
        return $this->orderTrackingFilePath;
    }

    public function getDefaultTMSSlot(): string
    {
        return $this->defaultTMSSlot;
    }

    public function getWeekSetting(): WeekSetting
    {
        $weekDays = [];
        $weekDaysInfo = $this->getOptionData('weekDays');
        
        foreach ($weekDaysInfo as $dayInfo) {
            $weekNumber = $dayInfo['weekNumber'];
            if(\Helpers\CustomTools::isFastDeliveryEnabled() && $weekNumber === (int)(new \Bitrix\Main\Type\DateTime())->format('N')) {
                $expressSlotSettings = $this->getExpressSlotSetting();
                $expressSlotSettings['disableConditions'][] = [
                    'type' => \Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition::SPECIAL_TYPE,
                    'value' => \Helpers\Constants::EXPRESS_DISABLED_CONDITION_VALUE,
                ];
                array_unshift($dayInfo['timeSlots'], $expressSlotSettings);
            }
            
            $timeSlots = $this->createTimeSlotsFromOptions($dayInfo['timeSlots']);
            $weekDays[] = new DaySetting((int)$weekNumber, $timeSlots, $dayInfo['disabled']);
        }

        return new WeekSetting($weekDays);
    }

    public function getWeekSettingForAdminPage(): WeekSetting
    {
        $weekDays = [];
        $weekDaysInfo = $this->getOptionData('weekDays');

        foreach ($weekDaysInfo as $dayInfo) {
            $weekNumber = $dayInfo['weekNumber'];
            $timeSlots = $this->createTimeSlotsFromOptions($dayInfo['timeSlots']);
            $daySetting = DaySetting::NewEmpty();
            $daySetting->setWeekNumberOnly((int)$weekNumber);
            $daySetting->setTimeSlots($timeSlots);
            $daySetting->setDisabled((bool)$dayInfo['disabled']);
            $weekDays[] = $daySetting;
        }

        return new WeekSetting($weekDays);
    }

    public function getSpecialDaysSetting(): SpecialDaysSetting
    {
        $specDaysSetting = new SpecialDaysSetting();
        $specialDaysInfo = $this->getOptionData('specialDays');
        foreach ($specialDaysInfo as $dayInfo) {
            $timeSlots = $this->createTimeSlotsFromOptions($dayInfo['timeSlots']);

            $dateTime = \DateTime::createFromFormat('!d.m.Y', "{$dayInfo['dateInfo']['day']}.{$dayInfo['dateInfo']['month']}.{$dayInfo['dateInfo']['year']}");
            if ($dateTime) {
                $daySetting = DaySetting::NewFromDate($dateTime, $timeSlots, $dayInfo['disabled']);
                $specDaysSetting->addDaySetting($daySetting);
            }
        }

        return $specDaysSetting;
    }

    public function getSpecialDaysSettingForAdminPage(): SpecialDaysSetting
    {
        $specDaysSetting = new SpecialDaysSetting();
        $specialDaysInfo = $this->getOptionData('specialDays');
        foreach ($specialDaysInfo as $dayInfo) {
            $timeSlots = $this->createTimeSlotsFromOptions($dayInfo['timeSlots']);

            $dateTime = \DateTime::createFromFormat('!d.m.Y', "{$dayInfo['dateInfo']['day']}.{$dayInfo['dateInfo']['month']}.{$dayInfo['dateInfo']['year']}");
            if ($dateTime) {
                $daySetting = DaySetting::NewEmpty();
                $daySetting->setDateTimeOnly($dateTime);
                $daySetting->setTimeSlots($timeSlots);
                $daySetting->setDisabled((bool)$dayInfo['disabled']);
                $specDaysSetting->addDaySetting($daySetting);
            }
        }
        return $specDaysSetting;
    }

    protected function createTimeSlotsFromOptions($timeSlotsInfo): array
    {
        $timeSlots = [];
        foreach ($timeSlotsInfo as $timeSlotInfo) {
            $disableConditions = [];
            foreach ($timeSlotInfo['disableConditions'] as $type => $value) {
                $newDC = null;
                if (isset($value['type'], $value['value'])) {
                    //для данных из Options базы данных
                    $newDC = TimeSlotDisableCondition::NewFromType((string)$value['type'], $value['value']);
                } else {
                    //для данных из $this->defaultOptions
                    $newDC = TimeSlotDisableCondition::NewFromType((string)$type, $value);
                }
                if ($newDC) {
                    $disableConditions[] = $newDC;
                }
            }
            $timeSlots[] = new TimeSlot($timeSlotInfo['name'], $disableConditions, $timeSlotInfo['disabled']);
        }
        return $timeSlots;
    }

    public function getHiddenDaysSetting(): HiddenDaysSetting
    {
        $hiddenDays = new HiddenDaysSetting();
        $hiddenDates = $this->getOptionData('hiddenDates');
        foreach ($hiddenDates as $date) {
            $dateTime = \DateTime::createFromFormat('!d.m.Y', "{$date['day']}.{$date['month']}.{$date['year']}");
            if ($dateTime) {
                $hiddenDays->addHiddenDay($dateTime);
            }
        }
        return $hiddenDays;
    }

    public function getBannedDaysSetting(): BannedDaysSetting
    {
        $bannedDays = new BannedDaysSetting();
        $bannedDates = $this->getOptionData('bannedDates');
        foreach ($bannedDates as $date) {
            $dateTime = \DateTime::createFromFormat('!d.m.Y', "{$date['day']}.{$date['month']}.{$date['year']}");
            if ($dateTime) {
                $bannedDays->addBannedDay($dateTime);
            }
        }
        return $bannedDays;
    }
    
    public function getExpressSlotSetting()
    {
        return $this->getOptionData('expressSlot');
    }

    public function getUsedSlots(): UsedSlots
    {
        return $this->usedSlots;
    }

    public function getProductConditionSetting(): ProductConditionSetting
    {
        return $this->productConditionSetting;
    }

    public static function slotToTMSFormat($timeSlot)
    {
        if (preg_match('/^\d/', $timeSlot)) {
            return $timeSlot;
        }
        return self::DEFAULT_TMS_SLOT;
    }
}