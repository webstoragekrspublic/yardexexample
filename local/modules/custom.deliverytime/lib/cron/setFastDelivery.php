<?
// обновляем свойство "FAST_DELIVERY"
use Custom\DeliveryTime\DeliveryDateOptions;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
set_time_limit(1000);

$fastDeliveryPropCode = "FAST_DELIVERY";

$deliveryProductSetting = DeliveryDateOptions::getInstance()->getProductConditionSetting();

$productsQ = \CIBlockElement::GetList(
    ['ID' => 'ASC'],
    [
        'IBLOCK_ID' => \Helpers\Constants::PRODUCTS_IBLOCK_ID,
        'ACTIVE' => 'Y'
    ],
    false,
    false,
    ['ID', 'PROPERTY_*']
);

while ($row = $productsQ->GetNextElement()) {
    $deliveryTimeProduct = \Custom\DeliveryTime\Models\DeliveryTimeProduct::NewFromDBRow($row);
    if ($deliveryTimeProduct) {
        $newFastDeliveryValue = "1";

        $dcs = $deliveryProductSetting->disableConditionsForDeliveryTimeProduct($deliveryTimeProduct);
        if ($dcs) {
            $newFastDeliveryValue = "0";
        }
        if($deliveryTimeProduct->getPropValue(\Helpers\Constants::PROP_DELIVERY_TIME_SLOT_CONDITION) === 'express') {
            $newFastDeliveryValue = \Helpers\Constants::PROP_FAST_DELIVERY_VALUE_EXPRESS;
        }
        $currentFastDeliveryValue = $deliveryTimeProduct->getPropValue($fastDeliveryPropCode);
        if ($newFastDeliveryValue !== $currentFastDeliveryValue){
            CIBlockElement::SetPropertyValuesEx($deliveryTimeProduct->getId(), \Helpers\Constants::PRODUCTS_IBLOCK_ID, [$fastDeliveryPropCode => $newFastDeliveryValue]);
            var_dump($deliveryTimeProduct->getId().' - updated!');
        }
    }
}

echo 'uspeh!';

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>