<?
// изменение настроек модуля custom.deliverytime для Континента по планировщику, во все дни, кроме субботы и воскресенья
// до 16 устанавливать параметр "минимальный час на текущий и следующий день": 0
// после 16 устанавливать параметр "минимальный час на текущий и следующий день": 15
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Custom\DeliveryTime\SlotsByProvider;
use Custom\DeliveryTime\DisableConditionByProvider;

$arProductProps = json_decode(COption::GetOptionString("custom.deliverytime", 'productProps'), true);

$currentTS = time();
//$currentTS = strtotime(date("Y-m-d 18:00:00"));

const SATURDAY = 6;
const SUNDAY = 7;
foreach ($arProductProps as $propIndex => $prop) {
    if (isset($prop["propValue"])) {

        if($prop["propCode"] == "SUPPLIER_ID"){
            $providers[$prop["propValue"]][] = $prop["propValue"];
        }

        // "Код поставщика" == Continent, изменяем параметры по времени
        if ($prop["propValue"] == "Continent") {
            foreach ($prop["disableConditions"] as $DCindex => $dcInfo) {
                if ($dcInfo["type"] == "MinHour") {
                    $limitTS = strtotime(date("Y-m-d 16:45:00"));
                    if ($currentTS < $limitTS) {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 0;
                    } else {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 15;
                    }
                    if ((date("N") == SATURDAY) || (date("N") == SUNDAY)) {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 15;
                    }
                    break;
                }
            }
        }
        
        if ($prop["propValue"] == "Vkysnoe_ribnoe") {
            foreach ($prop["disableConditions"] as $DCindex => $dcInfo) {
                if ($dcInfo["type"] == "MinHour") {
                    $limitTS = strtotime(date("Y-m-d 16:45:00"));
                    if ($currentTS < $limitTS) {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 0;
                    } else {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 15;
                    }
                    if ((date("N") == SATURDAY) || (date("N") == SUNDAY)) {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 15;
                    }
                    break;
                }
            }
        }
        
        if ($prop["propValue"] == "Myasoegka") {
            foreach ($prop["disableConditions"] as $DCindex => $dcInfo) {
                if ($dcInfo["type"] == "MinHour") {
                    $limitTS = strtotime(date("Y-m-d 15:00:00"));
                    if ($currentTS < $limitTS) {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 0;
                    } else {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 15;
                    }
                    if ((date("N") == SATURDAY) || (date("N") == SUNDAY)) {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 15;
                    }
                    break;
                }
            }
        }

        // "Условие Доставки" == next_day, изменяем параметры по времени
        if ($prop["propCode"] == "DELIVERY_TIME_SLOT_CONDITION" && $prop["propValue"] == "next_day") {
            foreach ($prop["disableConditions"] as $DCindex => $dcInfo) {
                $limitTS = strtotime(date("Y-m-d 16:50:00"));
                if($dcInfo["type"] == "MinHour"){
                    if ($currentTS < $limitTS) {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 0;
                    } else {
                        $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = 15;
                    }
                }
            }
        }
    }
}

$arProductPropsByInterval = json_decode(json_decode(COption::GetOptionString("custom.deliverytime", 'productPropsByInterval'), true), true);
foreach ($arProductPropsByInterval as $propIndex => $prop) {
    foreach($prop["disableConditions"] as $key=>$dc){
        foreach($dc as $type=>$value){
            $disableCondition[$key][$value["type"]] = new DisableConditionByProvider($value["type"], $value["value"]);
        }
    }
    $settingsByProvider[] = new SlotsByProvider($prop["propCode"], $prop["propValue"], $disableCondition);
}

foreach ($settingsByProvider as $key => $setting) {
    if (isset($setting->propValue)) {


        $info = $setting->getTimeSlotInfo();

        //pr($info);
        
        // Генерация свойств, если есть данные из слотов поставщиков
        if(!in_array($setting->propValue, $providers[$setting->propValue])){
            $providers[$setting->propValue] = $setting->propValue;
            $arProductProps[] = Array(
                "propCode" => $setting->propCode,
                "propValue" => $setting->propValue,
                "disableConditions" => Array
                    (
                        0 => Array
                            (
                                "type" => "MinHour",
                                "value" => $info["MinHour"]
                            ),
                        1 => Array
                            (

                                "type" => "DayDelay",
                                "value" => $info["DayDelay"]
                            )

                    )

            );
        }
        else{
            // Устанавливаем время исходя из слотов поставщиков
            foreach ($arProductProps as $propIndex => $prop) {
                if($prop["propValue"] == $setting->propValue){
                    foreach ($prop["disableConditions"] as $DCindex => $dcInfo) {
                        if ($dcInfo["type"] == "MinHour") {
                            $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = $info["MinHour"];
                        }
                        else if($dcInfo["type"] == "DayDelay"){
                            $arProductProps[$propIndex]["disableConditions"][$DCindex]["value"] = $info["DayDelay"];
                        }
                    }
                }
            }
        }
    }
}

COption::SetOptionString("custom.deliverytime", 'productProps', json_encode($arProductProps));