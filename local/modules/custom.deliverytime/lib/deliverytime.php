<?

namespace Custom\DeliveryTime;

use Custom\DeliveryTime\Models\DeliveryTimeProduct;
use Custom\DeliveryTime\Settings\DaySetting;
use Helpers\CurrentCart;
use Custom\DeliveryTime\Models\UsedSlots;

class DeliveryTime
{
    protected static ?DeliveryTime $instance = null;

    protected function __construct()
    {
        $this->init();
    }

    protected function init()
    {
    }

    public function updateUsedSlots()
    {
        UsedSlots::updateUsedSlots();
    }

    /**
     * определяем слоты доставки для текущей корзины
     * @param int|null $nextDayCount
     * @return array
     */
    public function getDaysInfoForCurrentCart(int $nextDayCount = null): array
    {
        $cartProducts = CurrentCart::getInstance()->getCartProducts();
        $productIds = [];
        foreach ($cartProducts as $cartProduct) {
            $productIds[] = $cartProduct->getField('PRODUCT_ID');
        }
        $deliveryTimeProducts = DeliveryTimeProduct::NewFromProductsId($productIds);
        return Serializer::daysSettingToArr(DeliveryDateAvailable::getDaysInfoForProducts($deliveryTimeProducts, $nextDayCount));
    }


    /**
     * @param int $productId
     * @return array
     */
    public function getShortlyDayInfoForProductId(int $productId): array
    {
        $deliveryTimeProduct = DeliveryTimeProduct::NewFromProductId($productId);
        return $this->getShortlyDayInfoForProduct($deliveryTimeProduct);
    }

    /**
     * @param array $productIds
     * @return array
     */
    public function getShortlyDayInfoForProductIds(array $productIds): array
    {
        $deliveryTimeProducts = DeliveryTimeProduct::NewFromProductsId($productIds);
        $res = [];
        foreach ($deliveryTimeProducts as $dtProduct) {
            $res[$dtProduct->getId()] = $this->getShortlyDayInfoForProduct($dtProduct);
        }
        return $res;
    }

    /**
     * определяет минимальную дату доставки для конкретного товара
     * @param DeliveryTimeProduct $deliveryTimeProduct
     * @return array
     */
    public function getShortlyDayInfoForProduct(DeliveryTimeProduct $deliveryTimeProduct): array
    {
        return $this->getFirstNotDisabledDayInfo(DeliveryDateAvailable::getDaysInfoForProducts([$deliveryTimeProduct]));
    }

    /**
     * получаем ближайший день доставки без применения правил по товарам
     */
    public function getShortlyWeekDay(): array
    {
        return $this->getFirstNotDisabledDayInfo(DeliveryDateAvailable::getNextDaysSetting());
    }

    /**
     * получаем ближайший день доставки если бы были применены все TimeSlotDisableCondition из текущих настроек модуля.
     * Нужно, что бы выводить минимальныый срод доставки для самых сложных товаров
     */
    public function getShortlyWeekDayWithAllConditions(): array
    {
        $weekDaysSetting = DeliveryDateAvailable::getNextDaysSetting();
        
        
        $productSetting = DeliveryDateOptions::getInstance()->getProductConditionSetting();
        $productSetting->applyAllProductConditionsToDaysSetting($weekDaysSetting);

        return $this->getFirstNotDisabledDayInfo($weekDaysSetting);
    }

    /**
     * преобразуем массив дня доставки в строку доставки
     * $dayArr => Сегодня, 31 Января 11:00-13:00
     */
    public function dayArrToDeliveryInfo($dayArr): string
    {
        if (!is_array($dayArr) || !$dayArr['dateInfo']) {
            return '';
        }
        $dayInfoTxt = $dayArr['infoDate'] . ', ' . $dayArr['dateInfo']['day'] . ' ' . $dayArr['dateInfo']['monthText'];;

        $dayInfoTxt .= ' ' . $this->getFirstNotDisabledTimeSlot($dayArr['timeSlots']);
        return $dayInfoTxt;
    }

    /**
     * @param DaySetting[] $daysSetting
     * @return array
     */
    protected function getFirstNotDisabledDayInfo(array $daysSetting): array
    {
        $resDsInfo = [];
        

//        echo 123;
//        echo '<pre>';
//        print_r($daysSetting);
//        echo '</pre>';

        foreach ($daysSetting as $ds) {
            if (!$ds->isDisabled()) {
                $resDsInfo = Serializer::daySettingToArr($ds);
                break;
            }
        }

        return $resDsInfo;
    }

    /**
     * @param DaySetting[] $daysSetting
     * @return array
     */
    public function getFirstNotDisabledTimeSlot(array $timeSlots): string
    {
        $res = "";

        foreach ($timeSlots as $ts) {
            if (!$ts['disabled'] && preg_match('/^[\d]+/', $ts['time'])) {
                $res = $ts['time'];
                break;
            }
        }

        return $res;
    }

    public function getUsedSlots(): array
    {
        return DeliveryDateOptions::getInstance()->getUsedSlots()->getUsedSlots();
    }

    /**
     * @return self
     */
    public static function getInstance(): DeliveryTime
    {
        if (!static::$instance) {
            static::$instance = new self();
        }
        return static::$instance;
    }
}