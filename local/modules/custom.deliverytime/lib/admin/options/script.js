$(document).ready(function () {

    (function init() {
        initAdminPage();
        initFormSubmit();
    }());

    function escapeHtml(text) {
        if (typeof text !== "string") {
            return text;
        }
        var map = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };

        return text.replace(/[&<>"']/g, function (m) {
            return map[m];
        });
    }

    function initAdminPage() {
        $.ajax({
            url: "/local/modules/custom.deliverytime/lib/admin/options/ajax/currentOptions.php",
            dataType: 'json',
            success: function (data) {
                console.log(data)
                try {
                    disableConditions.setItems(data.disableConditionsInfo);
                    productAllProps.setItems(data.productAllProps);

                    weekBlock.setItems(data.week);
                    weekBlock.render()
                    bannedDatesBlock.setItems(data.bannedDates)
                    bannedDatesBlock.render()
                    hiddenDatesBlock.setItems(data.hiddenDates)
                    hiddenDatesBlock.render()
                    specialDays.setItems(data.specialDays)
                    specialDays.render()
                    productProps.setItems(data.productProps)
                    productProps.render();
                    weeksEnum.setItems(data.weeksEnum)

                    propsByProviders.setItems(data.productPropsByInterval);
                    propsByProviders.render();

                    helperBlock.render()

                    expressBlock.setItems(data.expressSlot)
                    expressBlock.render()

                } catch (e) {
                    alert('ОШИБКА! НЕ сохраняйте форму и обратитесь к разработчику!')
                    $('#form_deliverytime_save').remove();
                    console.log(e);
                }

                $('#form_deliverytime_save').removeAttr('disabled')
            },
            error: function () {
                alert('Ошибка загрузки заказа')
            }
        });
    }

    function initFormSubmit() {

        var $form = $('#form_deliverytime_save').closest('form');
        if (!$form.length) {
            alert('ошибка, форма не найдена');
            return;
        }
        $('#form_deliverytime_save').off('click');

        $form.off('submit').on('submit', function (e) {
            e.preventDefault();

            var data = {};
            for (var prop in sendData) {
                try {
                    data[prop] = sendData[prop].getItems();
                } catch (e) {
                    alert('ошибка при вызове метода getItems у объекта ' + prop + '! Обратитесь к разработчику.')
                    console.log(e);
                    return;
                }
            }
            console.log(data);
            $.ajax({
                data: JSON.stringify(data),
                url: "/local/modules/custom.deliverytime/lib/admin/options/ajax/setOptions.php",
                dataType: 'json',
                method: 'POST',
                contentType: 'application/json',
                success: function (data) {
                    alert(data.message);
                    if (!data.error){
                        document.location.reload();
                    }

                    $form.find('.adm-btn-load-img-green').remove();
                    $form.find('#form_deliverytime_save').removeAttr('disabled');
                    $form.find('#form_deliverytime_save').removeClass('adm-btn-load');
                },
                error: function () {
                    alert('Ошибка!')
                    $form.find('.adm-btn-load-img-green').remove();
                    $form.find('#form_deliverytime_save').removeAttr('disabled');
                    $form.find('#form_deliverytime_save').removeClass('adm-btn-load');
                }
            });
        })
    }

    function ProductAllProps() {
    }

    ProductAllProps.prototype.setItems = function (currentItems) {
        this.items = currentItems;
    }

    ProductAllProps.prototype.getItems = function (currentItems) {
        return this.items;
    }

    ProductAllProps.prototype.getPropByCode = function (code) {
        var res = {
            'ID': 0,
            'CODE': 'ERROR!!!',
            'NAME': 'ОШИБКА!!!',
        }
        for (var k = 0; k < this.items.length; k++) {
            var item = this.items[k];
            if (item.CODE == code) {
                res = item;
                break;
            }
        }

        return res;
    }

    ProductAllProps.prototype.getItems = function () {
        return this.items;
    }

    ProductAllProps.prototype.getOptionsForSelect = function () {
        var html = '';
        for (var k = 0; k < this.items.length; k++) {
            var item = this.items[k];
            html += `<option value="${escapeHtml(item.CODE)}" >${escapeHtml(item.NAME)}</option>`;
        }

        return html;
    }


    function DisableConditions() {
        this.items = [];
    }

    DisableConditions.prototype.setItems = function (currentItems) {
        this.items = currentItems;
    }

    DisableConditions.prototype.getItems = function (currentItems) {
        return this.items;
    }

    DisableConditions.prototype.getOptionsForSelect = function (selectedType) {
        var html = '';
        for (var k = 0; k < this.items.length; k++) {
            var item = this.items[k];
            var type = item.type;
            html += `<option value="${escapeHtml(type)}" ` + (selectedType === type ? 'selected' : '') + `>${escapeHtml(item.description)}</option>`;
        }

        return html;
    }


    function WeekBlock() {
        var renderBlockSelector = '#option_week_block';
        this.dataRolePrefix = 'week';
        this.$renderBlock = $(renderBlockSelector);
        if (!this.$renderBlock.length) {
            throw ('Отсутсвует блок ' + renderBlockSelector);
        }
    }

    WeekBlock.prototype.setItems = function (currentItems) {
        this.items = currentItems;
    }

    WeekBlock.prototype.getItems = function (currentItems) {
        return this.items;
    }

    WeekBlock.prototype.render = function () {
        var html = '';
        for (var dayIndex = 0; dayIndex < this.items.length; dayIndex++) {
            var item = this.items[dayIndex];
            html += this.dayHTML(item, dayIndex)
        }
        if (!this.items.length) {
            html += "<div>Нет данных</div>";
        }

        this.$renderBlock.html(html);
        this.addFormEvents();
    }

    WeekBlock.prototype.dayHTML = function (item, itemIndex) {
        var html = "<div>";
        html += "   <h2 class='options_weekname'>" + item.weekName + "</h2>";
        html += "   <label>Не доступен для доставки</label>";
        html += `   <input data-role='${this.dataRolePrefix}_setting_day_input_disable' data-dayindex='` + itemIndex + `'
                               type='checkbox' ` + (item.disabled ? 'checked' : '') + `>`;
        html += "   <h3>Временные слоты</h3>";
        for (var timeSlotIndex = 0; timeSlotIndex < item.timeSlots.length; timeSlotIndex++) {
            var timeSlot = item.timeSlots[timeSlotIndex];
            html += "<div class='options_timeslot_block'>";
            html += "   <div>";
            html += "       <label>название слота</label>";
            html += `       <input type='text' value='` + escapeHtml(timeSlot.name) + `'
                                       data-role='${this.dataRolePrefix}_setting_timeslot_input_name'
                                       data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'>`;
            html += "   </div>";
            html += "   <div>";
            html += "       <label>Не доступен для доставки</label>";
            html += `       <input type='checkbox' ` + (timeSlot.disabled ? 'checked' : '') + `
                                       data-role='${this.dataRolePrefix}_setting_timeslot_input_disable' 
                                       data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'>`;
            html += "   </div>";
            html += "   <div>";
            html += "       <div>";
            html += "           <h4>Условия блокировки</h4>";
            for (var dcIndex = 0; dcIndex < timeSlot.disableConditions.length; dcIndex++) {
                var disableCondition = timeSlot.disableConditions[dcIndex];
                html += "<div class='options_conditions_block'>";
                html += "   <label>Тип условия</label>";
                html += `   <select data-role='${this.dataRolePrefix}_setting_timeslot_dc_type'
                                         data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                         data-dcindex='` + dcIndex + `'>`
                html += disableConditions.getOptionsForSelect(disableCondition.type);
                html += "   </select>";
                html += "   <label>Значение</label>";
                html += `   <input value="${escapeHtml(disableCondition.value)}" data-role='${this.dataRolePrefix}_setting_timeslot_dc_name'
                                         data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                         data-dcindex='` + dcIndex + `'/>`;
                html += `   <div><button data-role='${this.dataRolePrefix}_setting_timeslot_dc_delete'
                                             data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                             data-dcindex='` + dcIndex + `'
                                             >Удалить условие</button></div>`;
                html += "</div>";
            }

            html += `       <div class='options_conditions_block'><button data-role='${this.dataRolePrefix}_setting_timeslot_dc_add'
                                             data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                             >Добавить условие</button></div>`;
            html += "       </div>";
            html += `       <div><button data-role='${this.dataRolePrefix}_setting_timeslot_delete'
                                             data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                             >Удалить слот</button></div>`;
            html += "   </div>";
            html += "</div>";
        }
        html += `    <div><button data-role='${this.dataRolePrefix}_setting_timeslot_add' data-dayindex='` + itemIndex + `'>Добавить слот</button></div>`;
        html += "</div>";
        return html;
    }

    WeekBlock.prototype.getItemByWeekNumber = function (weekNumber) {
        var res = [];
        for (var k = 0; k < this.items.length; k++) {
            var item = this.items[k];
            if (item.weekNumber == weekNumber) {
                res = item;
                break;
            }
        }
        return res;
    }

    WeekBlock.prototype.addFormEvents = function () {
        var self = this;
        $("[data-role='" + this.dataRolePrefix + "_setting_day_input_disable']").on('change', function (e) {
            var disabled = $(this).is(':checked');
            var dayIndex = $(this).data('dayindex');
            self.items[dayIndex].disabled = disabled;
            console.log(disabled)
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_input_disable']").on('change', function (e) {
            console.log('change');
            var disabled = $(this).is(':checked');
            var dayIndex = $(this).data('dayindex');
            var timeSlotIndex = $(this).data('timeslotindex');
            self.items[dayIndex].timeSlots[timeSlotIndex].disabled = disabled;
            console.log(disabled)
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_input_name']").on('change', function (e) {
            var newName = $(this).val();
            var dayIndex = $(this).data('dayindex');
            var timeSlotIndex = $(this).data('timeslotindex');
            self.items[dayIndex].timeSlots[timeSlotIndex].name = newName;
            console.log(newName)
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_delete']").on('click', function (e) {
            e.preventDefault();
            var dayIndex = $(this).data('dayindex');
            var timeSlotIndex = $(this).data('timeslotindex');
            var slotName = self.items[dayIndex].timeSlots[timeSlotIndex].name;
            if (window.confirm("удалить слот '" + slotName + "'?")) {
                self.items[dayIndex].timeSlots.splice(timeSlotIndex, 1);
                self.render()
            }
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_add']").on('click', function (e) {
            e.preventDefault();
            var dayIndex = $(this).data('dayindex');
            var newSlot = {
                name: '8:00-10:00',
                disabled: false,
                disableConditions: []
            }
            self.items[dayIndex].timeSlots.push(newSlot);
            console.log(self.items)
            self.render()
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_delete']").on('click', function (e) {
            e.preventDefault();
            var dayIndex = $(this).data('dayindex');
            var timeSlotIndex = $(this).data('timeslotindex');
            var dcIndex = $(this).data('dcindex');
            if (window.confirm("удалить условие?")) {
                self.items[dayIndex].timeSlots[timeSlotIndex].disableConditions.splice(dcIndex, 1);
                self.render()
            }
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_add']").on('click', function (e) {
            e.preventDefault();
            var dayIndex = $(this).data('dayindex');
            var timeSlotIndex = $(this).data('timeslotindex');
            var newDc = {
                type: disableConditions.items[0].type,
                value: 0
            }
            self.items[dayIndex].timeSlots[timeSlotIndex].disableConditions.push(newDc);
            self.render()
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_type']").on('change', function (e) {
            var newType = $(this).find(":selected").val();
            var dayIndex = $(this).data('dayindex');
            var timeSlotIndex = $(this).data('timeslotindex');
            var dcIndex = $(this).data('dcindex');
            self.items[dayIndex].timeSlots[timeSlotIndex].disableConditions[dcIndex].type = newType;
            console.log(newType)
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_name']").on('change', function (e) {
            var newValue = $(this).val();
            var dayIndex = $(this).data('dayindex');
            var timeSlotIndex = $(this).data('timeslotindex');
            var dcIndex = $(this).data('dcindex');
            self.items[dayIndex].timeSlots[timeSlotIndex].disableConditions[dcIndex].value = newValue;
            console.log(newValue)
        })

    }


    function BannedDatesBlock() {
        var renderBlockSelector = '#option_bannedDates_block';
        this.dataRolePrefix = 'banneddates';
        this.$renderBlock = $(renderBlockSelector);
        if (!this.$renderBlock.length) {
            throw ('Отсутсвует блок ' + renderBlockSelector);
        }
    }

    BannedDatesBlock.prototype.setItems = function (currentItems) {
        this.items = currentItems;
    }

    BannedDatesBlock.prototype.getItems = function (currentItems) {
        return this.items;
    }

    BannedDatesBlock.prototype.render = function () {
        var html = '';
        if (this.items.length) {
            html += '<div class="options_date_block">'
            for (var itemIndex = 0; itemIndex < this.items.length; itemIndex++) {
                var item = this.items[itemIndex];
                html += '<label class="options_date_block_label">День</label>'
                html += '<span>' + item.day + '</span>'
                html += '<label class="options_date_block_label">месяц</label>'
                html += '<span>' + item.month + '</span>'
                html += '<label class="options_date_block_label">год</label>'
                html += '<span>' + item.year + '</span>'
                html += `<div>
                            <button data-role='${this.dataRolePrefix}_setting_timeslot_delete' data-itemindex='` + itemIndex + `'>Удалить день</button>
                        </div>`;
            }
            html += '</div>';
        } else {
            html += '<div>нет данных</div>'
        }
        html += `   <div data-role='${this.dataRolePrefix}_setting_timeslot_add_block'>
                        <input name="day" value="" placeholder="день 01"/>
                        <input name="month" value="" placeholder="месяц 01"/>
                        <input name="year" value="" placeholder="год 2022"/>
                        <button data-role='${this.dataRolePrefix}_setting_timeslot_add'>Добавить день</button>
                    </div>`;

        this.$renderBlock.html(html);
        this.addFormEvents();
    }

    BannedDatesBlock.prototype.addFormEvents = function () {
        var self = this;
        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_delete']").on('click', function (e) {
            e.preventDefault();
            var itemindex = $(this).data('itemindex');
            if (window.confirm("удалить?")) {
                self.items.splice(itemindex, 1);
                self.render()
            }
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_add']").on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).closest("[data-role='" + self.dataRolePrefix + "_setting_timeslot_add_block']")
            if (!$parent.length) {
                alert('ошибка! не найден родитесльский элеменет');
                console.log($parent)
                return;
            }

            var day = parseInt($parent.find('input[name="day"]').val());
            var month = parseInt($parent.find('input[name="month"]').val());
            var year = parseInt($parent.find('input[name="year"]').val());

            if (isNaN(day) || day < 1 || day > 31) {
                alert('неверно введен день')
                return;
            }
            if (isNaN(month) || month < 1 || month > 12) {
                alert('неверно введен месяц')
                return;
            }
            if (isNaN(year) || year < 2000) {
                alert('неверно введен год')
                return;
            }
            var newItem = {
                day: ('0' + day).slice(-2), // '11','02'
                month: ('0' + month).slice(-2),
                year: year.toString()
            }
            self.items.push(newItem);
            self.render()
        })
    }


    function HiddenDatesBlock() {
        var renderBlockSelector = '#option_hiddenDates_block';
        this.dataRolePrefix = 'hiddendates';
        this.$renderBlock = $(renderBlockSelector);
        if (!this.$renderBlock.length) {
            throw ('Отсутсвует блок ' + renderBlockSelector);
        }
    }

    HiddenDatesBlock.prototype = Object.create(BannedDatesBlock.prototype);


    function SpecialDays() {
        var renderBlockSelector = '#option_specialDays_block';
        this.dataRolePrefix = 'specialdays';
        this.$renderBlock = $(renderBlockSelector);
        if (!this.$renderBlock.length) {
            throw ('Отсутсвует блок ' + renderBlockSelector);
        }
    }

    SpecialDays.prototype = Object.create(WeekBlock.prototype);

    SpecialDays.prototype.render = function () {
        var html = '';
        for (var dayIndex = 0; dayIndex < this.items.length; dayIndex++) {
            var item = this.items[dayIndex];
            html += `<div class="options_specialDays_date_block">
                        <label class="options_date_block_label">День</label><span>${item.dateInfo.day}</span>
                        <label class="options_date_block_label">месяц</label><span>${item.dateInfo.month}</span>
                        <label class="options_date_block_label">год</label><span>${item.dateInfo.year}</span>
                        <div>
                            <button data-role="${this.dataRolePrefix}_item_delete" data-itemindex="${dayIndex}">Удалить день</button>
                        </div>
                    </div>`;

            html += this.dayHTML(item, dayIndex)
        }

        if (!this.items.length) {
            html += "<div>Нет данных</div>";
        }

        html += `<div data-role="${this.dataRolePrefix}_item_add_block">
                    <input name="day" value="" placeholder="день 01">
                    <input name="month" value="" placeholder="месяц 01">
                    <input name="year" value="" placeholder="год 2022">
                    <button data-role="${this.dataRolePrefix}_item_add">Добавить день</button>
                </div>`;

        this.$renderBlock.html(html);
        this.addFormEvents();
        this.addFormSpecialDaysEvents();
    }

    SpecialDays.prototype.addFormSpecialDaysEvents = function () {
        var self = this;

        $("[data-role='" + this.dataRolePrefix + "_item_delete']").on('click', function (e) {
            e.preventDefault();
            var itemindex = $(this).data('itemindex');
            var item = self.items[itemindex];
            if (window.confirm(`удалить ${item.dateInfo.day}.${item.dateInfo.month}.${item.dateInfo.year}?`)) {
                self.items.splice(itemindex, 1);
                self.render()
            }
        })

        $("[data-role='" + this.dataRolePrefix + "_item_add']").on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).closest("[data-role='" + self.dataRolePrefix + "_item_add_block']")
            if (!$parent.length) {
                alert('ошибка! не найден родитесльский элеменет');
                console.log($parent)
                return;
            }

            var day = parseInt($parent.find('input[name="day"]').val());
            var month = parseInt($parent.find('input[name="month"]').val());
            var year = parseInt($parent.find('input[name="year"]').val());

            if (isNaN(day) || day < 1 || day > 31) {
                alert('неверно введен день')
                return;
            }
            if (isNaN(month) || month < 1 || month > 12) {
                alert('неверно введен месяц')
                return;
            }
            if (isNaN(year) || year < 2000) {
                alert('неверно введен год')
                return;
            }
            day = ('0' + day).slice(-2), // '11','02'
                month = ('0' + month).slice(-2),
                year = year.toString()

            var date = new Date(`${year}-${month}-${day}`)//new Date('1995-12-17T03:24:00');
            var weekNumber = date.getDay();

            var newItem = weekBlock.getItemByWeekNumber(weekNumber)
            newItem.dateInfo = {
                day: day,
                month: month,
                year: year,
            }
            self.items.push(newItem);
            self.render()
        })
    }

    function ProductProps() {
        var renderBlockSelector = '#option_productProps_block';
        this.dataRolePrefix = 'productprops';
        this.$renderBlock = $(renderBlockSelector);
        if (!this.$renderBlock.length) {
            throw ('Отсутсвует блок ' + renderBlockSelector);
        }
    }

    ProductProps.prototype.setItems = function (currentItems) {
        this.items = currentItems;
    }

    ProductProps.prototype.getItems = function (currentItems) {
        return this.items;
    }

    ProductProps.prototype.render = function () {
        var html = '';
        if (this.items.length) {
            html += '<div class="options_productProps_block">'
            for (var itemIndex = 0; itemIndex < this.items.length; itemIndex++) {
                var item = this.items[itemIndex];
                var propInfo = productAllProps.getPropByCode(item.propCode);
                html += "<div class='options_productProps_item'>";
                html += `
                    <div>
                        <h2>код свойства <b>"${escapeHtml(propInfo.CODE)}"</b></h2>
                        <h2><b>"${escapeHtml(propInfo.NAME)}"</b> = <b>"${escapeHtml(item.propValue)}"</b></h2>
                    </div>
                `;
                html += `<div>
                            <button data-role='${this.dataRolePrefix}_item_delete' data-itemindex='` + itemIndex + `'>Удалить условие по свойству</button>
                        </div>`;
                html += "<h4>Условия блокировки</h4>";
                for (var dcIndex = 0; dcIndex < item.disableConditions.length; dcIndex++) {
                    var disableCondition = item.disableConditions[dcIndex];
                    html += "<div class='options_conditions_block'>";
                    html += "   <label>Тип условия</label>";
                    html += `   <select data-role='${this.dataRolePrefix}_setting_timeslot_dc_type'
                                         data-dayindex='` + itemIndex + `'
                                         data-dcindex='` + dcIndex + `'>`
                    html += disableConditions.getOptionsForSelect(disableCondition.type);
                    html += "   </select>";
                    html += "   <label>Значение</label>";
                    html += `   <input value="${escapeHtml(disableCondition.value)}" data-role='${this.dataRolePrefix}_setting_timeslot_dc_name'
                                         data-dayindex='` + itemIndex + `'
                                         data-dcindex='` + dcIndex + `'/>`;
                    html += `   <div><button data-role='${this.dataRolePrefix}_setting_timeslot_dc_delete'
                                             data-dayindex='` + itemIndex + `'
                                             data-dcindex='` + dcIndex + `'
                                             >Удалить условие</button></div>`;
                    html += "</div>";
                }

                html += `<div class='options_conditions_block'><button data-role='${this.dataRolePrefix}_setting_timeslot_dc_add'
                                 data-dayindex='` + itemIndex + `'
                             >Добавить условие</button></div>`;

                html += "</div>";
            }
            html += '</div>';
        } else {
            html += '<div>нет данных</div>'
        }
        var propsOptions = productAllProps.getOptionsForSelect()
        html += `<div data-role='${this.dataRolePrefix}_item_add_block'>
                    <select name="propCode">${propsOptions}</select>
                    <input name="propValue" placeholder="значение свойства" value="0"/>
                    <button data-role='${this.dataRolePrefix}_item_add'>Добавить условие по свойству</button>
                </div>`;

        this.$renderBlock.html(html);
        this.addFormEvents();
    }

    ProductProps.prototype.addFormEvents = function () {
        var self = this;
        $("[data-role='" + this.dataRolePrefix + "_item_delete']").on('click', function (e) {
            e.preventDefault();
            var itemindex = $(this).data('itemindex');
            var item = self.items[itemindex];
            if (window.confirm(`удалить условие ${item.propCode} = ${item.propValue}?`)) {
                self.items.splice(itemindex, 1);
                self.render()
            }
        })

        $("[data-role='" + this.dataRolePrefix + "_item_add']").on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).closest("[data-role='" + self.dataRolePrefix + "_item_add_block']")
            if (!$parent.length) {
                alert('ошибка! не найден родитесльский элеменет');
                console.log($parent)
                return;
            }

            var code = $parent.find('select[name="propCode"]').val();
            var value = $parent.find('input[name="propValue"]').val();

            if (!code) {
                alert('ошибка с кодом свойства!')
                return;
            }
            if (!value) {
                alert('неверно введено значение свойства!')
                return;
            }

            var newItem = {
                disableConditions: [],
                propCode: code,
                propValue: value
            }
            self.items.push(newItem);
            self.render()
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_delete']").on('click', function (e) {
            e.preventDefault();
            var dayIndex = $(this).data('dayindex');
            var dcIndex = $(this).data('dcindex');
            if (window.confirm("удалить условие?")) {
                self.items[dayIndex].disableConditions.splice(dcIndex, 1);
                self.render()
            }
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_add']").on('click', function (e) {
            e.preventDefault();
            var dayIndex = $(this).data('dayindex');
            var newDc = {
                type: disableConditions.items[0].type,
                value: 0
            }
            self.items[dayIndex].disableConditions.push(newDc);
            self.render()
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_type']").on('change', function (e) {
            var newType = $(this).find(":selected").val();
            var dayIndex = $(this).data('dayindex');
            var dcIndex = $(this).data('dcindex');
            self.items[dayIndex].disableConditions[dcIndex].type = newType;
            console.log(newType)
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_name']").on('change', function (e) {
            var newValue = $(this).val();
            var dayIndex = $(this).data('dayindex');
            var dcIndex = $(this).data('dcindex');
            self.items[dayIndex].disableConditions[dcIndex].value = newValue;
            console.log(newValue)
        })
    }


    function PropsByProviders() {
        var renderBlockSelector = '#option_propsByProviders_block';
        this.dataRolePrefix = 'propsByProviders';
        this.$renderBlock = $(renderBlockSelector);
        if (!this.$renderBlock.length) {
            throw ('Отсутсвует блок ' + renderBlockSelector);
        }
    }

    PropsByProviders.prototype.setItems = function (currentItems) {
        this.items = currentItems;
    }

    PropsByProviders.prototype.getItems = function (currentItems) {
        return this.items;
    }

    PropsByProviders.prototype.render = function () {
        var html = '';
        if (this.items.length) {
            html += '<div class="options_propsByProviders_block">';
            for (var itemIndex = 0; itemIndex < this.items.length; itemIndex++) {
                var item = this.items[itemIndex];
                var propInfo = productAllProps.getPropByCode(item.propCode);
                html += "<div class='options_propsByProviders_item'>";
                html += `
                    <div>
                        <h2>код свойства <b>"${escapeHtml(propInfo.CODE)}"</b></h2>
                        <h2><b>"${escapeHtml(propInfo.NAME)}"</b> = <b>"${escapeHtml(item.propValue)}"</b></h2>
                    </div>
                `;
                html += `
<div class="spoiler-wrap disabled" data-name-slot = '${escapeHtml(item.propValue)}'>
  <div class="spoiler-head"><b>"${escapeHtml(propInfo.NAME)}"</b> = <b>"${escapeHtml(item.propValue)}" (спойлер)</b></div>
  <div class="spoiler-body">
                            <button data-role='${this.dataRolePrefix}_item_delete' data-itemindex='` + itemIndex + `'>Удалить условие по свойству</button>
                        `;
                html += "<h4>Условия</h4>";
                html += "<div class = 'flex-container'>\n\
<div class = 'flex-item'>день недели</div>\n\
<div class = 'flex-item'>от</div>\n\
<div class = 'flex-item'>до</div>\n\
<div class = 'flex-item'>формат</div>\n\
<div class = 'flex-item'>слоты с</div>\n\
</div>";
                console.log(item.disableConditions);
                if(item.disableConditions[0] != undefined){
                    if(Object.keys(item.disableConditions[0]).length != 0){
                        for (var dcIndex = 0; dcIndex < item.disableConditions.length; dcIndex++) {
                            html += "<div class='flex-container options_conditions_block' data-dcindex='" + dcIndex + "' data-dayindex='" + itemIndex + "'>";
                            var disableCondition = item.disableConditions[dcIndex];
                            html += `   <select class='flex-item' data-role='${this.dataRolePrefix}_day_of_week' data-dcIndex='${dcIndex}' data-itemIndex='${itemIndex}'>`
                            html += `   <option ${(disableCondition.DayOfWeek.value == "monday")?"selected":""} value = 'monday'>понедельник</option> `;
                            html += `   <option ${(disableCondition.DayOfWeek.value == "tuesday")?"selected":""} value = 'tuesday'>вторник</option> `;
                            html += `   <option ${(disableCondition.DayOfWeek.value == "wednesday")?"selected":""} value = 'wednesday'>среда</option> `;
                            html += `   <option ${(disableCondition.DayOfWeek.value == "thursday")?"selected":""} value = 'thursday'>четверг</option> `;
                            html += `   <option ${(disableCondition.DayOfWeek.value == "friday")?"selected":""} value = 'friday'>пятница</option> `;
                            html += `   <option ${(disableCondition.DayOfWeek.value == "saturday")?"selected":""} value = 'saturday'>суббота</option> `;
                            html += `   <option ${(disableCondition.DayOfWeek.value == "sunday")?"selected":""} value = 'sunday'>воскресенье</option> `;
                            html += "   </select>";
                            html += `   <input class='flex-item' value="${disableCondition.TimeFrom.value}" data-role='${this.dataRolePrefix}_time_from'  data-dcIndex='${dcIndex}' data-itemIndex='${itemIndex}'/>`;
                            html += `   <input class='flex-item' value="${disableCondition.TimeTo.value}" data-role='${this.dataRolePrefix}_time_to'  data-dcIndex='${dcIndex}' data-itemIndex='${itemIndex}'/>`;
                            html += `   <select class='flex-item' data-role='${this.dataRolePrefix}_time_format' data-dcIndex='${dcIndex}' data-itemIndex='${itemIndex}'>`
                            html += `   <option ${(disableCondition.TimeFormat.value == "today")?"selected":""} value = 'today'>сегодня</option> `;
                            html += `   <option ${(disableCondition.TimeFormat.value == "next_monday")?"selected":""} value = 'next_monday'>следующий понедельник</option> `;
                            html += `   <option ${(disableCondition.TimeFormat.value == "next_tuesday")?"selected":""} value = 'next_tuesday'>следующий вторник</option> `;
                            html += `   <option ${(disableCondition.TimeFormat.value == "next_wednesday")?"selected":""} value = 'next_wednesday'>следующая среда</option> `;
                            html += `   <option ${(disableCondition.TimeFormat.value == "next_thursday")?"selected":""} value = 'next_thursday'>следующий четверг</option> `;
                            html += `   <option ${(disableCondition.TimeFormat.value == "next_friday")?"selected":""} value = 'next_friday'>следующая пятница</option> `;
                            html += `   <option ${(disableCondition.TimeFormat.value == "next_saturday")?"selected":""} value = 'next_saturday'>следующая суббота</option> `;
                            html += `   <option ${(disableCondition.TimeFormat.value == "next_sunday")?"selected":""} value = 'next_sunday'>следующее воскресенье</option> `;
                            html += "   </select>";
                            html += `   <input value="${disableCondition.TimeSlot.value}" class='flex-item' data-role='${this.dataRolePrefix}_time_slot'  data-dcIndex='${dcIndex}' data-itemIndex='${itemIndex}'/>`;
                            html += `   <div><button  data-dcindex='` + dcIndex + `' data-dayindex='` + itemIndex + `' data-role='${this.dataRolePrefix}_setting_timeslot_dc_delete'>Удалить условие</button></div>`;
                            html += "</div>";
                        }
                    }
                }

                html += `<div class='options_conditions_block'><button data-role='${this.dataRolePrefix}_setting_timeslot_dc_add'
                                     data-dayindex='` + itemIndex + `'
                                 >Добавить условие</button></div>`;

                html += "</div></div>";
            }
            html += '</div></div>';
        } else {
            html += '<div>нет данных</div>'
        }
        var propsOptions = productAllProps.getOptionsForSelect()
        html += `<div data-role='${this.dataRolePrefix}_item_add_block'>
                    <select name="propCode">${propsOptions}</select>
                    <input name="propValue" placeholder="значение свойства" value="0"/>
                    <button data-role='${this.dataRolePrefix}_item_add'>Добавить условие по свойству</button>
                </div>`;

        this.$renderBlock.html(html);
        this.addFormEvents();
    }

    $('.spoiler-body').hide(300);
    PropsByProviders.prototype.addFormEvents = function () {
        var self = this;
        $("[data-role='" + this.dataRolePrefix + "_item_delete']").on('click', function (e) {
            e.preventDefault();
            var itemindex = $(this).data('itemindex');
            var item = self.items[itemindex];
            if (window.confirm(`удалить условие ${item.propCode} = ${item.propValue}?`)) {
                self.items.splice(itemindex, 1);
                self.render()
            }
        })

        $("[data-role='" + this.dataRolePrefix + "_item_add']").on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).closest("[data-role='" + self.dataRolePrefix + "_item_add_block']")
            if (!$parent.length) {
                alert('ошибка! не найден родитесльский элеменет');
                console.log($parent)
                return;
            }

            var code = $parent.find('select[name="propCode"]').val();
            var value = $parent.find('input[name="propValue"]').val();

            if (!code) {
                alert('ошибка с кодом свойства!')
                return;
            }
            if (!value) {
                alert('неверно введено значение свойства!')
                return;
            }

            var newItem = {
                disableConditions: [],
                propCode: code,
                propValue: value
            }
            self.items.push(newItem);
            self.render()
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_delete']").on('click', function (e) {
            e.preventDefault();
            var dayIndex = $(this).data('dayindex');
            var dcIndex = $(this).data('dcindex');
            if (window.confirm("удалить условие?")) {
                self.items[dayIndex].disableConditions.splice(dcIndex, 1);
                self.render()
            }
            let curNameSlot = $(this).parents('.spoiler-wrap').data("name-slot");
            $(`[data-name-slot="${curNameSlot}"]`).find('.spoiler-body').slideToggle();
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_add']").on('click', function (e) {
            e.preventDefault();
            var dayIndex = $(this).data('dayindex');
            var DayOfWeek = {
                type: "DayOfWeek",
                value: "monday"
            }
            var TimeFrom = {
                type: "TimeFrom",
                value: "00:00"
            }
            var TimeTo = {
                type: "TimeTo",
                value: "23:59"
            }
            var TimeFormat = {
                type: "TimeFormat",
                value: "ND"
            }
            var TimeSlot = {
                type: "TimeSlot",
                value: "15:00"
            }
            self.items[dayIndex].disableConditions.push({DayOfWeek, TimeFrom, TimeTo, TimeFormat, TimeSlot});
            self.render()
            let curNameSlot = $(this).parents('.spoiler-wrap').data("name-slot");
            $(`[data-name-slot="${curNameSlot}"]`).find('.spoiler-body').slideToggle();
        })

        $("[data-role='" + this.dataRolePrefix + "_day_of_week']").on('change', function (e) {
            var newValue = $(this).find(":selected").val();
            var itemIndex = $(this).data('itemindex');
            var dcIndex = $(this).data('dcindex');
            var DayOfWeek = {
                type: "DayOfWeek",
                value: newValue
            }
            self.items[itemIndex].disableConditions[dcIndex].DayOfWeek = DayOfWeek;
            console.log(DayOfWeek)
        })


        $("[data-role='" + this.dataRolePrefix + "_time_from']").on('change', function (e) {
            var newValue = $(this).val();
            var itemIndex = $(this).data('itemindex');
            var dcIndex = $(this).data('dcindex');
            var TimeFrom = {
                type: "TimeFrom",
                value: newValue
            }
            self.items[itemIndex].disableConditions[dcIndex].TimeFrom = TimeFrom;
            console.log(TimeFrom)
        })

        $("[data-role='" + this.dataRolePrefix + "_time_to']").on('change', function (e) {
            var newValue = $(this).val();
            var itemIndex = $(this).data('itemindex');
            var dcIndex = $(this).data('dcindex');
            var TimeTo = {
                type: "TimeTo",
                value: newValue
            }
            self.items[itemIndex].disableConditions[dcIndex].TimeTo = TimeTo;
            console.log(TimeTo)
        })

        $("[data-role='" + this.dataRolePrefix + "_time_format']").on('change', function (e) {
            var newValue = $(this).find(":selected").val();
            var itemIndex = $(this).data('itemindex');
            var dcIndex = $(this).data('dcindex');
            var TimeFormat = {
                type: "TimeFormat",
                value: newValue
            }
            self.items[itemIndex].disableConditions[dcIndex].TimeFormat = TimeFormat;
            console.log(TimeFormat)
        })

        $("[data-role='" + this.dataRolePrefix + "_time_slot']").on('change', function (e) {
            var newValue = $(this).val();
            var itemIndex = $(this).data('itemindex');
            var dcIndex = $(this).data('dcindex');
            var TimeSlot = {
                type: "TimeSlot",
                value: newValue
            }
            self.items[itemIndex].disableConditions[dcIndex].TimeSlot = TimeSlot;
            console.log(TimeTo)
        })
        
        $('input[data-role=\"propsByProviders_time_from\"]').mask("99:99");
        $('input[data-role=\"propsByProviders_time_to\"]').mask("99:99");
        $('input[data-role=\"propsByProviders_time_slot\"]').mask("99:99");
    }


        
    $(document).on('click', '.spoiler-head', function (e) {
        $(this).parents('.spoiler-wrap').toggleClass("active").find('.spoiler-body').slideToggle();
    })

    function HelperBlock() {
        var renderBlockSelector = '#option_helper_block';
        this.dataRolePrefix = 'helper';
        this.$renderBlock = $(renderBlockSelector);
        if (!this.$renderBlock.length) {
            throw ('Отсутсвует блок ' + renderBlockSelector);
        }
    }


    HelperBlock.prototype.render = function () {
        var html = '';

        let curDate = new Date();

        html += "<h2>Инструмент для просмотра временных слотов</h2>"
        html += `<div data-role="${this.dataRolePrefix}_view_time_slots_block">
                    <label for="">дата</label>
                    <input readonly type="text" value="${curDate.toLocaleDateString()} ${curDate.toLocaleTimeString()}" name="date" onclick="BX.calendar({node: this, field: this, bTime: true});">
                    <button data-role="${this.dataRolePrefix}_view_time_slots">сгенерировать</button>
                     <div>Ссылка: <span data-role="${this.dataRolePrefix}_view_time_slots_result"></span></div>
                </div>`;

        html += "<h2>Инструмент для подсчета секунд</h2>"
        html += `<div data-role="${this.dataRolePrefix}_seconds_calculate_block">
                    <label for="">дни</label>
                    <input name="day" value="0">
                    <label for="">часы</label>
                    <input name="hours" value="0">
                    <label for="">минуты</label>
                    <input name="minutes" value="0">
                    <button data-role="${this.dataRolePrefix}_seconds_calculate">рассчитать</button>
                    <div>Результат: <span data-role="${this.dataRolePrefix}_seconds_calculate_result"></span></div>
                </div>`;

        html += "<h2>Инструмент для подсчета дней, часов, минут из секунд</h2>"
        html += `<div data-role="${this.dataRolePrefix}_etc_calculate_block">
                    <label for="">секунды</label>
                    <input name="seconds" value="0">
                    <button data-role="${this.dataRolePrefix}_etc_calculate">рассчитать</button>
                    <div>Результат: <span data-role="${this.dataRolePrefix}_etc_calculate_result"></span></div>
                </div>`;

        html += "<h2>Значения дней недели</h2>"

        var weeksEnumItems = weeksEnum.getItems();
        for (var k = 0; k < weeksEnumItems.length; k++){
            var weeksEnumItem = weeksEnumItems[k];
            html += `<div>${escapeHtml(weeksEnumItem.name)} = ${escapeHtml(weeksEnumItem.value)}</div>`;
        }

        this.$renderBlock.html(html);
        this.addFormEvents();
    }

    HelperBlock.prototype.addFormEvents = function () {
        var self = this;

        $("[data-role='" + this.dataRolePrefix + "_seconds_calculate']").on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).closest("[data-role='" + self.dataRolePrefix + "_seconds_calculate_block']")
            if (!$parent.length) {
                alert('ошибка! не найден родитесльский элеменет');
                console.log($parent)
                return;
            }

            var day = parseInt($parent.find('input[name="day"]').val());
            var hours = parseInt($parent.find('input[name="hours"]').val());
            var minutes = parseInt($parent.find('input[name="minutes"]').val());

            if (isNaN(day)) {
                alert('неверно введено количество дней')
                return;
            }
            if (isNaN(hours)) {
                alert('неверно введено количество часов')
                return;
            }
            if (isNaN(minutes)) {
                alert('неверно введено количество минут')
                return;
            }

            var resSeconds = day * (60 * 60 * 24) + hours * (60 * 60) + minutes * (60);

            var resMessage = `${day} дней, ${hours} часов, ${minutes} минут = ${resSeconds} секундам`;
            $(`[data-role="${self.dataRolePrefix}_seconds_calculate_result"]`).html(resMessage);
        })

        $("[data-role='" + this.dataRolePrefix + "_etc_calculate']").on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).closest("[data-role='" + self.dataRolePrefix + "_etc_calculate_block']")
            if (!$parent.length) {
                alert('ошибка! не найден родительский элеменет');
                console.log($parent)
                return;
            }

            var seconds = parseInt($parent.find('input[name="seconds"]').val());

            if (isNaN(seconds)) {
                alert('неверно введено количество секунд')
                return;
            }

            seconds = Math.abs(seconds);

            let days = parseInt(seconds / (24*60*60) );
            let leftSeconds = seconds % (24*60*60);
            let hours = parseInt(leftSeconds / (60*60) );
            leftSeconds = leftSeconds % (60*60);
            let minutes = parseInt(leftSeconds / (60));

            var resMessage = `${seconds} секунд = ${days} дней, ${hours} часов, ${minutes} минут`;

            $(`[data-role="${self.dataRolePrefix}_etc_calculate_result"]`).html(resMessage);
        })

        $("[data-role='" + this.dataRolePrefix + "_view_time_slots']").on('click', function (e) {
            e.preventDefault();
            var $parent = $(this).closest("[data-role='" + self.dataRolePrefix + "_view_time_slots_block']")
            if (!$parent.length) {
                alert('ошибка! не найден родительский элеменет');
                console.log($parent)
                return;
            }

            var date = $parent.find('input[name="date"]').val();

            var resLink = '<a href ="/order/?dev=1&ts='+date+'" target="_blank">перейти</a>';
            $(`[data-role="${self.dataRolePrefix}_view_time_slots_result"]`).html(resLink);
        })
    }

    function WeeksEnum() {
    }

    WeeksEnum.prototype.setItems = function (items) {
        this.items = items;
    }

    WeeksEnum.prototype.getItems = function () {
        return this.items;
    }

    function ExpressBlock() {
        var renderBlockSelector = '#option_express_block';
        this.dataRolePrefix = 'express';
        this.$renderBlock = $(renderBlockSelector);
        if (!this.$renderBlock.length) {
            throw ('Отсутсвует блок ' + renderBlockSelector);
        }
    }

    ExpressBlock.prototype.setItems = function (item) {
        this.item = typeof item === 'object' && !Array.isArray(item) ? item : {
            disableConditions: []
        };
    }

    ExpressBlock.prototype.getItems = function () {
        return this.item;
    }

    ExpressBlock.prototype.render = function () {
        var html = '';

        var timeSlot = this.getItems();
        let itemIndex = 0;
        let timeSlotIndex = 0;
        html += "<div class=''>";
        html += "   <div>";
        html += "       <label>Название слота</label>";
        html += `       <input type='text' value='` + escapeHtml(timeSlot.name) + `'
                                       data-role='${this.dataRolePrefix}_setting_timeslot_input_name'
                                       data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'>`;
        html += "   </div>";
        html += "   <label>Не доступен для доставки</label>";
        html += `   <input data-role='${this.dataRolePrefix}_setting_day_input_disable' data-dayindex='` + itemIndex + `'
                               type='checkbox' ` + (timeSlot.disabled ? 'checked' : '') + `>`;
        html += "   <div>";
        html += "       <div>";
        html += "           <h4>Условия блокировки</h4>";
        if(timeSlot.disableConditions) {
            for (var dcIndex = 0; dcIndex < timeSlot.disableConditions.length; dcIndex++) {
                var disableCondition = timeSlot.disableConditions[dcIndex];
                html += "<div class='options_conditions_block'>";
                html += "   <label>Тип условия</label>";
                html += `   <select data-role='${this.dataRolePrefix}_setting_timeslot_dc_type'
                                         data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                         data-dcindex='` + dcIndex + `'>`
                html += disableConditions.getOptionsForSelect(disableCondition.type);
                html += "   </select>";
                html += "   <label>Значение</label>";
                html += `   <input value="${escapeHtml(disableCondition.value)}" data-role='${this.dataRolePrefix}_setting_timeslot_dc_name'
                                         data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                         data-dcindex='` + dcIndex + `'/>`;
                html += `   <div><button data-role='${this.dataRolePrefix}_setting_timeslot_dc_delete'
                                             data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                             data-dcindex='` + dcIndex + `'
                                             >Удалить условие</button></div>`;
                html += "</div>";
            }
        }

        html += `       <div class='options_conditions_block'><button data-role='${this.dataRolePrefix}_setting_timeslot_dc_add'
                                             data-dayindex='` + itemIndex + `' data-timeslotindex='` + timeSlotIndex + `'
                                             >Добавить условие</button></div>`;
        html += "       </div>";
        html += "   </div>";
        html += "</div>";

        this.$renderBlock.html(html);
        this.addFormEvents();
    }

    ExpressBlock.prototype.addFormEvents = function () {
        var self = this;

        $("[data-role='" + this.dataRolePrefix + "_setting_day_input_disable']").on('change', function (e) {
            var disabled = $(this).is(':checked');
            self.item.disabled = disabled;
            console.log(disabled)
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_input_name']").on('change', function (e) {
            var newName = $(this).val();
            self.item.name = newName;
            console.log(newName)
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_type']").on('change', function (e) {
            var newType = $(this).find(":selected").val();
            var dcIndex = $(this).data('dcindex');
            self.item.disableConditions[dcIndex].type = newType;
            console.log(newType)
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_delete']").on('click', function (e) {
            e.preventDefault();
            var dcIndex = $(this).data('dcindex');
            if (window.confirm("удалить условие?")) {
                self.item.disableConditions.splice(dcIndex, 1);
                self.render()
            }
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_add']").on('click', function (e) {
            e.preventDefault();
            var newDc = {
                type: disableConditions.items[0].type,
                value: 0
            }
            self.item.disableConditions.push(newDc);
            self.render()
        })

        $("[data-role='" + this.dataRolePrefix + "_setting_timeslot_dc_name']").on('change', function (e) {
            var newValue = $(this).val();
            var dcIndex = $(this).data('dcindex');
            self.item.disableConditions[dcIndex].value = newValue;
            console.log(newValue)
        })

    }

    var disableConditions = new DisableConditions();
    var weekBlock = new WeekBlock();
    var bannedDatesBlock = new BannedDatesBlock();
    var hiddenDatesBlock = new HiddenDatesBlock();
    var specialDays = new SpecialDays();
    var productProps = new ProductProps();
    var productAllProps = new ProductAllProps()
    var helperBlock = new HelperBlock()
    var weeksEnum = new WeeksEnum();
    var propsByProviders = new PropsByProviders();
    var expressBlock = new ExpressBlock();

    var sendData = {
        'week': weekBlock,
        'bannedDates': bannedDatesBlock,
        'hiddenDates': hiddenDatesBlock,
        'specialDays': specialDays,
        'productProps': productProps,
        'productPropsByInterval': propsByProviders,
        'expressSlot': expressBlock,
    }
});