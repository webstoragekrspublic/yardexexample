<?

use Custom\DeliveryTime\DeliveryDateOptions;
use Custom\DeliveryTime\Models\WeekEnum;
use Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition;
use Custom\DeliveryTime;

define("BX_PUBLIC_TOOLS", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!\Helpers\SafetyValidation::isAdminOrManger(CUser::GetID())) {
    die('login is required');
}

$currentOptions = [
    'disableConditionsInfo' => [],
    'week' => [],
    'bannedDates' => [],
    'hiddenDates' => [],
    'specialDays' => [],
    'productProps' => [],
    'specialProducts' => [],
    'weeksEnum' => [],
    'productAllProps' => [],
    'productPropsByInterval' => [],
    'expressSlot' => [],
];

///////////////////////////////
/////// weeksEnum
$weeksEnum = WeekEnum::getAllNames();
foreach ($weeksEnum as $weeksEnumValue => $weeksEnumName) {
    $currentOptions['weeksEnum'][] = [
        'value' => $weeksEnumValue,
        'name' => $weeksEnumName,
    ];
}
///////////////////////////////


///////////////////////////////
/////// week
$weeks = WeekEnum::getAllNames();
$currentWeekSetting = DeliveryDateOptions::getInstance()->getWeekSettingForAdminPage();
foreach ($weeks as $weekNumber => $weekName) {
    $currentDaySetting = $currentWeekSetting->getDaySettingByNumber($weekNumber);
    $currentOptions['week'][] = DeliveryTime\Serializer::daySettingToAdminArr($currentDaySetting);
}
///////////////////////////////

///////////////////////////////
/// disableConditionsInfo
$dcTypes = TimeSlotDisableCondition::getAllTypes();
foreach ($dcTypes as $type) {
    $dc = TimeSlotDisableCondition::NewFromType($type, 0);
    $currentOptions['disableConditionsInfo'][] = [
        'type' => $dc->getType(),
        'description' => $dc->getDescription(),
    ];
}
///////////////////////////////////////////////////////////


///////////////////////////////
/// bannedDates
$bannedDaysSetting = DeliveryDateOptions::getInstance()->getBannedDaysSetting();
$currentOptions['bannedDates'] = DeliveryTime\Serializer::bannedDaysToAdminArr($bannedDaysSetting);
///////////////////////////////////////////////////////////

///////////////////////////////
/// hiddenDates
$hiddenDaysSetting = DeliveryDateOptions::getInstance()->getHiddenDaysSetting();
$currentOptions['hiddenDates'] = DeliveryTime\Serializer::hiddenDaysToAdminArr($hiddenDaysSetting);
///////////////////////////////////////////////////////////

///////////////////////////////
/// specialDays
$specialDays = DeliveryDateOptions::getInstance()->getSpecialDaysSettingForAdminPage();
foreach ($specialDays->getDaysSetting() as $specialDay) {
    $currentOptions['specialDays'][] = DeliveryTime\Serializer::daySettingToAdminArr($specialDay);
}
///////////////////////////////////////////////////////////

///////////////////////////////
/// productProps
$productConditionSetting = DeliveryDateOptions::getInstance()->getProductConditionSetting();
$propsDisableCondition = $productConditionSetting->getPropsDisableCondition();
foreach ($propsDisableCondition as $propDisableCondition) {
    $currentOptions['productProps'][] = DeliveryTime\Serializer::propDisableConditionToAdminArr($propDisableCondition);
}

///////////////////////////////////////////////////////////

///////////////////////////////
/// productAllProps todo
$properties = CIBlockProperty::GetList(
    ["name" => "asc"],
    ["ACTIVE" => "Y", "IBLOCK_ID" => \Helpers\Constants::PRODUCTS_IBLOCK_ID]
);
while ($row = $properties->Fetch()) {
    $currentOptions['productAllProps'][] = [
        'ID' => $row['ID'],
        'NAME' => $row['NAME'],
        'CODE' => $row['CODE'],
    ];
}
///////////////////////////////////////////////////////////


///////////////////////////////
/// productPropsByInterval todo
$productConditionSetting = DeliveryDateOptions::getInstance()->getProductConditionSetting();
$propsByInterval = $productConditionSetting->getPropsByInterval();

foreach ($propsByInterval as $propByInterval) {
    $currentOptions['productPropsByInterval'][] = DeliveryTime\Serializer::propDisableConditionToAdminArr($propByInterval);
}
///////////////////////////////////////////////////////////

///////////////////////////////
/// specialProducts todo

///////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////
$currentOptions['expressSlot'] = DeliveryDateOptions::getInstance()->getExpressSlotSetting();
///////////////////////////////////////////////////////////

echo json_encode($currentOptions, JSON_UNESCAPED_UNICODE);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
