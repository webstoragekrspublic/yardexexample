<?

use Bitrix\Main\Loader;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!\Helpers\SafetyValidation::isAdminOrManger(CUser::GetID())) {
    $answer = [
        'error' => true,
        'message' => 'вы не авторизованы!'
    ];

    die(json_encode($answer, JSON_UNESCAPED_UNICODE));
}

$moduleID = 'custom.deliverytime';
Loader::includeModule($moduleID);

$body = file_get_contents('php://input');
$answer = [
    'error' => true,
    'message' => 'что-то пошло не так!'
];

$bodyData = json_decode($body,1);

if (json_last_error()) {
    $answer = [
        'error' => true,
        'message' => 'ошибка обработки тела запроса!'
    ];
    die(json_encode($answer, JSON_UNESCAPED_UNICODE));
}

$saveOptions = [
    [
        'bodyName' => 'week',
        'optionName' => 'weekDays'
    ],
    [
        'bodyName' => 'bannedDates',
        'optionName' => 'bannedDates'
    ],
    [
        'bodyName' => 'hiddenDates',
        'optionName' => 'hiddenDates'
    ],
    [
        'bodyName' => 'specialDays',
        'optionName' => 'specialDays'
    ],
    [
        'bodyName' => 'productProps',
        'optionName' => 'productProps'
    ],
    [
        'bodyName' => 'productPropsByInterval',
        'optionName' => 'productPropsByInterval'
    ],
    [
        'bodyName' => 'expressSlot',
        'optionName' => 'expressSlot'
    ],
];

foreach ($saveOptions as $saveOption) {
    if (!isset($bodyData[$saveOption['bodyName']])) {
        $answer = [
            'error' => true,
            'message' => 'ошибка! Отсутствует обязательное поле ' . $saveOption['bodyName']
        ];
        die(json_encode($answer, JSON_UNESCAPED_UNICODE));
    }
}


foreach ($saveOptions as $saveOption) {
    $optionName = $saveOption['optionName'];
    $optionValue = json_encode($bodyData[$saveOption['bodyName']], JSON_UNESCAPED_UNICODE);
    try {
        if($optionName == "productPropsByInterval") {
            COption::SetOptionString($moduleID, $optionName, json_encode($optionValue));
        }
        else{
            COption::SetOptionString($moduleID, $optionName, $optionValue);
        }
    } catch (\Exception $e) {
        $answer = [
            'error' => true,
            'message' => 'ошибка при сохраниии свойства! '.$e->getMessage()
        ];
        die(json_encode($answer, JSON_UNESCAPED_UNICODE));
    }
}

$answer = [
    'error' => false,
    'message' => 'Данные сохранены!'
];

echo json_encode($answer, JSON_UNESCAPED_UNICODE);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
