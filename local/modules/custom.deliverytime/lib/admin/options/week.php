<?

namespace Custom\DeliveryTime\Admin\Options;

use Custom\DeliveryTime\DeliveryDateOptions;
use Custom\DeliveryTime\DeliveryTime;
use Custom\DeliveryTime\Models\WeekEnum;

class Week
{
    public static function renderAdminFormInputs()
    {
        $html = '';
        $weeks = WeekEnum::getAllNames();
        $currentWeekSetting = DeliveryDateOptions::getInstance()->getWeekSetting();
        foreach ($weeks as $weekNumber => $weekName) {
            $currentDaySetting = $currentWeekSetting->getDaySettingByNumber($weekNumber);
            $disabled = $currentDaySetting && $currentDaySetting->isDisabled();
            $currentTimeSlots = $currentDaySetting ? $currentDaySetting->getTimeSlots() : [];

            $inputWeekName = 'week_setting_day_' . $weekNumber;

            $html .= "<div>";
            $html .= "<h2>$weekName</h2>";
            $html .= "<label>Не доступен для доставки</label>";
            $html .= "<input name='{$inputWeekName}_disabled' type='checkbox' " . ($disabled ? 'checked' : '') . ">";
            $html .= "<h3>Временные слоты</h3>";
            $next_index = 0;
            foreach ($currentTimeSlots as $timeSlot) {
                $inputTimeSlotName = $inputWeekName . '_timeslots_' . $next_index;
                $html .= "<div data-role='time_slot_block' class='options_timeslot_block'>";
                $html .= "<div>";
                $html .= "<label>название слота</label>";
                $html .= "<input type='text' name='{$inputTimeSlotName}_name' value='" . htmlspecialchars($timeSlot->getName()) . "'>";
                $html .= "</div>";
                $html .= "<div>";
                $html .= "<label>Не доступен для доставки</label>";
                $html .= "<input type='checkbox' name='{$inputTimeSlotName}_disabled' " . ($timeSlot->isDisabled() ? 'checked' : '') . ">";
                $html .= "<div>";
                $html .= "</div>";
                $html .= "<label>Условия</label>";
                $html .= "</div>";
                $html .= "<div><button data-role='delivery_time_timeslot_delete'>Удалить слот</button></div>";
                $html .= "</div>";
                $next_index++;
            }
            $html .= "<div>
                        <button data-role='delivery_time_timeslot_add' data-name='{$inputTimeSlotName}' data-next_index='{$next_index}'>
                            Добавить новый слот
                        </button>
                    </div>";
            $html .= "</div>";
        }

        return $html;
    }

}