<?

namespace Custom\DeliveryTime;

use Custom\DeliveryTime\Settings\DaySetting;
use Custom\DeliveryTime\Models\WeekEnum;
use Custom\DeliveryTime\Models\DeliveryTimeProduct;

class DeliveryDateAvailable
{
    /**
     * @param int|null $nextDayCount
     * @return DaySetting[]
     */
    public static function getNextDaysSetting(int $nextDayCount = null): array
    {
        
//        $nextDayCount = null;
        
        if ($nextDayCount === null) {
            $flagNull = true;
            $nextDayCount = DeliveryDateOptions::DAYS_SHOW_NUMBER;
        }
        else if($nextDayCount === -1) {
            $flagNull = true;
            $nextDayCount = DeliveryDateOptions::DAYS_SHOW_NUMBER;
        }

        $resDaysSetting = [];

        $OneDaySeconds = 24 * 60 * 60;
        $currentTimeStamp = DeliveryTimeTools::getInstance()->getCurrentTimeStamp();

        $usedSlots = DeliveryDateOptions::getInstance()->getUsedSlots();
        
        
        $weekSetting = DeliveryDateOptions::getInstance()->getWeekSetting();
        $specialDaysSetting = DeliveryDateOptions::getInstance()->getSpecialDaysSetting();
        $hiddenDaysSetting = DeliveryDateOptions::getInstance()->getHiddenDaysSetting();
        $bannedDaysSetting = DeliveryDateOptions::getInstance()->getBannedDaysSetting();
        
        for ($dayCounter = 0; $dayCounter < $nextDayCount; $dayCounter++) {
            $dayTimeStamp = $currentTimeStamp + $OneDaySeconds * $dayCounter;
            $dayDateTime = new \DateTime();
            $dayDateTime->setTimestamp($dayTimeStamp);
            $dayD = (int)$dayDateTime->format('d');
            $dayM = (int)$dayDateTime->format('m');
            $dayY = (int)$dayDateTime->format('Y');
            $dayW = (int)$dayDateTime->format('w');
            

            if ($hiddenDaysSetting->isHiddenDay($dayD, $dayM, $dayY)) {
                $nextDayCount++;
                continue;
            }

            $startDayTime = \DateTime::createFromFormat('!d.m.Y', $dayDateTime->format('d.m.Y'));
//            $startDayTime->setTimezone(new \DateTimeZone('Asia/Krasnoyarsk'));
            

            $daySetting = $weekSetting->getDaySettingByNumber($dayW);
            

            $daySetting->setDateTime($startDayTime);

            if ($specialDaysSetting->isHaveSpecDaySetting($dayD, $dayM, $dayY)) {
                $daySetting = $specialDaysSetting->getDaySetting($dayD, $dayM, $dayY);
            }

            if ($bannedDaysSetting->isBannedDay($dayD, $dayM, $dayY)) {
                $daySetting->setDisabledDayAndSlots();
            }

            switch ($dayCounter) {
                case 0:
                    $daySetting->setName(WeekEnum::getTodayName());
                    break;
                case 1:
                    $daySetting->setName(WeekEnum::getTomorrowName());
                    break;
            }
            

            $daySetting->applyTimeCondition($currentTimeStamp);
            
            // ЗАМЕЧАНИЕ
            
            $daySetting->applyUsedSlotCondition($usedSlots);

//            pr(date("H:i:s"));
            
            
            if($flagNull && !\Helpers\CustomTools::isFastDeliveryEnabled()){
                if($daySetting->getName() == "Сегодня"){
//                    $daySetting->setDisabled(true);
                }
                $d = strtotime("now");
                $dayWeek = date("N", $d);

                if($dayWeek == 6){
                    if($daySetting->getName() == "Завтра"){
//                        $daySetting->setDisabled(true);
                    }
                    else if($daySetting->getName() == "Вс"){
//                        $daySetting->setDisabled(true);
                    }
                }
                else if($dayWeek == 7){
                    if($daySetting->getName() == "Завтра"){
//                        $daySetting->setDisabled(true);
                    }
                }
            }

            $resDaysSetting[] = $daySetting;
        }

//        pr($resDaysSetting);
        return $resDaysSetting;
    }

    /**
     * @param DeliveryTimeProduct[] $deliveryTimeProducts
     * @param int|null $nextDayCount
     * @return array
     */
    public static function getDaysInfoForProducts(array $deliveryTimeProducts, int $nextDayCount = null): array
    {
        $daysSetting = static::getNextDaysSetting($nextDayCount);
        if ($deliveryTimeProducts) {
            foreach ($deliveryTimeProducts as $deliveryTimeProduct) {
                $daysSetting = self::applyProductConditionsToDaysSetting($deliveryTimeProduct, $daysSetting);
            }
        }

        return $daysSetting;
    }

    /**
     * @param DeliveryTimeProduct $DeliveryTimeProduct
     * @param DaySetting[] $daysSetting
     * @return DaySetting[]
     */
    protected static function applyProductConditionsToDaysSetting(DeliveryTimeProduct $DeliveryTimeProduct, array $daysSetting): array
    {
        $productSetting = DeliveryDateOptions::getInstance()->getProductConditionSetting();
        $productSetting->applyProductConditionsToDaysSetting($DeliveryTimeProduct, $daysSetting);
        return $daysSetting;
    }
}