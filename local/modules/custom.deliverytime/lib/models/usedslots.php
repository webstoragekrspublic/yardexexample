<?

namespace Custom\DeliveryTime\Models;

class UsedSlots
{
    protected const ORDER_TRACKING_TS = 14 * 24 * 3600;//за какой период смотрим зарезервированные слоты
    // хранилище с информацией о использованных слотах
    protected const ORDER_TRACKING_FILE_PATH = __DIR__ . '/UsedSlotsTracking.txt';

    protected array $usedTimeSlots;

    public function __construct()
    {
        $this->init();
    }

    protected function init()
    {
        $this->initUsedSlots();
    }

    protected function initUsedSlots()
    {
        $usedSlots = [];
        $data = file_get_contents(static::ORDER_TRACKING_FILE_PATH);
        if ($data) {
            $usedSlots = json_decode($data, 1);
            if ($usedSlots === null) {
                $usedSlots = [];
            }
        }
        $this->usedTimeSlots = $usedSlots;
    }

    public function getUsedSlots(): array
    {
        return $this->usedTimeSlots;
    }

    public function isUsedSlot(\DateTime $day, string $slotName): bool
    {
        return (bool)$this->usedSlotCount($day, $slotName);
    }

    public function usedSlotCount(\DateTime $day, string $slotName): int
    {
        $cnt = 0;
        $dayMonthYear = $day->format('d.m.Y');
        if (isset($this->usedTimeSlots[$dayMonthYear][$slotName])) {
            $cnt = (int)$this->usedTimeSlots[$dayMonthYear][$slotName];
        }
        return $cnt;
    }

    public static function updateUsedSlots()
    {
        $parameters = [
            'select' => [
                "ID",
                "STATUS_ID",
                "PROPERTY_DELIVERY_DATE.VALUE",
                "PROPERTY_DELIVERY_TIME.VALUE",
                "PROPERTY_HOLD_TIME_SLOT.VALUE"
            ],
            'filter' => [
                '=PROPERTY_HOLD_TIME_SLOT.CODE' => 'HOLD_TIME_SLOT',
                '=PROPERTY_DELIVERY_DATE.CODE' => 'DELIVERY_DATE',
                '=PROPERTY_DELIVERY_TIME.CODE' => 'DELIVERY_TIME',
                '>=DATE_INSERT' => \Bitrix\Main\Type\DateTime::createFromTimestamp(time() - static::ORDER_TRACKING_TS)
            ],
            'runtime' => [
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_HOLD_TIME_SLOT',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_DELIVERY_DATE',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
                new \Bitrix\Main\Entity\ReferenceField(
                    'PROPERTY_DELIVERY_TIME',
                    '\Bitrix\sale\Internals\OrderPropsValueTable',
                    ["=this.ID" => "ref.ORDER_ID"],
                    ["join_type" => "left"]
                ),
            ],
            'order' => ['ID' => 'DESC']
        ];

        $q = \Bitrix\Sale\Order::getList($parameters);
        $deliveryUsedSlots = [];
        while ($order = $q->fetch()) {
            if ($order['STATUS_ID'] != 'O') {
                $deliveryDate = trim($order['SALE_INTERNALS_ORDER_PROPERTY_DELIVERY_DATE_VALUE']);
                $deliveryTime = trim($order['SALE_INTERNALS_ORDER_PROPERTY_DELIVERY_TIME_VALUE']);
                $holdTimeSlot = trim($order['SALE_INTERNALS_ORDER_PROPERTY_HOLD_TIME_SLOT_VALUE']);
                if ($deliveryDate && $deliveryTime && $holdTimeSlot === 'Y') {
                    if (isset($deliveryUsedSlots[$deliveryDate][$deliveryTime])) {
                        $deliveryUsedSlots[$deliveryDate][$deliveryTime]++;
                    } else {
                        $deliveryUsedSlots[$deliveryDate][$deliveryTime] = 1;
                    }
                }
            }
        }
        file_put_contents(static::ORDER_TRACKING_FILE_PATH, json_encode($deliveryUsedSlots, JSON_UNESCAPED_UNICODE));
    }

}
