<?

namespace Custom\DeliveryTime\Models;

use Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition;

class ProductPropDisableCondition
{
    protected string $propCode;
    protected string $propValue;

    /**
     * @var TimeSlotDisableCondition[]
     */
    protected array $timeSlotDisableConditions;


    /**
     * @param string $propCode
     * @param string $propValue
     * @param TimeSlotDisableCondition[] $timeSlotDisableConditions
     */
    public function __construct(string $propCode, string $propValue, array $timeSlotDisableConditions)
    {
        $this->propCode = $propCode;
        $this->propValue = $propValue;
        $this->timeSlotDisableConditions = $timeSlotDisableConditions;
    }

    public function getPropCode(): string
    {
        return $this->propCode;
    }

    public function getPropValue(): string
    {
        return $this->propValue;
    }

    /**
     * @return TimeSlotDisableCondition[]
     */
    public function getTimeSlotDisableConditions(): array
    {
        return $this->timeSlotDisableConditions;
    }
}
