<?

namespace Custom\DeliveryTime\Models;

class WeekEnum
{
    protected const weekNames = [
        1 => 'Пн',
        2 => 'Вт',
        3 => 'Ср',
        4 => 'Чт',
        5 => 'Пт',
        6 => 'Сб',
        0 => 'Вс',
    ];

    protected const today = 'Сегодня';
    protected const tomorrow = 'Завтра';

    public static function getWeekDayName(int $weekNumber): string
    {
        return self::weekNames[$weekNumber] ?? '';
    }

    public static function getAllNames(): array
    {
        return self::weekNames;
    }

    public static function getTodayName(): string
    {
        return self::today;
    }

    public static function getTomorrowName(): string
    {
        return self::tomorrow;
    }
}
