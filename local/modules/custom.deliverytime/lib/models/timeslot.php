<?

namespace Custom\DeliveryTime\Models;

use Custom\DeliveryTime\TimeSlotConditions\TimeSlotDisableCondition;

class TimeSlot
{
    /**
     * @var string 'любое время'|'10:00-13:00'|...
     */
    protected string $name;

    protected bool $disabled;

    protected bool $anyTimeSlot;

    protected int $startHour;

    /**
     * @var TimeSlotDisableCondition[]
     */
    protected array $disabledConditions = [];

    /**
     * @param string $name
     * @param TimeSlotDisableCondition[] $disabledConditions
     * @param bool $disabled
     */
    public function __construct(string $name, array $disabledConditions, bool $disabled = false)
    {
        $this->name = $name;
        foreach ($disabledConditions as $dc) {
            $this->addDisabledCondition($dc);
        }
        $this->disabled = $disabled;
        $this->initAnyTimeSlot();
        $this->initSlotStartHour();
    }

    /**
     * @param TimeSlotDisableCondition $disabledCondition
     */
    public function addDisabledCondition(TimeSlotDisableCondition $disabledCondition): void
    {
        $this->disabledConditions[$disabledCondition->getType()] = $disabledCondition;
    }

    protected function initAnyTimeSlot()
    {
//        $this->anyTimeSlot = !preg_match('/^\d\d:/', $this->name);
        $this->anyTimeSlot = false;
    }

    public function isAnyTimeSlot(): bool
    {
        return $this->anyTimeSlot;
    }

    protected function initSlotStartHour()
    {
        $startHour = 0;
        if (!$this->isAnyTimeSlot()) {
            $startHour = (int)$this->name;
        }
        $this->startHour = $startHour;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getStartHour(): int
    {
        return $this->startHour;
    }

    public function setDisabled(bool $val)
    {
        $this->disabled = $val;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @return TimeSlotDisableCondition[]
     */
    public function getDisabledConditions(): array
    {
        return $this->disabledConditions;
    }

    public function disableByExistTimeSlotConditions(TimeSlotDisableCondition $compareDC): bool
    {
        if ($this->isDisabled()) {
            return true;
        }

        if ($this->isHaveDisabledCondition($compareDC->getType())) {
            $tsDc = $this->getDisableConditionByType($compareDC->getType());
            if ($tsDc->isConditionWork($compareDC)) {
                $this->setDisabled(true);
                return true;
            }
        }
        
        return false;
    }

    protected function getDisableConditionByType(string $type): ?TimeSlotDisableCondition
    {
        return $this->disabledConditions[$type] ?? null;
    }

    protected function isHaveDisabledCondition(string $type): bool
    {
        return isset($this->disabledConditions[$type]);
    }
}
