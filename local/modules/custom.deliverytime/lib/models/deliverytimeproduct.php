<?

namespace Custom\DeliveryTime\Models;

use Helpers\Constants;

class DeliveryTimeProduct
{
    protected int $id;

    /**
     * свойства товара в формате array[PROP_CODE]['VALUE' => val, '~VALUE' => rawVal]
     */
    protected array $props;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    protected function __construct(int $id, array $allProps)
    {
        $this->id = $id;
        $this->props = $allProps;
    }

    /**
     * @param $productInfo - массив информации о товаре создаваемый в детальной карточке товара
     * @return DeliveryTimeProduct
     */
    public static function NewFromComponentResult($productInfo): ?DeliveryTimeProduct
    {
        if (!isset($productInfo['ID'],$productInfo['PROPERTIES'])){
            return null;
        }
        $props = [];
        if (is_array($productInfo['PROPERTIES'])) {
            $props = $productInfo['PROPERTIES'];
        }
        return new self((int)$productInfo['ID'], $props);
    }

    /**
     * @param array $productsId
     * @return DeliveryTimeProduct[]
     */
    public static function NewFromProductsId(array $productsId): array
    {
        $deliveryTimeProducts = [];
        if (!is_array($productsId) or !$productsId)
            return $deliveryTimeProducts;

        $select = ['ID', 'PROPERTY_*'];
        $arFilter = ['IBLOCK_ID' => Constants::PRODUCTS_IBLOCK_ID, 'ID' => $productsId];
        $productsQ = \CIBlockElement::GetList(
            ["ID" => "ASC"],
            $arFilter,
            false,
            false,
            $select
        );

        while ($row = $productsQ->GetNextElement()) {
            $dtr = self::NewFromDBRow($row);
            if ($dtr){
                $deliveryTimeProducts[] = $dtr;
            }
        }
        return $deliveryTimeProducts;
    }

    /**
     * @param int $productId
     * @return DeliveryTimeProduct
     */
    public static function NewFromProductId(int $productId): ?DeliveryTimeProduct
    {
        $objets = self::NewFromProductsId([$productId]);
        $res = null;
        if (isset($objets[0])){
            $res = $objets[0];
        }
        return $res;
    }

    /**
     * @param \_CIBElement $row = $Q->GetNextElement()
     * @return DeliveryTimeProduct|null
     */
    public static function NewFromDBRow(\_CIBElement $row): ?DeliveryTimeProduct
    {
        $rowFields = $row->GetFields();
        $productId = (int)$rowFields['ID'];
        if (!$productId) {
            return null;
        }
        $productProps = [];

        foreach ($row->props as $propInfo) {
            $rowPropKey = 'PROPERTY_' . $propInfo['ID'];
            $rowRawPropKey = '~PROPERTY_' . $propInfo['ID'];
            if (key_exists($rowPropKey, $rowFields)) {
                $productProps[$propInfo['CODE']]['VALUE'] = $rowFields[$rowPropKey];
                $productProps[$propInfo['CODE']]['~VALUE'] = $rowFields[$rowRawPropKey];
            }
        }
        return new self($productId, $productProps);
    }

    public function getPropValue($propCode)
    {
        return $this->props[$propCode]['VALUE'] ?? null;
    }
}