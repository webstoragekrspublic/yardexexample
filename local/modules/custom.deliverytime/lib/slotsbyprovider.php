<?

namespace Custom\DeliveryTime;

use Custom\DeliveryTime\DisableConditionByProvider;

class SlotsByProvider
{
    public $propCode;
    public $propValue;
    public $disableConditions;

    function __construct($propCode, $propValue, $disableCondition){
        $this->propCode = $propCode;
        $this->propValue = $propValue;
        $this->disableConditions[] = $disableCondition;
    }

    public function getTimeSlotInfo(){
        $currentTS = time();
        //$currentTS = strtotime(date("Y-m-d 18:00:00"));
        foreach($this->disableConditions as $key=>$dcs) {
            foreach ($dcs as $dc) {
                
                if($dc["DayOfWeek"]->propValue == "monday") {
                    $day = 1;
                }
                elseif($dc["DayOfWeek"]->propValue == "tuesday"){
                    $day = 2;
                }
                elseif($dc["DayOfWeek"]->propValue == "wednesday"){
                    $day = 3;
                }
                elseif($dc["DayOfWeek"]->propValue == "thursday"){
                    $day = 4;
                }
                elseif($dc["DayOfWeek"]->propValue == "friday"){
                    $day = 5;
                }
                elseif($dc["DayOfWeek"]->propValue == "saturday"){
                    $day = 6;
                }
                elseif($dc["DayOfWeek"]->propValue == "sunday"){
                    $day = 7;
                }

                if($dc["TimeFormat"]->propValue == "next_monday") {
                    $deliveryDate = 1;
                }
                elseif($dc["TimeFormat"]->propValue == "next_tuesday"){
                    $deliveryDate = 2;
                }
                elseif($dc["TimeFormat"]->propValue == "next_wednesday"){
                    $deliveryDate = 3;
                }
                elseif($dc["TimeFormat"]->propValue == "next_thursday"){
                    $deliveryDate = 4;
                }
                elseif($dc["TimeFormat"]->propValue == "next_friday"){
                    $deliveryDate = 5;
                }
                elseif($dc["TimeFormat"]->propValue == "next_saturday"){
                    $deliveryDate = 6;
                }
                elseif($dc["TimeFormat"]->propValue == "next_sunday"){
                    $deliveryDate = 7;
                }

                $limitTS1 = strtotime(date("Y-m-d " . $dc["TimeFrom"]->propValue . ":00"));
                //$limitTS1 = date("Y-m-d " . $dc["TimeFrom"]->propValue . ":00");
                $limitTS2 = strtotime(date("Y-m-d " . $dc["TimeTo"]->propValue . ":00"));
                //$limitTS2 = date("Y-m-d " . $dc["TimeTo"]->propValue . ":00");
                if (($limitTS1 < $currentTS) && ($currentTS < $limitTS2)) {
                    if(date("N") == $day) {
                        $currDate = date("Y-m-d ".$dc["TimeSlot"]->propValue.":00");
                        
                        if($dc["TimeFormat"]->propValue == "today") {
                            $timeSlotInfo["DayDelay"] = 0;
                        }// Эта неделя
                        elseif($day < $deliveryDate){
                            $timeSlotInfo["DayDelay"] = $deliveryDate - $day;
                        }// Следующая неделя
                        elseif($day > $deliveryDate){
                            $timeSlotInfo["DayDelay"] = (7 - $day) + $deliveryDate;
                        }
                        elseif($day == intval(date("N"))){
                            $timeSlotInfo["DayDelay"] = 7;
                        }
                        
                        $timeSlotInfo["MinHour"] = explode(":", $dc["TimeSlot"]->propValue);
                        $timeSlotInfo["MinHour"] = $timeSlotInfo["MinHour"][0];
                        
                        return $timeSlotInfo;
                    }
                }
            }
        }
        return false;
    }
}