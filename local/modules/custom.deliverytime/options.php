<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

Loc::loadMessages(__FILE__);

// получаем идентификатор модуля
$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialchars($request['mid'] != '' ? $request['mid'] : $request['id']);
// подключаем наш модуль
Loader::includeModule($module_id);
$asset = Bitrix\Main\Page\Asset::getInstance();
$asset->addJs('/local/modules/custom.deliverytime/lib/admin/options/script.js');
echo $asset->insertCss('/local/modules/custom.deliverytime/lib/admin/options/style.css');
/*
 * Параметры модуля со значениями по умолчанию
 */
$aTabs = [
    [
        'DIV' => 'edit1',
        'TAB' => 'Настройки дней недели',
        'TITLE' => 'Общая настройка дней недели',
        'HTML' => '<div><strong>все названия слотов с временным интервалом должны быть в формате "13:00-15:00",
            иначе это будет считаться как "любой слот" (что может привеcти к некорректным выгрузкам в другие системы)</strong></div>
            <div id="option_week_block">загружается...</div>
            '
    ],
    [
        'DIV' => 'edit2',
        'TAB' => 'Недоступные дни',
        'TITLE' => 'Дни будут не активными для заказа (например праздники)',
        'HTML' => '<div id="option_bannedDates_block">загружается...</div>',
    ],
    [
        'DIV' => 'edit3',
        'TAB' => 'Скрытые дни',
        'TITLE' => 'Дни не будут видны для выбора (нужно для длинных выходных, чтобы не оказалось что у пользователя показываеются только не доступные дни)',
        'HTML' => '<div id="option_hiddenDates_block">загружается...</div>',
    ],
    [
        'DIV' => 'edit4',
        'TAB' => 'Дни со спец условиями',
        'TITLE' => 'вместо настроек из "дней недели" условия будут браться отсюда на конкретную дату.',
        'HTML' => '<div id="option_specialDays_block">загружается...</div>',
    ],
    [
        'DIV' => 'edit5',
        'TAB' => 'Свойства товаров',
        'TITLE' => 'Условия доставки основанные на свойствах товаров',
        'HTML' => '<div id="option_productProps_block">загружается...</div>',
    ],
    [
        'DIV' => 'edit6',
        'TAB' => 'Товары',
        'TITLE' => 'Условия доставки основанные на конкретных товарах',
        'HTML' => '<div id="option_products_block">в разработке...</div>',
    ],
    [
        'DIV' => 'edit7',
        'TAB' => 'Описание/прочие инструменты',
        'TITLE' => 'Описание/прочие инструменты',
        'HTML' => '<div id="option_helper_block">в разработке...</div>',
    ],
    [
        'DIV' => 'edit8',
        'TAB' => 'Слоты по поставщикам',
        'TITLE' => 'Слоты по поставщикам',
        'HTML' => '<div id="option_propsByProviders_block">в разработке...</div>',
    ],
    [
        'DIV' => 'edit9',
        'TAB' => 'Экспресс слот',
        'TITLE' => 'Настройка слота экспресс доставки',
        'HTML' => '<div id="option_express_block">загружается...</div>',
    ]
];

/*
 * Создаем форму для редактирвания параметров модуля
 */
$tabControl = new CAdminTabControl(
    'tabControl',
    $aTabs
);

$tabControl->begin();
?>
    <form action="<?= $APPLICATION->getCurPage(); ?>?mid=<?= $module_id; ?>&lang=<?= LANGUAGE_ID; ?>" method="post">
        <?= bitrix_sessid_post(); ?>
        <?php
        foreach ($aTabs as $aTab) { // цикл по вкладкам
            if ($aTab['HTML']) {
                $tabControl->beginNextTab();
                echo $aTab['HTML'];
            } else if ($aTab['OPTIONS']) {
                $tabControl->beginNextTab();
                __AdmSettingsDrawList($module_id, $aTab['OPTIONS']);
            }
        }
        $tabControl->buttons();
        ?>
        <input type="submit" name="apply" disabled="disabled" id="form_deliverytime_save"
               value="Сохранить" class="adm-btn-save"/>
    </form>

<?php
$tabControl->end();