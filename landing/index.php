<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Ярдекс.рф - Лендинг");
$APPLICATION->SetTitle("Ярдекс.рф - Лендинг");
?>
<? $APPLICATION->IncludeComponent(
    "bitrix:catalog.top",
    "landing_top_slider",
    Array(
        "ACTION_VARIABLE" => "action",
        "ADD_PICT_PROP" => "",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "BASKET_URL" => "/basket.php",
        "BRAND_PROPERTY" => "BRAND_REF",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "N",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
        "COMPARE_PATH" => "",
        "COMPATIBLE_MODE" => "N",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "CUSTOM_FILTER" => '',
        "DATA_LAYER_NAME" => "dataLayer",
        "DISCOUNT_PERCENT_POSITION" => "bottom-right",
        "DISPLAY_COMPARE" => "N",
        "ELEMENT_COUNT" => '20',
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_FIELD2" => "id",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_ORDER2" => "desc",
        "ENLARGE_PRODUCT" => "STRICT",
        "FILTER_NAME" => "",
        "HIDE_NOT_AVAILABLE" => "L",
        "HIDE_NOT_AVAILABLE_OFFERS" => "L",
        "IBLOCK_ID" => 121,
        "IBLOCK_TYPE" => "",
        "LABEL_PROP" => array("SALELEADER"),
        "LABEL_PROP_MOBILE" => array(),
        "LABEL_PROP_POSITION" => "top-left",
        "LINE_ELEMENT_COUNT" => "",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_COMPARE" => "Сравнить",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "MESS_RELATIVE_QUANTITY_FEW" => "мало",
        "MESS_RELATIVE_QUANTITY_MANY" => "много",
        "MESS_SHOW_MAX_QUANTITY" => "Наличие",
        "OFFERS_CART_PROPERTIES" => array("COLOR_REF", "SIZES_SHOES", "SIZES_CLOTHES"),
        "OFFERS_FIELD_CODE" => array("", ""),
        "OFFERS_LIMIT" => "5",
        "OFFERS_PROPERTY_CODE" => array("SIZES_SHOES", "SIZES_CLOTHES", "MORE_PHOTO", ""),
        "OFFERS_SORT_FIELD" => "sort",
        "OFFERS_SORT_FIELD2" => "id",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_ORDER2" => "desc",
        "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
        "OFFER_TREE_PROPS" => array("COLOR_REF", "SIZES_SHOES"),
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRICE_CODE" => array(
            0 => "Цены единицы"
        ),
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
        "PRODUCT_DISPLAY_MODE" => "Y",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPERTIES" => array("NEWPRODUCT"),
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "",
        "PRODUCT_ROW_VARIANTS" => "",
        "PRODUCT_SUBSCRIPTION" => "Y",
        "PROPERTY_CODE" => array("MANUFACTURER", "MATERIAL", ""),
        "PROPERTY_CODE_MOBILE" => array(),
        "RELATIVE_QUANTITY_FACTOR" => "5",
        "ROTATE_TIMER" => "30",
        "SECTION_URL" => '',
        "SEF_MODE" => "N",
        "SEF_RULE" => "",
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "Y",
        "SHOW_MAX_QUANTITY" => "M",
        "SHOW_OLD_PRICE" => "Y",
        "SHOW_PAGINATION" => "Y",
        "SHOW_PRICE_COUNT" => "1",
        "SHOW_SLIDER" => "Y",
        "SLIDER_INTERVAL" => "3000",
        "SLIDER_PROGRESS" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_ENHANCED_ECOMMERCE" => "Y",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "Y",
        "VIEW_MODE" => "SECTION"
    )
); ?>


<img class="landing_bg_image_medium" src="/landing/imgs/landing_medium_bg.jpg">
<img class="landing_bg_image_small" src="/landing/imgs/landing_small_bg.jpg">

    <div class="maxwidth-theme">
        <?
        global $productSlidersMain;
        $productSlidersMain = [
            'ID' => [
                '40632',//Товары по акции
                '40631',//Лидеры продаж
                '40794'//новинки
            ]
        ];
        ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "product_sliders_main",
            Array(
                //"ADDITIONAL_BLOCKS" => "[[\"1042\",\"1\"],[\"1059\",\"321\"],[\"1058\",\"321\"],[\"986\",\"3213\"]]",
                "ACTIVE_DATE_FORMAT" => "j F Y",
                "ADDITIONAL_BLOCKS" => "",
                "ADDITIONAL_IBLOCK_ID" => "114",
                "ADD_SECTIONS_CHAIN" => "N",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array("", ""),
                "FILTER_NAME" => "productSlidersMain",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "120",
                "IBLOCK_TYPE" => "aspro_next_content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "99",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_TITLE" => "",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PRODUCT_COUNT" => "16",
                "PROPERTY_CODE" => array("LINK_GOODS_FILTER", ""),
                "SET_BROWSER_TITLE" => "N",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "SHOW_404" => "N",
                "SORT_BY1" => "SORT",
                "SORT_BY2" => "ID",
                "SORT_ORDER1" => "ASC",
                "SORT_ORDER2" => "DESC",
                "STRICT_SECTION_CHECK" => "N"
            ),
            false,
            Array(
                'HIDE_ICONS' => 'Y'
            )
        ); ?>
    </div>

<?
global $normaListFilter;
$normaListFilter = [
    'ID' => [
        '40633',//Одобрено Енисейским стандартом
        '40635',//Товары для детей
        '40643',//Товары для дома
        '40644',//Товары для животных
        //Старшму поколению
    ]
]
?>
    <div class="maxwidth-theme">
        <? $APPLICATION->IncludeComponent("bitrix:news.list",
            "normal_list",
            Array(
                "IBLOCK_TYPE" => "",
                "IBLOCK_ID" => "120",
                "NEWS_COUNT" => "999",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "normaListFilter",
                "FIELD_CODE" => Array(""),
                "PROPERTY_CODE" => Array(""),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_LAST_MODIFIED" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "N",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_BASE_LINK_ENABLE" => "Y",
                "SET_STATUS_404" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => "",
                "PAGER_BASE_LINK" => "",
                "PAGER_PARAMS_NAME" => "arrPager",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
            )
        ); ?>
    </div>

    <div class="landing_picnic_slider">
        <div class="maxwidth-theme">
            <div class="landing_picnic_slider_title">
                Товары для офиса
            </div>
            <?
            global $productSlidersMain2;
            $productSlidersMain2 = [
                'ID' => [
//                    '40634',//пикник (подборка для тестов)
                    '43460',//офис
                ]
            ]
            ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "product_sliders_main",
                Array(
                    //"ADDITIONAL_BLOCKS" => "[[\"1042\",\"1\"],[\"1059\",\"321\"],[\"1058\",\"321\"],[\"986\",\"3213\"]]",
                    'HIDE_TITLE' => 'Y',
                    'HIDE_SLIDER_PAGINATION' => 'Y',
                    "ACTIVE_DATE_FORMAT" => "j F Y",
                    "ADDITIONAL_BLOCKS" => "",
                    "ADDITIONAL_IBLOCK_ID" => "114",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "Y",
                    "CACHE_GROUPS" => "N",
                    "CACHE_TIME" => "36000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "COMPOSITE_FRAME_MODE" => "A",
                    "COMPOSITE_FRAME_TYPE" => "AUTO",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array("", ""),
                    "FILTER_NAME" => "productSlidersMain2",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "120",
                    "IBLOCK_TYPE" => "aspro_next_content",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "99",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "",
                    "PAGER_TITLE" => "",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PRODUCT_COUNT" => "16",
                    "PROPERTY_CODE" => array("LINK_GOODS_FILTER", ""),
                    "SET_BROWSER_TITLE" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "N",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "SORT",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER1" => "ASC",
                    "SORT_ORDER2" => "DESC",
                    "STRICT_SECTION_CHECK" => "N"
                ),
                false,
                Array(
                    'HIDE_ICONS' => 'Y'
                )
            ); ?>

            <a href="/podborki-tovarov/dlya_ofisa/" class="landing_picnic_show_more">
                Смотреть все
            </a>
        </div>
    </div>
    <div class="maxwidth-theme landing_delivery">
        <div class="landing_delivery_title">как работает доставка</div>
        <div class="landing_delivery_title2">Ярдекс</div>
        <div class="landing_delivery_icons">
            <div class="landing_delivery_icon landing_delivery_icon1">выбор<br>товара</div>
            <div class="landing_delivery_icon landing_delivery_icon2">оформление<br>заказа</div>
            <div class="landing_delivery_icon landing_delivery_icon3">сбор заказа<br> на складе</div>
            <div class="landing_delivery_icon landing_delivery_icon4">доставка<br>курьером</div>
            <div class="landing_delivery_icon_big"></div>
            <div class="landing_delivery_icon landing_delivery_icon5">получение<br>и оплата</div>
        </div>
        <div class="landing_delivery_map"></div>
    </div>

    <div class="maxwidth-theme landing_partners">
        <div class="landing_partners_title">Наши партнеры</div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.top",
            "partners_block",
            Array(
                "ACTION_VARIABLE" => "action",
                "ADD_PICT_PROP" => "",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "ADD_TO_BASKET_ACTION" => "ADD",
                "BASKET_URL" => "/basket.php",
                "BRAND_PROPERTY" => "BRAND_REF",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000",
                "CACHE_TYPE" => "A",
                "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
                "COMPARE_PATH" => "",
                "COMPATIBLE_MODE" => "N",
                "CONVERT_CURRENCY" => "Y",
                "CURRENCY_ID" => "RUB",
                "CUSTOM_FILTER" => '',
                "DATA_LAYER_NAME" => "dataLayer",
                "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                "DISPLAY_COMPARE" => "N",
                "ELEMENT_COUNT" => '20',
                "ELEMENT_SORT_FIELD" => "sort",
                "ELEMENT_SORT_FIELD2" => "id",
                "ELEMENT_SORT_ORDER" => "asc",
                "ELEMENT_SORT_ORDER2" => "desc",
                "ENLARGE_PRODUCT" => "STRICT",
                "FILTER_NAME" => "",
                "HIDE_NOT_AVAILABLE" => "L",
                "HIDE_NOT_AVAILABLE_OFFERS" => "L",
                "IBLOCK_ID" => 123,
                "IBLOCK_TYPE" => "",
                "LABEL_PROP" => array("SALELEADER"),
                "LABEL_PROP_MOBILE" => array(),
                "LABEL_PROP_POSITION" => "top-left",
                "LINE_ELEMENT_COUNT" => "",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_COMPARE" => "Сравнить",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "MESS_RELATIVE_QUANTITY_FEW" => "мало",
                "MESS_RELATIVE_QUANTITY_MANY" => "много",
                "MESS_SHOW_MAX_QUANTITY" => "Наличие",
                "OFFERS_CART_PROPERTIES" => array("COLOR_REF", "SIZES_SHOES", "SIZES_CLOTHES"),
                "OFFERS_FIELD_CODE" => array("", ""),
                "OFFERS_LIMIT" => "5",
                "OFFERS_PROPERTY_CODE" => array("SIZES_SHOES", "SIZES_CLOTHES", "MORE_PHOTO", ""),
                "OFFERS_SORT_FIELD" => "sort",
                "OFFERS_SORT_FIELD2" => "id",
                "OFFERS_SORT_ORDER" => "asc",
                "OFFERS_SORT_ORDER2" => "desc",
                "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                "OFFER_TREE_PROPS" => array("COLOR_REF", "SIZES_SHOES"),
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRICE_CODE" => array(
                    0 => "Цены единицы"
                ),
                "PRICE_VAT_INCLUDE" => "Y",
                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
                "PRODUCT_DISPLAY_MODE" => "Y",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_PROPERTIES" => array("NEWPRODUCT"),
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PRODUCT_QUANTITY_VARIABLE" => "",
                "PRODUCT_ROW_VARIANTS" => "",
                "PRODUCT_SUBSCRIPTION" => "Y",
                "PROPERTY_CODE" => array("MANUFACTURER", "MATERIAL", ""),
                "PROPERTY_CODE_MOBILE" => array(),
                "RELATIVE_QUANTITY_FACTOR" => "5",
                "ROTATE_TIMER" => "30",
                "SECTION_URL" => '',
                "SEF_MODE" => "N",
                "SEF_RULE" => "",
                "SHOW_CLOSE_POPUP" => "N",
                "SHOW_DISCOUNT_PERCENT" => "Y",
                "SHOW_MAX_QUANTITY" => "M",
                "SHOW_OLD_PRICE" => "Y",
                "SHOW_PAGINATION" => "Y",
                "SHOW_PRICE_COUNT" => "1",
                "SHOW_SLIDER" => "Y",
                "SLIDER_INTERVAL" => "3000",
                "SLIDER_PROGRESS" => "N",
                "TEMPLATE_THEME" => "blue",
                "USE_ENHANCED_ECOMMERCE" => "Y",
                "USE_PRICE_COUNT" => "N",
                "USE_PRODUCT_QUANTITY" => "Y",
                "VIEW_MODE" => "SECTION"
            )
        ); ?>
    </div>
    <div class="maxwidth-theme landing_smi">
        <div class="landing_smi_title">Сми о нас</div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:catalog.top",
            "smi_slider",
            Array(
                "ACTION_VARIABLE" => "action",
                "ADD_PICT_PROP" => "",
                "ADD_PROPERTIES_TO_BASKET" => "Y",
                "ADD_TO_BASKET_ACTION" => "ADD",
                "BASKET_URL" => "/basket.php",
                "BRAND_PROPERTY" => "BRAND_REF",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "CACHE_TIME" => "36000",
                "CACHE_TYPE" => "A",
                "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
                "COMPARE_PATH" => "",
                "COMPATIBLE_MODE" => "N",
                "CONVERT_CURRENCY" => "Y",
                "CURRENCY_ID" => "RUB",
                "CUSTOM_FILTER" => '',
                "DATA_LAYER_NAME" => "dataLayer",
                "DISCOUNT_PERCENT_POSITION" => "bottom-right",
                "DISPLAY_COMPARE" => "N",
                "ELEMENT_COUNT" => '20',
                "ELEMENT_SORT_FIELD" => "sort",
                "ELEMENT_SORT_FIELD2" => "id",
                "ELEMENT_SORT_ORDER" => "asc",
                "ELEMENT_SORT_ORDER2" => "desc",
                "ENLARGE_PRODUCT" => "STRICT",
                "FILTER_NAME" => "",
                "HIDE_NOT_AVAILABLE" => "L",
                "HIDE_NOT_AVAILABLE_OFFERS" => "L",
                "IBLOCK_ID" => 122,
                "IBLOCK_TYPE" => "",
                "LABEL_PROP" => array("SALELEADER"),
                "LABEL_PROP_MOBILE" => array(),
                "LABEL_PROP_POSITION" => "top-left",
                "LINE_ELEMENT_COUNT" => "",
                "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                "MESS_BTN_BUY" => "Купить",
                "MESS_BTN_COMPARE" => "Сравнить",
                "MESS_BTN_DETAIL" => "Подробнее",
                "MESS_NOT_AVAILABLE" => "Нет в наличии",
                "MESS_RELATIVE_QUANTITY_FEW" => "мало",
                "MESS_RELATIVE_QUANTITY_MANY" => "много",
                "MESS_SHOW_MAX_QUANTITY" => "Наличие",
                "OFFERS_CART_PROPERTIES" => array("COLOR_REF", "SIZES_SHOES", "SIZES_CLOTHES"),
                "OFFERS_FIELD_CODE" => array("", ""),
                "OFFERS_LIMIT" => "5",
                "OFFERS_PROPERTY_CODE" => array("SIZES_SHOES", "SIZES_CLOTHES", "MORE_PHOTO", ""),
                "OFFERS_SORT_FIELD" => "sort",
                "OFFERS_SORT_FIELD2" => "id",
                "OFFERS_SORT_ORDER" => "asc",
                "OFFERS_SORT_ORDER2" => "desc",
                "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                "OFFER_TREE_PROPS" => array("COLOR_REF", "SIZES_SHOES"),
                "PARTIAL_PRODUCT_PROPERTIES" => "N",
                "PRICE_CODE" => array(
                    0 => "Цены единицы"
                ),
                "PRICE_VAT_INCLUDE" => "Y",
                "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
                "PRODUCT_DISPLAY_MODE" => "Y",
                "PRODUCT_ID_VARIABLE" => "id",
                "PRODUCT_PROPERTIES" => array("NEWPRODUCT"),
                "PRODUCT_PROPS_VARIABLE" => "prop",
                "PRODUCT_QUANTITY_VARIABLE" => "",
                "PRODUCT_ROW_VARIANTS" => "",
                "PRODUCT_SUBSCRIPTION" => "Y",
                "PROPERTY_CODE" => array("MANUFACTURER", "MATERIAL", ""),
                "PROPERTY_CODE_MOBILE" => array(),
                "RELATIVE_QUANTITY_FACTOR" => "5",
                "ROTATE_TIMER" => "30",
                "SECTION_URL" => '',
                "SEF_MODE" => "N",
                "SEF_RULE" => "",
                "SHOW_CLOSE_POPUP" => "N",
                "SHOW_DISCOUNT_PERCENT" => "Y",
                "SHOW_MAX_QUANTITY" => "M",
                "SHOW_OLD_PRICE" => "Y",
                "SHOW_PAGINATION" => "Y",
                "SHOW_PRICE_COUNT" => "1",
                "SHOW_SLIDER" => "Y",
                "SLIDER_INTERVAL" => "3000",
                "SLIDER_PROGRESS" => "N",
                "TEMPLATE_THEME" => "blue",
                "USE_ENHANCED_ECOMMERCE" => "Y",
                "USE_PRICE_COUNT" => "N",
                "USE_PRODUCT_QUANTITY" => "Y",
                "VIEW_MODE" => "SECTION"
            )
        ); ?>
    </div>
<?
//    <div class="maxwidth-theme landing_delivery_coins">
//        <img src="/landing/imgs/bgCoins.jpg"/>
//    </div>
    ?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>