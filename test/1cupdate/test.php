<?
define('BX_SESSION_ID_CHANGE', false);
define('BX_SKIP_POST_UNQUOTE', true);
define("NO_AGENT_CHECK", true);
define("NO_AGENT_STATISTIC", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//$_REQUEST['mode'] ='import';
//$_REQUEST['type'] ='sale';
//die();
//error_reporting(E_ALL);
///bitrix/admin/1cv7_exchange.php?type=catalog&mode=import&filename=del.csv&delete=1 - последний вызов при полном обмене
$USER->Authorize(7);//7 - пользователь обмена

$mode = $_GET['mode'] ?? false;
if ($mode == 'sale') { //загрузка заказов/////////////////////////////
    $_REQUEST['type'] = 'sale';
    $_REQUEST['mode'] = 'query';
} else if ($mode == 'final') { //финальный запрос из 1с/////////////////////////////
    $_REQUEST['type'] = 'catalog';
    $_REQUEST['mode'] = 'import';
    $_REQUEST['filename'] = 'del.csv';
    $_GET['delete'] = '1';
} else if ($mode == 'catalog') { //загрузка товаров/////////////////////////////
    $_REQUEST['type'] = 'catalog';
    $_REQUEST['mode'] = 'import';
    $_REQUEST['filename'] = 'catalog0.csv';
    copyOrderTestFile($_REQUEST['filename']);
} else if ($mode == 'offers') { //загрузка товаров/////////////////////////////
    $_REQUEST['type'] = 'catalog';
    $_REQUEST['mode'] = 'import';
    $_REQUEST['filename'] ='offers0.csv';
    copyOrderTestFile($_REQUEST['filename']);
} else if ($mode == 'order') { //загрузка товаров/////////////////////////////
    $_REQUEST['type'] = 'sale';
    $_REQUEST['mode'] = 'import';
    if (!isset($_REQUEST['filename'])){
        $_REQUEST['filename'] = 'order_20210825_094039.xml';
    }
    copyOrderTestFile($_REQUEST['filename']);
} else {
    die('нет mode в параметрах вызова');
}


?>
<?
IncludeModuleLangFile(__FILE__);

$res = CModule::IncludeModuleEx('htmls.1c77exchange');
//echo $res;
if ($res == 3) die(utf2win($MESS["V77EXCHANGE_MODULE_DEMO_EXPIRED"]));

if ($_REQUEST['mode'] == 'init') {
    $res = COption::GetOptionString("catalog", "1C_USE_ZIP", "Y");
    if (CModule::IncludeModuleEx("htmls.1c77plus") < 3) {
        $res .= '@' . COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_USE_STORE", "N");;
    } else {
        $res .= '@N';
    }
    $res .= '@' . COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_USE_SKU", "N");
    $res .= '@' . COption::GetOptionString("catalog", "1C_SKIP_ROOT_SECTION", "N");
    $res .= '@' . COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_CATALOG_USE_BASE64", "N");
    echo $res;
}
if ($_REQUEST['type'] == 'catalog') {
    CModule::IncludeModule('iblock');
    CModule::IncludeModule('catalog');

    $V7Exchange = new CV7ExchangeProcessing;

    if ($_REQUEST['mode'] == 'checkauth') {
        $V7Exchange->Auth();
    }

    if ($_REQUEST['mode'] == 'file') {
        $V7Exchange->Upload();
    }

    if ($_REQUEST['mode'] == 'import') {
        $file = $_REQUEST['filename'];
        dump($file);
        if ($file == 'file') {
            $V7Exchange->ImportOffLine();
        } else {
            $V7Exchange->Import();
        }
    }
}
if ($_REQUEST['type'] == 'sale') {
    CModule::IncludeModule('sale');
    $V7Exchange = new CV7ExchangeProcessing;

    if ($_REQUEST['mode'] == 'checkauth') {
        $V7Exchange->Auth();
    }

    if ($_REQUEST['mode'] == 'query') {
        $V7Exchange->Export();
    }

    if ($_REQUEST['mode'] == 'status') {
        $V7Exchange->SetStatus();
    }

    if ($_REQUEST['mode'] == 'file') {
        $V7Exchange->Upload();
    }
    if ($_REQUEST['mode'] == 'import') {
        $V7Exchange->ImportOrders();
    }

    if ($_REQUEST['mode'] == 'import_profiles') {
        $V7Exchange->ImportProfiles();
    }

    if ($_REQUEST['mode'] == 'query_profiles') {
        $V7Exchange->QueryProfiles();
    }

    if ($_REQUEST['mode'] == 'import_userprice') {
        $V7Exchange->ImportUserPrice();
    }

    if ($_REQUEST['mode'] == 'import_sale') {
        $V7Exchange->ImportSale();
    }
}
function utf2win($str)
{
    if (strtoupper(LANG_CHARSET) == 'UTF-8')
        return iconv('UTF-8', 'WINDOWS-1251', $str);
    else
        return $str;
}

?>

<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

function copyOrderTestFile($filename)
{
    $fromPath = __DIR__ . '/' . $filename;
    $toPath = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/tmp/htmls.1c77exchange/" . $filename;
    if (copy($fromPath, $toPath)) {
        echo 'скопирован файл в ' . $toPath . '<br>';
    } else {
        echo 'НЕ! скопирован файл в ' . $toPath;
        die('ошибка');
    }
}


