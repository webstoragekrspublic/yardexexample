<?
/*******************************************************************************
 * Module:   1Cv77Exchange                                                      *
 * Version:  3.0.28                                                             *
 * Author:   Alexey Andreev, HTMLStudio (www.htmls.ru, www.bx77.ru)             *
 * License:  Shareware                                                          *
 * Date:     2017-11-15                                                         *
 * e-mail:   aav@htmls.ru                                                       *
 *******************************************************************************/

//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/htmls.1c77exchange/log.txt"); - include.php
IncludeModuleLangFile(__FILE__);
CModule::IncludeModule("currency");

class CV7ExchangeProcessing extends CV7Exchange
{
    function CV7ExchangeProcessing() {
        $this->IP_LIST = explode(',', COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IP_LIST"));
        $this->CGI = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_PHPLikeCGI");
        $this->CHECK_IP = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_CHECK_IP");
        $this->TMP_FOLDER = $_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/htmls.1c77exchange/";
        $this->USE_ZIP = COption::GetOptionString("catalog", "1C_USE_ZIP", "Y");
        $this->IMG_FOLDER = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IMG_IMPORT_FOLDER");
        $this->IMG_SUBFOLDER = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IMG_IMPORT_SUBFOLDER", '');
        $this->IMG_MULTI = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IMG_IMPORT_MULTI", "N");
        $this->IMG_TYPE = explode(',', COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IMG_IMPORT_TYPES", "jpg,jpeg,png,gif"));
        $this->IMG_LOG = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IMG_IMPORT_LOG");
        $this->KEEP_STR = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_KEEP_LOCAL_CATALOG_STRUCTURE", "N");
        $this->UPD_NAME = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_CATALOG_UPDATE_NAME", "Y");
        $this->IMG_DEL = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IMG_IMPORT_DELETE");
        $this->NEGATIVE_AMOUNT_TRACE = COption::GetOptionString("catalog", "allow_negative_amount", "N");
        $this->CAN_BUY_ZERO = COption::GetOptionString("catalog", "default_can_buy_zero", "N");
        $this->FINAL_ORDERS = COption::GetOptionString("sale", '1C_EXPORT_FINAL_ORDERS', 'N');
        //f(defined('BUSINESS_EDITION') && 'Y' == BUSINESS_EDITION){
        if (!CBXFeatures::IsFeatureEnabled('CatMultiStore')) {
            $this->BASE_EDITION = true;
        } else {
            $this->BASE_EDITION = false;
        }
        $this->USE_STORE = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_USE_STORE", "N");
        $this->USE_SKU = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_USE_SKU", "N");
        //$this->IMG_METHOD = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IMG_IMPORT_METHOD");
        $this->Base64 = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_CATALOG_USE_BASE64", "N");

        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/"))
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/");

        if (!is_dir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/htmls.1c77exchange/"))
            mkdir($_SERVER['DOCUMENT_ROOT'] . "/bitrix/tmp/htmls.1c77exchange/");

        if (file_exists($this->TMP_FOLDER . 'cache.txt')) {
            $fname = $this->TMP_FOLDER . 'cache.txt';
            $handle = fopen($fname, "rb");
            $arStr = fread($handle, filesize($fname));
            $this->CACHE = unserialize($arStr);
        }

        /*if(file_exists($this->TMP_FOLDER . 'noimg.csv')){
            $fname = $this->TMP_FOLDER . 'noimg.csv';
            $handle = fopen($fname, "rb");
            $arStr = fread($handle, filesize($fname));
            $this->NO_IMGS = unserialize($arStr);
    }*/
    }

    function Upload() {
        $headers = $this->emu_getallheaders();
        $res = $this->Check($headers);
        $r = $res[0];
        if (!$r) die('Access Denied! - ' . $res[1]);
        // get zip for import
        $fh = fopen("php://input", "rb");
        if ($fh === false) {
            die('error getting file');
        }

        if (file_exists($this->TMP_FOLDER . 'cache.txt')) {
            @unlink($this->TMP_FOLDER . 'cache.txt');
        }
        if (file_exists($this->TMP_FOLDER . 'noimg.csv')) {
            @unlink($this->TMP_FOLDER . 'noimg.csv');
        }

        $name = $_REQUEST['filename'];

        $fileTarget = $this->TMP_FOLDER . $name;
        $ft = fopen($fileTarget, "w+b");
        stream_copy_to_stream($fh, $ft);
        fclose($fh);
        fclose($ft);

        if (filesize($fileTarget) > 0) {
            //+++
            if ($name == 'images.zip') {
                $zip = new ZipArchive;
                if ($zip->open($fileTarget) === true) {
                    $folder = $_SERVER["DOCUMENT_ROOT"] . $this->IMG_FOLDER;
                    $this->deleteAllImgFolder($folder);
                    $zip->extractTo($folder);
                    $zip->close();
                    echo 'success';
                } else {
                    echo 'failure';
                }
                @unlink($fileTarget);
            } //^^^
            elseif (strpos($name, '.jpg') > 0) {
                $folder = $_SERVER["DOCUMENT_ROOT"] . $this->IMG_FOLDER;
                if (copy($fileTarget, $folder . $name)) {
                    @unlink($fileTarget);
                    echo "success";
                } else
                    echo "failure";
            } else {
                if ($this->USE_ZIP == 'Y') {
                    $zip = zip_open($fileTarget);

                    $today = date("d.m.y");
                    $time = date("H.i.s");
                    $orders_dir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/htmls.1c77exchange/admin/orders_zip/' . $today;
                    if (!file_exists($orders_dir)) {
                        mkdir($orders_dir, 0777, true);
                    }
                    $filename = $orders_dir  .  '/' . $time . '_' . $name;
                    copy($fileTarget, $filename);
                    
                    if ($zip) {
                        while ($zip_entry = zip_read($zip)) {
                            if (zip_entry_open($zip, $zip_entry, "r")) {
                                $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                                file_put_contents($this->TMP_FOLDER . zip_entry_name($zip_entry), $buf);
                                zip_entry_close($zip_entry);
                            }
                        }
                        zip_close($zip);
                    }
                    @unlink($fileTarget);
                }
                echo "success";
            }
        } else
            echo "File " . $name . " not uploaded";
    }

    function Import() {
        global $USER;
        $headers = $this->emu_getallheaders();
        $res = $this->Check($headers);
        $r = $res[0];
        if (!$r) die('Access Denied! - ' . $res[1]);

        $file = $_REQUEST['filename'];
        if ($file == 'md_catalog.csv') {
            $this->ImportMD($file);
            $this->afterProductsUpdate();
        }

        if ($file == 'ib_iblocks.csv') $this->ImportIB($file);
        if (substr($file, 0, 8) == 'sections') $this->ImportSEC($file, $headers['cnt']);
        if (substr($file, 0, 7) == 'catalog') $this->ImportCatalog($file, $headers['cnt']);
        if (substr($file, 0, 6) == 'offers') $this->ImportOffers($file, $headers['cnt']);
        if (substr($file, 0, 12) == 'store_offers') $this->ImportStoreOffers($file, $headers['cnt']);
        if (!empty($_GET['delete']) && $_GET['delete'] == 1) {
            if ($file == 'del.csv') $this->DeleteElemSec($headers['cnt']);
            $this->afterProductsUpdate();
        }
        if ($file == 'fin.csv') $this->fin($headers['cnt']);

    }

    function ImportOffLine() {
        global $USER;
        $headers = $this->emu_getallheaders();
        $res = $this->Check($headers);
        $r = $res[0];
        if (!$r) die('Access Denied! - ' . $res[1]);
        //$file = $_REQUEST['filename'];
        if (file_exists($this->TMP_FOLDER . 'md_properties.csv')) {
            $file = 'md_catalog.csv';
            $this->ImportMD($file);
            //return;
        }
        if (file_exists($this->TMP_FOLDER . 'ib_iblocks.csv')) {
            $file = 'ib_iblocks.csv';
            $this->ImportIB($file);
            //return;
        }
        for ($s = 0; $s < 1000; $s++) {
            if (file_exists($this->TMP_FOLDER . 'sections' . $s . '.csv')) {
                $file = 'sections' . $s . '.csv';
                $this->ImportSEC($file, $headers['cnt']);
                //return;
            }

        }
        for ($c = 0; $c < 5000; $c++) {
            if (file_exists($this->TMP_FOLDER . 'catalog' . $c . '.csv')) {
                $file = 'catalog' . $c . '.csv';
                $this->ImportCatalog($file, $headers['cnt']);
                //return;
            }
        }

        for ($o = 0; $o < 10000; $o++) {
            if (file_exists($this->TMP_FOLDER . 'offers' . $o . '.csv')) {
                $file = 'offers' . $o . '.csv';
                $this->ImportOffers($file, $headers['cnt']);
                //return;
            }
        }

        $this->DeleteElemSec();
    }

    //import metadata - start
    function ImportCatalogID($file = 'md_catalog.csv') {
        $ar = $this->ParseCSV($file);
        $arResult['CATALOG_ID'] = $ar[0]['CATALOG_ID'];
        $arResult['CATALOG_NAME'] = $ar[0]['CATALOG_NAME'];
        return $arResult;
    }

    function getMeasureList() {
        $arMeasure = array();
        $res = CCatalogMeasure::getList();
        while ($arItem = $res->Fetch()) {
            $arMeasure[$arItem['CODE']] = $arItem['ID'];
        }
        return $arMeasure;
    }

    function ImportMD($file) {
        global $DB, $USER, $MESS;
        $ar = $this->ParseCSV($file);
        $arResult['CATALOG_ID'] = $ar[0]['CATALOG_ID'];
        $arResult['CATALOG_NAME'] = $ar[0]['CATALOG_NAME'];
        $cache = $this->CACHE;
        if (!is_array($cache)) {
            $cache = array();
        }
        $ar = $this->ParseCSV("md_properties.csv");
        $arCAT = array();
        $arSKU = array();
        foreach ($ar as $k => $prop) {

            if ($prop['TYPE'] == 'L') {
                $list = $this->ParseCSV("md_" . strtolower($prop['CODE']) . ".csv");
                if ($list) {
                    $arList = array();
                    foreach ($list as $val)
                        $arList[$val['XML_ID']] = $val['VALUE'];
                    $ar[$k]['LIST'] = $arList;
                    @unlink($this->TMP_FOLDER . "md_" . strtolower($prop['CODE']) . ".csv");
                }
            }
            if ($prop['TYPE'] == 'LC') {
                //$list = $this->ParseCSV("md_" . strtolower($prop['CODE']) . ".csv");
                //if ($list){
                $arList = array();
                //foreach($list as $val)
                $arList[$ar[$k]['CODE']] = 1;
                $ar[$k]['LIST'] = $arList;

                @unlink($this->TMP_FOLDER . "md_" . strtolower($prop['CODE']) . ".csv");
                //}
            }
            if ($prop['ibTYPE'] == 'SKU') {
                $arSKU[$k] = $ar[$k];
            } else {
                $arCAT[$k] = $ar[$k];
            }
        }

        if ($this->IMG_MULTI == 'Y') {
            $arCAT[] = array('TYPE' => 'F', 'CODE' => 'MORE_PHOTO', 'NAME' => 'MORE_PHOTO', 'MULTIPLE' => 'Y');
        }
        $arResult['PROPERTIES'] = $arCAT;
        //check iblock
        $IBLOCK_TYPE = COption::GetOptionString("catalog", "1C_IBLOCK_TYPE", "-");
        if ($IBLOCK_TYPE == '-') {
            $strSql = "SELECT ID FROM  b_iblock_type WHERE ID='catalog1c77'";
            $w = $DB->Query($strSql, false);
            //check iblock type
            $res = CIBlockType::GetByID('catalog1c77');
            $arIBT = $res->Fetch();
            if (!$arIBT || !$w->Fetch()) {
                $arFields = Array(
                    'ID' => 'catalog1c77', 'SECTIONS' => 'Y', 'IN_RSS' => 'N', 'SORT' => 100,
                    'LANG' => Array('en' => Array('NAME' => 'Catalog1C77', 'SECTION_NAME' => 'Sections', 'ELEMENT_NAME' => 'Products')));

                $obBlocktype = new CIBlockType;
                $DB->StartTransaction();
                $res = $obBlocktype->Add($arFields);
                if (!$res) {
                    $DB->Rollback();
                    AddMessage2Log('010->Error: ' . $obBlocktype->LAST_ERROR, "htmls.1c77exchange");
                    die('010->Error: ' . $obBlocktype->LAST_ERROR);
                } else
                    $DB->Commit();
            }
            $IBLOCK_TYPE = 'catalog1c77';
        }
        //get site list
        $SITE_LIST = array(COption::GetOptionString("catalog", "1C_SITE_LIST", "-"));
        if ($SITE_LIST[0] == '-') {
            $rsSites = CSite::GetList($by = "sort", $order = "desc");
            $SITE_LIST = array();
            while ($arSite = $rsSites->Fetch()) {
                $SITE_LIST[] = $arSite['LID'];
            }
        }
        //if iblock not find by xml_id, create new iblock
        $res = CIBlock::GetList(Array(), Array('XML_ID' => $arResult['CATALOG_ID'], "CHECK_PERMISSIONS" => "N"), true);
        if ($ar_res = $res->Fetch()) {
            $IBLOCK_ID = $ar_res['ID'];
        } else {
            $arFields = array('CODE' => 'Catalog1C', 'XML_ID' => $arResult['CATALOG_ID'], 'IBLOCK_TYPE_ID' => $IBLOCK_TYPE, 'NAME' => $arResult['CATALOG_NAME'], 'ACTIVE' => 'Y', "SITE_ID" => $SITE_LIST);
            $ib = new CIBlock;
            $arFields["GROUP_ID"] = array(2 => "R");
            $IBLOCK_ID = $ib->Add($arFields);
            if (!$IBLOCK_ID) {
                AddMessage2Log('020->Error: ' . $ib->LAST_ERROR, "htmls.1c77exchange");
                AddMessage2Log('021->Error: ' . print_r($arFields, true), "htmls.1c77exchange");
                AddMessage2Log('022->Error: ' . print_r($arResult, true), "htmls.1c77exchange");
                die('02->Error (ImportMD): ' . $ib->LAST_ERROR);
            }
            $arFields = array("IBLOCK_ID" => $IBLOCK_ID, "YANDEX_EXPORT" => 'N', "SUBSCRIPTION" => 'N');
            CCatalog::Add($arFields);
        }
        //add/update iblock properties
        //AddMessage2Log(print_r($arResult['PROPERTIES'], true), "htmls.1c77exchange");
        foreach ($arResult['PROPERTIES'] as $prop) {
            if ($prop['CODE'] == 'SORT' ||
                $prop['CODE'] == 'LENGTH' ||
                $prop['CODE'] == 'WIDTH' ||
                $prop['CODE'] == 'HEIGHT') continue;
            elseif ($prop['CODE'] == 'MEASURE') {
                $arMeasure = $this->getMeasureList();
                foreach ($prop['LIST'] as $id => $val) {
                    $code = intval($id);
                    if (!$arMeasure[$code]) {
                        $arFields = array('CODE' => $code, 'MEASURE_TITLE' => $val);
                        CCatalogMeasure::add($arFields);
                    }
                }

            } else {
                $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID, "CODE" => $prop['CODE']));
                $id = 1;
                $ibp = new CIBlockProperty;

                if ($properties->SelectedRowsCount() == 0) {
                    //no properties
                    $arFields = Array("NAME" => $prop['NAME'],
                        "ACTIVE" => "Y", "SORT" => 100 * $id,
                        "CODE" => $prop['CODE'], "PROPERTY_TYPE" => $prop['TYPE'],
                        "IBLOCK_ID" => $IBLOCK_ID);
                    //add new property
                    if ($prop['TYPE'] == 'E') {
                        //AddMessage2Log(print_r($prop, true), "htmls.1c77exchange");
                        if ($prop['IBLOCK_CODE']) {
                            $res = CIBlock::GetList(Array(), Array('TYPE' => $IBLOCK_TYPE, "CODE" => $prop['IBLOCK_CODE']), true);
                        } else {
                            $res = CIBlock::GetList(Array(), Array('TYPE' => $IBLOCK_TYPE, "CODE" => $prop['CODE']), true);
                        }

                        while ($ar_res = $res->Fetch()) {
                            $arFields['LINK_IBLOCK_ID'] = $ar_res['ID'];
                        }

                        //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
                    } elseif ($prop['TYPE'] == 'LC') {
                        $arFields['PROPERTY_TYPE'] = 'L';
                        $arFields['LIST_TYPE'] = 'C';
                    } elseif ($prop['TYPE'] == 'F') {
                        $arFields['MULTIPLE'] = 'Y';
                        //$arFields['LIST_TYPE'] = 'C';
                    }
                    //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
                    $PropID = $ibp->Add($arFields);
                    if (!$PropID) {
                        AddMessage2Log('030->Error: ' . $ibp->LAST_ERROR, "htmls.1c77exchange");
                    } else {
                        if ($prop['TYPE'] == 'L' || $prop['TYPE'] == 'LC') {
                            //property type = list
                            $ibpenum = new CIBlockPropertyEnum;
                            foreach ($prop['LIST'] as $id => $val) {
                                $enumID = $ibpenum->Add(Array('PROPERTY_ID' => $PropID, 'VALUE' => $val, 'XML_ID' => $id));
                                if (!$enumID) {
                                    AddMessage2Log('040->Error: ' . $ibpenum->LAST_ERROR, "htmls.1c77exchange");
                                }
                                $props[$id] = $enumID;
                            }
                            $cache[$prop['CODE']] = $props;
                        }
                    }
                } else {
                    $prop_fields = $properties->GetNext();
                    $PropID = $prop_fields['ID'];
                    if ($prop['TYPE'] == 'E') {
                        if ($prop_fields['LINK_IBLOCK_ID'] == 0) {
                            if ($prop['IBLOCK_CODE']) {
                                $res = CIBlock::GetList(Array(), Array('TYPE' => $IBLOCK_TYPE, "CODE" => $prop['IBLOCK_CODE']), true);
                            } else {
                                $res = CIBlock::GetList(Array(), Array('TYPE' => $IBLOCK_TYPE, "CODE" => $prop['CODE']), true);
                            }
                            while ($ar_res = $res->Fetch()) {
                                $arFields['LINK_IBLOCK_ID'] = $ar_res['ID'];
                            }
                            $ibp->Update($PropID, $arFields);
                        }
                    }
                    if ($prop['TYPE'] == 'L' || $prop['TYPE'] == 'LC') {
                        $ibpenum = new CIBlockPropertyEnum;
                        //AddMessage2Log($prop, "htmls.1c77exchange");
                        $props = $this->GetEnumList($IBLOCK_ID, $prop);

                        foreach ($prop['LIST'] as $id => $val) {
                            if (!array_key_exists($id, $props)) {
                                $enumID = $ibpenum->Add(Array('PROPERTY_ID' => $PropID, 'VALUE' => $val, 'XML_ID' => $id));
                                if (!$enumID) {
                                    AddMessage2Log('050->Error: ' . $ibpenum->LAST_ERROR, "htmls.1c77exchange");
                                }
                                $props[$id] = $enumID;
                            }
                        }
                        $cache[$prop['CODE']] = $props;
                    }
                }
                $id++;
            }
        }

        //sku - start
        if ($this->USE_SKU == 'Y') {
            $res = CIBlock::GetList(Array(), Array('XML_ID' => $arResult['CATALOG_ID'] . '#', "CHECK_PERMISSIONS" => "N"), true);
            $addCatalog = false;
            if ($ar_res = $res->Fetch()) {
                $IBLOCK_SKU_ID = $ar_res['ID'];
            } else {
                $arFields = array('CODE' => 'Catalog1COffer', 'XML_ID' => $arResult['CATALOG_ID'] . '#', 'IBLOCK_TYPE_ID' => $IBLOCK_TYPE, 'NAME' => $arResult['CATALOG_NAME'] . GetMessage('SKU'), 'ACTIVE' => 'Y', "SITE_ID" => $SITE_LIST);
                $ib = new CIBlock;
                $arFields["GROUP_ID"] = array(2 => "R");
                $IBLOCK_SKU_ID = $ib->Add($arFields);
                if (!$IBLOCK_SKU_ID) {
                    AddMessage2Log('020->Error: ' . $ib->LAST_ERROR, "htmls.1c77exchange");
                    AddMessage2Log('021->Error: ' . print_r($arFields, true), "htmls.1c77exchange");
                    AddMessage2Log('022->Error: ' . print_r($arResult, true), "htmls.1c77exchange");
                    die('02->Error (ImportMD): ' . $ib->LAST_ERROR);
                }
                $addCatalog = true;
                $arCatalog = array("IBLOCK_ID" => $IBLOCK_SKU_ID,
                    "YANDEX_EXPORT" => 'N',
                    "SUBSCRIPTION" => 'N',
                    'PRODUCT_IBLOCK_ID' => $IBLOCK_ID);
                //CCatalog::Add($arCatalog);
            }

            //add/update sku-iblock properties
            $arSKU['CML2_LINK'] = array('NAME' => 'CML2_LINK', 'CODE' => 'CML2_LINK', 'TYPE' => 'E:SKU');
            foreach ($arSKU as $prop) {
                $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_SKU_ID, "CODE" => $prop['CODE']));
                $id = 1;
                $ibp = new CIBlockProperty;
                if ($prop['CODE'] == 'SORT') continue;
                if ($properties->SelectedRowsCount() == 0) {
                    //no properties
                    $arFields = Array("NAME" => $prop['NAME'],
                        "ACTIVE" => "Y", "SORT" => 100 * $id,
                        "CODE" => $prop['CODE'], "PROPERTY_TYPE" => $prop['TYPE'],
                        "IBLOCK_ID" => $IBLOCK_SKU_ID);
                    //add new property
                    if ($prop['TYPE'] == 'E') {
                        $res = CIBlock::GetList(Array(), Array('TYPE' => 'catalog1c77', "CODE" => $prop['CODE']), true);
                        while ($ar_res = $res->Fetch()) {
                            $arFields['LINK_IBLOCK_ID'] = $ar_res['ID'];
                        }

                        //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
                    }
                    if ($prop['TYPE'] == 'E:SKU') {
                        $arFields['LINK_IBLOCK_ID'] = $IBLOCK_ID;
                        $arFields['PROPERTY_TYPE'] = 'E';
                        $arFields['USER_TYPE'] = 'SKU';
                    } elseif ($prop['TYPE'] == 'LC') {
                        $arFields['PROPERTY_TYPE'] = 'L';
                        $arFields['LIST_TYPE'] = 'C';
                    } elseif ($prop['TYPE'] == 'F') {
                        $arFields['MULTIPLE'] = 'Y';
                        //$arFields['LIST_TYPE'] = 'C';
                    }

                    $PropID = $ibp->Add($arFields);
                    if (!$PropID) {
                        AddMessage2Log('030->Error: ' . $ibp->LAST_ERROR, "htmls.1c77exchange");
                    } else {
                        if ($prop['TYPE'] == 'L' || $prop['TYPE'] == 'LC') {
                            //property type = list
                            $ibpenum = new CIBlockPropertyEnum;
                            foreach ($prop['LIST'] as $id => $val) {
                                $enumID = $ibpenum->Add(Array('PROPERTY_ID' => $PropID, 'VALUE' => $val, 'XML_ID' => $id));
                                if (!$enumID) {
                                    AddMessage2Log('040->Error: ' . $ibpenum->LAST_ERROR, "htmls.1c77exchange");
                                }
                                $props[$id] = $enumID;
                            }
                            $cache[$prop['CODE']] = $props;
                        } elseif ($prop['TYPE'] == 'E:SKU') {
                            $pSKU = $PropID;
                        }
                    }
                } else {
                    $prop_fields = $properties->GetNext();
                    $PropID = $prop_fields['ID'];
                    if ($prop['TYPE'] == 'E') {
                        if ($prop_fields['LINK_IBLOCK_ID'] == 0) {
                            $res = CIBlock::GetList(Array(), Array('TYPE' => 'catalog1c77', "CODE" => $prop['CODE']), true);
                            while ($ar_res = $res->Fetch()) {
                                $arFields['LINK_IBLOCK_ID'] = $ar_res['ID'];
                            }
                            $ibp->Update($PropID, $arFields);
                        }
                    }
                    if ($prop['TYPE'] == 'L' || $prop['TYPE'] == 'LC') {
                        $ibpenum = new CIBlockPropertyEnum;
                        //AddMessage2Log($prop, "htmls.1c77exchange");
                        $props = $this->GetEnumList($IBLOCK_SKU_ID, $prop);

                        foreach ($prop['LIST'] as $id => $val) {
                            if (!array_key_exists($id, $props)) {
                                $enumID = $ibpenum->Add(Array('PROPERTY_ID' => $PropID, 'VALUE' => $val, 'XML_ID' => $id));
                                if (!$enumID) {
                                    AddMessage2Log('050->Error: ' . $ibpenum->LAST_ERROR, "htmls.1c77exchange");
                                }
                                $props[$id] = $enumID;
                            }
                        }
                        $cache[$prop['CODE']] = $props;
                    }
                }
                $id++;
            }

            if ($addCatalog) {
                $arCatalog['SKU_PROPERTY_ID'] = $pSKU;
                CCatalog::Add($arCatalog);
            }
        }
        //SKU - end
        //set cache file
        $cache['CATALOG_ID'] = $IBLOCK_ID;
        $cache['SKU_ID'] = $IBLOCK_SKU_ID;
        $cache['IBLOCK_TYPE'] = $IBLOCK_TYPE;
        $cache['SITE_LIST'] = $SITE_LIST;
        if ($this->USE_STORE == 'Y') {
            if (CModule::IncludeModuleEx("htmls.1c77plus") < 3) {
                if (IsModuleInstalled("htmls.1c77plus")) {
                    $arStore = $this->ParseCSV('store.csv');
                    $V7Plus = new CV7Plus();
                    $cache['STORE'] = $V7Plus->Store($arStore);
                }
            }
        }
        $handle = fopen($this->TMP_FOLDER . 'cache.txt', "w");
        fwrite($handle, serialize($cache));
        $filename = $this->TMP_FOLDER . 'md_properties.csv';
        @unlink($filename);
        echo "Import MetaData - success.";
    }//ImportMD()

    //$p['CODE'] - property code
    function GetEnumList($IBLOCK_ID, $p) {
        global $USER;

        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $IBLOCK_ID, "CODE" => $p['CODE']));
        while ($enum_fields = $property_enums->GetNext()) {
            $props[$enum_fields["XML_ID"]] = $enum_fields["ID"];
        }
        return $props;
    }//GetEnumList($IBLOCK_ID, $p)
    //import metadata - end

    //import iblock elements - start
    function ImportIB($file) {
        global $USER;
        $arIB = $this->ParseCSV($file);
        $arIB = $this->AddElements($arIB);
        $arResult = $this->ImportCatalogID();
        $arResult['IBlocks'] = $arIB;
        if (is_array($this->CACHE)) {
            $IBLOCK_TYPE = $this->CACHE['IBLOCK_TYPE'];
            $SITE_LIST = $this->CACHE['SITE_LIST'];
        } else {
            $IBLOCK_TYPE = COption::GetOptionString("catalog", "1C_IBLOCK_TYPE", "-");
            if ($IBLOCK_TYPE == '-') {
                $IBLOCK_TYPE = 'catalog1c77';
            }
            $SITE_LIST = array(COption::GetOptionString("catalog", "1C_SITE_LIST", "-"));
            if ($SITE_LIST[0] == '-') {
                $rsSites = CSite::GetList($by = "sort", $order = "desc");
                $SITE_LIST = array();
                while ($arSite = $rsSites->Fetch()) {
                    $SITE_LIST[] = $arSite['LID'];
                }
            }
        }
        $ib = new CIBlock;
        foreach ($arResult['IBlocks'] as $IBlock) {
            //check iblock
            $res = CIBlock::GetList(Array(), Array('XML_ID' => $IBlock['XML_ID']), true);
            if ($ar_res = $res->Fetch()) {
                $IBLOCK_ID = $ar_res['ID'];
            } else {//if iblock not exists, create them
                $arFields = array('CODE' => $IBlock['CODE'], 'XML_ID' => $IBlock['XML_ID'], 'IBLOCK_TYPE_ID' => $IBLOCK_TYPE, 'NAME' => html_entity_decode($IBlock['NAME']), 'ACTIVE' => 'Y', "SITE_ID" => $SITE_LIST);
                $IBLOCK_ID = $ib->Add($arFields);
                if (!$IBLOCK_ID) {
                    AddMessage2Log('060->Error: ' . $ib->LAST_ERROR, "htmls.1c77exchange");
                    die('060->Error: ' . $ib->LAST_ERROR);
                }
            }
            //add new elements for this iblock
            $elems[$IBLOCK_ID] = $this->UploadIBlockElements($IBlock['LIST'], $IBLOCK_ID);
        }
        //update cache
        $cache = $this->CACHE;
        if (!is_array($cache)) {
            $cache = array();
        }
        $cache['IBlocks'] = $elems;
        $handle = fopen($this->TMP_FOLDER . 'cache.txt', "w");
        fwrite($handle, serialize($cache));
        $filename = $this->TMP_FOLDER . $file;
        @unlink($filename);
        $filename = $this->TMP_FOLDER . "ib_elements.csv";
        @unlink($filename);
        //die();
        echo "Import IBlocks - success.";
    }//ImportIB()

    //return array(xml_id => id)
    function UploadIBlockElements($arElements, $IBLOCK_ID) {
        global $USER;

        $el = new CIBlockElement;
        foreach ($arElements as $xid => $name) {
            $arFilter = array('IBLOCK_ID' => $IBLOCK_ID, 'XML_ID' => $xid);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('ID', 'NAME'));
            if ($res->SelectedRowsCount() == 0) {
                //add new element
                $arLoadProductArray = Array("IBLOCK_ID" => $IBLOCK_ID, "NAME" => html_entity_decode($name), "ACTIVE" => "Y", "XML_ID" => $xid);
                if ($PRODUCT_ID = $el->Add($arLoadProductArray))
                    $elem[$xid] = $PRODUCT_ID;
                else
                    AddMessage2Log('070->Error: ' . $el->LAST_ERROR, "htmls.1c77exchange");
            } else {
                //add value to cache
                while ($ob = $res->GetNextElement()) {
                    $ar = $ob->GetFields();
                    $elem[$xid] = $ar['ID'];
                    if ($ar['NAME'] <> $name) {
                        $arLoadProductArray = Array("NAME" => html_entity_decode($name));
                        $el->Update($ar['ID'], $arLoadProductArray);
                    }
                }
            }
        }
        return $elem;
    }//UploadIBlockElements($arElements, $IBLOCK_ID)
    //import iblock elements - end

    //import sections - start
    function ImportSEC($file, $cnt) {
        global $USER, $MESS;
        $arResult = $this->ImportCatalogID();
        $arResult['SECTION'] = $this->ParseCSV($file);
        $cache = $this->CACHE;
        if (is_array($this->CACHE)) {
            $IBLOCK_ID = $this->CACHE['CATALOG_ID'];
        } else {
            $res = CIBlock::GetList(Array(), Array('XML_ID' => $arResult['CATALOG_ID']), true);
            $ar_res = $res->Fetch();
            $IBLOCK_ID = $ar_res['ID'];
        }
        $dw = COption::GetOptionString("catalog", "1C_DETAIL_WIDTH", 300);
        $dh = COption::GetOptionString("catalog", "1C_DETAIL_HEIGHT", 300);
        $pw = COption::GetOptionString("catalog", "1C_PREVIEW_WIDTH", 100);
        $ph = COption::GetOptionString("catalog", "1C_PREVIEW_HEIGHT", 100);
        //AddMessage2Log($this->KEEP_STR, "htmls.1c77exchange");
        //AddMessage2Log($cache['NEW_SECTION'], "htmls.1c77exchange");

        $bs = new CIBlockSection;
        if ($this->KEEP_STR == 'Y') {
            if (!$cache['NEW_SECTION']) {
                $sort = array('sort' => 'asc');
                $filter = array('IBLOCK_ID' => $IBLOCK_ID, 'NAME' => GetMessage('NEW_SECTION'));
                //AddMessage2Log(print_r($filter, true), "htmls.1c77exchange");
                $srch = CIBlockSection::GetList($sort, $filter);
                //AddMessage2Log($srch->SelectedRowsCount(), "htmls.1c77exchange");
                if ($srch->SelectedRowsCount() == 0) {
                    $newFields['IBLOCK_SECTION_ID'] = false;
                    $newFields['ACTIVE'] = 'N';
                    $newFields['NAME'] = GetMessage('NEW_SECTION');
                    $newFields['IBLOCK_ID'] = $IBLOCK_ID;
                    if ($newID = $bs->Add($newFields))
                        $cache['NEW_SECTION'] = $newID;
                    else {
                        AddMessage2Log($bs->LAST_ERROR, "htmls.1c77exchange");
                    }
                } else {
                    $fetch = $srch->fetch();
                    $cache['NEW_SECTION'] = $fetch['ID'];
                }
            }
            //AddMessage2Log($cache['NEW_SECTION'], "htmls.1c77exchange");
        }

        foreach ($arResult['SECTION'] as $arFields) {
            $arFields['ACTIVE'] = 'Y';
            if ($arFields['PICTURE'] == 0) {
                unset($arFields['PICTURE']);
            } else {
                $Picture = $arFields['PICTURE'];
                $aPicture = explode('.', $Picture);
                $arProps['PICTURE'] = $aPicture[0];//str_replace('.jpg', '', $Picture);
                //$arProps['PICTURE'] = str_replace('.png', '', $Picture);
                $arFields["DETAIL_PICTURE"] = false;
                $arFields["PICTURE"] = false;
                $arFileD = false;
                $arFileP = false;
                foreach ($this->IMG_TYPE as $type) {
                    $folder = $_SERVER["DOCUMENT_ROOT"] . $this->IMG_FOLDER;
                    if (strlen($this->IMG_SUBFOLDER) > 0) {
                        $folder .= $arProps[$this->IMG_SUBFOLDER] . '/';
                    }
                    if (file_exists($folder . $arProps['PICTURE'] . "." . $type)) {
                        $sourceFile = $folder . $arProps['PICTURE'] . "." . $type;
                        $destinationFile = $folder . $arProps['PICTURE'] . "r." . $type;
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($sourceFile, $destinationFile, array("width" => $dw, "height" => $dh));
                        $arFileD = CFile::MakeFileArray($destinationFile);
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($destinationFile, $destinationFileP, array("width" => $pw, "height" => $ph));
                        $arFileP = CFile::MakeFileArray($destinationFileP);
                        break;
                    }
                    if (file_exists($folder . $arProps['PICTURE'] . "." . strtoupper($type))) {
                        $sourceFile = $folder . $arProps['PICTURE'] . "." . strtoupper($type);
                        $destinationFile = $folder . $arProps['PICTURE'] . "r." . $type;
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($sourceFile, $destinationFile, array("width" => $dw, "height" => $dh));
                        $arFileD = CFile::MakeFileArray($destinationFile);
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($destinationFile, $destinationFileP, array("width" => $pw, "height" => $ph));
                        $arFileP = CFile::MakeFileArray($destinationFileP);
                        break;
                    }
                    //hack for cp1251 filename
                    $sOriginalPicture = $arProps['PICTURE'];
                    $arProps['PICTURE'] = iconv('UTF-8', 'CP1251', $sOriginalPicture);
                    if (file_exists($folder . $arProps['PICTURE'] . "." . $type)) {
                        $sourceFile = $folder . $arProps['PICTURE'] . "." . $type;
                        $destinationFile = $folder . $arProps['PICTURE'] . "r." . $type;
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($sourceFile, $destinationFile, array("width" => $dw, "height" => $dh));
                        $arFileD = CFile::MakeFileArray($destinationFile);
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($destinationFile, $destinationFileP, array("width" => $pw, "height" => $ph));
                        $arFileP = CFile::MakeFileArray($destinationFileP);
                        break;
                    }
                    if (file_exists($folder . $arProps['PICTURE'] . "." . strtoupper($type))) {
                        $sourceFile = $folder . $arProps['PICTURE'] . "." . strtoupper($type);
                        $destinationFile = $folder . $arProps['PICTURE'] . "r." . $type;
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($sourceFile, $destinationFile, array("width" => $dw, "height" => $dh));
                        $arFileD = CFile::MakeFileArray($destinationFile);
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($destinationFile, $destinationFileP, array("width" => $pw, "height" => $ph));
                        $arFileP = CFile::MakeFileArray($destinationFileP);
                        break;
                    }
                    $arProps['PICTURE'] = $sOriginalPicture;
                }
                $arFields["DETAIL_PICTURE"] = $arFileD;
                $arFields["PICTURE"] = $arFileP;

                if ($this->IMG_LOG == 'Y' && !$arFileD) {
                    //$arNoImg = $this->NO_IMGS;
                    $str = $arFields['XML_ID'] . ";" . $arFields['SORT'] . ";" . $arFields['NAME'] . "\n";
                    $handle = fopen($this->TMP_FOLDER . 'noimg.csv', "a+");
                    fwrite($handle, $str);
                    fclose($handle);
                }
            }

            /*
             if($this->KEEP_STR == 'Y'){
                 $arFields['ACTIVE'] = 'N';
             }*/

            //else{

            if (!empty($arFields['SECTION_XML_ID'])) {
                if ($cache['SECTIONS'][$arFields['XML_ID']]) $arFields['IBLOCK_SECTION_ID'] = $cache['SECTIONS'][$arFields['XML_ID']];
                else $arFields['IBLOCK_SECTION_ID'] = $this->GetSection($arFields['SECTION_XML_ID'], $IBLOCK_ID);
            } else {
                if ($this->KEEP_STR == 'Y') {
                    $arFields['IBLOCK_SECTION_ID'] = $cache['NEW_SECTION'];
                } else {
                    $arFields['IBLOCK_SECTION_ID'] = false;
                }
                //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
            }
            //}
            $arFields['CODE'] = strtolower($arFields['CODE']);
            $IsSec = $this->GetSection($arFields['XML_ID'], $IBLOCK_ID);

            $arFields['IBLOCK_ID'] = $IBLOCK_ID;
            if ($IsSec == null) {
                /*if(is_array($this->CACHE)){
                    if(!$arFields['IBLOCK_SECTION_ID']){
                        if(!$cache['NEW_SECTION']){
                            $newFields['IBLOCK_SECTION_ID'] = false;
                            $newFields['ACTIVE'] = 'N';
                            $newFields['NAME'] = $MESS['NEW_SECTION'];
                            $newFields['IBLOCK_ID'] = $IBLOCK_ID;
                            $newID = $bs->Add($newFields);
                            $cache['NEW_SECTION'] = $newID;
                        }
                        else{
                            $arFields['IBLOCK_SECTION_ID'] = $cache['NEW_SECTION'];
                        }
                    }
                }*/
                $ID = $bs->Add($arFields);
                //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
                if (!$ID)
                    AddMessage2Log('080->Error: ' . $bs->LAST_ERROR, "htmls.1c77exchange");
                else
                    $cache['SECTIONS'][$arFields['XML_ID']] = $ID;
            } else {
                if ($this->UPD_NAME != "Y") {
                    unset($arFields['NAME']);
                }
                if ($this->KEEP_STR == 'Y') {
                    //on update, probably, will be lost current hierarchy and group will be remove to "new goods"
                    unset($arFields['IBLOCK_SECTION_ID']);
                    //unset($arFields['ACTIVE']);
                }
                //unset($arFields['ACTIVE']);
                unset($arFields['SORT']);//убираем сортировку с помощью 1с
                $res = $bs->Update($IsSec, $arFields);
                if (!$res)
                    AddMessage2Log('090->Error: ' . $bs->LAST_ERROR, "htmls.1c77exchange");
                $cache['SECTIONS'][$arFields['XML_ID']] = $IsSec;
            }
            $cnt++;
        }
        //update cache
        $handle = fopen($this->TMP_FOLDER . 'cache.txt', "w");
        fwrite($handle, serialize($cache));
        $filename = $this->TMP_FOLDER . $file;
        @unlink($filename);

        echo "progress@" . $cnt;//. '@DoNext'.$filename;
    }//ImportSEC()

    //return section_id if section find, null - not find
    function GetSection($XID, $IBLOCK_ID) {
        global $USER;

        $sort = array('sort' => 'asc');
        $filter = array('IBLOCK_ID' => $IBLOCK_ID, 'XML_ID' => $XID);
        $srch = CIBlockSection::GetList($sort, $filter);
        if ($srch->selectedRowsCount() == 0) {
            return null;
        }
        $fetch = $srch->fetch();
        $parent_id = $fetch['ID'];
        return $parent_id;
    }//GetSection($XID, $IBLOCK_ID)

    //import sections - end

    function ImportCatalog($file, $cnt) {
        global $USER;
        $arResult = $this->ImportCatalogID();
        $ar = $this->ParseCSV($file);
        $arResult['CATALOG'] = $this->PrepareCatalog($ar);

        $OnAfterIBlockElementAdd = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_USER_FUNCTION_AFTER_ADD");
        $OnAfterIBlockElementUpdate = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_USER_FUNCTION_AFTER_UPDATE");
        //$arMeasure = $this->getMeasureList();
        $cache = $this->CACHE;
        if (is_array($this->CACHE)) {
            $IBLOCK_ID = $this->CACHE['CATALOG_ID'];
        } else {
            $res = CIBlock::GetList(Array(), Array('XML_ID' => $arResult['CATALOG_ID']), true);
            $ar_res = $res->Fetch();
            $IBLOCK_ID = $ar_res['ID'];
        }

        $IBFields = CIBlock::GetFields($IBLOCK_ID);
        $dw = COption::GetOptionString("catalog", "1C_DETAIL_WIDTH", 300);
        $dh = COption::GetOptionString("catalog", "1C_DETAIL_HEIGHT", 300);
        $pw = COption::GetOptionString("catalog", "1C_PREVIEW_WIDTH", 100);
        $ph = COption::GetOptionString("catalog", "1C_PREVIEW_HEIGHT", 100);
        if ($IBFields['PREVIEW_PICTURE']['DEFAULT_VALUE']['WIDTH']) {
            $pw = $IBFields['PREVIEW_PICTURE']['DEFAULT_VALUE']['WIDTH'];
        }
        if ($IBFields['PREVIEW_PICTURE']['DEFAULT_VALUE']['HEIGHT']) {
            $ph = $IBFields['PREVIEW_PICTURE']['DEFAULT_VALUE']['HEIGHT'];
        }
        if ($IBFields['DETAIL_PICTURE']['DEFAULT_VALUE']['WIDTH']) {
            $dw = $IBFields['DETAIL_PICTURE']['DEFAULT_VALUE']['WIDTH'];
        }
        if ($IBFields['DETAIL_PICTURE']['DEFAULT_VALUE']['HEIGHT']) {
            $dh = $IBFields['DETAIL_PICTURE']['DEFAULT_VALUE']['HEIGHT'];
        }

        if (!$cache['arLinkIblock']) {
            $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID));
            while ($prop_fields = $properties->GetNext()) {
                if ($prop_fields['PROPERTY_TYPE'] == 'E') {
                    $arLinkIblock[$prop_fields['CODE']] = $prop_fields['LINK_IBLOCK_ID'];
                }
            }
        } else
            $arLinkIblock = $cache['arLinkIblock'];
        $el = new CIBlockElement;
        //$cnt = 0;
        foreach ($arResult['CATALOG'] as $arCat) {
            $arFields = $arCat['arFields'];
            $arFields['ACTIVE'] = 'Y';

            //if($this->KEEP_STR == 'Y'){

            //	$arFields['IBLOCK_SECTION_ID'] = false;
            //}
            //else{
            if ($arFields['SECTION_XML_ID'])
                $arFields['IBLOCK_SECTION_ID'] = $cache['SECTIONS'][$arFields['SECTION_XML_ID']];
            else
                $arFields['IBLOCK_SECTION_ID'] = false;
            //}

            $arProps = $arCat['arProps'];
            foreach ($arProps as $k => $v) {
                //property enum
                if ($cache[$k]) {
                    $arProps[$k] = $cache[$k][$v];
                } else {
                    //property link iblock
                    if ($arLinkIblock[$k] > 0) {
                        //if set ttype iblock link or set by cache
                        $arProps[$k] = $cache['IBlocks'][$arLinkIblock[$k]][$v];
                    } else {
                        if ($arLinkIblock[$k]) {
                            //set iblock type for iblock link
                            foreach ($cache['IBlocks'] as $id => $list) {
                                if (is_array($list)) {
                                    if (array_key_exists($v, $list)) {
                                        $arProps[$k] = $list[$v];
                                        if ($arLinkIblock[$k])
                                            $arLinkIblock[$k] = $id;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $arFilter = Array("XML_ID" => $arFields['XML_ID'], "IBLOCK_ID" => $IBLOCK_ID);
            $list = CIBlockElement::GetList($sort, $arFilter, false, false, array('ID'));
            //check element by xml_id
            if ($arFields['DETAIL_TEXT']) {
                if ($this->Base64 == 'Y') {
                    $arFields['DETAIL_TEXT'] = $this->win2utf(html_entity_decode(base64_decode($arFields['DETAIL_TEXT'])));
                } else {
                    $arFields['DETAIL_TEXT'] = html_entity_decode($arFields['DETAIL_TEXT']);
                }
                $arFields['DETAIL_TEXT_TYPE'] = 'html';
            }
            if ($arFields['PREVIEW_TEXT']) {
                if ($this->Base64 == 'Y') {
                    $arFields['PREVIEW_TEXT'] = $this->win2utf(html_entity_decode(base64_decode($arFields['PREVIEW_TEXT'])));
                } else {
                    $arFields['PREVIEW_TEXT'] = html_entity_decode($arFields['PREVIEW_TEXT']);
                }
                $arFields['PREVIEW_TEXT_TYPE'] = 'html';
            }

            $arFields["DETAIL_PICTURE"] = false;
            $arFields["PREVIEW_PICTURE"] = false;
            /*
            if($measID = $arMeasure[$arFields['MEASURE']]){
              $arFields['MEASURE'] = $measID;
            }
            */
            if ($arProps['PICTURE']) {
                if ($this->IMG_MULTI == 'N') {
                    $arProps['PICTURE'] = explode(',', $arProps['PICTURE']);
                    $Picture = array_shift($arProps['PICTURE']);
                    $oPicture = $arProps['PICTURE'];
                } else {
                    $name = 1;
                    $next = true;
                    $folder = $arProps['PICTURE'];
                    $path = $_SERVER["DOCUMENT_ROOT"] . $this->IMG_FOLDER . $arProps['PICTURE'] . '/';
                    $Picture = '';
                    $oPicture = array();
                    while ($next) {
                        $next = false;
                        foreach ($this->IMG_TYPE as $type) {
                            $pname = $path . $name . "." . $type;
                            if (file_exists($pname)) {
                                if ($name == 1) {
                                    $Picture = $folder . '/' . $name;
                                } else {
                                    $oPicture[] = $folder . '/' . $name . "." . $type;
                                }
                                $next = true;
                                $name++;
                                break;
                            }
                        }
                    }
                }

                //$Picture = explode('.', $Picture);
                $aPicture = explode('.', $Picture);
                $arProps['PICTURE'] = $aPicture[0];//str_replace('.jpg', '', $Picture);
                //$arProps['PICTURE'] = str_replace('.png', '', $Picture);
                $arFields["DETAIL_PICTURE"] = false;
                $arFields["PREVIEW_PICTURE"] = false;
                $arFileD = false;
                $arFileP = false;
                foreach ($this->IMG_TYPE as $type) {
                    $folder = $_SERVER["DOCUMENT_ROOT"] . $this->IMG_FOLDER;
                    if (strlen($this->IMG_SUBFOLDER) > 0) {
                        $folder .= $arProps[$this->IMG_SUBFOLDER] . '/';
                    }
                    if (file_exists($folder . $arProps['PICTURE'] . "." . $type)) {
                        $sourceFile = $folder . $arProps['PICTURE'] . "." . $type;
                        $destinationFile = $folder . $arProps['PICTURE'] . "r." . $type;
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($sourceFile, $destinationFile, array("width" => $dw, "height" => $dh));
                        $arFileD = CFile::MakeFileArray($destinationFile);
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($destinationFile, $destinationFileP, array("width" => $pw, "height" => $ph));
                        $arFileP = CFile::MakeFileArray($destinationFileP);
                        break;
                    }
                    if (file_exists($folder . $arProps['PICTURE'] . "." . strtoupper($type))) {
                        $sourceFile = $folder . $arProps['PICTURE'] . "." . strtoupper($type);
                        $destinationFile = $folder . $arProps['PICTURE'] . "r." . $type;
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($sourceFile, $destinationFile, array("width" => $dw, "height" => $dh));
                        $arFileD = CFile::MakeFileArray($destinationFile);
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($destinationFile, $destinationFileP, array("width" => $pw, "height" => $ph));
                        $arFileP = CFile::MakeFileArray($destinationFileP);
                        break;
                    }
                    //hack for cp1251 filename
                    $sOriginalPicture = $arProps['PICTURE'];
                    $arProps['PICTURE'] = iconv('UTF-8', 'CP1251', $sOriginalPicture);
                    if (file_exists($folder . $arProps['PICTURE'] . "." . $type)) {
                        $sourceFile = $folder . $arProps['PICTURE'] . "." . $type;
                        $destinationFile = $folder . $arProps['PICTURE'] . "r." . $type;
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($sourceFile, $destinationFile, array("width" => $dw, "height" => $dh));
                        $arFileD = CFile::MakeFileArray($destinationFile);
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($destinationFile, $destinationFileP, array("width" => $pw, "height" => $ph));
                        $arFileP = CFile::MakeFileArray($destinationFileP);
                        break;
                    }
                    if (file_exists($folder . $arProps['PICTURE'] . "." . strtoupper($type))) {
                        $sourceFile = $folder . $arProps['PICTURE'] . "." . strtoupper($type);
                        $destinationFile = $folder . $arProps['PICTURE'] . "r." . $type;
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($sourceFile, $destinationFile, array("width" => $dw, "height" => $dh));
                        $arFileD = CFile::MakeFileArray($destinationFile);
                        $destinationFileP = $folder . $arProps['PICTURE'] . "p." . $type;
                        CFile::ResizeImageFile($destinationFile, $destinationFileP, array("width" => $pw, "height" => $ph));
                        $arFileP = CFile::MakeFileArray($destinationFileP);
                        break;
                    }
                    $arProps['PICTURE'] = $sOriginalPicture;
                }
                $arFields["DETAIL_PICTURE"] = $arFileD;
                $arFields["PREVIEW_PICTURE"] = $arFileP;

                if ($this->IMG_LOG == 'Y' && !$arFileD) {
                    //$arNoImg = $this->NO_IMGS;
                    $str = $arFields['XML_ID'] . ";" . $arFields['SORT'] . ";" . $arFields['NAME'] . "\n";
                    $handle = fopen($this->TMP_FOLDER . 'noimg.csv', "a+");
                    fwrite($handle, $str);
                    fclose($handle);
                }
                if (count($oPicture) > 0) {
                    $n = 0;
                    foreach ($oPicture as $pict) {
                        $arProps['MORE_PHOTO']['n' . $n] = CFile::MakeFileArray($folder . $pict);
                        $n++;
                    }
                } else {
                    $arProps['MORE_PHOTO'] = array('VALUE' => array(''));
                }
            }
            if ($arProps['SORT']) {
                $arFields['SORT'] = $arProps['SORT'];
                unset($arProps['SORT']);
            }
            if ($arProps['ACTIVE']) {
                $arFields['ACTIVE'] = $arProps['ACTIVE'];
                unset($arProps['ACTIVE']);
            }
            if ($arProps['MEASURE']) {
                unset($arProps['MEASURE']);
            }
            if (!empty($arProps['TORGOVAJAMARKA'])) {
                if (!ctype_digit($arProps['TORGOVAJAMARKA'])){
                    $torgMarkaId = (\Helpers\TorgovayaMarka::getIdByXMLId($arProps['TORGOVAJAMARKA']));
                    if ($torgMarkaId){
                        $arProps['TORGOVAJAMARKA'] = $torgMarkaId;
                    } else {
                        $arProps['TORGOVAJAMARKA'] = '';
                    }
                }
            }

            //$arFields['CODE'] = strtolower($arFields['CODE']);
            //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
            //AddMessage2Log(print_r($arProps, true), "htmls.1c77exchange");
            if ($list->selectedRowsCount() == 0) {
                //create element
                $arFields['IBLOCK_ID'] = $IBLOCK_ID;
                $arFields['PROPERTY_VALUES'] = $arProps;

                if ($ID = $el->add($arFields)) {
                    $cache['CATALOG'][$arFields['XML_ID']] = $ID;
                    if (function_exists($OnAfterIBlockElementAdd)) {
                        $OnAfterIBlockElementAdd($arFields);
                    }
                    $CCatalogProduct = new CCatalogProduct;
                    $arFieldsPurchasing = array(
                        'ID' => $ID,
                        'PURCHASING_PRICE' => trim($arProps['PURCHASING_PRICE']) > 0?trim($arProps['PURCHASING_PRICE']):'',
                        'PURCHASING_CURRENCY' => "RUB"
                    );
                    $CCatalogProduct->add($arFieldsPurchasing);
                } else {
                    AddMessage2Log('100->Error: ' . $el->LAST_ERROR . ' (ADD => ' . $arFields['SORT'] . ')', "htmls.1c77exchange");
                }
            } else {
                if ($this->UPD_NAME != "Y") {
                    unset($arFields['NAME']);
                }
                if ($this->KEEP_STR == 'Y') {
                    unset($arFields['IBLOCK_SECTION_ID']);
                }
                //update element
                while ($ob = $list->GetNextElement()) {
                    $ar = $ob->GetFields();
                    $ID = $ar['ID'];
                    if (!$el->update($ID, $arFields))
                        AddMessage2Log('110->Error: ' . $el->LAST_ERROR . ' (UPDATE => ' . $arFields['SORT'] . ')', "htmls.1c77exchange");
//AddMessage2Log('(UPDATE => ' . print_r($arProps, true) . ')', "htmls.1c77exchange");

                    $CCatalogProduct = new CCatalogProduct;
                    $arFieldsPurchasing = array(
                        'ID' => $ID,
                        'PURCHASING_PRICE' => trim($arProps['PURCHASING_PRICE']) > 0?trim($arProps['PURCHASING_PRICE']):'',
                        'PURCHASING_CURRENCY' => "RUB"
                    );
                    $CCatalogProduct->add($arFieldsPurchasing);
                    CIBlockElement::SetPropertyValuesEx($ID, false, $arProps);
                    if (function_exists($OnAfterIBlockElementUpdate)) {
                        $arFields['IBLOCK_ID'] = $IBLOCK_ID;
                        $arFields['PROPERTY_VALUES'] = $arProps;
                        $OnAfterIBlockElementUpdate($arFields);
                    }
                    $cache['CATALOG'][$arFields['XML_ID']] = $ID;

                    if ($this->USE_STORE == 'Y') {
                        $oRes = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $ID));
                        while ($aOffer = $oRes->Fetch()) {
                            $cache['StoreOffer'][$ID . '#' . $aOffer['STORE_ID']] = $aOffer['ID'];
                        }
                    }
                }
            }

            if ($this->IMG_DEL == 'Y') {
                @unlink($sourceFile);
            }
            @unlink($destinationFile);
            @unlink($destinationFileP);

            $cnt++;
        }
        //update cache
        $cache['arLinkIblock'] = $arLinkIblock;
        $handle = fopen($this->TMP_FOLDER . 'cache.txt', "w");
        fwrite($handle, serialize($cache));
        fclose($handle);
        $filename = $this->TMP_FOLDER . $file;
        @unlink($filename);
        /*if($this->IMG_LOG == 'Y'){
            $handle = fopen($this->TMP_FOLDER . 'noimg.csv', "w");
            fwrite($handle, serialize($this->NO_IMGS));
        }*/

        echo "progress@" . $cnt;//. '@DoNext'.$filename;
    }//ImportCatalog($file)

    function ImportOffers2Iblock($arOffers) {

        global $USER;
        //AddMessage2Log(print_r($arOffers, true), "htmls.1c77exchange");
        $cache = $this->CACHE;
        //AddMessage2Log(print_r($cache, true), "htmls.1c77exchange");
        if (is_array($this->CACHE)) {
            $IBLOCK_ID = $this->CACHE['SKU_ID'];
            $ownerID = $this->CACHE['CATALOG_ID'];
        } else {
            $res = CIBlock::GetList(Array(), Array('XML_ID' => $arResult['CATALOG_ID'] . '#'), true);
            $ar_res = $res->Fetch();
            $IBLOCK_ID = $ar_res['ID'];

            $res = CIBlock::GetList(Array(), Array('XML_ID' => $arResult['CATALOG_ID']), true);
            $ar_res = $res->Fetch();
            $ownerID = $ar_res['ID'];
        }

        $IBFields = CIBlock::GetFields($IBLOCK_ID);

        $arLinkIblock = $cache['arLinkIblock'];
        //if(!$cache['arLinkIblock']){
        $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $IBLOCK_ID));
        while ($prop_fields = $properties->GetNext()) {
            if ($prop_fields['PROPERTY_TYPE'] == 'E') {
                $arLinkIblock[$prop_fields['CODE']] = $prop_fields['LINK_IBLOCK_ID'];
            }
        }
        //}
        //else

        $el = new CIBlockElement;
        //$cnt = 0;
        foreach ($arOffers as $arElement) {
            $xml_id = $arElement['CATALOG']['CML2_LINK'];
            //get owner
            $arFilter = Array("XML_ID" => $xml_id, "IBLOCK_ID" => $ownerID);
            $list = CIBlockElement::GetList($sort, $arFilter, false, false, array('ID', 'NAME'));
            //AddMessage2Log(print_r($arFilter, true), "htmls.1c77exchange");
            //AddMessage2Log($list->selectedRowsCount(), "htmls.1c77exchange");
            if ($list->selectedRowsCount() == 0) {

                continue;
            } else {
                while ($ob = $list->GetNextElement()) {
                    $ar = $ob->GetFields();
                    $oID = $ar['ID'];
                    $oName = $ar['NAME'];
                }
            }

            $arFields = array();//$arCat['arFields'];
            $arFields['NAME'] = $arElement['CATALOG']['NAME'];//$oName;
            $arFields['ACTIVE'] = 'Y';
            $arFields['XML_ID'] = $arElement['PRODUCT']['XML_ID'];
            $arFields['IBLOCK_SECTION_ID'] = false;

            $arProps = $arElement['CATALOG'];
            foreach ($arProps as $k => $v) {
                //property enum
                if ($cache[$k]) {
                    $arProps[$k] = $cache[$k][$v];
                } else {
                    //property link iblock
                    if ($arLinkIblock[$k] > 0) {
                        //if set ttype iblock link or set by cache
                        $arProps[$k] = $cache['IBlocks'][$arLinkIblock[$k]][$v];
                    } else {
                        if ($arLinkIblock[$k]) {
                            //set iblock type for iblock link
                            foreach ($cache['IBlocks'] as $id => $list) {
                                if (is_array($list)) {
                                    if (array_key_exists($v, $list)) {
                                        $arProps[$k] = $list[$v];
                                        if ($arLinkIblock[$k])
                                            $arLinkIblock[$k] = $id;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $arProps['CML2_LINK'] = $oID;

            $arFilter = Array("XML_ID" => $arFields['XML_ID'], "IBLOCK_ID" => $IBLOCK_ID);
            $list = CIBlockElement::GetList($sort, $arFilter, false, false, array('ID'));
            //check element by xml_id

            if ($arProps['SORT']) {
                $arFields['SORT'] = $arProps['SORT'];
                unset($arProps['SORT']);
            }
            if ($arProps['ACTIVE']) {
                $arFields['ACTIVE'] = $arProps['ACTIVE'];
                unset($arProps['ACTIVE']);
            }
            if ($list->selectedRowsCount() == 0) {
                //create element
                $arFields['IBLOCK_ID'] = $IBLOCK_ID;
                $arFields['PROPERTY_VALUES'] = $arProps;
                if ($ID = $el->add($arFields)) {
                    $cache['CATALOG'][$arFields['XML_ID']] = $ID;
                } else {
                    AddMessage2Log('100->Error: ' . $el->LAST_ERROR . ' (ADD => ' . $arFields['SORT'] . ')', "htmls.1c77exchange");
                }
            } else {
                if ($this->UPD_NAME != "Y") {
                    unset($arFields['NAME']);
                }
                if ($this->KEEP_STR == 'Y') {
                    unset($arFields['IBLOCK_SECTION_ID']);
                }
                //update element
                while ($ob = $list->GetNextElement()) {
                    $ar = $ob->GetFields();
                    $ID = $ar['ID'];
                    if (!$el->update($ID, $arFields))
                        AddMessage2Log('110->Error: ' . $el->LAST_ERROR . ' (UPDATE => ' . $arFields['SORT'] . ')', "htmls.1c77exchange");
                    CIBlockElement::SetPropertyValuesEx($ID, false, $arProps);

                    $cache['CATALOG'][$arFields['XML_ID']] = $ID;

                    if ($this->USE_STORE == 'Y') {
                        $oRes = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $ID));
                        while ($aOffer = $oRes->Fetch()) {
                            $cache['StoreOffer'][$ID . '#' . $aOffer['STORE_ID']] = $aOffer['ID'];
                        }
                    }
                }
            }

            $cnt++;
        }
        //update cache
        $cache['arLinkIblock'] = $arLinkIblock;
        $handle = fopen($this->TMP_FOLDER . 'cache.txt', "w");
        fwrite($handle, serialize($cache));
        fclose($handle);
    }

    function ImportOffers($file, $cnt) {
        global $USER, $APPLICATION;
        if (empty($cnt)) $cnt = 0;
        /*
        if($cnt == 0){
          foreach(GetModuleEvents("catalog", "OnSuccessCatalogImport1C", true) as $arEvent)
                ExecuteModuleEventEx($arEvent);
        }
        */
        $arMeasure = $this->getMeasureList();
        $arResult = $this->ImportCatalogID();
        $arOffID = $this->ParseCSV('offers_id.csv');

        foreach ($arOffID as $arData) {
            $pr[$arData['OFFER_ID']] = $arData['NAME'];
            //if($this->BASE_EDITION) break;//get first price
        }
        $arResult['CATALOG_GROUP'] = $pr;
        $ar = $this->ParseCSV($file);
        $arResult['OFFERS'] = $this->PrepareOffers($ar, $pr);
        //AddMessage2Log($this->USE_SKU, "htmls.1c77exchange");
        //AddMessage2Log(print_r($arResult['OFFERS'], true), "htmls.1c77exchange");
        if ($this->USE_SKU == 'Y') {
            $this->ImportOffers2Iblock($arResult['OFFERS']);
        }
        $cache = $this->CACHE;
        if (is_array($this->CACHE) && isset($this->CACHE['SKU_ID']) && (intval($this->CACHE['SKU_ID']) > 0)) {
            $IBLOCK_ID = $this->CACHE['SKU_ID'];
        } else {
            $res = CIBlock::GetList(Array(), Array('XML_ID' => $arResult['CATALOG_ID'] . '#'), true);
            $ar_res = $res->Fetch();
            $IBLOCK_ID = $ar_res['ID'];
        }

        if (!$cache['arCatalogGroup']) {
            foreach ($arResult['CATALOG_GROUP'] as $xid => $name) {
                //try to get by xml_id
                $dbPriceType = CCatalogGroup::GetList(array("SORT" => "ASC"), array("XML_ID" => $xid));
                $arPriceType = $dbPriceType->Fetch();
                if ($arPriceType) {
                    $arCatalogGroup[$xid] = $arPriceType["ID"];
                } else {
                    //try to get be name
                    $dbPriceType = CCatalogGroup::GetList(array("SORT" => "ASC"), array("NAME" => $name));
                    $arPriceType = $dbPriceType->Fetch();
                    if ($arPriceType) {
                        $arCatalogGroup[$xid] = $arPriceType["ID"];
                        $arFields = array('XML_ID' => $xid);
                        $CatGroup = new CCatalogGroup;
                        if (!$CatGroup->Update($arPriceType["ID"], $arFields)) {
                            $arCatalogGroup[$xid] = $ID;
                        }
                    } else {
                        $arFields = array("NAME" => $name, 'XML_ID' => $xid,
                            "SORT" => 100, "USER_GROUP" => array(2), "USER_GROUP_BUY" => array(2),                                  // &#242;&#238;&#235;&#252;&#234;&#238; &#247;&#235;&#229;&#237;&#251; &#227;&#240;&#243;&#239;&#239;&#251; 2
                            "USER_LANG" => array(
                                "ru" => $name,
                                "en" => $name));
                        $CatGroup = new CCatalogGroup;
                        if ($ID = $CatGroup->Add($arFields)) {
                            $arCatalogGroup[$xid] = $ID;
                        } else {
                            AddMessage2Log('120->Error: ' . $CatGroup->LAST_ERROR . ' (ADD => ' . print_r($arFields, true) . ')', "htmls.1c77exchange");
                        }
                    }
                }
            }
            $cache['arCatalogGroup'] = $arCatalogGroup;
        } else
            $arCatalogGroup = $cache['arCatalogGroup'];
        //AddMessage2Log(print_r($arResult['OFFERS'], true), "htmls.1c77exchange");
        $prod = new CCatalogProduct;
        foreach ($arResult['OFFERS'] as $ar) {
            $arPropd = $ar['PRODUCT'];
            //if update only offers
            if (!$cache['CATALOG'][$arPropd['XML_ID']]) {
                $arFilter = Array("XML_ID" => $arPropd['XML_ID'], "IBLOCK_ID" => $IBLOCK_ID);
                $list = CIBlockElement::GetList($sort, $arFilter, false, false, array('ID'));
                while ($ob = $list->GetNextElement()) {
                    $ar1 = $ob->GetFields();
                    if (!empty($arPropd['XML_ID'])) {
                        $cache['CATALOG'][$arPropd['XML_ID']] = $ar1['ID'];
                    }
                }
            }
            //AddMessage2Log(print_r($arPropd, true), "htmls.1c77exchange");
            $arFields = array('QUANTITY' => $arPropd['QUANTITY'],
                'QUANTITY_TRACE' => $arPropd['QUANTITY_TRACE'],
//                'CAN_BUY_ZERO' => $arPropd['CAN_BUY_ZERO'],
                'CAN_BUY_ZERO' => $this->CAN_BUY_ZERO,
                'NEGATIVE_AMOUNT_TRACE' => $arPropd['NEGATIVE_AMOUNT_TRACE'],
                "VAT_INCLUDED" => "Y",
                "WEIGHT" => $arPropd['WEIGHT'],
                "LENGTH" => $arPropd['LENGTH'],
                "WIDTH" => $arPropd['WIDTH'],
                "HEIGHT" => $arPropd['HEIGHT']
            );
            if ($measID = $arMeasure[$arPropd['MEASURE']]) {
                $arFields['MEASURE'] = $measID;
            }

            if ($arPropd['VAT'] == 20) $arFields['VAT_ID'] = 2;
            elseif ($arPropd['VAT'] == 10) $arFields['VAT_ID'] = 3;
            else $arFields['VAT_ID'] = 1;//2

            $arFields['ID'] = $cache['CATALOG'][$arPropd['XML_ID']];
            if (intval($arFields['ID']) == 0) continue;
            //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
//            if (!$arFields['CAN_BUY_ZERO'])
            $arFields['CAN_BUY_ZERO'] = $this->CAN_BUY_ZERO;
            if (!$arFields['NEGATIVE_AMOUNT_TRACE'])
                $arFields['NEGATIVE_AMOUNT_TRACE'] = $this->NEGATIVE_AMOUNT_TRACE;
            if ($prod->add($arFields)) {
            } else {
                AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
                AddMessage2Log($prod->LAST_ERROR, "htmls.1c77exchange");
            }

            $arFields = Array("PRODUCT_ID" => $cache['CATALOG'][$arPropd['XML_ID']], "CURRENCY" => "RUB");
            $pr = new CPrice;
            $arPr = $ar['OFFERS'];
            $arCur = $ar['CATALOG'];

            if (isset($arCur['MEASURE_RATIO'])){
                $ratio = 1;
                if ($arCur['MEASURE_RATIO'] > 0){
                    $ratio = round((float)$arCur['MEASURE_RATIO'],6);
                }
                $this->setMeasureRatioForProduct($arFields['PRODUCT_ID'],$ratio);
            }

            foreach ($arPr as $arOffer) {
                $arFields['CATALOG_GROUP_ID'] = $arCatalogGroup[$arOffer['ID']];
                $arFields['PRICE'] = $arOffer['VALUE'];
                $cur = $arCur['CUR' . $arOffer['ID']];
                if ($ar_usd_cur = CCurrency::GetByID($cur)) $arFields['CURRENCY'] = $cur;
                //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange");
                if ($this->BASE_EDITION) {
                    CPrice::SetBasePrice($cache['CATALOG'][$arPropd['XML_ID']],
                        $arOffer['VALUE'], $cur);
                } else {
                    $cnt++;
                    $res = CPrice::GetList(array(), array("PRODUCT_ID" => $cache['CATALOG'][$arPropd['XML_ID']], "CATALOG_GROUP_ID" => $arCatalogGroup[$arOffer['ID']]));
                    if ($arr = $res->Fetch()) {
                        //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange.update");
                        if ($id = CPrice::Update($arr["ID"], $arFields)) {
                            $id = true;
                        } else {
                            AddMessage2Log('130->Error: ' . $APPLICATION->GetException() . ' (UPDATE => ' . print_r($arFields, true) . ')', "htmls.1c77exchange");
                        }
                    } else {
                        //AddMessage2Log(print_r($arFields, true), "htmls.1c77exchange.add");
                        if ($id = CPrice::Add($arFields)) {
                            $id = true;
                        } else {
                            AddMessage2Log('140->Error: ' . $APPLICATION->GetException() . ' (ADD => ' . print_r($arFields, true) . ')', "htmls.1c77exchange");
                        }
                    }
                }
            }
        }

        //update cache
        $handle = fopen($this->TMP_FOLDER . 'cache.txt', "w");
        fwrite($handle, serialize($cache));
        fclose($handle);
        $filename = $this->TMP_FOLDER . $file;
        @unlink($filename);

        echo "progress@" . $cnt;//. '@DoNext'.$filename;
    }//ImportOffers($file)

    function ImportStoreOffers($file, $cnt) {
        if ($this->USE_STORE == 'Y') {
            if (CModule::IncludeModuleEx("htmls.1c77plus") < 3) {
                if (IsModuleInstalled("htmls.1c77plus")) {
                    $arStoreOffers = $this->ParseCSV($file);
                    $V7Plus = new CV7Plus();
                    //$cache['STORE'] =
                    $cache = $this->CACHE;
                    $res = $V7Plus->StoreOffers($arStoreOffers, $cnt, $cache);
                    $cnt = $res['cnt'];
                    $cache = $res['cache'];
                    //update cache
                    $handle = fopen($this->TMP_FOLDER . 'cache.txt', "w");
                    fwrite($handle, serialize($cache));
                    fclose($handle);
                    echo "progress@" . $cnt;
                }
            }
        } else {
            echo "progress@0";
        }
    }

    function fin($cnt) {
        global $USER;
        $cache = $this->CACHE;
        if ($this->USE_STORE == 'Y') {
            //$cache = $this->CACHE;
            foreach ($cache['StoreOffer'] as $id) {
                CCatalogStoreProduct::Delete($id);
            }
        }
        $filename = $this->TMP_FOLDER . 'offers_id.csv';
        @unlink($filename);
        $filename = $this->TMP_FOLDER . 'md_catalog.csv';
        //@unlink($filename);

        if (file_exists($this->TMP_FOLDER . 'cache.txt')) {
            @unlink($this->TMP_FOLDER . 'cache.txt');
        }
        @unlink($filename);

        foreach (GetModuleEvents("catalog", "OnSuccessCatalogImport1C", true) as $arEvent)
            ExecuteModuleEventEx($arEvent);

        echo "Delete - success.";

        $USER->Logout();
    }

    function DeleteElemSec($cnt) {
        global $USER;
        $cache = $this->CACHE;
        if ($this->USE_STORE == 'Y') {
            //$cache = $this->CACHE;
            foreach ($cache['StoreOffer'] as $id) {
                CCatalogStoreProduct::Delete($id);
            }
        }
        $ELEMENT_ACTION = COption::GetOptionString("catalog", "1C_ELEMENT_ACTION", "D");
        $SECTION_ACTION = COption::GetOptionString("catalog", "1C_SECTION_ACTION", "D");
        //$cache = $this->CACHE;
        $complete = false;
        //$cnt = 0;
        $IBLOCK_ID = $cache['CATALOG_ID'];
        if (intval($IBLOCK_ID) > 0) {
            if ($ELEMENT_ACTION != 'N') {
                $el = new CIBlockElement;
                $arFilter = Array("!ID" => $cache['CATALOG'], "IBLOCK_ID" => $IBLOCK_ID);
                if ($ELEMENT_ACTION == 'A') {
                    $arFilter['ACTIVE'] = 'Y';
                }
                $list = CIBlockElement::GetList(array('ID' => 'ASC'), $arFilter, false, array('nTopCount' => 100), array('ID'));
                if ($list->selectedRowsCount() > 0) {
                    while ($ob = $list->GetNextElement()) {
                        $arFields = $ob->GetFields();
                        if ($ELEMENT_ACTION == 'A') {
                            $upFields = array("ACTIVE" => 'N');
                            $el->update($arFields['ID'], $upFields);
                        } else {
                            $el->Delete($arFields['ID']);
                        }
                        $cache['CATALOG'][] = $arFields['ID'];
                        $cnt++;
                    }
                } else {
                    $complete = true;
                }
            } else {
                $complete = true;
            }

            if ($SECTION_ACTION != 'N') {
                $arFilter = array('IBLOCK_ID' => $IBLOCK_ID, '!ID' => $cache['SECTIONS']);
                $list = CIBlockSection::GetList(array(), $arFilter);
                if ($list->selectedRowsCount() > 0) {
                    $bs = new CIBlockSection;
                    while ($ar_result = $list->GetNext()) {
                        if ($SECTION_ACTION == 'A') {
                            $arFields = Array("ACTIVE" => 'N');
                            $res = $bs->Update($ar_result['ID'], $arFields);
                        } else
                            $bs->Delete($ar_result['ID']);
                    }
                }
            }
        } else {
            $complete = true;
        }
        $filename = $this->TMP_FOLDER . 'offers_id.csv';
        @unlink($filename);
        $filename = $this->TMP_FOLDER . 'md_catalog.csv';
        //@unlink($filename);

        if ($complete) {
            if (file_exists($this->TMP_FOLDER . 'cache.txt')) {
                @unlink($this->TMP_FOLDER . 'cache.txt');
            }
            @unlink($filename);
            echo "Delete@success.";

            foreach (GetModuleEvents("catalog", "OnSuccessCatalogImport1C", true) as $arEvent)
                ExecuteModuleEventEx($arEvent);

            $USER->Logout();
        } else {
            //update cache
            $handle = fopen($this->TMP_FOLDER . 'cache.txt', "w");
            fwrite($handle, serialize($cache));
            fclose($handle);
            echo "Delete@progress@" . $cnt;
        }
    }

    //EXPORT ORDERS
    function GetOrders() {
        global $USER, $DB;
        $dt = COption::GetOptionString("1Cv77Exchange", "last_export_time_committed");

        $shipmentXmlId = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_SHIPMENT_XML_ID", '');

        //AddMessage2Log($this->FINAL_ORDERS);
        if (intval($dt) == 0) $dt = mktime() - 160 * 24 * 60 * 60;
        $start = \Bitrix\Main\Type\DateTime::createFromTimestamp($dt);
        $fDate = COption::GetOptionString('htmls.1c77exchange', 'V77EXCHANGE_GET_ORDERS_BY', 'DATE_INSERT');

        $sort = array('DATE_INSERT' => 'ASC');
        $filter = array('>=' . $fDate => $start, 'UPDATED_1C' => 'N');
        if ($this->FINAL_ORDERS != '' && $this->FINAL_ORDERS != 0) {
            $s = $this->FINAL_ORDERS;
            $fStatus = array($s);
            $dbOrderStatus = CSaleStatus::GetList($by = "sort", $order = "asc");
            $getIt = false;
            while ($arStatus = $dbOrderStatus->Fetch()) {
                if ($arStatus["ID"] == $s) {
                    $getIt = true;
                    continue;
                }
                if ($getIt) {
                    $fStatus[] = $arStatus["ID"];
                }
            }
            $filter['@STATUS_ID'] = $fStatus;
        }

        //своя логика выгрузки информации о заказе для 1с
        $orderIds = \ExternalApi\Exchange1c\Exchange1c::getOrderIdsNeedExchange();
//        $orderIds = [811]; //фильтр для выгрузки конкретных заказов или для тестов

        if (!$orderIds) {
            return [];
        }
        $filter = ['ID' => $orderIds];
        /////////////////////////////////////////////////

        static::setEmptyGeoInfoBeforeExport($sort,$filter);
        $orders = CSaleOrder::GetList($sort, $filter);
        COption::SetOptionString("1Cv77Exchange", "last_export_time_committed", time());
        if (!$orders) return array();
        if ($this->USE_STORE == 'Y') {
            CModule::IncludeModule("catalog");
        }
        $arOrders = array();
        while ($fe = $orders->fetch()) {
            //AddMessage2Log(print_r($fe, true));
            $ctype = ($fe['PERSON_TYPE_ID'] == 1) ? 'F' : 'U';

            $u = $USER->GetByID($fe['USER_ID'])->fetch();
            $name = trim($u['NAME'] . ' ' . $u['LAST_NAME']);
            if (empty($name)) $name = $u['LOGIN'];
            $arUser = array('PERSON_TYPE' => $ctype, 'USER_ID' => $fe['USER_ID'], 'USER_NAME' => $this->utf2win($name));

            $sort = array('sort' => 'asc');
            $filter = array('ORDER_ID' => $fe['ID']);
            $prop = CSaleOrderPropsValue::GetList($sort, $filter);
            $arOrderProp = array();
            $DATE_DELIVERY = '';
            $TIME_DELIVERY = '';
            $TIME_DELIVERY_TMS = '';
            $PREPARE_CHANGE_FROM_AMOUNT = '';
            $GEO_INFO = '';
            while ($p = $prop->fetch()) {
                if ($p['CODE'] == 'DATE_DELIVERY' || $p['CODE'] == 'DELIVERY_DATE') {
                    $DATE_DELIVERY = $this->utf2win(htmlspecialchars($p['VALUE']));
                } elseif ($p['CODE'] == 'DELIVERY_TIME' ) {
                    $TIME_DELIVERY_TMS = $this->utf2win(htmlspecialchars(\Custom\DeliveryTime\DeliveryDateOptions::slotToTMSFormat($p['VALUE'])));
                    $TIME_DELIVERY = $this->utf2win(htmlspecialchars($p['VALUE']));
                } elseif ($p['CODE'] == 'PREPARE_CHANGE_FROM_AMOUNT') {
                    $PREPARE_CHANGE_FROM_AMOUNT = $this->utf2win(htmlspecialchars($p['VALUE']));
                } elseif ($p['CODE'] == 'DADATA_GIS') {
                    $GEO_INFO = $this->utf2win(htmlspecialchars($p['VALUE']));
                } else {
                    $arOrderProp[] = array('NAME' => $p['CODE'], 'VALUE' => $this->utf2win(htmlspecialchars($p['VALUE'])));
                }
            }
            $PRICE_DELIVERY = $fe['PRICE_DELIVERY'];
            if (!is_numeric($fe['DELIVERY_ID'])) {
                $rs = CSaleDeliveryHandler::GetBySID($fe['DELIVERY_ID'], $fe['LID']);
            } else {
                $rs = \Bitrix\Sale\Delivery\Services\Table::getById($fe['DELIVERY_ID']);
            }
            $arDeliv = $rs->fetch();

            $arPayment = array('ID' => 0, 'NAME' => '');
            if (is_numeric($fe['PAY_SYSTEM_ID'])) {
                $arPayment = CSalePaySystem::GetByID($fe['PAY_SYSTEM_ID']);
            }

            $sort = array('sort' => 'asc');
            $filter = array('ORDER_ID' => $fe['ID']);
//			$fields = array('PRODUCT_ID', 'PRICE', 'QUANTITY', 'PRODUCT_XML_ID', 'NAME');
            $fields = array('*');
            $rows = CSaleBasket::GetList($sort, $filter, false, false, $fields);
            $arBasketID = array();
            $arRows = array();
            while ($row = $rows->fetch()) {
                $arRows[$row['ID']] = array(
                    'PRODUCT_XML_ID' => $this->utf2win($row['PRODUCT_XML_ID']),
                    'VAT_RATE' => $row['VAT_RATE'],
                    'VAT_SUM' => '',
                    'UNIT' => '',
                    'QUANTITY' => $row['QUANTITY'],
                    'PRICE' => $row['PRICE'],
                    'PRODUCT_NAME' => $this->utf2win(htmlspecialchars($row['NAME']))
                );
                $arBasketID[] = $row['ID'];
            }
            $rs = CSaleBasket::GetPropsList($sort, $filter + array('BASKET_ID' => $arBasketID));
            while ($ar = $rs->Fetch()) {
                if ($ar['CODE'] == 'UNIT_ID') {
                    $arRows[$ar['BASKET_ID']]['UNIT'] = $ar['VALUE'];
                }
            }
            $arDelivery = array();
            $arDelivery['GEO_INFO'] = $GEO_INFO;
            $arDelivery['DATE'] = $DATE_DELIVERY;
            $arDelivery['TIME'] = $TIME_DELIVERY;
            $arDelivery['TIME_TMS'] = $TIME_DELIVERY_TMS;
            $arDelivery['TYPE'] = $this->utf2win($arDeliv['XML_ID']);
            $arDelivery['PRICE'] = $PRICE_DELIVERY;
            $arDelivery['PREPARE_CHANGE_FROM_AMOUNT'] = $PREPARE_CHANGE_FROM_AMOUNT;

            $sid = 0;
            if ($this->USE_STORE == 'Y') {
                $rs = CCatalogStore::GetList(array(), array('ID' => $fe['STORE_ID']));
                while ($rowS = $rs->fetch()) {
                    $sid = $rowS['XML_ID'];
                }
            }
            $arOrders[] = array('STATUS_ID' => $fe['STATUS_ID'],
                'NUMBER' => $fe['ACCOUNT_NUMBER'],
                'ID' => $fe['ID'],
                'XML_ID' => $fe['ID_1C'],
                'DATE' => $fe['DATE_INSERT'],
                'DELIVERY' => $arDelivery,
                'PAYMENT' => $arPayment,
                'USER' => $arUser,
                'PROPERTIES' => $arOrderProp,
                'ROWS' => $arRows,
                'COMMENT' => $this->utf2win(htmlspecialchars($fe['COMMENTS'])),
                'STORE_ID' => $sid);
        }
        //COption::SetOptionString("1Cv77Exchange", "last_export_time_committed", MakeTimeStamp(date("d.m.Y H:i:s"), "DD.MM.YYYY HH:MI:SS"));

        foreach ($orderIds as $orderId){
            \ExternalApi\Exchange1c\Exchange1c::setOrderExchangeStatusFinished($orderId);
        }

        return $arOrders;
    }//GetOrders()

    static function setEmptyGeoInfoBeforeExport($sort, $filter)
    {
        $orders = CSaleOrder::GetList($sort, $filter);
        if ($orders) {
            while ($order = $orders->fetch()) {
                $sort = array('sort' => 'asc');
                $filter = array('ORDER_ID' => $order['ID']);
                $prop = CSaleOrderPropsValue::GetList($sort, $filter);
                $dadataGis = '';
                $dadataGisProp = false;
                $geoCoords = false;
                while ($p = $prop->fetch()) {
                    if ($p['CODE'] == 'DADATA_GIS') {
                        $dadataGisProp = $p;
                        if (!empty(trim($p['VALUE']))) {
                            $dadataGis = trim($p['VALUE']);
                        }
                    } elseif ($p['CODE'] == 'GEO_COORDS') {
                        $geoCoords = trim($p['VALUE']);
                    }
                }
                if (!$dadataGis && $dadataGisProp && $geoCoords) {

                    [$lat, $lon] = explode(',', $geoCoords);

                    $yandexGeoCode = \ExternalApi\YandexMaps\YandexMapsApi::geocodeByCoords($lat, $lon);
                    if ($yandexGeoCode) {
                        $addressParts = $yandexGeoCode['GeoObjectCollection']['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData'];
                        $address = $addressParts['text'];
                        $house = '';
                        if (isset($addressParts['Address']['Components'])) {
                            foreach ($addressParts['Address']['Components'] as $addressComponents) {
                                switch ($addressComponents['kind']) {
                                    case 'house':
                                        $house = $addressComponents['name'];
                                        break;
                                    case 'entrance':
                                        if (preg_match('/подъезд/',$addressComponents['name'])){
                                            $address = str_replace($addressComponents['name'],'',$address);
                                            $address = trim($address,"\t\n\r\0\x0B, ");
                                        }
                                        break;
                                    default:

                                        break;
                                }
                            }
                        }

                        $dadataSuggestedAddress = \ExternalApi\Dadata\Dadata::getFirstMainSuggestAddresses($address,false);
                        if ($dadataSuggestedAddress && $dadataSuggestedAddress['data']['kladr_id']) {
                            $newGeoParams = [
                                'ORDER_ID' => $order['ID'],
                                'ORDER_PROPS_ID' => $dadataGisProp['ORDER_PROPS_ID'],
                                'VALUE' => $dadataSuggestedAddress['data']['kladr_id'] . ':' . $house
                            ];
                            CSaleOrderPropsValue::Update($dadataGisProp['ID'], $newGeoParams);
                        }
                    }
                }
            }
        }
    }

    function Export() {
        global $USER;
        $headers = $this->emu_getallheaders();
        $res = $this->Check($headers);
        $r = $res[0];
        //mail('dev@htmls.ru', 'Export', print_r($res, true));
        if (!$r) die('Access Denied! - ' . $res[1]);

        $arOrders = $this->GetOrders();
        if (count($arOrders) > 0) {
            $xml = $this->GetXml($arOrders);
            $time = time();
            $hash = md5($time);
            $filename = 'orders_' . $time . '_' . $hash . '.xml';
            $path = $this->TMP_FOLDER . date("d.m.Y");
            if (!file_exists($path)) {
                mkdir($path);
            }
            $filename = $path . '/' . $filename;
            file_put_contents($filename, $xml);
            $altPath = str_replace($_SERVER['DOCUMENT_ROOT'], '', $filename);
            $url = self::getCurrentSiteURL() . $altPath;
            echo "success@" . $url;
        } else echo "success@No Orders";
        $USER->Logout();
    }//Export()

    public static function getCurrentSiteURL() {

        if (isset($_SERVER['HTTPS'])) {
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        } else {
            $protocol = 'http';
        }
        return $protocol . "://" . $_SERVER['HTTP_HOST'];

    }

    function SetStatus() {
        global $USER, $APPLICATION;
        $headers = $this->emu_getallheaders();
        $res = $this->Check($headers);
        $r = $res[0];
        if(!$r) die('Access Denied! - ' . $res[1]);

        if (!CSaleOrder::StatusOrder($headers['Orderid'], $headers['Status'])) {
            try {
                $order = Bitrix\Sale\Order::load($headers['Orderid']);
                $order->setField('STATUS_ID',$headers['Status']);
                $order->save();
            } catch (\Exception $exception) {
            }
        }

//        if (!CSaleOrder::StatusOrder($headers['Orderid'], $headers['Status'])) {
//            echo ($APPLICATION->LAST_ERROR && $APPLICATION->LAST_ERROR->GetID() === 'ALREADY_FLAG') ? 'unchanged' : 'failure';
//        } else {
            echo 'success';
//        }
        $USER->Logout();
    }//SetStatus()

    function _p($ar) {
        echo "<pre>";
        print_r($ar);
        echo "</pre>";
    }


    public function ImportOrders() {
        global $USER;
        $headers = $this->emu_getallheaders();
        $res = $this->Check($headers);
        $r = $res[0];
        if (!$r) {
            die('Access Denied! - ' . $res[1]);
        }
        $IBLOCK_ID = 114; // todo: get from import by ExternalID
        $filename = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/tmp/htmls.1c77exchange/" . $_REQUEST['filename'];
        $xml = simplexml_load_file($filename);
        if ($xml) {
            $arXMLID = array();
            $orderNum = 0;
            $orderProceed = 0;
            foreach ($xml->ORDER as $orderXML) {
                $orderNum++;
                $orderID = (string)$orderXML->ID;
                //todo: make logging on skipped orders
                if ($orderID) {
                    $order = \Bitrix\Sale\Order::load($orderID);
                    if ($order) { //this order exist
                        // FIELDS PROCEED
                        $order->setField('ID_1C', (string)$orderXML->XML_ID);
                        // PROPS PROCEED
                        $arPropsToSet = array();
                        if (!empty($orderXML->DELIVERY->DATE)) {
                            $arPropsToSet['DATE_DELIVERY'] = (string)$orderXML->DELIVERY->DATE;
                        }
                        if (!empty($arPropsToSet)) {
                            $propertyCollection = $order->getPropertyCollection();
                            $ar = $propertyCollection->getArray();
                            foreach ($ar['properties'] as $arProp) {
                                if (isset($arPropsToSet[$arProp['CODE']])) {
                                    $prop = $propertyCollection->getItemByOrderPropertyId($arProp['ID']);
                                    $prop->setValue($arPropsToSet[$arProp['CODE']]);
                                }
                            }
                        }
                        // ROW PROCEED
                        $arNeedXML = array();
                        foreach ($orderXML->TABLE->ROW as $row) {
                            $row = json_decode(json_encode($row), true);
                            if (empty($arXMLID[$row['PRODUCT']])) {
                                $arNeedXML[] = $row['PRODUCT'];
                            }
                        }
                        if (!empty($arNeedXML)) {
                            $rs = \CIBlockElement::getList(array(), array('XML_ID' => $arNeedXML, 'IBLOCK_ID' => $IBLOCK_ID));
                            while ($ar = $rs->GetNext(0, 0)) {
                                $arXMLID[$ar['XML_ID']] = $ar;
                            }
                        }
                        // todo: make logging on skipped orders
                        if (count($orderXML->TABLE->ROW)) {
                            $orderItems = array();
                            foreach ($orderXML->TABLE->ROW as $row) {
                                $row = json_decode(json_encode($row), true);
                                $orderItems[] = array_merge($row, array('PRODUCT_ID' => $arXMLID[$row['PRODUCT']]['ID'], 'PRODUCT_BASE' => $arXMLID[$row['PRODUCT']]));
                            }

                            $basket = $order->getBasket();
                            $basketItems = $basket->getBasketItems();
                            $rowNumber = 0;
                            $count = count($orderItems);
                            /** @var \Bitrix\Sale\BasketItem $item */
                            foreach ($basketItems as $item) {
                                $rowProduct = &$orderItems[$rowNumber];
                                if (empty($rowProduct)) {
                                    $item->delete();
                                } else {
                                    $item->setFieldsNoDemand(array(
                                        'PRODUCT_ID' => $rowProduct['PRODUCT_ID'],
                                        'QUANTITY' => $rowProduct['QUANTITY'],
                                        'PRICE' => $rowProduct['PRICE'],
                                        'BASE_PRICE' => $rowProduct['PRICE'],
                                        'NAME' => $rowProduct['PRODUCT_BASE']['NAME'],
                                        'PRODUCT_XML_ID' => $rowProduct['PRODUCT'],
                                        'CUSTOM_PRICE' => 'Y',
                                    ));
                                }
                                ++$rowNumber;
                                --$count;
                            }
                            if ($count > 0) {
                                for ($i = $rowNumber - 1; ++$i < count($orderItems);) {
                                    $rowProduct = &$orderItems[$i];
                                    if ($rowProduct) {
                                        $item = $basket->createItem('catalog', $rowProduct['PRODUCT_ID']);
                                        $item->setFieldsNoDemand(array(
                                            'PRICE' => $rowProduct['PRICE'],
                                            'BASE_PRICE' => $rowProduct['PRICE'],
                                            'CUSTOM_PRICE' => 'Y',
                                            'NAME' => $rowProduct['PRODUCT_BASE']['NAME'],
                                            'PRODUCT_XML_ID' => $rowProduct['PRODUCT'],
                                            'QUANTITY' => $rowProduct['QUANTITY'],
                                            'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                                            'LID' => $basket->getSiteId(),
                                        ));
                                    }
                                }
                            }
                            // todo: make exception catch
                            $GLOBALS['IS_DISABLE_UNIT_CALC'] = true;
                            $basket->save();
                            $GLOBALS['IS_DISABLE_UNIT_CALC'] = false;
                            $discount = $order->getDiscount();
                            \Bitrix\Sale\DiscountCouponsManager::clearApply(true);
                            \Bitrix\Sale\DiscountCouponsManager::useSavedCouponsForApply(true);
                            $discount->setOrderRefresh(true);
                            $discount->setApplyResult(array());
                            $basket->refreshData(array('PRICE', 'COUPONS'));
                            $discount->calculate();
                        }
                        $GLOBALS['IS_DISABLE_UNIT_CALC'] = true;
                        $order->refreshData();
                        $order->save();
                        $GLOBALS['IS_DISABLE_UNIT_CALC'] = false;
                        ++$orderProceed;
                    }
                    // todo: make exception catch
                    echo 'success@' . $orderProceed . '@' . $orderNum;
                }
            }
        }

    }

    public function ImportProfiles() {
        static $cache, $location_cache;

        $headers = $this->emu_getallheaders();
        $res = $this->Check($headers);
        $r = $res[0];
        if (!$r) {
            die('Access Denied! - ' . $res[1]);
        }
        $filename = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/tmp/htmls.1c77exchange/" . $_REQUEST['filename'];
        $xml = simplexml_load_file($filename);
        if (!$xml) {
            die('error@xml read error');
        }
        if (is_null($cache)) {
            $rs = \CSaleOrderProps::GetList(array('SORT' => 'desc'), array('ACTIVE' => 'Y', 'USER_PROPS' => 'Y'));
            while ($ar = $rs->Fetch()) {
                $cache[$ar['PERSON_TYPE_ID']][$ar['CODE']] = $ar;
            }
        }
        foreach ($xml->CUSTOMER as $customerXML) {
            $userID = (int)$customerXML->USER_ID;

            if ($userID <= 0) {
                continue; // User ID or Customer TYempty
            }
            $rs = \Bitrix\Main\UserTable::getById($userID);
            if (!$rs->fetch()) {
                continue; // User with this $userID not exist
            }

            foreach ($customerXML->PROFILES->PROFILE as $profileXML) {
                $profileID = (string)$profileXML->XML_ID;
                $personTypeID = (int)$profileXML->TYPE;
                if (strlen($profileID) <= 0) {
                    continue;
                }
                $arProps = array();

                foreach ($profileXML->PROPERTIES->PROPERTY as $propertyXML) {
                    $code = (string)$propertyXML->NAME;
                    $value = (string)$propertyXML->VALUE;
                    if (!empty($cache[$personTypeID][$code])) {
                        $arProp = $cache[$personTypeID][$code];
                        if ($arProp['IS_LOCATION'] == 'Y') {
                            if (empty($location_cache[$value])) {
                                $rs = \CSaleLocation::GetList(null, array('CITY_NAME' => $value));
                                $arCity = $rs->Fetch();
                                $location_cache[$value] = $arCity['ID'];
                            }
                            $value = $location_cache[$value];
                        }
                        $arProps[$arProp['ID']] = $value;
                    }
                }
                $USER_PROPS_ID = null;
                // get all user profiles ID
                $rs = \CSaleOrderUserProps::GetList(null, array('USER_ID' => $userID));
                $arUserProfiles = array();
                while ($ar = $rs->Fetch()) {
                    $arUserProfiles[] = $ar['ID'];
                }
                // get all user profiles XML_ID
                if (!empty($arUserProfiles)) {
                    $rs = \CSaleOrderUserPropsValue::GetList(null, array('@USER_PROPS_ID' => $arUserProfiles, 'CODE' => 'IS_PROFILE_XML_ID'));
                    while ($ar = $rs->Fetch()) {
                        if ($ar['VALUE'] == $profileID) {
                            $USER_PROPS_ID = $ar['USER_PROPS_ID']; // find it!
                        }
                    }
                }
                if (!$USER_PROPS_ID) { // not exist - create new profile
                    $USER_PROPS_ID = \CSaleOrderUserProps::Add(array(
                        'NAME' => (string)$profileXML->NAME,
                        'USER_ID' => $userID,
                        'PERSON_TYPE_ID' => $personTypeID,
                    ));
                }

                // add technical props
                $arProps[$cache[$personTypeID]['IS_PROFILE_ID']['ID']] = $USER_PROPS_ID;
                $arProps[$cache[$personTypeID]['IS_PROFILE_XML_ID']['ID']] = $profileID;
                $arProps[$cache[$personTypeID]['IS_PROFILE_NAME']['ID']] = (string)$profileXML->NAME;

                // update profile name and other fields
                \CSaleOrderUserProps::Update($USER_PROPS_ID, array('NAME' => (string)$profileXML->NAME, 'USER_ID' => $userID, 'PERSON_TYPE_ID' => $personTypeID));

                // save props
                foreach ($arProps as $propID => $propVal) {
                    if (strlen($propVal) > 0) {
                        \CSaleOrderUserPropsValue::Add(array(
                            'USER_PROPS_ID' => $USER_PROPS_ID,
                            'ORDER_PROPS_ID' => $propID,
                            'VALUE' => $propVal,
                        ));
                    }
                }
            }
        }
        die('success');
    }

//end class CV7ExchangeProcessing

    protected function deleteAllImgFolder($path) {
        if (is_file($path)) return unlink($path);
        if (is_dir($path)) {
            foreach (scandir($path) as $p) if (($p != '.') && ($p != '..'))
                $this->deleteAllImgFolder($path . DIRECTORY_SEPARATOR . $p);
            return rmdir($path);
        }
        return false;
    }

    public function afterProductsUpdate() {
        $this->addCodeToTorgovyeMarki();
        $this->setUnAvailableInActiveProducts();
        $this->deactivateEmptyCategories();

        $this->createSiteMap();

        \Handlers\ProductDiscount\ProductDiscountController::markAllDiscountProducts();
    }

    //добавляем коды к загруженым торговым маркам без кода
    protected function addCodeToTorgovyeMarki() {
        $TM_IBLOCK_ID = 118;
        $translitParams = array("replace_space" => "_", "replace_other" => "_");
        $СElement = new CIBlockElement;

        $elementsTMRows = CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => $TM_IBLOCK_ID, 'CODE' => false
            ],
            false,
            false,
            ['ID', 'CODE', 'NAME']
        );
        while ($element = $elementsTMRows->fetch()) {
            if (empty($element['CODE'])) {
                $newCode = Cutil::translit($element['NAME'], "ru", $translitParams);
                $СElement->Update(
                    $element['ID'],
                    ['CODE' => $newCode]
                );
                $allExistCodes[$newCode] = $element;
            }
        }
    }

    //создаем карту сайта
    protected function createSiteMap() {
        $url = 'https://yardex.ru/local/cron/sitemap.php';
        file_get_contents($url);
    }

    protected function setMeasureRatioForProduct($idProduct,$ratio){
        $ratio = round($ratio,6);
        $productMeasure = CCatalogMeasureRatio::getList(
            [],
            ['PRODUCT_ID' => $idProduct]
        )->fetch();
        $arFields = [
            'PRODUCT_ID' => $idProduct,
            'RATIO' => $ratio
        ];
        if (!$productMeasure){
            CCatalogMeasureRatio::add(
                $arFields
            );
        } else {
            CCatalogMeasureRatio::update(
                $productMeasure['ID'],
                $arFields
            );
        }
    }


    //выставляем 'AVAILABLE' => 'N' и 'QUANTITY' => 0 для неактивных товаров/////////////////////
    //выставляем 'ACTIVE' => 'N' для активных не доступных товаров /////////////////////
    protected function setUnAvailableInActiveProducts() {
        $iblockId = 114; //Каталог 1с 7.7
        $elementsQ = CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => (int)$iblockId, 'ACTIVE' => 'N', 'AVAILABLE' => 'Y'],
            false,
            false,
            ['ID', 'NAME', 'ACTIVE', 'QUANTITY', 'AVAILABLE']
        );
        $CCatalogProduct = new CCatalogProduct;
        while ($row = $elementsQ->fetch()) {
            $arFields = array(
                'ID' => $row['ID'],
                'QUANTITY' => 0,
                'AVAILABLE' => 'N'
            );
            $CCatalogProduct->add($arFields);
        }

        $el = new CIBlockElement;
        $elementsQ = CIBlockElement::GetList(
            [],
            ['IBLOCK_ID' => (int)$iblockId, 'ACTIVE' => 'Y', 'AVAILABLE' => 'N'],
            false,
            false,
            ['ID', 'NAME', 'ACTIVE', 'QUANTITY', 'AVAILABLE']
        );
        $CCatalogProduct = new CCatalogProduct;
        while ($row = $elementsQ->fetch()) {
            $arFields = array(
                'ACTIVE' => 'N',
            );
            $el->update($row['ID'], $arFields);
        }
    }

    function deactivateEmptyCategories(){
        $iblockId = 114; //Каталог 1с 7.7
        $bs = new CIBlockSection;
        $q = $bs->GetList(
            [],
            ['ACTIVE' => 'Y','IBLOCK_ID'=> $iblockId, 'CNT_ACTIVE' => 'Y'],
            true,
            ['NAME','ID','ELEMENT_CNT']
        );

        while ($row = $q->fetch()){
            if ($row['ELEMENT_CNT'] == 0){
                $bs->Update($row['ID'],['ACTIVE'=>'N']);
            }
        }
    }

}