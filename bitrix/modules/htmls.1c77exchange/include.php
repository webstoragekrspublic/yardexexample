<?
/*******************************************************************************
* Module:   1Cv77Exchange                                                      *
* Version:  3.0.28                                                             *
* Author:   Alexey Andreev, HTMLStudio (www.htmls.ru)                          *
* License:  Shareware                                                          *
* Date:     2017-11-15                                                         *
* e-mail:   aav@htmls.ru                                                       *
* set read-rights to file /bitrix/admin/1cv7_exchange.php                      *
*******************************************************************************/

IncludeModuleLangFile(__FILE__);

if(!IsModuleInstalled('sale')){
	ShowError(GetMessage("SALE_MODULE_IS_NOT_INSTALLED"));
	return;
}

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tmp/htmls.1c77exchange/log.txt");

define("V77EXCHANGE_MODULE_DEMO", false);
CModule::AddAutoloadClasses(
	"htmls.1c77exchange",
	array(
		"CV7ExchangeProcessing" => "classes/general/processing.php",
		"CV7ExchangeProcessing2" => "classes/general/processing2.php"
		)
	);


class CV7Exchange{
	var $IP_LIST;

	function CV7Exchange(){
		$this->IP_LIST = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_IP_LIST");
		$this->CGI = COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_PHPLikeCGI");
	}

	function emu_getallheaders(){
		//if (function_exists('getallheaders')) return getallheaders();
		//if($this->CGI != 'Y') return getallheaders();
		foreach ($_SERVER as $name => $value){
			if (substr($name, 0, 5) == 'HTTP_'){
				$name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
				if($name == 'X-Real-Ip') $name = 'X-Real-IP';
				$headers[$name] = $value;
			}
			else if ($name == "CONTENT_TYPE"){
				$headers["Content-Type"] = $value;
			}
			else if ($name == "CONTENT_LENGTH") {
				$headers["Content-Length"] = $value;
			}
		}
		$headers["login"] = $headers["Login"];
		$headers["password"] = $headers["Password"];
		return $headers;
	}

	function Auth(){
		global $APPLICATION, $USER;

		$headers = $this->emu_getallheaders();
        //AddMessage2Log(print_r($headers, true), "htmls.1c77exchange");
		$agent = $headers['User-Agent'];
		if($agent != '1C') die('Access Denied: User-Agent - conflict');
		$ip = $headers['X-Real-IP'];
		if($this->CHECK_IP == 'Y'){
			if(!in_array($ip, $this->IP_LIST)){
				//AddMessage2Log(print_r($headers, true), "htmls.1c77exchange");
				die('Access Denied: IP - conflict');
			}
		}
        $headers['login'] = isset($headers['login']) ? $headers['login'] : $headers['HTTP_LOGIN'];
        $headers['password'] = isset($headers['password']) ? $headers['password'] : $headers['HTTP_PASSWORD'];
        $authResult = $USER->Login($headers['login'], $headers['password'], 'Y');
        if ($authResult === true) {
            echo 'success' . '@' . session_name() . '@' . session_id();
        } else {
            echo 'failure@' . json_encode($authResult);
        }
	}

	function Check($headers){
		global $APPLICATION, $USER, $MESS;

		$agent = $headers['User-Agent'];
		//if($agent != '1C'){ return array(false, '1Cv77');}
		$ip = $headers['X-Real-IP'];
		if($this->CHECK_IP == 'Y'){
			if(!in_array($ip, $this->IP_LIST)){
				//AddMessage2Log(print_r($headers, true), "htmls.1c77exchange");
				return array(false, 'IPList');
			}
		}
		if(!$USER->IsAuthorized()){return array(false, 'Not Authorized');}

		$GROUP_PERMISSIONS = explode(",", COption::GetOptionString("catalog", "1C_GROUP_PERMISSIONS", ""));
		$bUSER_HAVE_ACCESS = false;
		if(isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"])){
			$arUserGroupArray = $GLOBALS["USER"]->GetUserGroupArray();
			foreach($GROUP_PERMISSIONS as $PERM){
				if(in_array($PERM, $arUserGroupArray)){
					$bUSER_HAVE_ACCESS = true;
					break;
				}
			}
		}
		if(!$bUSER_HAVE_ACCESS){
			return array(false, $this->utf2win(GetMessage("CC_BSC1_PERMISSION_DENIED")));
		}
		elseif(!CModule::IncludeModule('iblock')){
			return array(false, $this->utf2win(GetMessage("CC_BSC1_ERROR_MODULE")));
		}
		return array(true);
	}

	function ParseCSV($filename){
		$fname = $filename;
		$filename = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/tmp/htmls.1c77exchange/" . $filename;
		//if(!file_exists($filename)) die('File ' . $fname . ' not exist!');
        if(!file_exists($filename)){
            AddMessage2Log('File ' . $fname . ' not exist!', "htmls.1c77exchange.include");
            return false;
        }

		$row = 1;
		$handle = fopen($filename, "r");
        $header = array();

        while (($arr = fgetcsv($handle, 0, ";",'"')) !== FALSE) {

			if($row == 1){
				$header = $arr;
			}
			else{
                if (count($arr) < count($header)) {
                    $row++;
                    continue;
                }
				foreach($arr as $k => $val){
					//$arRow[$header[$k]] = htmlspecialchars(trim($this->win2utf($val)), ENT_NOQUOTES);
                    $arRow[$header[$k]] = htmlspecialchars(trim($this->win2utf($val)), ENT_NOQUOTES) ? htmlspecialchars(trim($this->win2utf($val)), ENT_NOQUOTES) : htmlspecialcharsEx(trim($this->win2utf($val)));;
                    $arRow[$header[$k]] = html_entity_decode($arRow[$header[$k]]);
				}
                $arResult[] = $arRow;
			}
			$row++;
		}
		fclose($handle);
		return $arResult;
	}
	//$arIB - array from ParseCSV
	function AddElements($arIB){
		foreach($arIB as $k => $v){
			$keys[$v['XML_ID']] = $k;
		}

		$arEL = $this->ParseCSV('ib_elements.csv');
		foreach($arEL as $arr){
			$key = $keys[$arr['IBLOCK_XML_ID']];
			$arElems = $arIB[$key]['LIST'];
			$arElems[$arr['XML_ID']] = $arr['NAME'];
			$arIB[$key]['LIST'] = $arElems;
		}
		return $arIB;
	}
	//$ar - array from ParseCSV
	function PrepareCatalog($ar){

    	$fields = array('CODE', 'NAME', 'XML_ID', 'SECTION_XML_ID', 'PREVIEW_TEXT', 'DETAIL_TEXT', 'SORT', 'PREVIEW_PICTURE', 'DETAIL_PICTURE');
		foreach($ar as $arData){
			$arFields = array();
			$arProps = array();
			foreach($arData as $k => $v){
					if(in_array($k, $fields)) $arFields[$k] = $v;
					else $arProps[$k] = $v;
			}
			$p[] = array('arFields' => $arFields, 'arProps' => $arProps);
		}
		return $p;
	}
	//$ar - array from ParseCSV
	//$pr - array(offers_id => offers_name) use only offers_id
	function PrepareOffers($ar, $pr){
		$fields = array('IBLOCK_ID', 'QUANTITY', 'XML_ID', 'QUANTITY_TRACE', 'VAT', 'WEIGHT', 'CAN_BUY_ZERO', 'NEGATIVE_AMOUNT_TRACE', 'MEASURE', 'LENGTH', 'WIDTH', 'HEIGHT');
		foreach($ar as $arData){
			$arProduct = array();
			$arOffers = array();
            $arCatalog = array();
			foreach($arData as $k => $v){
					if(in_array($k, $fields)) $arProduct[$k] = $v;
					else{
						if(array_key_exists($k, $pr)){
						  $arOffers[] = array('ID' => $k, 'VALUE' => $v);
                        }
                        else{
                            $arCatalog[$k] = $v;
                        }
					}
			}
			$p[] = array('PRODUCT' => $arProduct, 'OFFERS' => $arOffers, 'CATALOG' => $arCatalog);
		}
		return $p;
	}

	//EXPORT ORDERS
	function GetXml($arOrders){
		$xml = '<?xml version="1.0" encoding="windows-1251" standalone="no"?>';
		$FEEDBACK=COption::GetOptionString('htmls.1c77exchange', 'V77EXCHANGE_ORDER_STATUS_FROM_1C', 'NO');
		$xml .= '<ORDERS>';
		foreach($arOrders as $arOrder){
    		$xml .= '<ORDER><DATE>'.$arOrder['DATE'].'</DATE>';
			$xml .= '<ID>'.$arOrder['ID'].'</ID>';
			$xml .= '<NUMBER>'.$arOrder['NUMBER'].'</NUMBER>';
            $xml .= '<XML_ID>' . $arOrder['XML_ID'] . '</XML_ID>';
            $xml .= '<STATUS_ID>'.$arOrder['STATUS_ID'].'</STATUS_ID>';
			$xml .= '<FEEDBACK>'.$FEEDBACK.'</FEEDBACK>';
            $xml .= '<COMMENT>'.$arOrder['COMMENT'].'</COMMENT>';
            $xml .= '<DELIVERY>';
            $xml .= '<DATE>'.$arOrder['DELIVERY']['DATE'].'</DATE>';
            $xml .= '<TIME>'.$arOrder['DELIVERY']['TIME'].'</TIME>';
            $xml .= '<TIME_TMS>'.$arOrder['DELIVERY']['TIME_TMS'].'</TIME_TMS>';
            $xml .= '<TYPE>'.$arOrder['DELIVERY']['TYPE'].'</TYPE>';
            $xml .= '<PRICE>'.$arOrder['DELIVERY']['PRICE'].'</PRICE>';
            $xml .= '<GEO_INFO>'.$arOrder['DELIVERY']['GEO_INFO'].'</GEO_INFO>';
            $xml .= '<PREPARE_CHANGE_FROM_AMOUNT>'.$arOrder['DELIVERY']['PREPARE_CHANGE_FROM_AMOUNT'].'</PREPARE_CHANGE_FROM_AMOUNT>';
            $xml .= '</DELIVERY>';
            $xml .= '<PROPERTIES>';
            foreach ($arOrder['PROPERTIES'] as $row) {
                $propName = $this->win2utf(htmlspecialchars($row['NAME']));
                $xml .= "<$propName>{$row['VALUE']}</$propName>";
            }
            $xml .= '</PROPERTIES>';
            $xml .= '<PAYMENT>';
            $xml .= '<ID>'.$arOrder['PAYMENT']['ID'].'</ID>';
            $xml .= '<NAME>'.$this->utf2win($arOrder['PAYMENT']['NAME']).'</NAME>';
            $xml .= '</PAYMENT>';
            $xml .= '<STORE>'.$arOrder['STORE_ID'].'</STORE>';
            $xml .= self::GetXMLCustomer($arOrder['USER'], self::getProfileFromOrder($arOrder));
            $xml .= '<TABLE>';

            foreach ($arOrder['ROWS'] as $row) {
                $code = ($row['PRODUCT_XML_ID']);
                $xml .= '<ROW>';
                $xml .= '<PRODUCT>' . $code . '</PRODUCT>';
                $xml .= '<QUANTITY>' . $row['QUANTITY'] . '</QUANTITY>';
                $xml .= '<PRICE>' . $row['PRICE'] . '</PRICE>';
                $xml .= '<NAME>' . $row['PRODUCT_NAME'] . '</NAME>';
                $xml .= '<UNIT>' . $row['UNIT'] . '</UNIT>';
                $xml .= '<VAT_RATE>' . $row['VAT_RATE'] . '</VAT_RATE>';
                $xml .= '</ROW>';
            }
			$xml .= '</TABLE></ORDER>';
		}
		$xml .= '</ORDERS>';
		return $xml;
	}

    private static function getProfileFromOrder($arOrder)
    {
        $arCustom = array(
            'IS_PROFILE_ID' => '',
            'IS_PROFILE_NAME' => '',
            'IS_PROFILE_XML_ID' => '',
        );
        foreach ($arOrder['PROPERTIES'] as $key => $arProp) {
            if (isset($arCustom[$arProp['NAME']])) {
                $arCustom[$arProp['NAME']] = $arProp['VALUE'];
                unset($arOrder['PROPERTIES'][$key]);
            }
        }
        $arProfile = array(
            'ID' => $arCustom['IS_PROFILE_ID'],
            'NAME' => $arCustom['IS_PROFILE_NAME'],
            'XML_ID' => $arCustom['IS_PROFILE_XML_ID'],
            'TYPE' => $arOrder['USER']['PERSON_TYPE'],
            'PROPERTIES' => $arOrder['PROPERTIES'],
        );

        return array($arProfile);
    }


    public static function GetXMLCustomer($arUser, $arProfiles)
    {
        $rs = CUser::GetByID($arUser['USER_ID']);
        $arUserFull = $rs->Fetch();
        $userName = trim($arUserFull['LAST_NAME'] . ' ' . $arUserFull['NAME']);
        $userName = self::utf2win(htmlspecialchars($userName));
        $xml = "";
        $xml .= "<CUSTOMER><TYPE>${arUser['PERSON_TYPE']}</TYPE>";
        $xml .= "<USER_ID>${arUserFull['ID']}</USER_ID>";
        $xml .= "<XML_ID>${arUserFull['XML_ID']}</XML_ID>";
        $xml .= "<NAME>${userName}</NAME>";
        $xml .= "<EMAIL>${arUserFull['EMAIL']}</EMAIL>";
        $xml .= "<PROFILES>";
        foreach ($arProfiles as $p) {
            $xml .= "<PROFILE>";
            $xml .= "<ID>${p['ID']}</ID>";
            $xml .= "<XML_ID>${p['XML_ID']}</XML_ID>";
            $xml .= "<NAME>${p['NAME']}</NAME>";
            $xml .= "<TYPE>${p['TYPE']}</TYPE>";
            $xml .= "<PROPERTIES>";
            foreach ($p['PROPERTIES'] as $prop) {
                $xml .= '<PROPERTY><NAME>' . $prop['NAME'] . '</NAME><VALUE>' . $prop['VALUE'] . '</VALUE></PROPERTY>';
            }
            $xml .= "</PROPERTIES>";
            $xml .= "</PROFILE>";
        }
        $xml .= "</PROFILES>";
        $xml .= "</CUSTOMER>";
        return $xml;
    }

	function win2utf($str) {
		if(strtoupper(LANG_CHARSET) == 'UTF-8')
			return iconv('WINDOWS-1251', 'UTF-8', $str);
		else
			return $str;
	}

	function utf2win($str) {
		if(strtoupper(LANG_CHARSET) == 'UTF-8')
			return iconv('UTF-8', 'WINDOWS-1251', $str);
		else
			return $str;
	}
//end class CV7Exchange
}
?>