<?
//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.1c77exchange/prolog.php");

$module_id = "htmls.1c77exchange";
$V77_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($V77_RIGHT>="R") :

global $MESS;

$groups = array();
include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/main/lang/", "/options.php"));
include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"]."/bitrix/modules/".$module_id."/lang/", "/options.php"));
if (!IsModuleInstalled('sale')){
	ShowError(GetMessage("SALE_MODULE_IS_NOT_INSTALLED"));
	return;
}

CModule::IncludeModule('sale');
$res = CSaleStatus::GetList(array(), array('LID'=>SITE_ID), false, false, array('ID', 'NAME'));
$arSelectStatus['NO'] = $MESS['V77EXCHANGE_ORDER_STATUS_FROM_1C_NO'];
while($arStatus = $res->Fetch()){
	$arSelectStatus[$arStatus['ID']] = $arStatus['NAME'];
}

$arSelectBy['DATE_INSERT'] = $MESS['DATE_INSERT'];
$arSelectBy['DATE_UPDATE'] = $MESS['DATE_UPDATE'];
$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("V77EXCHANGE_TAB_SET"), "ICON" => "v77exchange_settings", "TITLE" => GetMessage("V77EXCHANGE_TAB_SET_ALT")),
    array("DIV" => "edit4", "TAB" => GetMessage("V77EXCHANGE_TAB_1CEXCHANGE"), "ICON" => "v77exchange_settings", "TITLE" => GetMessage("V77EXCHANGE_TAB_1CEXCHANGE_ALT")),
	array("DIV" => "edit3", "TAB" => GetMessage("V77EXCHANGE_TAB_1CFILES"), "ICON" => "v77exchange_settings", "TITLE" => GetMessage("V77EXCHANGE_TAB_1CFILES_ALT")),
	array("DIV" => "edit2", "TAB" => GetMessage("V77EXCHANGE_TAB_RIGHTS"), "ICON" => "v77exchange_settings", "TITLE" => GetMessage("V77EXCHANGE_TAB_RIGHTS_ALT"))
);

$arAllOptions = array(
	array("V77EXCHANGE_CHECK_IP", GetMessage("V77EXCHANGE_CHECK_IP"), "",Array("checkbox")),
	array("V77EXCHANGE_IP_LIST", GetMessage("V77EXCHANGE_IP_LIST"), "",array("text", 50)),
	Array("V77EXCHANGE_IP_LIST_HINT", "", BeginNote().GetMessage("V77EXCHANGE_IP_LIST_HINT").EndNote(), Array("statichtml", "")),
	array("V77EXCHANGE_PHPLikeCGI", GetMessage("V77EXCHANGE_PHPLikeCGI"), "",Array("checkbox")),
    array("V77EXCHANGE_GET_ORDERS_BY", GetMessage("V77EXCHANGE_GET_ORDERS_BY"), "", Array("selectbox", $arSelectBy)),
	array("V77EXCHANGE_ORDER_STATUS_FROM_1C", GetMessage("V77EXCHANGE_ORDER_STATUS_FROM_1C"), "", Array("selectbox", $arSelectStatus)),
    array("V77EXCHANGE_SHIPMENT_XML_ID", GetMessage("V77EXCHANGE_SHIPMENT_XML_ID"), "",array("text", 50)),
	GetMessage("V77EXCHANGE_CATALOG_STRUCTURE_IMPORT_SETTINGS"),
	array("V77EXCHANGE_CATALOG_USE_BASE64", GetMessage("V77EXCHANGE_CATALOG_USE_BASE64"), "N",Array("checkbox")),
	array("V77EXCHANGE_CATALOG_UPDATE_NAME", GetMessage("V77EXCHANGE_CATALOG_UPDATE_NAME"), "Y",Array("checkbox")),
	array("V77EXCHANGE_KEEP_LOCAL_CATALOG_STRUCTURE", GetMessage("V77EXCHANGE_KEEP_LOCAL_CATALOG_STRUCTURE"), "",Array("checkbox")),
	Array("V77EXCHANGE_CATALOG_STRUCTURE_IMPORT_HINT", "", BeginNote().GetMessage("V77EXCHANGE_CATALOG_STRUCTURE_IMPORT_HINT").EndNote(), Array("statichtml", "")),
	GetMessage("V77EXCHANGE_IMG_IMPORT_SETTINGS"),
	array("V77EXCHANGE_IMG_IMPORT_DELETE", GetMessage("V77EXCHANGE_IMG_IMPORT_DELETE"), "",Array("checkbox")),
	array("V77EXCHANGE_IMG_IMPORT_FOLDER", GetMessage("V77EXCHANGE_IMG_IMPORT_FOLDER"), "",Array("text", 50)),
	array("V77EXCHANGE_IMG_IMPORT_SUBFOLDER", GetMessage("V77EXCHANGE_IMG_IMPORT_SUBFOLDER"), "",Array("text", 50)),
    Array("V77EXCHANGE_IMG_IMPORT_TYPES_HINT", "", BeginNote().GetMessage("V77EXCHANGE_IMG_IMPORT_SUBFOLDER_HINT").EndNote(), Array("statichtml", "")),
    array("V77EXCHANGE_IMG_IMPORT_MULTI", GetMessage("V77EXCHANGE_IMG_IMPORT_MULTI"), "N",Array("checkbox")),
    Array("V77EXCHANGE_IMG_IMPORT_TYPES_HINT", "", BeginNote().GetMessage("V77EXCHANGE_IMG_IMPORT_MULTI_HINT").EndNote(), Array("statichtml", "")),
	array("V77EXCHANGE_IMG_IMPORT_TYPES", GetMessage("V77EXCHANGE_IMG_IMPORT_TYPES"), "",Array("text", 50)),
	Array("V77EXCHANGE_IMG_IMPORT_TYPES_HINT", "", BeginNote().GetMessage("V77EXCHANGE_IMG_IMPORT_TYPES_HINT").EndNote(), Array("statichtml", "")),
	array("V77EXCHANGE_IMG_IMPORT_LOG", GetMessage("V77EXCHANGE_IMG_IMPORT_LOG"), "",Array("checkbox")),
	Array("V77EXCHANGE_IMG_IMPORT_LOG_HINT", "", BeginNote().GetMessage("V77EXCHANGE_IMG_IMPORT_LOG_HINT").EndNote(), Array("statichtml", "")),
	GetMessage("V77EXCHANGE_USER_FUNCTION"),
	array("V77EXCHANGE_USER_FUNCTION_AFTER_ADD", GetMessage("V77EXCHANGE_USER_FUNCTION_AFTER_ADD"), "",Array("text", 50)),
	array("V77EXCHANGE_USER_FUNCTION_AFTER_UPDATE", GetMessage("V77EXCHANGE_USER_FUNCTION_AFTER_UPDATE"), "",Array("text", 50)),
	//array("V77EXCHANGE_IMG_IMPORT_METHOD", GetMessage("V77EXCHANGE_IMG_IMPORT_METHOD"), "",Array("selectbox", array("SYS"=>GetMessage("V77EXCHANGE_IMG_IMPORT_SYS"),"CUSTOM"=>GetMessage("V77EXCHANGE_IMG_IMPORT_CUSTOM")))),
	//Array("V77EXCHANGE_IMG_IMPORT_METHOD_HINT", "", BeginNote().GetMessage("V77EXCHANGE_IMG_IMPORT_METHOD_HINT").EndNote(), Array("statichtml", "")),
//	array("V77EXCHANGE_ADD_HEADER_NAME", GetMessage("V77EXCHANGE_ADD_HEADER_NAME"), Array("text", 30)),
//	array("V77EXCHANGE_ADD_HEADER_VALUE", GetMessage("V77EXCHANGE_ADD_HEADER_VALUE"), array("text-list", 3, 20))

);

//1cexchange - start
if(CModule::IncludeModule("iblock")){

		$arIBlockType = array(
			"-" => GetMessage("CAT_1C_CREATE"),
		);
		$rsIBlockType = CIBlockType::GetList(array("sort"=>"asc"), array("ACTIVE"=>"Y"));
		while ($arr=$rsIBlockType->Fetch())
		{
			if($ar=CIBlockType::GetByIDLang($arr["ID"], LANGUAGE_ID))
			{
				$arIBlockType[$arr["ID"]] = "[".$arr["ID"]."] ".$ar["NAME"];
			}
		}

		$rsSite = CSite::GetList($by="sort", $order="asc", $arFilter=array("ACTIVE" => "Y"));
		$arSites = array(
			"-" => GetMessage("CAT_1C_CURRENT"),
		);
		while ($arSite = $rsSite->GetNext())
		{
			$arSites[$arSite["LID"]] = $arSite["NAME"];
		}

		$arUGroupsEx = Array();
		$dbUGroups = CGroup::GetList($by = "c_sort", $order = "asc");
		while($arUGroups = $dbUGroups -> Fetch())
		{
			$arUGroupsEx[$arUGroups["ID"]] = $arUGroups["NAME"];
		}

        $arOrderStatus = Array();
        $arOrderStatus[0] = GetMessage("1C_EXPORT_FINAL_ORDERS_ALL");
		$dbOrderStatus = CSaleStatus::GetList($by = "sort", $order = "asc");
		while($arStatus = $dbOrderStatus -> Fetch())
		{
			$arOrderStatus[$arStatus["ID"]] = '['.$arStatus["ID"].'] ' . $arStatus["NAME"];
		}

		$arAction = array(
			"N" => GetMessage("CAT_1C_NONE"),
			"A" => GetMessage("CAT_1C_DEACTIVATE"),
			"D" => GetMessage("CAT_1C_DELETE"),
		);

        $arAllOptions1C = array(
            //GetMessage("V77EXCHANGE_CATALOG_IMPORT_SETTINGS"),
			array("1C_IBLOCK_TYPE", GetMessage("CAT_1C_IBLOCK_TYPE"), "-", Array("list", $arIBlockType)),
			array("1C_SITE_LIST", GetMessage("CAT_1C_SITE_LIST"), "-", Array("list", $arSites)),
			//array("1C_INTERVAL", GetMessage("CAT_1C_INTERVAL"), "30", Array("text", 20)),
			array("1C_GROUP_PERMISSIONS", GetMessage("CAT_1C_GROUP_PERMISSIONS"), "-", Array("mlist", 5, $arUGroupsEx)),
			array("1C_ELEMENT_ACTION", GetMessage("CAT_1C_ELEMENT_ACTION"), "D", Array("list", $arAction)),
			array("1C_SECTION_ACTION", GetMessage("CAT_1C_SECTION_ACTION"), "D", Array("list", $arAction)),
			array("1C_EXPORT_FINAL_ORDERS", GetMessage("1C_EXPORT_FINAL_ORDERS"), "", Array("list", $arOrderStatus)),
			//array("1C_FILE_SIZE_LIMIT", GetMessage("CAT_1C_FILE_SIZE_LIMIT"), 200*1024, Array("text", 20)),
			//array("1C_USE_CRC", GetMessage("CAT_1C_USE_CRC"), "Y", Array("checkbox")),
			array("1C_USE_ZIP", GetMessage("CAT_1C_USE_ZIP"), "Y", Array("checkbox")),
			//array("1C_USE_IBLOCK_PICTURE_SETTINGS", GetMessage("CAT_1C_USE_IBLOCK_PICTURE_SETTINGS"), "N", Array("checkbox")),
			//array("1C_GENERATE_PREVIEW", GetMessage("CAT_1C_GENERATE_PREVIEW"), "Y", Array("checkbox")),
			array("1C_PREVIEW_WIDTH", GetMessage("CAT_1C_PREVIEW_WIDTH"), 100, Array("text", 20)),
			array("1C_PREVIEW_HEIGHT", GetMessage("CAT_1C_PREVIEW_HEIGHT"), 100, Array("text", 20)),
			//array("1C_DETAIL_RESIZE", GetMessage("CAT_1C_DETAIL_RESIZE"), "Y", Array("checkbox")),
			array("1C_DETAIL_WIDTH", GetMessage("CAT_1C_DETAIL_WIDTH"), 300, Array("text", 20)),
			array("1C_DETAIL_HEIGHT", GetMessage("CAT_1C_DETAIL_HEIGHT"), 300, Array("text", 20)),
			//array("1C_USE_OFFERS", GetMessage("CAT_1C_USE_OFFERS"), "N", Array("checkbox")),
			//array("1C_FORCE_OFFERS", GetMessage("CAT_1C_FORCE_OFFERS"), "N", Array("checkbox")),
			//array("1C_USE_IBLOCK_TYPE_ID", GetMessage("CAT_1C_USE_IBLOCK_TYPE_ID"), "N", Array("checkbox")),
			array("1C_SKIP_ROOT_SECTION", GetMessage("CAT_1C_SKIP_ROOT_SECTION"), "N", Array("checkbox")),
			//array("1C_TRANSLIT_ON_ADD", GetMessage("CAT_1C_TRANSLIT_ON_ADD"), "N", Array("checkbox")),
			//array("1C_TRANSLIT_ON_UPDATE", GetMessage("CAT_1C_TRANSLIT_ON_UPDATE"), "N", Array("checkbox")),
		);
}
//1cexchange - end

//CModule::IncludeModuleEx("htmls.1c77plus");
if(CModule::IncludeModuleEx("htmls.1c77plus") < 3){
  if(IsModuleInstalled("htmls.1c77plus")){
    $arAllOptions[] = GetMessage("V77EXCHANGE_BUSINESS_SETTINGS");
    $arAllOptions[] = array("V77EXCHANGE_USE_STORE", GetMessage("V77EXCHANGE_USE_STORE"), "",Array("checkbox"));
    $arAllOptions[] = array("V77EXCHANGE_USE_SKU", GetMessage("V77EXCHANGE_USE_SKU"), "",Array("checkbox"));
  }
}

if ($REQUEST_METHOD=="POST" && $V77_RIGHT=="W" && check_bitrix_sessid() && $_REQUEST["SUpdate"])
{

		foreach($arAllOptions as $arOption)
		{
			__AdmSettingsSaveOption($module_id, $arOption);

		/*	$name = $arOption[0];
			if($arOption[2][0]=="text-list")
			{
				$val = "";
				for($j=0; $j<count($$name); $j++)
				{
					if(strlen(trim(${$name}[$j])) > 0)
						$val .= ($val <> ""? ",":"").trim(${$name}[$j]);
				}
			}
			else
				$val=$$name;
			if($arOption[2][0] == "checkbox" && $val <> "Y")
				$val="N";
			if($name != "mail_additional_parameters" || $USER->IsAdmin())
				COption::SetOptionString($module_id, $name, $val);*/
		}
        for ($i = 0, $intCount = count($arAllOptions1C); $i < $intCount; $i++)
    	{
    		$name = $arAllOptions1C[$i][0];
    		$val = $_REQUEST[$name];
        	if($arAllOptions1C[$i][3][0]=="checkbox" && $val!="Y")
    			$val = "N";
    		if($arAllOptions1C[$i][3][0]=="mlist")
    			$val = implode(",", $val);
            if($name == '1C_EXPORT_FINAL_ORDERS')
    		    COption::SetOptionString("sale", $name, $val, $arAllOptions1C[$i][1]);
            else
    		    COption::SetOptionString("catalog", $name, $val, $arAllOptions1C[$i][1]);
    	}

}



$tabControl = new CAdminTabControl("tabControl", $aTabs);

function ShowParamsHTMLByArray($arParams)
{
	foreach($arParams as $Option)
	{
	 	__AdmSettingsDrawRow("htmls.1c77exchange", $Option);
	}
}

function ShowParamsHTMLByArray1C($arParams)
{
	foreach($arParams as $Option)
	{
	 	__AdmSettingsDrawRow("catalog", $Option);
	}
}

$tabControl->Begin();
?><form method="POST" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialchars($mid)?>&lang=<?=LANGUAGE_ID?>"><?
bitrix_sessid_post();

$tabControl->BeginNextTab();//settings
ShowParamsHTMLByArray($arAllOptions);
	/*foreach($arAllOptions as $Option):
	$type = $Option[2];
	$val = COption::GetOptionString($module_id, $Option[0]);
	?>
	<tr>
		<td valign="top" width="50%"><?
			if($type[0]=="checkbox")
				echo "<label for=\"".htmlspecialchars($Option[0])."\">".$Option[1]."</label>";
			else
				echo $Option[1];
		?></td>
		<td valign="middle" width="50%"><?
			if($type[0]=="checkbox"):
				?><input type="checkbox" name="<?echo htmlspecialchars($Option[0])?>" id="<?echo htmlspecialchars($Option[0])?>" value="Y"<?if($val=="Y")echo" checked";?>><?
			elseif($type[0]=="text"):
				?><input type="text" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialchars($val)?>" name="<?echo htmlspecialchars($Option[0])?>"><?
			elseif($type[0]=="textarea"):
				?><textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialchars($Option[0])?>"><?echo htmlspecialchars($val)?></textarea><?
			elseif($type[0]=="text-list"):
				$aVal = explode(",", $val);
				for($j=0; $j<count($aVal); $j++):
					?><input type="text" size="<?echo $type[2]?>" value="<?echo htmlspecialchars($aVal[$j])?>" name="<?echo htmlspecialchars($Option[0])."[]"?>"><br><?
				endfor;
				for($j=0; $j<$type[1]; $j++):
					?><input type="text" size="<?echo $type[2]?>" value="" name="<?echo htmlspecialchars($Option[0])."[]"?>"><br><?
				endfor;
			elseif($type[0]=="selectbox"):
				$arr = $type[1];
				$arr_keys = array_keys($arr);
				?><select name="<?echo htmlspecialchars($Option[0])?>"><?
					for($j=0; $j<count($arr_keys); $j++):
						?><option value="<?echo $arr_keys[$j]?>"<?if($val==$arr_keys[$j])echo" selected"?>><?echo htmlspecialchars($arr[$arr_keys[$j]])?></option><?
					endfor;
					?></select><?
			endif;
		?></td>
	</tr>
	<?endforeach*/?>

<?
$tabControl->BeginNextTab();//1cexchange
//ShowParamsHTMLByArray1C($arAllOptions1C);
    foreach($arAllOptions1C as $Option){
      if(is_array($Option)){
        if($Option[0] == '1C_EXPORT_FINAL_ORDERS')
			$val = COption::GetOptionString("sale", $Option[0], $Option[2]);
        else
			$val = COption::GetOptionString("catalog", $Option[0], $Option[2]);
			$type = $Option[3];
			?>
		<tr>
			<td <? echo ('textarea' == $type[0] || 'mlist' == $type[0] ? 'valign="top"' : ''); ?> width="40%"><?	if($type[0]=="checkbox")
							echo "<label for=\"".htmlspecialcharsbx($Option[0])."\">".$Option[1]."</label>";
						else
							echo $Option[1];?>:</td>
			<td width="60%">
					<?if($type[0]=="checkbox"):?>
						<input type="checkbox" name="<?echo htmlspecialcharsbx($Option[0])?>" id="<?echo htmlspecialcharsbx($Option[0])?>" value="Y"<?if($val=="Y")echo" checked";?> onclick="Check(this.id);">
					<?elseif($type[0]=="text"):?>
						<input type="text" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($Option[0])?>" id="<?echo htmlspecialcharsbx($Option[0])?>">
					<?elseif($type[0]=="textarea"):?>
						<textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialcharsbx($Option[0])?>" id="<?echo htmlspecialcharsbx($Option[0])?>"><?echo htmlspecialcharsbx($val)?></textarea>
					<?elseif($type[0]=="list"):?>
						<select name="<?echo htmlspecialcharsbx($Option[0])?>" id="<?echo htmlspecialcharsbx($Option[0])?>">
						<?foreach($type[1] as $key=>$value):?>
							<option value="<?echo htmlspecialcharsbx($key)?>" <?if($val==$key) echo "selected"?>><?echo htmlspecialcharsbx($value)?></option>
						<?endforeach?>
						</select>
					<?elseif($type[0]=="mlist"):
						$val = explode(",", $val)?>
						<select multiple name="<?echo htmlspecialcharsbx($Option[0])?>[]" size="<?echo $type[1]?>" id="<?echo htmlspecialcharsbx($Option[0])?>">
						<?foreach($type[2] as $key=>$value):?>
							<option value="<?echo htmlspecialcharsbx($key)?>" <?if(in_array($key, $val)) echo "selected"?>><?echo htmlspecialcharsbx($value)?></option>
						<?endforeach?>
						</select>
					<?endif?>
			</td>
		</tr>
		<?
        }//is_array
        else{
        ?>
		<tr class="heading">
            <td colspan="2">
                <?echo $Option; ?>
			</td>
		</tr>
		<?
        }
        };//foreach

//1cexchange - end

$tabControl->BeginNextTab();//1c77 files
$Exchange = "/bitrix/modules/".$module_id."/1Cv77/1CExchangeV2.3.ert";
$processing = "/bitrix/modules/".$module_id."/1Cv77/processing.ert";
$za7 = "/bitrix/modules/".$module_id."/1Cv77/7za.exe";
?>
	<tr>
		<td valign="top" width="100%" colspan='2'><?=GetMessage('V77EXCHANGE_1CFILES_MANUAL')?></td>
	</tr>
	<tr>
		<td valign="top" width="60%"><?=GetMessage('V77EXCHANGE_1CFILES_EXCHANGE')?></td>
		<td valign="top"><a href='<?=$Exchange?>' onclick="jsUtils.Redirect([], 'fileman_file_download.php?path=<?=$Exchange?>&site=s1&lang=ru'); return false;"><?=GetMessage("V77EXCHANGE_1CFILES_DOWNLOAD")?></a></td>
	</tr>
	<tr>
		<td valign="top" width="60%"><?=GetMessage('V77EXCHANGE_1CFILES_PROCESSING')?></td>
		<td valign="top"><a href='<?=$processing?>' onclick="jsUtils.Redirect([], 'fileman_file_download.php?path=<?=$processing?>&site=s1&lang=ru'); return false;"><?=GetMessage("V77EXCHANGE_1CFILES_DOWNLOAD")?></a></td>
	</tr>
	<tr>
		<td valign="top" width="60%"><?=GetMessage('V77EXCHANGE_1CFILES_7ZA')?></td>
		<td valign="top"><a href='<?=$za7?>' onclick="jsUtils.Redirect([], 'fileman_file_download.php?path=<?=$za7?>&site=s1&lang=ru'); return false;"><?=GetMessage("V77EXCHANGE_1CFILES_DOWNLOAD")?></a></td>
	</tr>
<?

$tabControl->BeginNextTab();//rights
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");


$tabControl->Buttons();
?>
<script language="JavaScript">
function RestoreDefaults()
{
	if (confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>'))
		window.location = "<?echo $APPLICATION->GetCurPage()?>?RestoreDefaults=Y&lang=<?echo LANG?>&mid=<?echo urlencode($mid)."&".bitrix_sessid_get();?>";
}
</script>

<input type="submit" <?if ($V77_RIGHT<"W") echo "disabled" ?> name="SUpdate" value="<?echo GetMessage("MAIN_SAVE")?>">
<input type="hidden" name="Update" value="Y">
<input type="reset" name="reset" value="<?echo GetMessage("MAIN_RESET")?>">
<?
$tabControl->End();
?>
</form>
<?
endif;
?>
<? /*require ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");*/?>
