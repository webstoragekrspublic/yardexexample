<?
$MESS['V77EXCHANGE_IP_LIST'] = 'Список IP-адресов, с которых возможен обмен';
$MESS['V77EXCHANGE_TAB_SET'] = 'Настройка обмена с 1С:Предприятие 7.7';
$MESS['V77EXCHANGE_TAB_SET_ALT'] = 'Настройка модуля обмена данными с 1С:Предприятие 7.7';
$MESS['V77EXCHANGE_TAB_RIGHTS'] = 'Доступ';
$MESS['V77EXCHANGE_TAB_RIGHTS_ALT'] = '	Уровень доступа к модулю';
$MESS['V77EXCHANGE_PHPLikeCGI'] = 'PHP установлен как CGI';
$MESS['V77EXCHANGE_CHECK_IP'] = 'Проверять IP-адрес';
$MESS["V77EXCHANGE_TAB_1CFILES"] = 'Файлы для 1С7.7';
$MESS["V77EXCHANGE_TAB_1CFILES_ALT"] = 'Внешние обработки для 1С:Предприятие 7.7';
$MESS['V77EXCHANGE_1CFILES_EXCHANGE'] = 'Файл 1CExchangeV2.3.ert<br/>(Файл предназначен для настроек процедуры обмена.<br/>Модификация данного файла допустима)';
$MESS['V77EXCHANGE_1CFILES_DOWNLOAD'] = 'Скачать';
$MESS['V77EXCHANGE_1CFILES_PROCESSING'] = 'Файл processing.ert<br/>(Файл выполняет процедуры обмена данными с сайтом.<br/><b><font color="red">Модификация данного файла может привести к сбоям в процедуре обмена</font></b>)';
$MESS['V77EXCHANGE_1CFILES_MANUAL'] = 'Скачайте данные файлы на локальный диск и разместите в папке ExtForms Вашей базы данных';
$MESS['V77EXCHANGE_1CFILES_7ZA'] = 'Файл 7za.exe<br/>(Файл используется для сжатия файлов, передаваемых на сайт)';
//v2.0.1
$MESS['V77EXCHANGE_IMG_IMPORT_SETTINGS'] = 'Настройки импорта изображений';
$MESS['V77EXCHANGE_IMG_IMPORT_FOLDER'] = 'Путь к папке изображений относительно корня';
$MESS['V77EXCHANGE_IMG_IMPORT_TYPES'] = 'Типы файлов';
$MESS['V77EXCHANGE_IMG_IMPORT_TYPES_HINT'] = 'например: jpg,jpeg,png,gif<br/>Типы файлов указываются через запятую маленькими буквами';
$MESS['V77EXCHANGE_IMG_IMPORT_METHOD'] = 'Способ импорта изображений';
$MESS['V77EXCHANGE_IMG_IMPORT_SYS'] = 'Системные поля 1С-Битрик';
$MESS['V77EXCHANGE_IMG_IMPORT_CUSTOM'] = 'Пользовательские поля';
$MESS['V77EXCHANGE_IMG_IMPORT_METHOD_HINT'] = "При использовании системных полей 1С-Битрикс, картинка будет удалена из папки после загрузки\обвноления элемента";
$MESS['V77EXCHANGE_IP_LIST_HINT'] = 'Список IP-адресов, с которых возможен обменЮ через запятую. <br/>Например, 127.0.0.1,127.0.0.2<br/>Необязательный параметр.';
//v2.0.9
$MESS['V77EXCHANGE_IMG_IMPORT_LOG'] = 'Вывести список товаров, у которых нет изображений';
$MESS['V77EXCHANGE_IMG_IMPORT_LOG_HINT'] = "Файл с элементами будет доступен по адресу /bitrix/tmp/htmls.1c77exchange/noimg.csv";

//15.11.2012
$MESS['V77EXCHANGE_CATALOG_STRUCTURE_IMPORT_SETTINGS'] = 'Настройка импорта структуры каталога';
$MESS['V77EXCHANGE_KEEP_LOCAL_CATALOG_STRUCTURE'] = 'Сохранять пользовательскую структуру каталога товаров';
$MESS['V77EXCHANGE_CATALOG_STRUCTURE_IMPORT_HINT'] = 'При выключенной опции каталог будет повторять иерархию 1С. <hr>При включении опции на сайте будут обновляться все реквзиты, кроме родительской секции. Все новые позии будут записаны в папку "Новые товары" в корневом разделе каталога.';

//05.01.2013
$MESS['V77EXCHANGE_USER_FUNCTION'] = 'Вызов пользовательских функций при добавлении и обновлении элемента';
$MESS['V77EXCHANGE_USER_FUNCTION_AFTER_ADD'] = 'Событие "OnAfterIBlockElementAdd"';
$MESS['V77EXCHANGE_USER_FUNCTION_AFTER_UPDATE'] = 'Событие "OnAfterIBlockElementUpdate"';

//24.01.2013
$MESS['V77EXCHANGE_ORDER_STATUS_FROM_1C'] = 'После загрузки зказа в 1С установить статус';
$MESS['V77EXCHANGE_ORDER_STATUS_FROM_1C_NO'] = 'не изменять статус';

//05.03.2013
$MESS['V77EXCHANGE_IMG_IMPORT_DELETE'] = 'Удалять картинки после импорта';
$MESS['V77EXCHANGE_CATALOG_UPDATE_NAME'] = 'Обновлять наименование при импорте';

//06.10.2013
$MESS['V77EXCHANGE_BUSINESS_SETTINGS'] = 'Настройка расширенного обмена с 1С:Предприятие 7.7';
$MESS['V77EXCHANGE_USE_STORE'] = 'Распределять остатки по складам';

//20.10.2013
$MESS['V77EXCHANGE_TAB_1CEXCHANGE'] = 'Интеграция с 1С';
$MESS['V77EXCHANGE_TAB_1CEXCHANGE_ALT'] = 'Интеграция с "1С:Предприятие"';

$MESS["CAT_1C_CREATE"] = "Создать по необходимости";
$MESS["CAT_1C_CURRENT"] = "Текущий";
$MESS["CAT_1C_NONE"] = "ничего";
$MESS["CAT_1C_DEACTIVATE"] = "деактивировать";
$MESS["CAT_1C_DELETE"] = "удалить";
$MESS["CAT_1C_IBLOCK_TYPE"] = "Тип инфо-блока";
$MESS["CAT_1C_ELEMENT_ACTION"] = "Что делать с элементами, отсутствующими в файле импорта";
$MESS["CAT_1C_SECTION_ACTION"] = "Что делать с разделами, отсутствующими в файле импорта";
$MESS["CAT_1C_GROUP_PERMISSIONS"] = "Разрешить загрузку группам пользователей";
$MESS["CAT_1C_SITE_LIST"] = "Привязывать вновь создаваемые инфоблоки к сайтам";
$MESS["CAT_1C_USE_ZIP"] = "Использовать сжатие zip, если доступно";
$MESS["CAT_1C_USE_IBLOCK_TYPE_ID"] = "При выгрузке учитывать тип инфоблока";
$MESS['V77EXCHANGE_CATALOG_IMPORT_SETTINGS'] = 'Настройка параметров импорта каталога';

$MESS['CAT_1C_SKIP_ROOT_SECTION'] = 'Не импортировать корневой раздел инфоблока, если он единственный';

$MESS['CAT_1C_PREVIEW_WIDTH'] = 'Максимально допустимая ширина картинки анонса';
$MESS['CAT_1C_PREVIEW_HEIGHT'] = 'Максимально допустимая высота картинки анонса';
$MESS['CAT_1C_DETAIL_WIDTH'] = 'Максимально допустимая ширина детальной картинки';
$MESS['CAT_1C_DETAIL_HEIGHT'] = 'Максимально допустимая высота детальной картинки';

$MESS['V77EXCHANGE_GET_ORDERS_BY'] = 'Загружать заказы в 1С по';
$MESS['DATE_INSERT'] = 'дате создания (загружать только новые заказы)';
$MESS['DATE_UPDATE'] = 'дате изменения (загружать новые и измененные заказы)';

$MESS['V77EXCHANGE_USE_SKU'] = 'Загружать торговые предложения в отдельный инфоблок:';

$MESS['V77EXCHANGE_IMG_IMPORT_SUBFOLDER'] = 'Свойсто инфоблока, в котором хранится относительный путь к артинке:';
$MESS['V77EXCHANGE_IMG_IMPORT_SUBFOLDER_HINT'] = 'Если в 1С используется описание пути к картинке с помощью двух реквизитов относительный путь+имя картинки, укажите в этом поле название свойства инфоблока, в котором храниится относительный путь. Иначе оставбте это поле незаполненным.';
$MESS['V77EXCHANGE_IMG_IMPORT_MULTI'] = 'Загрузка нескольких картинок:';
$MESS['V77EXCHANGE_IMG_IMPORT_MULTI_HINT'] = 'Все картинки одного товара хранятся в папке с именем, указанным в настройках обмена на стороне 1С в качестве реквизита "Картинка". Имена картинок состоят из цифр 1, 2, 3, и т.д. Каринка с именем 1 загружается в качестве основного изображения. Остальные картинки загружаются в свойство MORE_PHOTO';

$MESS['V77EXCHANGE_CATALOG_USE_BASE64'] = 'Использовать Base64 кодирование для сохранения html-кода в описании товара';
$MESS['1C_EXPORT_FINAL_ORDERS'] = 'Выгружать заказы начиная со статуса';
$MESS['1C_EXPORT_FINAL_ORDERS_ALL'] = '<Не выбрано>';
$MESS['V77EXCHANGE_SHIPMENT_XML_ID'] = 'Код товара в 1С для добавления доставки в табличную часть заказа';
?>