<?
$MESS["SALE_MODULE_IS_NOT_INSTALLED"] = "Не установлен модуль Интернет-магазин";
$MESS["V77EXCHANGE_MODULE_DEMO_EXPIRED"] = "Срок работы демо-режима модуля 1Cv77Exchange истек.";
$MESS["CC_BSC1_PERMISSION_DENIED"] = "У Вас нет прав для импорта каталога. Проверьте настройки компонента импорта.";
$MESS["CC_BSC1_ERROR_MODULE"] = "Модуль Информационных блоков не установлен.";
?>
