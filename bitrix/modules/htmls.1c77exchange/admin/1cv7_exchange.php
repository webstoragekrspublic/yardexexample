<?
define('BX_SESSION_ID_CHANGE', false);
define('BX_SKIP_POST_UNQUOTE', true);
define("NO_AGENT_CHECK", true);
define("NO_AGENT_STATISTIC", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//error_reporting(E_ALL);
?>
<?
ob_start();

IncludeModuleLangFile(__FILE__);

$res = CModule::IncludeModuleEx('htmls.1c77exchange');
//echo $res;
if($res == 3) die(utf2win($MESS["V77EXCHANGE_MODULE_DEMO_EXPIRED"]));

if($_REQUEST['mode'] == 'init'){
	$res = COption::GetOptionString("catalog", "1C_USE_ZIP", "Y");
    if(CModule::IncludeModuleEx("htmls.1c77plus") < 3){
    	$res .= '@' . COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_USE_STORE", "N");;
	}
	else{
		$res .= '@N';
	}
    $res .= '@' . COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_USE_SKU", "N");
    $res .= '@' . COption::GetOptionString("catalog", "1C_SKIP_ROOT_SECTION", "N");
    $res .= '@' . COption::GetOptionString("htmls.1c77exchange", "V77EXCHANGE_CATALOG_USE_BASE64", "N");
    echo $res;
}
if($_REQUEST['type'] == 'catalog'){
	CModule::IncludeModule('iblock');
	CModule::IncludeModule('catalog');

	$V7Exchange = new CV7ExchangeProcessing;

	if($_REQUEST['mode'] == 'checkauth'){
		$V7Exchange->Auth();
	}

	if($_REQUEST['mode'] == 'file'){
		$V7Exchange->Upload();
	}

	if($_REQUEST['mode'] == 'import'){
		$file = $_REQUEST['filename'];
		if($file == 'file'){
			$V7Exchange->ImportOffLine();
		}
		else{
			$V7Exchange->Import();
		}
	}
}
if($_REQUEST['type'] == 'sale'){
	CModule::IncludeModule('sale');

	$V7Exchange = new CV7ExchangeProcessing;

	if($_REQUEST['mode'] == 'checkauth'){
		$V7Exchange->Auth();
	}

	if($_REQUEST['mode'] == 'query'){
		$V7Exchange->Export();
	}

	if($_REQUEST['mode'] == 'status'){
		$V7Exchange->SetStatus();
	}

    if ($_REQUEST['mode'] == 'file') {
        $V7Exchange->Upload();
    }
    if ($_REQUEST['mode'] == 'import') {
        $V7Exchange->ImportOrders();
    }

    if ($_REQUEST['mode'] == 'import_profiles') {
        $V7Exchange->ImportProfiles();
    }

}

$today = date("d.m.y");
$filename = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/htmls.1c77exchange/admin/logs/' . $today . '.txt';
$response = ob_get_contents();

$entityBody = file_get_contents('php://input', false, null, 0, 5242885);
if (strlen($entityBody) > 5242880) {
	$entityBody = 'больше 5 МБ';
} else {
	// parse_str($entityBody, $entityBody);	
}

try {
	$curTime = date("H:i:s");
	$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	$headers =getallheaders();

	$todayLogs = [
		'TIME' => $curTime,
		'TYPE' => $request->isPost() ? 'POST' : 'GET',
		'HEADERS' => $headers,
		'PARAMS' => $_REQUEST,
		'REQUEST_BODY' => $entityBody,
		'ANSWER' => $response
	];
	foreach ($todayLogs as $key => $value) {
		file_put_contents($filename, $key . '=' . json_encode($value) . "\n", FILE_APPEND);
	}
	file_put_contents($filename, "-----------------------------------------\n", FILE_APPEND);
	 
} catch (\Throwable $th) {
	//throw $th;
}

	function utf2win($str) {
		if(strtoupper(LANG_CHARSET) == 'UTF-8')
			return iconv('UTF-8', 'WINDOWS-1251', $str);
		else
			return $str;
	}
?>

<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
