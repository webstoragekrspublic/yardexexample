<?
global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-18);
include(GetLangFileName($strPath2Lang."/lang/", "/install/index.php"));

Class htmls_1c77exchange extends CModule
{
	var $MODULE_ID = "htmls.1c77exchange";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	function htmls_1c77exchange()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		else
		{
			$this->MODULE_VERSION = V77EXCHANGE_VERSION;
			$this->MODULE_VERSION_DATE = V77EXCHANGE_VERSION_DATE;
		}

		$this->MODULE_NAME = GetMessage("V77EXCHANGE_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("V77EXCHANGE_MODULE_DESC");
        
		$this->PARTNER_NAME = 'HTMLStudio';
		$this->PARTNER_URI = "http://www.htmls.ru";

	}

	function InstallDB()
	{
                global $DB, $DBType, $APPLICATION, $install_smiles;

                if (!empty($errors))
                {
                    $APPLICATION->ThrowException(implode("", $errors)); 
                    return false;
                }
		RegisterModule("htmls.1c77exchange");

		return true;
	}

	function UnInstallDB()
	{
	        global $DB, $DBType, $APPLICATION;

	            if (!empty($errors))
        	    {
                	$APPLICATION->ThrowException(implode("", $errors)); 
	                return false;
        	    }
		UnRegisterModule("htmls.1c77exchange");

		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.1c77exchange/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/htmls.1c77exchange/install/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
		return true;
	}

	function DoInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->InstallDB();
		$this->InstallFiles();
		$APPLICATION->IncludeAdminFile(GetMessage("V77EXCHANGE_INSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/htmls.1c77exchange/install/step.php");
	}

	function DoUninstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallFiles();
		$this->UnInstallDB();
		$APPLICATION->IncludeAdminFile(GetMessage("V77EXCHANGE_UNINSTALL_TITLE"), $DOCUMENT_ROOT."/bitrix/modules/htmls.1c77exchange/install/unstep.php");
	}
}
?>