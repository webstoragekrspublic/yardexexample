<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

$paySystemsResult = \Bitrix\Sale\PaySystem\Manager::getList(array(
    'order' => ['ACTIVE' => 'DESC', 'ID' => 'DESC']
));
$paySystemsOptions = [];
while ($row = $paySystemsResult->fetch()) {
    $paySystemsOptions[] = [
        'name' => $row['NAME'],
        'value' => $row['ID'],
        'active' => $row['ACTIVE'],
    ];
}

$deliverySystemsResult = \Bitrix\Sale\Delivery\Services\Table::getList(array(
    'order' => ['ACTIVE' => 'DESC', 'ID' => 'DESC']
));
$deliverySystemsOptions = [];
while ($row = $deliverySystemsResult->fetch()) {
    $deliverySystemsOptions[] = [
        'name' => $row['NAME'],
        'value' => $row['ID'],
    ];
}

$statusResult = \Bitrix\Sale\Internals\StatusLangTable::getList(array(
    'order' => array('STATUS.SORT' => 'ASC'),
    'filter' => array('STATUS.TYPE' => 'O', 'LID' => LANGUAGE_ID),
));

$statusOptions = [];
while ($row = $statusResult->fetch()) {
    $statusOptions[] = [
        'name' => $row['NAME'],
        'value' => $row['STATUS_ID'],
        'active' => $row['ACTIVE'],
    ];
}


$fields = [
    [
        'name' => 'idOrder',
        'label' => 'ID заказа',
        'type' => 'text'
    ],
    [
        'name' => 'userName',
        'label' => 'Имя заказчика',
        'type' => 'text'
    ],
    [
        'name' => 'userLastName',
        'label' => 'Фамилия заказчика',
        'type' => 'text'
    ],
    [
        'name' => 'deliveryDate',
        'label' => 'Дата доставки',
        'type' => 'date_interval'
    ],
    [
        'name' => 'insertDate',
        'label' => 'Дата создания',
        'type' => 'date_interval'
    ],
    [
        'name' => 'paySystems',
        'label' => 'Тип оплаты',
        'type' => 'select_multi',
        'selectOptions' => $paySystemsOptions
    ],
    [
        'name' => 'deliverySystems',
        'label' => 'Способ доставки',
        'type' => 'select_multi',
        'selectOptions' => $deliverySystemsOptions
    ],
    [
        'name' => 'statusSystems',
        'label' => 'Статус заказа',
        'type' => 'select_multi',
        'selectOptions' => $statusOptions
    ],
    [
        'name' => 'payed',
        'label' => 'Оплачен',
        'type' => 'select',
        'selectOptions' => [
            [
                'name' => 'все',
                'value' => 'ALL',
            ],
            [
                'name' => 'Да',
                'value' => 'Y',
            ],
            [
                'name' => 'Нет',
                'value' => 'N',
            ]
        ]
    ],
    [
        'name' => 'canceled',
        'label' => 'Отменен',
        'type' => 'select',
        'selectOptions' => [
            [
                'name' => 'все',
                'value' => 'ALL',
            ],
            [
                'name' => 'Да',
                'value' => 'Y',
            ],
            [
                'name' => 'Нет',
                'value' => 'N',
            ]
        ]
    ],
    [
        'name' => 'report',
        'label' => 'Вид отчета',
        'type' => 'select',
        'selectOptions' => [
            [
                'name' => 'Отчет по Заказам с Товарами',
                'value' => 'ROG',
                'selected' => true
            ],
            [
                'name' => 'Отчет по заказам',
                'value' => 'RO',
            ],
            [
                'name' => 'Отчет по вычеркам товаров',
                'value' => 'ROD',
            ]
        ]
    ],
]
?>

    <form class="reports-ajax-container" action="" method="post" id="reports-ajax-container">

        <table class="adm-filter-content-table" style="max-width: 600px">
            <tbody>

            <? foreach ($fields as $field): ?>

                <? switch ($field['type']) {
                    case 'text': ?>
                        <tr>
                            <td class="adm-filter-item-left"><?= $field['label']; ?>:</td>
                            <td class="adm-filter-item-center">
                                <div class="adm-filter-alignment">
                                    <div class="adm-filter-box-sizing">
                                        <div class="adm-input-wrap"><input type="text" name="<?= $field['name']; ?>"
                                                                           value=""
                                                                           size="10" class="adm-input"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <? break; ?>
                    <? case 'date_interval': ?>
                        <tr>
                        <td class="adm-filter-item-left"><?= $field['label']; ?>:</td>
                        <td class="adm-filter-item-center">
                            <div class="adm-calendar-block adm-filter-alignment">
                                <div class="adm-filter-box-sizing">
                                    <div class="adm-input-wrap adm-calendar-inp adm-calendar-first"
                                         style="display: inline-block;"><input type="text"
                                                                               class="adm-input adm-calendar-from"
                                                                               id="<?= $field['name']; ?>_from"
                                                                               name="<?= $field['name']; ?>_from"
                                                                               size="15"
                                                                               value=""><span
                                                class="adm-calendar-icon" title="Нажмите для выбора даты"
                                                onclick="BX.calendar({node:this, field:'<?= $field['name']; ?>_from', form: '', bTime: false, bHideTime: false});">

                                            </span>
                                    </div>
                                    <span class="adm-calendar-separate" style="display: inline-block;"></span>
                                    <div class="adm-input-wrap adm-calendar-second" style="display: inline-block;">
                                        <input
                                                type="text" class="adm-input adm-calendar-to"
                                                id="<?= $field['name']; ?>_to" name="<?= $field['name']; ?>_to"
                                                size="15"
                                                value=""><span class="adm-calendar-icon" title="Нажмите для выбора даты"
                                                               onclick="BX.calendar({node:this, field:'<?= $field['name']; ?>_to', form: '', bTime: false, bHideTime: false});"></span>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <? break; ?>
                    <? case 'date_choice': ?>
                        <tr>
                        <td class="adm-filter-item-left"><?= $field['label']; ?>:</td>
                        <td class="adm-filter-item-center">
                            <div class="adm-calendar-block adm-filter-alignment">
                                <div class="adm-filter-box-sizing">
                                    <div class="adm-input-wrap adm-calendar-inp adm-calendar-first"
                                         style="display: inline-block; max-width: 100%;"><input type="text"
                                                                               class="adm-input adm-calendar-from"
                                                                               id="<?= $field['name']; ?>"
                                                                               name="<?= $field['name']; ?>"
                                                                               size="25"
                                                                               value=""><span
                                                class="adm-calendar-icon" title="Нажмите для выбора даты"
                                                onclick="BX.calendar({node:this, field:'<?= $field['name']; ?>', form: '', bTime: false, bHideTime: false});">

                                            </span>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <? break; ?>
                    <? case 'select_multi': ?>
                        <tr>
                            <td class="adm-filter-item-left"><?= $field['label']; ?>:</td>
                            <td class="adm-filter-item-center">
                                <div class="adm-filter-alignment">
                                    <div class="adm-filter-box-sizing">
                                        <span class="adm-select-wrap-multiple">
                                            <select name="<?= $field['name']; ?>[]" multiple="" size="5"
                                                    class="adm-select-multiple">
                                                <? foreach ($field['selectOptions'] as $option): ?>
                                                    <option <?= $option['value'] ? 'value="' . htmlspecialchars($option['value']) . '"' : 'value'; ?>"
                                                            <?= $option['selected'] ? 'selected="selected"' : ''; ?>
                                                    >
                                                        [<?= $option['value']; ?>] <?= $option['name']; ?> <?= ($option['active'] == 'N') ? '(не активно)' : ''; ?>
                                                    </option>
                                                <? endforeach; ?>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <? break; ?>
                    <? case 'select': ?>

                        <tr>
                            <td class="adm-filter-item-left"><?= $field['label']; ?>:</td>
                            <td class="adm-filter-item-center">
                                <div class="adm-filter-alignment">
                                    <div class="adm-filter-box-sizing">
                                        <span class="adm-select-wrap">
                                            <select name="<?= $field['name']; ?>" class="adm-select">
                                                <? foreach ($field['selectOptions'] as $option): ?>
                                                    <option <?= $option['value'] ? 'value="' . htmlspecialchars($option['value']) . '"' : 'value'; ?>"
                                                            <?= $option['selected'] ? 'selected="selected"' : ''; ?>
                                                    >
                                                        [<?= $option['value']; ?>] <?= $option['name']; ?>
                                                    </option>
                                                <? endforeach; ?>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <? break; ?>
                    <? default: ?>
                        <tr>
                            <td class="adm-filter-item-left">
                                нет обработчика!!! <?= $field['type']; ?>
                            </td>
                        </tr>
                        <? break; ?>
                    <? } ?>

                <tr>
                    <td colspan="3" class="delimiter">
                        <div class="empty"></div>
                    </td>
                </tr>
            <? endforeach; ?>


            </tbody>
        </table>

        <div class="adm-filter-bottom">
            <input type="submit" class="adm-btn" value="Скачать">
        </div>
    </form>
    <p class = "amount-orders">Суммарное количество заказов: <span class = "amount-orders-value"></span></p>

    <script>

        $("#reports-ajax-container").submit(function (event) {
            var $form = $(this);
            event.preventDefault();

            $.ajax({
                url: '/local/php_interface/helpers/ReportsAdmin/ajax/reports.php',
                data: $(this).serialize(),
                type: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    $form.find('input,textarea,select').attr('disabled', 'disabled');
                },
                success: function (Data) {
                    if (Data.error) {
                        modalError(Data.error_text);
                    } else {
                        // Проверка на вид отчета
                        $(".amount-orders").css("display", "none");
                        if(($("select[name = 'report'] option:selected").val() == "ROD") && (Data.count_orders != '')) {
                            $(".amount-orders").css("display", "block");
                            $(".amount-orders-value").html(Data.count_orders);
                        }
                        downloadCSVString({filename: 'Отчет.csv'});
                    }

                    $form.find('input,textarea,select').removeAttr('disabled');

                    function downloadCSVString(args) {
                        var string = Data.answer
                        if (!string || string == '') {
                            modalError("нет данных для файла.");
                            return
                        }

                        var filename = args.filename || 'sale_order.csv';

                        string = 'data:text/csv;charset=utf-8,' + '\uFEFF' + encodeURIComponent(string);
                        var link = document.createElement('a');
                        link.setAttribute('href', string);
                        link.setAttribute('download', filename);
                        link.click();
                    }

                },
                error: function () {
                    modalError('Произошла ошибка, попробуйте повторить действие позже.');
                    $form.find('input,textarea,select').removeAttr('disabled');
                }
            });
        });


    </script>

<?php
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');
?>