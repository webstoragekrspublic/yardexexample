<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

global $APPLICATION;
IncludeModuleLangFile(__FILE__);

$products = CIBlockElement::getList(
    [],
    ['IBLOCK_CODE' => 'NOTIFY_BASKET_FORM_ADMIN'],
    false,
    false,
    []
);
?>
<link rel="stylesheet" type="text/css" href="/local/templates/aspro_next_custom/styles.css">
<link rel="stylesheet" type="text/css" href="/local/templates/aspro_next_custom/css/jquery.fancybox.css" media="screen">
<link rel="stylesheet" type="text/css" href="/bitrix/css/main/bootstrap_v4/bootstrap.min.css">
<script type="text/javascript" src="/bitrix/js/main/jquery/jquery-2.1.3.min.js"></script>
<script type="text/javascript" src="/local/templates/aspro_next_custom/js/browser.js"></script>
<script type="text/javascript" src="/local/templates/aspro_next_custom/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="/bitrix/js/ui/bootstrap4/js/bootstrap.min.js"></script>



<div class="notifications-ajax-container" id="notifications-ajax-container">
<?php
$iter = 1;
while($item = $products->getNextElement()){
    $fields = $item->getFields();
    $id =$fields['ID'];
    $title = $fields['NAME'];
    $fields['ACTIVE'] == 'Y' ? $active = 'yes': $active = 'no';
    $product = $item->getProperties();
    ?>
        <div class="notifications-ajax-item"
             style="<?if($active == 'yes')
             {echo 'background-color:#21ff0030;';}
             elseif ($active == 'no')
             {echo 'background-color:#ffc30087;';};
             ?>">
            <div class="notifications-ajax-number col-2">
                <input type="checkbox" id="<?=$id?>">
                <?=$iter?>
            </div>
            <div class="notifications-ajax-title col-4">
                <?=$title?>
            </div>
            <div class="notifications-ajax-text col-4">
                <?=$product['TEXT_NOTIFY']['VALUE']?>
            </div>
            <button type="button" class="btn btn-outline-success" data-selected="<?=$active?>" data-href="<?=$product['TEXT_HREF']['VALUE']?>" data-title="<?=$title?>" data-text="<?=$product['TEXT_NOTIFY']['VALUE']?>" data-toggle="modal" data-id="<?=$id?>" data-target="#updateButton" class="col-2" style="margin:20px;">
                изменить
            </button>
        </div>
    <?
    $iter++;
}
?>
    <div class="notifications-buttons-control">
        <!-- Button modal Add -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addButton">
            Добавить уведомление
        </button>
        <!-- Button Delete -->
        <button type="button" class="btn btn-primary disabled"  id="deleteButton" tabindex="-1" role="button" aria-disabled="true">Удалить выбранное</button>
    </div>
</div>

<!-- Modal add -->
<div class="modal fade" id="addButton" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLongTitle">Добавить новое уведомление</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row">
                <textarea type="text" placeholder="Название уведомления" name="title-ntfy" id="title-ntfy" style="width:45%;resize:none;min-height:80px;margin: 20px 20px 20px 15px;"></textarea>
                <textarea type="text" placeholder="Текст уведомления" name="text-ntfy" id="text-ntfy" style="width:45%;resize:none;min-height:80px;margin: 20px 0px 20px 0px;"></textarea>
            </div>
            <div class="modal-add-href">
                <input id="modal-add-href" style="width:92%;margin:5px 4%;" placeholder="Добавить ссылку...">
            </div>
            <div class="modal-footer" style="justify-content:space-between;">
                <select class="form-control col-7" id="activeNotifyChckbx">
                    <option selected value="yes">Отображать уведомление</option>
                    <option value="no">Скрыть уведомление</option>
                </select>
                <button class="btn btn-primary" type="submit" id="saveButton" name="button" value="Submit">
                    Сохранить
                </button>
            </div>
        </div>
    </div>
</div>


<!-- Modal update -->
<div class="modal fade" id="updateButton" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalLongTitle">Изменить уведомление</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body row">
                <textarea type="text" placeholder="Название уведомления" name="title-update-ntfy" id="title-update-ntfy" style="width:45%;resize:none;min-height:80px;margin: 20px 20px 20px 15px;"></textarea>
                <textarea type="text" placeholder="Текст уведомления" name="text-update-ntfy" id="text-update-ntfy" style="width:45%;resize:none;min-height:80px;margin: 20px 0px 20px 0px;"></textarea>

            </div>
            <div class="modal-update-href">
                <input id="modal-update-href" style="width:92%;margin:5px 4%;" placeholder="Добавить ссылку...">
            </div>
            <div class="modal-footer" style="justify-content:space-between;">
                <select class="form-control col-7" id="updateActiveNotifyChckbx">
                    <option selected value="yes">Отображать уведомление</option>
                    <option value="no">Скрыть уведомление</option>
                </select>
                <button class="btn btn-primary" type="submit" id="updateButtonInModal" name="button" value="Submit">
                    Изменить
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    //активация кнопки удаления при выборе элемента
    document.querySelectorAll('input[type="checkbox"]').forEach(
        function (checkbox){
            checkbox.onclick = function (){
                delBtn = document.querySelector("#deleteButton");
                if (checkbox.checked){
                    delBtn.classList.remove('disabled');
                    delBtn.setAttribute('aria-disabled', 'false');
                }
                else
                {
                    var checkActive = false;
                    items = document.querySelectorAll('input[type="checkbox"]');
                    for (let i = 0; i < items.length; i++) {
                        if (items[i].checked == true) {
                            checkActive = true;
                        }
                    };
                    if (checkActive == false){
                        delBtn.classList.add('disabled');
                        delBtn.setAttribute('aria-disabled', 'true');
                    }
                }
            };
        }
    );

    //удаление выбранных элементов
    document.querySelector("#deleteButton").onclick = function () {
        if (this.ariaDisabled == 'false') {
            if (confirm('Вы уверены что хотите удалить выбранные элементы?')) {
                var checkboxes = document.querySelectorAll('input[type="checkbox"]');
                var checkedId = [];
                checkboxes.forEach(
                    function (item) {
                        if (item.checked) {
                            checkedId.push(item.id);
                        }
                    }
                );
                $.ajax({
                    url: "/ajax/admin/notifications/deleteNotificationsFromIB.php",
                    data: {'notify_id': checkedId},
                    success: function(){
                        $("#notifications-ajax-container").load("/bitrix/admin/notificationsFromInfoBlockGlobalMenu.php #notifications-ajax-container > *");
                    },
                    error: function() {
                        alert('Ошибка удаления')
                    }
                });
            }
        }
    };

    //Добавление уведолмения
    document.querySelector("#saveButton").onclick = function () {
        var title = document.querySelector('#title-ntfy').value;
        var text = document.querySelector('#text-ntfy').value;
        var active = document.querySelector('#activeNotifyChckbx').value;
        var href = document.querySelector('#modal-add-href').value;
        $.ajax({
            url: "/ajax/admin/notifications/addNotificationsInIB.php",
            data: {'title':title,'text':text,'active':active,'mode':'add', 'href':href},
            success: function(){
                $("#notifications-ajax-container").load("/bitrix/admin/notificationsFromInfoBlockGlobalMenu.php #notifications-ajax-container > *");
                $("#addButton").modal('hide');
            },
            error: function() {
                alert('Ошибка')
            }
        });
    };
    var idTarget = -1;
    //изменение уведомления
    $('#updateButton').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var title = button.data('title');
        var text = button.data('text');
        var href = button.data('href');
        var selected = button.data('selected');
        idTarget = button.data('id');
        var modal = $(this);
        modal.find('#title-update-ntfy').val(title);
        modal.find('#text-update-ntfy').val(text);
        modal.find('#modal-update-href').val(href);
        modal.find('#updateActiveNotifyChckbx option').prop('selected', false);
        modal.find('#updateActiveNotifyChckbx option[value='+selected+']').prop('selected', true);
    });

    document.querySelector("#updateButtonInModal").onclick = function () {
        var title = document.querySelector('#title-update-ntfy').value;
        var text = document.querySelector('#text-update-ntfy').value;
        var active = document.querySelector('#updateActiveNotifyChckbx').value;
        var href = document.querySelector('#modal-update-href').value;
        if (idTarget != -1) {
            $.ajax({
                url: "/ajax/admin/notifications/addNotificationsInIB.php",
                data: {'title': title, 'text': text, 'active': active, 'mode': 'update','id':idTarget,'href':href},
                success: function () {
                    $("#notifications-ajax-container").load("/bitrix/admin/notificationsFromInfoBlockGlobalMenu.php #notifications-ajax-container > *");
                    $("#updateButton").modal('hide');
                },
                error: function () {
                    alert('Ошибка')
                }
            });
        }
    };
</script>

<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
?>