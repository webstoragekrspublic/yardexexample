<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php');
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

global $APPLICATION;
IncludeModuleLangFile(__FILE__);

$sitesDB = CSite::getList(
    $by='sort',
    $order='asc',
    []
    );
if ($site = $sitesDB->fetch()){
    $sites[$site['LID']] = $site['SITE_NAME'];
}

$products = \Bitrix\Sale\Internals\DiscountTable::getList(
    ['filter'=>['XML_ID'=>'PROGRESS_BAR%', 'ACTIVE'=>'Y'],
        'select'=>['ACTIONS_LIST']]
);

$iBlockID = 114;
$arrId = [];

while($item = $products->fetch()) {
    foreach ($item['ACTIONS_LIST']['CHILDREN'] as $childrenAct) {
        if ($childrenAct['CLASS_ID'] == 'ActSaleBsktGrp') {
            foreach ($childrenAct['CHILDREN'] as $isProdact) {
                if ($isProdact['CLASS_ID'] == 'CondBsktFldProduct') {
                    $arrId += [$isProdact['DATA']['value'] => $isProdact['DATA']['value']];
                }
            }
        }
    }
}

if ($arrId) {
    $products = CIBlockElement::getList(
        [],
        ['IBLOCK_ID' => $iblockID, 'ID' => $arrId],
        false,
        false,
        ['select' => 'PREVIEW_PICTURE', 'ID', 'NAME']
    );
    while ($item = $products->fetch()) {
        $arrId[$item['ID']] = ['PREVIEW_PICTURE' => $item['PREVIEW_PICTURE'], 'NAME' => $item['NAME']];
    }
}

$products = \Bitrix\Sale\Internals\DiscountTable::getList(
    ['filter'=>['XML_ID'=>'PROGRESS_BAR%', 'ACTIVE'=>'Y'],
        'select'=>['ACTIVE_FROM','ACTIVE_TO','ACTIVE','NAME','SORT', 'PRIORITY','LAST_DISCOUNT','LAST_LEVEL_DISCOUNT','LID','ID','USE_COUPONS','XML_ID','CONDITIONS_LIST','ACTIONS_LIST']]
);
?>
    <link rel="stylesheet" type="text/css" href="/local/templates/aspro_next_custom/styles.css">
    <link rel="stylesheet" type="text/css" href="/local/templates/aspro_next_custom/css/jquery.fancybox.css" media="screen">
    <link rel="stylesheet" type="text/css" href="/bitrix/css/main/bootstrap_v4/bootstrap.min.css">
    <script type="text/javascript" src="/bitrix/js/main/jquery/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="/local/templates/aspro_next_custom/js/browser.js"></script>
    <script type="text/javascript" src="/local/templates/aspro_next_custom/js/jquery.fancybox.js"></script>
    <script type="text/javascript" src="/bitrix/js/ui/bootstrap4/js/bootstrap.min.js"></script>



    <div class="admin-basket-rule-ajax-container" id="admin-basket-rule-ajax-container">
        <?php
        $iter = 1;
        while($item = $products->fetch()){
            $nameSale = $item['NAME'];
            $lidSiteName = '(' . $item['LID'] . ') ' .$sites[$item['LID']];//id сайта + название
            $id = $item['ID'];
            $item['ACTIVE'] == 'Y' ? $active = 'yes': $active = 'no';
            foreach ($item['CONDITIONS_LIST']['CHILDREN'] as $children){
                if ($children['CLASS_ID'] == 'CondBsktAmtGroup'){
                    $sum = $children['DATA']['Value'];
                }
            }
            foreach ($item['ACTIONS_LIST']['CHILDREN'] as $childrenAct) {
                if ($childrenAct['CLASS_ID'] == 'ActSaleBsktGrp') {
                    foreach ($childrenAct['CHILDREN'] as $isProdact) {
                        if ($isProdact['CLASS_ID'] == 'CondBsktFldProduct'){
                            $itemArrId = $isProdact['DATA']['value'];
                        }
                    }
                }
            }
            $imgId = $arrId[$itemArrId]['PREVIEW_PICTURE'];
            $productName = $arrId[$itemArrId]['NAME'];
            $imgSrc = \Helpers\CustomTools::getResizedPictSrc($imgId, ['width' => 100, 'height' => 100]);
            ?>
            <div class="admin-basket-rule-ajax-item"
                 style="<?if($active == 'yes')
                 {echo 'background-color:#21ff0030;';}
                 elseif ($active == 'no')
                 {echo 'background-color:#ffc30087;';};
                 ?>">
                <div class="admin-basket-rule-ajax-item-container">
                    <div class="admin-basket-rule-ajax-block col-9">
                        <div class="rule-sale-name-site">
                            <?= $nameSale?>
                        </div>
                        <div class="rule-sale-id-site">
                            <?= $lidSiteName?>
                        </div>
                        <?if($item['ACTIVE_FROM'] !== null || $item['ACTIVE_TO'] !== null):?>
                        <div class="rule-sale-period">
                            <div class="rule-sale-period-start">
                                <?=$item['ACTIVE_FROM']?>
                            </div>
                            <div class="rule-sale-period-end">
                                <?=$item['ACTIVE_TO']?>
                            </div>
                        </div>
                        <div class="priority">
                            <div class="">
                                Приоритет применимости: <?= $item['PRIORITY']?>
                            </div>
                            <div class="">
                                Индекс сортировки в уровне приоритета: <?= $item['SORT']?>
                            </div>
                            <div class="">
                                Прекратить применение скидок на текущем уровне приоритетов: <?= $item['LAST_DISCOUNT'] == 'Y' ? 'да' : 'нет'?>
                            </div>
                            <div class="">
                                Прекратить дальнейшее применение правил: <?= $item['LAST_LEVEL_DISCOUNT'] ? 'нет' : 'да'?>
                            </div>
                        </div>
                        <?endif;?>
                    </div>
                    <div class="basket-rule-ajax-number col-2">
                        <input type="checkbox" id="<?=$id?>">
                        <?=$iter?>.&nbsp
                    </div>
                </div>
                <div class="basket-rule-ajax-sum col-2">
                    <?=$sum?>
                </div>
                <div class="image-rule-basket-in-admin column col-3">
                    <div class="product-name"><?=$productName?></div>
                    <img src="<?=$imgSrc?>" id="<?=$itemArrId?>"/>
                </div>
                <button type="button" class="btn btn-outline-success" data-selected="<?=$active?>" data-sum="<?=$sum?>" data-name="<?=$nameSale?>" data-toggle="modal" data-id="<?=$id?>" data-target="#updateButton" class="col-2" style="margin:20px;">
                    изменить
                </button>
            </div>
            <?
            $iter++;
        }
        ?>
        <div class="notifications-buttons-control">
            <!-- Button modal Add -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addButton">
                Добавить уведомление
            </button>
            <!-- Button Delete -->
            <button type="button" class="btn btn-primary disabled"  id="deleteButton" tabindex="-1" role="button" aria-disabled="true">Удалить выбранное</button>
        </div>
    </div>

    <!-- Modal add -->
    <div class="modal fade" id="addButton" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLongTitleAdd">Добавить новое правило</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body label row" style="padding-top:0px;padding-bottom:0px;">
                    <label style="width:45%;margin: 0px 20px 0px 15px;">Название:</label>
                    <label style="width:45%;margin: 0px 0px 0px 0px;">Сумма корзины для скидки:</label>
                </div>
                <div class="modal-body row" style="padding-top:0px;">
                    <textarea type="text" placeholder="Название правила корзины" name="name-save-rule" id="name-save-rule" style="width:45%;resize:none;min-height:40px;margin: 10px 20px 10px 15px;"></textarea>
                    <textarea type="text" placeholder="Сумма корзины на которую идет скидка" name="sum-save-rule" id="sum-save-rule" style="width:45%;resize:none;min-height:40px;margin: 10px 0px 10px 0px;"></textarea>
                </div>
                <button type="button" class="btn btn-outline-success" data-product="<?=1?>" id="productButtonAdd" class="col-2" style="margin:20px;">
                    Изменить товар
                </button>
                <div class="modal-footer" style="justify-content:space-between;">
                    <select class="form-control col-7" id="activeRuleChckbx">
                        <option selected value="yes">Отображать бонус</option>
                        <option value="no">Скрыть бонус</option>
                    </select>
                    <button class="btn btn-primary" type="submit" id="saveButton" name="button" value="Submit">
                        Сохранить
                    </button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal update -->
    <div class="modal fade" id="updateButton" tabindex="-1" role="dialog" aria-labelledby="ModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLongTitleUpdate">Изменить правило</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body label row" style="padding-top:0px;padding-bottom:0px;">
                    <label style="width:45%;margin: 0px 20px 0px 15px;">Название:</label>
                    <label style="width:45%;margin: 0px 0px 0px 0px;">Сумма корзины для скидки:</label>
                </div>
                <div class="modal-body row" style="padding-top:0px;">
                    <textarea type="text" placeholder="Название правила корзины" name="name-update-rule" id="name-update-rule" style="width:45%;resize:none;min-height:40px;margin: 10px 20px 10px 15px;"></textarea>
                    <textarea type="text" placeholder="Сумма корзины на которую идет скидка" name="sum-update-rule" id="sum-update-rule" style="width:45%;resize:none;min-height:40px;margin: 10px 0px 10px 0px;"></textarea>
                </div>
                <button type="button" class="btn btn-outline-success" data-product="<?=1?>" id="productButton" class="col-2" style="margin:20px;">
                    Изменить товар
                </button>
                <div class="modal-footer" style="justify-content:space-between;">
                    <select class="form-control col-7" id="updateActiveRuleChckbx">
                        <option selected value="yes">Отображать бонус</option>
                        <option value="no">Скрыть бонус</option>
                    </select>
                    <button class="btn btn-primary" type="submit" id="updateButtonInModal" name="button" value="Submit">
                        Изменить
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        //активация кнопки удаления при выборе элемента
        document.querySelectorAll('input[type="checkbox"]').forEach(
            function (checkbox){
                checkbox.onclick = function (){
                    delBtn = document.querySelector("#deleteButton");
                    if (checkbox.checked){
                        delBtn.classList.remove('disabled');
                        delBtn.setAttribute('aria-disabled', 'false');
                    }
                    else
                    {
                        var checkActive = false;
                        items = document.querySelectorAll('input[type="checkbox"]');
                        for (let i = 0; i < items.length; i++) {
                            if (items[i].checked == true) {
                                checkActive = true;
                            }
                        };
                        if (checkActive == false){
                            delBtn.classList.add('disabled');
                            delBtn.setAttribute('aria-disabled', 'true');
                        }
                    }
                };
            }
        );

        //удаление выбранных элементов
        document.querySelector("#deleteButton").onclick = function () {
            if (this.ariaDisabled == 'false') {
                if (confirm('Вы уверены что хотите удалить выбранные элементы?')) {
                    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
                    var checkedId = [];
                    checkboxes.forEach(
                        function (item) {
                            if (item.checked) {
                                checkedId.push(item.id);
                            }
                        }
                    );
                    // $.ajax({
                    //     url: "/ajax/admin/basket_rule/ruleDelete.php",
                    //     data: {'notify_id': checkedId},
                    //     success: function(){
                    //         $("#admin-basket-rule-ajax-container").load("/bitrix/admin/global_menu_basket_rule_for_admin.php #admin-basket-rule-ajax-container > *");
                    //     },
                    //     error: function() {
                    //         alert('Ошибка удаления')
                    //     }
                    // });
                }
            }
        };

        //Добавление уведолмения
        document.querySelector("#saveButton").onclick = function () {
            // var title = document.querySelector('#name-save-rule').value;
            // var sum = document.querySelector('#sum-save-rule').value;
            // var text = document.querySelector('#text-ntfy').value;
            // var active = document.querySelector('#activeRuleChckbx').value;
            // var href = document.querySelector('#modal-add-href').value;
            // $.ajax({
            //     url: "/ajax/admin/basket_rule/add_basket_rule.php",
            //     data: {'title':title,'text':text,'active':active,'mode':'add', 'href':href, 'sum':sum},
            //     success: function(){
            //         $("#admin-basket-rule-ajax-container").load("/bitrix/admin/global_menu_basket_rule_for_admin.php #admin-basket-rule-ajax-container > *");
            //         $("#addButton").modal('hide');
            //     },
            //     error: function() {
            //         alert('Ошибка')
            //     }
            // });
        };
        var idTarget = -1;
        //изменение уведомления
        $('#updateButton').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var name = button.data('name');
            var basketSum = button.data('sum');
            var selected = button.data('selected');
            idTarget = button.data('id');
            var modal = $(this);
            modal.find('#name-update-rule').val(name);
            modal.find('#sum-update-rule').val(basketSum);
            modal.find('#updateActiveRuleChckbx option').prop('selected', false);
            modal.find('#updateActiveRuleChckbx option[value='+selected+']').prop('selected', true);
        });

        document.querySelector("#updateButtonInModal").onclick = function () {
            // var name = document.querySelector('#name-update-rule').value;
            // var basketSum = document.querySelector('#sale-update-rule').value;
            // var active = document.querySelector('#updateActiveRuleChckbx').value;
            // if (idTarget != -1) {
            //     $.ajax({
            //         url: "/ajax/admin/baket_rule/add_basket_rule.php",
            //         data: {'name': name,'sum': basketSum, 'active': active, 'mode': 'update','id':idTarget},
            //         success: function () {
            //             $("#admin-basket-rule-ajax-container").load("/bitrix/admin/global_menu_basket_rule_for_admin.php #admin-basket-rule-ajax-container > *");
            //             $("#updateButton").modal('hide');
            //         },
            //         error: function () {
            //             alert('Ошибка')
            //         }
            //     });
            // }
        };
        //класс для отображения диалога выбора продуктов
        class ElementSearch {
            constructor(props){
                this.props = Object.assign({}, this.getDefaultProps(), props);
                this._dialogSearch = null;
            }
            getDefaultProps(){
                return {
                    event: 'onSelectElement',
                    lang: 'ru',
                    allow_select_parent: 'Y',
                    url: '/ajax/admin/basket_rule/product_search.php'
                }
            }
            compileUrl(){
                return BX.util.add_url_param(this.props.url, this.props);
            }
            dialogSearch() {
                this._dialogSearch = new BX.CDialog({
                    zIndex: 1052,
                    title: 'Выбор продуктов',
                    width: 1350,
                    height: 600,
                    content_url: this.compileUrl(),
                    ESD: true
                });
                this._dialogSearch.SetButtons([{
                    title: BX.message('JS_CORE_WINDOW_SAVE'),
                    id: 'savebtn',
                    name: 'savebtn',
                    className: BX.browser.IsIE() && BX.browser.IsDoctype() && !BX.browser.IsIE10() ? '' : 'adm-btn-save',
                    action: () => {
                        this._dialogSearch.Close();
                    }
                }]);
                return this;
            }
            getDialog(){
                return this._dialogSearch
            }
            getEvent(){
                return this.props.event;
            }
        }
        function showDialog(action) {
            const dialog = new ElementSearch({
                IBLOCK_ID: 114,
                url: '/ajax/admin/basket_rule/product_search.php'
            });
            console.info(dialog);
            dialog.dialogSearch().getDialog().Show();
            BX.addCustomEvent(dialog.getEvent(), (dataEvent) => {
                if (action == 'update'){

                }
                console.info(dataEvent); // данные о выбранном элементе.
                dialog.getDialog().Close(); // закроем окно
            });
        }
        //onclick для выбора товара
        document.querySelector('#productButton').addEventListener('click',function(){showDialog('update')});
        document.querySelector('#productButtonAdd').addEventListener('click',function(){showDialog('add')});
    </script>

<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php');
?>