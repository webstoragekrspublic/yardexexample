<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Context;

$iosVersion = '2.4.3';
$androidVersion = '2.4.2';


$request = Context::getCurrent()->getRequest();
$platform = $request->get('platform');

if ($platform == 'ios') {
    echo json_encode(['result' => 'ok', 'version' => $iosVersion], JSON_UNESCAPED_UNICODE);
    die();
} else {
    echo json_encode(['result' => 'ok', 'version' => $androidVersion], JSON_UNESCAPED_UNICODE);
    die();
}
echo json_encode(['result' => 'error'], JSON_UNESCAPED_UNICODE);


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
