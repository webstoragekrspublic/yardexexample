<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$SALE_IBLOCK_ID = 109;
$saleID = $_GET['sale_id'];
if (isset($_GET['sale_id'])) {
    $arSelect = array("ID", "NAME", "DETAIL_TEXT", "PROPERTY_LINK_GOODS", "PROPERTY_LINK_GOODS_FILTER");
    $arFilter = array("IBLOCK_ID" => $SALE_IBLOCK_ID, "ID" => $sale_id,);
    $itemIds = [];
    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    if ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        if (empty($itemIds)) {
            $itemIds = \Helpers\Cache::getProductsIdForCustomFilters($arFields['~PROPERTY_LINK_GOODS_FILTER_VALUE']);
        }
        $itemIds[$arFields['~PROPERTY_LINK_GOODS_VALUE']] = $arFields['~PROPERTY_LINK_GOODS_VALUE'];
    }
    $CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;
    $requestResult = [];
    $requestResult['DETAIL_TEXT'] = strip_tags($arFields['~DETAIL_TEXT']);
    $requestResult['ITEMS'] = [];
    $requestResult['CUR_PAGE'] = 0;
    $requestResult['PAGE_COUNT'] = 0;
    $requestResult['IS_WEBVIEW_SALE'] = false;
    if (empty($itemIds) && !empty($requestResult['DETAIL_TEXT'])) {
        $requestResult['IS_WEBVIEW_SALE'] = true;
    }
    if (!empty($itemIds)) {
        $arSelect = array("ID", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID", "ACTIVE");
        $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, 'ID' => $itemIds, "ACTIVE" => "Y",);

        $res = CIBlockElement::GetList(array($_GET['sort_field'] => $_GET['sort_order']), $arFilter, false, array("nPageSize" => 6, "iNumPage" => $_GET['page_num'] ?? 1), $arSelect);

        $arItems = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
            $arItem = [
                'ID' => $arFields['ID'],
                'NAME' => $arFields['NAME'],
                'ACTIVE' => $arFields['ACTIVE'],
                'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
                'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
                'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
                'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
                'RATIO' => $measure['RATIO'],
            ];
            $arItems[] = $arItem;
        }
        $requestResult['CUR_PAGE'] = $res->NavPageNomer;
        $requestResult['PAGE_COUNT'] = $res->NavPageCount;
        $requestResult['SORT_FIELDS'] = Helpers\Events::getCatalogAllowSortFields();
        $requestResult['ITEMS'] = $arItems;
    }
    // pr($requestResult);
    echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);
}




require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
