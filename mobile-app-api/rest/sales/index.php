<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$SALE_IBLOCK_ID = 109;

$arFilter = array(
    'IBLOCK_ID' => $SALE_IBLOCK_ID,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y"
);
$arSelect = array('ID', 'NAME', 'PREVIEW_PICTURE', 'ACTIVE_FROM', 'ACTIVE_TO');
$res = CIBlockElement::GetList(array("name" => "ASC"), $arFilter, false, false,  $arSelect);

$arItems = [];
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arItem = [
        'ID' => $arFields['ID'],
        'NAME' => $arFields['NAME'],
        'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 390, 'height' => 106), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
        'ACTIVE_FROM' => $arFields['ACTIVE_FROM'],
        'ACTIVE_TO' => $arFields['ACTIVE_TO'],
    ];
    $arItems[] = $arItem;
}

// pr($arItems);
echo json_encode($arItems, JSON_UNESCAPED_UNICODE);


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
