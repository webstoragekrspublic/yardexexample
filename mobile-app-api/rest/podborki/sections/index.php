<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;

$IBLOCK_ID = $request->get('iblock_id');
$podborkaID = $request->get('podborka_id');

$requestResult = [];

if (isset($podborkaID)) {
    $itemIds = [];
    if ($IBLOCK_ID != 118) {
        $arSelect = array("ID", "NAME", "DETAIL_TEXT", "PROPERTY_LINK_GOODS", "PROPERTY_LINK_GOODS_FILTER");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $podborkaID,);

        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            if (empty($itemIds)) {
                $itemIds = \Helpers\Cache::getProductsIdForCustomFilters($arFields['~PROPERTY_LINK_GOODS_FILTER_VALUE']);
            }
            $itemIds[$arFields['~PROPERTY_LINK_GOODS_VALUE']] = $arFields['~PROPERTY_LINK_GOODS_VALUE'];
        }
    } else {
        $arSelect =  ['ID', 'PROPERTY_TORGOVAJAMARKA'];
        $arFilter = ['IBLOCK_ID' => $CATALOG_IBLOCK_ID, 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', '>CATALOG_QUANTITY' => 0];

        $productsQ = CIBlockElement::GetList(
            [],
            $arFilter,
            $arSelect,
            []
        );

        while ($row = $productsQ->fetch()) {
            if ($row['PROPERTY_TORGOVAJAMARKA_VALUE'] == $podborkaID) {
                $itemIds[] = $row['ID'];
            }
        };
    }
    if (!empty($itemIds)) {
        $arSelect = array("ID", "NAME", "IBLOCK_SECTION_ID", "ACTIVE",);
        $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, 'ID' => $itemIds, "ACTIVE" => "Y",);
        $res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);

        $arSections = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $arSections[] = $arFields['IBLOCK_SECTION_ID'];
        }
        $goodsCountInSections = array_count_values($arSections);

        $arFilter = array('IBLOCK_ID' => $CATALOG_IBLOCK_ID, 'ID' => $arSections);
        $arSelect = array('ID', 'NAME');
        $obSection = CIBlockSection::GetList(array("name" => "ASC"), $arFilter,  false,  $arSelect/* , array("nTopCount" => 2) */);

        $arSections = [['ID' => 0, 'NAME' => 'Все', 'COUNT' =>  array_sum($goodsCountInSections)]];
        while ($arResult = $obSection->GetNext()) {
            $arSection = [
                'ID' => $arResult['ID'],
                'NAME' => $arResult['NAME'],
                'COUNT' => 1
            ];
            $arSection['COUNT'] = $goodsCountInSections[$arSection['ID']];

            $arSections[] = $arSection;
        }
        $requestResult = $arSections;
    }
}
// pr($requestResult);
echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);




require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
