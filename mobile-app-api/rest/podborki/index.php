<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;

$IBLOCK_ID = $request->get('iblock_id');
$podborkaID = $request->get('podborka_id');
$sectionFilter = $request->get('section_filter');
$sortField = $request->get('sort_field');
$sortOrder = $request->get('sort_order');
$pageNum = $request->get('page_num');

$requestResult = [
    'NAME' => '',
    'ITEMS' => [],
    'CUR_PAGE' => 0,
    'PAGE_COUNT' => 0,
    'SORT_FIELDS' => [],
];

if (isset($podborkaID)) {
    $itemIds = [];
    if ($IBLOCK_ID != 118) {
        $arSelect = array("ID", "NAME", "DETAIL_TEXT", "PROPERTY_LINK_GOODS", "PROPERTY_LINK_GOODS_FILTER");
        $arFilter = array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $podborkaID,);

        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            if ($requestResult['NAME'] == '') {
                $requestResult['NAME'] = $arFields['NAME'];
            }
            if (empty($itemIds)) {
                $itemIds = \Helpers\Cache::getProductsIdForCustomFilters($arFields['~PROPERTY_LINK_GOODS_FILTER_VALUE']);
            }
            $itemIds[$arFields['~PROPERTY_LINK_GOODS_VALUE']] = $arFields['~PROPERTY_LINK_GOODS_VALUE'];
        }
    } else {
        $allTorgovajaMarka;
        setIndexedTorgovajaMarka();
        $arFilter = ['IBLOCK_ID' => $CATALOG_IBLOCK_ID, 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y', '>CATALOG_QUANTITY' => 0];
        $productsQ = CIBlockElement::GetList(
            [],
            $arFilter,
            ['ID', 'PROPERTY_TORGOVAJAMARKA'],
            []
        );
        if ($requestResult['NAME'] == '') {
            $requestResult['NAME'] = getTorgovajaMarkaName($podborkaID);
        }
        while ($row = $productsQ->fetch()) {
            if ($row['PROPERTY_TORGOVAJAMARKA_VALUE'] == $podborkaID) {
                $itemIds[] = $row['ID'];
            }
        };
    }
    if (!empty($itemIds)) {
        $arSelect = array("ID", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID", "ACTIVE",);
        $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, 'ID' => $itemIds, "ACTIVE" => "Y", 'IBLOCK_SECTION_ID' => $sectionFilter);

        $res = CIBlockElement::GetList(array($sortField => $sortOrder), $arFilter, false, array("nPageSize" => 6, "iNumPage" => $pageNum ?? 1), $arSelect);

        $arItems = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
            $arItem = [
                'ID' => $arFields['ID'],
                'NAME' => $arFields['NAME'],
                'ACTIVE' => $arFields['ACTIVE'],
                'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
                'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
                'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
                'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
                'RATIO' => $measure['RATIO'],
            ];
            $arItems[] = $arItem;
        }
        $requestResult['CUR_PAGE'] = $res->NavPageNomer;
        $requestResult['PAGE_COUNT'] = $res->NavPageCount;
        $requestResult['SORT_FIELDS'] = Helpers\Events::getCatalogAllowSortFields();
        $requestResult['ITEMS'] = $arItems;
    }
}
// pr($requestResult);
echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);

function setIndexedTorgovajaMarka()
{
    global $allTorgovajaMarka;
    $torgovajaMarkaIBlockId = 118;
    $columns = ['ID', 'NAME'];
    $filter = [
        "IBLOCK_ID" => $torgovajaMarkaIBlockId
    ];
    $tmQ = CIBlockElement::getList(
        ['DATE_CREATE' => 'ASC', 'ID' => 'ASC'],
        $filter,
        false,
        false,
        $columns
    );
    while ($tm = $tmQ->fetch()) {
        $allTorgovajaMarka[$tm['ID']] = $tm;
    }
}

function getTorgovajaMarkaName($tmId)
{
    global $allTorgovajaMarka;
    return $allTorgovajaMarka[$tmId]['NAME'] ?? 'Не заполнено';
}



require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
