<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$MAIN_TOP_SLIDER_IBLOCK = 125;

$imageMaxWidth = 360;
$imageMaxHeight = 180;

$arFilter = array(
    'IBLOCK_ID' => $MAIN_TOP_SLIDER_IBLOCK,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'PROPERTY_SHOW_IN_APP_VALUE' => 'Y'
);
$arSelect = array(
    'ID',
    'NAME',
    'PREVIEW_PICTURE',
    'PROPERTY_LINK_PODBORKA',
    'PROPERTY_LINK_GOODS',
    'PROPERTY_LINK_TORGOVAJAMARKA',
    'PROPERTY_LINK_SALE',
);
$res = CIBlockElement::GetList(array("sort" => "ASC"), $arFilter, false, false,  $arSelect);

$arItems = [];
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arItem = [
        'id' => $arFields['ID'],
        'name' => $arFields['NAME'],
        'image' =>  \Helpers\CustomTools::getResizedPictSrc($arFields['PREVIEW_PICTURE'], ["width" => $imageMaxWidth, "height" => $imageMaxHeight], BX_RESIZE_IMAGE_PROPORTIONAL),
        'podborka_id' => $arFields['PROPERTY_LINK_PODBORKA_VALUE'],
        'marka_id' => $arFields['PROPERTY_LINK_TORGOVAJAMARKA_VALUE'],
        'sale_id' => $arFields['PROPERTY_LINK_SALE_VALUE'],
        'catalog_item_id' => $arFields['PROPERTY_LINK_GOODS_VALUE'],

    ];
    $arItems[] = $arItem;
}

// pr($arItems);
echo json_encode($arItems, JSON_UNESCAPED_UNICODE);


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
