<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$PODBORKI_IBLOCK = 120;

$imageMaxWidth = 60;
$imageMaxHeight = 60;

$arFilter = array(
    'IBLOCK_ID' => $PODBORKI_IBLOCK,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
);
$arSelect = array(
    'ID',
    'NAME',
    'PREVIEW_PICTURE',
);
$res = CIBlockElement::GetList(array("sort" => "ASC"), $arFilter, false, false,  $arSelect);

$arItems = [];
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arItem = [
        'id' => $arFields['ID'],
        'name' => $arFields['NAME'],
        'image' =>  \Helpers\CustomTools::getResizedPictSrc($arFields['PREVIEW_PICTURE'], ["width" => $imageMaxWidth, "height" => $imageMaxHeight], BX_RESIZE_IMAGE_PROPORTIONAL),
        'items' => []
    ];
    $arItems[] = $arItem;
}

// pr($arItems);
echo json_encode($arItems, JSON_UNESCAPED_UNICODE);


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
