<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;

$PODBORKI_IBLOCK = 120;
$podborkaID = $request->get('podborka_id');


if (isset($podborkaID)) {
    $itemIds = [];
    $arSelect = array("ID", "NAME", "DETAIL_TEXT", "PROPERTY_LINK_GOODS", "PROPERTY_LINK_GOODS_FILTER");
    $arFilter = array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $podborkaID,);

    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        if ($requestResult['NAME'] == '') {
            $requestResult['NAME'] = $arFields['NAME'];
        }
        if (empty($itemIds)) {
            $itemIds = \Helpers\Cache::getProductsIdForCustomFilters($arFields['~PROPERTY_LINK_GOODS_FILTER_VALUE']);
        }
        $itemIds[$arFields['~PROPERTY_LINK_GOODS_VALUE']] = $arFields['~PROPERTY_LINK_GOODS_VALUE'];
    }
    if (!empty($itemIds)) {
        $arSelect = array("ID", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID", "ACTIVE",);
        $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, 'ID' => $itemIds, "ACTIVE" => "Y", 'IBLOCK_SECTION_ID' => $sectionFilter);

        $res = CIBlockElement::GetList(array($sortField => $sortOrder), $arFilter, false, array("nPageSize" => 6, "iNumPage" => $pageNum ?? 1), $arSelect);

        $arItems = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
            $arItem = [
                'ID' => $arFields['ID'],
                'NAME' => $arFields['NAME'],
                'ACTIVE' => $arFields['ACTIVE'],
                'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
                'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
                'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
                'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
                'RATIO' => $measure['RATIO'],
            ];
            $arItems[] = $arItem;
        }
        // pr($arItems);
        echo json_encode($arItems, JSON_UNESCAPED_UNICODE);
    }
}


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
