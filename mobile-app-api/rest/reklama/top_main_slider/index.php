<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$MAIN_TOP_SLIDER_IBLOCK = 124;

$imageMaxWidth = 400;

$arFilter = array(
    'IBLOCK_ID' => $MAIN_TOP_SLIDER_IBLOCK,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y",
    'PROPERTY_SHOW_IN_APP_VALUE' => 'Y'
);
$arSelect = array(
    'ID',
    'NAME',
    'DETAIL_PICTURE',
    'PROPERTY_HREF',
    'PROPERTY_LINK_PODBORKA',
    'PROPERTY_LINK_GOODS',
    'PROPERTY_LINK_TORGOVAJAMARKA',
    'PROPERTY_LINK_SALE',
);
$res = CIBlockElement::GetList(array("sort" => "ASC"), $arFilter, false, false,  $arSelect);

$arItems = [];
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arItem = [
        'id' => $arFields['ID'],
        'name' => $arFields['NAME'],
        'image' =>  \Helpers\CustomTools::getResizedPictSrc($arFields['DETAIL_PICTURE'], ["width" => $imageMaxWidth, "height" => $imageMaxWidth * 0.738], BX_RESIZE_IMAGE_PROPORTIONAL),
        'href' => $arFields['PROPERTY_HREF_VALUE'],
        'podborka_id' => $arFields['PROPERTY_LINK_PODBORKA_VALUE'],
        'marka_id' => $arFields['PROPERTY_LINK_TORGOVAJAMARKA_VALUE'],
        'sale_id' => $arFields['PROPERTY_LINK_SALE_VALUE'],
        'catalog_item_id' => $arFields['PROPERTY_LINK_GOODS_VALUE'],

    ];
    $arItems[] = $arItem;
}

// pr($arItems);
echo json_encode($arItems, JSON_UNESCAPED_UNICODE);


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
