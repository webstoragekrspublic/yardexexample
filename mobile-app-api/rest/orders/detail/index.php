<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Sale;
// Выведем даты всех заказов текущего пользователя за текущий месяц, отсортированные по дате заказа

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;

if (isset($_GET['order_id'])) {
    $order = Sale\Order::load($_GET['order_id']);
    $arOrderFields = [];
    $arUser =  CUser::GetByID($order->getUserId())->Fetch();
    $userFIO = $arUser['NAME'];
    if (!empty($arUser['LAST_NAME'])) {
        $userFIO .= ' ' . $arUser['LAST_NAME'];
    }
    $paymentCollection = $order->getPaymentCollection();
    $basket = $order->getBasket();
    $propertyCollection = $order->getPropertyCollection();
    $delivery = CSaleDelivery::GetByID($order->getDeliverySystemId());
    $quantity = 0;

    $arOrderFields['PAYMENT'] = [];
    $i = 1;
    foreach ($paymentCollection as $payment) {
        $onePayment  = [];
        $onePayment['ID'] = $order->getID() . '/' . $i++;
        $onePayment['IS_PAID'] = $payment->isPaid();
        $onePayment['IS_RETURN'] = $payment->isReturn();
        $onePayment['DATE_BILL'] = $payment->getField('DATE_BILL')->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "d.m.Y")));
        $onePayment['PAY_SYSTEM_NAME'] = $payment->getPaymentSystemName();
        $onePayment['PAY_SYSTEM_ID'] = $payment->getPaymentSystemId();
        $arOrderFields['PAYMENT'][] = $onePayment;
    }
    $statusResult = \Bitrix\Sale\Internals\StatusLangTable::getList(array(
        'order' => array('STATUS.SORT' => 'ASC'),
        'filter' => array('=STATUS_ID' => $order->getField('STATUS_ID')),
        'select' => array('STATUS_ID', 'NAME',),
    ));
    $statuses = [];
    while (
        $status = $statusResult->fetch()
    ) {
        $statuses[] = $status;
    }

    $arOrderFields['STATUS'] = $statuses[count($statuses) - 1];
    $arOrderFields['ID'] = $order->getID();
    $arOrderFields['PRICE'] = $payment->getSum();
    $arOrderFields['VAT_SUM'] = $order->getVatSum();
    $arOrderFields['PRICE'] = $order->getPrice();
    $arOrderFields['DELIVERY']['DATE'] = $propertyCollection->getItemByOrderPropertyId(20)->getValue();
    $arOrderFields['DELIVERY']['TIME'] = $propertyCollection->getItemByOrderPropertyId(21)->getValue();
    $arOrderFields['DELIVERY']['NAME'] = $delivery['NAME'];
    $arOrderFields['DELIVERY']['PRICE'] = $delivery['PRICE'];
    $arOrderFields['ADDRESS'] = $propertyCollection->getAddress()->getValue();
    $arOrderFields['DATE'] = $order->getDateInsert()->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "d.m.Y")));
    $arOrderFields['QUANTITY'] = count($basket->getBasketItems());
    $arOrderFields['FIO'] = $userFIO;

    foreach ($basket as $basketItem) {
        $itemID = $basketItem->getProductId();
        $itemIDs[] = $itemID;
        $itemQuantity[$itemID] = $basketItem->getQuantity();
    }

    $arSelect = array("ID", "NAME", "ACTIVE", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID",);
    $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, "ID" => $itemIDs,);

    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

    $arItems = [];
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
        $arItem = [
            'ID' => $arFields['ID'],
            'NAME' => $arFields['NAME'],
            'ACTIVE' => $arFields['ACTIVE'],
            'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
            'PREVIEW_PICTURE_SRC' =>  CFile::GetPath($arFields["PREVIEW_PICTURE"]),
            'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
            'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
            'RATIO' => $measure['RATIO'],
            'QUANTITY' => $itemQuantity[$arFields['ID']],
        ];
        $arItems[] = $arItem;
    }
    $arOrderFields['BASKET_ITEMS'] = $arItems;


    // pr($arOrderFields);
    echo json_encode($arOrderFields, JSON_UNESCAPED_UNICODE);
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
