<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Sale;

use \Bitrix\Main\Context;

$request = Context::getCurrent()->getRequest();

$arFilter = array(
    "USER_ID" => $request->get('user_id'),
);
$arSort = array("DATE_INSERT" => "DESC");
if ($request->get('awaiting_paiment') == 'Y') {
    $arFilter = array(
        "USER_ID" => $_GET['user_id'],
        'PAYED' => 'N',
        'STATUS_ID' => ['N', 'P', 'R'],
        'PAY_SYSTEM_ID' => [33, 34],
    );
    $arSort = array("DATE_INSERT" => "ASC");
}
$arSelect = [
    'ID',
    'PAY_SYSTEM_ID',
];

$dbSales = CSaleOrder::GetList($arSort, $arFilter, false, false, $arSelect);
$arOdrers = [];
while ($arOrderFields = $dbSales->Fetch()) {
    $order = Sale\Order::load($arOrderFields['ID']);
    $paymentCollection = $order->getPaymentCollection();
    $basket = $order->getBasket();
    $propertyCollection = $order->getPropertyCollection();

    $quantity = 0;
    $arOrderFields['PAYMENT'] = [];
    $i = 1;
    foreach ($paymentCollection as $payment) {
        $onePayment  = [];
        $onePayment['ID'] = $arOrderFields['ID'] . '/' . $i++;
        $onePayment['PRICE'] = $payment->getSum();
        $onePayment['IS_PAID'] = $payment->isPaid();
        $onePayment['IS_RETURN'] = $payment->isReturn();
        $onePayment['DATE_BILL'] = $payment->getField('DATE_BILL')->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "d.m.Y")));
        $onePayment['PAY_SYSTEM_NAME'] = $payment->getPaymentSystemName();
        $arOrderFields['PAYMENT'][] = $onePayment;
    }
    $statusResult = \Bitrix\Sale\Internals\StatusLangTable::getList(array(
        'order' => array('STATUS.SORT' => 'ASC'),
        'filter' => array('=STATUS_ID' => $order->getField('STATUS_ID')),
        'select' => array('STATUS_ID', 'NAME',),
    ));
    $statuses = [];
    while (
        $status = $statusResult->fetch()
    ) {
        $statuses[] = $status;
    }


    $arOrderFields['PRICE'] = $order->getPrice();
    $arOrderFields['DATE'] = $order->getDateInsert()->toString(new \Bitrix\Main\Context\Culture(array("FORMAT_DATETIME" => "d.m.Y")));

    $arOrderFields['QUANTITY'] = count($basket->getBasketItems());
    $arOrderFields['STATUS'] = $statuses[count($statuses) - 1];
    $arOdrers[] = $arOrderFields;
}
// pr($arOdrers);
echo json_encode($arOdrers, JSON_UNESCAPED_UNICODE);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
