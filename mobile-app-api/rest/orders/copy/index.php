<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Sale;

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;

if (isset($_GET['order_id'])) {
    $order = Sale\Order::load($_GET['order_id']);
    $basket = $order->getBasket();
    $itemIDs = [];
    $itemQuantity = [];
    foreach ($basket as $basketItem) {
        $itemID = $basketItem->getProductId();
        $itemIDs[] = $itemID;
        $itemQuantity[$itemID] = $basketItem->getQuantity();
    }

    $arSelect = array("ID", "ACTIVE", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID",);
    $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, "ID" => $itemIDs, "ACTIVE" => "Y");

    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

    $arItems = [];
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
        $arItem = [
            'ID' => $arFields['ID'],
            'ACTIVE' => $arFields['ACTIVE'],
            'NAME' => $arFields['NAME'],
            'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
            'PREVIEW_PICTURE_SRC' =>  CFile::GetPath($arFields["PREVIEW_PICTURE"]),
            'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
            'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
            'RATIO' => $measure['RATIO'],
            'QUANTITY' => $itemQuantity[$arFields['ID']],
        ];
        $arItems[] = $arItem;
    }
    $requestResult = $arItems;
    // pr($requestResult);
    echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
