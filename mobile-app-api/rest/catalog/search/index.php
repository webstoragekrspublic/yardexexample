<?

use ExternalApi\MultiSearch\MultiSearchApi;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$step = 50;

$sectionsFilter = json_decode($_REQUEST['section_filter']);
$searchResult = MultiSearchApi::getQueryResult($_REQUEST['q'], [], ['field' => $_REQUEST['sort_field'] ?? 'name', 'order' => $_REQUEST['sort_order'] ?? 'asc']);


$arResult = [
    'ITEMS' => [],
    'SECTIONS' => []
];

foreach ($searchResult['results']['categories'] as $section) {
    $arResult['SECTIONS'][] = ['ID' => $section['id'], 'NAME' => $section['name']];
}
if (!empty($sectionsFilter)) {
    $searchResult = MultiSearchApi::getQueryResult($_REQUEST['q'], $sectionsFilter, ['field' => $_REQUEST['sort_field'] ?? 'name', 'order' => $_REQUEST['sort_order'] ?? 'asc'], '0', $step, 0);
    if ($searchResult['results']['count'] > $step) {

        for ($offset = $step; $offset < $searchResult['results']['count']; $offset += $step) {
            $offsetIds = MultiSearchApi::getQueryResult($_REQUEST['q'], $sectionsFilter, ['field' => $_REQUEST['sort_field'] ?? 'name', 'order' => $_REQUEST['sort_order'] ?? 'asc'], '0', $step, $offset)['results']['ids'];
            $searchResult['results']['ids'] = array_merge($searchResult['results']['ids'], $offsetIds);
        }
    }
} else {
    if ($searchResult['total'] > $step) {

        for ($offset = $step; $offset < $searchResult['total']; $offset += $step) {
            $offsetIds =  MultiSearchApi::getQueryResult($_REQUEST['q'], [], ['field' => $_REQUEST['sort_field'] ?? 'name', 'order' => $_REQUEST['sort_order'] ?? 'asc'], '0', $step, $offset)['results']['ids'];
            $searchResult['results']['ids'] = array_merge($searchResult['results']['ids'], $offsetIds);
        }
    }
}


$productIds = $searchResult['results']['ids'];

if ($productIds) {

    $arSelect = array("ID", "ACTIVE", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID");
    $arFilter = array("IBLOCK_ID" => $IBLOCK_ID, "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE" => "Y", 'ID' => $productIds,);

    $res = CIBlockElement::GetList(
        array($_REQUEST['sort_field'] ?? 'NAME' => $_REQUEST['sort_order'] ?? 'asc'),
        $arFilter,
        false,
        array("nPageSize" => 12, "iNumPage" => $_REQUEST['page_num'] ?? 1),
        $arSelect
    );

    $arItems = [];
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
        $arItem = [
            'ID' => $arFields['ID'],
            'ACTIVE' => $arFields['ACTIVE'],
            'NAME' => $arFields['NAME'],
            'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
            'PREVIEW_PICTURE_SRC' => CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
            'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
            'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
            'RATIO' => $measure['RATIO'],
        ];
        $arItems[] = $arItem;
    }

    $arResult['CUR_PAGE'] = $res->NavPageNomer;
    $arResult['PAGE_COUNT'] = $res->NavPageCount;
    $arResult['SORT_FIELDS'] = Helpers\Events::getCatalogAllowSortFields();
    $arResult['ITEMS'] = $arItems;
}

echo json_encode($arResult, JSON_UNESCAPED_UNICODE);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
