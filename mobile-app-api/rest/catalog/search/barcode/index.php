<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Context;

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;
$request = Context::getCurrent()->getRequest();

$barcodeString = $request->get("code");

$arSelect = array("ID",);
$arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, "?PROPERTY_BARCODE" => $barcodeString);

$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

$requestResult = ['status' => 0, 'id' => ''];
if ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $requestResult = ['status' => 1, 'id' => $arFields['ID']];
}

echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
