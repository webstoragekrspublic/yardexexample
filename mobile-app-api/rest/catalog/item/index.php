<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;
$allTorgovajaMarka;

setIndexedTorgovajaMarka();
if (isset($_GET['id'])) {
    $arSelect = array(
        "ID",
        "NAME",
        "ACTIVE",
        "DETAIL_PICTURE",
        "PREVIEW_PICTURE",
        "DETAIL_TEXT",
        "IBLOCK_SECTION_ID",
        "PROPERTY_MORE_PHOTO",
        "PROPERTY_INGREDIENTS",
        "PROPERTY_PROTEINS",
        "PROPERTY_FAT",
        "PROPERTY_CARBOHYDRATES",
        "PROPERTY_ENERGY",
        "PROPERTY_SHELF_LIFE",
        "PROPERTY_DATE_EXPIRATION",
        "PROPERTY_TORGOVAJAMARKA"
    );
    $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, 'ID' => $_GET['id']);

    $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
    if ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
        $arItem = [
            'ID' => $arFields['ID'],
            'NAME' => $arFields['NAME'],
            'ACTIVE' => $arFields['ACTIVE'],
            'DETAIL_TEXT' => strip_tags($arFields['DETAIL_TEXT']),
            'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
            'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
            'RATIO' => $measure['RATIO'],
            'NUTRITIONAL' => [
                'PROTEINS' => $arFields['PROPERTY_PROTEINS_VALUE'],
                'FAT' => $arFields['PROPERTY_FAT_VALUE'],
                'CARBOHYDRATES' => $arFields['PROPERTY_CARBOHYDRATES_VALUE'],
                'ENERGY' => $arFields['PROPERTY_ENERGY_VALUE']
            ],
            'ABOUT' => [
                'INGREDIENTS' => $arFields['PROPERTY_INGREDIENTS_VALUE'],
                'SHELF_LIFE' => $arFields['PROPERTY_SHELF_LIFE_VALUE'],
                'DATE_EXPIRATION' => $arFields['PROPERTY_DATE_EXPIRATION_VALUE'],
                'TORGOVAJAMARKA' => getTorgovajaMarkaName($arFields['PROPERTY_TORGOVAJAMARKA_VALUE']),
            ],
            'MORE_PHOTO' => array_map('productResizeDetailPicture', $arFields['PROPERTY_MORE_PHOTO_VALUE']),
            'DETAIL_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["DETAIL_PICTURE"], array('width' => 270, 'height' => 360), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
            'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
            'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
        ];
    }
    $requestResult = $arItem;
    // pr($requestResult);
    echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);
}

function productResizeDetailPicture($image_id)
{
    return CFile::ResizeImageGet($image_id, array('width' => 270, 'height' => 360), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'];
}

function setIndexedTorgovajaMarka()
{
    global $allTorgovajaMarka;
    $torgovajaMarkaIBlockId = 118;
    $columns = ['ID', 'NAME'];
    $filter = [
        "IBLOCK_ID" => $torgovajaMarkaIBlockId
    ];

    $tmQ = CIBlockElement::getList(
        ['DATE_CREATE' => 'ASC', 'ID' => 'ASC'],
        $filter,
        false,
        false,
        $columns
    );

    while ($tm = $tmQ->fetch()) {
        $allTorgovajaMarka[$tm['ID']] = $tm;
    }
}
function getTorgovajaMarkaName($tmId)
{
    global $allTorgovajaMarka;
    return $allTorgovajaMarka[$tmId]['NAME'] ?? null;
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
