<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;

$arFilter = array(
    'IBLOCK_ID' => $CATALOG_IBLOCK_ID,
    'GLOBAL_ACTIVE' => 'Y',
);
if ($_GET['section_id'] > 0) {
    $arFilter['SECTION_ID'] = $_GET['section_id'];
}
$arSelect = array('ID', 'NAME', 'PICTURE', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID');
$obSection = CIBlockSection::GetList(array("name" => "ASC"), $arFilter,  false,  $arSelect/* , array("nTopCount" => 2) */);

$arSections = [];
while ($arResult = $obSection->GetNext()) {
    $arSection = [
        'ID' => $arResult['ID'],
        'NAME' => $arResult['NAME'],
        'PICTURE_SRC' =>  CFile::GetPath($arResult["PICTURE"]),
        'PARENT_SECTION_ID' => $arResult['IBLOCK_SECTION_ID'],
        'DEPTH_LEVEL' => $arResult['DEPTH_LEVEL'],
    ];

    $arSections[] = $arSection;
}

foreach ($arSections as $arSectionKey => $arSectionValue) {
    if ($_GET['section_id'] > 0 and $_GET['items'] === 'Y') {
        $arSelect = array("ID", "ACTIVE", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID");
        $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE" => "Y", 'SECTION_ID' => $arSectionValue['ID']);

        $res = CIBlockElement::GetList(array("name" => "ASC"), $arFilter, false, array("nTopCount" => 4), $arSelect);
        $arItems = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
            $arItem = [
                'ID' => $arFields['ID'],
                'ACTIVE' => $arFields['ACTIVE'],
                'NAME' => $arFields['NAME'],
                'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
                'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
                'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
                'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
                'RATIO' => $measure['RATIO'],
            ];
            $arItems[] = $arItem;
        }
        $arSections[$arSectionKey]['data'] = [$arItems];
    } else {
        $arSections[$arSectionKey]['data'] = [];
    }
}
// pr($arSections);
echo json_encode($arSections, JSON_UNESCAPED_UNICODE);


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
