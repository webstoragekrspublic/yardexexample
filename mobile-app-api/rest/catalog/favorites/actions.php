<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader;
use \Custom\FavoritesOnCookies\CurrentUserFavorites;
use Bitrix\Main\Application;

if (Loader::includeModule('custom.favoritesoncookies')) {
    $currentUserFavorites = CurrentUserFavorites::getInstance();

    $request = Application::getInstance()->getContext()->getRequest();
    $action = $request->get('action');
    $itemId = $request->get('itemId');
    $answer = [
        'ERROR' => 1,
        'ERROR_TEXT' => 'произошла ошибка'
    ];
    if ($itemId) {
        switch ($action) {
            case 'add':
                global $USER;
                if ($currentUserFavorites->isLimitIsReachedForItemIds()) {
                    $errorText = 'Лимит избранного в ' . $currentUserFavorites->getItemIdsLimit() . ' шт. достигнут.';
                    if (!$currentUserFavorites->isAuthorizedType()) {
                        $errorText .= ' Авторизуйтесь, чтобы добавлять больше.';
                    }
                    $answer = [
                        'ERROR' => 2,
                        'ERROR_TEXT' => $errorText,
                        'NOT_EMPTY' => $currentUserFavorites->getTotalCnt() > 0 ? true : false,
                        'ITEM_IDS' => array_keys($currentUserFavorites->getItemIds())
                    ];
                } else if ($currentUserFavorites->addItemId($itemId)) {
                    $answer = [
                        'ERROR' => 0,
                        'ERROR_TEXT' => '',
                        'NOT_EMPTY' => $currentUserFavorites->getTotalCnt() > 0 ? true : false,
                        'ITEM_IDS' => array_keys($currentUserFavorites->getItemIds())
                    ];
                }
                break;
            case 'remove':
                $currentUserFavorites->removeItemId($itemId);
                $answer = [
                    'ERROR' => 0,
                    'ERROR_TEXT' => '',
                    'NOT_EMPTY' => $currentUserFavorites->getTotalCnt() > 0 ? true : false,
                    'ITEM_IDS' => array_keys($currentUserFavorites->getItemIds())
                ];
                break;
        }
    }

    echo json_encode($answer, JSON_UNESCAPED_UNICODE);
}
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
