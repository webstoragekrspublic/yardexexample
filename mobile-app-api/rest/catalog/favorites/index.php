<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


use \Bitrix\Main\Loader;

Loader::includeModule('custom.favoritesoncookies');

global $USER;

$USER->logout();
$result = $USER->Login($_GET['login'], $_GET['hash'], "Y", 'N');
$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;

if ($result == 1) {

    $currentUserFavorites = \Custom\FavoritesOnCookies\CurrentUserFavorites::getInstance();

    $favorites = [
        'USER' => $USER->GetID(),
        'NOT_EMPTY' => $currentUserFavorites->getTotalCnt() > 0 ? true : false,
        'ITEM_IDS' => array_keys($currentUserFavorites->getItemIds()),
    ];
    if ($_REQUEST['items'] == 'Y') {
        $arSelect = array("ID", "ACTIVE", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID",);
        $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE" => "Y", 'ID' => $favorites['ITEM_IDS']);

        $res = CIBlockElement::GetList(array($_REQUEST['sort_field'] => $_REQUEST['sort_order']), $arFilter, false, array("nPageSize" => 6, "iNumPage" => $_REQUEST['page_num'] ?? 1), $arSelect);

        $arItems = [];
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
            $arItem = [
                'ID' => $arFields['ID'],
                'ACTIVE' => $arFields['ACTIVE'],
                'NAME' => $arFields['NAME'],
                'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
                'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
                'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
                'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
                'RATIO' => $measure['RATIO'],
            ];
            $arItems[] = $arItem;
        }
        $favorites['CUR_PAGE'] = $res->NavPageNomer;
        $favorites['PAGE_COUNT'] = $res->NavPageCount;
        $favorites['SORT_FIELDS'] = Helpers\Events::getCatalogAllowSortFields();
        $favorites['ITEMS'] = $arItems;
    }

    echo json_encode($favorites, JSON_UNESCAPED_UNICODE);
}


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
