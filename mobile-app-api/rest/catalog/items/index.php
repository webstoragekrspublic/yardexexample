<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;
if (isset($_GET['section_id'])) {
    $arSelect = array("ID", "ACTIVE", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID",);
    $arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE" => "Y", 'SECTION_ID' => $_GET['section_id']);

    $res = CIBlockElement::GetList(array($_GET['sort_field'] => $_GET['sort_order']), $arFilter, false, array("nPageSize" => 6, "iNumPage" => $_GET['page_num'] ?? 1), $arSelect);

    $arItems = [];
    while ($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
        $arItem = [
            'ID' => $arFields['ID'],
            'ACTIVE' => $arFields['ACTIVE'],
            'NAME' => $arFields['NAME'],
            'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
            'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
            'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
            'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
            'RATIO' => $measure['RATIO'],
        ];
        $arItems[] = $arItem;
    }
    $requestResult = [];
    $requestResult['CUR_PAGE'] = $res->NavPageNomer;
    $requestResult['PAGE_COUNT'] = $res->NavPageCount;
    $requestResult['SORT_FIELDS'] = Helpers\Events::getCatalogAllowSortFields();
    $requestResult['ITEMS'] = $arItems;
    // pr($requestResult);
    echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
