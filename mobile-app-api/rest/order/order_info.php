<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// Способы доставки
$resDelivery = CSaleDelivery::GetList(array(), array("LID" => SITE_ID, "ACTIVE" => "Y", "ID" => [2]), false, false, array());

if ($arDelivery = $resDelivery->Fetch()) {
    do {
        $arResult["DELIVERY_TYPE"][] = $arDelivery;
    } while ($arDelivery = $resDelivery->Fetch());
}

// Способы оплаты
$arPaySystem   = array();
$availablePayments =  [9, 22];
if ($_REQUEST['platform'] == 'android') {
    $availablePayments =  [9, 22];
} else if ($_REQUEST['platform'] == 'ios') {
    $availablePayments =  [9, 22];
}
$resPaySystem   = CSalePaySystem::GetList(array(), array("LID" => SITE_ID, "ACTIVE" => "Y", "ID" => $availablePayments), false, false, array());

while ($arPaySystemItem = $resPaySystem->Fetch()) {
    $arResult["PAY_SYSTEM"][] = $arPaySystemItem;
}
$arResult["TIME_SLOTS"] = \Custom\DeliveryTime\DeliveryTime::getInstance()->getDaysInfoForCurrentCart(5);

// pr($arResult);
echo json_encode($arResult, JSON_UNESCAPED_UNICODE);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
