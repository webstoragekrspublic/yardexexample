<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Context,
    Bitrix\Currency\CurrencyManager,
    Bitrix\Sale\Order,
    Bitrix\Sale\Basket,
    Bitrix\Sale\Delivery,
    Bitrix\Sale\PaySystem,
    Bitrix\Sale\DiscountCouponsManager;

global $USER;

Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");

/* $orderId = '838';
$order = Order::load($orderId); */

//ПРЕДВАРИТЕЛЬНАЯ ОБРАБОТКА

$request = Context::getCurrent()->getRequest();

$entityBody = file_get_contents('php://input');
$orderParams = json_decode($entityBody, true)['order']; //парсинг параметров


if (!empty($request->get("order"))) {
    $orderParams = json_decode($request->get("order"), true);
}

$loginFields = [];
if (!$USER->GetByLogin($orderParams['email'])->Fetch()['ID']) { //если заказ сделал незарегистрированный пользователь, то скрытая регистария и авторизация 
    $password = randString(8);
    $new_user_id = $USER->Add(array(
        'LOGIN' => $orderParams['email'],
        'EMAIL' => $orderParams['email'],
        'NAME' => $orderParams['fio'],
        'LAST_NAME' => '',
        'PASSWORD' => $password,
        "PHONE_NUMBER"    => $orderParams['phone'],
        "PERSONAL_PHONE"    => $orderParams['phone'],
        'CONFIRM_PASSWORD' => $password,
        "GROUP_ID" => array(3, 4),
        'ACTIVE' => "Y",
    ));
    if ($new_user_id > 0) {
        $USER->Authorize($new_user_id);
    }
    $loginFields = [
        "EMAIL" => $orderParams['email'],
        "PASSWORD" => $password,
    ];
    \CEvent::Send("USER_INFO", "s1", $loginFields, 'N', $postalTemplate = 145);
}
else{
    //getLog($orderParams);
}

//СОЗДАНИЕ ЗАКАЗА
$siteId = Context::getCurrent()->getSite();
$currencyCode = CurrencyManager::getBaseCurrency();

$order = Order::create($siteId, $USER->isAuthorized() ? $USER->GetID() : $USER->GetByLogin($orderParams['email'])->Fetch()['ID']);
$order->setPersonTypeId(1);
$order->setField('CURRENCY', $currencyCode);

//УСТАНОВКА КОРЗИНЫ
$basket = Basket::create($siteId);
foreach ($orderParams['basketItems'] as $product) {
    $item = $basket->createItem('catalog', $product['id']);
    $item->setFields(array(
        'QUANTITY' => $product['qty'],
        'CURRENCY' => $currencyCode,
        'LID' => $siteId,
        'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
    ));
}


//ПРИМЕНЕНИЕ ПРОМОКОДОВ 
DiscountCouponsManager::init();
DiscountCouponsManager::clear(true);
if (!empty($orderParams['coupons'])) {
    foreach ($orderParams['coupons'] as $coupon) {
        DiscountCouponsManager::add($coupon);
    }
}
$discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId(true)));
$basket->refreshData(['PRICE',  'COUPONS']);

try {
    $r = $discounts->calculate();
    $basket->applyDiscount($r->getData());
} catch (\Throwable $th) {
}

$order->setBasket($basket);

//УСТАНОВКА ДОСТАВКИ
$shipmentCollection = $order->getShipmentCollection();
$shipment = $shipmentCollection->createItem();
$service = Delivery\Services\Manager::getById($orderParams['delivery']);
$shipment->setFields(array(
    'DELIVERY_ID' => $service['ID'],
    'DELIVERY_NAME' => $service['NAME'],
));
$shipmentItemCollection = $shipment->getShipmentItemCollection();
foreach ($order->getBasket() as $item) {
    $shipmentItem = $shipmentItemCollection->createItem($item);
    $shipmentItem->setQuantity($item->getQuantity());
}

//УСТАНОВКА ОПЛАТЫ
$paymentCollection = $order->getPaymentCollection();
$payment = $paymentCollection->createItem();
$paySystemService = PaySystem\Manager::getObjectById($orderParams['payment']);
$payment->setFields(array(
    'PAY_SYSTEM_ID' => $paySystemService->getField("PAY_SYSTEM_ID"),
    'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
    'SUM' => $order->getPrice()
));

//УСТАНОВКА СВОЙСТВ
$propertyCollection = $order->getPropertyCollection();

$emailProp = $propertyCollection->getUserEmail();
$payNameProp  = $propertyCollection->getPayerName();
$profileNameProp = $propertyCollection->getProfileName();
$phoneProp = $propertyCollection->getPhone();
$addressProp  = $propertyCollection->getAddress();

$emailProp->setValue($orderParams['email']);
$payNameProp->setValue($orderParams['fio']);
$profileNameProp->setValue($orderParams['fio']);
$phoneProp->setValue($orderParams['phone']);
$addressProp->setValue($orderParams['address']);

if ($orderParams['comment']) {
    $order->setField('USER_DESCRIPTION', $orderParams['comment']);
}
$sourcePropValue = $propertyCollection->getItemByOrderPropertyId($orderSourcePropId = 37);
$sourcePropValue->setValue('Мобильное приложение ' . $request->get("platform"));

$timeslotDateProp = $propertyCollection->getItemByOrderPropertyId($timeslotDatePropId = 20);
$timeslotDateProp->setValue($orderParams['timeslot']['date']);

$timeslotTimeProp = $propertyCollection->getItemByOrderPropertyId($timeslotTimePropId = 21);
$timeslotTimeProp->setValue($orderParams['timeslot']['time']);

$curOrderUserCouponsPropValue = $propertyCollection->getItemByOrderPropertyId($curOrderUserCouponsPropId = 39);
$curOrderUserCouponsPropValue->setValue(json_encode($orderParams['coupons'], JSON_UNESCAPED_UNICODE));

if (!empty($orderParams['fias_code'])) {
    $fiasCodeProp = $propertyCollection->getItemByOrderPropertyId($fiasCodePropId = 26);
    $fiasCodeProp->setValue($orderParams['fias_code']);
}
if (!empty($orderParams['coords'])) {
    $geoCoordsProp = $propertyCollection->getItemByOrderPropertyId($geoCoordsPropId = 38);
    $geoCoordsProp->setValue($orderParams['coords']);
}
//ЗАВЕРШЕНИЕ
$order->doFinalAction(true);
$result = $order->save();
$orderId = $order->getId();

if (is_numeric($orderId) != 0) {
    echo json_encode(['STATUS' => true, 'ID' => $orderId, 'TOTAL_PRICE' => $order->getField('PRICE'), 'LOGIN_DATA' => $loginFields], JSON_UNESCAPED_UNICODE);

    //ДОП ОБРАБОТКА
    if ($orderParams['notCallChecked']) { //если отмечена галочка не перезванивать
        Helpers\Events::turnPropertyNotCallBack($orderId, false);
    }
} else {
    echo json_encode(['STATUS' => false,], JSON_UNESCAPED_UNICODE);
}


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
