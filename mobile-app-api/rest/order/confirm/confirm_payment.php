<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Loader;
use \Custom\OrderPaycheck\OrderPaycheckHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$orderId = $_REQUEST['order_id'];

if (Loader::includeModule('custom.orderpaycheck')) {
    $paymentInfo = OrderPaycheckHelper::getOrderPaidStatusAndSumFromSber($orderId);
    if ($paymentInfo['STATUS'] == 'IS_PAID') {
        OrderPaycheckHelper::setOrderPaidStatusAndSum($orderId, 'Y', $paymentInfo['SUM']);
    }
}


// pr($arResult);
// echo json_encode($arResult, JSON_UNESCAPED_UNICODE);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
