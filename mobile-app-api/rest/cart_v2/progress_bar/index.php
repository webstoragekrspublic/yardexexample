<?

use Bitrix\Main\Loader;
use Bitrix\Sale\DiscountCouponsManager;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
Loader::includeModule('sale');


$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;

$minSums = [];
$itemIDs = [];

$products = \Bitrix\Sale\Internals\DiscountTable::getList(
    [
        'filter' => ['XML_ID' => 'PROGRESS_BAR%', 'ACTIVE' => 'Y'],
        'select' => ['ACTIONS_LIST', 'CONDITIONS_LIST']
    ]
);
while ($item = $products->fetch()) {
    foreach ($item['CONDITIONS_LIST']['CHILDREN'] as $children) {
        if ($children['CLASS_ID'] == 'CondBsktAmtGroup') {
            $val = $children['DATA']['Value'];
        }
    }
    foreach ($item['ACTIONS_LIST']['CHILDREN'] as $childrenAct) {
        if ($childrenAct['CLASS_ID'] == 'ActSaleBsktGrp') {
            foreach ($childrenAct['CHILDREN'] as $isProdact) {
                if ($isProdact['CLASS_ID'] == 'CondBsktFldProduct') {
                    $itemIDs[] = $isProdact['DATA']['value'];
                    $minSums[$isProdact['DATA']['value']] = $val;
                }
            }
        }
        break;
    }
}
$arSelect = array("ID", "ACTIVE", "NAME", "PREVIEW_PICTURE", "IBLOCK_SECTION_ID",);
$arFilter = array("IBLOCK_ID" => $CATALOG_IBLOCK_ID, "ACTIVE" => "Y", 'ID' => $itemIDs);

$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);

$arItems = [];
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $measure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($arFields['ID'])[$arFields['ID']];
    $arItem = [
        'ID' => $arFields['ID'],
        'ACTIVE' => $arFields['ACTIVE'],
        'NAME' => $arFields['NAME'],
        'PARENT_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
        'PREVIEW_PICTURE_SRC' =>   CFile::ResizeImageGet($arFields["PREVIEW_PICTURE"], array('width' => 180, 'height' => 240), BX_RESIZE_IMAGE_PROPORTIONAL, true)['src'],
        'PRICE' => CCatalogProduct::GetOptimalPrice($arFields['ID'])['RESULT_PRICE'],
        'MEASURE' => $measure['MEASURE']['SYMBOL_RUS'],
        'RATIO' => $measure['RATIO'],
        'MIN_SUM' => $minSums[$arFields['ID']]
    ];
    $arItems[] = $arItem;
}
$requestResult = $arItems;

// pr($requestResult);
echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);




require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
