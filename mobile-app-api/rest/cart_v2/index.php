<?

use Bitrix\Main\Loader;
use Bitrix\Sale\DiscountCouponsManager;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
Loader::includeModule('sale');


$CATALOG_IBLOCK_ID = Helpers\Constants::PRODUCTS_IBLOCK_ID;
$arBackParametrs = CNext::GetBackParametrsValues(SITE_ID);

$requestResult = [];
// $_POST = json_decode(file_get_contents('php://input'), true);
if (CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID())) {
    $basketItemsLocal = json_decode($_GET['basket_items'], true) ?? [];
    $coupons = json_decode($_REQUEST['coupons'], true) ?? [];
    $basketIDs = [];
    foreach ($basketItemsLocal as $basketItemsLocalKey => $basketItemLocal) {
        $basketID  = Add2BasketByProductID($basketItemLocal['ID'], $basketItemLocal['QUANTITY'] ?? 1);
        $basketIDs[$basketItemLocal['ID']] = !empty($basketID) ? $basketID : 0;
    }
    $basket = Bitrix\Sale\Basket::loadItemsForFUser(Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

    if (!empty($basketIDs)) {
        DiscountCouponsManager::init();
        DiscountCouponsManager::clear(true);
        if (!empty($coupons)) {
            foreach ($coupons as $coupon) {
                DiscountCouponsManager::add($coupon['COUPON']);
            }
        }
        try {
            $result = calculateDiscount($basket);
            $basket->applyDiscount($result['BASKET_ITEMS']);
        } catch (\Throwable $th) {
        }

        $basketItems = $basket->getBasketItems();
        $resultBasketItems = [];
        foreach ($basket as $basketItem) {
            $resultBasketItem = [];
            $resultBasketItem['BASKET_ID'] = $basketItem->getId();
            $resultBasketItem['DISCOUNT_PRICE'] = $basketItem->getPrice();
            $resultBasketItems[$basketItem->getProductId()] = $resultBasketItem;
        }

        $coupons = DiscountCouponsManager::get(true, [], true, true);
        $resultCoupons = [];
        foreach ($coupons as $coupon) {
            $resultCoupon = [];
            $resultCoupon['COUPON'] = $coupon['COUPON'];
            if ($coupon['STATUS'] == 4) {
                $resultCoupon['STATUS'] = true;
                $resultCoupon['STATUS_TEXT'] = 'промокод применен, ' . $coupon['DISCOUNT_NAME'];
            } else {
                $resultCoupon['STATUS'] = false;
                $resultCoupon['STATUS_TEXT'] = $coupon['STATUS_TEXT'];
            }
            $resultCoupons[] = $resultCoupon;
        }

        $requestResult['ITEMS'] = $resultBasketItems;
        $requestResult['COUPONS'] = $resultCoupons;
        $requestResult['PRICE'] = $basket->getPrice();
        $requestResult['BASE_PRICE'] = $basket->getBasePrice();
        $requestResult['WEIGHT'] = $basket->getWeight();
        $requestResult['MIN_SUM'] = $arBackParametrs['MIN_ORDER_PRICE'];
    }
    // pr($requestResult);
    echo json_encode($requestResult, JSON_UNESCAPED_UNICODE);
}

function calculateDiscount($basket)
{
    $discounts = \Bitrix\Sale\Discount::buildFromBasket($basket, new \Bitrix\Sale\Discount\Context\Fuser($basket->getFUserId(true)));
    $basket->refreshData(['PRICE',  'COUPONS']);
    $r = $discounts->calculate();
    return $r->getData();
}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
