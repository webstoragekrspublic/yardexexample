<?

use Handlers\RestApi\Auth;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");



if (!isset($_GET['action'])) die('нет параметра действия');

$action = $_GET['action'];

if (!is_object($USER)) $USER = new CUser;

switch ($action) {
    case 'update_field':
        $fields = array(
            "NAME"              => $_GET['name'],
            "LAST_NAME"         => $_GET['last_name'],
            "EMAIL"             => $_GET['email'],
            "LOGIN"             => $_GET['email'],
            "PERSONAL_PHONE"    => $_GET['phone'],
        );
        $arResult = $USER->Update($_GET['id'], $fields);
        if ($USER->LAST_ERROR) {
            $error = [
                'MESSAGE' => explode('<br>', $USER->LAST_ERROR)[0],
                'TYPE' => 'ERROR',
                'ERROR_TYPE' => 'EDIT_PROFILE'
            ];
            echo json_encode($error, JSON_UNESCAPED_UNICODE);
        }
        break;
    case 'update_password':
        $result = $USER->Login($_GET['login'], $_GET['password'], "Y", 'Y');
        if ($result == 1) {
            $fields = array(
                "PASSWORD"          => $_GET['new_password'],
                "CONFIRM_PASSWORD"  => $_GET['confirm_password'],
            );
            $arResult = $USER->Update($_GET['id'], $fields);
            if ($USER->LAST_ERROR) {
                $error = [
                    'MESSAGE' => explode('<br>', $USER->LAST_ERROR)[0],
                    'TYPE' => 'ERROR',
                    'ERROR_TYPE' => 'EDIT_PASSWORD'
                ];
                echo json_encode($error, JSON_UNESCAPED_UNICODE);

                break;
            }
            $rsUser = CUser::GetByLogin($_GET['login']);
            $arUser = $rsUser->Fetch();
            echo json_encode($arUser['PASSWORD'], JSON_UNESCAPED_UNICODE);
            break;
        } else {
            $result['MESSAGE'] = 'Неверный пароль';
            echo json_encode($result, JSON_UNESCAPED_UNICODE);
        }

        break;
}


require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
